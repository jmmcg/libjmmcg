/******************************************************************************
 ** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline
xor_modulo_hasher::xor_modulo_hasher(container_type const &v, std::size_t const num_threads) noexcept(false)
: base_t(v, num_threads) {
}

REALLY_FORCE_INLINE inline
xor_modulo_hasher::element_type
xor_modulo_hasher::generate_hash(container_type::value_type const v, element_type const mask, std::size_t const denominator) const noexcept(true) {
	return static_cast<element_type>((v^mask)%denominator);
}

} }
