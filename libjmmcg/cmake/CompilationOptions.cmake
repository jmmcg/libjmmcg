## Copyright (c) 2023 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

include(CheckCXXCompilerFlag)
include(CheckIPOSupported)
include(FeatureSummary)
include(GenerateExportHeader)
include(TestCXXAcceptsFlag)
#CHECK_CXX_COMPILER_FLAG("-std=c++23" COMPILER_SUPPORTS_CXX23)
#set(CMAKE_CXX_STANDARD 23)

check_ipo_supported()

function(add_library_with_options TARGET_BASE_NAME TARGET_SOURCES)
    string(TOUPPER ${PROJECT_NAME} EXPORTS_MACRO_PREFIX)

    add_library(${TARGET_BASE_NAME}_objlib OBJECT
        ${TARGET_SOURCES}
    )
    target_compile_features(${TARGET_BASE_NAME}_objlib
        PUBLIC
            cxx_std_23
    )
    # To discover the '-march' & '-mtune- options use:
    # gcc -march=native -Q --help=target|grep march
    # gcc -mtune=native -Q --help=target|grep mtune
    target_compile_options(${TARGET_BASE_NAME}_objlib
        PUBLIC
            # Refer to https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html for details.
            -ggdb -march=native -mno-3dnow -pedantic -pipe -frtti -fsigned-char -Wall -Wcast-qual -Wconversion -Wextra -Wnull-dereference -Wpointer-arith -Wold-style-cast -pthread
            -mcx16 -msahf -mmovbe -mshstk -mcrc32 -mmwaitx
            $<$<CXX_COMPILER_ID:Clang>:-fbracket-depth=${JMMCG_TOTAL_NUM_MICS_ROUNDED_UP}>
            $<$<CXX_COMPILER_ID:GNU>:-Warith-conversion -frecord-gcc-switches -fpeel-loops -freg-struct-return>
            $<$<CONFIG:Debug>:-O0 -fstack-clash-protection -fstack-protector-all>
            $<$<CONFIG:Release>:-O3>
    )
    target_compile_definitions(${TARGET_BASE_NAME}_objlib
        PUBLIC
            BOOST_LOG_DYN_LINK
            NOMINMAX
            $<$<CONFIG:Debug>:DEBUG>
            $<$<CONFIG:Release>:NDEBUG>
    )
    target_include_directories(${TARGET_BASE_NAME}_objlib
        PUBLIC
          ${CMAKE_CURRENT_SOURCE_DIR}
          ${CMAKE_CURRENT_SOURCE_DIR}/..
          ${CMAKE_CURRENT_BINARY_DIR}
          ${CMAKE_CURRENT_BINARY_DIR}/..
    )
    set_target_properties(${TARGET_BASE_NAME}_objlib
        PROPERTIES
            POSITION_INDEPENDENT_CODE 1
            INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
    )
    add_dynamic_library_with_options_private(${TARGET_BASE_NAME})
    add_static_library_with_options_private(${TARGET_BASE_NAME})
    generate_export_header(${TARGET_BASE_NAME} BASE_NAME ${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME})
    install(
        TARGETS ${TARGET_BASE_NAME} ${TARGET_BASE_NAME}_archive
        EXPORT ${PROJECT_NAME}Targets
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT runtime
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT development
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}	COMPONENT runtime
        PUBLIC_HEADER DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME} COMPONENT development
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
    install(
        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME}
        COMPONENT development
        FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp" PATTERN "*.xdr" PATTERN "*.xml"
        PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
    )
    install(
        DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME}
        COMPONENT development
        FILES_MATCHING PATTERN "${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME}*.h*"
        PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
    )
endfunction()

function(add_dynamic_library_with_options_private TARGET_BASE_NAME)
    string(TOUPPER ${PROJECT_NAME} EXPORTS_MACRO_PREFIX)

    add_library(${TARGET_BASE_NAME} SHARED $<TARGET_OBJECTS:${TARGET_BASE_NAME}_objlib>)
    set_target_properties(${TARGET_BASE_NAME}
        PROPERTIES
            VERSION ${VERSION_NUMBER}
            SOVERSION ${VERSION_NUMBER}
            POSITION_INDEPENDENT_CODE 1
            INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
    )
    target_link_options(${TARGET_BASE_NAME}
        PRIVATE
            "-Wl,--build-id=0x${GIT_HEAD_HASH}"
            "-Wl,-O3"
            "-Wl,--enable-new-dtags"
            "-Wl,--sort-common"
            "-Wl,-z,relro"
        PUBLIC
            $<$<CONFIG:Debug>:-fsanitize=address -fsanitize=leak -fsanitize=undefined>
    )
    target_include_directories(${TARGET_BASE_NAME}
        PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/..>
            $<INSTALL_INTERFACE:include/${PROJECT_NAME}/${VERSION_NUMBER}>
    )
    target_compile_features(${TARGET_BASE_NAME}
        PUBLIC
            cxx_std_23
    )
    target_compile_options(${TARGET_BASE_NAME}
        PUBLIC
            # Refer to https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html for details.
            -ggdb -march=native -mno-3dnow -pedantic -pipe -frtti -fsigned-char -Wall -Wcast-qual -Wconversion -Wextra -Wnull-dereference -Wpointer-arith -Wold-style-cast -pthread
            -mcx16 -msahf -mmovbe -mshstk -mcrc32 -mmwaitx
            $<$<CXX_COMPILER_ID:Clang>:-fbracket-depth=${JMMCG_TOTAL_NUM_MICS_ROUNDED_UP}>
            $<$<CXX_COMPILER_ID:GNU>:-Warith-conversion -frecord-gcc-switches -fpeel-loops -freg-struct-return>
            $<$<CONFIG:Debug>:-O0 -fstack-clash-protection -fstack-protector-all>
            $<$<CONFIG:Release>:-O3>
    )
    target_compile_definitions(${TARGET_BASE_NAME}
        PUBLIC
            DEPENDS_EXPLICIT_ONLY
            NOMINMAX
            $<$<CONFIG:Debug>:DEBUG>
            $<$<CONFIG:Release>:NDEBUG>
        PRIVATE
            ${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME}_EXPORT
    )
endfunction()

function(add_static_library_with_options_private TARGET_BASE_NAME)
    string(TOUPPER ${PROJECT_NAME} EXPORTS_MACRO_PREFIX)

    add_library(${TARGET_BASE_NAME}_archive STATIC $<TARGET_OBJECTS:${TARGET_BASE_NAME}_objlib>)
    set_target_properties(${TARGET_BASE_NAME}_archive
        PROPERTIES
            OUTPUT_NAME "${TARGET_BASE_NAME}"
            PREFIX "lib"
            VERSION ${VERSION_NUMBER}
            POSITION_INDEPENDENT_CODE 1
            INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
            LINK_OPTIONS WHOLE_ARCHIVE
    )
    target_include_directories(${TARGET_BASE_NAME}_archive
        PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/..>
            $<INSTALL_INTERFACE:include/${PROJECT_NAME}/${VERSION_NUMBER}>
    )
    target_compile_features(${TARGET_BASE_NAME}_archive
        PUBLIC
            cxx_std_23
    )
    target_compile_options(${TARGET_BASE_NAME}_archive
        PUBLIC
            # Refer to https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html for details.
            -ggdb -march=native -mno-3dnow -pedantic -pipe -frtti -fsigned-char -Wall -Wcast-qual -Wconversion -Wextra -Wnull-dereference -Wpointer-arith -Wold-style-cast -pthread
            -mcx16 -msahf -mmovbe -mshstk -mcrc32 -mmwaitx
            $<$<CXX_COMPILER_ID:Clang>:-fbracket-depth=${JMMCG_TOTAL_NUM_MICS_ROUNDED_UP}>
            $<$<CXX_COMPILER_ID:GNU>:-Warith-conversion -frecord-gcc-switches -fpeel-loops -freg-struct-return>
            $<$<CONFIG:Debug>:-O0 -fstack-clash-protection -fstack-protector-all>
            $<$<CONFIG:Release>:-O3>
    )
    target_compile_definitions(${TARGET_BASE_NAME}_archive
        PUBLIC
            ${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME}_STATIC_DEFINE
            DEPENDS_EXPLICIT_ONLY
            NOMINMAX
            $<$<CONFIG:Debug>:DEBUG>
            $<$<CONFIG:Release>:NDEBUG>
    )
endfunction()

function(add_dynamic_library_with_options TARGET_BASE_NAME TARGET_SOURCES)
    string(TOUPPER ${PROJECT_NAME} EXPORTS_MACRO_PREFIX)

    add_library(${TARGET_BASE_NAME} SHARED
        ${TARGET_SOURCES}
    )
    target_compile_features(${TARGET_BASE_NAME}
        PUBLIC
            cxx_std_23
    )
    target_compile_options(${TARGET_BASE_NAME}
        PUBLIC
            # Refer to https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html for details.
            -ggdb -march=native -mno-3dnow -pedantic -pipe -frtti -fsigned-char -Wall -Wcast-qual -Wconversion -Wextra -Wnull-dereference -Wpointer-arith -Wold-style-cast -pthread
            -mcx16 -msahf -mmovbe -mshstk -mcrc32 -mmwaitx
            $<$<CXX_COMPILER_ID:Clang>:-fbracket-depth=${JMMCG_TOTAL_NUM_MICS_ROUNDED_UP}>
            $<$<CXX_COMPILER_ID:GNU>:-Warith-conversion -frecord-gcc-switches -fpeel-loops -freg-struct-return>
            $<$<CONFIG:Debug>:-O0 -fstack-clash-protection -fstack-protector-all>
            $<$<CONFIG:Release>:-O3>
    )
    target_include_directories(${TARGET_BASE_NAME}
        PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
            $<INSTALL_INTERFACE:include/${PROJECT_NAME}/${VERSION_NUMBER}>
    )
    set_target_properties(${TARGET_BASE_NAME}
        PROPERTIES
            VERSION ${VERSION_NUMBER}
            SOVERSION ${VERSION_NUMBER}
            POSITION_INDEPENDENT_CODE 1
            INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
    )
    target_link_options(${TARGET_BASE_NAME}
        PRIVATE
            "-Wl,--build-id=0x${GIT_HEAD_HASH}"
            "-Wl,-O3"
            "-Wl,--enable-new-dtags"
            "-Wl,--sort-common"
            "-Wl,-z,relro"
        PUBLIC
            $<$<CONFIG:Debug>:-fsanitize=address -fsanitize=leak -fsanitize=undefined>
    )
    target_compile_definitions(${TARGET_BASE_NAME}
        PUBLIC
            DEPENDS_EXPLICIT_ONLY
            NOMINMAX
            $<$<CONFIG:Debug>:DEBUG>
            $<$<CONFIG:Release>:NDEBUG>
        PRIVATE
            ${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME}_EXPORT
    )
    generate_export_header(${TARGET_BASE_NAME} BASE_NAME ${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME})
    install(
        TARGETS ${TARGET_BASE_NAME}
        EXPORT ${PROJECT_NAME}Targets
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT runtime
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT development
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}	COMPONENT runtime
        PUBLIC_HEADER DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME} COMPONENT development
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
    install(
        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME}
        COMPONENT development
        FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp" PATTERN "*.xdr" PATTERN "*.xml"
        PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
    )
    install(
        DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME}
        COMPONENT development
        FILES_MATCHING PATTERN "${EXPORTS_MACRO_PREFIX}_${TARGET_BASE_NAME}*.h*"
        PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
    )
endfunction()

function(add_executable_with_options TARGET_BASE_NAME TARGET_SOURCES)
    add_executable(${TARGET_BASE_NAME}
        ${TARGET_SOURCES}
    )
    target_compile_features(${TARGET_BASE_NAME}
        PUBLIC
            cxx_std_23
    )
    target_compile_options(${TARGET_BASE_NAME}
        PUBLIC
            # Refer to https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html for details.
            -ggdb -march=native -mno-3dnow -pedantic -pipe -frtti -fsigned-char -Wall -Wcast-qual -Wconversion -Wextra -Wnull-dereference -Wpointer-arith -Wold-style-cast -pthread
            -mcx16 -msahf -mmovbe -mshstk -mcrc32 -mmwaitx
            $<$<CXX_COMPILER_ID:Clang>:-fbracket-depth=${JMMCG_TOTAL_NUM_MICS_ROUNDED_UP}>
            $<$<CXX_COMPILER_ID:GNU>:-Warith-conversion -frecord-gcc-switches -fpeel-loops -freg-struct-return>
            $<$<CONFIG:Debug>:-O0 -fstack-clash-protection -fstack-protector-all>
            $<$<CONFIG:Release>:-O3>
    )
    target_include_directories(${TARGET_BASE_NAME}
        PRIVATE
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
    )
    set_target_properties(${TARGET_BASE_NAME}
        PROPERTIES
            VERSION ${VERSION_NUMBER}
            INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
    )
    target_link_options(${TARGET_BASE_NAME}
        PRIVATE
            "-Wl,--build-id=0x${GIT_HEAD_HASH}"
            "-Wl,-O3"
            "-Wl,--enable-new-dtags"
            "-Wl,--sort-common"
            "-Wl,-z,relro"
            $<$<CONFIG:Debug>:-fsanitize=address -fsanitize=leak -fsanitize=undefined>
    )
    target_compile_definitions(${TARGET_BASE_NAME}
        PUBLIC
            DEPENDS_EXPLICIT_ONLY
            NOMINMAX
            $<$<CONFIG:Debug>:DEBUG>
            $<$<CONFIG:Release>:NDEBUG>
    )
    install(
        TARGETS ${TARGET_BASE_NAME}
        EXPORT ${PROJECT_NAME}Targets
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT runtime
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT development
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}	COMPONENT runtime
        PUBLIC_HEADER DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}/${TARGET_BASE_NAME} COMPONENT development
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
endfunction()

function(add_target_details BASE_DEPENDEE_TARGET DEPENDANT_TARGET)
    target_compile_features(${BASE_DEPENDEE_TARGET}_objlib
        PUBLIC
            $<TARGET_PROPERTY:${DEPENDANT_TARGET},INTERFACE_COMPILE_FEATURES>
    )
    target_compile_options(${BASE_DEPENDEE_TARGET}_objlib
        PUBLIC
            $<TARGET_PROPERTY:${DEPENDANT_TARGET},INTERFACE_COMPILE_OPTIONS>
    )
    target_include_directories(${BASE_DEPENDEE_TARGET}_objlib
        PUBLIC
            $<TARGET_PROPERTY:${DEPENDANT_TARGET},INTERFACE_INCLUDE_DIRECTORIES>
    )
endfunction()

function(add_link_target_details BASE_DEPENDEE_TARGET BASE_DEPENDANT_TARGET)
    add_target_details(${BASE_DEPENDEE_TARGET} ${BASE_DEPENDANT_TARGET})
    target_link_libraries(${BASE_DEPENDEE_TARGET}
        PUBLIC
            ${BASE_DEPENDANT_TARGET}
    )
    target_link_libraries(${BASE_DEPENDEE_TARGET}_archive
        PUBLIC
            ${BASE_DEPENDANT_TARGET}_archive
    )
    add_dependency_target_details(${BASE_DEPENDEE_TARGET} ${BASE_DEPENDANT_TARGET})
endfunction()

# Needed due to the fact that the _objlibs need the correct include paths adding....
# TODO Consider: https://stackoverflow.com/questions/23327687/how-to-write-a-cmake-function-with-more-than-one-parameter-groups
function(add_link_target_details_raw BASE_DEPENDEE_TARGET DEPENDANT_TARGET)
    add_target_details(${BASE_DEPENDEE_TARGET} ${DEPENDANT_TARGET})
    target_link_libraries(${BASE_DEPENDEE_TARGET}
        PUBLIC
            ${DEPENDANT_TARGET}
    )
    target_link_libraries(${BASE_DEPENDEE_TARGET}_archive
        PUBLIC
            ${DEPENDANT_TARGET}
    )
    add_dependency_target_details(${BASE_DEPENDEE_TARGET} ${DEPENDANT_TARGET})
endfunction()

function(add_dependency_target_details BASE_DEPENDEE_TARGET DEPENDANT_TARGET)
    add_dependencies(${BASE_DEPENDEE_TARGET}_objlib ${DEPENDANT_TARGET})
    add_dependencies(${BASE_DEPENDEE_TARGET} ${DEPENDANT_TARGET})
    add_dependencies(${BASE_DEPENDEE_TARGET}_archive ${DEPENDANT_TARGET})
endfunction()
