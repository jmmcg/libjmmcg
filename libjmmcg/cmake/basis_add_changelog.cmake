# ============================================================================
# Copyright (c) 2011-2012 University of Pennsylvania
# Copyright (c) 2013-2014 Andreas Schuh & J.M.McGuiness
# All rights reserved.
#
# See COPYING file for license information or visit
# http://opensource.andreasschuh.com/cmake-basis/download.html#license
# ============================================================================

# ============================================================================
# change log
# From http://www.cbica.upenn.edu/sbia/software/basis/apidoc/v2.0/DocTools_8cmake_source.html
# ============================================================================

set (BASIS_SCRIPT_EXECUTE_PROCESS "${BASIS_MODULE_PATH}/ExecuteProcess.cmake")

# ----------------------------------------------------------------------------
## @brief Add target for generation of ChangeLog file.
#
# The ChangeLog is either generated from the Subversion or Git log depending
# on which revision control system is used by the project. Moreover, the
# project's source directory must be either a Subversion working copy or
# the root of a Git repository, respectively. In case of Subversion, if the
# command-line tool svn2cl(.sh) is installed, it is used to output a nicer
# formatted change log.
function (basis_add_changelog)
	option (BUILD_CHANGELOG "Request build (from the revision specified by 'START_SVN_REV') and/or installation of the ChangeLog." OFF)

	if (BUILD_CHANGELOG)
		message (STATUS "Adding ChangeLog: '${CHANGELOG_FILE}' from revision=[${START_SVN_REV}..HEAD]...")

		set (DISABLE_BUILD_CHANGELOG FALSE)

		# --------------------------------------------------------------------------
		# generate ChangeLog from Subversion history
		if (EXISTS "${PROJECT_SOURCE_DIR}/.svn")
			find_package (Subversion QUIET)
			if (Subversion_FOUND)

				message ("Generation of ChangeLog enabled as part of ALL."
							" Be aware that the ChangeLog generation from the Subversion"
							" commit history can take several minutes and may require the"
							" input of your Subversion repository credentials during the"
							" build. If you would like to build the ChangeLog separate"
							" from the rest of the software package, disable the option"
							" BUILD_CHANGELOG. You can then build the changelog target"
							" separately from ALL.")

				# using svn2cl command
				find_program (
					SVN2CL_EXECUTABLE
						NAMES svn2cl svn2cl.sh
						DOC   "The command line tool svn2cl."
				)
				mark_as_advanced (SVN2CL_EXECUTABLE)
				if (SVN2CL_EXECUTABLE)
					add_custom_command(
						OUTPUT ${CHANGELOG_FILE}
						COMMAND "${SVN2CL_EXECUTABLE}"
							"--output=${CHANGELOG_FILE}"
							"--linelen=79"
							"--reparagraph"
							"--group-by-day"
							"--include-actions"
							"--separate-daylogs"
							"--include-rev"
							"--revision=${START_SVN_REV}:HEAD"
							"--username=${CHANGELOG_USER}"
							"${PROJECT_SOURCE_DIR}"
						COMMAND "${CMAKE_COMMAND}"
							"-DCHANGELOG_FILE:FILE=${CHANGELOG_FILE}" -DINPUTFORMAT=SVN2CL
							-P "${BASIS_MODULE_PATH}/PostprocessChangeLog.cmake"
						WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
						COMMENT "Generating ChangeLog from Subversion log (using svn2cl)..."
					)
					add_custom_target(changelog ALL
						DEPENDS ${CHANGELOG_FILE}
					)
				# otherwise, use svn log output directly
				else ()
					add_custom_command(
						OUTPUT ${CHANGELOG_FILE}
						COMMAND "${CMAKE_COMMAND}"
							"-DCOMMAND=${Subversion_SVN_EXECUTABLE};log"
							"-DWORKING_DIRECTORY=${PROJECT_SOURCE_DIR}"
							"-DOUTPUT_FILE=${CHANGELOG_FILE}"
							-P "${BASIS_SCRIPT_EXECUTE_PROCESS}"
						COMMAND "${CMAKE_COMMAND}"
							"-DCHANGELOG_FILE:FILE=${CHANGELOG_FILE}" -DINPUTFORMAT=SVN
							-P "${BASIS_MODULE_PATH}/PostprocessChangeLog.cmake"
						COMMENT "Generating ChangeLog from Subversion log..."
						VERBATIM
					)
					add_custom_target(changelog ALL
						DEPENDS ${CHANGELOG_FILE}
					)
				endif ()

			else ()
				message (STATUS "Project is SVN working copy but Subversion executable was not found."
									" Generation of ChangeLog disabled.")
				set (DISABLE_BUILD_CHANGELOG TRUE)
			endif ()

		# --------------------------------------------------------------------------
		# generate ChangeLog from Git log
		elseif (EXISTS "${PROJECT_SOURCE_DIR}/.git")
			find_package (Git QUIET)
			if (GIT_FOUND)

				add_custom_command(
					OUTPUT ${CHANGELOG_FILE}
					COMMAND "${CMAKE_COMMAND}"
						"-DCOMMAND=${GIT_EXECUTABLE};log;--date-order;--date=short;--pretty=format:%ad\ \ %an%n%n%w(79,8,10)* %s%n%n%b%n"
						"-DWORKING_DIRECTORY=${PROJECT_SOURCE_DIR}"
						"-DOUTPUT_FILE=${CHANGELOG_FILE}"
						-P "${BASIS_SCRIPT_EXECUTE_PROCESS}"
					COMMAND "${CMAKE_COMMAND}"
						"-DCHANGELOG_FILE=${CHANGELOG_FILE}" -DINPUTFORMAT=GIT
						-P "${BASIS_MODULE_PATH}/PostprocessChangeLog.cmake"
					COMMENT "Generating ChangeLog from Git log..."
					VERBATIM
				)
				add_custom_target(changelog ALL
					DEPENDS ${CHANGELOG_FILE}
				)

			else ()
				message (STATUS "Project is Git repository but Git executable was not found."
									" Generation of ChangeLog disabled.")
				set (DISABLE_BUILD_CHANGELOG TRUE)
			endif ()

		# --------------------------------------------------------------------------
		# neither SVN nor Git repository
		else ()
			message (STATUS "Project source directory ${PROJECT_SOURCE_DIR} is neither"
									" SVN working copy nor Git repository. Generation of ChangeLog disabled.")
			set (DISABLE_BUILD_CHANGELOG TRUE)
		endif ()

		# --------------------------------------------------------------------------
		# disable changelog target
		if (DISABLE_BUILD_CHANGELOG)
			set (BUILD_CHANGELOG OFF CACHE INTERNAL "" FORCE)
			message (STATUS "Adding ChangeLog... - skipped")
			return ()
		endif ()

		# --------------------------------------------------------------------------
		# cleanup on "make clean"
		set_property (DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "${CHANGELOG_FILE}")

		# --------------------------------------------------------------------------
		# install ChangeLog
		get_filename_component (CHANGELOG_NAME "${CHANGELOG_FILE}" NAME)
		set (CHANGELOG_NAME "${CHANGELOG_NAME}-${PROJECT_NAME}")

		install (
			FILES       "${CHANGELOG_FILE}"
			DESTINATION "${INSTALL_DOC_DIR}"
			COMPONENT   runtime
			RENAME      "${CHANGELOG_NAME}"
		)

		message (STATUS "Adding ChangeLog... - done")
	else()
		message(STATUS "User request: not building Changelog.")
	endif()
endfunction ()
