## Copyright © 2010 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

cmake_minimum_required(VERSION 3.20 FATAL_ERROR)
#set(CMAKE_CXX_COMPILER /usr/lib/ccache/bin/clang++)
#set(CMAKE_C_COMPILER /usr/lib/ccache/bin/clang)
project(libjmmcg LANGUAGES C CXX)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})
include(CompilationOptions)
include(FetchContent)
include(GNUInstallDirs)

set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
# Note that you might need to manually delete the build dir to make these settings work correctly.
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
find_package(Threads REQUIRED)	# Requires C to be supported.
set_package_properties(Threads PROPERTIES
	DESCRIPTION "GNU Hurd/POSIX Threading Library."
	URL "https://www.gnu.org/software/hurd/libpthread.html"
)
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
find_package(Boost 1.79 REQUIRED COMPONENTS chrono filesystem log log_setup program_options system)
set_package_properties(Boost PROPERTIES
	DESCRIPTION "...one of the most highly regarded and expertly designed C++ library projects in the world."
	URL "https://www.boost.org/"
)

find_package(CURL REQUIRED MODULE)	# Requires C to be supported.
set_package_properties(CURL PROPERTIES
	DESCRIPTION "A Client that groks URLs."
	URL "https://curl.haxx.se/"
)

find_package(fmt 9.1.0 REQUIRED)
set_package_properties(fmt PROPERTIES
	DESCRIPTION "{fmt} is an open-source formatting library providing a fast and safe alternative to C stdio and C++ iostreams."
	URL "https://github.com/fmtlib/fmt"
)

find_package(PkgConfig REQUIRED)
set_package_properties(PkgConfig PROPERTIES
	DESCRIPTION "pkg-config compatible replacement with no dependencies other than C99."
	URL "https://gitea.treehouse.systems/ariadne/pkgconf"
)

find_package(OpenSSL REQUIRED COMPONENTS Crypto SSL)
set_package_properties(OpenSSL PROPERTIES
	DESCRIPTION "The OpenSSL Project develops and maintains the OpenSSL software - a robust, commercial-grade, full-featured toolkit for general-purpose cryptography and secure communication."
	URL "https://www.openssl.org/"
)

find_package(Git REQUIRED)
set_package_properties(Git PROPERTIES
	DESCRIPTION "Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency."
	URL "https://git-scm.com/"
)
include(GetGitRevisionDescription)
get_git_head_revision(GIT_HEAD_REF GIT_HEAD_HASH)
get_git_head_revision_sized(GIT_HEAD_HASH_SHORT 32)
git_describe(RAW_GIT_VERSION)
string(REGEX REPLACE "[-]" "_" GIT_VERSION_FOR_CPP "${RAW_GIT_VERSION}")

message(STATUS "Current revision is '${GIT_VERSION_FOR_CPP}', SHA1 of HEAD=0x${GIT_HEAD_HASH}")
# project version
set(${PROJECT_NAME}_MAJOR_VERSION ${GIT_VERSION_FOR_CPP})
set(VERSION_NUMBER ${${PROJECT_NAME}_MAJOR_VERSION})
set(PROJECT_VERSION ${VERSION_NUMBER})

if(NOT LIBJMMCG_GITLAB_BUILD_NUMBER)
	set(LIBJMMCG_GITLAB_BUILD_NUMBER local)
endif()

include(CMakePackageConfigHelpers)
include(ExternalProject)

set(BUILD_DOCUMENTATION OFF CACHE BOOL "Build the doxygen-based documentation producing the HTML target, then install it, as required for packaging." FORCE)
# TODO Turned off, as doxygen (1.8.4-1.8.9.1) generates a bad makefile, not texlive 2012 & 2014 related. 
set(BUILD_PDF_DOCUMENTATION OFF CACHE BOOL "Build the doxygen-based documentation producing the PDF target, then install it, as required for packaging, requires BUILD_DOCUMENTATION to be ON." FORCE)
set(BUILD_CHANGELOG OFF CACHE BOOL "Request build and/or installation of the ChangeLog." FORCE)
set(BUILD_TESTING ON CACHE BOOL "Build the unit tests (can take a long time & consumes horrendous amounts of RAM up to ~20Gb/test)." FORCE)
set(JMMCG_OPTIMISATION_WARNINGS ON CACHE BOOL "Add compilation options to advise regarding certain potential optimisations. Can produce zillions of warnings." FORCE)
set(JMMCG_PERFORMANCE_TESTS OFF CACHE BOOL "If enabled performance tests are run with suitable parameters to measure performance." FORCE)

if (JMMCG_PERFORMANCE_TESTS)
	message(STATUS "Will build the unit tests to measure performance.")
	add_definitions(-DJMMCG_PERFORMANCE_TESTS)
endif()

configure_file(dockerfile.in ${CMAKE_CURRENT_SOURCE_DIR}/dockerfile)
configure_file(README.in ${CMAKE_CURRENT_BINARY_DIR}/README)

set(BUILD_SHARED_LIBS ON CACHE BOOL "Build shared libraries" FORCE)
if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	if(JMMCG_OPTIMISATION_WARNINGS)
		add_compile_options(-Wsuggest-override -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-final-types -Wsuggest-final-methods)
	endif()
	# Yes yes some might moan on and on about these compilation options. I just say go and read "Compiler Usage Guidelines for AMD64 Platforms" v3.22. Also this demonstrates correct operation for some of the most extreme optimisation options, so one needn't fear using them.
	# " -fwhole-program" is not used because this gives linker errors to linked libraries. See https://www.boost.org/doc/libs/1_62_0/libs/test/doc/html/boost_test/section_faq.html
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ftracer -funswitch-loops -funit-at-a-time" CACHE STRING "Fearless-crack release compiler flags." FORCE)
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
# On Gentoo libcxx may need to be installed, but 3rd party libraries will need to be re-compiled.
#	find_package(LLVM REQUIRED)
#	find_package(Clang REQUIRED)
#	set(CLANG_DEFAULT_CXX_STDLIB "libc++")
#	set(LIBCXXABI_LIBCXX_PATH /usr/include/c++/v1/)
#	add_compile_options(-stdlib=libc++)
endif()
# Yes yes some might moan on and on about these compilation options. I just say go and read "Compiler Usage Guidelines for AMD64 Platforms" v3.22. Also this demonstrates correct operation for some of the most extreme optimisation options, so one needn't fear using them.
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fomit-frame-pointer -ftree-vectorize" CACHE STRING "Fearless-crack release compiler flags." FORCE)
# Let's not bother with real support: copy all dynamically-linked libraries, loader, etc into a directory & set LD_LIBRARY_FLAGS...
#set(ANCIENT_OS_SUPPORT "-Wl,--dynamic-linker=./ld-linux-x86-64.so.2 -Wl,-rpath -Wl,.")
set(LINK_FLAGS ${ANCIENT_OS_SUPPORT} "${LINK_FLAGS}" CACHE STRING "Generic linker flags." FORCE)
set(CMAKE_SHARED_LINKER_FLAGS ${LINK_FLAGS})
set(CMAKE_EXE_LINKER_FLAGS ${LINK_FLAGS})
set(NUMA_TRAITS_HPP_FILE ${CMAKE_CURRENT_BINARY_DIR}/unix/numa_traits.hpp)

# TODO find_program(CPPCHECK_BINARY NAMES cppcheck)
if(CPPCHECK_BINARY)
	set(CMAKE_CXX_CPPCHECK
		${CPPCHECK_BINARY}
		"--cppcheck-build-dir=${CMAKE_CURRENT_BINARY_DIR}/cppcheck"
		"--enable=all"
		"-j2"
		"--project=${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json"
		"--output-file=${CMAKE_CURRENT_BINARY_DIR}/cppcheck/report.xml"
		"--xml"
	)
	file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/cppcheck)
	message(STATUS "'cppcheck' was found, so will be run, with the command: \"" ${CMAKE_CXX_CPPCHECK} "\".")
else()
	message(STATUS "'cppcheck' was not found, so will not be run.")
endif()

add_subdirectory(core)
add_subdirectory(compute_mph_mask)
if(WIN32)
	add_subdirectory(experimental)
endif()
if(UNIX)
	add_subdirectory(unix)
endif()
if(BUILD_TESTING)
	message(STATUS "Will build the unit tests.")
	add_subdirectory(examples)
endif()

# See "https://www.ancient.eu/article/221/the-mesopotamian-pantheon/".
add_subdirectory(isimud)

set(INSTALL_DOC_DIR .)
set(START_GIT_REV 5393ab8b CACHE STRING "The hash of the previous release for the ChangeLog, should be updated for each release." FORCE)
if(BUILD_CHANGELOG)
	message(STATUS "Will build the changelog.")
	set(CHANGELOG_FILE "${PROJECT_BINARY_DIR}/ChangeLog")
	configure_file(generate_changelog.sh.in generate_changelog.sh)
	add_custom_target(GenerateChangeLog ALL
		COMMAND ${PROJECT_BINARY_DIR}/generate_changelog.sh
	)
	add_dependencies(jmmcg
		GenerateChangeLog
	)
endif()

if(BUILD_DOCUMENTATION)
	# "htags" is required. On Gentoo htags may be found in the package "dev-util/global".
	message(STATUS "Will build the documentation.")
	find_package(Doxygen REQUIRED dot)
	set_package_properties(Doxygen PROPERTIES
		DESCRIPTION "Documentation system for most programming languages."
		URL "http://www.doxygen.org/"
	)
	set(QUIET_DOXYGEN_BUILD "YES" CACHE STRING "Enable or disable output from the doxygen process of building the documentation (warnings & errors still emitted).")
	# From http://mementocodex.wordpress.com/2013/01/19/how-to-generate-code-documentation-with-doxygen-and-cmake-a-slightly-improved-approach/
	configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
	file(GLOB_RECURSE CORE_HEADER_FILES "core/*.hpp")
	file(GLOB_RECURSE UNIX_HEADER_FILES "unix/*.hpp")
	file(GLOB_RECURSE EXAMPLES_HEADER_FILES "examples/*.hpp")
	file(GLOB_RECURSE EXAMPLES_SOURCE_FILES "examples/*.cpp")
	file(GLOB_RECURSE EXPERIMENTAL_HEADER_FILES "experimental/*.hpp")
	file(GLOB_RECURSE ISIMUD_HEADER_FILES "isimud/exchanges/*.hpp")
	file(GLOB_RECURSE ISIMUD_SOURCE_FILES "isimud/tests/*.cpp")
	add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/docs/html/index.html
		COMMAND Doxygen::doxygen ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
		DEPENDS
			${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
			${CORE_HEADER_FILES}
			${UNIX_HEADER_FILES}
			${EXAMPLES_HEADER_FILES}
			${EXAMPLES_SOURCE_FILES}
			${ISIMUD_HEADER_FILES}
			${ISIMUD_SOURCE_FILES}
		COMMENT "Generating API documentation with ${DOXYGEN_EXECUTABLE}." 
	)
	add_custom_target(GenerateDocumentation ALL
		DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/docs/html/index.html
			${LIB_HEADER_FILES}
	)
	add_dependencies(GenerateDocumentation
		numa_traits
	)
	install(
		DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/docs/html
		DIRECTORY_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
		FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
		DESTINATION share/doc/${PROJECT_NAME}/${VERSION_NUMBER}
		COMPONENT documentation
	)
	if(BUILD_PDF_DOCUMENTATION)
		message(STATUS "Will build the PDF documentation.")
		add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/docs/latex/refman.pdf
			COMMAND ${CMAKE_BUILD_TOOL}
			DEPENDS
				GenerateDocumentation
				${CMAKE_CURRENT_BINARY_DIR}/docs/latex/refman.tex
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/docs/latex
			COMMENT "Generating PDF of the API documentation." 
		)
		add_custom_target(GeneratePDF ALL
			DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/docs/latex/refman.pdf
		)
		install(
			FILES ${CMAKE_CURRENT_BINARY_DIR}/docs/latex/refman.pdf
			DESTINATION share/doc/${PROJECT_NAME}/${VERSION_NUMBER}
			COMPONENT documentation
			PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
			RENAME manual.pdf
		)
	endif()
endif()

set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/dist CACHE PATH "The directory into which the distribution is created.")

install(
	DIRECTORY ${SRC_MSWIN}
	DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}
	COMPONENT development
	DIRECTORY_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
	FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
	FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp" PATTERN "*.xdr" PATTERN "*.xml"
)
install(
	EXPORT ${PROJECT_NAME}Targets
	NAMESPACE ${PROJECT_NAME}::
	DESTINATION share/cmake/Modules COMPONENT development
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)
install(
	FILES AUTHORS COPYING dockerfile LICENSE.txt ${CMAKE_CURRENT_BINARY_DIR}/README WARRANTY
	DESTINATION share/doc/${PROJECT_NAME}/${VERSION_NUMBER}
	COMPONENT documentation
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)

set(${PROJECT_NAME}_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/${PROJECT_NAME}/${VERSION_NUMBER}/include)
set(${PROJECT_NAME}_LIBRARIES "${${PROJECT_NAME}_LIBRARIES} jmmcg jmmcg_archive jmmcg_unix jmmcg_unix_archive")

write_basic_package_version_file(
	${PROJECT_NAME}ConfigVersion.cmake
	COMPATIBILITY AnyNewerVersion
)

export(
	EXPORT ${PROJECT_NAME}Targets
	FILE ${CMAKE_CURRENT_BINARY_DIR}/share/cmake/Modules/${PROJECT_NAME}Targets.cmake
	NAMESPACE ${PROJECT_NAME}::
)

configure_package_config_file(
	${PROJECT_NAME}Config.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/share/cmake/Modules/${PROJECT_NAME}Config.cmake
	INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/share/cmake/Modules
)

write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/share/cmake/Modules/${PROJECT_NAME}ConfigVersion.cmake
	COMPATIBILITY SameMajorVersion
)

install(
	FILES
		${CMAKE_CURRENT_BINARY_DIR}/share/cmake/Modules/${PROJECT_NAME}Config.cmake
		${CMAKE_CURRENT_BINARY_DIR}/share/cmake/Modules/${PROJECT_NAME}ConfigVersion.cmake
		${CMAKE_CURRENT_SOURCE_DIR}/cmake/GetGitRevisionDescription.cmake
	DESTINATION share/cmake/Modules COMPONENT development
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)

# On Gentoo this is in the package "app-arch/rpm".
find_program(RPM_BUILD rpmbuild REQUIRED)
set(CPACK_PACKAGE_FILE_NAME ${PROJECT_NAME}-${VERSION_NUMBER}-${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}-${CMAKE_BUILD_TYPE})
set(CPACK_PACKAGE_VERSION_MAJOR ${VERSION_NUMBER})
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_BINARY_DIR}/README" CACHE FILEPATH "The description file.")
set(CPACK_PACKAGE_CHECKSUM SHA256)	# Compatible with "conan.io"...
set(CPACK_RESOURCE_FILE_COPYING "${CMAKE_CURRENT_SOURCE_DIR}/COPYING" CACHE FILEPATH "The license file.")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt" CACHE FILEPATH "The license under which the software is released.")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_BINARY_DIR}/README" CACHE FILEPATH "The readme.")
set(CPACK_RESOURCE_FILE_VENDOR "${CMAKE_CURRENT_SOURCE_DIR}/AUTHORS" CACHE FILEPATH "The authors of the project.")
set(CPACK_RESOURCE_FILE_WELCOME "${CMAKE_CURRENT_SOURCE_DIR}/WARRANTY" CACHE FILEPATH "The warranty under which the package is distributed.")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/isimud/libjmmcg/")
set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE ${CMAKE_SYSTEM_PROCESSOR})
set(CPACK_PACKAGE_CONTACT "isimud@hussar.me.uk")
set(CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS ON)
set(CPACK_DEBIAN_PACKAGE_DEPENDS "bash, ca-certificates, coreutils, cron, curl (>= ${CURL_VERSION_STRING}), exim4, exim4-daemon-light, gkrellmd, gkrelltopd, irqbalance, libboost (>= ${Boost_VERSION}), libboost-log(>= ${Boost_VERSION}), libboost-program-options (>= ${Boost_VERSION}), libboost-system (>= ${Boost_VERSION}), libfmt9 (>= ${fmt_VERSION}), libssl3 (>= ${OPENSSL_VERSION}), locales, logrotate, numactl, procps, psmisc, systemd-timesyncd")
set(CPACK_DEBIAN_COMPRESSION_TYPE lzma)
set(CPACK_RPM_PACKAGE_ARCHITECTURE ${CMAKE_SYSTEM_PROCESSOR})
set(CPACK_RPM_PACKAGE_LICENSE GPLV2)
set(CPACK_RPM_PACKAGE_REQUIRES "bash, boost>=${Boost_VERSION}, boost-log>=${Boost_VERSION}, boost-program-options>=${Boost_VERSION}, boost-system>=${Boost_VERSION}, ca-certificates, chrony, coreutils, curl>=${CURL_VERSION_STRING}, default-mta, gkrellmd, gkrelltopd, fmt9>=${fmt_VERSION}, irqbalance, locales, logrotate, numactl, procps, psmisc, ssl3>=${OPENSSL_VERSION}")
set(CPACK_RPM_PACKAGE_RELOCATABLE YES)
set(CPACK_RPM_COMPRESSION_TYPE lzma)
if(BUILD_CHANGELOG)
	set(CPACK_RPM_CHANGELOG_FILE ${CHANGELOG_FILE})
endif()
set(CPACK_DEBIAN_RUNTIME_PACKAGE_CONTROL_EXTRA ${CMAKE_CURRENT_BINARY_DIR}/isimud/scripts/postinst)
set(CPACK_GENERATOR "TXZ;7Z;DEB;RPM")
include(CPack)
cpack_add_component(development)
cpack_add_component(documentation)
cpack_add_component(runtime)

feature_summary(WHAT ALL
	QUIET_ON_EMPTY
)
