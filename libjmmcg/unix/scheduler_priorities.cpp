/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/application.hpp"
#include "../core/basic_exception.hpp"
#include "../core/stream_header_guard.hpp"

#include <boost/core/verbose_terminate_handler.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <fstream>

#include <sys/resource.h>
#include <sys/time.h>

/**
	\todo This cannot be set unless CAP_SYS_RESOURCE is applied.

		const int schedule_policy=SCHED_FIFO;

	This may be done thus:

		sudo setcap 'CAP_SYS_RESOURCE=+ep' /path/to/executable

	If SCHED_OTHER is used then basically one cannot set the priority, as the min & max are both set to zero.
*/
const int schedule_policy= SCHED_OTHER;

using namespace libjmmcg;

std::pair<int, int>
get_sched_min_max() noexcept(false) {
	rlimit lim{};
	int err= ::getrlimit(RLIMIT_NICE, &lim);
	if(err == -1) {
		BOOST_THROW_EXCEPTION(libjmmcg::throw_crt_exception<std::runtime_error>("Failed to get the rlimit of the priorities.", "getrlimit", &get_sched_min_max));
	}
	lim.rlim_cur= lim.rlim_max;
	err= ::setrlimit(RLIMIT_NICE, &lim);
	if(err == -1) {
		BOOST_THROW_EXCEPTION(libjmmcg::throw_crt_exception<std::runtime_error>("Failed to update the rlimit of the priorities.", "setrlimit", &get_sched_min_max));
	}
	const int max_priority= ::sched_get_priority_max(schedule_policy);
	const int min_priority= ::sched_get_priority_min(schedule_policy);
	if((max_priority != -1) && (min_priority != -1)) {
		return std::make_pair(min_priority, max_priority);
	} else {
		BOOST_THROW_EXCEPTION(libjmmcg::throw_crt_exception<std::runtime_error>("Failed to get the max & min priorities of the scheduler.", "sched_get_priority_min", &get_sched_min_max));
	}
}

/// Discover the minimum and maximum scheduler-priorities on the current system.
int
main(int argc, char const* const* argv) noexcept(true) {
	struct header_guard_t : libjmmcg::header_guard_t {
		header_guard_t(std::string const& exe, std::string const& fname, std::ostream& os) noexcept(false)
			: libjmmcg::header_guard_t(exe, "UNIX", fname, os) {
			os
				<< "/**\n"
					"\t\\file The constants created in this file are used in api_threading_traits::set_kernel_priority().\n"
					"*/\n"
					"namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {\n";
		}
		~header_guard_t() noexcept(false) {
			os << "} } } }\n";
		}
	};

	try {
		std::set_terminate(boost::core::verbose_terminate_handler);
		boost::program_options::options_description general(
			application::make_program_options(
				"A program to discover the minimum and maximum scheduler-priorities on the current system and put them into a C++ header-file." + application::make_help_message()));
		boost::program_options::options_description prog_opts("Program options.");
		prog_opts.add_options()("cpp_header", boost::program_options::value<std::string>()->required(), "The file-path of the output C++ header-file.");
		boost::program_options::options_description all("All options.");
		all.add(general).add(prog_opts);
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(auto const exit_code= application::check_basic_options(vm, all, std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		}
		boost::program_options::notify(vm);

		std::ofstream cpp_header(vm["cpp_header"].as<std::string>().c_str());
		const header_guard_t header_guard(argv[0], vm["cpp_header"].as<std::string>(), cpp_header);
		auto const& sched_min_max= get_sched_min_max();
		cpp_header
			<< "\nenum scheduler_policy : int {\n"
			<< "\tschedule_policy=" << schedule_policy << "\n"
																		 "};\n"
																		 "\nenum scheduler_min_max_priorities : int {\n"
																		 "\tscheduler_min_priority="
			<< sched_min_max.first << ",\n"
											  "\tscheduler_max_priority="
			<< sched_min_max.second << "\n"
												"};\n\n";

		return exit_codes::codes::exit_success;
	} catch(std::exception const& ex) {
		std::cerr << "STL-derived exception. Details: " << boost::diagnostic_information(ex) << std::endl;
		return exit_codes::codes::exit_stl_exception;
	} catch(...) {
		std::cerr << "Unknown exception. Details: " << boost::current_exception_diagnostic_information() << std::endl;
		return exit_codes::codes::exit_unknown_exception;
	}
	return exit_codes::codes::exit_unknown_failure;
}
