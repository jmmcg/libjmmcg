/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/deleter.hpp"

#include <csignal>

#include <cxxabi.h>
#include <execinfo.h>

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {
namespace ppd {

// Implementation details..... Don't look below here!!! ;)

//	"api_threading_traits" implementation...

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::pid_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_current_process_id() noexcept(true) {
	return ::getpid();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::tid_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_current_thread_id() noexcept(true) {
	return ::pthread_self();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::handle_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_current_thread() noexcept(true) {
	return ::pthread_self();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::username_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_current_username() noexcept(true) {
	api_params_type::username_type uname{};
	::cuserid(uname.data());
	*uname.rbegin()= '\0';
	return uname;
}

template<>
void
api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_backtrace_on_signal() noexcept(true);

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_name(typename api_params_type::handle_type, thread_name_t const&) noexcept(false) {
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::thread_name_t __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_name(typename api_params_type::handle_type) noexcept(false) {
	return thread_name_t{};
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::raise(int sig) noexcept(true) {
	[[maybe_unused]] auto const ret= ::raise(sig);
	DEBUG_ASSERT(ret == 0);
}

/// Run the work only once if it returns at all, unlike the multi-threaded variant.
/**
	If the worker_fn() returns true then it will run once, then exit, otherwise it will depend upon the return value for pre_exit(). If this always returns false, then the system will enter an infinite loop. By default pre_exit() for sequential mode returns false.

	\see worker_fn(), process()
*/
template<>
inline bool __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::create(const api_params_type::creation_flags creflag, api_params_type& parms) noexcept(true) {
	parms.id= get_current_thread_id();
	DEBUG_ASSERT(parms.work_fn);
	if(creflag == api_params_type::create_running) {
		(*parms.work_fn)(parms.arglist);
	}
	return true;
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::suspend_count __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::suspend(api_params_type const&) noexcept(true) {
	return static_cast<api_params_type::suspend_count>(0);
}

/// This does not run the work at all, unlike the multi-threaded variant.
template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::suspend_count __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::resume(api_params_type const& parms) noexcept(true) {
	(*parms.work_fn)(parms.arglist);
	return static_cast<api_params_type::suspend_count>(0);
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::states::element_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::state(api_params_type&) noexcept(true) {
	return api_params_type::states::no_kernel_thread;
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::exit(api_params_type::states::element_type&) noexcept(true) {
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::terminate(api_params_type::handle_type) noexcept(false) {
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::cleanup(api_params_type::handle_type) noexcept(false) {
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_kernel_priority(api_params_type::handle_type, const api_params_type::priority_type) noexcept(false) {
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_kernel_affinity(typename api_params_type::handle_type const, const api_params_type::processor_mask_type) noexcept(false) {
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::processor_mask_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_kernel_affinity(typename api_params_type::handle_type const) noexcept(false) {
	return api_params_type::processor_mask_type();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::priority_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_kernel_priority(api_params_type::handle_type) noexcept(false) {
	return api_params_type::normal;
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_cancelstate(api_params_type::thread_cancel_state::element_type) noexcept(false) {
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::sleep(const api_params_type::suspend_period_ms) noexcept(false) {
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::api_params_type::states::element_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::wait_thread_exit(api_params_type&, const lock_traits::timeout_type) noexcept(false) {
	return api_params_type::states::no_kernel_thread;
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::raise(int sig) noexcept(true) {
	api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::raise(sig);
}

template<>
inline bool
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::create(const api_params_type::creation_flags, api_params_type& cp) noexcept(true) {
	DEBUG_ASSERT(cp.work_fn);
	DEBUG_ASSERT(cp.arglist);
	const int ret= ::pthread_create(&cp.id, static_cast<const pthread_attr_t*>(cp.initflag), cp.work_fn, cp.arglist);
	DEBUG_ASSERT(ret == 0 && cp.id != 0);
	return !static_cast<bool>(ret);
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::suspend_count
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::suspend(api_params_type const&) noexcept(true) {
	// TODO		::pthread_kill(params.id, SIGTHSTOP);
	return static_cast<api_params_type::suspend_count>(0);
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::suspend_count
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::resume(api_params_type const&) noexcept(true) {
	// TODO		::pthread_kill(params.id, SIGTHCONT);
	return static_cast<api_params_type::suspend_count>(0);
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::states::element_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::state(api_params_type& thread) noexcept(true) {
	if(thread.state != api_params_type::states::no_kernel_thread) {
		api_params_type::states::element_type exit_code= api_params_type::states::unknown;
		const int ret= ::pthread_tryjoin_np(thread.id, reinterpret_cast<void**>(&exit_code));
		thread.state= exit_code;
		switch(ret) {
		case 0:
			thread.id= 0;
			if(!thread.state) {
				thread.state= api_params_type::states::no_kernel_thread;
			} else if(thread.state == static_cast<api_params_type::states::element_type>(reinterpret_cast<api_params_type::states::underlying_type_t>(PTHREAD_CANCELED))) {
				thread.state= api_params_type::states::cancelled;
			}
			break;
		case EDEADLK:
			thread.state= api_params_type::states::deadlocked_with_another_thread;
			break;
		case EINVAL:
			thread.state= api_params_type::states::invalid_thread;
			break;
		case ESRCH:
			thread.state= api_params_type::states::thread_id_not_found;
			break;
		case EBUSY:
			thread.state= api_params_type::states::active;
			break;
		default:
			thread.state= api_params_type::states::get_exit_code_failure;
		}
		return thread.state;
	} else {
		return thread.state;
	}
}

template<>
inline bool __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::is_running(api_params_type& thread) noexcept(true) {
	api_params_type::states::element_type const st= state(thread);
	return st == api_params_type::states::active || st == api_params_type::states::deadlocked_with_another_thread;
}

template<>
inline void
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::exit(api_params_type::states::element_type& exit_code) noexcept(true) {
	::pthread_exit(static_cast<void*>(&exit_code));
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_cancelstate(api_params_type::thread_cancel_state::element_type state) noexcept(false) {
	int old_state;
	const int pth_err= ::pthread_setcancelstate(state, &old_state);
	if(pth_err) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not set the cancel state of the current thread. thread_cancel_state={}", api_params_type::thread_cancel_state::to_string(state)), "pthread_setcancelstate", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_cancelstate, pth_err));
	}
}

template<>
inline void
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::terminate(api_params_type::handle_type thread) noexcept(false) {
	DEBUG_ASSERT(thread);
	try {
		set_cancelstate(api_params_type::thread_cancel_state::cancel_enable);
	} catch(...) {
		// We don't care about an error as we cannot do anything about it.
	}
	const int pth_err= ::pthread_cancel(thread);
	if(pth_err != EXIT_SUCCESS && pth_err != ESRCH) {
		DEBUG_ASSERT(pth_err);
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not send a cancel request to the thread. thread={}", thread), "pthread_cancel", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::terminate, pth_err));
	}
}

template<>
inline void
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::cleanup(api_params_type::handle_type thread) noexcept(false) {
	if(thread) {
		void* exit_code_p= reinterpret_cast<void*>(api_params_type::states::unknown);
		const int pth_err= ::pthread_join(thread, &exit_code_p);
		const auto exit_code= static_cast<api_params_type::states::element_type>(reinterpret_cast<api_params_type::states::underlying_type_t>(exit_code_p));
		if(pth_err) {
			BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not join with the requested thread. thread={}, return code from thread={}", thread, api_params_type::states::to_string(exit_code)), "pthread_join", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::terminate, pth_err));
		}
	}
}

template<>
inline void
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_kernel_priority(api_params_type::handle_type, const api_params_type::priority_type priority) noexcept(false) {
	// Note that this affects the entire process & all threads within it.
	sched_param sp;
	sp.sched_priority= api_params_type::priority_type::normal;
	const int schedule_policy= ::pthread_setschedparam(::pthread_self(), private_::scheduler_policy::schedule_policy, &sp);
	if(schedule_policy != -1) {
		const int checked_priority= (std::min(std::max(static_cast<int>(priority), static_cast<int>(api_params_type::priority_type::idle)), static_cast<int>(api_params_type::priority_type::time_critical)));
		const int err= ::pthread_setschedprio(pthread_self(), checked_priority);
		if(!err) {
			return;
		} else {
			BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Failed to set the new priority. thread={}, priority={}", ::pthread_self(), 0), "pthread_setschedprio", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_kernel_priority));
		}
	} else {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Failed to set the type of scheduler to use. thread={}, schedule_policy={}", ::pthread_self(), SCHED_RR), "pthread_setschedparam", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_kernel_priority));
	}
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::priority_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_kernel_priority(api_params_type::handle_type thread) noexcept(false) {
	sched_param param;
	const int err= ::sched_getparam(0, &param);
	if(err != -1) {
		return static_cast<api_params_type::priority_type>(param.sched_priority);
	} else {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not get the kernel priority of the specified thread. thread={}", thread), "sched_getparam", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_kernel_priority));
	}
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::pid_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_current_process_id() noexcept(true) {
	return ::getpid();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::tid_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_current_thread_id() noexcept(true) {
	return ::pthread_self();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::handle_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_current_thread() noexcept(true) {
	return ::pthread_self();
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::username_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_current_username() noexcept(true) {
	return api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::get_current_username();
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_kernel_affinity(typename api_params_type::handle_type const thread_id, api_params_type::processor_mask_type const mask) noexcept(false) {
	const int pth_err= ::pthread_setaffinity_np(thread_id, sizeof(api_params_type::processor_mask_type), &mask.mask());
	if(pth_err) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not set the kernel affinity of the specified thread. thread_id={}, processor_mask={}", thread_id, tostring(mask)), "pthread_setaffinity_np", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_kernel_affinity, pth_err));
	}
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::processor_mask_type __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_kernel_affinity(typename api_params_type::handle_type const thread_id) noexcept(false) {
	api_params_type::processor_mask_type mask;
	const int pth_err= ::pthread_getaffinity_np(thread_id, sizeof(api_params_type::processor_mask_type), &mask.mask());
	if(pth_err) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not get the kernel affinity of the specified thread. thread_id={}", thread_id), "pthread_getaffinity_np", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_kernel_affinity, pth_err));
	}
	return mask;
}

template<>
inline void
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::sleep(const api_params_type::suspend_period_ms period) noexcept(false) {
	if(period) {
		DEBUG_ASSERT(period <= std::numeric_limits<api_params_type::suspend_period_ms>::max() / 1000);
		const timespec period_ns= {
			static_cast<long>(period / 1000),
			static_cast<long>((period % 1000) * 1000000)};
		timespec requested;
		const int err= ::nanosleep(&period_ns, &requested);
		if(err) {
			BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not suspend for the requested period. suspend_period_ms={}", period), "nanosleep", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::sleep));
		}
	} else {
		const int err= ::sched_yield();
		if(err) {
			BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>("Period of zero implies a yield, which failed.", "sched_yield", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::sleep));
		}
	}
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::api_params_type::states::element_type
api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::wait_thread_exit(api_params_type& thread, const lock_traits::timeout_type) noexcept(false) {
	if(thread.id) {
		api_params_type::states::element_type exit_code= api_params_type::states::unknown;
		DEBUG_ASSERT(thread.detach_state() == api_params_type::detach_state_type::joinable);
		// TODO should use "pthread_timedjoin_np()", but it uses the very irritating Epoch for the abstimeout.
		const int ret= ::pthread_join(thread.id, reinterpret_cast<void**>(&exit_code));
		thread.state= exit_code;
		switch(ret) {
		case 0:
			thread.id= 0;
			if(!thread.state) {
				thread.state= api_params_type::states::no_kernel_thread;
			}
			break;
		case EDEADLK:
			thread.state= api_params_type::states::deadlocked_with_another_thread;
			break;
		case EINVAL:
			thread.id= 0;
			thread.state= api_params_type::states::invalid_thread;
			break;
		case ESRCH:
			thread.id= 0;
			thread.state= api_params_type::states::thread_id_not_found;
			break;
		case EBUSY:
			thread.state= api_params_type::states::active;
			break;
		default:
			thread.state= api_params_type::states::get_exit_code_failure;
		}
		return thread.state;
	} else {
		return thread.state;
	}
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_backtrace_on_signal() noexcept(true) {
	api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_backtrace_on_signal();
}

template<>
inline void __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_name(typename api_params_type::handle_type thread, thread_name_t const& name) noexcept(false) {
	const_cast<thread_name_t&>(name)[name.size() - 1]= '\0';
	DEBUG_ASSERT(std::strlen(name.begin()) <= name.size());
	const int pth_err= ::pthread_setname_np(thread, name.begin());
	if(pth_err) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not set the name of the specified thread. thread={}, thread_name='{}', max_thread_name_length={}", thread, std::string_view(name.begin(), std::find(name.begin(), name.end(), '\0')), max_thread_name_length), "pthread_setname_np", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_name, pth_err));
	}
}

template<>
inline api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::thread_name_t __fastcall api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::get_name(typename api_params_type::handle_type thread) noexcept(false) {
	thread_name_t thread_name{};
	const int pth_err= ::pthread_getname_np(thread, thread_name.data(), max_thread_name_length);
	*thread_name.rbegin()= '\0';
	if(pth_err) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not get the name of the specified thread. thread={}, max_thread_name_length={}", thread, max_thread_name_length), "pthread_getname_np", &api_threading_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::set_name, pth_err));
	} else {
		return thread_name;
	}
}
}
}
}
