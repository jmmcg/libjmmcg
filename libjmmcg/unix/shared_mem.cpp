/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/shared_mem.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class>
struct failure_code_mmap {
	static inline constexpr const std::int64_t value= -1;
};

void*
shared_mem::allocator_type::do_allocate(size_type bytes, [[maybe_unused]] std::size_t alignment) noexcept(false) {
	if(bytes <= shm_.capacity_) {
		DEBUG_ASSERT((alignment == 0) || ((reinterpret_cast<size_type>(shm_.mmap_manager.mmapped_buffer_) % alignment) == 0));
		return reinterpret_cast<void*>(shm_.mmap_manager.mmapped_buffer_);
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("The number of bytes to allocate must be smaller than that of the size of the shared-memory segment. Requested size={}, alignment={}, pointer to mapped segment for allocation from={}, capacity of mapped segment={}", bytes, alignment, reinterpret_cast<void const*>(shm_.mmap_manager.mmapped_buffer_), shm_.capacity_), &shared_mem::allocator_type::do_allocate));
	}
}

void
shared_mem::allocator_type::do_deallocate(void* p, [[maybe_unused]] size_type bytes, [[maybe_unused]] std::size_t alignment) noexcept(true) {
	if(p != nullptr) {
		DEBUG_ASSERT(bytes <= shm_.capacity_ && ((alignment == 0) || ((reinterpret_cast<std::size_t>(p) % alignment) == 0)));
	}
}

bool
shared_mem::allocator_type::do_is_equal(std::pmr::memory_resource const& other) const noexcept(true) {
	return this == &other;
}

shared_mem::handle_t
shared_mem::open_fd(name_type const& name, oflags_type oflags, mode_type::element_type mode) noexcept(false) {
	if(name.size() <= shm_internal_name_extra_size || name.size() > shm_name_size) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Shared-memory segment must have a distinguishable name that is >shm_internal_name_extra_size && <shm_name_size. oflags={}, mode={}, name.size()={}, name='{}', shm_internal_name_extra_size={}, shm_name_size={}", oflags_t::to_string(static_cast<oflags_t::element_type>(oflags)), mode_type::to_string(mode), name.size(), name, shm_internal_name_extra_size, shm_name_size), &shared_mem::open_fd));
	} else {
		return JMMCG_SYSCALL_WRAPPER(fmt::format("Failed to open shared-memory segment. oflags={}, mode={}", oflags_t::to_string(static_cast<oflags_t::element_type>(oflags)), mode_type::to_string(mode)), ::shm_open, name.data(), oflags, mode);
	}
}

shared_mem::pointer
shared_mem::map_fd(handle_t fd, size_type size, protection_type::element_type prot) noexcept(false) {
	if(size <= min_size) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Shared-memory segment must have a size greater than min_size. Min_size={}, size={}, protection_type={}", min_size, size, protection_type::to_string(prot)), &shared_mem::map_fd));
	} else {
		DEBUG_ASSERT(fd > 0);
		pointer const addr= reinterpret_cast<pointer>(JMMCG_SYSCALL_WRAPPER_FC(failure_code_mmap, fmt::format("Failed to map the shared-memory segment. protection_type={}", protection_type::to_string(prot)), ::mmap, nullptr, size, prot, MAP_SHARED_VALIDATE | MAP_LOCKED, fd, 0));
		DEBUG_ASSERT((reinterpret_cast<std::size_t>(addr) % sizeof(double)) == 0);
		return addr;
	}
}

shared_mem::shared_mem(name_type const& name, size_type size, boost::interprocess::ipcdetail::OS_process_id_t pid, oflags_type oflags, mode_type::element_type mode, protection_type::element_type prot) noexcept(false)
	: name_(name_prefix + name + pid_separator + to_string(pid)), capacity_(size + min_size), fd_manager(open_fd(name_, oflags, mode), name_), mmap_manager(map_fd(fd_manager.fd_, capacity_, prot), capacity_), allocator_(*this) {
	if(size >= max_size) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Shared-memory segment must have a size less than max_size. Size={}, pid={}, oflags={}, mode={}, protection_type={}, max_size={}", size, pid, oflags_t::to_string(static_cast<oflags_t::element_type>(oflags)), mode_type::to_string(mode), protection_type::to_string(prot), max_size), &shared_mem::map_fd));
	} else {
		JMMCG_SYSCALL_WRAPPER("Failed to resize the shared-memory segment to the requested size.", ::ftruncate, fd_manager.fd_, capacity_);
	}
}

shared_mem::~shared_mem() noexcept(true) try {
} catch(...) {
	LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Exception caught and ignored: " << boost::current_exception_diagnostic_information();
}

void
shared_mem::sync(pointer addr, size_type size, sync_flags_t::element_type sync_flags) noexcept(false) {
	if(size > this->size()) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Requested size larger than configured size. Requested size={}, sync_flags={}, size={}", size, sync_flags_t::to_string(sync_flags), this->size()), &shared_mem::sync<pointer>));
	}
	if(addr < data()) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Requested address less than mapped address. Requested size={}, sync_flags={}, size={}, requested pointer addr={}, mapped address={}", size, sync_flags_t::to_string(sync_flags), this->size(), reinterpret_cast<void const*>(addr), reinterpret_cast<void const*>(data())), &shared_mem::sync<pointer>));
	}
	if((addr + size) > (data() + this->size())) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Requested address plus length greater than mapped buffer. Requested size={}, sync_flags={}, size={}, requested pointer addr={}, mapped address={}", size, sync_flags_t::to_string(sync_flags), this->size(), reinterpret_cast<void const*>(addr), reinterpret_cast<void const*>(data())), &shared_mem::sync<pointer>));
	}
	JMMCG_SYSCALL_WRAPPER(fmt::format("Unable sync the portion of the mapped memory, sync_flags='{}'", sync_flags_t::to_string(sync_flags)), ::msync, mmap_manager.mmapped_buffer_, capacity_, sync_flags);
}

std::string
shared_mem::to_string(boost::interprocess::ipcdetail::OS_process_id_t pid) noexcept(false) {
	std::ostringstream os;
	os << std::setw(std::numeric_limits<boost::interprocess::ipcdetail::OS_process_id_t>::digits10) << std::setfill(pid_padding) << pid;
	return os.str();
}

}}
