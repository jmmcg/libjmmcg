/******************************************************************************
** Copyright © 2008 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <sys/time.h>
#include <iomanip>
#include <iostream>

/// Link with -lrt to include these functions.
#if _POSIX_C_SOURCE < 199309L
#	error "POSIX clock_*()s required for this library."
#endif

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<typename Mdl>
class hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl> {
public:
	typedef ppd::api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, Mdl> lock_traits;
	/// The units are microseconds.
	typedef unsigned long long value_type;
	typedef timespec time_utc_t;

	/**
		Time when the process started up.
	*/
	const time_utc_t start_up_time;

	/**
		Count in usec when the process started up.
	*/
	const value_type start_up_count;	  ///< In usec.

	__stdcall hp_timer() noexcept(false);

	/// Return the current time in a resolution of 1/frequency units.
	/**
		Note that this will have a systematic offset according to the properties of GetSystemTimeAsFileTime().

		\return Time (NOT local time!) in UTC.
	*/
	const time_utc_t __fastcall current_time() const noexcept(false);

	/// Convert a value_type in units of 1/frequency to usec.
	static value_type __fastcall to_usec(const time_utc_t ticks) noexcept(true);

	/// The current count in units of 1/frequency.
	static value_type __fastcall current_count() noexcept(false);

	/**
		Implemented using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend std::ostream& __fastcall operator<<(std::ostream& o, const time_utc_t& t);

private:
	static const time_utc_t __fastcall get_start_time() noexcept(false);
};

inline tostream& __fastcall
operator<<(tostream& ss, const timespec& st) {
	std::ios_base::iostate err= std::ios_base::goodbit;
	try {
		const tostream::sentry opfx(ss);
		if(opfx) {
			ss << st.tv_sec << _T(".") << std::setfill('0') << std::setw(std::numeric_limits<decltype(st.tv_nsec)>::digits10)
				<< st.tv_nsec;
		}
	} catch(std::bad_alloc const&) {
		err|= std::ios_base::badbit;
		const std::ios_base::iostate exception_mask= ss.exceptions();
		if((exception_mask & std::ios_base::failbit)
			&& !(exception_mask & std::ios_base::badbit)) {
			ss.setstate(err);
		} else if(exception_mask & std::ios_base::badbit) {
			try {
				ss.setstate(err);
			} catch(std::ios_base::failure const&) {
			}
			throw;
		}
	} catch(...) {
		err|= std::ios_base::failbit;
		const std::ios_base::iostate exception_mask= ss.exceptions();
		if((exception_mask & std::ios_base::badbit)
			&& (err & std::ios_base::badbit)) {
			ss.setstate(err);
		} else if(exception_mask & std::ios_base::failbit) {
			try {
				ss.setstate(err);
			} catch(std::ios_base::failure const&) {
			}
			throw;
		}
	}
	if(err)
		ss.setstate(err);
	return ss;
}

}}

#include "hp_timer_impl.hpp"
