/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/***************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2016, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at https://curl.haxx.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ***************************************************************************/

#include "curl.hpp"

#include "../core/basic_exception.hpp"
#include "../core/uuid.hpp"

#include <boost/exception/diagnostic_information.hpp>
#include <boost/lexical_cast.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <iomanip>
#include <sstream>
#include <stdexcept>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

curl::payload_message_t curl::payload_text_strs_{};

curl::email_recipients::~email_recipients() noexcept(true) {
	::curl_slist_free_all(recipients_);
}

void
curl::email_recipients::push_back(std::string const& addr) noexcept(true) {
	addrs_.push_back(addr);
	recipients_= ::curl_slist_append(recipients_, addrs_.rbegin()->c_str());
}

[[gnu::pure]] bool
curl::email_recipients::empty() const noexcept(true) {
	return addrs_.empty();
}

curl::curl(std::string const& smtp_url, unsigned short port, std::string const& username, std::string const& password, bool enable_logging, bool enable_ssl_verification) noexcept(false)
	: handle_(::curl_easy_init()), smtp_url_(smtp_url + ":" + boost::lexical_cast<std::string>(port)), username_(username), password_(password) {
	if(!handle_) {
		BOOST_THROW_EXCEPTION(throw_exception<std::runtime_error>("Failed to initialise curl library. Try running the program again, or rebooting...", this));
	}
	::curl_easy_setopt(handle_, CURLOPT_USERNAME, username_.c_str());
	::curl_easy_setopt(handle_, CURLOPT_PASSWORD, password_.c_str());
	::curl_easy_setopt(handle_, CURLOPT_URL, smtp_url_.c_str());

	/* In this example, we'll start with a plain text connection, and upgrade
	 * to Transport Layer Security (TLS) using the STARTTLS command. Be careful
	 * of using CURLUSESSL_TRY here, because if TLS upgrade fails, the transfer
	 * will continue anyway - see the security discussion in the libcurl
	 * tutorial for more details. */
	::curl_easy_setopt(handle_, CURLOPT_USE_SSL, static_cast<long>(CURLUSESSL_ALL));
	if(enable_logging) {
		set_logging();
	}
	if(!enable_ssl_verification) {
		ignore_ssl_verification();
	}
}

curl::~curl() noexcept(true) {
	::curl_easy_cleanup(handle_);
}

void
curl::ignore_ssl_verification() noexcept(true) {
	::curl_easy_setopt(handle_, CURLOPT_SSL_VERIFYPEER, 0L);
	::curl_easy_setopt(handle_, CURLOPT_SSL_VERIFYHOST, 0L);
}

void
curl::set_logging() noexcept(true) {
	::curl_easy_setopt(handle_, CURLOPT_VERBOSE, 1L);
}

void
curl::from(std::string const& from) noexcept(true) {
	from_= from;
	::curl_easy_setopt(handle_, CURLOPT_MAIL_FROM, from_.c_str());
}

size_t
curl::payload_source(void* ptr, size_t size, size_t nmemb, void* userp) {
	upload_status* upload_ctx= reinterpret_cast<upload_status*>(userp);
	DEBUG_ASSERT(upload_ctx);

	if((size == 0) || (nmemb == 0) || ((size * nmemb) < 1)) {
		return 0;
	}
	if(upload_ctx->lines_read < payload_text_strs_.size()) {
		payload_message_t::value_type const& data= payload_text_strs_[upload_ctx->lines_read];
		if(!data.empty()) {
			std::memcpy(ptr, data.data(), data.size());
			++upload_ctx->lines_read;
			return data.size();
		}
	}
	return 0;
}

curl::failures_t
curl::send(std::string const& subject, std::string const& body, recipients_t const& recipients) noexcept(false) {
	failures_t failures;
	for(auto const& recipient: recipients) {
		try {
			create_message(recipient, subject, body);
			upload_ctx_.lines_read= 0;
			email_recipients a_recipient;
			a_recipient.push_back(recipient);
			DEBUG_ASSERT(a_recipient.recipients_);
			::curl_easy_setopt(handle_, CURLOPT_MAIL_RCPT, a_recipient.recipients_);
			CURLcode res= ::curl_easy_perform(handle_);
			if(res == CURLE_OK) {
				::curl_easy_setopt(handle_, CURLOPT_READFUNCTION, payload_source);
				::curl_easy_setopt(handle_, CURLOPT_READDATA, &upload_ctx_);
				::curl_easy_setopt(handle_, CURLOPT_UPLOAD, 1L);
				res= ::curl_easy_perform(handle_);
				if(res != CURLE_OK) {
					failures.emplace_back(std::make_tuple(recipient, ::curl_easy_strerror(res)));
				}
			} else {
				failures.emplace_back(std::make_tuple(recipient, ::curl_easy_strerror(res)));
			}
		} catch(std::exception const& ex) {
			failures.emplace_back(std::make_tuple(recipient, boost::diagnostic_information(ex)));
		} catch(...) {
			failures.emplace_back(std::make_tuple(recipient, boost::current_exception_diagnostic_information()));
		}
	}
	return failures;
}

std::string
curl::current_time_for_email() noexcept(false) {
	std::stringstream buffer;
	std::time_t t= std::time(nullptr);
	std::tm tm= *std::localtime(&t);
	buffer << std::put_time(&tm, "%a %d %b %Y %H:%M:%S %Y %z");
	return buffer.str();
}

void
curl::create_message(std::string const& to, std::string const& subject, std::string const& body) const noexcept(false) {
	const char newline[]= "\r\n";
	payload_text_strs_.clear();
	payload_text_strs_.emplace_back("Date: " + current_time_for_email() + newline);
	payload_text_strs_.emplace_back("To: " + to + newline);
	payload_text_strs_.emplace_back("From: " + from_ + newline);
	payload_text_strs_.emplace_back("Message-ID: " + get_uuid_as_string() + from_ + newline);
	payload_text_strs_.emplace_back("Subject: " + subject + newline);
	payload_text_strs_.emplace_back(newline);
	payload_text_strs_.emplace_back(body + newline);
}

}}
