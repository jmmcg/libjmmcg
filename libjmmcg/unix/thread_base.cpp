/******************************************************************************
** Copyright © 2005 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/private_/thread_base.hpp"

#include <boost/exception/diagnostic_information.hpp>

// Implementation details..... Don't look below here!!! ;)

//	"thread_base" implementation...

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

typedef thread_base<generic_traits::api_type::posix_pthreads, sequential_mode> thread_t_s;

template<>
thread_t_s::thread_traits::api_params_type::core_work_fn_ret_t
thread_t_s::core_work_fn(thread_traits::api_params_type::core_work_fn_arg_t ptr) noexcept(false) {
	DEBUG_ASSERT(ptr);
	thread_t_s* const pthis= reinterpret_cast<thread_t_s*>(ptr);
	DEBUG_ASSERT(pthis);
	if(pthis) {
		try {
			pthis->thread_params.state= thread_traits::api_params_type::states::active;
			thread_traits::set_cancelstate(thread_traits::api_params_type::thread_cancel_state::cancel_disable);
			int old_state;
			[[maybe_unused]] const int pth_err= pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &old_state);
			DEBUG_ASSERT(pth_err == 0);
			const thread_traits::api_params_type::states::element_type ret= pthis->process_chk();
			pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
			return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(ret);
#if defined(__GNUC__) && !defined(__clang__)
		} catch(abi::__forced_unwind const&) {
			pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
			throw;
#endif
		} catch(...) {
			LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Unknown exception caught. If it is a SIGSEGV, then the ss value in the thread_params ctor may need increasing. Details: " << boost::current_exception_diagnostic_information();
			auto ex= std::current_exception();
			if(ex) {
				pthis->exception_thrown_in_thread.set(ex);
			}
		}
		pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
		return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(thread_traits::api_params_type::states::unknown_exception);
	}
	// In reality, no errors are actively reported to the user for this case, as the pthis was null.
	// So we almost silently fail, and hope that they aren't sooooo dumb as to not notice that their work isn't being processed...
	// This is because the only "opportunity" I have to report it is as an exception in the destructor. Hooo-boy would that be evil...
	// Just think of the memory leaks.... So I don't do that. That's the breaks....
	return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(thread_traits::api_params_type::states::null_this_pointer);
}

typedef thread_base<generic_traits::api_type::posix_pthreads, heavyweight_threading> thread_t_m;

/**
 * \todo https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100415
 */
template<>
[[gnu::no_sanitize("null")]] thread_t_m::thread_traits::api_params_type::core_work_fn_ret_t
thread_t_m::core_work_fn(thread_traits::api_params_type::core_work_fn_arg_t ptr) noexcept(false) {
	DEBUG_ASSERT(ptr);
	thread_t_m* const pthis= reinterpret_cast<thread_t_m*>(ptr);
	DEBUG_ASSERT(dynamic_cast<thread_t_m*>(pthis));
	if(pthis) {
		try {
			pthis->thread_params.state= thread_traits::api_params_type::states::active;
			thread_traits::set_cancelstate(thread_traits::api_params_type::thread_cancel_state::cancel_disable);
			int old_state;
			[[maybe_unused]] const int pth_err= pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &old_state);
			DEBUG_ASSERT(pth_err == 0);
			const thread_traits::api_params_type::states::element_type ret= pthis->process_chk();
			pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
			return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(ret);
#if defined(__GNUC__) && !defined(__clang__)
		} catch(abi::__forced_unwind const&) {
			pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
			throw;
#endif
		} catch(...) {
			LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Unknown exception caught. If it is a SIGSEGV, then the ss value in the thread_params ctor may need increasing. Details: " << boost::current_exception_diagnostic_information();
			auto ex= std::current_exception();
			if(ex) {
				pthis->exception_thrown_in_thread.set(ex);
			}
		}
		pthis->thread_params.state= thread_traits::api_params_type::states::exiting;
		return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(thread_traits::api_params_type::states::unknown_exception);
	}
	// In reality, no errors are actively reported to the user for this case, as the pthis was null.
	// So we almost silently fail, and hope that they aren't sooooo dumb as to not notice that their work isn't being processed...
	// This is because the only "opportunity" I have to report it is as an exception in the destructor. Hooo-boy would that be evil...
	// Just think of the memory leaks.... So I don't do that. That's the breaks....
	return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(thread_traits::api_params_type::states::null_this_pointer);
}

}}}}
