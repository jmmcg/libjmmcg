#ifndef LIBJMMCG_UNIX_POSIX_LOCKING_HPP
#define LIBJMMCG_UNIX_POSIX_LOCKING_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <unistd.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace pthreads {

class condition_var {
public:
	typedef api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading> lock_traits;
	typedef lock_traits::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;

	explicit __stdcall condition_var(const int shared= PTHREAD_PROCESS_PRIVATE) noexcept(false);
	condition_var(condition_var const&)= delete;
	__stdcall ~condition_var() noexcept(true);

	operator pthread_cond_t const*() const noexcept(true) {
		return &cond_var;
	}
	operator pthread_cond_t*() noexcept(true) {
		return &cond_var;
	}

private:
	class attr;

	pthread_cond_t cond_var;
};

/**
 * Note that this is not the most efficient mutex, but it is safer than default-created mutexes, as it uses PTHREAD_MUTEX_ERRORCHECK semantics [1].
 *
 * [1] https://www.securecoding.cert.org/confluence/display/seccode/POS04-C.+Avoid+using+PTHREAD_MUTEX_NORMAL+type+mutex+locks
 *
 * \see PTHREAD_PROCESS_PRIVATE
 * \see PTHREAD_MUTEX_ERRORCHECK
 */
class anon_mutex : public lock::lockable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<anon_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<anon_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	using api_mutex_type= lock_traits::api_mutex_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

	__stdcall anon_mutex() noexcept(false);
	anon_mutex(anon_mutex const&)= delete;
	virtual __stdcall ~anon_mutex() noexcept(true);

	operator api_mutex_type*() noexcept(true);
	atomic_state_type __fastcall lock(const timeout_type timeout) noexcept(true) override final;
	atomic_state_type __fastcall lock() noexcept(true) override final;
	atomic_state_type __fastcall unlock() noexcept(true) override final;
	void decay() noexcept(true);

protected:
	explicit __stdcall anon_mutex(const int shared, const int err_chk= PTHREAD_MUTEX_ERRORCHECK) noexcept(false);

private:
	friend class mutex;
	class attr;

	api_mutex_type mutex;
};

class recursive_anon_mutex final : public anon_mutex {
public:
	typedef anon_mutex base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<recursive_anon_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<recursive_anon_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;

	__stdcall recursive_anon_mutex() noexcept(false)
		: anon_mutex(PTHREAD_PROCESS_PRIVATE, PTHREAD_MUTEX_RECURSIVE) {
	}
	__stdcall ~recursive_anon_mutex() noexcept(true) {}
};

class mutex final : public anon_mutex {
public:
	typedef anon_mutex base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;

	__stdcall mutex() noexcept(false)
		: anon_mutex(PTHREAD_PROCESS_SHARED) {
	}
	__stdcall ~mutex() noexcept(true) {}
};

class recursive_mutex final : public anon_mutex {
public:
	typedef lock::in_process<recursive_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<recursive_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;

	__stdcall recursive_mutex() noexcept(false)
		: anon_mutex(PTHREAD_PROCESS_SHARED, PTHREAD_MUTEX_RECURSIVE) {
	}
	__stdcall ~recursive_mutex() noexcept(true) {}
};

class anon_semaphore final : public lock::lockable_settable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> {
public:
	typedef lock::lockable_settable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef int count_type;
	typedef atomic_state_type lock_result_type;
	typedef lock::in_process<anon_semaphore> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	using api_event_type= lock_traits::api_event_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

	/**
		Manual reset and initially signalled.
	*/
	explicit __stdcall anon_semaphore(const atomic_state_type state) noexcept(false);
	anon_semaphore(anon_semaphore const&)= delete;
	virtual __stdcall ~anon_semaphore() noexcept(true);

	operator api_event_type*() noexcept(true);
	atomic_state_type __fastcall set() noexcept(true) override;
	atomic_state_type __fastcall reset() noexcept(true) override;
	lock_result_type __fastcall try_lock() noexcept(true) override;
	lock_result_type __fastcall lock() noexcept(false) override;
	lock_result_type __fastcall lock(const timeout_type period) noexcept(false) override;
	lock_result_type __fastcall unlock() noexcept(true) override;
	void clear() noexcept(true);
	void decay() noexcept(true) {}
	count_type __fastcall count() const noexcept(false);

private:
	friend class semaphore;

	mutable sem_t sem;

	explicit __stdcall anon_semaphore(const atomic_state_type state, int shared) noexcept(false);
};

class semaphore : public lock::lockable_settable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> {
public:
	typedef lock::lockable_settable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef anon_semaphore atomic_t;
	typedef atomic_t::count_type count_type;
	typedef lock::in_process<semaphore> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	using api_event_type= lock_traits::api_event_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

	/**
		Manual reset and initially signalled.
	*/
	explicit __stdcall semaphore(const atomic_state_type state= lock_traits::atom_set) noexcept(false)
		: event_(state, 1) {
	}
	semaphore(semaphore const&)= delete;
	__stdcall ~semaphore() noexcept(true) {}

	operator api_event_type*() noexcept(true) {
		return static_cast<api_event_type*>(event_);
	}
	atomic_state_type __fastcall set() noexcept(true) override {
		return event_.set();
	}
	atomic_state_type __fastcall reset() noexcept(true) override {
		return event_.reset();
	}
	atomic_state_type __fastcall try_lock() noexcept(true) override {
		return event_.try_lock();
	}
	atomic_state_type __fastcall lock() noexcept(false) override {
		return event_.lock();
	}
	atomic_state_type __fastcall unlock() noexcept(true) override {
		return event_.unlock();
	}
	void decay() noexcept(true) {}
	count_type __fastcall count() const noexcept(noexcept(std::declval<atomic_t>().count())) {
		return event_.count();
	}

protected:
	atomic_state_type __fastcall lock(const timeout_type period) noexcept(false) override {
		return event_.lock(period);
	}

private:
	atomic_t event_;
};

/// An anonymous mutex-like atomic object that is non-recursive.
/**
	This object can be unlocked on a different thread from that which it was locked on. Moreover if thread A contains a reference to this atomic object and locks it, then subsequently attempts to re-gain the lock, thus blocking thread A (as the object is locked), and thread B also has a reference to this atomic object, and unlocks it, then thread A will be released.
	This atomic object is implemented via an anonymous event.

	\see anon_event
*/
class nonrecursive_anon_mutex final : public lock::lockable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>> base_t;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<nonrecursive_anon_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<nonrecursive_anon_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

private:
	anon_semaphore event_{lock_traits::atom_set};

public:
	__stdcall nonrecursive_anon_mutex() noexcept(noexcept(anon_semaphore(lock_traits::atom_set)));
	nonrecursive_anon_mutex(nonrecursive_anon_mutex const&)= delete;
	__stdcall ~nonrecursive_anon_mutex() noexcept(true);

	atomic_state_type __fastcall lock(const timeout_type timeout) noexcept(noexcept(event_.lock(timeout))) override;
	atomic_state_type __fastcall lock() noexcept(noexcept(event_.lock())) override;
	atomic_state_type __fastcall unlock() noexcept(noexcept(event_.set())) override;
	void decay() noexcept(true);
};

}}}}

#include "posix_locking_impl.hpp"

#endif
