/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public

** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/atomic_counter.hpp"
#include "../core/exception.hpp"

#include <boost/stacktrace.hpp>

#include <atomic>
#include <csignal>

#include <errno.h>
#include <pthread.h>

#if _POSIX_C_SOURCE < 200112L
#	error "POSIX semaphores, sem_*()s, required for this library. Also link with -lrt or -pthread to include the sem_*()s."
#endif

#include <semaphore.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

namespace pthreads {
class anon_mutex;
class nonrecursive_anon_mutex;
class recursive_anon_mutex;
class anon_semaphore;
class critical_section;
class mutex;
class recursive_mutex;
class event;
class anon_semaphore;
class semaphore;
}

/// The various lock-types for POSIX-threads derived, platforms.
template<>
struct api_lock_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading> {
	static inline constexpr const generic_traits::api_type::element_type api_type= generic_traits::api_type::posix_pthreads;
	typedef heavyweight_threading model_type;

	enum atomic_state_type {
		atom_set= 0,
		atom_abandoned= EINVAL,
		atom_already_set= EBUSY,
		atom_max_recurse= EAGAIN,
		atom_deadlocked= EDEADLK,
		atom_perm_error= EPERM,
		atom_failed= ESRCH,
		atom_interrupted= EINTR,
		atom_nomemory= ENOMEM,
		atom_unset= ETIMEDOUT,
		atom_sem_overflow= EOVERFLOW,
		atom_errno= -1
	};

	typedef ppd::pthreads::anon_mutex anon_mutex_type;
	typedef ppd::pthreads::nonrecursive_anon_mutex nonrecursive_anon_mutex_type;
	typedef ppd::pthreads::recursive_anon_mutex recursive_anon_mutex_type;
	typedef ppd::pthreads::anon_semaphore anon_event_type;
	using anon_spin_event_type= ppd::lock::anon_spin_event<api_lock_traits>;
	typedef ppd::pthreads::anon_mutex critical_section_type;
	typedef ppd::pthreads::mutex mutex_type;
	typedef ppd::pthreads::recursive_mutex recursive_mutex_type;
	typedef ppd::pthreads::recursive_mutex recursive_critical_section_type;
	typedef ppd::pthreads::semaphore event_type;
	typedef ppd::pthreads::anon_semaphore anon_semaphore_type;
	typedef ppd::pthreads::semaphore semaphore_type;
	template<class V>
	using atomic_counter_type= atomic_ctr<V, api_lock_traits<api_type, model_type>>;
	template<class V>
	using noop_atomic_ctr= noop_atomic_ctr_base<V, api_lock_traits<api_type, model_type>>;
	template<class V>
	using atomic= atomic_ctr<V, api_lock_traits<api_type, model_type>>;
	using api_mutex_type= pthread_mutex_t;
	using api_event_type= sem_t;
	using timeout_type= unsigned long;
	template<class... T>
	using scoped_lock= std::scoped_lock<T...>;

	using exception_type= std::runtime_error;

	static constexpr timeout_type __fastcall infinite_timeout() noexcept(true) {
		return ~static_cast<timeout_type>(0);
	}

	api_lock_traits()= delete;
};

/**
 * \todo Relace with LIBJMMCG_MAKE_ENUM?
 *
 * See <a href="https://stackoverflow.com/questions/77751341/fmt-failed-to-compile-while-formatting-enum-after-upgrading-to-v10-and-above#77751342/>
 */
inline auto
format_as(api_lock_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::atomic_state_type t) {
	return fmt::underlying(t);
}

template<>
struct api_threading_traits<generic_traits::api_type::no_api, sequential_mode> : api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode> {};

}}}

#include "thread_api_traits_impl.hpp"
