/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace pthreads {

class condition_var::attr {
public:
	explicit __stdcall attr(const int shared) noexcept(true) {
		[[maybe_unused]] const int ret1= pthread_condattr_init(&attr_);
		DEBUG_ASSERT(ret1 == 0);
		[[maybe_unused]] const int ret2= pthread_condattr_setpshared(&attr_, shared);
		DEBUG_ASSERT(ret2 == 0);
	}
	attr(attr const&)= delete;
	__stdcall ~attr() noexcept(true) {
		[[maybe_unused]] const int ret= pthread_condattr_destroy(&attr_);
		DEBUG_ASSERT(ret == 0);
	}

	operator pthread_condattr_t const*() const noexcept(true) {
		return &attr_;
	}
	operator pthread_condattr_t*() noexcept(true) {
		return &attr_;
	}

	friend inline tostream& __fastcall
	operator<<(tostream& os, attr const& a) {
		os << static_cast<pthread_condattr_t const*>(a);
		return os;
	}

private:
	pthread_condattr_t attr_;
};

inline condition_var::condition_var(const int shared) noexcept(false) {
	attr attrs(shared);
	const atomic_state_type pth_err= static_cast<atomic_state_type>(::pthread_cond_init(&cond_var, static_cast<const pthread_condattr_t*>(attrs)));
	if(pth_err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not initialise condition variable. shared={}, attrs={}", shared, tostring(attrs)), "pthread_cond_init", this, pth_err));
	}
}

inline condition_var::~condition_var() noexcept(true) {
	[[maybe_unused]] const atomic_state_type pth_err= static_cast<atomic_state_type>(pthread_cond_destroy(&cond_var));
	DEBUG_ASSERT(pth_err == lock_traits::atom_set);
}

class anon_mutex::attr {
public:
	__stdcall attr(const int shared, const int err_chk) noexcept(true) {
		[[maybe_unused]] const int ret1= pthread_mutexattr_init(&attr_);
		DEBUG_ASSERT(ret1 == 0);
		[[maybe_unused]] const int ret2= pthread_mutexattr_setpshared(&attr_, shared);
		DEBUG_ASSERT(ret2 == 0);
		[[maybe_unused]] const int ret3= pthread_mutexattr_settype(&attr_, err_chk);
		DEBUG_ASSERT(ret3 == 0);
	}
	attr(attr const&)= delete;
	__stdcall ~attr() noexcept(true) {
		[[maybe_unused]] const int ret= pthread_mutexattr_destroy(&attr_);
		DEBUG_ASSERT(ret == 0);
	}

	operator pthread_mutexattr_t const*() const noexcept(true) {
		return &attr_;
	}
	operator pthread_mutexattr_t*() noexcept(true) {
		return &attr_;
	}

	friend inline tostream& __fastcall
	operator<<(tostream& os, attr const& a) noexcept(false) {
		os << static_cast<pthread_mutexattr_t const*>(a);
		return os;
	}

private:
	pthread_mutexattr_t attr_;
};

inline anon_mutex::anon_mutex() noexcept(false) {
	attr attrs(PTHREAD_PROCESS_PRIVATE, PTHREAD_MUTEX_ERRORCHECK);
	const atomic_state_type pth_err= static_cast<atomic_state_type>(::pthread_mutex_init(&mutex, static_cast<pthread_mutexattr_t*>(attrs)));
	if(pth_err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not initialise manonymous utex. attrs={}", tostring(attrs)), "pthread_mutex_init", this, pth_err));
	}
}

inline anon_mutex::~anon_mutex() noexcept(true) {
	[[maybe_unused]] const atomic_state_type pth_err= static_cast<atomic_state_type>(pthread_mutex_destroy(&mutex));
	DEBUG_ASSERT(pth_err == lock_traits::atom_set);
}

inline anon_mutex::operator api_mutex_type*() noexcept(true) {
	return &mutex;
}

inline anon_mutex::atomic_state_type
anon_mutex::lock() noexcept(true) {
	return static_cast<atomic_state_type>(pthread_mutex_lock(&mutex));
}

inline anon_mutex::atomic_state_type
anon_mutex::lock(const timeout_type timeout) noexcept(true) {
	if(timeout == lock_traits::infinite_timeout()) {
		return lock();
	} else {
		return static_cast<atomic_state_type>(pthread_mutex_trylock(&mutex));
	}
}

inline anon_mutex::atomic_state_type
anon_mutex::unlock() noexcept(true) {
	const atomic_state_type ret= static_cast<atomic_state_type>(pthread_mutex_unlock(&mutex));
	return ret ? static_cast<atomic_state_type>(ret) : lock_traits::atom_unset;
}

inline void
anon_mutex::decay() noexcept(true) {
}

inline anon_mutex::anon_mutex(const int shared, const int err_chk) noexcept(false) {
	attr attrs(shared, err_chk);
	const atomic_state_type pth_err= static_cast<atomic_state_type>(::pthread_mutex_init(&mutex, static_cast<pthread_mutexattr_t*>(attrs)));
	if(pth_err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not initialise anonymous mutex. shared={}, err_chk={}, attrs={}", shared, err_chk, tostring(attrs)), "pthread_mutex_init", this, pth_err));
	}
}

inline anon_semaphore::anon_semaphore(const atomic_state_type state) noexcept(false) {
	const atomic_state_type err= static_cast<atomic_state_type>(::sem_init(&sem, 0, state == lock_traits::atom_set));
	if(err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not initialise anonymous semaphore. state={}", state), "sem_init", this));
	}
}

inline anon_semaphore::anon_semaphore(const atomic_state_type state, int shared) noexcept(false) {
	const atomic_state_type err= static_cast<atomic_state_type>(::sem_init(&sem, shared, state == lock_traits::atom_set));
	if(err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not initialise anonymous semaphore. state={}, shared={}", state, shared), "sem_init", this));
	}
}

inline anon_semaphore::~anon_semaphore() noexcept(true) {
	[[maybe_unused]] const atomic_state_type err= static_cast<atomic_state_type>(sem_destroy(&sem));
	DEBUG_ASSERT(err == lock_traits::atom_set);
}

inline anon_semaphore::operator api_event_type*() noexcept(true) {
	return static_cast<api_event_type*>(&sem);
}

inline anon_semaphore::atomic_state_type
anon_semaphore::set() noexcept(true) {
	return static_cast<atomic_state_type>(sem_post(&sem));
}

inline anon_semaphore::lock_result_type
anon_semaphore::unlock() noexcept(true) {
	return static_cast<atomic_state_type>(sem_trywait(&sem));
}

inline anon_semaphore::atomic_state_type
anon_semaphore::reset() noexcept(true) {
	return unlock();
}

inline anon_semaphore::lock_result_type
anon_semaphore::try_lock() noexcept(true) {
	if(!sem_trywait(&sem)) {
		return lock_traits::atom_set;
	} else {
		return lock_traits::atom_unset;
	}
}

inline anon_semaphore::lock_result_type
anon_semaphore::lock() noexcept(false) {
	if(!sem_wait(&sem)) {
		return lock_traits::atom_set;
	} else {
		return lock_traits::atom_abandoned;
	}
}

inline void
anon_semaphore::clear() noexcept(true) {
	while(try_lock() == lock_traits::atom_set)
		;
}

inline anon_semaphore::count_type
anon_semaphore::count() const noexcept(false) {
	count_type val= 0;
	const atomic_state_type err= static_cast<atomic_state_type>(::sem_getvalue(&sem, &val));
	if(err != lock_traits::atom_set) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>(fmt::format("Could not get the count from the anonymouns semaphore. count={}", val), "sem_getvalue", &anon_semaphore::count));
	}
	return val;
}

inline nonrecursive_anon_mutex::nonrecursive_anon_mutex() noexcept(noexcept(anon_semaphore(lock_traits::atom_set))) {
	DEBUG_ASSERT(event_.count() == 1);
}

inline nonrecursive_anon_mutex::~nonrecursive_anon_mutex() noexcept(true) {
	event_.clear();
}

inline nonrecursive_anon_mutex::atomic_state_type
nonrecursive_anon_mutex::lock(const timeout_type timeout) noexcept(noexcept(event_.lock(timeout))) {
	const atomic_state_type ret= event_.lock(timeout);
	DEBUG_ASSERT(event_.count() >= 0);
	return ret;
}

inline nonrecursive_anon_mutex::atomic_state_type
nonrecursive_anon_mutex::lock() noexcept(noexcept(event_.lock())) {
	const atomic_state_type ret= event_.lock();
	DEBUG_ASSERT(event_.count() >= 0);
	return ret;
}

inline nonrecursive_anon_mutex::atomic_state_type
nonrecursive_anon_mutex::unlock() noexcept(noexcept(event_.set())) {
	DEBUG_ASSERT(event_.count() >= 0);
	const atomic_state_type ret= event_.set();
	DEBUG_ASSERT(event_.count() >= 0);
	return ret;
}

inline void
nonrecursive_anon_mutex::decay() noexcept(true) {
}

}}}}
