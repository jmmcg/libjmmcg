/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<typename Mdl>
inline typename hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::value_type
hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::to_usec(const time_utc_t ticks) noexcept(true) {
	const value_type tmp= static_cast<value_type>(static_cast<value_type>(ticks.tv_sec) * 1000000 + static_cast<value_type>(static_cast<double>(ticks.tv_nsec) / 1000.0));
	return tmp;
}

template<typename Mdl>
inline const typename hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::time_utc_t
hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::get_start_time() noexcept(false) {
	time_utc_t tm;
	if(::clock_gettime(CLOCK_REALTIME, &tm)) {
		BOOST_THROW_EXCEPTION(throw_crt_exception<std::invalid_argument>("High-performance counter not supported.", "clock_gettime", &hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::get_start_time));
	}
	return tm;
}

template<typename Mdl>
inline typename hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::value_type
hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::current_count() noexcept(false) {
	return to_usec(get_start_time());
}

template<typename Mdl>
inline const typename hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::time_utc_t
hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::current_time() const noexcept(false) {
	return get_start_time();
}

template<typename Mdl>
inline hp_timer<ppd::generic_traits::api_type::posix_pthreads, Mdl>::hp_timer() noexcept(false)
	: start_up_time(get_start_time()),
	  start_up_count(current_count()) {
}

template<typename T>
inline hp_interval<T>::~hp_interval() noexcept(true) {
	const typename timer_t::time_utc_t end= timer.current_time();
	if(end.tv_nsec >= start.tv_nsec) {
		interval.tv_nsec= end.tv_nsec - start.tv_nsec;
		interval.tv_sec= end.tv_sec - start.tv_sec;
	} else {
		interval.tv_nsec= 1000000000 + (end.tv_nsec - start.tv_nsec);
		interval.tv_sec= end.tv_sec - start.tv_sec - 1;
	}
}

}}
