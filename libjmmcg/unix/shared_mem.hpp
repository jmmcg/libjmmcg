/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/exception.hpp"
#include "core/int128_compatibility.hpp"
#include "core/logging.hpp"
#include "core/max_min.hpp"
#include "core/sizeof_void.hpp"
#include "core/syscall_wrapper.hpp"
#include "core/yet_another_enum_wrapper.hpp"

#include <boost/interprocess/detail/os_thread_functions.hpp>

#include <cstddef>
#include <cstring>
#include <memory>
#include <memory_resource>
#include <typeinfo>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Create a region of shared-memory.
/**
 * This tries to model a container in a simplistic way, or a PMR-style allocator that can only allocate a single instance of a object, e.g. the actual PMR-style allocator one really desires.
 *
 * \note: That suitable setting for 'ulimit' must be set and swap turned on, of appropriate size, otherwise this may fail, perhaps by throwing an exception that contains the string-translation similar to "permission denied".
 *
 * \see ulimit
 */
class shared_mem final {
public:
	static inline constexpr char name_prefix= '/';
	static inline constexpr char pid_separator= '.';
	static inline constexpr char pid_padding= '0';

	static inline constexpr std::size_t shm_internal_name_extra_size= sizeof(name_prefix) + sizeof(pid_separator) + std::numeric_limits<boost::interprocess::ipcdetail::OS_process_id_t>::digits10;
	static inline constexpr std::size_t shm_name_size= NAME_MAX - shm_internal_name_extra_size;

	BOOST_MPL_ASSERT_RELATION(shm_internal_name_extra_size, <, NAME_MAX);
	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using element_type= std::byte;
	using value_type= std::byte;
	using size_type= std::size_t;
	using difference_type= std::ptrdiff_t;
	using pointer= element_type*;
	using const_pointer= element_type const*;
	using iterator= pointer;
	using const_iterator= const_pointer;
	using name_type= std::string;
	using handle_t= int;
	/// The user may specify a 'version' for their messages that will be used to check type-conversions.
	/**
	 * \see allocator_type::make_unique(), get()
	 */
	using magic_type= uint128_t;

	LIBJMMCG_MAKE_ENUM_TAG_VALUES(oflags_t, int, (oflags_read_only, O_RDONLY), (oflags_read_write, O_RDWR), (oflags_create_if_not_exists, O_CREAT), (oflags_exclusive_or_error, O_EXCL), (oflags_truncate_if_exists, O_TRUNC));
	using oflags_type= oflags_t::underlying_type_t;
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(flags_t, int,
		/// Requires kernel support and configuration.
		/**
			<a href="https://www.kernel.org/doc/html/latest/admin-guide/mm/hugetlbpage.html"/>
		*/
		(flags_use_hugetlb, MAP_HUGETLB));
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(mode_type, mode_t, (mode_rwxrwxrwx, ACCESSPERMS), (mode_srwxrwxrwx, ALLPERMS), (mode_rwrwrw, DEFFILEMODE));
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(protection_type, int, (protection_read_only, PROT_READ), (protection_read_write, PROT_READ | PROT_WRITE), (protection_read_write_execte, PROT_READ | PROT_WRITE | PROT_EXEC));
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(sync_flags_t, int, (schedule_updates_return_immed, MS_ASYNC), (req_update_wait, MS_SYNC));

	/// A very simple PMR-style allocator.
	/**
	 * This allocator is an utterly trivial one that basically returns *the* pointer to the underlying buffer, i.e. that returned pointer never changes, so one can only ever allocate one object at a time in the shared-memory region! The idea is that the user might allocate *into* the shared-memory region their own pmr-style allocator, which would then be appropriate for the needs of each user, rather than imposing upon the unsuspecting user an allocator that I try to implement (that would be inefficient, buggy, etc, etc).
	 */
	class allocator_type final : public std::pmr::memory_resource {
	public:
		using element_type= shared_mem::element_type;
		using value_type= shared_mem::value_type;
		using size_type= shared_mem::size_type;
		using difference_type= shared_mem::difference_type;
		using pointer= shared_mem::pointer;
		using const_pointer= shared_mem::const_pointer;
		/// The deletor to be used to deallocate the pointer that is returned.
		class deletor;

		/// This is used to add extra information to permit run-time verification that the correct casting is done.
		/**
		 * \see shared_mem::get()
		 */
		template<class T>
		class internal_shm_wrapper {
		public:
			friend class deletor;
			using element_type= T;

			template<class... Args>
			explicit internal_shm_wrapper(magic_type Magic, Args&&... args) noexcept(false);

			element_type* operator->() noexcept(true);
			element_type const* operator->() const noexcept(true);
			element_type& operator*() noexcept(true);
			element_type const& operator*() const noexcept(true);

			/// Try to verify that the requested type, Dest, is the same as the allocated data, element_type and that the magic is the same as the user-supplied DestMagic.
			template<magic_type DestMagic, class Dest>
			[[nodiscard]] bool equal() const noexcept(true);

			[[nodiscard]] std::string to_string() const noexcept(false);

		private:
			const magic_type lib_version{LIBJMMCG_GIT_SHA_SHORT};	  ///< Each executable should have been compiled with the same version of libjmmcg.
			/// Permit the user to specify their own compatibility requirements.
			/**
			 * This permits the user to have a finer-grained control regarding the compatibility of the types they create that are used in the shared-memory.
			 * The underlying type is large enough for a 32-hex digit CRC, SHA, etc for convenience, e.g. a CRC of just the types to be used in the shared memory that the user should generate.
			 */
			const magic_type magic_;
			const std::size_t hash_of_type_of_data_;	 ///< Try to ensure that the created and requested types are the same to avoid incorrect casting.
			alignas(double) element_type data_;	  ///< Maximally align the buffer to ensure alignment issues for any contained element_type.
		};
		/// A managed-pointer type to assist in allocating the memory safely.
		/**
		 * \see make_unique()
		 */
		template<class Dest>
		using unique_ptr= std::unique_ptr<Dest, deletor>;

		[[nodiscard]] explicit allocator_type(shared_mem& shm) noexcept(true);

		/// Allocate and in-place construct the requested type Dest in the shared-memory block, using the arguments args.
		/**
		 * This should be called once only by only one of the processes that creates an instance of this shared_mem type.
		 *
		 * \param args	The arguments used to in-place construct an instance of the object of type Dest.
		 * \return	A unique_ptr wrapper to the pointer to the allocated object of type Dest.
		 *
		 * \see unique_ptr
		 */
		template<
			magic_type Magic,	  ///< The user-specified version that should be associated with the instance of data allocated.
			class Dest,	  ///< The type of object to be allocated. Any internal allocations within the object will not be in the shared-memory buffer, so those pointers will be invalid in the virtual-memory space of other processes.
			class... Args>
		unique_ptr<Dest> make_unique(Args&&... args) noexcept(false);

	private:
		shared_mem& shm_;

		void* do_allocate(size_type bytes, std::size_t alignment) noexcept(false) override;
		void do_deallocate(void* p, size_type bytes, std::size_t alignment) noexcept(true) override;
		[[nodiscard]] bool do_is_equal(std::pmr::memory_resource const& other) const noexcept(true) override;
	};

	static inline constexpr std::size_t min_size= sizeof(allocator_type::internal_shm_wrapper<std::uint8_t>) - sizeof(std::uint8_t);	  ///< The minimum size of the mapped region, including any implementation-specific data.
	static inline constexpr std::size_t max_size= std::numeric_limits<size_type>::max() - min_size;	  ///< The maximum size available to the user that they may allocate in the shared_mem instance, which excludes any implementation-specific data.

	const name_type name_;
	/// The size of the block of memory that was requested to be allocated plus any necessary extra required by this implementation.
	const std::size_t capacity_;

	/// Create a shared-memory region with a name of name prefixed by start with an initial name_prefix concatenated with name then he pid_separator then the pid, of capacity of at least size bytes.
	/**
	 * \param	name	The name of the shared-memory region that must be distinguishable, i.e. non-empty. Note that if the program aborts, i.e. dtors are not run, it is likely that a file will be leaked in "/dev/shm/" that will affect subsequent execution. It is UB if this file has been leaked. Naturally, the leaked file will contain the last details as the program aboirted. This may be useful for subsequent triage, thus it is not automatically deleted.
	 * \param	size	The size that the shared-memory region shall be created in bytes. Note that this will be smaller than that actually allocated, as it has to include certain, implementation-defined, house-keeping data, as recorded in capacity_.
	 * \param	pid	The PID that should be used, concatenated onto the name, separated by pid_separator, by default this is set to zero, thus the named shared-memory region is shared across all processes. This parameter allows this to be changed so that similarly name'd shared-memory regions may be distinguished this is of a suitable fixed-with, padded with leading pid_padding. May be obtained via boost::interprocess::ipcdetail::get_current_process_id()
	 *
	 * \see boost::interprocess::ipcdetail::get_current_process_id()
	 */
	[[nodiscard]] shared_mem(name_type const& name, size_type size, boost::interprocess::ipcdetail::OS_process_id_t pid= boost::interprocess::ipcdetail::OS_process_id_t{}, oflags_type oflags= oflags_t::oflags_create_if_not_exists | oflags_t::oflags_read_write, mode_type::element_type mode= mode_type::mode_rwrwrw, protection_type::element_type prot= protection_type::protection_read_write) noexcept(false);
	~shared_mem() noexcept(true);

	constexpr allocator_type& get_allocator() const noexcept(true);

	/// Return a pointer to the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous. A safer version is get().
	 *
	 * \return	A pointer suitable for reinterpret_cast'ing to one's desired target-type, into the underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique(), get()
	 */
	pointer data() noexcept(true);
	/// Return a pointer to the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous. A safer version is get().
	 *
	 * \return	A pointer suitable for reinterpret_cast'ing to one's desired target-type, into the underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique(), get()
	 */
	const_pointer data() const noexcept(true);

	/// Return an iterator to the beginning of the block of the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous.
	 *
	 * \return	An iterator into the raw, underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique()
	 */
	iterator begin() noexcept(true);
	/// Return an iterator to the beginning of the block of the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous.
	 *
	 * \return	An iterator into the raw, underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique()
	 */
	const_iterator begin() const noexcept(true);
	/// Return an iterator to the end of the block of the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous.
	 *
	 * \return	An iterator into the raw, underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique()
	 */
	iterator end() noexcept(true);
	/// Return an iterator to the end of the block of the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * No checks are performed with this function, thus it is potentially dangerous.
	 *
	 * \return	An iterator into the raw, underlying buffer, which should have been previously allocated. On no account attempt to deallocate the returned pointer via any means.
	 *
	 * \see allocator_type::make_unique()
	 */
	const_iterator end() const noexcept(true);

	/// Return the size of the block of memory that was requested to be allocated.
	/**
	 * \return	Note that the actual amount allocated will almost certainly be larger, but the original amount requested will be returned.
	 *
	 * \see capacity_
	 */
	size_type size() const noexcept(true);

	/// Obtain a reference to the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * Runtime checks are performed to attempt to ensure that the requested type should be the same as the data (the instance of the actual type allocated in the shared_mem). These include checking: the std::type_info::hash_code() of the stored and requested types (Dest), the version (git-SHA) that libjmmcg was compiled with that has been stored in the shared_mem vs what version of libjmmcg the user has used and finally a 'magic' that the user may supply via DestMagic which must compare equal for the data allocated and the magic the user must have supplied. If these fail an exception shall be thrown.
	 *
	 * \return	A reference to the previously allocated data.
	 *
	 * \see allocator_type::make_unique(), get_shared_memory_magic()
	 */
	template<
		magic_type DestMagic,	///< The user-specified version that should be associated with the instance of data allocated.
		class Dest>
	Dest const& get() const noexcept(false);
	/// Obtain a reference to the previously allocated data.
	/**
	 * The data must have been previously allocated via allocator_type::make_unique().
	 * Runtime checks are performed to attempt to ensure that the requested type should be the same as the data (the instance of the actual type allocated in the shared_mem). These include checking: the std::type_info::hash_code() of the stored and requested types (Dest), the version (git-SHA) that libjmmcg was compiled with that has been stored in the shared_mem vs what version of libjmmcg the user has used and finally a 'magic' that the user may supply via DestMagic which must compare equal for the data allocated and the magic the user must have supplied. If these fail an exception shall be thrown.
	 *
	 * \return	A reference to the previously allocated data.
	 *
	 * \see allocator_type::make_unique(), get_shared_memory_magic()
	 */
	template<
		magic_type DestMagic,	///< The user-specified version that should be associated with the instance of data allocated.
		class Dest>
	Dest& get() noexcept(false);

	void sync(pointer addr, size_type size, sync_flags_t::element_type sync_flags= sync_flags_t::schedule_updates_return_immed) noexcept(false);
	template<class T>
	void sync(T* addr, sync_flags_t::element_type sync_flags= sync_flags_t::schedule_updates_return_immed) noexcept(false);

private:
	class fd_deletor {
	public:
		handle_t fd_;

		fd_deletor(handle_t fd, name_type const& name) noexcept(true)
			: fd_(fd), name_(name) {
		}

		~fd_deletor() noexcept(true) {
			try {
				JMMCG_SYSCALL_WRAPPER("Failed unlink the named shared-memory object.", ::shm_unlink, name_.data());
			} catch(...) {
				LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Exception caught and ignored: " << boost::current_exception_diagnostic_information();
			}
		}

	private:
		name_type const& name_;
	};

	class mmap_deletor {
	public:
		pointer mmapped_buffer_;	/// Note that this address is maximally aligned.

		mmap_deletor(pointer mmapped_buffer, const std::size_t size) noexcept(true)
			: mmapped_buffer_(mmapped_buffer), size_(size) {
			DEBUG_ASSERT(mmapped_buffer_);
			DEBUG_ASSERT(size_ > 0);
		}

		~mmap_deletor() noexcept(true) {
			try {
				JMMCG_SYSCALL_WRAPPER("Failed to unmap region.", ::munmap, mmapped_buffer_, size_);
			} catch(...) {
				LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Exception caught and ignored: " << boost::current_exception_diagnostic_information();
			}
		}

	private:
		const std::size_t size_;
	};

	const fd_deletor fd_manager;
	const mmap_deletor mmap_manager;
	mutable allocator_type allocator_;

	static handle_t open_fd(name_type const& name, oflags_type oflags, mode_type::element_type mode) noexcept(false);
	static pointer map_fd(handle_t fd, std::size_t length, protection_type::element_type prot) noexcept(false);
	static std::string to_string(boost::interprocess::ipcdetail::OS_process_id_t pid) noexcept(false);
};

}}

#include "shared_mem_impl.hpp"
