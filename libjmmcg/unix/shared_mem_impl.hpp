/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class T>
template<class... Args>
inline shared_mem::allocator_type::internal_shm_wrapper<T>::internal_shm_wrapper(magic_type Magic, Args&&... args) noexcept(false)
	: magic_{Magic}, hash_of_type_of_data_(typeid(internal_shm_wrapper<element_type>).hash_code()), data_(std::forward<Args>(args)...) {
}

template<class T>
inline
	typename shared_mem::allocator_type::internal_shm_wrapper<T>::element_type*
	shared_mem::allocator_type::internal_shm_wrapper<T>::operator->() noexcept(true) {
	return &data_;
}

template<class T>
inline
	typename shared_mem::allocator_type::internal_shm_wrapper<T>::element_type const*
	shared_mem::allocator_type::internal_shm_wrapper<T>::operator->() const noexcept(true) {
	return &data_;
}

template<class T>
inline
	typename shared_mem::allocator_type::internal_shm_wrapper<T>::element_type&
	shared_mem::allocator_type::internal_shm_wrapper<T>::operator*() noexcept(true) {
	return data_;
}

template<class T>
inline
	typename shared_mem::allocator_type::internal_shm_wrapper<T>::element_type const&
	shared_mem::allocator_type::internal_shm_wrapper<T>::operator*() const noexcept(true) {
	return data_;
}

template<class T>
template<shared_mem::magic_type DestMagic, class Dest>
inline bool
shared_mem::allocator_type::internal_shm_wrapper<T>::equal() const noexcept(true) {
	return lib_version == LIBJMMCG_GIT_SHA_SHORT
			 && hash_of_type_of_data_ == typeid(internal_shm_wrapper<Dest>).hash_code()
			 && magic_ == DestMagic;
}

template<class T>
inline std::string
shared_mem::allocator_type::internal_shm_wrapper<T>::to_string() const noexcept(false) {
	std::ostringstream os;
	os
		<< "git-SHA of libjmmcg=" << lib_version
		<< ", hash of embedded type=" << hash_of_type_of_data_
		<< "', magic=" << magic_;
	return os.str();
}

class shared_mem::allocator_type::deletor {
public:
	explicit deletor(allocator_type& allocator) noexcept(true)
		: allocator_(allocator) {
	}

	template<class T, class U= internal_shm_wrapper<T>>
	void operator()(T* t) const noexcept(noexcept(std::declval<U>().~U())) {
		constexpr std::size_t const base_offset= offsetof(U, data_);
		U* const internal_obj= reinterpret_cast<U*>(reinterpret_cast<std::byte*>(t) - base_offset);
		internal_obj->~U();
		allocator_.deallocate(internal_obj, sizeof(U));
	}

private:
	allocator_type& allocator_;
};

inline shared_mem::allocator_type::allocator_type(shared_mem& shm) noexcept(true)
	: shm_(shm) {
}

template<shared_mem::magic_type Magic, class Dest, class... Args>
inline shared_mem::allocator_type::unique_ptr<Dest>
shared_mem::allocator_type::make_unique(Args&&... args) noexcept(false) {
	using element_type= internal_shm_wrapper<Dest>;
	pointer buff= reinterpret_cast<pointer>(allocate(sizeof(element_type)));
	try {
		element_type* allocated_obj= new(buff) element_type(Magic, std::forward<Args>(args)...);
		DEBUG_ASSERT(allocated_obj);
		return unique_ptr<Dest>(allocated_obj->operator->(), deletor(*this));
	} catch(...) {
		deallocate(buff, sizeof(element_type));
		throw;
	}
}

inline constexpr shared_mem::allocator_type&
shared_mem::get_allocator() const noexcept(true) {
	return allocator_;
}

inline shared_mem::pointer
shared_mem::data() noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, min_size);
}

inline shared_mem::const_pointer
shared_mem::data() const noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, min_size);
}

inline shared_mem::iterator
shared_mem::begin() noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, min_size);
}

inline shared_mem::const_iterator
shared_mem::begin() const noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, min_size);
}

inline shared_mem::iterator
shared_mem::end() noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, static_cast<allocator_type::difference_type>(capacity_));
}

inline shared_mem::const_iterator
shared_mem::end() const noexcept(true) {
	return std::next(mmap_manager.mmapped_buffer_, static_cast<allocator_type::difference_type>(capacity_));
}

inline shared_mem::size_type
shared_mem::size() const noexcept(true) {
	return capacity_ - min_size;
}

template<shared_mem::magic_type DestMagic, class Dest>
inline Dest const&
shared_mem::get() const noexcept(false) {
	using internal_element_type= allocator_type::internal_shm_wrapper<Dest>;
	internal_element_type const* data= reinterpret_cast<internal_element_type const*>(mmap_manager.mmapped_buffer_);
	if(data->template equal<DestMagic, Dest>()) {
		return **data;
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("The destination type does not match the type, version or magic in the shared-memory region. Destination type: '{}', internal destination type: '{}', hash_code of internal destination type={}, git-SHA of destination libjmmcg=" LIBJMMCG_GIT_SHA_STR ", magic of destination libjmmcg={}, details of source libjmmcg: '{}'", boost::core::demangle(typeid(Dest).name()), boost::core::demangle(typeid(internal_element_type).name()), typeid(internal_element_type).hash_code(), DestMagic, data->to_string()), static_cast<Dest const& (shared_mem::*)() const>(&shared_mem::get<DestMagic, Dest>)));
	}
}

template<shared_mem::magic_type DestMagic, class Dest>
inline Dest&
shared_mem::get() noexcept(false) {
	using internal_element_type= allocator_type::internal_shm_wrapper<Dest>;
	internal_element_type* data= reinterpret_cast<internal_element_type*>(mmap_manager.mmapped_buffer_);
	if(data->template equal<DestMagic, Dest>()) {
		return **data;
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("The destination type does not match the type, version or magic in the shared-memory region. Destination type: '{}', internal destination type: '{}', hash_code of internal destination type={}, git-SHA of destination libjmmcg=" LIBJMMCG_GIT_SHA_STR ", magic of destination libjmmcg={}, details of source libjmmcg: '{}'", boost::core::demangle(typeid(Dest).name()), boost::core::demangle(typeid(internal_element_type).name()), typeid(internal_element_type).hash_code(), DestMagic, data->to_string()), static_cast<Dest& (shared_mem::*)()>(&shared_mem::get<DestMagic, Dest>)));
	}
}

template<class T>
inline void
shared_mem::sync(T* addr, sync_flags_t::element_type sync_flags) noexcept(false) {
	sync(reinterpret_cast<pointer>(addr), sizeof(T), sync_flags);
}

}}
