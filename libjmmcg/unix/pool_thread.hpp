/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace pool { namespace private_ { namespace thread_types {

template<class PTT, class QM>
class steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, PTT, QM> final : public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<QM>::exit_requested_type> {
public:
	using queue_model= QM;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<QM>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<QM>::container_type;
	using statistics_type= typename PTT::template statistics_type<QM>;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	constexpr __stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true) {}

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(true) override {
		return thread_traits::api_params_type::states::no_kernel_thread;
	}
};

template<class PTT>
class steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT, pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	using batch_type= typename PTT::template pool_thread_queue_details<queue_model>;
	using statistics_type= typename batch_type::statistics_type;
	using signalled_work_queue_type= typename batch_type::container_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	__stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true);
	~steal() noexcept(false);

	bool __fastcall add_work_to_batch(typename signalled_work_queue_type::value_type&& wk);
	bool __fastcall process_a_batch_item(typename os_traits::thread_exception const& exception_thrown_in_thread);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return batch.statistics();
	}

private:
	batch_type batch;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

template<class PTT>
class steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT, pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>>::exit_requested_type> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	using container_type= typename PTT::template pool_thread_queue_details<queue_model>;
	using statistics_type= typename container_type::statistics_type;
	using signalled_work_queue_type= typename container_type::container_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	explicit steal(exit_requested_type&) noexcept(true);
	~steal() noexcept(false);

	bool __fastcall push_front(typename signalled_work_queue_type::value_type&& wk);
	bool __fastcall process_a_batch_item(typename os_traits::thread_exception const& exception_thrown_in_thread);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return work.statistics();
	}

private:
	container_type work;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

template<class PTT>
class slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, PTT> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type>,
	  public sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::lock_traits> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::lock_traits>::value_type value_type;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<queue_model>::container_type;
	using statistics_type= typename PTT::template pool_thread_queue_details<queue_model>::statistics_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	using deleter_t= default_delete<slave>;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type&) noexcept(true)
		: base_t(exit_requested) {
	}
	~slave() noexcept(true) {}

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(true) override {
		return thread_traits::api_params_type::states::no_kernel_thread;
	}
};

template<class PTT>
class slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type>,
	  public sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::lock_traits> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::lock_traits>::value_type value_type;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<queue_model>::container_type;
	using statistics_type= typename PTT::template pool_thread_queue_details<queue_model>::statistics_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	using deleter_t= default_delete<slave>;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type&& wk) noexcept(true);
	~slave() noexcept(true);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	typename signalled_work_queue_type::value_type some_work;
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

/**
	You can report back exceptions from this thread wrapper type. Oh - and make sure you construct the execution_context too (because you get the exceptions through that type).
*/
template<class PTT, class QM>
class steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, PTT, QM> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<QM>::exit_requested_type> {
public:
	using queue_model= QM;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<QM>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<QM>::container_type;
	using statistics_type= typename signalled_work_queue_type::statistics_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	constexpr __stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true) {}

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(true) override {
		return thread_traits::api_params_type::states::no_kernel_thread;
	}
};

/**
	You can report back exceptions from this thread wrapper type. Oh - and make sure you construct the execution_context too (because you get the exceptions through that type).
*/
template<class PTT>
class steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT, pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	using base_t= pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type>;
	using os_traits= typename base_t::os_traits;
	using model_type= typename os_traits::thread_traits::model_type;
	using lock_traits= typename os_traits::lock_traits;
	using thread_traits= typename os_traits::thread_traits;
	using batch_type= typename PTT::template pool_thread_queue_details<queue_model>;
	using statistics_type= typename batch_type::statistics_type;
	using signalled_work_queue_type= typename batch_type::container_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	__stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true);
	~steal() noexcept(false);

	bool __fastcall add_work_to_batch(typename signalled_work_queue_type::value_type&& wk);
	bool __fastcall process_a_batch_item(typename os_traits::thread_exception const& exception_thrown_in_thread);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return batch.statistics();
	}

private:
	batch_type batch;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

/**
	You can report back exceptions from this thread wrapper type. Oh - and make sure you construct the execution_context too (because you get the exceptions through that type).
*/
template<class PTT>
class steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT, pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>>::exit_requested_type> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	using container_type= typename PTT::template pool_thread_queue_details<queue_model>;
	using statistics_type= typename container_type::statistics_type;
	using signalled_work_queue_type= typename container_type::container_type;
	using exit_requested_type= typename base_t::exit_requested_type;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	explicit steal(exit_requested_type&) noexcept(true);
	steal(steal&&) noexcept(true);
	~steal() noexcept(false);

	bool __fastcall push_front(typename signalled_work_queue_type::value_type&& wk);
	bool __fastcall process_a_batch_item(typename os_traits::thread_exception const& exception_thrown_in_thread);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return work.statistics();
	}

private:
	container_type work;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

/**
	\todo JMG: Looks like this needs completing, as some fns are undefined! Maybe not used...
*/
template<class PTT>
class slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, PTT> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type>,
	  public sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::lock_traits> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::lock_traits>::value_type value_type;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<queue_model>::container_type;
	using statistics_type= typename PTT::template statistics_type<queue_model>;
	using exit_requested_type= typename base_t::exit_requested_type;
	using deleter_t= default_delete<slave>;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type&& wk) noexcept(true)
		: base_t(exit_requested), some_work(wk) {
	}
	~slave() noexcept(true) {}

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	typename signalled_work_queue_type::value_type some_work;
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(true) override {
		return thread_traits::api_params_type::states::no_kernel_thread;
	}
};

template<class PTT>
class slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, PTT> final
	: public pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>::exit_requested_type>,
	  public sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::lock_traits> {
public:
	using queue_model= pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue;
	typedef pool_thread<thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>, typename PTT::template pool_thread_queue_details<queue_model>::exit_requested_type> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename sp_counter_type<long, thread_os_traits<generic_traits::api_type::posix_pthreads, heavyweight_threading>::lock_traits>::value_type value_type;
	typedef typename os_traits::thread_traits thread_traits;
	using signalled_work_queue_type= typename PTT::template pool_thread_queue_details<queue_model>::container_type;
	using statistics_type= typename PTT::template statistics_type<queue_model>;
	using exit_requested_type= typename base_t::exit_requested_type;
	using deleter_t= default_delete<slave>;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type&& wk) noexcept(true);
	~slave() noexcept(true);

	statistics_type const& __fastcall statistics() const noexcept(true) {
		return statistics_;
	}

private:
	typename signalled_work_queue_type::value_type some_work;
	statistics_type statistics_;

	typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

}}}}}}

#include "pool_thread_impl.hpp"
