/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/basic_exception.hpp"
#include "core/yet_another_enum_wrapper.hpp"

#include "unix/scheduler_priorities.hpp"

#include <atomic>
#include <cerrno>
#include <limits>

#include <pthread.h>
#include <unistd.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/// A constant, suitably defined to be the appropriate API for the platform on which the code is compiled, to allow one to be platform-agnostic.
inline constexpr const generic_traits::api_type::element_type platform_api= generic_traits::api_type::posix_pthreads;

/// Wrap up the PThreads details regarding creating a thread, so that it is normalised for the rest of the library.
template<>
class thread_params<generic_traits::api_type::posix_pthreads> {
public:
	typedef size_t stack_size_type;

	static inline constexpr const generic_traits::api_type::element_type api_type= generic_traits::api_type::posix_pthreads;
	static inline const stack_size_type max_stack_size= static_cast<stack_size_type>(PTHREAD_STACK_MIN) << 14;	 ///< The large stack size, about 66Mb.

	/**
		Pthreads doesn't allow threads to be created suspended, only running.
	*/
	enum creation_flags {
		create_running
	};

	/**
		Note that these states are in a specific order - the higher the number, the more severe any error.
	*/
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(states, std::uint64_t, (no_kernel_thread, 0),	///< This is not a failure - the thread may not be started yet, or may have exited.
		(suspended, no_kernel_thread + 1),
		(active, EBUSY),

		// Error conditions now...
		(thread_id_not_found, ESRCH),
		(invalid_thread, EINVAL),
		(deadlocked_with_another_thread, EDEADLK),
		(get_exit_code_failure, deadlocked_with_another_thread + 1),
		(failed_to_cancel, get_exit_code_failure + 1),
		(null_this_pointer, failed_to_cancel + 1),
		(stl_exception, null_this_pointer + 1),
		(unknown_exception, stl_exception + 1),
		(terminated, unknown_exception + 1),

		(activating, terminated + 1),
		(exiting, activating + 1),

		(cancelled, static_cast<std::uint64_t>(-1LL)),	 ///< Must be equivalent to PTHREAD_CANCELED (which cannot be used here, as it uses a reinterpret_cast).

		(unknown, std::numeric_limits<std::uint64_t>::max()));
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(thread_cancel_state, int, (cancel_enable, PTHREAD_CANCEL_ENABLE), (cancel_disable, PTHREAD_CANCEL_DISABLE));

	LIBJMMCG_MAKE_ENUM_TAG_VALUES(detach_state_type, int, (detached, PTHREAD_CREATE_DETACHED), (joinable, PTHREAD_CREATE_JOINABLE));

	class attr {
	public:
		typedef pthread_attr_t element_type;

		attr(const detach_state_type::element_type detach_state, const stack_size_type stack_size, const int scope) noexcept(false) {
			int err= ::pthread_attr_init(&attr_);
			if(err != 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>("Unable to initialize the pthread attr object.", "pthread_attr_init", this));
			}
			err= ::pthread_attr_setdetachstate(&attr_, detach_state);
			if(err != 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>(fmt::format("Unable to set the pthread detached state to={}.", detach_state_type::to_string(detach_state)), "pthread_attr_setdetachstate", this));
			}
			if(stack_size == 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>(fmt::format("Unable to set the pthread stack-size to={}.", stack_size), "pthread_attr_setstacksize", this));
			}
			err= ::pthread_attr_setstacksize(&attr_, stack_size);
			if(err != 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>(fmt::format("Unable to set the pthread stack-size to={}.", stack_size), "pthread_attr_setstacksize", this));
			}
			err= ::pthread_attr_setscope(&attr_, scope);
			if(err != 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>(fmt::format("Unable to set the pthread scope to={}.", scope), "pthread_attr_setscope", this));
			}
			err= ::pthread_attr_setinheritsched(&attr_, PTHREAD_EXPLICIT_SCHED);
			if(err != 0) {
				BOOST_THROW_EXCEPTION(throw_crt_exception<std::runtime_error>("Unable to set the pthread scheduling to=PTHREAD_EXPLICIT_SCHED.", "pthread_attr_setinheritsched", this));
			}
		}

		attr(const attr& a) noexcept(true) {
			[[maybe_unused]] int err= ::pthread_attr_init(&attr_);
			DEBUG_ASSERT(err == 0);
			int val;
			err= ::pthread_attr_getdetachstate(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setdetachstate(&attr_, val);
			DEBUG_ASSERT(err == 0);
			stack_size_type size;
			err= ::pthread_attr_getstacksize(&a.attr_, &size);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setstacksize(&attr_, size);
			DEBUG_ASSERT(err == 0);
			err= pthread_attr_getscope(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setscope(&attr_, val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setinheritsched(&attr_, PTHREAD_EXPLICIT_SCHED);
			DEBUG_ASSERT(err == 0);
		}

		explicit attr(const element_type& a) noexcept(true) {
			[[maybe_unused]] int err= pthread_attr_init(&attr_);
			DEBUG_ASSERT(err == 0);
			int val;
			err= ::pthread_attr_getdetachstate(&a, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setdetachstate(&attr_, val);
			DEBUG_ASSERT(err == 0);
			stack_size_type size;
			err= ::pthread_attr_getstacksize(&a, &size);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setstacksize(&attr_, size);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_getscope(&a, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setscope(&attr_, val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setinheritsched(&attr_, PTHREAD_EXPLICIT_SCHED);
			DEBUG_ASSERT(err == 0);
		}

		~attr(void) noexcept(true) {
			[[maybe_unused]] const int err= ::pthread_attr_destroy(&attr_);
			DEBUG_ASSERT(err == 0);
		}

		attr& operator=(const attr& a) noexcept(true) {
			int val;
			[[maybe_unused]] int err= ::pthread_attr_getdetachstate(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setdetachstate(&attr_, val);
			DEBUG_ASSERT(err == 0);
			stack_size_type size;
			err= ::pthread_attr_getstacksize(&a.attr_, &size);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setstacksize(&attr_, size);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_getscope(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setscope(&attr_, val);
			DEBUG_ASSERT(err == 0);
			err= ::pthread_attr_setinheritsched(&attr_, PTHREAD_EXPLICIT_SCHED);
			DEBUG_ASSERT(err == 0);
			return *this;
		}

		attr& operator=(const creation_flags) noexcept(true) {
			return *this;
		}

		operator const pthread_attr_t*() const noexcept(true) {
			return &attr_;
		}

		operator pthread_attr_t*() noexcept(true) {
			return &attr_;
		}

		stack_size_type stack_size() const noexcept(true) {
			stack_size_type sz= 0;
			[[maybe_unused]] const int err= ::pthread_attr_getstacksize(&attr_, &sz);
			DEBUG_ASSERT(err == 0);
			return sz;
		}

		detach_state_type::element_type detach_state() const noexcept(true) {
			detach_state_type::element_type st;
			[[maybe_unused]] const int err= ::pthread_attr_getdetachstate(&attr_, reinterpret_cast<int*>(&st));
			DEBUG_ASSERT(err == 0);
			return st;
		}

		friend inline tostream& operator<<(tostream& os, attr const& a) noexcept(false) {
			const detach_state_type::element_type detach_st= a.detach_state();
			os << _T("Detached state=") << detach_state_type::to_string(detach_st)
				<< _T(", stack size=") << a.stack_size();
			int val;
			[[maybe_unused]] int err= ::pthread_attr_getscope(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			os << _T(", scope=") << (val == PTHREAD_SCOPE_SYSTEM ? _T("system") : _T("unknown"));
			err= ::pthread_attr_getinheritsched(&a.attr_, &val);
			DEBUG_ASSERT(err == 0);
			os << _T(", scheduling=") << (val == PTHREAD_INHERIT_SCHED ? _T("inherit") : (val == PTHREAD_EXPLICIT_SCHED ? _T("explicit") : _T("unknown")));
			return os;
		}

	private:
		element_type attr_;
	};

	typedef pthread_t handle_type;
	typedef pid_t pid_type;
	typedef pthread_t tid_type;
	using username_type= std::array<char, L_cuserid>;

	typedef unsigned long suspend_count;
	typedef unsigned long suspend_period_ms;

	typedef void* security_type;	 // Not implemented on Posix.

	class processor_mask_type {
	public:
		typedef cpu_set_t element_type;	 // Not implemented on Posix.

		__stdcall processor_mask_type() noexcept(true) {
			CPU_ZERO(&mask_);
		}

		explicit __stdcall processor_mask_type(std::size_t i) noexcept(true)
			: cpu_(i) {
			CPU_ZERO(&mask_);
			CPU_SET(cpu_, &mask_);
		}

		element_type const& __fastcall mask() const noexcept(true) {
			return mask_;
		}

		element_type& __fastcall mask() noexcept(true) {
			return mask_;
		}

		std::size_t get_cpu() const noexcept(true) {
			return cpu_;
		}

		friend inline tostream& __fastcall
		operator<<(tostream& os, processor_mask_type const& m) noexcept(false) {
			os << CPU_COUNT(&m.mask_) << ", " << m.cpu_;
			return os;
		}

	private:
		element_type mask_;
		const std::size_t cpu_{};
	};

	static processor_mask_type all_cpus_mask() noexcept(true) {
		processor_mask_type res;
		DEBUG_ASSERT(std::thread::hardware_concurrency() <= CPU_SETSIZE);
		for(unsigned int i= 0; i < std::thread::hardware_concurrency(); ++i) {
			CPU_SET(i, &res.mask());
		}
		[[maybe_unused]] int const registered_cpus= CPU_COUNT(&res.mask());
		DEBUG_ASSERT(registered_cpus > 0);
		DEBUG_ASSERT(static_cast<unsigned int>(registered_cpus) == std::thread::hardware_concurrency());
		return res;
	}

	typedef void* core_work_fn_ret_t;
	typedef void* core_work_fn_arg_t;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"

	typedef core_work_fn_ret_t(__cdecl core_work_fn_type)(core_work_fn_arg_t);

#pragma GCC diagnostic pop

	typedef core_work_fn_arg_t arglist_type;
	typedef attr initflag_type;

	enum priority_type {
		idle= private_::scheduler_min_max_priorities::scheduler_min_priority,
		normal= (private_::scheduler_min_max_priorities::scheduler_max_priority + private_::scheduler_min_max_priorities::scheduler_min_priority) / 2,
		time_critical= private_::scheduler_min_max_priorities::scheduler_max_priority,
		unknown_priority
	};

	core_work_fn_type* work_fn{};
	arglist_type arglist{};
	initflag_type initflag;

	/**
		Warning - this is not a valid parameter to check to identify if the managed thread has exited. On the other hand, the state parameter is.
	*/
	tid_type id{};
	std::atomic<states::element_type> state{states::no_kernel_thread};	///< The Pthread API doesn't allow one to call pthread_join() on a thread, successfully, more than once, so we need to store the state here to ensure that if we've called it successfully, the state is correctly stored, and we can return the appropriate stored state upon subsequent calls.

	thread_params() noexcept(false)
		: initflag(detach_state_type::joinable, static_cast<stack_size_type>(PTHREAD_STACK_MIN), PTHREAD_SCOPE_SYSTEM) {
		DEBUG_ASSERT(states::cancelled == reinterpret_cast<states::underlying_type_t>(PTHREAD_CANCELED));
	}

	/**
		\param sa	A pointer to a function that will called by the OS-specific thread.
		\param ss	We set the stack size by default to be "big enough". Well, really I haven't a clue what this should be, as it is down to how the library is used. If you get bizarre SIGSEGV failures whilst constructing objects that should "just work", then you might need to set this value larger.

		\todo Have a better way of setting the stack size, rather than magically, here.
	*/
	explicit thread_params(core_work_fn_type* const sa, const security_type= nullptr, const stack_size_type ss= max_stack_size) noexcept(false)
		: work_fn(sa), arglist(), initflag(detach_state_type::joinable, ss, PTHREAD_SCOPE_SYSTEM) {
		DEBUG_ASSERT(work_fn);
		DEBUG_ASSERT(states::cancelled == reinterpret_cast<states::underlying_type_t>(PTHREAD_CANCELED));
	}

	thread_params(const thread_params& tp) noexcept(true)
		: work_fn(tp.work_fn), arglist(tp.arglist), initflag(tp.initflag), id(tp.id), state(tp.state.load()) {
	}

	stack_size_type stack_size() const noexcept(true) {
		return initflag.stack_size();
	}

	detach_state_type::element_type detach_state() const noexcept(true) {
		return initflag.detach_state();
	}

	const tstring to_string() const noexcept(false) {
		tostringstream ss;
		ss << _T("API type: ") << generic_traits::api_type::to_string(api_type)
			<< _T(", max. stack size=0x") << std::hex << max_stack_size
			<< _T(", work function ptr=0x") << std::hex << work_fn
			<< _T(", argument list=0x") << std::hex << arglist
			<< _T(", attributes: ") << initflag
			<< _T(", ID=0x") << std::hex << id
			<< _T(", state=") << states::to_string(state);
		return ss.str();
	}
};

/**
 * \todo Relace with LIBJMMCG_MAKE_ENUM?
 *
 * See <a href="https://stackoverflow.com/questions/77751341/fmt-failed-to-compile-while-formatting-enum-after-upgrading-to-v10-and-above#77751342/>
 */
inline auto
format_as(typename thread_params<generic_traits::api_type::posix_pthreads>::priority_type t) {
	return fmt::underlying(t);
}

}}}
