/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

// 4284 return type for 'X::operator ->' is 'Y *' (ie; not a UDT or reference to a UDT.  Will produce errors if applied using infix notation)
#pragma warning(disable:4284)
// 4786 identifier was truncated to '255' characters in the browser information
#pragma warning(disable:4786)
// 4710 function '...' not inlined
#pragma warning(disable:4710)
// 4514 unreferenced inline function has been removed
#pragma warning(disable:4514)

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#ifdef ODBC_EXPORTS
#define ODBC_DECLSPEC __declspec(dllexport)
#define ODBC_EXTERN
#else
#define ODBC_DECLSPEC __declspec(dllimport)
#define ODBC_EXTERN extern
#endif

#include<windows.h>
#include<typeinfo.h>
#include<sql.h>
#include<sqlext.h>
// This header file is not really required, but I leave it here as a reminder that it exists
// and is strictly part of the ODBC API. Basically it declares bcp functionality, and
// distributed server functionality. I don't include it to save compilation time.
//#include <odbcss.h>
#include<tchar.h>
#include<cassert>
#include<map>
#include<string>
#include<sstream>
#include<typeinfo.h>
#include<vector>

namespace jmmcg {

	typedef std::basic_string<TCHAR> tstring;
	typedef std::basic_ostream<TCHAR> tostream;
	typedef std::basic_stringstream<TCHAR> tstringstream;

	// Why like this? Because otherwise we'd have the code too far to the right.
	namespace NTUtils { namespace Database {

	/**
		For details on the ODBC SDK see the following link in the MSDN (assuming the Jan 2000
		MSDN is installed):

			"mk:@MSITStore:D:\devtools\Microsoft%20Visual%20Studio\MSDN\2000JAN\1033"

		To see a comparison of ADO/OLE DB/ODBC see (M$ keeps moving these around...):

			<a href="http://msdn.microsoft.com/library/psdk/sql/highprog.htm"/>

		Or:

			ADO:	<a href="http://msdn.microsoft.com/library/en-us/architec/8_ar_ad_4zqn.asp?frame=true"/>
				Excerpt from page:
					"...ADO is the API most recommended for general-purpose data access to SQL Server for these reasons:
						- ADO is easy to learn and program.
						- ADO has the feature set required by most general-purpose applications.
						- ADO enables programmers to quickly produce robust applications..."

			ODBC:	<a href="http://msdn.microsoft.com/library/en-us/architec/8_ar_ad_86lf.asp?frame=true"/>
				Excerpt from page:
					"...ODBC can be used in tools, utilities, or system level development needing either top performance or access to SQL Server features. These features include:
						- A set of bulk copy functions based on the earlier DB-Library bulk copy functions.
						- Extensions to the ODBC diagnostic functions and records to get SQL Server-specific information from messages and errors.
						- A set of functions that exposes catalog information from the linked servers used in SQL Server distributed queries.
						- Various driver-specific attributes and connection string keywords to control SQL Server-specific behaviors."

		Useage details:
		===============

		This ODBC wrapper requires a minimum of ODBC version 3 header files/libs/dlls.
		They must support the following, to quote the MSDN:

			"ODBC aligns with the following specifications and standards that deal with the
			Call-Level Interface (CLI). (The ODBC features are a superset of each of these
			standards.)

			- The X/Open CAE Specification "Data Management: SQL Call-Level Interface (CLI)"
			- ISO/IEC 9075-3:1995 (E) Call-Level Interface (SQL/CLI)

			As a result of this alignment, the following are true:

			- An application written to the X/Open and ISO CLI specifications will work with
			an ODBC 3.x driver or a standards-compliant driver when it is compiled with the
			ODBC 3.x header files and linked with ODBC 3.x libraries, and when it gains
			access to the driver through the ODBC 3.x Driver Manager.
			- A driver written to the X/Open and ISO CLI specifications will work with an
			ODBC 3.x application or a standards-compliant application when it is compiled
			with the ODBC 3.x header files and linked with ODBC 3.x libraries, and when the
			application gains access to the driver through the ODBC 3.x Driver Manager.
			(For more information, see "Standards-Compliant Applications and Drivers
			<odbcstandards_compliant_applications_and_drivers.htm>" in Chapter 17,
			"Programming Considerations.")"

		Further specific details of this layer follow:
		==============================================

		1. You must have the "Platform SDK" installed. (Specifically the ODBC SDK in the
			"Data Access Services\MDAC SDK" books.) You can download the MDAC SDK from the M$
			web site at: "http://www.microsoft.com/Data/download.htm". The MDAC SDK contains
			just the ADO/OLE DB/ODBC SDKs, a sub-set of the Platform SDK.

		2. To run executables created with this code you must install the required ODBC drivers.
			These must conform to the above specification. To get the correct ODBC to SQL server
			drivers, which are included in MDAC there are two ways:

			a) Install them using "mdac_typ.exe" available from:

				<a href="http://www.microsoft.com/data/download.htm"/>

				The minimum version to install is "2.1.2.4202.3 (GA)". Note that this installs:

				- ADO v 2.10.4202.1
				- ODBC v 3.510.4202.0

				Also note that as of MDAC v2.6, ODBC support (amongst others) is no longer included.

			b) MDAC v2.5 is shipped with Windows 2000 & Windows XP. This page:

				"http://support.microsoft.com/support/kb/articles/Q271/9/08.ASP"

				Search for article id "Q271908". I quote from that page:

				"The ODBC Desktop Drivers and Visual FoxPro ODBC driver, which no longer ship
				with MDAC 2.6, are shipped with the Microsoft Windows 2000 operating system
				(which contains MDAC 2.5). Microsoft Windows Millennium Edition (Me) also ships
				with MDAC 2.5.

				Microsoft Windows XP also contains the ODBC Desktop Drivers and Visual FoxPro
				Driver. For Microsoft Windows NT and Microsoft Windows 9x, MDAC 2.5 or an
				earlier version will need to be installed to obtain these drivers."


			Amongst other things. (See the release manifest at:
			<a href="http://www.microsoft.com/data/MDAC21info/MDAC21sp2manifest.htm"/>.)

		3. The exceptions all inherit from the "ODBCExceptionErr" class, which can be used as
			a filter to catch all exceptions thrown by this layer.

		4. The "Environment" class is pretty useless, apart from the fact that it hides a
			lot of the messy allocation and deallocation details of accessing the ODBC drivers.
			This class is derived from to be actually useful, despite the fact that it is
			concrete. Note that this class checks to ensure the ODBC layer is the correct version.
			Which must be ODBC version 3.

		5. The "Connection" class wraps a few more messy ODBC details regarding setting
			various connection based attributes for read only or write-only connections.
			Again this class is only really useful if derived from. By default this class enables
			connection pooling. Note that DBMS-specific translation on SQL strings is turned off
			by this class for speed. Hence only ANSI SQL is allowed.

		6. The "RORecordset" class. This is a fairly trivial class that gets the data from the
			data base one element at a time. It was created mainly to be able to implement the
			loathed "count()" more memory efficiently than using the "ROBulkRecordset". Tests reveal
			that this class is over 10% slower than the "ROBulkRecordset" class for a given SQL
			statement. So basically only use it if you want a single row with a single column
			as the result of the SQL. For any other case don't use it, use it's big brother.

		7. The "ROBulkRecordset" class. Now we're getting to the meat of this layer.
			This class is for read only (i.e. no SQL "update", "insert" or "delete" statements)
			data base manipulations. It attempts to mimick a real recordset, at which it is only
			possibly partially successful. It does not require an ODBC connection to be in scope
			as it creates and manages its own. The reference to "bulk" in the name means that
			the class has the ability to pre-fetch a pre-defined quantity of rows from the data
			base at a time. (Calls to "moveNext()" only get more data from the data base when the
			internal buffer is exhausted.) In general it has been found that optimium values for
			the internal buffer size are either "1" (one) or >100. Some testing may be required
			to determine the exact value, which will be dependant upon the process, the data base
			server, the network, and the dynamic profiles of these.

		8. The "WOConnection" class. This is used to write data to the data base. It is implemented as
			write-only, but this is not a limitation of ODBC, only the wrapper. The "count()" function
			returns the number of records affected by the update. (Note well the comments about
			"count()" in "RecordsetBase".) Also it is perfectly acceptable to have multiple
			"WOConnection"s in scope with the same data base connection string. Further it is acceptable
			to have multiple transactions (they are wrapped within the "WOConnection" class) active
			at any one time, even on the same table. But obviously not the same row... The
			"WOConnection" class checks to ensure that multiple transaction support is supported by the
			data base driver. (This is part of the ODBC v2.0 standard.)

		9. The "WOBulkConnection" class. This is also used to write data to the data base. It prepares
			the SQL statement before execution. (Hence the "execute()" implementation does not take an
			argument.) Also you can optionally bind the arguments the SQL needs, if it is parameterised.
			Note that if you bind a "tstring" this MUST be sized correctly before binding. i.e. the
			variable must be declared as "tstring param(50, '\0');" where "50" is at least the size
			of the string in the data base, and '\0' is the value the string is initialised to. ('\0', or
			NULL is the fastest value to use.) Otherwise you'll get exceptions thrown at run-time when
			committing the SQL. Note that this class does similar multiple transaction support checks,
			etc, as per the "WOConnection" class.

		Performance vs. ADO:
		====================

		In all cases the core loop has been designed to be such that the rate determining step is the
		data base activity. Also all values are quoted to within 10% error (average deviation).
		In all read cases an entire row of the calls table was returned.

		1. "RORecordset": ~50% faster than ADO.
		2. "ROBulkRecordset":
			- 1 row:       3x faster.
			- 10 rows:     20% slower.
			- 100 rows:    same speed as ADO.
			- 1000 rows:   3x faster.
			- >1000 rows:  varies depending upon returned data. If the data is the full row of a calls
								table, as 1000. If the data is an arbitrary number of non-nullable "long"s,
								up to 100x faster. (i.e. so fast the data base is no longer the limiting
								factor, but the memory copies to get the data out of the recordset.)
			The constructor for the "ROBulkRecordset" can take a argument of the number of rows it is
			to fetch at a time. By default this is set to "1" (one), as reasonable & fast default that
			can only be beaten by careful tweaking.
		3. "WOConnection": ~50% faster than ADO.
		4. "WOBulkConnection": About the same speed as "WOConnection" for simple SQL. Untested for more
			complex SQL.
		5. Indexing the elements in a row. Testing has revealed that indexing an element of a
			row by the name is around 40% slower than using the *correct* column number. If your SQL
			returns a single column, plainly this can be indexed by number without causing a dependancy
			on the data base table design. (And is recommended.) (Specifically the order in which rows
			are returned.) If more than one column is returned and indexing by column number is used,
			a dependancy on the data base table design is introduced, which may not be acceptable.

		As a matter of interest the ODBC layer (thus ADO) cannot transfer a single character into
		an SQL table with a column of type "char" size 1. It has to make a string out of this. Thus
		it would be better to use a "tinyint" of size 1, precision 3 and cast it. (This is equivalent
		to the C data type "char". (Also the ODBC layer has to do a lot more messing around with
		passing the data too, which makes it even worse.)

		Future Improvements:
		====================

		1. Write a prepared "ROBulkRecordset" layer & test its performance.
		2. Improve the error reporting to give even more information.
		3. Implement "item(... , tstring &...)".
		4. Implement "item(...)" for nullable columns. Currently if a nullable element that is "NULL"
			is returned, an ODBC exception of "SQLState 22002: Indicator variable required but not
			supplied" is thrown. This is because the "StrLen_or_IndPtr" parameter is set to "NULL", as
			it is unused.
		5. Resolve the use of maps across a dll boundary, as really slows the stuff down.  (Compiler
			bugs again prevent me from doing this...)
		6. More...?

	*/
	namespace ODBC {

	namespace Exceptions {

		// I *know* that officially you shouldn't derive from "std::exception",
		// (because the destructor is not virtual) but it is just *sooo* handy
		// to derive from it, so that a "catch (std::exception &err) {..."
		// can be done to catch these too. Anyway exceptions *should* be in a
		// hierarchy. Nyyyerrr. It's my code & I'll do what I want!
		class ODBC_DECLSPEC ODBCExceptionErr : public std::exception {
		public:
			virtual inline ~ODBCExceptionErr(void);

			// I'd really like this to be a "const" member function, but, alas,
			// another limitation of "std::exception" is that this function is
			// declared as non-const....
			inline const TCHAR * __fastcall what(void) noexcept(true);

		protected:
			tstringstream m_info;
			std::auto_ptr<TCHAR> str_hack;

			inline ODBCExceptionErr(const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);

		private:
			static inline tstring __fastcall GetODBCERRInfoTimeout(const SQLHANDLE * const hstmt, const SQLSMALLINT type, bool &timed_out);
		};

		class ODBC_DECLSPEC EnvironmentErr : public ODBCExceptionErr {
		public:
			inline EnvironmentErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ~EnvironmentErr(void);

		};

		class ODBC_DECLSPEC ConnectionErr : public ODBCExceptionErr {
		public:
			inline ConnectionErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ConnectionErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ~ConnectionErr(void);
		};

		class ODBC_DECLSPEC RecordsetBaseErr : public ODBCExceptionErr {
		public:
			inline RecordsetBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline RecordsetBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ~RecordsetBaseErr(void);
		};

		class ODBC_DECLSPEC RORecordsetBaseErr : public RecordsetBaseErr {
		public:
			inline RORecordsetBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline RORecordsetBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline RORecordsetBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline RORecordsetBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ~RORecordsetBaseErr(void);
		};

		class ODBC_DECLSPEC WOConnectionBaseErr : public RORecordsetBaseErr {
		public:
			inline WOConnectionBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline WOConnectionBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline WOConnectionBaseErr(const SQLUSMALLINT col_num, const type_info &data,const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline WOConnectionBaseErr(const SQLUSMALLINT col_num, const type_info &data,const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err);
			inline ~WOConnectionBaseErr(void);
		};

	}

	class ODBC_DECLSPEC Environment {
	public:
		inline Environment(const bool read_only = true, const SQLUINTEGER timeout=15);
		virtual inline ~Environment(void) noexcept(true);

	protected:
		SQLHDBC hdbc;

	private:
		SQLHENV henv;

		// Prevent copying or assignment.
		inline Environment(const Environment &) noexcept(true) {DEBUG_ASSERT(false);}
		inline Environment & __fastcall operator=(const Environment &) noexcept(true) {DEBUG_ASSERT(false);return *this;}
	};

	class ODBC_DECLSPEC Connection : protected Environment {
	public:
		inline Connection(const tstring &cnx, const bool read_only, const SQLUINTEGER timeout=15);
		inline ~Connection(void) noexcept(true);
		inline const tstring &__fastcall cnx_string() const noexcept(true) {return cnx_str;}
		virtual inline void __fastcall execute(const tstring &sql);
		virtual inline void __fastcall execute(const tstringstream &sql) {execute(sql.str());}

	protected:
		const SQLUINTEGER stmt_timeout;
		SQLHSTMT hstmt;

		inline void __fastcall AllocateStmt();

	private:
		tstring cnx_str;

		// Prevent assignment.
		inline Connection & __fastcall operator=(const Connection &) noexcept(true) {DEBUG_ASSERT(false);return *this;}

	};

	class ODBC_DECLSPEC RecordsetBase : public Connection {
	public:
		inline RecordsetBase(const tstring &cnx, const bool read_only, const SQLUINTEGER timeout=15);
		inline ~RecordsetBase() noexcept(true);
		inline void __fastcall execute(const tstring &sql);
		// Return the number of rows affected by the previously executed SQL.
		// The return value of this is either:
		// 1. "-1" signifying that the count could not be obtained (possibly due to limitiations
		//    imposed by the driver, cursor, or SQL statement.
		// 2. An interger >=0, signifying the number of rows affected by the SQL statement.
		//    (Which may be non-obvious due to the way the ODBC drivers handle compound SQL
		//    statements, e.g. "select * from table1;select * from table2".)
		// Using this function with "select"s SQL statements implies re-executing the SQL
		// statemenet (suitably mangled), see comments below.
		// Using it with "insert/update/delete" SQL statemenets before the statement is executed
		// is meaningless (and will return "-1"). After the statement is executed it will return
		// a (possibly different) value - see above.
		// NOTE: Non-const to allow later overriding of this function. This is because the
		// later implementations of "count()" imply doing tricky things with "row_count". Which
		// therefore makes the object non-const. O.k. "row_count" could be "mutable", but in
		// this case as we are doing quite a bit of work in the "count()" function, and I don't
		// like it (the "count()" function), it isn't "mutable".
		virtual inline SQLINTEGER __fastcall count() noexcept(true) {return row_count;}

	protected:
		SQLINTEGER row_count;

		inline void __fastcall RowsAffected();

	private:
		bool allocated_cursor;

		// Prevent assignment.
		inline RecordsetBase & __fastcall operator=(const RecordsetBase &) noexcept(true) {DEBUG_ASSERT(false);return *this;}
	};

	// The base read-only recordset object.
	class ODBC_DECLSPEC RORecordsetBase : public RecordsetBase {
	public:
		inline RORecordsetBase(const tstring &cnx, const SQLUINTEGER timeout=15) throw (Database::ODBC::Exceptions::ODBCExceptionErr &, std::bad_alloc);
		inline ~RORecordsetBase(void) noexcept(true);
		inline const tstring & __fastcall sql_string(void) const noexcept(true) {return sql_statement;}
		inline void __fastcall execute(const tstring &sql);
		virtual inline void __fastcall moveNext(void) = 0;
		// Testing shows that implementing this function here gives around a 2% speed improvement,
		// depite claiming to be "inline"d...
		inline bool __fastcall eof() const noexcept(true) {return no_more_data;}
		// NOTE:
		// This function is really horrid. Why?
		// 1. Tests have shown that SQL executed after a count suffers a performance loss of around 10%, even
		//    excluding the time it takes to do a count. (!)
		// 2. The returned value is not guaranteed to be accurate. Why? The rows aren't locked, so if someone
		//    deletes a load of rows after doing a count, the value *will* be inaccurate. So don't use it as
		//    a critical counter to count over all of the values in an ODBC result set, otherwise you may.get
		//    unpredicatable results.
		// 3. You can't use this count with a SQL statement that includes "order by". It *will* break and throw
		//    an exception. e.g. "select id from types order by id" is a *total* no-no.
		inline SQLINTEGER __fastcall count();
		// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
		// Hence no "inline".
		SQLSMALLINT __fastcall GetColumnIdx(const tstring &col_name) noexcept(true);

	protected:
		std::map<tstring, SQLSMALLINT> column_names;
		bool no_more_data;

		// Testing shows that implementing this function here gives around a 2% speed improvement,
		// depite claiming to be "inline"d...
		inline SQLSMALLINT __fastcall NumColumns(void) const noexcept(true) {return num_cols;}

	private:
		tstring sql_statement;
		SQLSMALLINT num_cols;

		// Prevent assignment.
		inline RORecordsetBase & __fastcall operator=(const RORecordsetBase &) noexcept(true) {DEBUG_ASSERT(false);return *this;}
	};

	// A plain-old boring implementation that doesn't use anything clever like bulk row caching.
	// It is perfectly acceptable to use the same "RORecordset" with different SQL statements
	// passed in by "execute(...)".
	class ODBC_DECLSPEC RORecordset : public RORecordsetBase {
	public:
		inline RORecordset(const tstring &cnx, const SQLUINTEGER timeout=15);
		inline ~RORecordset(void) noexcept(true);
		inline void __fastcall execute(const tstring &sql);
		inline void __fastcall moveNext(void);

		template<class T> inline void __fastcall item(const SQLUSMALLINT col_num, T &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			const SQLRETURN ret=SQLGetData(hstmt, col_num, SQL_C_DEFAULT, &data, sizeof(T), NULL);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}
		template<> inline void __fastcall item(const SQLUSMALLINT col_num, tstring &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Getting 'tstrings' from the statement handle is unsupported, use an array of 'char' or 'wchar' that is big enough instead."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			const SQLRETURN ret=SQLGetData(hstmt, col_num, SQL_C_CHAR, data.begin(), data.size(), NULL);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

		template<> inline void __fastcall item(const SQLUSMALLINT col_num, long &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			const SQLRETURN ret=SQLGetData(hstmt, col_num, SQL_C_SLONG, &data, sizeof(long), NULL);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

		template<> inline void __fastcall item(const SQLUSMALLINT col_num, char &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			// Use 'SQL_C_DEFAULT' so that this function will work for any 8-bit sized SQL type.
			const SQLRETURN ret=SQLGetData(hstmt, col_num, SQL_C_CHAR, &data, sizeof(char), NULL);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

		template<class T> inline void __fastcall item(const tstring &col_name, T &data) const {
			item(GetColumnIdx(col_name), data);
		}

	private:
		void operator=(const RORecordset &)=delete;
	};

	// This class declares an object that can retrieve row(s) of data from a data base.
	// It doesn't merely get the individual column of a row at a time.
	// Note that is specifically read-only, so only supports SQL "select" statements.
	// Any SQL that has "update", "delete" or "insert" in it will cause the object to
	// throw an error condition. Use the write record set object for when you want to modify
	// the data base.
	// It is perfectly acceptable to use the same "RORecordset" with different SQL statements
	// passed in by "execute(...)".
	class ODBC_DECLSPEC ROBulkRecordset : public RORecordsetBase {
	public:
		inline ROBulkRecordset(const tstring &cnx, const SQLUINTEGER row_arr_sz=1, const SQLUINTEGER timeout=15);
		inline ~ROBulkRecordset() noexcept(true);
		inline void __fastcall execute(const tstring &sql);
		inline void __fastcall moveNext();
		inline SQLUINTEGER __fastcall NumRowsInDataBlock(void) const noexcept(true) {return row_buffer_size;}

		template<class T> inline void __fastcall item(const SQLUSMALLINT col_num, T &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			if (RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS || c.RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS_WITH_INFO) {
				memcpy(&data, GetRowElement(col_num), sizeof(T));
			}
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
		}

		template<> inline void __fastcall item(const SQLUSMALLINT col_num, tstring &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			if (RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS || RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS_WITH_INFO) {
				data=reinterpret_cast<const TCHAR * const>(GetRowElement(col_num));
			}
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
		}

		template<> inline void __fastcall item(const SQLUSMALLINT col_num, long &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			if (RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS || RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS_WITH_INFO) {
				data=*reinterpret_cast<const long * const>(GetRowElement(col_num));
			}
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
		}

		template<> inline void __fastcall item(const SQLUSMALLINT col_num, char &data) const {
			DEBUG_ASSERT(col_num>=1);
			if (eof()) {
				throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Unexpected EOF in the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
			}
			if (RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS || RowStatusArray.get()[row_buffer_pos]==SQL_ROW_SUCCESS_WITH_INFO) {
				data=*reinterpret_cast<const char * const>(GetRowElement(col_num));
			}
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(col_num, typeid(data), _T("Failed to get the data from the recordset."), sql_string(), &hstmt, SQL_HANDLE_STMT, SQL_ERROR);
		}

		template<class T> inline void __fastcall item(const tstring &col_name, T &data) const {
			item(GetColumnIdx(col_name), data);
		}

	protected:
		typedef unsigned char RowBufferElementType;

		inline void __fastcall AllocateDataBlock();

	private:
		std::auto_ptr<RowBufferElementType> rows_buffer;
		SQLUINTEGER row_buffer_size;
		std::vector<std::pair<size_t, size_t> > cols_sizes;
		SQLUINTEGER TotalColumnSizePtr;
		SQLUINTEGER NumRowsFetched;
		std::auto_ptr<SQLUSMALLINT> RowStatusArray;
		SQLUINTEGER row_buffer_pos;

		inline void __fastcall RawMoveNext();
		inline const RowBufferElementType * const __fastcall GetRowElement(const SQLUSMALLINT col_num) const noexcept(true);

		void operator=(const ROBulkRecordset &)=delete;
	};

	class ODBC_DECLSPEC WOConnectionBase : public RecordsetBase {
	public:
		inline WOConnectionBase(const tstring &cnx, const SQLUINTEGER timeout=15);
		inline ~WOConnectionBase(void) noexcept(true);
		virtual inline void __fastcall commit();
		virtual inline void __fastcall rollback();
		inline SQLINTEGER __fastcall count();
		inline bool __fastcall in_trans() const noexcept(true);

		template<class T> inline void __fastcall BindParam(const SQLUSMALLINT col_num, const SQLSMALLINT inp_or_out, T &data) {
			DEBUG_ASSERT(col_num>=1);
			const SQLRETURN ret=SQLBindParameter(
				hstmt,
				col_num,
				inp_or_out,
				SQL_C_DEFAULT,
				0,
				0,
				0,
				&data,
				sizeof(T),
				NULL
			);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::WOConnectionBase(col_num, typeid(data), _T("Failed to bind the parameter to the column on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

		template<> inline void __fastcall BindParam(const SQLUSMALLINT col_num, const SQLSMALLINT inp_or_out, long &data) {
			DEBUG_ASSERT(col_num>=1);
			const SQLRETURN ret=SQLBindParameter(
				hstmt,
				col_num,
				inp_or_out,
				SQL_C_SLONG,
				SQL_INTEGER,
				0,
				0,
				&data,
				sizeof(long),
				NULL
			);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::WOConnectionBaseErr(col_num, typeid(data), _T("Failed to bind the parameter to the column on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

		template<> inline void __fastcall BindParam(const SQLUSMALLINT col_num, const SQLSMALLINT inp_or_out, tstring &data) {
			DEBUG_ASSERT(col_num>=1);
			str_data_at_exec_type tmp={
				&data,
				SQL_LEN_DATA_AT_EXEC(data.size())
			};
			AddExecData(col_num, tmp);
			const SQLRETURN ret=SQLBindParameter(
				hstmt,
				col_num,
				inp_or_out,
				SQL_C_CHAR,
				SQL_CHAR,
				data.size(),
				0,
				reinterpret_cast<SQLPOINTER>(col_num),
				data.size(),
				&(ExecData(col_num).StrLen_or_IndPtr)
			);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::WOConnectionBaseErr(col_num, typeid(data), _T("Failed to bind the parameter to the column on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
			}
		}

	protected:
		struct str_data_at_exec_type {
			tstring *str;
			SQLINTEGER StrLen_or_IndPtr;
		};
		bool in_transaction;
		std::map<SQLSMALLINT, str_data_at_exec_type> exec_data;

	private:
		// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
		// Hence no "inline".
		void __fastcall AddExecData(const SQLUSMALLINT col_num, const str_data_at_exec_type &data);
		// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
		// Hence no "inline".
		str_data_at_exec_type & __fastcall ExecData(const SQLUSMALLINT col_num);

		void operator=(const WOConnectionBase &)=delete;
	};

	class DBTransaction {
	public:
		inline DBTransaction(WOConnectionBase &cnx) noexcept(true) : wo_cnx(cnx) {
		}
		inline ~DBTransaction(void) throw (Database::ODBC::Exceptions::ODBCExceptionErr *) {
			if (wo_cnx.in_trans()) {
				wo_cnx.rollback();
			}
		}

		inline void __fastcall commit(void) throw (Database::ODBC::Exceptions::ODBCExceptionErr *) {
			// If the "wo_cnx.commit()" is successful, "wo_cnx.in_transaction()" will return false.
			wo_cnx.commit();
		}

	private:
		WOConnectionBase &wo_cnx;

		DBTransaction(const DBTransaction &)=delete;
		void operator=(const DBTransaction &)=delete;
	};

	class ODBC_DECLSPEC WOConnection : public WOConnectionBase {
	public:
		inline WOConnection(const tstring &cnx, const SQLUINTEGER timeout=15);
		inline ~WOConnection() noexcept(true);
		inline void __fastcall execute(const tstring &sql);

	private:
		void operator=(const WOConnection &)=delete;
	};

	class ODBC_DECLSPEC WOBulkConnection : public WOConnectionBase {
	public:
		inline WOBulkConnection(const tstring &cnx, const SQLUINTEGER row_arr_sz=1, const SQLUINTEGER timeout=15);
		inline ~WOBulkConnection() noexcept(true);
		inline const tstring & __fastcall sql_string() const noexcept(true) {return sql_statement;}
		inline void __fastcall execute();
		inline void __fastcall commit();
		inline void __fastcall rollback();
		inline void __fastcall prepare(const tstring &sql);

	private:
		struct sp_behaviour_type {
			bool commit;
			bool rollback;
		};
		sp_behaviour_type sp_behaviour;
		SQLUINTEGER row_buffer_size;
		bool prepared;
		tstring sql_statement;

		// Ensure that the base class "execute(...)" cannot be called, as it is deprecated in this class.
		inline void __fastcall execute(const tstring &) {}

		void operator=(const WOBulkConnection &)=delete;
	};

} } } }

#pragma warning(default:4284)
