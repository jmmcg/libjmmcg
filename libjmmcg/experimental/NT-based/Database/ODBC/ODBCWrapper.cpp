/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include"ODBCWrapper.hpp"
#include<iostream>
#include<memory>
#include<ostream>
#include<sstream>

using namespace libjmmcg;
using namespace NTUtils::Database::ODBC;

namespace {

const SQLUINTEGER rs_count_timeout=60;	// Seconds.
const TCHAR timeout_sql_state[]=_T("HYT0");

class ODBCErrInfo {
public:
	const SQLTCHAR * const SqlState;
	const SQLTCHAR * const Msg;
	const SQLINTEGER NativeError;

	inline ODBCErrInfo(const SQLTCHAR * const a, const SQLTCHAR * const b, const SQLINTEGER c) : SqlState(a), Msg(b), NativeError(c) {};
	inline ~ODBCErrInfo(void) {};
	friend inline tostream & __fastcall operator<<(tostream &o, const ODBCErrInfo i) {o<<_T("State: '")<<reinterpret_cast<const TCHAR * const>(i.SqlState)<<_T("'\nMessage: '")<<reinterpret_cast<const TCHAR * const>(i.Msg)<<_T("'\nNative error number: ")<<i.NativeError<<std::endl;return o;};

private:
	inline ODBCErrInfo(void) : SqlState(NULL), Msg(NULL), NativeError(NULL) {};
};

}

inline
Database::ODBC::Exceptions::ODBCExceptionErr::ODBCExceptionErr(const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : std::exception() {
	m_info<<_T("ODBC error details:\n");
	bool timed_out;
	m_info<<GetODBCERRInfoTimeout(hstmt, type, timed_out)<<std::endl;
	if (timed_out) {
		m_info<<_T("Timed out.\n");
	}
	m_info<<_T("ODBC error raw code:")<<err<<std::endl;
}

inline
Database::ODBC::Exceptions::ODBCExceptionErr::~ODBCExceptionErr(void) {
	m_info<<_T("ODBC Wrapper error.\n");
}

inline
const TCHAR * __fastcall Database::ODBC::Exceptions::ODBCExceptionErr::what(void) {
	tstring str(m_info.str());
	str_hack=std::auto_ptr<TCHAR>(new TCHAR[str.size()+sizeof(TCHAR)]);
	memcpy(str_hack.get(), str.c_str(), sizeof(TCHAR)*str.size());
	return str_hack.get();
}

inline
tstring __fastcall Database::ODBC::Exceptions::ODBCExceptionErr::GetODBCERRInfoTimeout(const SQLHANDLE * const hstmt, const SQLSMALLINT type, bool &timed_out) {
	SQLTCHAR SqlState[6], Msg[SQL_MAX_MESSAGE_LENGTH];
	SQLINTEGER NativeError;
	SQLSMALLINT i=1, MsgLen;
	SQLRETURN rc2;
	tstringstream ss;
	timed_out=false;
	while ((rc2=SQLGetDiagRec(type, *hstmt, i, SqlState, &NativeError, Msg, sizeof(Msg), &MsgLen)) != SQL_NO_DATA) {
		const tstring state(reinterpret_cast<TCHAR *>(SqlState));
		if (state.find(timeout_sql_state)!=tstring::npos) {
			timed_out=true;
		}
		ss<<ODBCErrInfo(SqlState, Msg, NativeError);
		i++;
	}
	return ss.str();
}

inline
Database::ODBC::Exceptions::EnvironmentErr::EnvironmentErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : ODBCExceptionErr(hstmt, type, err) {
	m_info<<_T("Environment error. Details:\n");
	m_info<<str<<std::endl;
}

inline
Database::ODBC::Exceptions::EnvironmentErr::~EnvironmentErr(void) {
}

inline
Database::ODBC::Exceptions::ConnectionErr::ConnectionErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : ODBCExceptionErr(hstmt, type, err) {
	m_info<<_T("Connection error. Details:\n");
	m_info<<str<<std::endl;
}

inline
Database::ODBC::Exceptions::ConnectionErr::ConnectionErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : ODBCExceptionErr(hstmt, type, err) {
	m_info<<_T("Connection error. Details:\n");
	m_info<<str<<std::endl;
	m_info<<_T("SQL statement:\n");
	m_info<<sql<<std::endl;
}

inline
Database::ODBC::Exceptions::ConnectionErr::~ConnectionErr(void) {
}

inline
Database::ODBC::Exceptions::RecordsetBaseErr::RecordsetBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : ODBCExceptionErr(hstmt, type, err) {
	m_info<<_T("Record set error. Details:\n");
	m_info<<str<<std::endl;
}

inline
Database::ODBC::Exceptions::RecordsetBaseErr::RecordsetBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : ODBCExceptionErr(hstmt, type, err) {
	m_info<<_T("Record set error. Details:\n");
	m_info<<str<<std::endl;
	m_info<<_T("SQL statement:\n");
	m_info<<sql<<std::endl;
}

inline
Database::ODBC::Exceptions::RecordsetBaseErr::~RecordsetBaseErr(void) {
	m_info<<_T("Read-only record set error.\n");
}

inline
Database::ODBC::Exceptions::RORecordsetBaseErr::RORecordsetBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RecordsetBaseErr(str, hstmt, type, err) {
	m_info<<_T("Read-only record set error.\n");
}

inline
Database::ODBC::Exceptions::RORecordsetBaseErr::RORecordsetBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RecordsetBaseErr(str, sql, hstmt, type, err) {
}

inline
Database::ODBC::Exceptions::RORecordsetBaseErr::RORecordsetBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RecordsetBaseErr(str, hstmt, type, err) {
	m_info<<_T("Read-only record set error. Parameters:\n");
	m_info<<_T("Column number: ")<<col_num<<std::endl;
	m_info<<_T("Parameter type: '")<<data.name()<<_T("'")<<std::endl;
}

inline
Database::ODBC::Exceptions::RORecordsetBaseErr::RORecordsetBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RecordsetBaseErr(str, sql, hstmt, type, err) {
	m_info<<_T("Read-only record set error. Parameters:\n");
	m_info<<_T("Column number: ")<<col_num<<std::endl;
	m_info<<_T("Parameter type: '")<<data.name()<<_T("'")<<std::endl;
}

inline
Database::ODBC::Exceptions::RORecordsetBaseErr::~RORecordsetBaseErr(void) {
	m_info<<_T("Write-only connection error.\n");
}

inline
Database::ODBC::Exceptions::WOConnectionBaseErr::WOConnectionBaseErr(const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RORecordsetBaseErr(str, hstmt, type, err) {
	m_info<<_T("Write-only connection error.\n");
}

inline
Database::ODBC::Exceptions::WOConnectionBaseErr::WOConnectionBaseErr(const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RORecordsetBaseErr(str, sql, hstmt, type, err) {
	m_info<<_T("Write-only connection error.\n");
}

inline
Database::ODBC::Exceptions::WOConnectionBaseErr::WOConnectionBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RORecordsetBaseErr(col_num, data, str, hstmt, type, err) {
	m_info<<_T("Write-only connection error.\n");
}

inline
Database::ODBC::Exceptions::WOConnectionBaseErr::WOConnectionBaseErr(const SQLUSMALLINT col_num, const type_info &data, const tstring &str, const tstring &sql, const SQLHANDLE * const hstmt, const SQLSMALLINT type, const SQLRETURN err) : RORecordsetBaseErr(col_num, data, str, sql, hstmt, type, err) {
	m_info<<_T("Write-only connection error.\n");
}

inline
Database::ODBC::Exceptions::WOConnectionBaseErr::~WOConnectionBaseErr(void) {
}

inline
Environment::Environment(const bool read_only, const SQLUINTEGER timeout) : henv(NULL), hdbc(NULL) {
	SQLRETURN ret=SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to allocate ODBC handle."), &henv, SQL_HANDLE_ENV, ret);
	}
	ret=SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, reinterpret_cast<SQLPOINTER>(SQL_OV_ODBC3), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set ODBC version behaviour as v3.x on the environment handle."), &henv, SQL_HANDLE_ENV, ret);
	}
	ret=SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to allocate data base connection handle."), &henv, SQL_HANDLE_ENV, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, reinterpret_cast<SQLPOINTER>(timeout), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set the login timeout on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_ATTR_AUTOCOMMIT, reinterpret_cast<SQLPOINTER>(read_only ? SQL_AUTOCOMMIT_ON : SQL_AUTOCOMMIT_OFF), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set the auto commit feature on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_ATTR_CONNECTION_POOLING, reinterpret_cast<SQLPOINTER>(SQL_CP_ONE_PER_HENV), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to enable connection pooling on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_ATTR_CP_MATCH, reinterpret_cast<SQLPOINTER>(SQL_CP_STRICT_MATCH), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set the connection pooling matching algorithm on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_ATTR_ACCESS_MODE, reinterpret_cast<SQLPOINTER>(read_only ? SQL_MODE_READ_ONLY : SQL_MODE_READ_WRITE), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set the read/write attributes on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetConnectAttr(hdbc, SQL_ATTR_TXN_ISOLATION, reinterpret_cast<SQLPOINTER>(read_only ? SQL_TXN_READ_UNCOMMITTED : SQL_TXN_SERIALIZABLE), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::EnvironmentErr(_T("Failed to set the transation isolation level on the data base connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
}

inline
Environment::~Environment(void) {
	if (hdbc) {
		SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
	}
	if (henv) {
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}


inline
Connection::Connection(const tstring &cnx, const bool read_only, const SQLUINTEGER timeout) : Environment(read_only, timeout), stmt_timeout(timeout), hstmt(NULL) {
	const SQLSMALLINT cnx_buff_sz=1024;
	SQLTCHAR cnx_buff[cnx_buff_sz];
	SQLSMALLINT StringLength2Ptr;
	const SQLRETURN ret=SQLDriverConnect(hdbc, NULL, const_cast<SQLTCHAR *>(cnx.c_str()), SQL_NTS, cnx_buff, cnx_buff_sz, &StringLength2Ptr, SQL_DRIVER_COMPLETE);
	cnx_str=cnx_buff;
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to connect to the supplied DSN."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	AllocateStmt();
}

inline
Connection::~Connection(void) {
	if (hstmt) {
		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	}
	SQLDisconnect(hdbc);
}

inline
void __fastcall Connection::execute(const tstring &sql) {
	const SQLRETURN ret=SQLExecDirect(hstmt, const_cast<SQLTCHAR *>(sql.c_str()), SQL_NTS);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RecordsetBaseErr(_T("Failed to execute the SQL statement on the statement handle."), sql, &hstmt, SQL_HANDLE_STMT, ret);
	}
}

inline
void __fastcall Connection::AllocateStmt(void) {
	SQLRETURN ret=SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to allocate the SQL statement handle on the data base handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_CONCURRENCY, reinterpret_cast<SQLPOINTER>(SQL_CONCUR_READ_ONLY), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to set the concurrency attribute as a forward-only cursor on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_CURSOR_TYPE, reinterpret_cast<SQLPOINTER>(SQL_CURSOR_FORWARD_ONLY), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to set the cursor attribute as a forward-only cursor on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_CURSOR_SCROLLABLE, reinterpret_cast<SQLPOINTER>(SQL_NONSCROLLABLE), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to set the non-scollable cursor attribute on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_QUERY_TIMEOUT, reinterpret_cast<SQLPOINTER>(stmt_timeout), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to set the query time-out attribute on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_RETRIEVE_DATA, reinterpret_cast<SQLPOINTER>(SQL_RD_ON), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to set the data retrieval attribute on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_NOSCAN, reinterpret_cast<SQLPOINTER>(SQL_NOSCAN_ON), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::ConnectionErr(_T("Failed to turn off DBMS-specific scanning on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
}

inline
RecordsetBase::RecordsetBase(const tstring &cnx, const bool read_only, const SQLUINTEGER timeout) : Connection(cnx, read_only, timeout), allocated_cursor(false), row_count(-1) {
}

inline
RecordsetBase::~RecordsetBase(void) {
	if (allocated_cursor) {
		SQLCloseCursor(hstmt);
	}
}

inline
void __fastcall RecordsetBase::execute(const tstring &sql) {
	Connection::execute(sql);
	allocated_cursor=true;
	row_count=-1;
}

inline
void __fastcall RecordsetBase::RowsAffected(void) {
	const SQLRETURN ret=SQLRowCount(hstmt, &row_count);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RecordsetBaseErr(_T("Failed to get the row count from the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
}

inline
RORecordsetBase::RORecordsetBase(const tstring &cnx, const SQLUINTEGER timeout) : RecordsetBase(cnx, true, timeout), no_more_data(true) {
}

inline
RORecordsetBase::~RORecordsetBase(void) {
}

inline
void __fastcall RORecordsetBase::execute(const tstring &sql) {
	RecordsetBase::execute(sql);
	sql_statement=sql;
	const SQLRETURN ret=SQLNumResultCols(hstmt, &num_cols);
	DEBUG_ASSERT(num_cols>0);
	if (!num_cols || (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO)) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to get the number of columns returned by the SQL statement from the statement handle."), sql, &hstmt, SQL_HANDLE_STMT, ret);
	}
	column_names.clear();
}

inline
SQLINTEGER __fastcall RORecordsetBase::count(void) {
	if (row_count==-1) {
		// Whack up the timeout, as I've seen it sometimes time out if the db server is busy.
		RORecordset rs(cnx_string(), rs_count_timeout);
		// Need to name the column, otherwise the "field_cast(...)" doesn't work because the index
		// is poo.
		rs.execute(_T("select count(*) as pants from (") + sql_statement + _T(") as tmp_cnt_table"));
		rs.item(1, row_count);
	}
	return RecordsetBase::count();
}

// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
// Hence no "inline".
SQLSMALLINT __fastcall RORecordsetBase::GetColumnIdx(const tstring &col_name) {
	return column_names[col_name];
}

inline
RORecordset::RORecordset(const tstring &cnx, const SQLUINTEGER timeout) : RORecordsetBase(cnx, timeout) {
}

inline
RORecordset::~RORecordset(void) {
}

inline
void __fastcall RORecordset::execute(const tstring &sql) {
	RORecordsetBase::execute(sql);
	SQLSMALLINT i=1;
	SQLTCHAR ColumnName[MAX_PATH];
	SQLSMALLINT NameLengthPtr;
	SQLSMALLINT DataTypePtr;
	SQLUINTEGER ColumnSizePtr;
	SQLSMALLINT DecimalDigitsPtr;
	SQLSMALLINT NullablePtr;
	do {
		const SQLRETURN ret=SQLDescribeCol(hstmt, i, ColumnName, MAX_PATH, &NameLengthPtr, &DataTypePtr, &ColumnSizePtr, &DecimalDigitsPtr, &NullablePtr);
		if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to get the required SQL column details from the statement handle."), sql, &hstmt, SQL_HANDLE_STMT, ret);
		}
		column_names[ColumnName]=i;
	} while (++i<=NumColumns());
	DEBUG_ASSERT(NumColumns()==column_names.size());
	no_more_data=false;
	moveNext();
}

inline
void __fastcall RORecordset::moveNext(void) {
	const SQLRETURN ret=SQLFetch(hstmt);
	if (ret==SQL_NO_DATA) {
		no_more_data=true;
	} else if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to fetch the next data on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
}

inline
ROBulkRecordset::ROBulkRecordset(const tstring &cnx, const SQLUINTEGER row_arr_sz, const SQLUINTEGER timeout) : RORecordsetBase(cnx, timeout), row_buffer_size(row_arr_sz) {
}

inline
ROBulkRecordset::~ROBulkRecordset(void) {
}

inline
void __fastcall ROBulkRecordset::execute(const tstring &sql) {
	SQLRETURN ret=SQLFreeStmt(hstmt, SQL_UNBIND);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to unbind any old row data blocks from the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_RETRIEVE_DATA, reinterpret_cast<SQLPOINTER>(SQL_RD_OFF), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to set the data retrieval attribute on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	RORecordsetBase::execute(sql);
	cols_sizes.clear();
	SQLSMALLINT i=1;
	SQLTCHAR ColumnName[MAX_PATH];
	SQLSMALLINT NameLengthPtr;
	SQLSMALLINT DataTypePtr;
	SQLUINTEGER ColumnSizePtr;
	SQLSMALLINT DecimalDigitsPtr;
	SQLSMALLINT NullablePtr;
	TotalColumnSizePtr=0;
	size_t offset=0;
	do {
		ret=SQLDescribeCol(hstmt, i, ColumnName, MAX_PATH, &NameLengthPtr, &DataTypePtr, &ColumnSizePtr, &DecimalDigitsPtr, &NullablePtr);
		if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
			tostringstream ss;
			ss<<_T("Failed to get the required ")<<i<<_T("-th SQL column details from the statement handle.");
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(ss.str(), sql, &hstmt, SQL_HANDLE_STMT, ret);
		}
		column_names[ColumnName]=i;
		// Seems to under-estimate the size of strings that are returned by 2 bytes....
		const size_t size=ColumnSizePtr+2*sizeof(TCHAR);
		cols_sizes.push_back(std::pair<size_t, size_t>(size, offset));
		// Remember to add on a bit for the the element status...
		offset+=size+sizeof(SQLINTEGER);
	} while (++i<=NumColumns());
	TotalColumnSizePtr=offset;
	DEBUG_ASSERT(NumColumns()==cols_sizes.size());
	DEBUG_ASSERT(NumColumns()==column_names.size());
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	AllocateStmt();
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_RETRIEVE_DATA, reinterpret_cast<SQLPOINTER>(SQL_RD_ON), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to set the data retrieval attribute on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_ROW_BIND_TYPE, reinterpret_cast<SQLPOINTER>(TotalColumnSizePtr), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to bind the row data block to the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_ROW_ARRAY_SIZE, reinterpret_cast<SQLPOINTER>(row_buffer_size), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to set the number of rows in the row data block attribute in the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	AllocateDataBlock();
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_ROW_STATUS_PTR, RowStatusArray.get(), 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to bind the rows status array to the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	ret=SQLSetStmtAttr(hstmt, SQL_ATTR_ROWS_FETCHED_PTR, &NumRowsFetched, 0);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to bind the number of rows filled variable to the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	i=0;
	do {
		ret=SQLBindCol(
			hstmt,
			i+1,
			SQL_C_DEFAULT,
			rows_buffer.get()+cols_sizes[i].second,
			cols_sizes[i].first,
			reinterpret_cast<SQLINTEGER *>(
				rows_buffer.get()+
				(
					i<(cols_sizes.size()-1) ?
						cols_sizes[i+1].second
					:
						TotalColumnSizePtr
				)
				-sizeof(SQLINTEGER)
			)
		);
		if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
			tostringstream ss;
			ss<<_T("Failed to bind the ")<<i<<_T("-th element from a row in the row data block to the statement handle.");
			throw Database::ODBC::Exceptions::RORecordsetBaseErr(ss.str(), &hstmt, SQL_HANDLE_STMT, ret);
		}
	} while (++i<cols_sizes.size());
	RecordsetBase::execute(sql);
	row_buffer_pos=0;
	no_more_data=false;
	RawMoveNext();
}

inline
void __fastcall ROBulkRecordset::moveNext(void) {
	++row_buffer_pos;
	if (row_buffer_pos>=NumRowsFetched) {
		row_buffer_pos=0;
		RawMoveNext();
	}
}

inline
void __fastcall ROBulkRecordset::RawMoveNext(void) {
	const SQLRETURN ret=SQLFetch(hstmt);
	DEBUG_ASSERT(NumRowsFetched>=0);
	if (ret==SQL_NO_DATA) {
		DEBUG_ASSERT(NumRowsFetched==0);
		no_more_data=true;
		return;
	} else if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::RORecordsetBaseErr(_T("Failed to fetch the next block of data on the statement handle."), &hstmt, SQL_HANDLE_STMT, ret);
	}
	DEBUG_ASSERT(NumRowsFetched>0);
}

inline
void __fastcall ROBulkRecordset::AllocateDataBlock(void) {
	DEBUG_ASSERT(TotalColumnSizePtr);
	DEBUG_ASSERT(row_buffer_size);
	rows_buffer=std::auto_ptr<RowBufferElementType>(new RowBufferElementType[TotalColumnSizePtr*row_buffer_size]);
	RowStatusArray=std::auto_ptr<SQLUSMALLINT>(new SQLUSMALLINT[row_buffer_size]);
}

inline
const ROBulkRecordset::RowBufferElementType * const __fastcall ROBulkRecordset::GetRowElement(const SQLUSMALLINT col_num) const {
	return rows_buffer.get()+row_buffer_pos*TotalColumnSizePtr+cols_sizes[col_num-1].second;
}

inline
WOConnectionBase::WOConnectionBase(const tstring &cnx, const SQLUINTEGER timeout) : RecordsetBase(cnx, false, timeout), in_transaction(false) {
	SQLUSMALLINT tx_level;
	SQLSMALLINT StringLengthPtr;
	SQLRETURN ret=SQLGetInfo(hdbc, SQL_TXN_CAPABLE, reinterpret_cast<SQLPOINTER>(&tx_level), sizeof(SQLUSMALLINT), &StringLengthPtr);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to find transaction support information on the connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	if (tx_level==SQL_TC_NONE) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Transactions (which this applicationrequires) are not supported at all on the data base connection driver."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	SQLTCHAR many_txs[]=_T(" \0");
	ret=SQLGetInfo(hdbc, SQL_MULTIPLE_ACTIVE_TXN, reinterpret_cast<SQLPOINTER>(&many_txs), sizeof(many_txs), &StringLengthPtr);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to find multiple-transaction support information on the data base connection driver."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	if (many_txs[0]!='Y') {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Multiple transactions are not supported at all on the data base connection driver."), &hdbc, SQL_HANDLE_DBC, ret);
	}
}

inline
WOConnectionBase::~WOConnectionBase(void) {
	if (in_transaction) {
		rollback();
	}
}

inline
void __fastcall WOConnectionBase::commit(void) {
	const SQLRETURN ret=SQLEndTran(SQL_HANDLE_DBC, hdbc, SQL_COMMIT);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to commit a transaction on the connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	in_transaction=false;
}

inline
void __fastcall WOConnectionBase::rollback(void) {
	const SQLRETURN ret=SQLEndTran(SQL_HANDLE_DBC, hdbc, SQL_ROLLBACK);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to rollback a transaction on the connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	in_transaction=false;
}

inline
SQLINTEGER __fastcall WOConnectionBase::count(void) {
	RecordsetBase::RowsAffected();
	return RecordsetBase::count();
}

inline
bool __fastcall WOConnectionBase::in_trans(void) const {
	return in_transaction;
}

// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
// Hence no "inline".
void __fastcall WOConnectionBase::AddExecData(const SQLUSMALLINT col_num, const str_data_at_exec_type &data) {
	exec_data[col_num]=data;
}

// Avoid passing the map across a dll boundary, otherwise you get silly memory errors when accessing it...
// Hence no "inline".
WOConnectionBase::str_data_at_exec_type & __fastcall WOConnectionBase::ExecData(const SQLUSMALLINT col_num) {
	return exec_data[col_num];
}

inline
WOConnection::WOConnection(const tstring &cnx, const SQLUINTEGER timeout) : WOConnectionBase(cnx, timeout) {
}

inline
WOConnection::~WOConnection(void) {
}

inline
void __fastcall WOConnection::execute(const tstring &sql) {
	DEBUG_ASSERT(!in_transaction);
/*	if (in_transaction) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Already in a transaction on the connection handle - it must be committed first."), sql, &hdbc, SQL_HANDLE_DBC, SQL_ERROR);
	}
*/	WOConnectionBase::execute(sql);
	in_transaction=true;
}

inline
WOBulkConnection::WOBulkConnection(const tstring &cnx, const SQLUINTEGER row_arr_sz, const SQLUINTEGER timeout) : WOConnectionBase(cnx, timeout), row_buffer_size(row_arr_sz), prepared(false) {
	SQLUSMALLINT property;
	SQLSMALLINT StringLengthPtr;
	SQLRETURN ret=SQLGetInfo(hdbc, SQL_CURSOR_COMMIT_BEHAVIOR, reinterpret_cast<SQLPOINTER>(&property), sizeof(SQLUSMALLINT), &StringLengthPtr);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to get the prepared statement behaviour after commits on the connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	sp_behaviour.commit=(property!=SQL_CB_DELETE);
	ret=SQLGetInfo(hdbc, SQL_CURSOR_ROLLBACK_BEHAVIOR, reinterpret_cast<SQLPOINTER>(&property), sizeof(SQLUSMALLINT), &StringLengthPtr);
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to get the prepared statement behaviour after rollbacks on the connection handle."), &hdbc, SQL_HANDLE_DBC, ret);
	}
	sp_behaviour.rollback=(property!=SQL_CB_DELETE);
}

inline
WOBulkConnection::~WOBulkConnection(void) {
}

inline
void __fastcall WOBulkConnection::prepare(const tstring &sql) {
	DEBUG_ASSERT(!in_transaction);
	if (in_transaction) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Already in a transaction on the connection handle - it must be committed first."), sql_statement, &hdbc, SQL_HANDLE_DBC, SQL_ERROR);
	}
	if (prepared) {
		const SQLRETURN ret=SQLFreeStmt(hstmt, SQL_RESET_PARAMS);
		if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
			throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to unbind the previously bound paramaters on the SQL statement on the statement handle."), sql_statement, &hstmt, SQL_HANDLE_STMT, ret);
		}
	}
	if (sql!=sql_statement){
		prepared=false;
	}
	if (!prepared) {
		sql_statement=sql;
		SQLRETURN ret=SQLPrepare(hstmt, const_cast<SQLTCHAR *>(sql.c_str()), SQL_NTS);
		if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
			throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to prepare the SQL statement on the statement handle."), sql_statement, &hstmt, SQL_HANDLE_STMT, ret);
		}
		prepared=true;
	}
	DEBUG_ASSERT(prepared);
}

inline
void __fastcall WOBulkConnection::execute(void) {
	DEBUG_ASSERT(prepared);
	DEBUG_ASSERT(!in_transaction);
	if (in_transaction) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Already in a transaction on the connection handle - it must be committed first."), sql_statement, &hdbc, SQL_HANDLE_DBC, SQL_ERROR);
	}
	SQLRETURN ret=SQLExecute(hstmt);
	if (ret==SQL_NEED_DATA) {
		SQLPOINTER pToken;
		while ((ret=SQLParamData(hstmt, &pToken))==SQL_NEED_DATA) {
			ret=SQLPutData(hstmt, exec_data[reinterpret_cast<SQLUSMALLINT>(pToken)].str->begin(), SQL_NTS);
			if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
				throw Database::ODBC::Exceptions::WOConnectionBaseErr(reinterpret_cast<SQLUSMALLINT>(pToken), typeid(*(exec_data[reinterpret_cast<SQLUSMALLINT>(pToken)].str)), _T("Failed to bung some data into the prepared, but hungry SQL statement on the statement handle."), sql_statement, &hstmt, SQL_HANDLE_STMT, ret);
			}
		}
	}
	if (ret!=SQL_SUCCESS && ret!=SQL_SUCCESS_WITH_INFO) {
		throw Database::ODBC::Exceptions::WOConnectionBaseErr(_T("Failed to execute the prepared SQL statement on the statement handle."), sql_statement, &hstmt, SQL_HANDLE_STMT, ret);
	}
	in_transaction=true;
}

inline
void __fastcall WOBulkConnection::commit(void) {
	WOConnectionBase::commit();
	if (!sp_behaviour.commit) {
		prepared=false;
		prepare(sql_statement);
	}
}

inline
void __fastcall WOBulkConnection::rollback(void) {
	WOConnectionBase::rollback();
	if (!sp_behaviour.rollback) {
		prepared=false;
		prepare(sql_statement);
	}
}
