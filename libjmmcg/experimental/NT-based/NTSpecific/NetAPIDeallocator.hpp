/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../../core/deleter.hpp"

#include <boost/noncopyable.hpp>

/////////////////////////////////////////////////////////////////////////////

namespace jmmcg { namespace NTUtils {

const TCHAR netapi32_nt_lib_name[]= _T("netapi32.dll");
const TCHAR NetApiBufferFree_name[]= _T("NetApiBufferFree");
typedef NET_API_STATUS(NET_API_FUNCTION* pNetApiBufferFreeType)(IN LPVOID Buffer);

template<class T>
struct NetEnumDeallocator {
	using result_typer= void;
	using argument_type= T;
	using deleter_t= default_deleter<T>;

	result_type __fastcall operator()(argument_type* t) const {
		if(t) {
			if(*t) {
				register DWORD ret= ::WNetCloseEnum(*t);
				LIBJMMCG_LOG_TRIVIAL(trace) << "NetEnumDeallocator::~operator()(): Windows error code: " << win_exception::StrFromWinErr(ret != NO_ERROR ? ::GetLastError() : 0);
			}
			deleter_t().operator()(t);
		}
	}
};

template<class T>
class NetAPIDeallocator : virtual private LoadLibraryWrapper, private boost::noncopyable {
public:
	using result_type= void;
	using argument_type= T;

	__stdcall NetAPIDeallocator()
		: LoadLibraryWrapper(netapi32_nt_lib_name),
		  pNetApiBufferFree(reinterpret_cast<pNetApiBufferFreeType>(::GetProcAddress(Handle(), NetApiBufferFree_name))) {
		DEBUG_ASSERT(pNetApiBufferFree);
	}

	result_type __stdcall operator()(argument_type* t) const {
		if(t) {
			register NET_API_STATUS ret= (*pNetApiBufferFree)(t);
			LIBJMMCG_LOG_TRIVIAL(trace) << "NetAPIDeallocator::~operator()(): Windows error code: " << win_exception::StrFromWinErr(ret != NO_ERROR ? ::GetLastError() : 0);
		}
	}

private:
	const pNetApiBufferFreeType pNetApiBufferFree;
};

}}
