/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "Directory.hpp"

#include "../../../core/exception.hpp"

/////////////////////////////////////////////////////////////////////////////

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

const tchar security_lib_name[]= _T("advapi32.dll");
#ifdef UNICODE
const TCHAR SetFileSecurity_name[]= _T("SetFileSecurityW");
#else
const tchar SetFileSecurity_name[]= _T("SetFileSecurityA");
#endif	// !UNICODE

/////////////////////////////////////////////////////////////////////////////

Directory::Directory(const tstring& unc_to_dir, const TCHAR* const machine, const TCHAR* const user, const DWORD access)
	: LoadLibraryWrapper(security_lib_name),
	  pSetFileSecurity(reinterpret_cast<SetFileSecurityType>(::GetProcAddress(Handle(), SetFileSecurity_name))),
	  unc(unc_to_dir + dir_separator_s) {
	unsigned long err= sd.Allow(machine, user, access);
	if(err) {
		info::function info(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T("const tstring &"), unc_to_dir));
		info.add_arg(_T("const tstring &"), machine);
		info.add_arg(_T("const tstring &"), user);
		info.add_arg(_T("const DWORD"), access);
		throw exception_type(_T("Failed to add access allowed ACE to security descriptor for user."), info, __REV_INFO__);
	}
	err= sd.Allow(machine, _T("SYSTEM"), GENERIC_ALL);
	if(err) {
		info::function info(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T("const tstring &"), unc_to_dir));
		info.add_arg(_T("const tstring &"), machine);
		info.add_arg(_T("const tstring &"), user);
		info.add_arg(_T("const DWORD"), access);
		throw exception_type(_T("Failed to add access allowed ACE to security descriptor for user 'SYSTEM' on the specified machine with an access mask of 'GENERIC_ALL'."), info, __REV_INFO__);
	}
	SECURITY_ATTRIBUTES sa= {
		sizeof(SECURITY_ATTRIBUTES),
		const_cast<SECURITY_DESCRIPTOR*>(&sd.SD()),
		true};
	if(!::CreateDirectory(unc.c_str(), &sa)) {
		info::function info(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T("const tstring &"), unc_to_dir));
		info.add_arg(_T("const tstring &"), machine);
		info.add_arg(_T("const tstring &"), user);
		info.add_arg(_T("const DWORD"), access);
		throw exception_type(_T("Failed to create the specified directory on the specified machine using the specified user name with the specified access mask."), info, __REV_INFO__);
	}
}

Directory::~Directory(void) {
	CFileFind finder;
	bool bWorking= finder.FindFile((unc + wildcard).c_str());
	bool emptied= true;
	while(bWorking) {
		bWorking= finder.FindNextFile();
		if(!finder.IsDirectory()) {
			const tstring path(unc + finder.GetFileName().GetBuffer(0));
			if(pSetFileSecurity) {
				// Ensure we have permission to delete the file...
				(*pSetFileSecurity)(path.c_str(), DACL_SECURITY_INFORMATION, const_cast<SECURITY_DESCRIPTOR*>(&sd.SD()));
			}
			if(!DeleteFile(path.c_str())) {
				emptied= false;
			}
		}
	}
	bool err= ::RemoveDirectory(unc.c_str());
	if(!err) {
		std::stringstream ss;
		ss << "Failed to delete directory '" << unc << "'. " << (emptied ? "For some unknown reason" : "Because it wasn't empty") << ".";
		throw exception_type(ss.str(), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(*this)), __REV_INFO__);
	}
}
