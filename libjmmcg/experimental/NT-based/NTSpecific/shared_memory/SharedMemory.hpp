/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include"../NTSecuritySettings.hpp"
#include"../RegistryKey.hpp"
#include"../DumpWinMsg.hpp"
#include"../NTLocking.hpp"

//#include<atlbase.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	namespace NTUtils {

		class HeapID {
		public:
			unsigned long size;
			std::pair< std::string, std::string > names;
			const NTUtils::SecuritySettings *security;

			inline HeapID(void) : size( 0 ), names( std::pair< std::string, std::string >("", "") ), security( NULL ) {}
			inline HeapID(const unsigned long s, const char * const mem_n, const char * const mtx_n, const NTUtils::SecuritySettings * const sec) : size( s ), names( std::pair< std::string, std::string >(mem_n, mtx_n) ), security( sec ) {}
			inline HeapID(const unsigned long s, const std::pair< std::string, std::string > &n, const NTUtils::SecuritySettings * const sec) : size( s ), names( n ), security( sec )  {}
			inline HeapID(const HeapID &h) : size( h.size ), names( h.names ), security( h.security ) {}
			inline ~HeapID(void) {}
			inline HeapID &operator=(const HeapID &h) {size=h.size;names=h.names;security=h.security;return *this;}

		private:
		};

		// Create a named shared-memory area with no memory management of the shared area.
		// It's just an array of type "contents".
		template < class contents > class RawSharedMemory {
		public:
			typedef contents contents_type;
			// Note this allocates memory in blocks of "sizeof(contents)" units.
			// Yes - you can use a real file instead of the swap file! Who needs serialisation?!
			inline RawSharedMemory(const HeapID &details, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			virtual inline ~RawSharedMemory(void);

			inline const std::string &Name(void) const noexcept(true);
			// In units of "sizeof(contents)".
			virtual inline unsigned long Capacity(void) const noexcept(true);
			// This is very slow!!! This function has to delete and re-allocate the shared
			// memory area, but keeps the same name. Note that it is likely to invalidate
			// any stored pointers to the memory block area.
			// In units of "sizeof(contents)".
			virtual inline bool Capacity(const unsigned long sz, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			inline bool IsCreator(void) const noexcept(true);
			// See warning by "MapDataPtr(...)". Also note the "const" and heed well.
			// The pointer returned is *really* REALLY read-only. No amount of "const_cast<...>(...)"
			// will get round it. NT will throw loads of access violations if you try and write
			// into the data using any perversion of a pointer returned by this function.
			// Use the non-const one below if you want read-write access.
			// Also note that the two pointers returned by these functions may not be equal!!!
			// i.e. "(const contents *)*this 'may not ==' (contents *)*this" !!! Tee hee!
			inline operator const contents *() const;
			// See warning by "MapDataPtr(...)".
			inline operator contents *();
			inline NTUtils::Mutex &Lock(void) noexcept(true);
			// Handy for dumping the memory contents....
			// Doesn't save the private bit.
			virtual inline std::ostream &operator<<(std::ostream &strm) const;
			// Might as well..... What the heck. Bit pointless, as it would probably be better to use
			// a file handle in the contructor.
			// Doesn't restore the private bit.
			virtual inline std::istream &operator>>(std::istream &strm);
			inline unsigned long UsageCount(void) const noexcept(true);

		protected:
			const NTUtils::SecuritySettings * const ss;
			NTUtils::Mutex mutex;
			HANDLE rw_pub_mapping;
			HANDLE ro_pub_mapping;
			contents *rw_raw_data;
			// These public pointers are offset just beyond the private data area to protect them
			// from joe public.
			contents *rw_pub_data;
			const contents *ro_pub_data;
			// This usage counter is stored in the memory mapped file in a private data area.
			// It is accessed using the "rw_raw_data" pointer.
			struct private_data_block_type {
				unsigned long usage_ctr;
			};

			// "p" in bytes, "s" in "sizeof(contents)".
			inline RawSharedMemory(const unsigned long p, const HeapID &details, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			inline contents *MapDataPtr(HANDLE file, const DWORD access, const DWORD hi_offset, const DWORD low_offset, const unsigned long size);
			inline void UnmapDataPtr(const contents *);
			static inline unsigned long GetEndPrivateDataBlock(void) noexcept(true);

		private:
			const std::string name;
			// In bytes including the private data block.
			unsigned long raw_size;
			// In "sizeof(contents)" excluding the private data block.
			unsigned long pub_size;
			bool creator;
			HANDLE rw_raw_mapping;

			inline bool Create(HANDLE file, const SECURITY_ATTRIBUTES * const security, const DWORD protection);
			inline void Delete(void);

			// Don't allow copying or assignment.
			inline RawSharedMemory(const RawSharedMemory &) : ss( NULL ) {DEBUG_ASSERT(false);}
			inline RawSharedMemory &operator=(const RawSharedMemory &) {DEBUG_ASSERT(false);return *this;}
		};

		// This is just an interface class, and does nothing, but let you test the object to see if it
		// has a memory manager, so useable with, say objects that take an "std::allocator<...>" object.
		template < class contents > class ManagedSharedMemory : public RawSharedMemory< contents > {
		public:
			inline ManagedSharedMemory(const std::string &mgr_name, const HeapID &details, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			virtual inline ~ManagedSharedMemory();

			// Can't use "new" and "delete" much as I'd like to as "new" is a static member
			// funciton in C++, so doesn't have a "this" pointer, so I can't get at the current
			// object (easily).
			// "n" in units of "sizeof(contents)".
			inline contents *Allocate(const unsigned long n, const void * const hint);
			inline void Construct(contents * const ptr,const contents &val);
			inline void Deallocate(const contents * const p, const unsigned long n);

		protected:
			struct private_data_block_type {
				// In units of "sizeof(contents)".
				unsigned long used_memory;
				char manager_name;
			};
			struct allocated_data_block_info {
#ifdef _DEBUG
				unsigned long allocating_pid;
				HANDLE allocating_phandle;
				unsigned long allocating_tid;
				HANDLE allocating_thandle;
				char allocating_pname[MAX_PATH];
				unsigned long block_size;
				// For future use. Currently unused.
				unsigned long allocating_line;
				char allocating_src[MAX_PATH];
#endif _DEBUG
			};

			inline ManagedSharedMemory(const unsigned long p, const std::string &mgr_name, const HeapID &details, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			virtual inline contents *BaseAllocate(const unsigned long n, const void * const hint) = 0;
			virtual inline unsigned long BaseDeallocate(const contents * const p, const unsigned long n) = 0;
			static inline unsigned long GetEndPrivateDataBlock(void) noexcept(true);

		private:
			// Don't allow assignment.
			inline ManagedSharedMemory &operator=(const ManagedSharedMemory &) {DEBUG_ASSERT(false);return *this;}
		};

		// Jason's unbeliveably crap memory manager. "It's top-tastic!"
		// I apologise in advance for this implemetation.
		// I've looked all over the web for a descent memory-manager implementation, (inc. "boost.org") &
		// loads of other places. Couldn't find anything....
		// I even looked over the M$ debug memory manager, but that was just too complex to re-
		// implement....  Hence this lash-up.
		// A few beers later: Check out K&R chapter 8.7 for a better example of a memory manager.

		const unsigned long CMSM_free_marker=~(unsigned long)NULL;

		template < class contents > class CrapManagedSharedMemory : public ManagedSharedMemory< contents > {
		public:
			inline CrapManagedSharedMemory(const HeapID &details);
			inline ~CrapManagedSharedMemory(void);
			// In units of "sizeof(contents)".
			inline unsigned long Capacity(void) const noexcept(true);

		private:
			struct private_data_block_type {
				NTUtils::guid_type mem_id;
				NTUtils::guid_type mtx_id;
			};
			// Warning: Crap implementation....
			RawSharedMemory<unsigned long> in_use_list;

			inline contents *BaseAllocate(const unsigned long n, const void * const hint);
			inline unsigned long BaseDeallocate(const contents * const p, const unsigned long n);
			static inline std::pair<std::string, std::string> GetMgrAreaName(const bool first, contents * const rw_raw_data);
			// Don't allow re-sizing.
			inline bool Capacity(const unsigned long sz, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			// Don't allow assignment.
			inline CrapManagedSharedMemory &operator=(const CrapManagedSharedMemory &) {DEBUG_ASSERT(false);return *this;}
		};

		// An implemetation of the K&R memory manager see chapter 8.7 in "The C Programming Language"..
		template < class contents > class KnRManagedSharedMemory : public ManagedSharedMemory< contents > {
		public:
			inline KnRManagedSharedMemory(const HeapID &details);
			inline ~KnRManagedSharedMemory(void);
			// In units of "sizeof(contents)".
			inline unsigned long Capacity(void) const noexcept(true);

		private:
			// Don't use an STL list as we haven't implemented the SMAllocator yet, as this class is implementing it!!!
			// (And it's easier to copy KnR...!)
			struct free_memory_list_type {
				free_memory_list_type *next;
				// In units of "sizeof(contents)".
				unsigned long size;
			};
			free_memory_list_type *free_list;

			inline contents *BaseAllocate(const unsigned long n, const void * const hint);
			inline unsigned long BaseDeallocate(const contents * const p, const unsigned long n);
			// Don't allow re-sizing.
			inline bool Capacity(const unsigned long sz, const DWORD protection = PAGE_READWRITE, HANDLE file = INVALID_HANDLE_VALUE);
			// Don't allow assignment.
			inline KnRManagedSharedMemory &operator=(const KnRManagedSharedMemory &) {DEBUG_ASSERT(false);return *this;}
		};

		// This class is required to allow the allocator used in an STL object (which for
		// complex STL types basically takes a default constructor) the ability to call back
		// via a static member function to obtain the required heap details, so that it can
		// connect to its heap.
		// The "id" template paramter is crucial, and identifies the specific heap object
		// that you want. i.e. if you have more that one heap of the same type, this parameter
		// is used to identify the correct heap object.
		template < class heap_manager, unsigned long id > class ManagedSharedMemorySTLIntf : public heap_manager {
		public:
			inline ManagedSharedMemorySTLIntf(const HeapID &details);
			virtual inline ~ManagedSharedMemorySTLIntf(void);
			// This function is declared here, but not defined. Why?
			// Because it forces the user to implement it. Why? It is the way that the STL
			// allocator-compatible class gets the information it needs to access the shared
			// memory area it will use.
			static HeapID GetHeapID();

		protected:
			inline ManagedSharedMemorySTLIntf(const unsigned long p, const HeapID &details);

		private:
			// Don't allow assignment.
			inline ManagedSharedMemorySTLIntf &operator=(const ManagedSharedMemorySTLIntf &) {DEBUG_ASSERT(false);return *this;}
		};

		// Finally create an STL compatible allocator class. (Note: Don't inherit from "std::allocator<...>" to
		// avoid picking up invalid implemetations.)
		template < class heap_type, class heap_contents > class SMAllocator : private std::allocator<heap_type::contents_type> {
		public:
			typedef unsigned long size_type;
			typedef long difference_type;
			typedef heap_type::contents_type *pointer;
			typedef const heap_type::contents_type *const_pointer;
			typedef heap_type::contents_type &reference;
			typedef const heap_type::contents_type &const_reference;
			typedef heap_type::contents_type value_type;
			// The "heap_type::GetHeap()" fn is a static template function used to get the
			// parameters for linking to the shared memory heap. The "heap_type" class must derive from
			// "ManagedSharedMemorySTLIntf<...>" (which contains the declaration for the public static member
			// function "heap_type::GetHeap()".
			inline SMAllocator() noexcept(true) : heap( heap_type::GetHeapID() ) {}
			inline SMAllocator(const SMAllocator &) noexcept(true) : heap( heap_type::GetHeapID() ) {}
			inline ~SMAllocator() noexcept(true) {};

			inline pointer allocate(const size_type _N, const void *hint) {return heap.Allocate(_N, hint);}
			inline value_type *_Charalloc(const size_type _N) {return heap.Allocate(_N/sizeof(value_type)+1, NULL);}
			inline void construct(pointer _P, const value_type &_V) {
				value_type::allocator_type::value_type * const ptr=_P->get_allocator()._Charalloc(_V.size());
				_P->get_allocator().construct(ptr, _V);
				value_type::const_iterator i(_V.begin());
				value_type::size_type j=0;
				do {
					_P[j]=*i;
					++i;
					++j;
				} while (i!=_V.end());
			}
			inline void deallocate(void *_P, const size_type sz) {heap.Deallocate(static_cast<pointer>(_P), sz);}
// ???
//			inline void destroy(pointer _P) {_P->get_allocator().deallocate(_P,sizeof(value_type));}
			inline void destroy(pointer _P) {_P->heap_contents::~heap_contents();}
			inline size_type max_size() const {return heap.Capacity();}
			template <class alloc> inline bool operator==(const alloc &a) const {return heap.Name()==a.heap.Name();}
			template <class alloc> inline bool operator!=(const alloc &a) const {return !operator==(a);}
			inline const heap_type &Heap() const noexcept(true) {return heap;}

		private:
			heap_type heap;

			// Don't allow assignment.
			template < class ook, class ook1 > inline SMAllocator &operator=(const SMAllocator< ook, ook1 > &) noexcept(true) {DEBUG_ASSERT(false);return *this;}
		};

		template < class contents > inline RawSharedMemory<contents>::RawSharedMemory(const HeapID &details, const DWORD protection, HANDLE file) : ss( details.security ), mutex( details.names.second=="" ? NTUtils::GetGUID() : details.names.second, &(ss->SA()) ), pub_size(details.size), raw_size( sizeof(private_data_block_type)+details.size*sizeof(contents) ), name( details.names.first=="" ? NTUtils::GetGUID() : details.names.first ) {
			// Use the swap-file for backing the memory-mapped file.
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			Create(file, &(ss->SA()), protection);
			if (creator) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr=0;
			}
			++reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr;
		}

		template < class contents > inline RawSharedMemory<contents>::~RawSharedMemory(void) {
			DEBUG_ASSERT(reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr);
			--reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr;
			Delete();
		}

		template < class contents > const std::string &RawSharedMemory<contents>::Name(void) const {
			return name;
		}

		template < class contents > unsigned long RawSharedMemory<contents>::Capacity(void) const {
			return pub_size;
		}

		template < class contents > inline bool RawSharedMemory<contents>::Capacity(const unsigned long sz, const DWORD protection, HANDLE file) {
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			if (reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr==1) {
				// Can only re-size if there is only one process connected to the shared
				// memory area, otherwise the call to "Create(...)" will simply return the
				// existinfg one.
				// Get the total size - public and private.
				std::auto_ptr<contents> tmp(new contents[raw_size]);
				// Use our "pointer-to-everything".
				memcpy(tmp.get(), rw_raw_data, raw_size);
				Delete();
				unsigned long old_raw_size=raw_size;
				raw_size=sz*sizeof(contents)+sizeof(private_data_block_type);
				if (Create(file, &(ss->SA()), protection)) {
					std::cerr << "RawSharedMemory<...>::Capacity(...) : Failed to re-size the shared memory area to: " << raw_size << ", named: '" << name << "' - 'Create(...)' failed." << std::endl;
					return true;
				}
				memcpy(rw_raw_data, tmp.get(), old_raw_size);
				return false;
			}
			std::cerr << "RawSharedMemory<...>::Capacity(...) : Failed to re-size the shared memory area to: " << raw_size << ", named: '" << name << "' - reference count of: "<< reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr << ", must be 1." << std::endl;
			return true;
		}

		template < class contents > bool RawSharedMemory<contents>::IsCreator(void) const {
			return creator;
		}

		// See warning by "MapDataPtr(...)".
		template < class contents > inline RawSharedMemory<contents>::operator const contents *() const {
			return ro_pub_data;
		}

		// See warning by "MapDataPtr(...)".
		template < class contents > inline RawSharedMemory<contents>::operator contents *() {
			return rw_pub_data;
		}

		template < class contents > inline NTUtils::Mutex &RawSharedMemory<contents>::Lock(void) {
			return mutex;
		}

		template < class contents > inline std::ostream &RawSharedMemory<contents>::operator<<(std::ostream &strm) const {
			unsigned long i=0;
			do {
// ???
//				strm << ro_pub_data[i];
			} while (++i<pub_size);
			return strm;
		}

		template < class contents > inline std::istream &RawSharedMemory<contents>::operator>>(std::istream &strm) {
			std::cerr << "RawSharedMemory<...>::operator>>(...) : Not implemented!!!" << std::endl;
			unsigned long i=0;
			do {
// ???
//				strm >> rw_pub_data[i];
			} while (++i<pub_size);
			return strm;
		}

		template < class contents > inline unsigned long RawSharedMemory<contents>::UsageCount(void) const {
			return usage_ctr;
		}

		template < class contents > inline RawSharedMemory<contents>::RawSharedMemory(const unsigned long p, const HeapID &details, const DWORD protection, HANDLE file) : ss( details.security ), mutex( details.names.second=="" ? NTUtils::GetGUID() : details.names.second, &(ss->SA()) ), pub_size(details.size), raw_size( p+sizeof(private_data_block_type)+details.size*sizeof(contents) ), name( details.names.first=="" ? NTUtils::GetGUID() : details.names.first ) {
			// Use the swap-file for backing the memory-mapped file.
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			Create(file, &(ss->SA()), protection);
			if (creator) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr=0;
			}
			++reinterpret_cast<private_data_block_type *>(rw_raw_data)->usage_ctr;
		}

		template < class contents > inline contents *RawSharedMemory<contents>::MapDataPtr(HANDLE mapping, const DWORD access, const DWORD hi_offset, const DWORD low_offset, const unsigned long size) {
			// Note this maps the pointer into the current process space. Therefore we need to
			// re-map it as required, as different processes may be calling for the mapping.
			// Note therefore that this handle is only valid in the context of the current process.
			// Also note that the map may not work if the underlying memory mapped file has lower access
			// than the requested access.
			contents *raw_data=static_cast<contents *>(::MapViewOfFile(mapping, access, hi_offset, low_offset, size));
			if (!raw_data) {
				unsigned long err=::GetLastError();
				std::cerr << "RawSharedMemory<...>::MapDataPtr(...) : Failed to map view of file. Name: '" << name
					<< "'. With access: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex)
					<< access << " and offset: 0x" << hi_offset << ":" << low_offset << ", size: 0x"
					<< size << ". Windows error: '" << NTUtils::DumpWinMessage(err) << "'." << std::endl;
			}
			return raw_data;
		}

		template < class contents > inline void RawSharedMemory<contents>::UnmapDataPtr(const contents *raw_data) {
			if (raw_data) {
				::UnmapViewOfFile(raw_data);
			}
		}

		template < class contents > inline bool RawSharedMemory<contents>::Create(HANDLE file, const SECURITY_ATTRIBUTES * const security, DWORD protection) {
			SYSTEM_INFO info;
			::GetSystemInfo(&info);
			// Need to expand the private data area to align it on a virtual memory granularity boundary for the later mapped views.
			raw_size=((raw_size-pub_size*sizeof(contents))/info.dwAllocationGranularity+1)*info.dwAllocationGranularity+pub_size*sizeof(contents);
			rw_raw_mapping=::CreateFileMapping(file, const_cast<SECURITY_ATTRIBUTES * const>(security), protection, 0, raw_size, name.c_str());
			if (!rw_raw_mapping || rw_raw_mapping==INVALID_HANDLE_VALUE) {
				std::cerr << "RawSharedMemory<...>::Create() : Failed to allocate the read/write raw memory mapped file. Size: " << raw_size << ", name: '" << name << "'." << std::endl;
				return true;
			} else {
				creator=(::GetLastError() != ERROR_ALREADY_EXISTS);
				// We'll create another connection to the memory mapped file to allow creating the read-only
				// mapped view of the file.
				rw_pub_mapping=::CreateFileMapping(file, const_cast<SECURITY_ATTRIBUTES * const>(security), protection, 0, raw_size, name.c_str());
				ro_pub_mapping=::CreateFileMapping(file, const_cast<SECURITY_ATTRIBUTES * const>(security), protection, 0, raw_size, name.c_str());
				// We get access to everything - private, public, you name it....
				rw_raw_data=MapDataPtr(rw_raw_mapping, FILE_MAP_ALL_ACCESS, 0, 0, 0);
				// Make sure joe public can't stamp over any private data.
				rw_pub_data=MapDataPtr(rw_pub_mapping, FILE_MAP_ALL_ACCESS, 0, raw_size-pub_size*sizeof(contents), pub_size*sizeof(contents));
				ro_pub_data=MapDataPtr(ro_pub_mapping, FILE_MAP_READ, 0, raw_size-pub_size*sizeof(contents), pub_size*sizeof(contents));
				if (!rw_raw_data || !rw_pub_data || !ro_pub_data) {
					Delete();
					return true;
				}
			}
			return false;
		}

		template < class contents > inline void RawSharedMemory<contents>::Delete(void) {
			UnmapDataPtr(ro_pub_data);
			UnmapDataPtr(rw_pub_data);
			UnmapDataPtr(rw_raw_data);
			::CloseHandle(ro_pub_mapping);
			::CloseHandle(rw_pub_mapping);
			::CloseHandle(rw_raw_mapping);
		}

		template < class contents > inline unsigned long RawSharedMemory<contents>::GetEndPrivateDataBlock(void) {
			return sizeof(private_data_block_type);
		}

		template < class contents > inline ManagedSharedMemory<contents>::ManagedSharedMemory(const std::string &mgr_name, const HeapID &details, const DWORD protection, HANDLE file) : RawSharedMemory<contents>( sizeof(private_data_block_type)+mgr_name.size()+1, details, protection, file) {
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			if (IsCreator()) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->used_memory=0;
				memcpy(&(reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name), mgr_name.c_str(), mgr_name.size()+1);
			} else if (strcmp(&(reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name), mgr_name.c_str())) {
				std::cerr << "ManagedSharedMemory<...>::ManagedSharedMemory(...) : Attempting to use wrong memory manager '"<< mgr_name << "', on existing shared memory, which is managed by '" << &(reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name) << "'." << std::endl;
				throw;
			}
		}

		template < class contents > inline ManagedSharedMemory<contents>::~ManagedSharedMemory(void) {
		}

		template < class contents > inline contents *ManagedSharedMemory<contents>::Allocate(const unsigned long n, const void * const hint) {
			contents *ptr;
			if (ptr=BaseAllocate(n, hint)) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->used_memory+=n;
			}
			return ptr;
		}

		template < class contents > inline void ManagedSharedMemory<contents>::Deallocate(const contents * const p, const unsigned long n) {
			unsigned long amnt;
			if (amnt=BaseDeallocate(p, n)) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->used_memory-=amnt;
			}
		}

		template < class contents > inline void ManagedSharedMemory<contents>::Construct(contents * const ptr,const contents &val) {
			DEBUG_ASSERT(ptr);
// ???
			// Problem:
			// The pointer "ptr" is uninitialised memory, by definition of "Allocate".
			// So if it points to an object that contains a non-default allocator, e.g. an SM STL string,
			// how do I "kick-start" the allocator object? I could try:
//			memcpy(ptr, &val, sizeof(val));
			// But this links "ptr" to the contents of the std::string, which means that the reference counts
			// on it are one less than it should be.
//			ptr->get_allocator();
/*			ptr->ss=val.ss;
			ptr->mutex=Mutex(val.Name(), ss->SA());
			ptr->pub_size=val.pub_size;
			ptr->raw_size=sizeof(private_data_block_type)+val.pub_size*sizeof(contents);
			ptr->name=val.name;
*/
			// This leaks memory...
			contents tmp;
			memcpy(ptr, &tmp, sizeof(contents));
			// This won't work as the heap in "ptr" is uninitialised.
			*ptr=val;
//			DEBUG_ASSERT(false);
//			throw "Don't try this - it's not implemented!";
		}

		template < class contents > inline ManagedSharedMemory<contents>::ManagedSharedMemory(const unsigned long p, const std::string &mgr_name, const HeapID &details, const DWORD protection, HANDLE file) : RawSharedMemory<contents>( p+sizeof(private_data_block_type)+mgr_name.size()+1, details, protection, file) {
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			if (IsCreator()) {
				reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->used_memory=0;
				memcpy(&(reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name), mgr_name.c_str(), mgr_name.size()+1);
			} else if (strcmp(&(reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name), mgr_name.c_str())) {
				std::cerr << "ManagedSharedMemory<...>::ManagedSharedMemory(...) : Attempting to use wrong memory manager '"<< mgr_name << "', on existing shared memory, which is managed by '" << reinterpret_cast<private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name << "'." << std::endl;
				throw;
			}
		}

		template < class contents > inline unsigned long ManagedSharedMemory<contents>::GetEndPrivateDataBlock(void) {
			return RawSharedMemory<contents>::GetEndPrivateDataBlock()+sizeof(private_data_block_type);
		}

		template < class contents > inline CrapManagedSharedMemory<contents>::CrapManagedSharedMemory(const HeapID &details) : ManagedSharedMemory<contents>( 2*sizeof(NTUtils::guid_type), typeid(this).name(), details, PAGE_READWRITE, INVALID_HANDLE_VALUE), in_use_list( HeapID(details.size*(1+sizeof(allocated_data_block_info)/sizeof(unsigned long)), GetMgrAreaName(IsCreator(), rw_raw_data), ss), PAGE_READWRITE, INVALID_HANDLE_VALUE ) {
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			if (IsCreator()) {
				memset((unsigned long *)in_use_list, CMSM_free_marker, in_use_list.Capacity()*sizeof(unsigned long));
			}
		}
		
		template < class contents > inline CrapManagedSharedMemory<contents>::~CrapManagedSharedMemory(void) {
#ifdef _SMEM_DEBUG
#ifdef _DEBUG
			if (reinterpret_cast<RawSharedMemory<contents>::private_data_block_type *>(rw_raw_data)->usage_ctr==1) {
				// Check that all memory is freed.
				const unsigned long *used=(const unsigned long *)in_use_list;
				unsigned long s=0;
				do {
					unsigned long beginning_of_block=CMSM_free_marker;
					if (beginning_of_block!=used[s+sizeof(allocated_data_block_info)/sizeof(unsigned long)]) {
						beginning_of_block=used[s+sizeof(allocated_data_block_info)/sizeof(unsigned long)];
					}
					if (beginning_of_block!=CMSM_free_marker) {
						std::cerr << "Allocating process ID: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_pid << std::endl;
						std::cerr << "Allocating process handle: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_phandle << std::endl;
						std::cerr << "Allocating thread ID: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_tid << std::endl;
						std::cerr << "Allocating thread handle: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_thandle << std::endl;
						std::cerr << "Allocating process name: '" << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_pname << "'" << std::endl;
						std::cerr << "Line number in allocating file: " << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_line << std::endl;
						std::cerr << "Allocating source file name: '" << reinterpret_cast<const allocated_data_block_info *>(used+s)->allocating_src << "'" << std::endl;
						std::cerr << "Address: 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << ((contents *)*this)+s << std::endl;
						std::cerr << "Block size: " << (reinterpret_cast<const allocated_data_block_info *>(used+s)->block_size<<2) << " bytes." << std::endl;
						std::cerr << "Data: <";
						const unsigned long start=s;
						while (beginning_of_block==used[s+sizeof(allocated_data_block_info)/sizeof(unsigned long)]) {
							std::cerr << *reinterpret_cast<char *>(&(((contents *)*this)[s]));
							++s;
						}
						std::cerr << ">" << std::endl;
						std::cerr << "Hex: <";
						for (s=start;beginning_of_block==used[s];++s) {
							std::cerr << " 0x" << std::setw(8) << std::setfill('0') << std::setbase(std::ios_base::hex) << *reinterpret_cast<unsigned long *>(&(((contents *)*this)[s]));
						}
						std::cerr << ">" << std::endl;
					}
					s+=sizeof(allocated_data_block_info)/sizeof(unsigned long);
					DEBUG_ASSERT(used[s]==CMSM_free_marker);
				} while (++s<in_use_list.Capacity());
			}
#endif _DEBUG
#endif _SMEM_DEBUG
		}

		template < class contents > inline unsigned long CrapManagedSharedMemory<contents>::Capacity(void) const {
			return ManagedSharedMemory<contents>::Capacity();
		}

		template < class contents > inline contents *CrapManagedSharedMemory<contents>::BaseAllocate(const unsigned long m, const void * const /*hint*/) {
			// Need to find a block of memory of the correct size that is not in use.
			// Warning: Crap algorithm!!!
			// Poxy linear search coming up!
			// Note - "m" in units of "sizeof(contents)" rounded up.
			if (m>Capacity()) {
				// Ran out of memory...
				return NULL;
			}
			// Search through the in-use list for the first block of memory that is big enough.
			NTUtils::MutexLock lock(in_use_list.Lock().Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			__asm {
				int 3
			}
			unsigned long *used=(unsigned long *)in_use_list;
			unsigned long s=sizeof(allocated_data_block_info)/sizeof(unsigned long);
			do {
				unsigned long e=s;
				// Find a free element.
				if (used[s]==CMSM_free_marker) {
					// It's a free element.
					// Is there enough free memory?
					if ((s+m)<=in_use_list.Capacity()) {
						// Now need to see if the block it begins is big enough...
						e=s;
						bool big_enough=true;
						do {
							if (used[e]!=CMSM_free_marker) {
								// Block too small.
								big_enough=false;
								s=e;
								break;
							}
						} while (++e<(s+m));
						if (big_enough) {
							// Mark each element as used in this block by using the value of the first
							// pointer in the block.
#ifdef _SMEM_DEBUG
#ifdef _DEBUG
							// "I did this!"
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_pid=::GetCurrentProcessId();
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_phandle=::GetCurrentProcess();
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_tid=::GetCurrentThreadId();
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_thandle=::GetCurrentThread();
							::GetModuleFileName(NULL, reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_pname, MAX_PATH);
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->block_size=m;
// ???
							// For future use. Currently unused. Put something in anyway.
							reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_line=__LINE__;
							strcpy(reinterpret_cast<allocated_data_block_info *>(used+s-sizeof(allocated_data_block_info)/sizeof(unsigned long))->allocating_src,__FILE__);
#endif _DEBUG
#endif _SMEM_DEBUG
							e=s;
							do {
								used[e]=s;
							} while (++e<(s+m));
							return ((contents *)*this)+s;
						}
					} else {
						// Ran out of memory...
						return NULL;
					}
				} else {
#ifdef _SMEM_DEBUG
#ifdef _DEBUG
					s+=sizeof(allocated_data_block_info)/sizeof(unsigned long)+reinterpret_cast<allocated_data_block_info *>(used-s)->block_size;
#else
					++s;
#endif _DEBUG
#endif _SMEM_DEBUG
				}
			} while (++s<in_use_list.Capacity());
			return NULL;
		}

		// Note: "m" in units of "contents".
		template < class contents > inline unsigned long CrapManagedSharedMemory<contents>::BaseDeallocate(const contents * const p, const unsigned long m) {
			// "p" must be from the same process space.
			NTUtils::MutexLock lock(in_use_list.Lock().Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			// NOTE: Can't deallocate a "const" pointer (as in "(const contents * const)"), only non-const. Hence the cast and type declaration.
			std::cerr<<p<<" "<<m<<std::endl;
			contents * const beginning=(contents *)*this;
			__asm {
				int 3
			}
			DEBUG_ASSERT(p>=beginning);
			DEBUG_ASSERT(m*sizeof(contents)<Capacity());
			const unsigned long mgr_ptr=p-beginning;
			DEBUG_ASSERT((mgr_ptr+m*sizeof(contents))<Capacity());
			unsigned long *used=(unsigned long *)in_use_list;
			DEBUG_ASSERT(mgr_ptr<in_use_list.Capacity());
			DEBUG_ASSERT((mgr_ptr+m*sizeof(contents))<in_use_list.Capacity());
			std::cerr << used[(mgr_ptr+(m-1)*sizeof(allocated_data_block_info))/sizeof(unsigned long)] << " " << mgr_ptr << std::endl;
			DEBUG_ASSERT(used[(mgr_ptr+m*sizeof(allocated_data_block_info))/sizeof(unsigned long)]==mgr_ptr);
			unsigned long s=mgr_ptr+sizeof(allocated_data_block_info)/sizeof(unsigned long);
			do {
				if (used[s]!=mgr_ptr) {
					// End of block found.
					// The block should be of the right size.
					unsigned long i=(s-mgr_ptr-sizeof(allocated_data_block_info)/sizeof(unsigned long));
					std::cerr<<i<<std::endl;
					i/=sizeof(contents);
					std::cerr<<i<<std::endl;
//					DEBUG_ASSERT(!m || ((s-mgr_ptr-sizeof(allocated_data_block_info)/sizeof(unsigned long))/sizeof(contents)==m));
					// Mark the elements in the block as free.
					memset(used+mgr_ptr, CMSM_free_marker, (s-mgr_ptr)*sizeof(unsigned long)+sizeof(allocated_data_block_info));
#ifdef _DEBUG
					// Zero the main memory, so that in debug mode I can see it being released.
					memset(beginning+mgr_ptr, 0, (s-mgr_ptr)*sizeof(contents));
#endif _DEBUG
					return s-mgr_ptr;
				}
				// Check the block's integrity.
				// It should be in use by someone.
				DEBUG_ASSERT(used[s]!=CMSM_free_marker);
			} while (++s<in_use_list.Capacity());
			// Couldn't find end of the block.
			DEBUG_ASSERT(false);
			return 0;
		}

		template < class contents > inline std::pair<std::string, std::string> CrapManagedSharedMemory<contents>::GetMgrAreaName(const bool first, contents * const rw_raw_data) {
			std::pair<std::string, std::string> names;
			if (first) {
				names.first=NTUtils::GetGUID();
				names.second=NTUtils::GetGUID();
				// Write the names into the beginning of the raw data area for other processes to access.
				memcpy(reinterpret_cast<private_data_block_type *>(rw_raw_data+ManagedSharedMemory<contents>::GetEndPrivateDataBlock()+strlen(&(reinterpret_cast<ManagedSharedMemory<contents>::private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name)))->mem_id, names.first.c_str(), sizeof(NTUtils::guid_type));
				memcpy(reinterpret_cast<private_data_block_type *>(rw_raw_data+ManagedSharedMemory<contents>::GetEndPrivateDataBlock()+strlen(&(reinterpret_cast<ManagedSharedMemory<contents>::private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name)))->mtx_id, names.second.c_str(), sizeof(NTUtils::guid_type));
			} else {
				// The names are at the beginning of the shared raw data area. Extract it.
				NTUtils::guid_type raw_name;
				memcpy(raw_name, reinterpret_cast<private_data_block_type *>(rw_raw_data+ManagedSharedMemory<contents>::GetEndPrivateDataBlock()+strlen(&(reinterpret_cast<ManagedSharedMemory<contents>::private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name)))->mem_id, sizeof(NTUtils::guid_type));
				DEBUG_ASSERT(strlen(raw_name)==(sizeof(NTUtils::guid_type)-1));
				names.first=raw_name;
				memcpy(raw_name, reinterpret_cast<private_data_block_type *>(rw_raw_data+ManagedSharedMemory<contents>::GetEndPrivateDataBlock()+strlen(&(reinterpret_cast<ManagedSharedMemory<contents>::private_data_block_type *>(rw_raw_data+RawSharedMemory<contents>::GetEndPrivateDataBlock())->manager_name)))->mtx_id, sizeof(NTUtils::guid_type));
				DEBUG_ASSERT(strlen(raw_name)==(sizeof(NTUtils::guid_type)-1));
				names.second=raw_name;
			}
			return names;
		}

		template < class contents > inline bool CrapManagedSharedMemory<contents>::Capacity(const unsigned long, const DWORD, HANDLE) {
			std::cerr << "CrapManagedSharedMemory<...>::Capacity(...) : Not supported!!!" << std::endl;
			return true;
		}

		template < class contents > inline KnRManagedSharedMemory<contents>::KnRManagedSharedMemory(const HeapID &details)
			: ManagedSharedMemory<contents>( typeid(this).name(), details, PAGE_READWRITE, INVALID_HANDLE_VALUE) {
			free_list=reinterpret_cast<free_memory_list_type *>(rw_pub_data);
			free_list->next=free_list;
			free_list->size=Capacity()*sizeof(contents);
		}

		template < class contents > inline KnRManagedSharedMemory<contents>::~KnRManagedSharedMemory(void) {
#ifdef _DEBUG
			if (reinterpret_cast<RawSharedMemory<contents>::private_data_block_type *>(rw_raw_data)->usage_ctr==1) {
				// Check that all memory is freed.
				DEBUG_ASSERT(free_list->size==Capacity());
			}
#endif _DEBUG
		}

		template < class contents > inline unsigned long KnRManagedSharedMemory<contents>::Capacity(void) const {
			return ManagedSharedMemory<contents>::Capacity();
		}

		template < class contents > inline contents *KnRManagedSharedMemory<contents>::BaseAllocate(const unsigned long m, const void * const /*hint*/) {
			// Need to find a block of memory of the correct size that is not in use.
			// Note - "m" in units of "sizeof(contents)" rounded up.
			if (m>Capacity()) {
				// Ran out of memory...
				return NULL;
			}
			__asm {
				int 3
			}
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			const unsigned long nunits=m*sizeof(contents)+sizeof(free_memory_list_type);
			free_memory_list_type *prevp=free_list;
			// (Linear) Search through the list to find a free block.
			for (free_memory_list_type *p=prevp->next; ;prevp=p, p=p->next) {
				if (p->size>=nunits) {
					// The block is big enough.
					if (p->size==nunits) {
						// Exactly big enough.
						prevp->next=p->next;
					} else {
						// Allocate tail end.
						p->size-=nunits;
						p+=p->size/sizeof(free_memory_list_type);
						p->size=nunits;
					}
					free_list=prevp;
#ifdef _DEBUG
					// Set the main memory, so that in debug mode I can see it being allocated.
					memset(reinterpret_cast<void *>(p+1), 0xFE, m*sizeof(contents));
#endif _DEBUG
					return reinterpret_cast<contents *>(p+1);
				}
				if (p==free_list) {
					// Wrapped around the free list! Couldn't find a big enough free block.
					return NULL;
				}
			}
			return NULL;
		}

		// Note: "m" in units of "contents".
		template < class contents > inline unsigned long KnRManagedSharedMemory<contents>::BaseDeallocate(const contents * const ptr, const unsigned long m) {
			// "p" must be from the same process space.
			NTUtils::MutexLock lock(mutex.Name(), INFINITE, const_cast<SECURITY_ATTRIBUTES * const>(&(ss->SA())));
			// NOTE: Can't deallocate a "const" pointer (as in "(const contents * const)"), only non-const. Hence the cast and type declaration.
			free_memory_list_type *p;
			free_memory_list_type * const bp=reinterpret_cast<free_memory_list_type * const>(const_cast<contents * const>(ptr))-1;
			__asm {
				int 3
			}
#ifdef _DEBUG
			// Ensure the block size is correct.
			DEBUG_ASSERT(m || (bp->size==m*sizeof(contents)+sizeof(free_memory_list_type)));
			// Set the main memory, so that in debug mode I can see it being allocated.
			memset(const_cast<contents * const>(ptr), 0xFD, m*sizeof(contents));
#endif _DEBUG
			for (p=free_list; !(bp>p && bp<p->next); p=p->next) {
				if (p>=p->next && (bp>p || bp<p->next)) {
					break;	// Freed block at the start or end of arena.
				}
			};
			if (bp+bp->size/sizeof(free_memory_list_type)==p->next) {	// Join to upper neighbour.
				bp->size+=p->next->size;
				bp->next=p->next->next;
			} else {
				bp->next=p->next;
			}
			if (p+p->size/sizeof(free_memory_list_type)==bp) {	// Join to lower neighbour.
				p->size+=bp->size;
				p->next=bp->next;
			} else {
				p->next=bp;
			}
			free_list=p;
			return bp->size/sizeof(contents);
		}

		template < class contents > inline bool KnRManagedSharedMemory<contents>::Capacity(const unsigned long, const DWORD, HANDLE) {
			std::cerr << "KnRManagedSharedMemory<...>::Capacity(...) : Not supported!!!" << std::endl;
			return true;
		}

		template < class heap_manager, unsigned long id > inline ManagedSharedMemorySTLIntf<heap_manager, id>::ManagedSharedMemorySTLIntf(const HeapID &details) : heap_manager(details) {
		}

		template < class heap_manager, unsigned long id > inline ManagedSharedMemorySTLIntf<heap_manager, id>::~ManagedSharedMemorySTLIntf(void) {
		}

		template < class heap_manager, unsigned long id > inline ManagedSharedMemorySTLIntf<heap_manager, id>::ManagedSharedMemorySTLIntf(const unsigned long p, const HeapID &details) : heap_manager(p, details) {
		}

	}

} }
