/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#include<winbase.h>	// Ensure the SYSTEM_INFO structure is defined.

#include<lmcons.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	inline tstring __fastcall GetLocalUsrName(void) {
		register tchar tmp[UNLEN+1];
		register unsigned long size=sizeof(tmp)/sizeof(tchar);
		::GetUserName(tmp,&size);
		DEBUG_ASSERT(tmp && size);
		return tmp;
	}

	inline tstring __fastcall GetLocalPCName(void) {
		register tchar tmp[MAX_COMPUTERNAME_LENGTH+1];
		register unsigned long size=sizeof(tmp)/sizeof(tchar);
		::GetComputerName(tmp,&size);
		DEBUG_ASSERT(tmp && size);
		return tmp;
	}

	inline void __fastcall GetLocalUsrInfo(tstring &unc,tstring &user) {
		unc=GetLocalPCName();
		user=GetLocalUsrName();
	}

	inline bool __fastcall OnNT(void) noexcept(true) {
		register OSVERSIONINFO osinfo;
		osinfo.dwOSVersionInfoSize=sizeof(OSVERSIONINFO);
		return ::GetVersionEx(&osinfo) && osinfo.dwPlatformId==VER_PLATFORM_WIN32_NT;
	}

} } }
