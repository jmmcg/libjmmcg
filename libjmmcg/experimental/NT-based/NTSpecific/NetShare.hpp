/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "SecurityDescriptor.hpp"

#include "../../../core/ttypes.hpp"

// Networking functionality such as browsing.
#include<lm.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

/////////////////////////////////////////////////////////////////////////////

const tchar server_info_9X_lib_name[]=_T("svrapi.dll");
const tchar dir_separator=_T('\\');

/////////////////////////////////////////////////////////////////////////////

class AFX_EXT_CLASS NetShare : virtual protected LoadLibraryWrapper
{
public:
	__stdcall NetShare(void);
	__stdcall NetShare(const tstring &DirectoryToShare,const TCHAR * const Sharename,const TCHAR * const description,const TCHAR * const Server,const DWORD max_connections,SECURITY_DESCRIPTOR *sd,const DWORD share_perms);
	__stdcall NetShare(const tstring &DirectoryToShare,const TCHAR * const Sharename,const TCHAR * const description,const TCHAR * const Username,const TCHAR * const Server,const DWORD access_mask,const DWORD max_connections,const DWORD share_perms);
	__stdcall ~NetShare(void);

	bool __fastcall Create(const tstring &DirectoryToShare,const TCHAR * const Sharename,const TCHAR * const description,const TCHAR * const Server,const DWORD max_connections,const SECURITY_DESCRIPTOR * const sd,const DWORD share_perms);
	bool __fastcall Create(const tstring &DirectoryToShare,const TCHAR * const Sharename,const TCHAR * const description,const TCHAR * const Username,const TCHAR * const Server,const DWORD access_mask,const DWORD max_connections,const DWORD share_perms);
	bool __fastcall Delete(void);

private:
	typedef NET_API_STATUS (NET_API_FUNCTION * const NetShareAddType)(IN LPCWSTR servername,IN DWORD level,IN LPBYTE buf,OUT LPDWORD parm_err);
	typedef NET_API_STATUS (NET_API_FUNCTION * const NetShareDelType)(IN LPCWSTR servername,IN LPCWSTR netname,IN DWORD reserved);

	const NetShareAddType pNetShareAdd;
	const NetShareDelType pNetShareDel;

	std::wstring machine_w,sharename_w;
	bool deleted;
	SecurityDescriptor sd;

	// Stop any compiler silliness...
	__stdcall NetShare(const NetShare &);
	NetShare & __fastcall operator=(const NetShare &);
};

} } }
