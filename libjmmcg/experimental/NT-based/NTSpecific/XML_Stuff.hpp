// TITLE:
//
// AUTHOR:
// Created by J.M.McGuiness, E-mail: coder@hussar.me.uk
//
// DESCRIPTION:
// Note: You need to put the line "#import <msxml4.dll> implementation_only" in the "stdafx.cpp" or you'll get link errors with the MSXML stuff....
// (I need to do this as the more simple route of omitting the "no_implementation" in this file leads to those link errors anyway, if the "#import <msxml4.dll>" is put in "stdafx.h".
//
// LEGALITIES:
// Copyright © 2004 by J.M.McGuiness, all rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
/////////////////////////////////////////////////////////////////////////////

#import<msxml6.dll> no_implementation

#include"exception.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils { namespace XML {

	typedef win_exception exception_type;

	typedef enum {
		attribute,
		document,
		element,
		text
	} node_types;

	inline MSXML2::IXMLDOMNodePtr __fastcall CreateNode(const node_types type,const tchar *node_name,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns);
	inline MSXML2::IXMLDOMNodePtr __fastcall CreateNode(const node_types type,const tchar *node_name,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns,const MSXML2::IXMLDOMNodePtr &node);
	inline MSXML2::IXMLDOMNodePtr __fastcall CreateComment(const tchar *node_name,const tchar *comment,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns,const MSXML2::IXMLDOMNodePtr &node);
	inline tstring __fastcall GetParseError(const MSXML2::IXMLDOMDocumentPtr &doc);

	inline void __fastcall ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const POINT &pt,const tchar *xml_ns);
	inline void __fastcall FromXML(const MSXML2::IXMLDOMNodePtr &n,POINT &pt);
	inline void __fastcall ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const RECT &rect,const tchar *xml_ns);
	inline void __fastcall FromXML(const MSXML2::IXMLDOMNodePtr &n,RECT &rect);
	template<typename Value> inline void __fastcall ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<Value> &colln,const tchar *xml_ns);
	template<> inline void __fastcall ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<double> &colln,const tchar *xml_ns);
	template<> inline void __fastcall ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<CPoint> &colln,const tchar *xml_ns);
	template<typename Value> inline void __fastcall FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<Value> &colln);
	template<> inline void __fastcall FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<double> &colln);
	template<> inline void __fastcall FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<CPoint> &colln);

	class DocWrapper {
	public:
		inline __stdcall DocWrapper(void);
		inline __stdcall DocWrapper(const DocWrapper &);
		virtual inline __stdcall ~DocWrapper(void);
		
		inline const MSXML2::IXMLDOMDocumentPtr & __fastcall Doc(void) const noexcept(true);
		
		inline void __fastcall LoadXML(const tchar *xml);
		inline void __fastcall Load(const tchar *filename);
		inline void __fastcall Load(const IStreamPtr &file);
		inline void __fastcall Save(const tchar *filename);

	private:
		const MSXML2::IXMLDOMDocumentPtr doc;

		static inline MSXML2::IXMLDOMDocumentPtr __fastcall MakeDoc(void);

		// I don't allow assignment.
		inline DocWrapper & __fastcall operator=(const DocWrapper &);
	};

	class DocNS : public DocWrapper {
	public:
		inline __stdcall DocNS(const tstring &ns=_T(""))
		: DocWrapper(),n_space(ns) {
		}
		inline __stdcall DocNS(const DocNS &d)
		: DocWrapper(d),n_space(d.n_space) {
		}
		virtual inline __stdcall ~DocNS(void) {
		}

		inline const tstring & __fastcall
		Namespace(void) const noexcept(true) {
			return n_space;
		}

		inline MSXML2::IXMLDOMNodePtr __fastcall
		CreateNode(const node_types type,const tchar *node_name,const MSXML2::IXMLDOMNodePtr &node) {
			return XML::CreateNode(type,node_name,Doc(),n_space.c_str(),node);
		}

		inline MSXML2::IXMLDOMNodePtr __fastcall
		CreateComment(const tchar *node_name,const tchar *comment,const MSXML2::IXMLDOMNodePtr &node) {
			return XML::CreateComment(node_name,comment,Doc(),n_space.c_str(),node);
		}

		inline void __fastcall
		ToXML(const MSXML2::IXMLDOMNodePtr &n,const POINT &pt) {
			XML::ToXML(Doc(),n,pt,n_space.c_str());
		}
		inline void __fastcall
		ToXML(const MSXML2::IXMLDOMNodePtr &n,const RECT &rect) {
			XML::ToXML(Doc(),n,rect,n_space.c_str());
		}
		template<typename Value> inline void __fastcall
		ToXML(const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<Value> &colln) {
			XML::ToXML(Doc(),n,colln_name,colln,n_space.c_str());
		}

	private:
		const tstring n_space;

		// I don't allow assignment.
		inline DocNS & __fastcall operator=(const DocNS &);
	};

} } } }
