/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../../core/exception.hpp"

#include <cassert>
#include <iomanip>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A class for providing timings in high-performance counter resolution.
template <
	typename Mdl_
>
class hp_timer<ppd::generic_traits::MS_Win32,Mdl_> {
public:
	typedef ppd::api_lock_traits<ppd::generic_traits::MS_Win32,Mdl_> lock_traits;
	typedef typename lock_traits::exception_type exception_type;
	/// The units are microseconds.
	typedef long long value_type;
	typedef FILETIME time_utc_t;
	static inline constexpr const unsigned long Hz_to_usec=1000000;	///< The frequency is in Hz, so convert to usec.

	/**
		In Hz. Set at program start-up, once.
	*/
	const value_type frequency;	///< In Hz.

	/**
		Time when the process started up.
	*/
	const time_utc_t start_up_time;

	/**
		Count in usec when the process started up.
	*/
	const value_type start_up_count;	///< In usec.

	__stdcall hp_timer();

	/// Return the current time in a resolution of 1/frequency units.
	/**
		Note that this will have a systematic offset according
to the properties of GetSystemTimeAsFileTime().

		\return Time (NOT local time!) in UTC.
	*/
	const time_utc_t __fastcall current_time() const noexcept(true);

	/// Convert a value_type in units of 1/frequency to usec.
	const value_type __fastcall to_usec(const time_utc_t ticks) const noexcept(true);

	/// The current count in units of 1/frequency.
	static const value_type __fastcall current_count() noexcept(true);

	/**
		\return In Hz.
	*/
	static const value_type __fastcall get_frequency();

	/**
		Implemented using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend std::ostream & __fastcall operator<<(std::ostream &o, const time_utc_t &t);

private:
	static const time_utc_t __fastcall get_start_time();

	void operator=(hp_timer const &)=delete;
};

template <typename Mdl_>
inline const typename hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::value_type
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::to_usec(const time_utc_t ticks) const {
	const ULARGE_INTEGER ft={
		ticks.dwLowDateTime,
		ticks.dwHighDateTime
	};
	return static_cast<value_type>(static_cast<double>(ft.QuadPart)*Hz_to_usec/frequency);
}

template <typename Mdl_>
inline const typename hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::value_type
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::current_count() {
	LARGE_INTEGER Finish;
	::QueryPerformanceCounter(&Finish);
	DEBUG_ASSERT(Finish.QuadPart>=0i64);
	return static_cast<value_type>(Finish.QuadPart);
}

template <typename Mdl_>
inline const typename hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::time_utc_t
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::current_time() const {
	const value_type cur_ct=current_count();
	DEBUG_ASSERT(cur_ct>=start_up_count);
	const value_type usec_since_start=cur_ct-start_up_count;
	DEBUG_ASSERT(usec_since_start>=0);
	const value_type to_ft_100ns=usec_since_start*10;
	ULARGE_INTEGER curr_ft={
		start_up_time.dwLowDateTime,
		start_up_time.dwHighDateTime
	};
	curr_ft.QuadPart+=to_ft_100ns;
	const time_utc_t ret={
		curr_ft.LowPart,
		curr_ft.HighPart
	};
	return ret;
}

template <typename Mdl_>
inline const typename hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::value_type
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::get_frequency() {
	LARGE_INTEGER freq;
	freq.QuadPart=0i64;
	const BOOL ret=::QueryPerformanceFrequency(&freq);
	if (!ret || (freq.QuadPart==0i64)) {
		throw exception_type(_T("High-performance counter not supported."), info::function(__LINE__,__PRETTY_FUNCTION__,typeid(&hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::get_frequency)), JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
	}
	return static_cast<value_type>(freq.QuadPart);
}

template <typename Mdl_>
inline const typename hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::time_utc_t
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::get_start_time() {
	time_utc_t tm;
	::GetSystemTimeAsFileTime(&tm);
	return tm;
}

template <typename Mdl_>
inline
hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::hp_timer()
: frequency(get_frequency()),
	start_up_time(get_start_time()),
	start_up_count(current_count()) {
}

template <typename T>
inline
hp_interval<T>::~hp_interval() noexcept(true) {
	const typename timer_t::time_utc_t end=timer.current_time();
	const ULARGE_INTEGER start_ft={
		start.dwLowDateTime,
		start.dwHighDateTime
	};
	ULARGE_INTEGER end_ft={
		end.dwLowDateTime,
		end.dwHighDateTime
	};
	end_ft.QuadPart-=start_ft.QuadPart;
	interval.dwLowDateTime=end_ft.LowPart;
	interval.dwHighDateTime=end_ft.HighPart;
}

} }

inline std::ostream &__fastcall
operator<<(std::ostream &ss, const SYSTEMTIME &st) {
	std::ios_base::iostate err=std::ios_base::goodbit;
	try {
		const std::ostream::sentry opfx(ss);
		if (opfx) {
			ss<<std::setiosflags(std::ios::right)
				<<std::setfill('0')
				<<std::setw(2)<<st.wDay<<"/"
				<<std::setw(2)<<st.wMonth<<"/"
				<<std::setw(1)<<st.wYear
				<<","
				<<std::setw(2)<<st.wHour<<":"
				<<std::setw(2)<<st.wMinute<<":"
				<<std::setw(2)<<st.wSecond<<"."
				<<std::setw(3)<<st.wMilliseconds;
		}
	} catch (std::bad_alloc const &) {
		err|=std::ios_base::badbit;
		const std::ios_base::iostate exception_mask=ss.exceptions();
		if ((exception_mask & std::ios_base::failbit)
			&& !(exception_mask & std::ios_base::badbit)) {
			ss.setstate(err);
		} else if (exception_mask & std::ios_base::badbit) {
			try {
				ss.setstate(err);
			} catch (std::ios_base::failure const &) {
			}
			throw;
		}
	} catch (...) {
		err|=std::ios_base::failbit;
		const std::ios_base::iostate exception_mask=ss.exceptions();
		if ((exception_mask & std::ios_base::badbit)
			&& (err & std::ios_base::badbit)) {
			ss.setstate(err);
		} else if (exception_mask & std::ios_base::failbit) {
			try {
				ss.setstate(err);
			} catch (std::ios_base::failure const &) {
			}
			throw;
		}
	}
	if (err) ss.setstate(err);
	return ss;
}

template <typename Mdl_>
inline std::ostream & __fastcall
operator<<(std::ostream &o, const typename jmmcg::LIBJMMCG_VER_NAMESPACE::hp_timer<ppd::generic_traits::MS_Win32,Mdl_>::time_utc_t &t) {
	SYSTEMTIME st;
	if (::FileTimeToSystemTime(&t,&st)) {
		o<<st;
		return o;
	} else {
		throw hp_timer<generic_traits::MS_Win32,Mdl_>::exception(_T("Failed to convert the input FILETIME to a SYSTEMTIME."), info::function(__LINE__,__PRETTY_FUNCTION__), JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
	}
}
