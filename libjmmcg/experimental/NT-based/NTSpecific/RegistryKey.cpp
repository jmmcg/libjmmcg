/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "DumpWinMsg.hpp"
#include "RegistryKey.hpp"

#include "../../../core/exception.hpp"

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

namespace {
const TCHAR* error_msgs[]= {
	_T("Registry key error."),
	_T("~RegistryKey()"),
	_T("Failed to create the registry key."),
	_T("NULL parent key."),
	_T("Failed to open the registry key."),
	_T("NULL parent key."),
	_T("Failed to set the value in the registry key.")};
}

/////////////////////////////////////////////////////////////////////////////

RegistryKey::~RegistryKey(void) {
	if(delete_key) {
		if(key.m_hKey && ::RegDeleteKey(parent, name.c_str()) != ERROR_SUCCESS) {
			throw exception_type(error_msgs[1], info::function(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T(""), _T(""))), __REV_INFO__);
		}
	}
}

inline long
RegistryKey::Create(HKEY p, const tstring& n) {
	if(p) {
		name= n;
		long ret;
		if((ret= key.Create(parent= p, name.c_str())) != ERROR_SUCCESS) {
			info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("HKEY parent"), tostring(p)));
			fn.add_arg(_T("const tstring &n"), n);
			throw exception_type(error_msgs[2], fn, __REV_INFO__);
		}
		delete_key= true;
		return ret;
	}
	info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("HKEY parent"), tostring(p)));
	fn.add_arg(_T("const tstring &n"), n);
	throw exception_type(error_msgs[3], fn, __REV_INFO__);
}

inline long
RegistryKey::Open(HKEY parent, const tstring& n) {
	delete_key= false;
	if(parent) {
		name= n;
		long ret;
		if((ret= key.Open(parent, name.c_str())) != ERROR_SUCCESS) {
			info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("HKEY parent"), tostring(parent)));
			fn.add_arg(_T("const tstring &n"), n);
			throw exception_type(error_msgs[4], fn, __REV_INFO__);
		}
		return ret;
	}
	info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("HKEY parent"), tostring(parent)));
	fn.add_arg(_T("const tstring &n"), n);
	throw exception_type(error_msgs[5], fn, __REV_INFO__);
}

inline long
RegistryKey::SetValue(const tstring& val, const tstring& key_name) {
	long ret;
	if((ret= key.SetStringValue(val.c_str(), key_name.c_str())) != ERROR_SUCCESS) {
		info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("const tstring &val"), val));
		fn.add_arg(_T("const tstring &key_name"), key_name);
		throw exception_type(error_msgs[6], fn, __REV_INFO__);
	}
	return ret;
}

inline long
RegistryKey::SetValue(const tstring& val, const tstring& key_name, const DWORD type) {
	long ret;
	if((ret= ::RegSetValueEx(key.m_hKey, key_name.c_str(), NULL, type, reinterpret_cast<const BYTE*>(val.c_str()), val.size())) != ERROR_SUCCESS) {
		info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("const tstring &val"), val));
		fn.add_arg(_T("const tstring &key_name"), key_name);
		fn.add_arg(_T("const DWORD type"), tostring(type));
		throw exception_type(error_msgs[6], fn, __REV_INFO__);
	}
	return ret;
}

inline long
RegistryKey::SetValue(const unsigned long val, const tstring& key_name) {
	long ret;
	if((ret= key.SetDWORDValue(key_name.c_str(), val)) != ERROR_SUCCESS) {
		info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&RegistryKey::Create), info::function::argument(_T("const unsigned long val"), tostring(val)));
		fn.add_arg(_T("const tstring &key_name"), key_name);
		throw exception_type(error_msgs[6], fn, __REV_INFO__);
	}
	return ret;
}
