/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "../../../core/non_copyable.hpp"


namespace jmmcg { namespace NTUtils {

	const TCHAR scm_lib_name[]=_T("advapi32.dll");

	class LoadLibraryWrapper : protected non_copyable {
	public:
		LoadLibraryWrapper(const TCHAR * const lib_name)
		: library_handle(::LoadLibrary(lib_name)) {
			DEBUG_ASSERT(library_handle);
		}
		virtual ~LoadLibraryWrapper(void) {
			::FreeLibrary(library_handle);
		}

		const HMODULE &Handle(void) const noexcept(true) {
			return library_handle;
		}

	private:
		const HMODULE library_handle;

		void operator=(const LoadLibraryWrapper &)=delete;
	};

} }
