/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "NetAPIDeallocator.hpp"
#include "NetShare.hpp"
#include "OnNT.hpp"

#include "../../../core/unicode_conversions.hpp"

/////////////////////////////////////////////////////////////////////////////

const TCHAR NetShareAdd_name[]= _T("NetShareAdd");
const TCHAR NetShareDel_name[]= _T("NetShareDel");

using namespace libNTUtils;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

NetShare::NetShare(void)
	: LoadLibraryWrapper(OnNT() ? netapi32_nt_lib_name : server_info_9X_lib_name),
	  pNetShareAdd(reinterpret_cast<NetShareAddType>(::GetProcAddress(Handle(), NetShareAdd_name))),
	  pNetShareDel(reinterpret_cast<NetShareDelType>(::GetProcAddress(Handle(), NetShareDel_name))) {
	DEBUG_ASSERT(pNetShareAdd);
	DEBUG_ASSERT(pNetShareDel);
	deleted= true;
}

inline NetShare::NetShare(const tstring& DirectoryToShare, const TCHAR* const Sharename, const TCHAR* const description, const TCHAR* const Server, const DWORD max_connections, SECURITY_DESCRIPTOR* sd, const DWORD share_perms)
	: LoadLibraryWrapper(OnNT() ? netapi32_nt_lib_name : server_info_9X_lib_name),
	  pNetShareAdd(reinterpret_cast<NetShareAddType>(::GetProcAddress(Handle(), NetShareAdd_name))),
	  pNetShareDel(reinterpret_cast<NetShareDelType>(::GetProcAddress(Handle(), NetShareDel_name))) {
	DEBUG_ASSERT(pNetShareAdd);
	DEBUG_ASSERT(pNetShareDel);
	deleted= true;
	Create(DirectoryToShare, Sharename, description, Server, max_connections, sd, share_perms);
}

NetShare::NetShare(const tstring& DirectoryToShare, const TCHAR* const Sharename, const TCHAR* const description, const TCHAR* const Username, const TCHAR* const Server, const DWORD access_mask, const DWORD max_connections, const DWORD share_perms)
	: LoadLibraryWrapper(OnNT() ? netapi32_nt_lib_name : server_info_9X_lib_name),
	  pNetShareAdd(reinterpret_cast<NetShareAddType>(::GetProcAddress(Handle(), NetShareAdd_name))),
	  pNetShareDel(reinterpret_cast<NetShareDelType>(::GetProcAddress(Handle(), NetShareDel_name))) {
	DEBUG_ASSERT(pNetShareAdd);
	DEBUG_ASSERT(pNetShareDel);
	deleted= true;
	Create(DirectoryToShare, Sharename, description, Username, Server, access_mask, max_connections, share_perms);
}

NetShare::~NetShare(void) {
	Delete();
}

inline bool
NetShare::Create(const tstring& DirectoryToShare, const TCHAR* const Sharename, const TCHAR* const description, const TCHAR* const Server, const DWORD max_connections, const SECURITY_DESCRIPTOR* const sd, const DWORD share_perms) {
	if(deleted) {
		DEBUG_ASSERT(Sharename);
		DEBUG_ASSERT(description);
		SHARE_INFO_502 si502;
		// setup share info structure
		// Note that we *must* use Unicode strings, as despite the declarations (which are
		// wrong), the function will error if Unicode strings aren't used. Hence the
		// silly casting.
		sharename_w= TStringToWString(Sharename);
		si502.shi502_netname= reinterpret_cast<LPWSTR>(const_cast<wchar_t*>(sharename_w.c_str()));
		si502.shi502_type= STYPE_DISKTREE;
		std::wstring rmb(TStringToWString(description));
		si502.shi502_remark= reinterpret_cast<LPWSTR>(const_cast<wchar_t*>(rmb.c_str()));
		si502.shi502_permissions= share_perms;
		si502.shi502_max_uses= max_connections;
		si502.shi502_current_uses= 0;
		std::wstring dirb(TStringToWString(DirectoryToShare));
		si502.shi502_path= reinterpret_cast<LPWSTR>(const_cast<wchar_t*>(dirb.c_str()));
		si502.shi502_passwd= NULL;
		si502.shi502_reserved= 0;
		si502.shi502_security_descriptor= const_cast<SECURITY_DESCRIPTOR*>(sd);
		machine_w= (Server ? TStringToWString(Server) : L"");
		NET_API_STATUS nas= (*pNetShareAdd)(
			machine_w.c_str(),	// share is on local machine
			502,	 // info-level
			reinterpret_cast<BYTE*>(&si502),	  // info-buffer
			NULL	 // don't bother with parm
		);
		if(nas == NERR_Success) {
			LIBJMMCG_LOG_TRIVIAL(trace) << "NetShare::Create(...): Successfully created the share '" << Sharename << _T("' (path '") << DirectoryToShare << _T("') on machine '") << (Server ? Server : _T("")) << _T("'");
			deleted= false;
			return false;
		}
		LIBJMMCG_LOG_TRIVIAL(trace) << "NetShare::Create(...): NetShareAdd error: 0x"<<std::hex<<nas<<". Did not create the share '")<<Sharename<<_T("' (path '")<<DirectoryToShare<<_T("') on machine '")<<(Server ? Server : _T(""))<<_T("'");
		deleted= true;
		return true;
	}
	return false;
}

bool
NetShare::Create(const tstring& DirectoryToShare, const TCHAR* const Sharename, const TCHAR* const description, const TCHAR* const Username, const TCHAR* const Server, const DWORD access_mask, const DWORD max_connections, const DWORD share_perms) {
	register OSVERSIONINFO osinfo;
	osinfo.dwOSVersionInfoSize= sizeof(OSVERSIONINFO);
	if(::GetVersionEx(&osinfo) && osinfo.dwPlatformId != VER_PLATFORM_WIN32_NT) {
		// Create the share with no restrictions if it's a non-NT box.
		return Create(DirectoryToShare, Sharename, description, Server, max_connections, NULL, share_perms);
	}
	if(!sd.Allow(NULL, Username, access_mask)) {
		return Create(DirectoryToShare, Sharename, description, Server, max_connections, &sd.SD(), share_perms);
	}
	return true;
}

bool
NetShare::Delete(void) {
	if(!deleted) {
		deleted= true;
		bool ret= ((*pNetShareDel)(machine_w.c_str(), sharename_w.c_str(), 0) == NERR_Success);
		LIBJMMCG_LOG_TRIVIAL(trace) << "NetShare::Create(...): Deleted the share '" << WStringToTString(sharename_w) << _T("') on machine '") << WStringToTString(machine_w) << _T("', yes/no: ") << ret;
		return ret;
	}
	return false;
}

bool
NetFindNTSubDir(NetShare& share, const TCHAR* const sharename, const TCHAR* const machine, const TCHAR* const path, const TCHAR* const username, const DWORD access_mask, const DWORD max_connections, const DWORD share_perms) {
	const TCHAR drive_share_name[]= _T("A1_dir_share$");
	const TCHAR description[]= _T("Temporary share on drive letter...");
	const TCHAR search_pattern[]= _T("\\*");
	NetShare drive_share;
	// It would be perverse to have the path on a floppy on the remote machine....
	tstring dir_ltr(_T("c"));
	do {
		if(!drive_share.Create(dir_ltr + _T(":\\"), drive_share_name, description, username, machine, GENERIC_READ, 1, ACCESS_READ)) {
			// Found a drive on the remote machine. Is it the right one?
			CFileFind finder;
			tstring pattern(machine);
			pattern+= dir_separator;
			pattern+= drive_share_name;
			int len= pattern.length();
			// Note that we assume they are not so perverse as to plonk NT in a path like "?:\*\*\...\*\system32" although it is possible...
			pattern+= search_pattern;
			pattern+= path;
			if(finder.FindFile(pattern.c_str()) && finder.FindNextFile()) {
				DEBUG_ASSERT(finder.IsDirectory());
				tstring remote_sys_path(dir_ltr);
				remote_sys_path+= _T(':');
				remote_sys_path+= static_cast<const TCHAR*>(finder.GetFilePath().Right(finder.GetFilePath().GetLength() - len));
				if(!share.Create(remote_sys_path, sharename, NULL, username, machine, access_mask, max_connections, share_perms)) {
					return false;
				}
			}
		}
		drive_share.Delete();
	} while(++*dir_ltr.begin() <= _T('z'));
	return true;
}
