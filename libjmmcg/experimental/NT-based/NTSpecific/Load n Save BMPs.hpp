/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// This bit of code was copied from an article called:
// "Bitmap & Palette - Drawing a bitmap from a BMP file" by
// Zafir Anjum (05/97). I found it at <a href="http://www.dsp.net/zafir/bitmap/draw_bmp.html"/>
//
// LoadBMP              - Loads a BMP file and creates a logical palette for it.
// Returns              - TRUE for success
// BMPFile              - Full path of the BMP file
// hDIB                 - Pointer to a HGLOBAL variable to hold the loaded bitmap
//                        Memory is allocated by this function but should be 
//                        released by the caller.
// Pal                  - Will hold the logical palette
//

#pragma once

#include "../../../core/ttypes.hpp"

#include <boost/shared_ptr.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	class BitMapInfoWrapper {
	public:
		explicit inline __stdcall BitMapInfoWrapper(const unsigned long size =0);
		inline __stdcall BitMapInfoWrapper(const unsigned int width,const unsigned int height,const unsigned short colour_depth);
		inline __stdcall BitMapInfoWrapper(const HBITMAP hbitmap,const unsigned short colour_depth);
		inline __stdcall BitMapInfoWrapper(BitMapInfoWrapper const &bmi) noexcept(true)
		: buff(bmi.buff) {
		}
		inline __stdcall ~BitMapInfoWrapper(void) noexcept(true) {
		}

		inline BitMapInfoWrapper & __fastcall operator=(BitMapInfoWrapper &bmi) noexcept(true) {
			buff=bmi.buff;
			return *this;
		}

		inline const BITMAPINFO * __fastcall Info(void) const noexcept(true) {
			return reinterpret_cast<BITMAPINFO *>(buff.get());
		}
		inline BITMAPINFO * __fastcall Info(void) noexcept(true) {
			return reinterpret_cast<BITMAPINFO *>(buff.get());
		}
		inline const BITMAPINFOHEADER * __fastcall Header(void) const noexcept(true) {
			return reinterpret_cast<BITMAPINFOHEADER *>(buff.get());
		}
		inline BITMAPINFOHEADER * __fastcall Header(void) noexcept(true) {
			return reinterpret_cast<BITMAPINFOHEADER *>(buff.get());
		}

		static inline DWORD __fastcall ColSize(const unsigned short colour_depth) noexcept(true);

	private:
		class DCWrapper {
		public:
			explicit inline __stdcall DCWrapper(void) noexcept(true)
			: dc_(CreateCompatibleDC(NULL)) {
			}
			inline __stdcall ~DCWrapper(void) noexcept(true) {
				DeleteDC(dc_);
			}

			inline HDC __fastcall dc(void) noexcept(true) {
				return dc_;
			}

		private:
			HDC dc_;

			inline __stdcall DCWrapper(const DCWrapper &) noexcept(true);
			inline DCWrapper & __fastcall operator=(const DCWrapper &) noexcept(true);
		};

		boost::shared_ptr<char> buff;

		inline void __fastcall InitHeader(void) noexcept(true);
		inline void __fastcall InitHeader(const unsigned int width,const unsigned int height,const unsigned short colour_depth) noexcept(true);

		static inline DWORD __fastcall CalcSize(const unsigned int width,const unsigned int height,const unsigned short colour_depth) noexcept(true);
	};

	AFX_EXT_CLASS bool __fastcall LoadBMP(const tstring &BMPFile,BitMapInfoWrapper &hDIB,std::auto_ptr<CPalette> &);
	AFX_EXT_CLASS bool __fastcall LoadBMP(CFile &,BitMapInfoWrapper &hDIB,std::auto_ptr<CPalette> &);
	AFX_EXT_CLASS BitMapInfoWrapper __fastcall CreateBitmapInfoStruct(const CBitmap &);
	AFX_EXT_CLASS bool __fastcall CreateBMPFile(const tstring &,const CBitmap &,const CDC &);
	AFX_EXT_CLASS bool __fastcall CreateBMPFile(CFile &,const CBitmap &,const CDC &);

	inline __stdcall
	BitMapInfoWrapper::BitMapInfoWrapper(const unsigned long size)
	: buff(new char[sizeof(BITMAPINFOHEADER)+size]) {
		InitHeader();
	}

	inline __stdcall
	BitMapInfoWrapper::BitMapInfoWrapper(const unsigned int width,const unsigned int height,const unsigned short colour_depth)
	: buff(new char[CalcSize(width,height,colour_depth)]) {
		InitHeader(width,height,colour_depth);
	}

	inline __stdcall
	BitMapInfoWrapper::BitMapInfoWrapper(const HBITMAP hbitmap,const unsigned short colour_depth) {
		BITMAP bitmap;
		GetObject(hbitmap,sizeof(BITMAP),&bitmap);
		buff=std::auto_ptr<char>(new char[CalcSize(bitmap.bmWidth,bitmap.bmHeight,colour_depth)]);
		InitHeader(bitmap.bmWidth,bitmap.bmHeight,colour_depth);
		// Get the bits from the bitmap and stuff them after the LPBI
		BYTE * const lpBits=reinterpret_cast<BYTE * const>(reinterpret_cast<BITMAPINFO *>(buff.get())->bmiColors)+ColSize(colour_depth);
		DCWrapper temp_dc;
		GetDIBits(temp_dc.dc(),hbitmap,0,bitmap.bmHeight,lpBits,reinterpret_cast<BITMAPINFO *>(buff.get()),DIB_RGB_COLORS);
		// Fix this if GetDIBits messed it up....
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biClrUsed=ColSize(colour_depth)/sizeof(RGBQUAD);
	}

	inline DWORD __fastcall
	BitMapInfoWrapper::ColSize(const unsigned short colour_depth) {
		return sizeof(RGBQUAD)*((colour_depth<= 8) ? 1<<colour_depth : 0);
	}

	inline void __fastcall
	BitMapInfoWrapper::InitHeader(void) {
		DEBUG_ASSERT(buff.get());
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biSize=sizeof(BITMAPINFOHEADER);
		/* If the bitmap is not compressed, set the BI_RGB flag. */
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biCompression=BI_RGB;
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biXPelsPerMeter=reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biYPelsPerMeter=0;
		/* 
		* Set biClrImportant to 0, indicating that all of the 
		* device colors are important.
		*/  
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biClrImportant=0;
	}

	inline void __fastcall
	BitMapInfoWrapper::InitHeader(const unsigned int width,const unsigned int height,const unsigned short colour_depth) {
		InitHeader();
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biWidth=width;
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biHeight=height;
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biPlanes=1;
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biBitCount=colour_depth;
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biSizeImage=CalcSize(width,height,colour_depth)-sizeof(BITMAPINFOHEADER)-ColSize(colour_depth);
		reinterpret_cast<BITMAPINFOHEADER *>(buff.get())->biClrUsed=ColSize(colour_depth)/sizeof(RGBQUAD);
	}

	inline DWORD __fastcall
	BitMapInfoWrapper::CalcSize(const unsigned int width,const unsigned int height,const unsigned short colour_depth) {
		//
		// DWORD align the width of the DIB
		// Figure out the size of the colour table
		// Calculate the size of the DIB
		//
		const UINT LineLen = (width*colour_depth+31)/32 * 4;
		return sizeof(BITMAPINFOHEADER)
			+ColSize(colour_depth)
			+static_cast<DWORD>(LineLen)*static_cast<DWORD>(height);
	}

} } }
