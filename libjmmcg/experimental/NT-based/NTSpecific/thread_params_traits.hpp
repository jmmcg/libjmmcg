/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <windows.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/// A constant, suitably defined to be the appropraite API for the plaform on which the code is compiled, to allow one to be platform-agnostic.
static inline constexpr const generic_traits::api_type::element_type platform_api= generic_traits::MS_Win32;

template<>
class thread_params<generic_traits::MS_Win32> : protected non_assignable {
public:
	static inline constexpr const generic_traits::api_type::element_type api_type= generic_traits::MS_Win32;

	typedef HANDLE handle_type;
	typedef unsigned int pid_type;
	typedef unsigned int tid_type;

	typedef DWORD processor_mask_type;

	typedef DWORD suspend_count;
	typedef DWORD suspend_period_ms;

	typedef void* security_type;
	typedef unsigned stack_size_type;
	typedef unsigned core_work_fn_ret_t;
	typedef void* core_work_fn_arg_t;
	typedef core_work_fn_ret_t(__stdcall core_work_fn_type)(core_work_fn_arg_t);
	typedef core_work_fn_arg_t arglist_type;
	typedef unsigned initflag_type;

	enum creation_flags {
		create_running= 0,
		create_suspended= CREATE_SUSPENDED
	};

	enum priority_type {
		lowest= THREAD_PRIORITY_LOWEST,
		idle= THREAD_PRIORITY_IDLE,
		below_normal= THREAD_PRIORITY_BELOW_NORMAL,
		normal= THREAD_PRIORITY_NORMAL,
		above_normal= THREAD_PRIORITY_ABOVE_NORMAL,
		highest= THREAD_PRIORITY_HIGHEST,
		time_critical= THREAD_PRIORITY_TIME_CRITICAL,
		unknown_priority
	};

	/**
		Note that these states are in a specific order - the higher the number, the more severe any error.
	*/
	enum states {
		active= STILL_ACTIVE,
		suspended,
		no_kernel_thread,	  ///< This is not a failure - the thread may not be started yet, or may have exited.
		get_exit_code_failure,
		// Error conditions now...
		null_this_pointer,
		jmmcg_exception,
		stl_exception,
		unknown_exception,
		terminated,
		unknown
	};
	enum thread_cancel_state {
	};

	const security_type security;
	const stack_size_type stack_size;
	core_work_fn_type* const work_fn;
	arglist_type arglist;

	handle_type handle;
	tid_type id;

	__stdcall thread_params(core_work_fn_type* const sa, const security_type se, const stack_size_type ss= 0) noexcept(true)
		: security(se), stack_size(ss), work_fn(sa), arglist(), handle(), id() {
		DEBUG_ASSERT(work_fn);
	}

	__stdcall thread_params(const thread_params& tp) noexcept(true)
		: security(tp.security), stack_size(tp.stack_size), work_fn(tp.work_fn), arglist(tp.arglist), handle(tp.handle), id(tp.id) {
	}

	tstring to_string(void) const {
		tostringstream ss;
		ss << _T("API type: 0x") << std::hex << api_type
			<< _T(", stack size: 0x") << std::dec << stack_size
			<< _T(", work function ptr: 0x") << std::hex << work_fn
			<< _T(", argument list: 0x") << std::hex << arglist
			<< _T(", handle: 0x") << std::hex << handle
			<< _T(", ID: 0x") << std::hex << id;
		return ss.str();
	}
};

}}}
