/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "NTSecuritySettings.hpp"
#include "exception.hpp"
#include "logging.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

/* An HRESULT is a 32-bit bit-field constructed thus:
	31				30-27		26-16			15-0
	Severity		Reserved	Facility		Actual
	code.						code			code, <=0x200
	0=success				Indicates
	1=failure				responsible
								module.
*/

typedef enum {
	all_ok= S_OK,
	standard_error= S_FALSE,
	unknown_error= MAKE_HRESULT(1, FACILITY_ITF, 0x201),
	unknown_exception= MAKE_HRESULT(1, FACILITY_ITF, 0x202),
	stl_exception= MAKE_HRESULT(1, FACILITY_ITF, 0x203),
	com_exception= MAKE_HRESULT(1, FACILITY_ITF, 0x204),
	jmmcg_exception= MAKE_HRESULT(1, FACILITY_ITF, 0x205)
} COM_error_codes;

class COMInit {
public:
	explicit inline __stdcall COMInit(const unsigned long CoInitType= COINIT_MULTITHREADED) {
#if _WIN32_WINNT >= 0x0400 & defined(_ATL_FREE_THREADED)
		const HRESULT hr= ::CoInitializeEx(NULL, CoInitType);
#else
		const HRESULT hr= ::CoInitialize(NULL);
#endif
		DEBUG_ASSERT(SUCCEEDED(hr));
	}

	inline __stdcall ~COMInit(void) noexcept(true) {
		try {
			::CoUninitialize();
		} catch(...) {
			LIBJMMCG_LOG_TRIVIAL(trace) << "Ignoring unknown exception caught. Details: " << boost::current_exception_diagnostic_information();
			DEBUG_ASSERT(!_T("COMInit::dtor: ::CoInitializeEx(..) failed."));
		}
	}

private:
	// I don't allow copying or assigment.
	inline __stdcall COMInit(const COMInit&) noexcept(true);
	inline COMInit& __fastcall operator=(const COMInit&) noexcept(true);
};

class COMSecuritySettings {
public:
	inline __stdcall COMSecuritySettings(const tstring& u, const tstring& domain_n_username, const tstring& p, const unsigned long AuthnLevel= RPC_C_AUTHN_LEVEL_CONNECT, const unsigned long ImpersonationLevel= RPC_C_IMP_LEVEL_IMPERSONATE)
		: unc(TStringToWString(u)), username(domain_n_username.empty() ? _T("") : domain_n_username.substr(domain_n_username.find_first_of(_T("\\/")) + 1, tstring::npos)), domain(domain_n_username.empty() ? _T("") : domain_n_username.substr(0, domain_n_username.find_first_of(_T("\\/")))), password(p) {
		Create(AuthnLevel, ImpersonationLevel);
	}

	inline __stdcall COMSecuritySettings(const tstring& u, const tstring& d, const tstring& us, const tstring& p, const unsigned long AuthnLevel= RPC_C_AUTHN_LEVEL_CONNECT, const unsigned long ImpersonationLevel= RPC_C_IMP_LEVEL_IMPERSONATE)
		: unc(TStringToWString(u)), username(us), domain(d), password(p) {
		Create(AuthnLevel, ImpersonationLevel);
	}

	inline __stdcall COMSecuritySettings(const COMSecuritySettings& ss)
		: unc(ss.unc), username(ss.username), domain(ss.domain), password(ss.password) {
		authid.User= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(username.c_str()));
		authid.UserLength= username.size();
		authid.Domain= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(domain.c_str()));
		authid.DomainLength= domain.size();
		authid.Password= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(password.c_str()));
		authid.PasswordLength= password.size();
		authid.Flags=
#ifdef UNICODE
			SEC_WINNT_AUTH_IDENTITY_UNICODE;
#else
			SEC_WINNT_AUTH_IDENTITY_ANSI;
#endif
		authinfo.dwAuthnSvc= ss.authinfo.dwAuthnSvc;
		authinfo.dwAuthzSvc= ss.authinfo.dwAuthzSvc;
		authinfo.pwszServerPrincName= ss.authinfo.pwszServerPrincName;
		authinfo.dwAuthnLevel= ss.authinfo.dwAuthnLevel;
		authinfo.dwImpersonationLevel= ss.authinfo.dwCapabilities;
		authinfo.pAuthIdentityData= &authid;
		authinfo.dwCapabilities= ss.authinfo.dwCapabilities;
		serverinfo.dwReserved1= serverinfo.dwReserved2= 0;
		serverinfo.pwszName= const_cast<wchar_t*>(unc.c_str());
		serverinfo.pAuthInfo= &authinfo;
	}

	inline __stdcall ~COMSecuritySettings(void) {
	}

	inline const COSERVERINFO& __fastcall ServerInfo(void) const noexcept(true) {
		return serverinfo;
	}

	inline COSERVERINFO& __fastcall ServerInfo(void) noexcept(true) {
		return serverinfo;
	}

	inline const tstring& __fastcall Domain(void) const noexcept(true) {
		return domain;
	}

	inline const tstring& __fastcall Username(void) const noexcept(true) {
		return username;
	}

private:
	const std::wstring unc;
	const tstring username;
	const tstring domain;
	const tstring password;
	COAUTHIDENTITY authid;
	COAUTHINFO authinfo;
	COSERVERINFO serverinfo;

	inline void __fastcall Create(const unsigned long AuthnLevel, const unsigned long ImpersonationLevel) {
		authid.User= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(username.c_str()));
		authid.UserLength= username.size();
		authid.Domain= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(domain.c_str()));
		authid.DomainLength= domain.size();
		authid.Password= reinterpret_cast<USHORT*>(const_cast<TCHAR*>(password.c_str()));
		authid.PasswordLength= password.size();
		authid.Flags=
#ifdef UNICODE
			SEC_WINNT_AUTH_IDENTITY_UNICODE;
#else
			SEC_WINNT_AUTH_IDENTITY_ANSI;
#endif
		authinfo.dwAuthnSvc= RPC_C_AUTHN_WINNT;
		authinfo.dwAuthzSvc= RPC_C_AUTHZ_NONE;
		authinfo.pwszServerPrincName= NULL;
		authinfo.dwAuthnLevel= AuthnLevel;
		authinfo.dwImpersonationLevel= ImpersonationLevel;
		authinfo.pAuthIdentityData= &authid;
		authinfo.dwCapabilities= EOAC_NONE;
		serverinfo.dwReserved1= serverinfo.dwReserved2= 0;
		serverinfo.pwszName= const_cast<wchar_t*>(unc.c_str());
		serverinfo.pAuthInfo= &authinfo;
	}

	// I don't allow assignment.
	inline COMSecuritySettings& __fastcall operator=(const COMSecuritySettings&) noexcept(true);
};

class InitCOMSecurity {
public:
	inline __stdcall InitCOMSecurity(void)
		: com(), sd() {
		// Set up authentication - any user can access this object. See pg 469,
		// "Professional ATL COM Programming".
		const HRESULT hr= ::CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_NONE, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);
	}

	inline __stdcall InitCOMSecurity(SecuritySettings* s)
		: com(), sd(s) {
		DEBUG_ASSERT(sd.get());
		// Set up authentication - any user can access this object. See pg 469,
		// "Professional ATL COM Programming".
		const HRESULT hr= ::CoInitializeSecurity(&sd->SD(), -1, NULL, NULL, RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_SECURE_REFS, NULL);
	}

	inline __stdcall ~InitCOMSecurity(void) {
	}

private:
	const COMInit com;
	const std::auto_ptr<SecuritySettings> sd;

	// I don't copying or allow assignment.
	inline __stdcall InitCOMSecurity(const InitCOMSecurity&) noexcept(true);
	inline InitCOMSecurity& __fastcall operator=(const InitCOMSecurity&) noexcept(true);
};

}}}
