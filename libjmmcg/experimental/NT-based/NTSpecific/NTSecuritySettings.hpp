/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#define VC_EXTRALEAN	  // Exclude rarely-used stuff from Windows headers
#include <winbase.h>
#include <winnt.h>

#pragma warning(disable:4290)	  // "C++ Exception Specification ignored."
#pragma warning(disable:4284)	  // "return type for 'std::auto_ptr<unsigned char>::operator ->' is 'unsigned char *' (ie; not a UDT or reference to a UDT.  Will produce errors if applied using infix notation)."

namespace jmmcg { namespace NTUtils {

class SecuritySettings {
public:
	virtual inline __stdcall ~SecuritySettings(void) noexcept(true) {
	}

	inline SecuritySettings& __fastcall operator=(const SecuritySettings&) noexcept(true) {
		::InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
		sa.nLength= sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor= &sd;
		sa.bInheritHandle= true;
		return *this;
	}

	inline const SECURITY_ATTRIBUTES& __fastcall SA(void) const noexcept(true) {
		return sa;
	}

	inline SECURITY_ATTRIBUTES& __fastcall SA(void) noexcept(true) {
		return sa;
	}

	inline const SECURITY_DESCRIPTOR& __fastcall SD(void) const noexcept(true) {
		return sd;
	}

	inline SECURITY_DESCRIPTOR& __fastcall SD(void) noexcept(true) {
		return sd;
	}

protected:
	SECURITY_DESCRIPTOR sd;

	inline __stdcall SecuritySettings(void) {
		::InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
		sa.nLength= sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor= &sd;
		sa.bInheritHandle= true;
	}

	inline __stdcall SecuritySettings(const SecuritySettings&) {
		::InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
		sa.nLength= sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor= &sd;
		sa.bInheritHandle= true;
	}

private:
	SECURITY_ATTRIBUTES sa;
};

// This class creates a security attributes object with an "everyone" & "full control"
// access list.
class EveryoneSecuritySettings : public SecuritySettings {
public:
	inline __stdcall EveryoneSecuritySettings(void)
		: SecuritySettings() {
		::SetSecurityDescriptorDacl(&sd, true, (ACL*)NULL, false);
	}

	inline __stdcall EveryoneSecuritySettings(const EveryoneSecuritySettings&)
		: SecuritySettings() {
		::SetSecurityDescriptorDacl(&sd, true, (ACL*)NULL, false);
	}

	inline __stdcall ~EveryoneSecuritySettings(void) noexcept(true) {
	}

	inline EveryoneSecuritySettings& __fastcall operator=(const EveryoneSecuritySettings& ess) noexcept(true) {
		static_cast<SecuritySettings&>(*this).operator=(ess);
		::SetSecurityDescriptorDacl(&sd, true, (ACL*)NULL, false);
		return *this;
	}
};

class SecurityID {
public:
	inline __stdcall SecurityID(const TCHAR* usr, const TCHAR* mc= NULL)
		: user(usr), machine(mc ? mc : _T("")) {
		Create();
	}

	inline __stdcall ~SecurityID(void) noexcept(true) {
	}

	inline __stdcall SecurityID(const SecurityID& secid)
		: user(secid.user), machine(secid.machine) {
		Create();
	}

	inline __fastcall operator SID*() noexcept(true) {
		return reinterpret_cast<SID*>(sid.get());
	}

	inline const TCHAR* __fastcall Domain(void) const noexcept(true) {
		return domain.get();
	}

private:
	tstring user, machine;
	std::auto_ptr<BYTE> sid;
	std::auto_ptr<TCHAR> domain;

	inline void __fastcall Create(void) {
		DWORD sid_size= 0;
		DWORD domain_size= 0;
		SID_NAME_USE sid_use;
		::LookupAccountName(machine.empty() ? NULL : machine.c_str(), user.c_str(), NULL, &sid_size, NULL, &domain_size, &sid_use);
		DEBUG_ASSERT(sid_size && domain_size);
		sid= std::auto_ptr<BYTE>(new BYTE[sid_size]);
		domain= std::auto_ptr<TCHAR>(new TCHAR[domain_size]);
		::LookupAccountName(machine.empty() ? NULL : machine.c_str(), user.c_str(), sid.get(), &sid_size, domain.get(), &domain_size, &sid_use);
		DEBUG_ASSERT(sid_use != SidTypeInvalid);
	}

	// I don't allow assignment.
	inline SecurityID& __fastcall operator=(const SecurityID&) noexcept(true);
};

/**
	The documentation states that the call to "CoQueryClientBlanket(...)" will only work on Win2K or higher. So on lower versions of NT you'll just get a NULL SID pointer.
*/
class CurrentSecurityID {
public:
	inline __stdcall CurrentSecurityID(void) {
#if(WINVER >= 0x0500)
		wchar_t* user_name= NULL;
		const HRESULT hr= ::CoQueryClientBlanket(NULL, NULL, NULL, NULL, NULL, reinterpret_cast<void**>(&user_name), NULL);
		LIBJMMCG_LOG_TRIVIAL(trace) << "CurrentSecurityID::CurrentSecurityID(): ::CoQueryClientBlanket(...) return code: " << win_exception::StrFromWinErr(hr);
		if(user_name) {
			current_sid= std::auto_ptr<SecurityID>(new SecurityID(NTUtils::WStringToTString(user_name).c_str()));
		}
#endif	// (WINVER >= 0x0500)
	}

	inline __stdcall ~CurrentSecurityID(void) noexcept(true) {
	}

	inline __fastcall operator SID*() noexcept(true) {
		return reinterpret_cast<SID*>(current_sid.get());
	}

private:
	std::auto_ptr<SecurityID> current_sid;

	// I don't allow copying or assignment.
	inline __stdcall CurrentSecurityID(const CurrentSecurityID&) noexcept(true);
	inline CurrentSecurityID& __fastcall operator=(const CurrentSecurityID&) noexcept(true);
};

}}

#pragma warning(default:4290)	  // "C++ Exception Specification ignored."
#pragma warning(default:4284)	  // "return type for 'std::auto_ptr<unsigned char>::operator ->' is 'unsigned char *' (ie; not a UDT or reference to a UDT.  Will produce errors if applied using infix notation)."
