// TITLE:
//
// AUTHOR:
// Created by J.M.McGuiness, E-mail: coder@hussar.me.uk
//
// DESCRIPTION:
// Note: You need to put the line "#import <msxml4.dll> implementation_only" in the "stdafx.cpp" or you'll get link errors with the MSXML stuff....
// (I need to do this as the more simple route of omitting the "no_implementation" in this file leads to those link errors anyway, if the "#import <msxml4.dll>" is put in "stdafx.h".
//
// LEGALITIES:
// Copyright © 2004 by J.M.McGuiness, all rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
/////////////////////////////////////////////////////////////////////////////

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils { namespace XML {

	inline MSXML2::IXMLDOMNodePtr __fastcall
	CreateNode(const node_types type,const tchar *node_name,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns) {
		static const tchar * const node_types_names[]={
			_T("attribute"),
			_T("document"),
			_T("element"),
			_T("text")
		};
		return doc->createNode(node_types_names[type],node_name,xml_ns);
	}

	inline MSXML2::IXMLDOMNodePtr __fastcall
	CreateNode(const node_types type,const tchar *node_name,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns,const MSXML2::IXMLDOMNodePtr &node) {
		return node->appendChild(CreateNode(type,node_name,doc,xml_ns));
	}

	inline MSXML2::IXMLDOMNodePtr __fastcall
	CreateComment(const tchar *node_name,const tchar *comment,const MSXML2::IXMLDOMDocumentPtr &doc,const tchar *xml_ns,const MSXML2::IXMLDOMNodePtr &node) {
		const MSXML2::IXMLDOMNodePtr comment_node(CreateNode(element,node_name,doc,xml_ns,node));
		const MSXML2::IXMLDOMTextPtr text(CreateNode(text,node_name,doc,xml_ns,comment_node));
		text->nodeValue=comment;
		return text;
	}

	inline tstring __fastcall
	GetParseError(const MSXML2::IXMLDOMDocumentPtr &doc) {
		return tstring(static_cast<const tchar *>(doc->parseError->reason));
	}

	/////////////////////////////////////////////////////////////////////////////

	inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const POINT &pt,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMElementPtr node(CreateNode(element,_T("point"),doc,xml_ns,n));
			node->setAttribute(_T("x"),pt.x);
			node->setAttribute(_T("y"),pt.y);
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const POINT *"),tostring(&pt));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,POINT &pt) {
		try {
			const MSXML2::IXMLDOMNamedNodeMapPtr attribs(n->selectSingleNode(_T("point"))->attributes);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("x"))->text),pt.x);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("y"))->text),pt.y);
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const POINT *"),tostring(&pt));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const RECT &rect,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMElementPtr node(CreateNode(element,_T("rectangle"),doc,xml_ns,n));
			node->setAttribute(_T("top"),rect.top);
			node->setAttribute(_T("left"),rect.left);
			node->setAttribute(_T("bottom"),rect.bottom);
			node->setAttribute(_T("right"),rect.right);
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const RECT *"),tostring(&rect));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,RECT &rect) {
		try {
			const MSXML2::IXMLDOMNamedNodeMapPtr attribs(n->selectSingleNode(_T("rectangle"))->attributes);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("top"))->text),rect.top);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("left"))->text),rect.left);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("bottom"))->text),rect.bottom);
			fromstring(static_cast<const tchar *>(attribs->getNamedItem(_T("right"))->text),rect.right);
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const RECT *"),tostring(&rect));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<typename Value> inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<Value> &colln,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMNodePtr colln_node(CreateNode(element,colln_name,doc,xml_ns,n));
			for (std::vector<Value>::const_iterator i(colln.begin());i!=colln.end();++i) {
				const MSXML2::IXMLDOMNodePtr node(CreateNode(element,_T("node"),doc,xml_ns,colln_node));
				i->ToXML(doc,node);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<Value> *"),tostring(&colln));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<double> &colln,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMNodePtr colln_node(CreateNode(element,colln_name,doc,xml_ns,n));
			for (std::vector<double>::const_iterator i(colln.begin());i!=colln.end();++i) {
				const MSXML2::IXMLDOMElementPtr node(CreateNode(element,_T("node"),doc,xml_ns,colln_node));
				node->setAttribute(_T("value"),*i);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<double> *"),tostring(&colln));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<unsigned long> &colln,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMNodePtr colln_node(CreateNode(element,colln_name,doc,xml_ns,n));
			for (std::vector<unsigned long>::const_iterator i(colln.begin());i!=colln.end();++i) {
				const MSXML2::IXMLDOMElementPtr node(CreateNode(element,_T("node"),doc,xml_ns,colln_node));
				node->setAttribute(_T("value"),static_cast<long>(*i));
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<unsigned long> *"),tostring(&colln));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	ToXML(const MSXML2::IXMLDOMDocumentPtr &doc,const MSXML2::IXMLDOMNodePtr &n,const tchar *colln_name,const std::vector<CPoint> &colln,const tchar *xml_ns) {
		try {
			const MSXML2::IXMLDOMNodePtr colln_node(CreateNode(element,colln_name,doc,xml_ns,n));
			for (std::vector<CPoint>::const_iterator i(colln.begin());i!=colln.end();++i) {
				const MSXML2::IXMLDOMNodePtr node(CreateNode(element,_T("node"),doc,xml_ns,colln_node));
				ToXML(doc,node,*i,xml_ns);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMDocumentPtr &"),tostring(doc)));
			info.add_arg(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<CPoint> *"),tostring(&colln));
			info.add_arg(_T("const tchar *"),xml_ns);
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<typename Value> inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<Value> &colln) {
		try {
			DEBUG_ASSERT(colln.empty());
			const MSXML2::IXMLDOMNodeListPtr nodes(n->selectNodes((colln_name+_T("/*")).c_str()));
			for (long i=0;i<nodes->length;++i) {
				colln.push_back(Value::FromXML(nodes->item[i]));
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<Value> *"),tostring(&colln));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<double> &colln) {
		try {
			DEBUG_ASSERT(colln.empty());
			const MSXML2::IXMLDOMNodeListPtr nodes(n->selectNodes((colln_name+_T("/node/@value")).c_str()));
			for (long i=0;i<nodes->length;++i) {
				double tmp;
				fromstring(static_cast<const tchar *>(nodes->item[i]->text),tmp);
				colln.push_back(tmp);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<double> *"),tostring(&colln));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<unsigned long> &colln) {
		try {
			DEBUG_ASSERT(colln.empty());
			const MSXML2::IXMLDOMNodeListPtr nodes(n->selectNodes((colln_name+_T("/node/@value")).c_str()));
			for (long i=0;i<nodes->length;++i) {
				unsigned long tmp;
				fromstring(static_cast<const tchar *>(nodes->item[i]->text),tmp);
				colln.push_back(tmp);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<unsigned long> *"),tostring(&colln));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	template<> inline void __fastcall
	FromXML(const MSXML2::IXMLDOMNodePtr &n,const tstring &colln_name,std::vector<CPoint> &colln) {
		try {
			DEBUG_ASSERT(colln.empty());
			const MSXML2::IXMLDOMNodeListPtr nodes(n->selectNodes((colln_name+_T("/*")).c_str()));
			for (long i=0;i<nodes->length;++i) {
				CPoint val;
				FromXML(nodes->item[i],val);
				colln.push_back(val);
			}
		} catch (const _com_error &err) {
			info::function info(__LINE__,__PRETTY_FUNCTION__,typeid(void),info::function::argument(_T("const MSXML2::IXMLDOMNodePtr &"),tostring(n)));
			info.add_arg(_T("const tchar *"),colln_name);
			info.add_arg(_T("const std::vector<CPoint> *"),tostring(&colln));
			throw exception(err.ErrorMessage(),info,JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER)));
		}
	}

	/////////////////////////////////////////////////////////////////////////////

	inline __stdcall
	DocWrapper::DocWrapper(void)
	: doc(MakeDoc()) {
		doc->appendChild(doc->createProcessingInstruction(_T("xml"),_T("version='1.0'")));
		doc->async=false;
	}

	inline __stdcall
	DocWrapper::DocWrapper(const DocWrapper &d)
	: doc(d.Doc()) {
		doc->appendChild(doc->createProcessingInstruction(_T("xml"),_T("version='1.0'")));
		doc->async=false;
	}

	inline __stdcall
	DocWrapper::~DocWrapper(void) {
	}

	inline const MSXML2::IXMLDOMDocumentPtr & __fastcall
	DocWrapper::Doc(void) const noexcept(true) {
		return doc;
	}

	inline void __fastcall
	DocWrapper::LoadXML(const tchar *xml) {
		if (doc->loadXML(xml)==VARIANT_FALSE) {
			throw exception(
				GetParseError(doc),
				info::function(__LINE__,__PRETTY_FUNCTION__,typeid(&DocWrapper::LoadXML),info::function::argument(_T("const tchar *"),xml)),
				JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))
			);
		}
	}

	inline void __fastcall
	DocWrapper::Load(const tchar *filename) {
		if (doc->load(filename)==VARIANT_FALSE) {
			throw exception(
				GetParseError(doc),
				info::function(__LINE__,__PRETTY_FUNCTION__,typeid(&DocWrapper::Load),info::function::argument(_T("const tchar *"),filename)),
				JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))
			);
		}
	}

	inline void __fastcall
	DocWrapper::Load(const IStreamPtr &file_ptr) {
		IUnknown *itf;
		const HRESULT hr=const_cast<IStreamPtr &>(file_ptr).QueryInterface(IID_IUnknown,itf);
		if (FAILED(hr) || doc->load(itf)==VARIANT_FALSE) {
			throw exception(
				GetParseError(doc),
				info::function(__LINE__,__PRETTY_FUNCTION__,typeid(&DocWrapper::Load),info::function::argument(_T("const IStreamPtr &"),tostring(file_ptr))),
				JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))
			);
		}
	}

	inline void __fastcall
	DocWrapper::Save(const tchar *filename) {
// TODO JMG			doc->insertBefore(doc->createProcessingInstruction(_T("xml"),_T("version='1.0'")),static_cast<_variant_t>(doc->firstChild));
		if (doc->save(filename)!=S_OK) {
			throw exception(
				GetParseError(doc),
				info::function(__LINE__,__PRETTY_FUNCTION__,typeid(&DocWrapper::Save),info::function::argument(_T("const tchar *"),filename)),
				JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))
			);
		}
	}

	inline MSXML2::IXMLDOMDocumentPtr __fastcall
	DocWrapper::MakeDoc(void) {
		return MSXML2::IXMLDOMDocumentPtr(__uuidof(MSXML2::DOMDocument));
	}

} } } }
