/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#ifndef _MT
#	error You need to configure the project build settings to be multithreaded!
#endif

#include <process.h>

#pragma warning(push)
#pragma warning(disable:4800)	// forcing value to bool 'true' or 'false' (performance warning)

namespace jmmcg { namespace ppd {

// Implementation details..... Don't look below here!!! ;)

//	"api_threading_traits" implementation...

	template<> inline api_threading_traits<generic_traits::MS_Win32,sequential_mode>::api_params_type::pid_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,sequential_mode>::get_current_process_id(void) noexcept(true) {
		return static_cast<api_params_type::pid_type>(::GetCurrentProcessId());
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,sequential_mode>::api_params_type::tid_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,sequential_mode>::get_current_thread_id(void) noexcept(true) {
		return static_cast<api_params_type::tid_type>(::GetCurrentThreadId());
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,sequential_mode>::api_params_type::handle_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,sequential_mode>::get_current_thread(void) noexcept(true) {
		return static_cast<api_params_type::handle_type>(::GetCurrentThread());
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32,sequential_mode>::demangle_name(char * const mangled_name, demangled_name_t &demangled_name) noexcept(true) {
		::strcpy(demangled_name, mangled_name);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_backtrace_on_signal() noexcept(true) {
// TODO
	}

	inline void dump_bt_to_cerr() {
		auto const &bt=boost::stacktrace::stacktrace();
		std::cerr<<bt<<std::endl;
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_backtrace_on_terminate() noexcept(true) {
		std::set_terminate(&dump_bt_to_cerr);
	}

	/// Run the work only once if it returns at all, unlike the multi-threaded variant.
	/**
		If the worker_fn() returns true then it will run once, then exit, otherwise it will depend upon the return value for pre_exit(). If this always returns false, then the system will enter an infinite loop. By default pre_exit() for sequential mode returns false.

		\see worker_fn(), pre_exit(), process()
	*/
	template<> inline bool __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::create(const api_params_type::creation_flags creflag, api_params_type &parms) noexcept(true) {
		parms.handle=get_current_thread();
		parms.id=get_current_thread_id();
		DEBUG_ASSERT(parms.work_fn);
		if (creflag==api_params_type::create_running) {
			(*parms.work_fn)(parms.arglist);
		}
		return true;
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::suspend_count __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::suspend(api_params_type const &) noexcept(true) {
		return static_cast<api_params_type::suspend_count>(0);
	}

	/// This does not run the work at all, unlike the multi-threaded variant.
	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::suspend_count __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::resume(api_params_type const &parms) noexcept(true) {
		(*parms.work_fn)(parms.arglist);
		return static_cast<api_params_type::suspend_count>(0);
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::states __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::state(api_params_type &) noexcept(true) {
		return static_cast<api_params_type::states>(api_params_type::no_kernel_thread);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::exit(const api_params_type::states) noexcept(true) {
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::terminate(api_params_type::handle_type) {
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::cleanup(api_params_type::handle_type) {
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_kernel_priority(api_params_type::handle_type, const api_params_type::priority_type) {
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::priority_type __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::get_kernel_priority(api_params_type::handle_type) {
		return api_params_type::normal;
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_kernel_affinity(api_params_type::handle_type const, api_params_type::processor_mask_type const) {
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::processor_mask_type __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::get_kernel_affinity(api_params_type::handle_type const) {
		return api_params_type::processor_mask_type();
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_cancelstate(api_params_type::thread_cancel_state) {
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::sleep(const api_params_type::suspend_period_ms) {
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::states __fastcall
	api_threading_traits<generic_traits::MS_Win32, sequential_mode>::wait_thread_exit(api_params_type &, const lock_traits::timeout_type) noexcept(true) {
		return api_params_type::no_kernel_thread;
	}

	template<> inline bool __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::create(const api_params_type::creation_flags creflag, api_params_type &cp) {
		DEBUG_ASSERT(cp.work_fn);
		DEBUG_ASSERT(cp.arglist);
		// Note that to use this, you must implement a lock around the assignment, so that you can guarantee that it is done before calling other functions that may rely on the value of "cp.handle", which is plain nasty, because most Win32 API fns require the handle, not the id, and you can't get the handle atomically...
		cp.handle=reinterpret_cast<api_params_type::handle_type>(::_beginthreadex(cp.security, cp.stack_size, cp.work_fn, cp.arglist, static_cast<api_params_type::initflag_type>(creflag), &cp.id));
		DEBUG_ASSERT(cp.id);
		return static_cast<bool>(cp.id);
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::suspend_count __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::suspend(api_params_type const &thread) {
		DEBUG_ASSERT(thread.handle);
		const api_params_type::suspend_count count=static_cast<api_params_type::suspend_count>(::SuspendThread(thread.handle));
		DEBUG_ASSERT(count!=-1 && count<MAXIMUM_SUSPEND_COUNT);
		return count;
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::suspend_count __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::resume(const api_params_type &parms) {
		DEBUG_ASSERT(parms.id);
		const api_params_type::suspend_count count=static_cast<api_params_type::suspend_count>(::ResumeThread(parms.handle));
		DEBUG_ASSERT(count!=-1 && count<MAXIMUM_SUSPEND_COUNT);
		return count;
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::states __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::state(api_params_type &thread) {
		DWORD exit_code=static_cast<DWORD>(api_params_type::unknown);
		DEBUG_ASSERT(thread.id);
		if (!::GetExitCodeThread(thread.handle, &exit_code)) {
			return api_params_type::get_exit_code_failure;
		}
		return static_cast<api_params_type::states>(exit_code);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::exit(const api_params_type::states exit_code) {
		::_endthreadex(exit_code);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::terminate(api_params_type const &thread) {
		DEBUG_ASSERT(thread.handle);
		::TerminateThread(thread.handle ,api_params_type::terminated);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::cleanup(api_params_type const &thread) {
		DEBUG_ASSERT(thread.handle);
		::CloseHandle(thread.handle);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::set_kernel_priority(api_params_type::handle_type thread,const api_params_type::priority_type priority) {
		DEBUG_ASSERT(thread);
		::SetThreadPriority(thread,priority);
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::priority_type
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::get_kernel_priority(api_params_type::handle_type thread) {
		DEBUG_ASSERT(thread);
		return static_cast<api_params_type::priority_type>(::GetThreadPriority(thread));
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::set_kernel_affinity(api_params_type::handle_type const thread_handle, api_params_type::processor_mask_type const mask) {
		DEBUG_ASSERT(thread_handle);
		::SetThreadAffinityMask(thread_handle, mask);
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::processor_mask_type __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::get_kernel_affinity(api_params_type::handle_type const thread_handle) {
		DEBUG_ASSERT(thread_handle);
		api_params_type::processor_mask_type mask;
		::GetThreadAffinityMask(thread_handle, mask);
		return mask;
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::pid_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::get_current_process_id(void) {
		return static_cast<api_params_type::pid_type>(::GetCurrentProcessId());
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::tid_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::get_current_thread_id(void) {
		return static_cast<api_params_type::tid_type>(::GetCurrentThreadId());
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::api_params_type::handle_type __fastcall
	api_threading_traits<generic_traits::MS_Win32,heavyweight_threading>::get_current_thread() {
		return static_cast<api_params_type::handle_type>(::GetCurrentThread());
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::set_cancelstate(api_params_type::thread_cancel_state) {
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::sleep(const api_params_type::suspend_period_ms period) {
		::Sleep(period);
	}

	template<> inline api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::states __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::wait_thread_exit(api_params_type &params, const lock_traits::timeout_type period) {
		return static_cast<api_params_type::states>(::WaitForSingleObject(params.handle, period));
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::demangle_name(char * const mangled_name, demangled_name_t &demangled_name) noexcept(true) {
		return api_threading_traits<generic_traits::MS_Win32, sequential_mode>::demangle_name(mangled_name, demangled_name);
	}

	template<> inline void __fastcall
	api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::set_backtrace_on_terminate() noexcept(true) {
		api_threading_traits<generic_traits::MS_Win32, sequential_mode>::set_backtrace_on_terminate();
	}

} }

#pragma warning(pop)
