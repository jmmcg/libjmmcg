/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "SecurityDescriptor.hpp"

#include "../../../core/exception.hpp"

/////////////////////////////////////////////////////////////////////////////

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

#ifdef UNICODE
const TCHAR LookupAccountName_name[]= _T("LookupAccountNameW");
#else
const TCHAR LookupAccountName_name[]= _T("LookupAccountNameA");
#endif	// !UNICODE
const TCHAR InitializeAcl_name[]= _T("InitializeAcl");
const TCHAR AddAccessAllowedAce_name[]= _T("AddAccessAllowedAce");
const TCHAR InitializeSecurityDescriptor_name[]= _T("InitializeSecurityDescriptor");
const TCHAR SetSecurityDescriptorDacl_name[]= _T("SetSecurityDescriptorDacl");
const TCHAR GetLengthSid_name[]= _T("GetLengthSid");
#ifdef _DEBUG
const TCHAR IsValidSid_name[]= _T("IsValidSid");
const TCHAR IsValidAcl_name[]= _T("IsValidAcl");
const TCHAR IsValidSecurityDescriptor_name[]= _T("IsValidSecurityDescriptor");
#endif

/////////////////////////////////////////////////////////////////////////////

inline ACL_wrapper::ACL_wrapper(void)
	: LoadLibraryWrapper(scm_lib_name),
	  pInitializeAcl(reinterpret_cast<InitializeAclType>(::GetProcAddress(Handle(), InitializeAcl_name))),
	  pAddAccessAllowedAce(reinterpret_cast<AddAccessAllowedAceType>(::GetProcAddress(Handle(), AddAccessAllowedAce_name))),
#ifdef _DEBUG
	  pIsValidAcl(reinterpret_cast<IsValidAclType>(::GetProcAddress(Handle(), IsValidAcl_name))),
#endif
	  size_(),
	  buff() {
}

inline ACL_wrapper::ACL_wrapper(const unsigned long acl_size)
	: LoadLibraryWrapper(scm_lib_name),
	  pInitializeAcl(reinterpret_cast<InitializeAclType>(::GetProcAddress(Handle(), InitializeAcl_name))),
	  pAddAccessAllowedAce(reinterpret_cast<AddAccessAllowedAceType>(::GetProcAddress(Handle(), AddAccessAllowedAce_name))),
#ifdef _DEBUG
	  pIsValidAcl(reinterpret_cast<IsValidAclType>(::GetProcAddress(Handle(), IsValidAcl_name))),
#endif
	  size_(acl_size),
	  buff(new BYTE[acl_size]) {
}

inline ACL_wrapper::ACL_wrapper(ACL_wrapper& sw)
	: LoadLibraryWrapper(scm_lib_name),
	  pInitializeAcl(reinterpret_cast<InitializeAclType>(::GetProcAddress(Handle(), InitializeAcl_name))),
	  pAddAccessAllowedAce(reinterpret_cast<AddAccessAllowedAceType>(::GetProcAddress(Handle(), AddAccessAllowedAce_name))),
#ifdef _DEBUG
	  pIsValidAcl(reinterpret_cast<IsValidAclType>(::GetProcAddress(Handle(), IsValidAcl_name))),
#endif
	  size_(sw.size_),
	  buff(sw.buff) {
}

inline ACL_wrapper::~ACL_wrapper(void) {
}

inline ACL_wrapper&
ACL_wrapper::operator=(ACL_wrapper& aw) {
	size_= aw.size_;
	buff= aw.buff;
	return *this;
}

inline void
ACL_wrapper::copy(const ACL_wrapper& aw) {
	::memcpy(buff.get(), aw.buff.get(), (size_ < aw.size_) ? size_ : aw.size_);
}

inline bool
ACL_wrapper::initialize(void) {
	const bool ret= !(*pInitializeAcl)(reinterpret_cast<ACL*>(buff.get()), size_, ACL_REVISION);
	DEBUG_ASSERT((*pIsValidAcl)(reinterpret_cast<ACL*>(buff.get())));
	return ret;
}

inline bool
ACL_wrapper::add_ACE(const DWORD access_mask, SID* sid) {
	const bool ret= (*pAddAccessAllowedAce)(reinterpret_cast<ACL*>(buff.get()), ACL_REVISION, access_mask, sid);
	DEBUG_ASSERT((*pIsValidAcl)(reinterpret_cast<ACL*>(buff.get())));
	return ret;
}

inline unsigned long
ACL_wrapper::size(void) const {
	return size_;
}

inline const ACL*
ACL_wrapper::get(void) const {
	return reinterpret_cast<const ACL*>(buff.get());
}

inline ACL*
ACL_wrapper::get(void) {
	return reinterpret_cast<ACL*>(buff.get());
}

SecurityDescriptor::SecurityDescriptor(void)
	: LoadLibraryWrapper(scm_lib_name),
	  pLookupAccountName(reinterpret_cast<LookupAccountNameType>(::GetProcAddress(Handle(), LookupAccountName_name))),
	  pInitializeSecurityDescriptor(reinterpret_cast<InitializeSecurityDescriptorType>(::GetProcAddress(Handle(), InitializeSecurityDescriptor_name))),
	  pSetSecurityDescriptorDacl(reinterpret_cast<SetSecurityDescriptorDaclType>(::GetProcAddress(Handle(), SetSecurityDescriptorDacl_name))),
	  pGetLengthSid(reinterpret_cast<GetLengthSidType>(::GetProcAddress(Handle(), GetLengthSid_name)))
#ifdef _DEBUG
	  ,
	  pIsValidSid(reinterpret_cast<IsValidSidType>(::GetProcAddress(Handle(), IsValidSid_name))),
	  pIsValidSecurityDescriptor(reinterpret_cast<IsValidSecurityDescriptorType>(::GetProcAddress(Handle(), IsValidSecurityDescriptor_name)))
#endif
{
	if(!(*pInitializeSecurityDescriptor)(&sd, SECURITY_DESCRIPTOR_REVISION)) {
		throw exception_type(_T("Failed to initialise security descriptor."), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T(""), _T(""))), __REV_INFO__);
	}
}

inline SecurityDescriptor::~SecurityDescriptor(void) {
	for(std::vector<SID*>::iterator iter(sids.begin()); iter != sids.end(); ++iter) {
		try {
			delete[] reinterpret_cast<BYTE*>(*iter);
		} catch(const std::exception& err) {
			LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::~SecurityDescriptor(): Ignoring exception ' << err.what() << "' thrown when trying to delete a SID, attempting to just leak the memory."; DEBUG_ASSERT(NULL);
		}
	}
}

unsigned long
SecurityDescriptor::Allow(const TCHAR* const machine, const TCHAR* const username, const DWORD access_mask) {
	DEBUG_ASSERT(username);
	const unsigned int SID_SIZE= 96;
	DWORD cbSid= SID_SIZE;
	SID* sid= reinterpret_cast<SID*>(new BYTE[cbSid]);
	if(!sid) {
		return S_FALSE;
	}
	TCHAR RefDomain[DNLEN + 1];
	DWORD cchDomain= DNLEN + 1;
	SID_NAME_USE peUse;
	// get the Sid associated with the supplied user/group name
	if(!(*pLookupAccountName)(
			machine,	  // default lookup logic
			username,	// user/group of interest from commandline
			sid,	 // Sid buffer
			&cbSid,	 // size of Sid
			RefDomain,	 // Domain account found on (unused)
			&cchDomain,	  // size of domain in chars
			&peUse)) {
		// if the buffer wasn't large enough, try again
		if(GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
			delete[] reinterpret_cast<BYTE*>(sid);
			sid= reinterpret_cast<SID*>(new BYTE[cbSid]);
			if(sid) {
				cchDomain= DNLEN + 1;
				if(!(*pLookupAccountName)(
						machine,	  // default lookup logic
						username,	// user/group of interest from commandline
						sid,	 // Sid buffer
						&cbSid,	 // size of Sid
						RefDomain,	 // Domain account found on (unused)
						&cchDomain,	  // size of domain in chars
						&peUse)) {
					const unsigned long err= GetLastError();
					LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::Allow(...): LookupAccountName(...) error! " << NTUtils::win_exception::StrFromWinErr(err);
					delete[] reinterpret_cast<BYTE*>(sid);
					return err;
				}
			} else {
				const unsigned long err= GetLastError();
				LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::Allow(...): SID new error!";
				delete[] reinterpret_cast<BYTE*>(sid);
				return err ? err : S_FALSE;
			}
		} else {
			const unsigned long err= GetLastError();
			LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::Allow(...): LookupAccountName(...) error! " << NTUtils::win_exception::StrFromWinErr(err);
			delete[] reinterpret_cast<BYTE*>(sid);
			return err;
		}
	}
	DEBUG_ASSERT((*pIsValidSid)(sid));
	// compute size of new acl
	unsigned long orig_dwAclSize= sizeof(ACL);
	for(std::vector<SID*>::const_iterator iter(sids.begin()); iter != sids.end(); ++iter) {
		orig_dwAclSize+= sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD) + ((*pGetLengthSid)(*iter) << 1);
	}
	// allocate storage for Acl
	const ACL_wrapper orig_acl(acl);
	ACL_wrapper acl_wrap(orig_dwAclSize + sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD) + ((*pGetLengthSid)(sid) << 1));
	acl= acl_wrap;
	if(sids.empty()) {
		if(acl.initialize()) {
			const unsigned long err= GetLastError();
			LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::Allow(...): InitializeAcl(...) error! " << NTUtils::win_exception::StrFromWinErr(err);
			delete[] reinterpret_cast<BYTE*>(sid);
			return err;
		}
	} else {
		acl.copy(orig_acl);
	}
	if(acl.get()) {
		if(acl.add_ACE(access_mask, sid)) {
			if((*pSetSecurityDescriptorDacl)(&sd, true, acl.get(), false)) {
				DEBUG_ASSERT((*pIsValidSecurityDescriptor)(&sd));
				LIBJMMCG_LOG_TRIVIAL(trace) << "SecurityDescriptor::Allow(...): Successfully added access allowed ACL to security descriptor for user '" << username << "' on machine '" << (machine ? machine : "") << "' with access mask: 0x" << std::hex << access_mask;
				sids.push_back(sid);
				return S_OK;
			}
		}
	}
	delete[] reinterpret_cast<BYTE*>(sid);
	const unsigned long err= GetLastError();
	return err ? err : S_FALSE;
}
