/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace ppd { namespace pool { namespace thread_types {

template<class WQ>
inline steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	steal(exit_requested_type& exit_requested, signalled_work_queue_type& work_q) noexcept(true)
	: base_t(exit_requested), signalled_work_queue(work_q) {
}

template<class WQ>
inline steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	~steal() {
	base_t::wait_thread_exit();
}

template<class WQ>
inline
	typename steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::thread_traits::api_params_type::states
	steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
		process() noexcept(false) {
	// Serialize access to the queue by all of the other threads in the pool, this means that only one job can be removed at a time from the work queue.
	for(;;) {
		const typename exit_requested_type::lock_result_type lkd= this->exit_requested.lock();
		DEBUG_ASSERT(lkd.second == lock_traits::atom_set);
		if(lkd.first == exit_requested_type::states::exit_requested) {
			// Ensure the rest of the threads in the pool exit.
			this->exit_requested.set(exit_requested_type::states::exit_requested);
			break;
		} else if(lkd.first == exit_requested_type::states::new_work_arrived) {
			batch.process_a_batch(this->exception_thrown_in_thread, signalled_work_queue);
		}
	}
	return thread_traits::api_params_type::no_kernel_thread;
}

template<class WQ>
inline slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type& wk) noexcept(true)
	: base_t(exit_requested), some_work(wk) {
}

template<class WQ>
inline slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	~slave() {
	base_t::wait_thread_exit();
}

template<class WQ>
inline
	typename slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::thread_traits::api_params_type::states
	slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
		process() noexcept(false) {
	some_work->process();
	return thread_traits::api_params_type::no_kernel_thread;
}

template<class WQ>
inline steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	steal(exit_requested_type& exit_r, signalled_work_queue_type& work_q) noexcept(true)
	: base_t(exit_r), signalled_work_queue(work_q) {
}

template<class WQ>
inline steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	~steal() {
	base_t::wait_thread_exit();
}

template<class WQ>
inline
	typename steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::thread_traits::api_params_type::states
	steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
		process() noexcept(false) {
	// Serialize access to the queue by all of the other threads in the pool, this means that only one job can be removed at a time from the work queue.
	for(;;) {
		const typename exit_requested_type::lock_result_type lkd= this->exit_requested.lock();
		DEBUG_ASSERT(lkd.second == lock_traits::atom_set);
		if(lkd.first == exit_requested_type::states::exit_requested) {
			// Ensure the rest of the threads in the pool exit.
			this->exit_requested.set(exit_requested_type::states::exit_requested);
			break;
		} else if(lkd.first == exit_requested_type::states::new_work_arrived) {
			batch.process_a_batch(this->exception_thrown_in_thread, signalled_work_queue);
		}
	}
	return thread_traits::api_params_type::no_kernel_thread;
}

template<class WQ>
inline slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type& wk) noexcept(true)
	: base_t(exit_requested), some_work(wk) {
}

template<class WQ>
inline slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
	~slave() {
	base_t::wait_thread_exit();
}

template<class WQ>
inline
	typename slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::thread_traits::api_params_type::states
	slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>::
		process() noexcept(false) {
	if(some_work->result_traits() == generic_traits::return_data::element_type::joinable) {
		some_work->process(this->exception_thrown_in_thread);
	} else {
		some_work->process();
	}
	return thread_traits::api_params_type::no_kernel_thread;
}

}}}}
