/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "DumpWinMsg.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

/// Handy for use in source files.
#define JMMCG_MAKE_WEXCPT_SRC1(_JMMCG_EXCPT_MSG) jmmcg::LIBJMMCG_VER_NAMESPACE::win_exception((_JMMCG_EXCPT_MSG),JMMCG_FUNCTION(void),__REV_INFO__)
/// Handy for use in header files.
#define JMMCG_MAKE_WEXCPT_HDR1(_JMMCG_EXCPT_MSG,_JMMCG_REVISION_HDR) jmmcg::LIBJMMCG_VER_NAMESPACE::win_exception((_JMMCG_EXCPT_MSG),JMMCG_FUNCTION(void),JMMCG_REVISION(_JMMCG_REVISION_HDR))

	class win_exception : public crt_exception<ppd::generic_traits::MS_Win32, ppd::heavyweight_threading> {
	public:
		typedef crt_exception<ppd::generic_traits::MS_Win32, ppd::heavyweight_threading> base_t;

		win_exception(const tstring &r, const info::function &f, const info::revision &ri)
		: base_t(r,f,ri), win_err(::GetLastError()) {
		}
		win_exception(const tostringstream &r, const info::function &f, const info::revision &ri)
		: base_t(r,f,ri), win_err(::GetLastError()) {
		}
		win_exception(const tstring &r, const unsigned long we, const info::function &f, const info::revision &ri)
		: base_t(r,f,ri), win_err(we) {
		}
		win_exception(const tostringstream &r, const unsigned long we, const info::function &f, const info::revision &ri)
		: base_t(r,f,ri), win_err(we) {
		}
		win_exception(const win_exception &ex)
		: base_t(ex), win_err(ex.win_err) {
		}
		virtual
		~win_exception() noexcept(true) {
		}

		static inline tstring __fastcall
		StrFromWinErr(const unsigned long win_err) {
			tostringstream ss;
			ss<<_T("Windows error: code=0x")<<std::hex<<win_err<<_T(", string: '")<<NTUtils::DumpWinMessage(win_err)<<_T("'.");
			return ss.str();
		};

		/**
			\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
		*/
		friend inline tostream & __fastcall
		operator<<(tostream &o, const win_exception &e) {
			return o<<e.to_string();
		}

	protected:
		virtual inline const tstring __fastcall
		to_string() const {
			tostringstream ss;
			ss<<base_t::to_string()
				<<StrFromWinErr(win_err)<<std::endl;
			return ss.str();
		}

	private:
		const unsigned long win_err;

		void operator=(win_exception const &)=delete;
	};

} } }
