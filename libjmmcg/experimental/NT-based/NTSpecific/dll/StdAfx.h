/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__F585FBDB_34EB_4369_BEB4_2190904D9C11__INCLUDED_)
#	define AFX_STDAFX_H__F585FBDB_34EB_4369_BEB4_2190904D9C11__INCLUDED_

#	if _MSC_VER > 1000
#		pragma once
#	endif	// _MSC_VER > 1000

#	define WINVER 0x0501
#	define _WIN32_WINNT 0x0501

#	define _CRT_SECURE_NO_WARNINGS

#	define VC_EXTRALEAN	  // Exclude rarely-used stuff from Windows headers
#	define WIN32_LEAN_AND_MEAN	// Exclude rarely-used stuff from Windows headers

#	define BOOST_USE_WINDOWS_H

#	pragma warning(disable:4800)	  ///< 'BOOL' : forcing value to bool 'true' or 'false' (performance warning).
#	include <afxwin.h>	 // MFC core and standard components
// #include <afxext.h>         // MFC extensions

#	ifndef _AFX_NO_OLE_SUPPORT
// #include <afxole.h>         // MFC OLE classes
// #include <afxodlgs.h>       // MFC OLE dialog classes
// #include <afxdisp.h>        // MFC Automation classes
#	endif	// _AFX_NO_OLE_SUPPORT

#	ifndef _AFX_NO_DB_SUPPORT
// #include <afxdb.h>			// MFC ODBC database classes
#	endif	// _AFX_NO_DB_SUPPORT

#	ifndef _AFX_NO_DAO_SUPPORT
// #include <afxdao.h>			// MFC DAO database classes
#	endif	// _AFX_NO_DAO_SUPPORT

// #include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#	ifndef _AFX_NO_AFXCMN_SUPPORT
// #include <afxcmn.h>			// MFC support for Windows Common Controls
#	endif	// _AFX_NO_AFXCMN_SUPPORT

#	include <windows.h>
// Networking functionality such as browsing.
#	include <lm.h>
// AT job scheduling structure.
#	include <lmat.h>
// For the Service Manager functions. (NT only.)
#	include <winsvc.h>

#	include <Loadperf.h>	// For "LoadPerfCounterTextStrings(...)" & "UnloadPerfCounterTextStrings(...)"

#	include <winperf.h>

#	pragma warning(push)
#	pragma warning(disable:4244)	  ///< Conversion from 'int' to 'unsigned short', possible loss of data.
#	include "../../../../core/blatant_old_msvc_compiler_hacks.hpp"
#	include "../../../../core/non_copyable.hpp"
#	include "../../../../core/ttypes.hpp"
#	include "../../../../core/unicode_conversions.hpp"

#	include "boost\date_time\microsec_time_clock.hpp"
#	include "boost\thread\thread_time.hpp"

#	pragma warning(pop)

#	include <algorithm>
#	include <cassert>
#	include <functional>
#	include <iomanip>
#	include <iostream>
#	include <limits>
#	include <list>
#	include <map>
#	include <memory>
#	include <ostream>
#	include <set>
#	include <sstream>
#	include <string>
#	include <vector>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif	// !defined(AFX_STDAFX_H__F585FBDB_34EB_4369_BEB4_2190904D9C11__INCLUDED_)
