/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// libjmmcg.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#include "logging.hpp"

#include <afxdllx.h>

static AFX_EXTENSION_MODULE LibjmmcgDLL= {NULL, NULL};

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved) {
	UNREFERENCED_PARAMETER(lpReserved);

	if(dwReason == DLL_PROCESS_ATTACH) {
		LIBJMMCG_LOG_TRIVIAL(trace) << "LIBJMMCG.DLL Initializing! Instance: 0x"<<static_cast<void *>(hInstance));

		if(!AfxInitExtensionModule(LibjmmcgDLL, hInstance))
			return 0;

		new CDynLinkLibrary(LibjmmcgDLL);
	} else if(dwReason == DLL_PROCESS_DETACH) {
		LIBJMMCG_LOG_TRIVIAL(trace) << "LIBJMMCG.DLL Terminating! Instance: 0x"<<static_cast<void *>(hInstance));
		AfxTermExtensionModule(LibjmmcgDLL);
	}
	return 1;
}
