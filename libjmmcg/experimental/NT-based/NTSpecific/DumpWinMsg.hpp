/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// Function to convert a Windows error code to a string.

#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>

namespace jmmcg { namespace NTUtils {

#ifndef _TSTRING_DEF
#		define _TSTRING_DEF
		typedef std::basic_string<TCHAR, std::char_traits<TCHAR> > tstring;
#endif _TSTRING_DEF

	// Handy-dandy function for turning windows error codes into strings...
	inline tstring __fastcall DumpWinMessage(const unsigned long err_no) {
		TCHAR *lpMsgBuf;
		::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,err_no,MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),reinterpret_cast<TCHAR *>(&lpMsgBuf),0,NULL);
		const tstring str(lpMsgBuf ? lpMsgBuf : _T("No Windows message string available."));
		::LocalFree(lpMsgBuf);
		return str;
	}

} }
