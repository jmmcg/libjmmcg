/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "NetAPIDeallocator.hpp"
#include "ScheduledTask.hpp"

#include "../../../core/unicode_conversions.hpp"

/////////////////////////////////////////////////////////////////////////////

const TCHAR NetScheduleJobAdd_name[]= _T("NetScheduleJobAdd");
const TCHAR NetScheduleJobDel_name[]= _T("NetScheduleJobDel");

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

ScheduledTask::ScheduledTask(void)
	: LoadLibraryWrapper(netapi32_nt_lib_name),
	  pNetScheduleJobAdd(reinterpret_cast<NetScheduleJobAddType>(::GetProcAddress(Handle(), NetScheduleJobAdd_name))),
	  pNetScheduleJobDel(reinterpret_cast<NetScheduleJobDelType>(::GetProcAddress(Handle(), NetScheduleJobDel_name))) {
	DEBUG_ASSERT(pNetScheduleJobAdd);
	DEBUG_ASSERT(pNetScheduleJobDel);
}

ScheduledTask::~ScheduledTask(void) {
	std::vector<std::pair<std::wstring, unsigned long> >::iterator iter= jobs.begin();
	while(iter != jobs.end()) {
		(*pNetScheduleJobDel)(iter->first.c_str(), iter->second, iter->second);
		LIBJMMCG_LOG_TRIVIAL(trace) << "ScheduledTask::~ScheduledTask(): Deleted job: ID:" << iter->second << _T(" on machine: ") << WStringToTString(iter->first);
		++iter;
	}
}

bool
ScheduledTask::AddJob(const std::wstring& machine, const AT_INFO& at, unsigned long& jobid) {
	if((*pNetScheduleJobAdd)(machine.c_str(), const_cast<BYTE*>(reinterpret_cast<const BYTE*>(&at)), &jobid) == NERR_Success) {
		jobs.push_back(jobs_list_type::value_type(machine, jobid));
#ifdef _DEBUG
		LIBJMMCG_LOG_TRIVIAL(trace) << "ScheduledTask::AddJob(...): Added job: ID:" << jobid << " on machine: " << WStringToTString(machine) << ". Job time: " << Time(at.JobTime);
		TCHAR buff[33];
		_ultot_s(at.DaysOfMonth, buff, 2);
		LIBJMMCG_LOG_TRIVIAL(trace) << fmt::format("Days it runs on in a month {%032.32s}", buff);
		_itot_s((int)at.DaysOfWeek, buff, 2);
		LIBJMMCG_LOG_TRIVIAL(trace) << fmt::format("Days it runs on in a week {%08.8s}", buff);
		LIBJMMCG_LOG_TRIVIAL(trace) << "Job flags:" << (short unsigned int)at.Flags;
		LIBJMMCG_LOG_TRIVIAL(trace) << "Command line: '" << t.Command << "'";
#endif
		return false;
	}
	return true;
}

bool
ScheduledTask::DeleteJob(const std::wstring& machine, const unsigned long job_id) {
	if((*pNetScheduleJobDel)(machine.c_str(), job_id, job_id) == NERR_Success) {
		const jobs_list_type::iterator iter(
			std::find_if(
				jobs.begin(),
				jobs.end(),
				std::bind(
					SameObj<std::pair<std::wstring, unsigned long>, jobs_list_type::value_type>(),
					std::placeholders::_1,
					jobs_list_type::value_type(machine, job_id))));
		if(iter != jobs.end()) {
			jobs.erase(iter);
			LIBJMMCG_LOG_TRIVIAL(trace) << "ScheduledTask::DeleteJob(...): Deleted job: ID:" << job_id << " on machine: " << WStringToTString(machine);
		}
		return false;
	}
	LIBJMMCG_LOG_TRIVIAL(trace) << "ScheduledTask::DeleteJob(...): Failed to delete job: ID:" << job_id << " on machine: " << WStringToTString(machine);
	return true;
}

tstring
ScheduledTask::Time(const unsigned long msec) {
	tostringstream ret;
	ret
		<< std::setw(2) << std::setfill(_T('0')) << msec / 3600000	 // hrs
		<< _T(":") << std::setw(2) << std::setfill(_T('0')) << (msec % 3600000) / 60000	 // mins
		<< _T(":") << std::setw(2) << std::setfill(_T('0')) << (msec % 60000) / 1000	 // sec
		<< _T(".") << std::setw(2) << std::setfill(_T('0')) << msec % 1000;	 // msec
	return ret.str();
}
