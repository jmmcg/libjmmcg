/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Scabbed from the "writeavi" MSDN example.

#include"Load n Save BMPs.hpp"
#include"SystemTime.hpp"
#include<vfw.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	const unsigned long bazoom=0x00000010L; // this frame is a key frame.

	class AVIFile {
	public:
		inline __stdcall AVIFile(const tchar *file_name);
		inline __stdcall ~AVIFile(void);

		inline bool __fastcall Initialise(const HWND parent,const UINT uiflags,const unsigned int fps,BitMapInfoWrapper &bminfo,const std::string &copyright,const std::string &creation_date=TStringToString(SystemTimeAsStr()));

		inline bool __fastcall WriteFrame(const unsigned int duration,const HBITMAP &frame,const unsigned short colour_depth);
		inline bool __fastcall WriteFrame(const unsigned int duration,BitMapInfoWrapper &frame);

		static inline BitMapInfoWrapper __fastcall MakeDIB(const HBITMAP hbitmap,const unsigned short colour_depth);

	private:
		PAVIFILE pfile;
		PAVISTREAM ps,psCompressed,psText;
		unsigned int frame_number;
		bool initialised;

		// I don't allow copying or assignment.
		inline __stdcall AVIFile(const AVIFile &) noexcept(true);
		inline AVIFile & __fastcall operator=(const AVIFile &) noexcept(true);
	};

	inline __stdcall
	AVIFile::AVIFile(const tchar *file_name)
	: pfile(),ps(),psCompressed(),psText(),frame_number(),initialised(false) {
		AVIFileInit();
		const HRESULT hr = AVIFileOpen(&pfile,		    // returned file pointer
					 file_name,		            // file name
					 OF_WRITE | OF_CREATE,	    // mode to open file with
					 NULL);			    // use handler determined
		DEBUG_ASSERT(hr==AVIERR_OK);
	}

	inline __stdcall
	AVIFile::~AVIFile(void) {
		if (ps) {
			AVIStreamClose(ps);
		}
		if (psCompressed) {
			AVIStreamClose(psCompressed);
		}
		if (psText) {
			AVIStreamClose(psText);
		}
		if (pfile){
			AVIFileClose(pfile);
		}
		AVIFileExit();
	}

	inline bool __fastcall
	AVIFile::Initialise(const HWND parent,const UINT uiflags,const unsigned int fps,BitMapInfoWrapper &bminfo,const std::string &copyright,const std::string &creation_date) {
		AVISTREAMINFO strhdr;
		memset(&strhdr, 0, sizeof(strhdr));
		strhdr.fccType                = streamtypeVIDEO;// stream type
		strhdr.fccHandler             = 0;
		strhdr.dwScale                = 1;
		strhdr.dwRate                 = fps;
		strhdr.dwSuggestedBufferSize  = bminfo.Header()->biSizeImage;
		// And create the stream;
		if (AVIFileCreateStream(pfile,		    // file pointer
							&ps,		    // returned stream pointer
							&strhdr)!=AVIERR_OK) {	    // stream header
			return true;
		}
		AVICOMPRESSOPTIONS opts;
		AVICOMPRESSOPTIONS *aopts[1] = {&opts};
		memset(&opts, 0, sizeof(opts));
		if (!AVISaveOptions(parent, uiflags, 1, &ps, reinterpret_cast<LPAVICOMPRESSOPTIONS *>(&aopts))) {
			 return true;
		}
		if (AVIMakeCompressedStream(&psCompressed, ps, &opts, NULL) != AVIERR_OK) {
			 return true;
		}
		if (AVIStreamSetFormat(psCompressed, 0,
						 bminfo.Info(),	    // stream format
						 bminfo.Header()->biSize   // format size
							+bminfo.Header()->biClrUsed * sizeof(RGBQUAD)) != AVIERR_OK) {
			 return true;
		}
		if (AVIFileWriteData(pfile,mmioFOURCC('I','C','O','P'),const_cast<char *>(copyright.c_str()),copyright.length()) != AVIERR_OK) {
			 return true;
		}
		if (AVIFileWriteData(pfile,mmioFOURCC('I','C','R','D'),const_cast<char *>(creation_date.c_str()),creation_date.length()) != AVIERR_OK) {
			 return true;
		}
		// We need this as the "AVISaveOptions(...)" call is asynchronous, so calls to "WriteFrame(...)" could occur before the initialisation is finished...
		initialised=true;
		return false;
	}

	inline bool __fastcall
	AVIFile::WriteFrame(const unsigned int duration,const HBITMAP &frame,const unsigned short colour_depth) {
		if (initialised) {
			BitMapInfoWrapper bm(frame,colour_depth);
			const bool ret=WriteFrame(duration,bm);
			return ret;
		}
		return true;
	}

	inline bool __fastcall
	AVIFile::WriteFrame(const unsigned int duration,BitMapInfoWrapper &frame) {
		if (initialised) {
			if (AVIStreamWrite(psCompressed,	// stream pointer
				frame_number * duration,				// time of this frame
				1,				// number to write
				reinterpret_cast<BYTE *>(frame.Info()->bmiColors)		// pointer to data
					+frame.Header()->biClrUsed * sizeof(RGBQUAD),
				frame.Header()->biSizeImage,	// size of this frame
				AVIIF_KEYFRAME,			 // flags....
				NULL,
				NULL) != AVIERR_OK) {
				return true;
			}
			++frame_number;
			return false;
		}
		return true;
	}

} }
