/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "ServiceManipulation.hpp"

#include "../../../core/exception.hpp"

/////////////////////////////////////////////////////////////////////////////

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

const TCHAR CloseServiceHandle_name[]= _T("CloseServiceHandle");
#ifdef UNICODE
const TCHAR OpenSCManager_name[]= _T("OpenSCManagerW");
const TCHAR OpenService_name[]= _T("OpenServiceW");
const TCHAR StartService_name[]= _T("StartServiceW");
#else
const TCHAR OpenSCManager_name[]= _T("OpenSCManagerA");
const TCHAR OpenService_name[]= _T("OpenServiceA");
const TCHAR StartService_name[]= _T("StartServiceA");
#endif	// !UNICODE
const TCHAR ControlService_name[]= _T("ControlService");

/////////////////////////////////////////////////////////////////////////////

typedef WINADVAPI
SC_HANDLE(WINAPI* const OpenSCManagerType)(LPCTSTR lpMachineName, LPCTSTR lpDatabaseName, DWORD dwDesiredAccess);

/////////////////////////////////////////////////////////////////////////////

ServiceManipulation::ServiceManipulation(const TCHAR* const machine)
	: LoadLibraryWrapper(scm_lib_name),
	  pCloseServiceHandle(reinterpret_cast<CloseServiceHandleType>(::GetProcAddress(Handle(), CloseServiceHandle_name))),
	  pOpenService(reinterpret_cast<OpenServiceType>(::GetProcAddress(Handle(), OpenService_name))),
	  pControlService(reinterpret_cast<ControlServiceType>(::GetProcAddress(Handle(), ControlService_name))),
	  pStartService(reinterpret_cast<StartServiceType>(::GetProcAddress(Handle(), StartService_name))) {
	DEBUG_ASSERT(pCloseServiceHandle);
	DEBUG_ASSERT(pOpenService);
	DEBUG_ASSERT(pControlService);
	DEBUG_ASSERT(pStartService);
	const OpenSCManagerType pOpenSCManager= reinterpret_cast<OpenSCManagerType>(::GetProcAddress(Handle(), OpenSCManager_name));
	DEBUG_ASSERT(pOpenSCManager);
	scm= (*pOpenSCManager)(machine, NULL, SC_MANAGER_ALL_ACCESS);
	if(!scm) {
		throw exception_type(_T("Failed to open the Service Control Manager on the specified machine."), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T("const TCHAR * const"), machine)), __REV_INFO__);
	}
	curr_service_name= NULL;
}

ServiceManipulation::~ServiceManipulation(void) {
	if(curr_service_name) {
		(*pCloseServiceHandle)(curr_service);
		LIBJMMCG_LOG_TRIVIAL(trace) << "ServiceManipulation::~ServiceManipulation(): Closing service: 0x" << std::hex << curr_service << ", windows error code: " << NTUtils::win_exception::StrFromWinErr(::GetLastError());
	}
	(*pCloseServiceHandle)(scm);
	LIBJMMCG_LOG_TRIVIAL(trace) << "ServiceManipulation::~ServiceManipulation(): Closing service control manager: 0x" << std::hex << scm << ", windows error code: " << NTUtils::win_exception::StrFromWinErr(::GetLastError());
}

void
ServiceManipulation::OpenService(const TCHAR* const service_name, const DWORD access) {
	if(curr_service_name && _tcscmp(curr_service_name, service_name)) {
		(*pCloseServiceHandle)(curr_service);
		LIBJMMCG_LOG_TRIVIAL(trace) << "ServiceManipulation::OpenService(...): Closing service: 0x" << std::hex << curr_service << ", windows error code: " << NTUtils::win_exception::StrFromWinErr(::GetLastError());
		curr_service_name= NULL;
	}
	if(!curr_service_name) {
		curr_service= (*pOpenService)(scm, curr_service_name= service_name, access);
		if(!curr_service) {
			info::function info(__LINE__, __PRETTY_FUNCTION__, typeid(*this), info::function::argument(_T("const TCHAR * const"), service_name));
			info.add_arg(_T("const DWORD"), access);
			throw exception_type(_T("Failed to open the specified service with the specified access mask."), info, __REV_INFO__);
		}
	}
	DEBUG_ASSERT(curr_service_name && curr_service);
}
