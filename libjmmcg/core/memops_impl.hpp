/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {

namespace private_ {

template<
	unsigned long long N	  ///< The number of times the operation should be applied.
	>
struct unroll {
	/// Apply the specified operation the specified number of times, sequentially.
	/**
		\param	o	The operation to apply.
	*/
	template<class Op>
	static constexpr void
	result(Op&& o) noexcept(noexcept(o.operator()(N - 1))) {
		o.operator()(N - 1);
		unroll<N - 1>::result(std::forward<Op>(o));
	}
	template<class Op>
	static constexpr bool
	result_b(Op&& o) noexcept(noexcept(o.operator()(N - 1))) {
		const bool res= o.operator()(N - 1);
		return res ? unroll<N - 1>::result_b(std::forward<Op>(o)) : false;
	}
};
template<>
struct unroll<0ull> {
	template<class Op>
	static constexpr void
	result(Op const&) noexcept(true) {
	}
	template<class Op>
	[[gnu::const]] static constexpr bool
	result_b(Op const&) noexcept(true) {
		return true;
	}
};

namespace bits {

template<class T>
[[gnu::always_inline, gnu::pure]] inline constexpr T
clear_leftmost_set(const T value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return value & (value - 1);
}

template<class T>
inline constexpr unsigned
count_trailing_zeroes(const T value) noexcept(true)
	= delete;
template<>
[[gnu::always_inline, gnu::pure]] inline unsigned
count_trailing_zeroes<std::uint32_t>(const std::uint32_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return static_cast<unsigned>(__builtin_ctz(value));
}
template<>
[[gnu::always_inline, gnu::pure]] inline unsigned
count_trailing_zeroes<std::uint64_t>(const std::uint64_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return static_cast<unsigned>(__builtin_ctzl(value));
}
template<>
inline unsigned
count_trailing_zeroes<uint128_t>(const uint128_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return count_trailing_zeroes_compat(value);
}

template<class T>
inline constexpr unsigned
count_leading_zeroes(const T value) noexcept(true)
	= delete;
template<>
[[gnu::always_inline, gnu::pure]] inline unsigned
count_leading_zeroes<std::uint32_t>(const std::uint32_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return static_cast<unsigned>(__builtin_clz(value));
}
template<>
[[gnu::always_inline, gnu::pure]] inline unsigned
count_leading_zeroes<std::uint64_t>(const std::uint64_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return static_cast<unsigned>(__builtin_clzl(value));
}
template<>
[[gnu::pure]] inline unsigned
count_leading_zeroes<uint128_t>(const uint128_t value) noexcept(true) {
	DEBUG_ASSERT(value != 0);
	return count_leading_zeroes_compat(value);
}

}

template<
	std::size_t SrcSz,
	std::size_t DestSz,
	class Unit,
	std::size_t SmallestBuff= min<std::size_t, SrcSz, DestSz>::value,
	std::size_t Div= SmallestBuff / sizeof(Unit),
	std::size_t Rem= SmallestBuff % sizeof(Unit)>
struct aligned_unroller {
	using element_type= Unit;
	enum : std::size_t {
		src_sz= SrcSz,
		dest_sz= DestSz,
		smallest_buff= SmallestBuff,
		div= Div,	///< How many whole Units can be copied.
		rem= Rem,
		end= smallest_buff - rem	///< How much of the buffer was copied.
	};
	using unrolled_op_t= private_::unroll<div>;

	static_assert(src_sz > 0 && dest_sz > 0, "Buffers must be non-zero.");
	BOOST_MPL_ASSERT_RELATION(smallest_buff, >, 0);
	BOOST_MPL_ASSERT_RELATION(sizeof(Unit), >, 0);
	BOOST_MPL_ASSERT_RELATION(div, >=, 0);
	BOOST_MPL_ASSERT_RELATION(rem, >=, 0);
	BOOST_MPL_ASSERT_RELATION(end, >=, 0);
	static_assert(smallest_buff <= src_sz && smallest_buff <= dest_sz, "TODO.");
	BOOST_MPL_ASSERT_RELATION(rem, <=, sizeof(Unit));
	BOOST_MPL_ASSERT_RELATION((sizeof(Unit) * div), <=, smallest_buff);
	BOOST_MPL_ASSERT_RELATION(rem, <=, smallest_buff);
	BOOST_MPL_ASSERT_RELATION(end, <=, smallest_buff);
	static_assert((div == 0 && rem <= sizeof(Unit)) || (div > 0 && end <= smallest_buff), "TODO.");

	template<class Op>
	static constexpr void
	result(Op&& o) noexcept(noexcept(unrolled_op_t::result(std::forward<Op>(o)))) {
		// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
		unrolled_op_t::result(std::forward<Op>(o));
	}
	template<class Op>
	static constexpr bool
	result_b(Op&& o) noexcept(noexcept(unrolled_op_t::result(std::forward<Op>(o)))) {
		// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
		return unrolled_op_t::result_b(std::forward<Op>(o));
	}
};
/**
	The buffer is not in units of Unit, so no-op.
*/
template<
	std::size_t SrcSz,
	std::size_t DestSz,
	class Unit,
	std::size_t SmallestBuff,
	std::size_t Rem>
struct aligned_unroller<SrcSz, DestSz, Unit, SmallestBuff, 0, Rem> {
	using element_type= Unit;
	enum : std::size_t {
		src_sz= SrcSz,
		dest_sz= DestSz,
		smallest_buff= SmallestBuff,
		div= std::size_t(),
		rem= Rem,
		end= std::size_t()
	};

	template<class Op>
	static constexpr void
	result(Op const&) noexcept(true) {
	}
	template<class Op>
	static constexpr bool
	result_b(Op const&) noexcept(true) {
		return true;
	}
};

struct greater_than_eq_512 {};
struct greater_than_eq_256 {};
struct greater_than_eq_128 {};
struct char_sized {};

template<std::size_t FirstSz>
class select_size {
private:
	enum : std::size_t {
		avx512_sz=
#ifdef __AVX512__
			sizeof(__m512i),
#elif defined(__AVX__)
			sizeof(__m256i),
#elif defined(__SSE2__)
			sizeof(__m128i),
#else
			1,
#endif
		avx_sz=
#ifdef __AVX__
			sizeof(__m256i),
#elif defined(__SSE2__)
			sizeof(__m128i),
#else
			1,
#endif
		sse2_sz=
#ifdef __SSE2__
			sizeof(__m128i),
#else
			1,
#endif
	};
	using sz512=
#ifdef __AVX512__
		greater_than_eq_512;
#elif defined(__AVX__)
		greater_than_eq_256;
#elif defined(__SSE2__)
		greater_than_eq_128;
#else
		char_sized;
#endif
	using sz256=
#ifdef __AVX__
		greater_than_eq_256;
#elif defined(__SSE2__)
		greater_than_eq_128;
#else
		char_sized;
#endif
	using sz128=
#ifdef __SSE2__
		greater_than_eq_128;
#else
		char_sized;
#endif

public:
	using type= typename std::conditional<
		FirstSz >= avx512_sz,
		sz512,
		typename std::conditional<
			FirstSz >= avx_sz,
			sz256,
			typename std::conditional<
				FirstSz >= sse2_sz,
				sz128,
				char_sized>::type>::type>::type;
};

template<
	char const needle,
	std::size_t FirstSz,
	class Sz>
struct strchr_opt;
#ifdef __AVX512__
template<
	char const needle,
	std::size_t FirstSz>
struct strchr_opt<needle, FirstSz, greater_than_eq_512> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack) noexcept(true) {
		using element_type= __m512i;
		struct strchr_opt_int {
			static std::byte const* [[gnu::no_sanitize("address"), gnu::pure]] result(std::byte const* const haystack) noexcept(true) {
				const element_type conv= _mm512_set1_epi8(needle);
				const element_type block_first= _mm512_loadu_si512(reinterpret_cast<element_type const*>(haystack));
				const element_type eq_needle= _mm512_movm_epi8(_mm512_cmpeq_epi8_mask(conv, block_first));
				const __mmask64 mask= _mm512_movepi8_mask(eq_needle);
				if(mask != 0) {
					const auto bitpos= private_::bits::count_trailing_zeroes(mask);
					return haystack + bitpos;
				} else {
					return nullptr;
				}
			}
		};

		const auto ret= strchr_opt_int::result(haystack);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strchr_opt<
					needle,
					next_portion,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type));
			} else {
				return nullptr;
			}
		}
	}
};
#endif
#ifdef __AVX__
template<
	char const needle,
	std::size_t FirstSz>
struct strchr_opt<needle, FirstSz, greater_than_eq_256> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack) noexcept(true) {
		using element_type= __m256i;
		struct strchr_opt_int {
			static std::byte const* result(std::byte const* const haystack) noexcept(true) {
				const element_type conv= _mm256_set1_epi8(needle);
				const element_type block_first= _mm256_loadu_si256(reinterpret_cast<element_type const*>(haystack));
				const element_type eq_needle= _mm256_cmpeq_epi8(conv, block_first);
				const auto mask= _mm256_movemask_epi8(eq_needle);
				if(mask != 0) {
					const auto bitpos= private_::bits::count_trailing_zeroes(static_cast<std::uint32_t>(mask));
					return haystack + bitpos;
				} else {
					return nullptr;
				}
			}
		};

		const auto ret= strchr_opt_int::result(haystack);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strchr_opt<
					needle,
					next_portion,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type));
			} else {
				return nullptr;
			}
		}
	}
};
#endif
#ifdef __SSE2__
template<
	char const needle,
	std::size_t FirstSz>
struct strchr_opt<needle, FirstSz, greater_than_eq_128> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack) noexcept(true) {
		using element_type= __m128i;
		struct strchr_opt_int {
			static std::byte const* result [[gnu::no_sanitize("address"), gnu::pure]] (std::byte const* const haystack) noexcept(true) {
				const element_type conv= _mm_set1_epi8(needle);
				const element_type block_first= _mm_loadu_si128(reinterpret_cast<element_type const*>(haystack));
				const element_type eq_needle= _mm_cmpeq_epi8(conv, block_first);
				const auto mask= _mm_movemask_epi8(eq_needle);
				if(mask != 0) {
					const auto bitpos= private_::bits::count_trailing_zeroes(static_cast<std::uint32_t>(mask));
					return haystack + bitpos;
				} else {
					return nullptr;
				}
			}
		};

		const auto ret= strchr_opt_int::result(haystack);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strchr_opt<
					needle,
					next_portion,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type));
			} else {
				return nullptr;
			}
		}
	}
};
#endif
template<
	char const needle,
	std::size_t FirstSz>
struct strchr_opt<needle, FirstSz, char_sized> {
	static constexpr std::byte const*
	result(std::byte const* const haystack) noexcept(true) {
		return reinterpret_cast<std::byte const*>(std::strchr(reinterpret_cast<char const*>(haystack), needle));
	}
};

template<
	std::size_t FirstSz,
	std::size_t SecondSz,
	class Sz>
struct strstr_opt;
#ifdef __AVX512__
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
struct strstr_opt<FirstSz, SecondSz, greater_than_eq_512> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
		using element_type= __m512i;
		struct strchr_opt_int {
			static char const* [[gnu::pure]] result(std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
				const element_type first= _mm512_set1_epi8(needle[0]);
				const element_type last= _mm512_set1_epi8(needle[SecondSz - 1]);
				const element_type block_first= _mm512_loadu_si512(reinterpret_cast<element_type const*>(haystack));
				const element_type block_last= _mm512_loadu_si512(reinterpret_cast<element_type const*>(haystack + SecondSz - 1));
				const element_type eq_first= _mm512_movm_epi8(_mm512_cmpeq_epi8_mask(first, block_first));
				const element_type eq_last= _mm512_movm_epi8(_mm512_cmpeq_epi8_mask(last, block_last));
				__mmask64 mask= _mm512_movepi8_mask(_mm512_or_si512(eq_first, eq_last));
				while(mask != 0) {
					if constexpr((SecondSz - 2) > 0) {
						const auto bitpos= private_::bits::count_trailing_zeroes(mask);
						if((bitpos + 1 + SecondSz - 2) <= FirstSz) {
							using needle_cmp_t= char const[SecondSz - 2];
							if(memcmp_opt(reinterpret_cast<needle_cmp_t&>(haystack[bitpos + 1]), reinterpret_cast<needle_cmp_t&>(needle[1]))) {
								return haystack + bitpos;
							}
							mask= private_::bits::clear_leftmost_set(mask);
						} else {
							return nullptr;
						}
					} else {
						return haystack;
					}
				}
				return nullptr;
			}
		};

		const auto ret= strstr_opt_int::result(haystack, needle);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strstr_opt<
					next_portion,
					SecondSz,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type), needle);
			} else {
				return nullptr;
			}
		}
		return nullptr;
	}
};
#endif
#ifdef __AVX2__
// TODO
#endif
/**
 * \Note Look at "eve" library for simd. <a href "
 */
#ifdef __AVX__
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
struct strstr_opt<FirstSz, SecondSz, greater_than_eq_256> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
		using element_type= __m256i;
		struct strstr_opt_int {
			static std::byte const* result [[gnu::no_sanitize("address"), gnu::pure]] (std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
				const element_type first= _mm256_set1_epi8(char(needle[0]));
				const element_type last= _mm256_set1_epi8(char(needle[SecondSz - 1]));
				BOOST_MPL_ASSERT_RELATION(FirstSz, >=, sizeof(__m256i));
				const element_type block_first= _mm256_lddqu_si256(reinterpret_cast<element_type const*>(haystack));
				const element_type block_last= _mm256_lddqu_si256(reinterpret_cast<element_type const*>(haystack + SecondSz - 1));
				const element_type eq_first= _mm256_cmpeq_epi8(first, block_first);
				const element_type eq_last= _mm256_cmpeq_epi8(last, block_last);
				auto mask= _mm256_movemask_epi8(_mm256_or_si256(eq_first, eq_last));
				while(mask != 0) {
					if constexpr((SecondSz - 2) > 0) {
						const auto bitpos= private_::bits::count_trailing_zeroes(static_cast<std::uint32_t>(mask));
						if((bitpos + 1 + SecondSz - 2) <= FirstSz) {
							using needle_cmp_t= std::byte const[SecondSz - 2];
							if(memcmp_opt(reinterpret_cast<needle_cmp_t&>(haystack[bitpos + 1]), reinterpret_cast<needle_cmp_t&>(needle[1]))) {
								return haystack + bitpos;
							}
							mask= private_::bits::clear_leftmost_set(mask);
						} else {
							return nullptr;
						}
					} else {
						return haystack;
					}
				}
				return nullptr;
			}
		};

		const auto ret= strstr_opt_int::result(haystack, needle);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strstr_opt<
					next_portion,
					SecondSz,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type), needle);
			} else {
				return nullptr;
			}
		}
		return nullptr;
	}
};
#endif
#ifdef __SSE2__
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
struct strstr_opt<FirstSz, SecondSz, greater_than_eq_128> {
	[[gnu::pure]] static constexpr std::byte const*
	result(std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
		using element_type= __m128i;
		struct strstr_opt_int {
			static std::byte const* result [[gnu::no_sanitize("address"), gnu::pure]] (std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
				const element_type first= _mm_set1_epi8(char(needle[0]));
				const element_type last= _mm_set1_epi8(char(needle[SecondSz - 1]));
				const element_type block_first= _mm_loadu_si128(reinterpret_cast<element_type const*>(haystack));
				const element_type block_last= _mm_loadu_si128(reinterpret_cast<element_type const*>(haystack + SecondSz - 1));
				const element_type eq_first= _mm_cmpeq_epi8(first, block_first);
				const element_type eq_last= _mm_cmpeq_epi8(last, block_last);
				auto mask= _mm_movemask_epi8(_mm_or_si128(eq_first, eq_last));
				while(mask != 0) {
					if constexpr((SecondSz - 2) > 0) {
						const auto bitpos= private_::bits::count_trailing_zeroes(static_cast<std::uint32_t>(mask));
						if((bitpos + 1 + SecondSz - 2) <= FirstSz) {
							using needle_cmp_t= std::byte const[SecondSz - 2];
							if(memcmp_opt(reinterpret_cast<needle_cmp_t&>(haystack[bitpos + 1]), reinterpret_cast<needle_cmp_t&>(needle[1]))) {
								return haystack + bitpos;
							}
							mask= private_::bits::clear_leftmost_set(mask);
						} else {
							return nullptr;
						}
					} else {
						return haystack;
					}
				}
				return nullptr;
			}
		};

		const auto ret= strstr_opt_int::result(haystack, needle);
		if(ret) {
			return ret;
		} else {
			constexpr const std::size_t next_portion= ((FirstSz >= sizeof(element_type)) ? (FirstSz - sizeof(element_type)) : FirstSz);
			if(next_portion) {
				return strstr_opt<
					next_portion,
					SecondSz,
					typename select_size<next_portion>::type>::result(haystack + sizeof(element_type), needle);
			} else {
				return nullptr;
			}
		}
		return nullptr;
	}
};
#endif
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
struct strstr_opt<FirstSz, SecondSz, char_sized> {
	static constexpr std::byte const*
	result(std::byte const* const haystack, std::byte const (&needle)[SecondSz]) noexcept(true) {
		return reinterpret_cast<std::byte const*>(std::strstr(reinterpret_cast<char const*>(haystack), reinterpret_cast<char const*>(needle)));
	}
};

}

template<class Iter1, class Iter2>
inline void
memcpy(Iter1 dest, Iter2 src, std::size_t n) noexcept(true) {
	std::uninitialized_copy(src, src + n, dest);
}

template<>
inline void
memcpy<char*, char const*>(char* dest, char const* src, std::size_t n) noexcept(true) {
	std::memcpy(dest, src, n);
}

template<>
inline void
memcpy<wchar_t*, wchar_t const*>(wchar_t* dest, wchar_t const* src, std::size_t n) noexcept(true) {
	std::wmemcpy(dest, src, n);
}

template<class Iter1, class Iter2>
inline void
memmove(Iter1 dest, Iter2 src, std::size_t n) noexcept(true) {
	std::uninitialized_copy(src, src + n, dest);
}

template<>
inline void
memmove<char*, char const*>(char* dest, char const* src, std::size_t n) noexcept(true) {
	std::memmove(dest, src, n);
}

template<>
inline void
memmove<wchar_t*, wchar_t const*>(wchar_t* dest, wchar_t const* src, std::size_t n) noexcept(true) {
	std::wmemmove(dest, src, n);
}

template<class Iter, class V>
inline typename std::enable_if<std::is_same<typename std::iterator_traits<Iter>::value_type, V>::value>::type
memset(Iter dest, V i, std::size_t n) noexcept(true) {
	std::fill_n(dest, n, i);
}

template<>
inline void
memset<char*, char>(char* dest, char i, std::size_t n) noexcept(true) {
	std::memset(dest, i, n);
}

template<>
inline void
memset<wchar_t*, wchar_t>(wchar_t* dest, wchar_t i, std::size_t n) noexcept(true) {
	std::wmemset(dest, i, n);
}

template<class Iter>
inline bool
memcmp(Iter src1, Iter src2, std::size_t n) noexcept(true) {
	return std::equal(src1, src1 + n, src2);
}

template<>
inline bool
memcmp<char const*>(char const* src1, char const* src2, std::size_t n) noexcept(true) {
	return std::memcmp(src1, src2, n) == 0;
}

template<>
inline bool
memcmp<wchar_t const*>(wchar_t const* src1, wchar_t const* src2, std::size_t n) noexcept(true) {
	return std::wmemcmp(src1, src2, n) == 0;
}

template<class Val, std::size_t SrcSz, std::size_t DestSz>
void
memcpy(Val const (&src)[SrcSz], Val (&dest)[DestSz]) noexcept(false) {
	enum : std::size_t {
		smallest_buff= min<std::size_t, SrcSz, DestSz>::value
	};
	using unrolled_op_t= private_::unroll<smallest_buff>;
	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
	unrolled_op_t::result([&src, &dest](std::size_t i) {
		dest[i]= src[i];
	});
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr void
memcpy_opt(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true) {
	using unrolled_512_op_t= private_::aligned_unroller<
		SrcSz,
		DestSz,
#ifdef __AVX512F__
		__m512
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
	using unrolled_256_op_t= private_::aligned_unroller<
		SrcSz - unrolled_512_op_t::end,
		DestSz - unrolled_512_op_t::end,
#ifdef __AVX__
		__m256
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
	using unrolled_128_op_t= private_::aligned_unroller<
		SrcSz - unrolled_256_op_t::end,
		DestSz - unrolled_256_op_t::end,
#ifdef __SSE__
		__m128
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
#pragma GCC diagnostic pop
	using unrolled_64_op_t= private_::aligned_unroller<
		SrcSz - unrolled_256_op_t::end - unrolled_128_op_t::end,
		DestSz - unrolled_256_op_t::end - unrolled_128_op_t::end,
		std::uint64_t>;
	using unrolled_32_op_t= private_::aligned_unroller<
		SrcSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end,
		DestSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end,
		std::uint32_t>;
	using unrolled_16_op_t= private_::aligned_unroller<
		SrcSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end,
		DestSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end,
		std::uint16_t>;
	using unrolled_8_op_t= private_::aligned_unroller<
		SrcSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end - unrolled_16_op_t::end,
		DestSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end - unrolled_16_op_t::end,
		std::uint8_t>;
	// After all of the larger word sizes have been use we MUST end up with only zero or one byte to copy...
	static_assert(
		(
			unrolled_512_op_t::smallest_buff - unrolled_512_op_t::end
			- unrolled_256_op_t::end
			- unrolled_128_op_t::end
			- unrolled_64_op_t::end
			- unrolled_32_op_t::end
			- unrolled_16_op_t::end)
			<= 1,
		"Oh b*ll*x. The unrolling meta-program is seriously fsck'd, please file a bug report.");

	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
#ifdef __AVX512F__
	unrolled_512_op_t::result(
		[&src, &dest](std::size_t i) {
			const typename unrolled_512_op_t::element_type tmp= _mm512_loadu_ps(&((src + unrolled_256_op_t::end)[i]));
			_mm512_storeu_ps(&((dest + unrolled_256_op_t::end)[i]), tmp);
		});
#endif
#ifdef __AVX__
	unrolled_256_op_t::result(
		[&src, &dest](std::size_t i) {
			const typename unrolled_256_op_t::element_type tmp= _mm256_loadu_ps(reinterpret_cast<float const*>(&((src + unrolled_256_op_t::end)[i])));
			_mm256_storeu_ps(reinterpret_cast<float*>(&((dest + unrolled_256_op_t::end)[i])), tmp);
		});
#endif
#ifdef __SSE__
	unrolled_128_op_t::result(
		[&src, &dest](std::size_t i) {
			const typename unrolled_128_op_t::element_type tmp= _mm_loadu_ps(reinterpret_cast<float const*>(&((src + unrolled_256_op_t::end)[i])));
			_mm_storeu_ps(reinterpret_cast<float*>(&((dest + unrolled_256_op_t::end)[i])), tmp);
		});
#endif
	class assign64_t {
	public:
		constexpr assign64_t(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true)
			: src_(src), dest_(dest) {
		}

		void operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			reinterpret_cast<uint64_t*>(dest_ + unrolled_256_op_t::end + unrolled_128_op_t::end)[i]= reinterpret_cast<uint64_t const*>(src_ + unrolled_256_op_t::end + unrolled_128_op_t::end)[i];
		}

	private:
		std::byte const (&src_)[SrcSz];
		std::byte (&dest_)[DestSz];
	};
	unrolled_64_op_t::result(assign64_t(src, dest));
	class assign32_t {
	public:
		constexpr assign32_t(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true)
			: src_(src), dest_(dest) {
		}

		void operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			reinterpret_cast<uint32_t*>(dest_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end)[i]= reinterpret_cast<uint32_t const*>(src_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end)[i];
		}

	private:
		std::byte const (&src_)[SrcSz];
		std::byte (&dest_)[DestSz];
	};
	unrolled_32_op_t::result(assign32_t(src, dest));
	class assign16_t {
	public:
		constexpr assign16_t(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true)
			: src_(src), dest_(dest) {
		}

		void operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			reinterpret_cast<std::uint16_t*>(dest_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end)[i]= reinterpret_cast<std::uint16_t const*>(src_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end)[i];
		}

	private:
		std::byte const (&src_)[SrcSz];
		std::byte (&dest_)[DestSz];
	};
	unrolled_16_op_t::result(assign16_t(src, dest));
	class assign8_t {
	public:
		constexpr assign8_t(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true)
			: src_(src), dest_(dest) {
		}

		void operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			(dest_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end + unrolled_16_op_t::end)[i]= (src_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end + unrolled_16_op_t::end)[i];
		}

	private:
		std::byte const (&src_)[SrcSz];
		std::byte (&dest_)[DestSz];
	};
	unrolled_8_op_t::result(assign8_t(src, dest));
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
inline constexpr void
memcpy_opt(char const (&src)[SrcSz], char (&dest)[DestSz]) noexcept(true) {
	memcpy_opt(reinterpret_cast<std::byte const(&)[SrcSz]>(src), reinterpret_cast<std::byte(&)[DestSz]>(dest));
}

template<
	std::size_t FirstSz,
	std::size_t SecondSz>
	requires(FirstSz > 0 && SecondSz > 0)
inline constexpr bool
memcmp_opt(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true) {
	if constexpr(FirstSz != SecondSz) {
		return false;
	}
	using unrolled_512_op_t= private_::aligned_unroller<
		FirstSz,
		SecondSz,
#ifdef __AVX512F__
		__m512
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
	using unrolled_256_op_t= private_::aligned_unroller<
		FirstSz - unrolled_512_op_t::end,
		SecondSz - unrolled_512_op_t::end,
#ifdef __AVX__
		__m256
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
	using unrolled_128_op_t= private_::aligned_unroller<
		FirstSz - unrolled_256_op_t::end,
		SecondSz - unrolled_256_op_t::end,
#ifdef __SSE__
		__m128
#else
		std::byte[std::numeric_limits<short>::max()]	  // A type assuredly bigger than anything reasonable.
#endif
		>;
#pragma GCC diagnostic pop
	using unrolled_64_op_t= private_::aligned_unroller<
		FirstSz - unrolled_256_op_t::end - unrolled_128_op_t::end,
		SecondSz - unrolled_256_op_t::end - unrolled_128_op_t::end,
		std::uint64_t>;
	using unrolled_32_op_t= private_::aligned_unroller<
		FirstSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end,
		SecondSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end,
		std::uint32_t>;
	using unrolled_16_op_t= private_::aligned_unroller<
		FirstSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end,
		SecondSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end,
		std::uint16_t>;
	using unrolled_8_op_t= private_::aligned_unroller<
		FirstSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end - unrolled_16_op_t::end,
		SecondSz - unrolled_256_op_t::end - unrolled_128_op_t::end - unrolled_64_op_t::end - unrolled_32_op_t::end - unrolled_16_op_t::end,
		std::uint8_t>;
	// After all of the larger word sizes have been use we MUST end up with only zero or one byte to copy...
	static_assert(
		(
			unrolled_256_op_t::smallest_buff - unrolled_256_op_t::end
			- unrolled_128_op_t::end
			- unrolled_64_op_t::end
			- unrolled_32_op_t::end
			- unrolled_16_op_t::end)
			<= 1,
		"Oh b*ll*x. The unrolling meta-program is seriously fsck'd, please file a bug report.");

	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
#ifdef __AVX512F__
	const bool res512= unrolled_512_op_t::result_b(
		[&first, &second](std::size_t i) {
			const typename unrolled_512_op_t::element_type f= _mm512_loadu_ps(&((first + unrolled_256_op_t::end)[i]));
			const typename unrolled_512_op_t::element_type s= _mm512_loadu_ps(&((second + unrolled_256_op_t::end)[i]));
			return f == s;
		});
#endif
#ifdef __AVX__
	const bool res256= unrolled_256_op_t::result_b(
#	ifdef __AVX512F__
		res512 &&
#	endif
		[&first, &second](std::size_t i) {
			const typename unrolled_256_op_t::element_type f= _mm256_loadu_ps(reinterpret_cast<float const*>(&((first + unrolled_256_op_t::end)[i])));
			const typename unrolled_256_op_t::element_type s= _mm256_loadu_ps(reinterpret_cast<float const*>(&((second + unrolled_256_op_t::end)[i])));
			return f == s;
		});
#endif
#ifdef __SSE__
	const bool res128=
#	ifdef __AVX__
		res256 &&
#	endif
		unrolled_128_op_t::result_b(
			[&first, &second](std::size_t i) {
				const typename unrolled_128_op_t::element_type f= _mm_loadu_ps(reinterpret_cast<float const*>(&((first + unrolled_256_op_t::end)[i])));
				const typename unrolled_128_op_t::element_type s= _mm_loadu_ps(reinterpret_cast<float const*>(&((second + unrolled_256_op_t::end)[i])));
				return f == s;
			});
#endif
	class assign64_t {
	public:
		constexpr assign64_t(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true)
			: first_(first), second_(second) {
		}

		bool operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			return reinterpret_cast<uint64_t const*>(second_ + unrolled_256_op_t::end + unrolled_128_op_t::end)[i] == reinterpret_cast<uint64_t const*>(first_ + unrolled_256_op_t::end + unrolled_128_op_t::end)[i];
		}

	private:
		std::byte const (&first_)[FirstSz];
		std::byte const (&second_)[SecondSz];
	};
	const bool res64=
#ifdef __SSE__
		res128 &&
#endif
		unrolled_64_op_t::result_b(assign64_t(first, second));
	class assign32_t {
	public:
		constexpr assign32_t(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true)
			: first_(first), second_(second) {
		}

		bool operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			return reinterpret_cast<uint32_t const*>(second_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end)[i] == reinterpret_cast<uint32_t const*>(first_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end)[i];
		}

	private:
		std::byte const (&first_)[FirstSz];
		std::byte const (&second_)[SecondSz];
	};
	const bool res32= res64 && unrolled_32_op_t::result_b(assign32_t(first, second));
	class assign16_t {
	public:
		constexpr assign16_t(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true)
			: first_(first), second_(second) {
		}

		bool operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			return reinterpret_cast<std::uint16_t const*>(second_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end)[i] == reinterpret_cast<std::uint16_t const*>(first_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end)[i];
		}

	private:
		std::byte const (&first_)[FirstSz];
		std::byte const (&second_)[SecondSz];
	};
	const bool res16= res32 && unrolled_16_op_t::result_b(assign16_t(first, second));
	class assign8_t {
	public:
		constexpr assign8_t(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true)
			: first_(first), second_(second) {
		}

		bool operator() [[gnu::no_sanitize("undefined")]] (std::size_t i) noexcept(true) {
			return (second_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end + unrolled_16_op_t::end)[i] == (first_ + unrolled_256_op_t::end + unrolled_128_op_t::end + unrolled_64_op_t::end + unrolled_32_op_t::end + unrolled_16_op_t::end)[i];
		}

	private:
		std::byte const (&first_)[FirstSz];
		std::byte const (&second_)[SecondSz];
	};
	const bool res8= res16 && unrolled_8_op_t::result_b(assign8_t(first, second));
	DEBUG_ASSERT(res8 == (std::memcmp(first, second, FirstSz) == 0));
	return res8;
}

template<
	std::size_t FirstSz,
	std::size_t SecondSz>
inline constexpr bool
memcmp_opt(char const (&first)[FirstSz], char const (&second)[SecondSz]) noexcept(true) {
	return memcmp_opt(reinterpret_cast<std::byte const(&)[FirstSz]>(first), reinterpret_cast<std::byte const(&)[SecondSz]>(second));
}

template<
	std::size_t FirstSz,
	std::size_t SecondSz>
inline constexpr bool
memcmp_opt(std::array<char, FirstSz> const& first, std::array<char, SecondSz> const& second) noexcept(true) {
	return memcmp_opt(reinterpret_cast<std::byte const(&)[FirstSz]>(*first.data()), reinterpret_cast<std::byte const(&)[SecondSz]>(*second.data()));
}

template<
	char const needle,
	std::size_t FirstSz>
	requires(FirstSz > 0)
inline constexpr std::byte const*
strchr_opt(std::byte const (&haystack)[FirstSz]) noexcept(true) {
	std::byte const* const found= private_::strchr_opt<
		needle,
		FirstSz,
		typename private_::select_size<FirstSz>::type>::result(haystack);
	DEBUG_ASSERT(reinterpret_cast<char const*>(found) == std::strchr(reinterpret_cast<char const*>(haystack), needle));
	return found;
}

template<
	char const needle,
	std::size_t FirstSz>
inline constexpr char const*
strchr_opt(char const (&haystack)[FirstSz]) noexcept(true) {
	return reinterpret_cast<char const*>(strchr_opt<needle>(reinterpret_cast<std::byte const(&)[FirstSz]>(haystack)));
}

template<
	std::size_t FirstSz,
	std::size_t SecondSz>
	requires(FirstSz > 0) && ((FirstSz % 32) == 0) && (SecondSz > 1) && (SecondSz <= 32)
inline constexpr std::byte const* strstr_opt(std::byte const (&haystack)[FirstSz], std::byte const (&needle)[SecondSz]) noexcept(true) {
	return private_::strstr_opt<
		FirstSz,
		SecondSz,
		typename private_::select_size<FirstSz>::type>::result(haystack, needle);
}
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
inline constexpr char const*
strstr_opt(char const (&haystack)[FirstSz], char const (&needle)[SecondSz]) noexcept(true) {
	return reinterpret_cast<char const*>(strstr_opt(reinterpret_cast<std::byte const(&)[FirstSz]>(haystack), reinterpret_cast<std::byte const(&)[SecondSz]>(needle)));
}
template<
	std::size_t FirstSz,
	std::size_t SecondSz>
inline constexpr char const*
strstr_opt(std::array<char, FirstSz> const& haystack, std::array<char, SecondSz> const& needle) noexcept(true) {
	return reinterpret_cast<char const*>(strstr_opt(reinterpret_cast<std::byte const(&)[FirstSz]>(*haystack.data()), reinterpret_cast<std::byte const(&)[SecondSz]>(*needle.data())));
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
inline constexpr void
memcpy_opt(std::array<char, SrcSz> const& src, std::array<char, DestSz>& dest) noexcept(true) {
	memcpy_opt(reinterpret_cast<std::byte const(&)[SrcSz]>(*src.data()), reinterpret_cast<std::byte(&)[DestSz]>(*dest.data()));
}
template<
	std::size_t SrcSz,
	std::size_t DestSz>
inline constexpr void
memcpy_opt(std::array<std::byte, SrcSz> const& src, std::array<std::byte, DestSz>& dest) noexcept(true) {
	memcpy_opt(reinterpret_cast<std::byte const(&)[SrcSz]>(*src.data()), reinterpret_cast<std::byte(&)[DestSz]>(*dest.data()));
}

template<
	std::size_t Sz>
inline bool
memcmp(std::array<char, Sz> const& src1, std::array<char, Sz> const& src2) noexcept(true) {
	return std::memcmp(src1.data(), src2.data(), Sz) == 0;
}
template<
	std::size_t Sz>
inline bool
memcmp(std::array<std::byte, Sz> const& src1, std::array<std::byte, Sz> const& src2) noexcept(true) {
	return std::memcmp(src1.data(), src2.data(), Sz) == 0;
}

template<
	std::size_t Sz>
inline bool
operator==(std::array<char, Sz> const& src1, std::array<char, Sz> const& src2) noexcept(true) {
	return memcmp(src1, src2);
}
template<
	std::size_t Sz>
inline bool
operator==(std::array<std::byte, Sz> const& src1, std::array<std::byte, Sz> const& src2) noexcept(true) {
	return memcmp(src1, src2);
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr void
memcpy_slow(char const (&src)[SrcSz], char (&dest)[DestSz]) noexcept(true) {
	using unrolled_op_t= private_::unroll<min<std::size_t, SrcSz, DestSz>::value>;
	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
	unrolled_op_t::result([&src, &dest](std::size_t i) {
		dest[i]= src[i];
	});
}

template<
	class T>
inline T
copy(std::string_view src) noexcept(true) {
	T dest;
	DEBUG_ASSERT(!src.empty());
	const std::ptrdiff_t dest_size= std::min(src.size(), dest.size());
	memcpy(dest.begin(), src.begin(), dest_size);
	std::fill_n(dest.begin() + dest_size, dest.size() - dest_size, '\0');
	return dest;
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr std::array<char, DestSz>
copy(std::array<char, SrcSz> const& src) noexcept(true) {
	std::array<char, DestSz> dest;
	using unrolled_op_t= private_::unroll<min<std::size_t, SrcSz, DestSz>::value>;
	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
	unrolled_op_t::result([&src, &dest](std::size_t i) {
		dest[i]= src[i];
	});
	return dest;
}

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr std::array<std::byte, DestSz>
copy(std::array<std::byte, SrcSz> const& src) noexcept(true) {
	std::array<std::byte, DestSz> dest;
	using unrolled_op_t= private_::unroll<min<std::size_t, SrcSz, DestSz>::value>;
	// Unroll the copy in the hope that the compiler will notice the sequence of copies and optimize it.
	unrolled_op_t::result([&src, &dest](std::size_t i) {
		dest[i]= src[i];
	});
	return dest;
}

}
}
