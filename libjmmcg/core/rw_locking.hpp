#ifndef LIBJMMCG_CORE_RW_LOCKING_HPP
#define LIBJMMCG_CORE_RW_LOCKING_HPP

/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "atomic_counter.hpp"
#include "locking.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock { namespace rw {

template<class T>
class locker;

template<class L>
class decaying_write_impl {
public:
	typedef L atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

private:
	atomic_t& rw_lk;

public:
	[[nodiscard]] explicit constexpr __stdcall decaying_write_impl(atomic_t& l) noexcept(true);

	/**
		Note that this is the write-lock interface.
	*/
	atomic_state_type __fastcall lock() noexcept(noexcept(rw_lk.lock())) {
		return rw_lk.lock();
	}
	/**
		Note that this is the write-lock interface. Also note that the timeout is ignored, the lock waits indefinitely.
	*/
	atomic_state_type __fastcall lock(const timeout_type) noexcept(false);
	/**
		Note that this is the write-lock interface.
	*/
	atomic_state_type __fastcall unlock() noexcept(true);
	/// Atomically convert the writer lock to a read lock.
	void decay() noexcept(true);
};
/// A class that allows a write lock to optionally, atomically decay to a read lock.
/**
	Only taken if there are no other readers nor any writers. If multiple writers attempt to gain the lock, only one will succeed, the choice is undefined. If at least one read lock is held, then any write locks will block until there are no read locks.

	\see rw
	\see read
	\see write
*/
template<class T>
class decaying_write_impl<locker<T> > {
public:
	typedef locker<T> atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

	[[nodiscard]] explicit constexpr __stdcall decaying_write_impl(atomic_t& l) noexcept(true);
	[[nodiscard]] constexpr decaying_write_impl(decaying_write_impl<locker<T> > const& dw) noexcept(true);

	/**
		Note that this is the write-lock interface.
	*/
	atomic_state_type __fastcall lock() noexcept(false);
	/**
		Note that this is the write-lock interface. Also note that the timeout is ignored, the lock waits indefinitely.
	*/
	atomic_state_type __fastcall lock(const timeout_type) noexcept(false);
	/**
		Note that this is the write-lock interface.
	*/
	atomic_state_type __fastcall unlock() noexcept(true);
	/// Atomically convert the writer lock to a read lock.
	void decay() noexcept(true);

private:
	atomic_t& rw_lk;
	typename atomic_t::writer_atomic_t& exclusive_w_lk;
	std::atomic_flag decayed{};
};

/// A readers-writer lock to control a resource.
/**
	The classic lock that allows many readers, but only one writer to occupy the lock at any one time.
	The order of writers vs. readers is implementation defined.
	This allows CREW implementations.

	\see read
	\see write
	\see decaying_write
*/
template<
	class T	 ///< The lock traits.
	>
class locker final : public lockable<T> {
public:
	typedef typename lockable<T>::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	/// Take a shared read lock on the resource, using RAII.
	/**
		If a writer has acquired the resource, then this will block until no writers have the resource. If multiple writers and readers are waiting for the resource, then the order in which readers and writers acquire the resource is undefined.

		\see rw
		\see write
	*/
	class read_lock_type final {
	public:
		typedef locker atomic_t;
		typedef typename atomic_t::lock_traits lock_traits;
		typedef typename lock_traits::timeout_type timeout_type;

		/**
			To assist in allowing compile-time computation of the algorithmic order of the threading model.
		*/
		static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

		/**
			Note that the time-out is ignored, the lock waits indefinitely.
		*/
		[[nodiscard]] __stdcall read_lock_type(atomic_t& l, const timeout_type) noexcept(false);
		read_lock_type(read_lock_type const&)= delete;
		__stdcall ~read_lock_type() noexcept(true);

	private:
		atomic_t& lk;
	};

	/// Take an exclusive write-lock on the resource, using RAII.
	/**
		Only taken if there are no other readers nor any writers. If multiple writers attempt to gain the lock, only one will succeed, the choice is undefined. If at least one read lock is held, then any write locks will block until there are no read locks.

		\see rw
		\see read_lock_type
		\see decaying_write
	*/
	class write_lock_type final {
	public:
		typedef locker atomic_t;
		typedef typename atomic_t::lock_traits lock_traits;
		typedef typename lock_traits::timeout_type timeout_type;

		/**
			To assist in allowing compile-time computation of the algorithmic order of the threading model.
		*/
		static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

		/**
			Note that the timeout is ignored, the lock waits indefinitely.
		*/
		[[nodiscard]] __stdcall write_lock_type(atomic_t& l, const timeout_type) noexcept(false);
		write_lock_type(write_lock_type const&)= delete;
		__stdcall ~write_lock_type() noexcept(true);

		/**
			Does nothing, just to make the interface the same.
		*/
		constexpr void decay() const noexcept(true) {}

	private:
		const typename atomic_t::writer_lk_t exclusive_w_lk;
		atomic_t& rw_lk;
	};

	/// A class that allows a write lock to optionally, atomically decay to a read lock, using RAII.
	/**
		\see decaying_write_impl
	*/
	class decaying_write_lock_type final : protected decaying_write_impl<locker<T> > {
	public:
		typedef decaying_write_impl<locker<T> > base_t;
		typedef typename base_t::atomic_t atomic_t;
		typedef typename atomic_t::lock_traits lock_traits;
		typedef typename lock_traits::timeout_type timeout_type;
		using base_t::memory_access_mode;

		/**
			Note that the timeout is ignored, the lock waits indefinitely.
		*/
		[[nodiscard]] __stdcall decaying_write_lock_type(atomic_t& l, const timeout_type) noexcept(false);
		__stdcall ~decaying_write_lock_type() noexcept(true);

		void operator=(decaying_write_lock_type const&)= delete;
		void operator=(decaying_write_lock_type&&)= delete;

		/// Atomically convert the writer lock to a read lock.
		void decay() noexcept(true);
	};

	[[nodiscard]] __stdcall locker() noexcept(false);

	locker(locker const&)= delete;
	locker(locker&&)= delete;
	void operator=(locker const&)= delete;
	void operator=(locker&&)= delete;

	/**
		Note that this is the read-lock interface.
	*/
	atomic_state_type __fastcall lock() noexcept(false) override;
	/**
		Note that this is the read-lock interface.
	*/
	atomic_state_type __fastcall lock(const timeout_type) noexcept(false) override {
		return this->lock();
	}
	/**
		Note that this is the read-lock interface.
	*/
	atomic_state_type __fastcall unlock() noexcept(true) override;

	///\todo G++ issues:		private:
	typedef typename lock_traits::critical_section_type writer_atomic_t;
	typedef typename writer_atomic_t::write_unlockable_type writer_lk_t;
	typedef typename lock_traits::anon_event_type readers_free_t;
	typedef typename lock_traits::template atomic_counter_type<long> atomic_counter_type;
	friend class write;
	friend class decaying_write;

	atomic_counter_type readers;
	readers_free_t readers_free;
	writer_atomic_t writer_lk;

private:
	atomic_state_type __fastcall try_lock() noexcept(true) override {
		return lock_traits::atom_unset;
	}
};

}}}}}

#include "rw_locking_impl.hpp"

#endif
