/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class KeyType, class MappedType>
template<class FwdIter>
inline constexpr
flat_bijective_map<KeyType, MappedType>::flat_bijective_map(FwdIter b, FwdIter e) noexcept(false) {
	key_to_mapped_.insert(b, e);
	std::vector<std::pair<MappedType, KeyType>> tmp;
	DEBUG_ASSERT(std::distance(b, e)>=0);
	tmp.reserve(static_cast<std::vector<std::pair<MappedType, KeyType>>::size_type>(std::distance(b, e)));
	std::transform(b, e, std::back_inserter(tmp), [](auto const &v) {return std::make_pair(v.second, v.first);});
	mapped_to_key_.insert(tmp.begin(), tmp.end());
}

template<class KeyType, class MappedType>
inline constexpr bool
flat_bijective_map<KeyType, MappedType>::empty() const noexcept(true) {
	DEBUG_ASSERT(key_to_mapped_.empty()==mapped_to_key_.empty());
	return key_to_mapped_.empty();
}

template<class KeyType, class MappedType>
inline constexpr typename flat_bijective_map<KeyType, MappedType>::size_type
flat_bijective_map<KeyType, MappedType>::size() const noexcept(true) {
	DEBUG_ASSERT(key_to_mapped_.size()==mapped_to_key_.size());
	return key_to_mapped_.size();
}

template<class KeyType, class MappedType>
inline void constexpr
flat_bijective_map<KeyType, MappedType>::clear() noexcept(true) {
	key_to_mapped_.clear();
	mapped_to_key_.clear();
}

template<class KeyType, class MappedType>
inline MappedType const &
flat_bijective_map<KeyType, MappedType>::at(KeyType const &k) const noexcept(false) {
	return key_to_mapped_.at(k);
}

template<class KeyType, class MappedType>
inline KeyType const &
flat_bijective_map<KeyType, MappedType>::at(MappedType const &k) const noexcept(false) {
	return mapped_to_key_.at(k);
}

template<class KeyType, class MappedType>
template<class Param1, class... Params>
inline void
flat_bijective_map<KeyType, MappedType>::insert(Param1 const &arg1, Params const &...args) noexcept(false) {
	using bijection_value_type=typename flat_map<MappedType, KeyType>::value_type;
	key_to_mapped_.push_back(arg1, args...);
	mapped_to_key_.push_back(bijection_value_type{arg1.second, arg1.first}, bijection_value_type{args.second, args.first}...);
	DEBUG_ASSERT(key_to_mapped_.empty()==mapped_to_key_.empty());
	DEBUG_ASSERT(key_to_mapped_.size()==mapped_to_key_.size());
}

template<class KeyType, class MappedType>
template<class Param1, class... Params>
inline void
flat_bijective_map<KeyType, MappedType>::insert(Param1 &&arg1, Params &&...args) noexcept(false) {
	using bijection_value_type=typename flat_map<MappedType, KeyType>::value_type;
	mapped_to_key_.push_back(bijection_value_type{arg1.second, arg1.first}, bijection_value_type{args.second, args.first}...);
	key_to_mapped_.push_back(std::forward<Param1>(arg1), std::forward<Params>(args)...);
	DEBUG_ASSERT(key_to_mapped_.empty()==mapped_to_key_.empty());
	DEBUG_ASSERT(key_to_mapped_.size()==mapped_to_key_.size());
}

template<class KeyType, class MappedType>
inline std::ostream &
operator<<(std::ostream &os, flat_bijective_map<KeyType, MappedType> const &v) noexcept(false) {
	os<<"size="<<v.size()
		<<"key-to-mapped: ["<<v.key_to_mapped_
		<<"], mapped-to-key: ["<<v.mapped_to_key_<<"]";
	return os;
}

} }
