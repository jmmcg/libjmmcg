#ifndef LIBJMMCG_CORE_LINE_ITERATOR_HPP
#define LIBJMMCG_CORE_LINE_ITERATOR_HPP

/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <istream>
#include <iterator>
#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// An iterator for reading lines from an std::istream.
/**
 * Taken from "Generic Programming and the STL" by M.H.Austern.
 * 
 * \see std::getline()
 */
class line_iterator {
public:
	using iterator_category=std::input_iterator_tag;
	using value_type=std::string;
	using difference_type=ptrdiff_t;
	using pointer=std::string const *;
	using reference=std::string const&;
	static inline constexpr const std::string::value_type default_delimiter='\n';
	
	constexpr line_iterator()=default;
	[[nodiscard]] explicit line_iterator(std::istream &s, std::string::value_type const delimiter=default_delimiter) noexcept(false);

	bool operator==(line_iterator const& i) const noexcept(true);
	bool operator!=(line_iterator const& i) const noexcept(true);

	reference operator*() const noexcept(true);
	pointer operator->() const noexcept(true);
	
	line_iterator operator++() noexcept(false);
	[[nodiscard]] line_iterator operator++(int) noexcept(false);

private:
	std::istream*in_{nullptr};
	std::string line_{};
	std::string::value_type const delimiter_{default_delimiter};
	bool valid_{false};
	
	void read();
};

} }

#include "line_iterator_impl.hpp"

#endif
