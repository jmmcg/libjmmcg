#ifndef LIBJMMCG_CORE_IN_RANGE_HPP
#define LIBJMMCG_CORE_IN_RANGE_HPP

/******************************************************************************
** Copyright © 2025 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <libassert/assert.hpp>

#include <functional>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

[[nodiscard]] inline constexpr bool
in_range(double min, double max, double test) noexcept(true) {
	DEBUG_ASSERT(min <= max);
	return std::greater_equal()(test, min)
			 && std::less_equal()(test, max);
}

}}

#endif
