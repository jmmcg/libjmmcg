/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<std::size_t Size>
inline fifo_flyback_buffer<Size>::fifo_flyback_buffer() noexcept(true)
	: cont_{}, head_{cont_.begin()}, tail_{cont_.begin()} {
	cont_.fill(initialisation_value);
	DEBUG_ASSERT(empty());
}

template<std::size_t Size>
inline constexpr bool
fifo_flyback_buffer<Size>::empty() const noexcept(true) {
	return size() <= 0U;
}

template<std::size_t Size>
inline constexpr typename fifo_flyback_buffer<Size>::size_type
fifo_flyback_buffer<Size>::size() const noexcept(true) {
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	auto const size= static_cast<size_type>(tail_ - head_);
	DEBUG_ASSERT(size <= cont_.size());
	return size;
}

template<std::size_t Size>
inline constexpr typename fifo_flyback_buffer<Size>::size_type
fifo_flyback_buffer<Size>::capacity() const noexcept(true) {
	return cont_.size();
}

template<std::size_t Size>
inline constexpr typename fifo_flyback_buffer<Size>::size_type
fifo_flyback_buffer<Size>::max_capacity() const noexcept(true) {
	return max_capacity_;
}

template<std::size_t Size>
inline void constexpr fifo_flyback_buffer<Size>::clear() noexcept(true) {
	head_= tail_= cont_.begin();
	DEBUG_ASSERT(empty());
#ifndef NDEBUG
	std::fill(head_, cont_.end(), initialisation_value);
#endif
}

template<std::size_t Size>
template<class ConsumeOp>
inline typename fifo_flyback_buffer<Size>::size_type
fifo_flyback_buffer<Size>::pop_front(ConsumeOp&& consume_op) noexcept(false) {
	if(!empty()) [[likely]] {
		size_type bytes_claimed_consumed{};
		try {
			auto const head= head_;
			auto const tail= tail_;
			bytes_claimed_consumed= consume_op(head_, tail_);
			if(head != head_ || tail != tail_) [[unlikely]] {
				if(empty()) [[likely]] {
					return size();
				} else {
					BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Consumer claimed to consume more bytes, {}, than were available {} as the underlying buffer was naughtily changed.", bytes_claimed_consumed, size()), &fifo_flyback_buffer::pop_front<ConsumeOp>));
				}
			}
		} catch(...) {
			clear();
			throw;
		}
		if(bytes_claimed_consumed <= size()) [[likely]] {
			head_+= bytes_claimed_consumed;
			DEBUG_ASSERT(cont_.begin() <= head_);
			DEBUG_ASSERT(head_ <= tail_);
			DEBUG_ASSERT(tail_ <= cont_.end());
			if(head_ == cont_.end()) {
				DEBUG_ASSERT(tail_ == cont_.end());
				clear();
			}
			return bytes_claimed_consumed;
		} else {
			// This might throw because in the call-chain in consume_op(), there may be a call to fifo_flyback_buffer::clear()...
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Consumer claimed to consume more bytes, {}, than were available {}.", bytes_claimed_consumed, size()), &fifo_flyback_buffer<Size>::pop_front<ConsumeOp>));
		}
	} else {
		return size_type{};
	}
}

template<std::size_t Size>
template<class Generator>
inline typename fifo_flyback_buffer<Size>::size_type
fifo_flyback_buffer<Size>::generate_n(Generator&& generator) noexcept(false) {
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	if(tail_ == cont_.end()) [[unlikely]] {
		// We need to move the data to the beginning. If this assert fails, it is possible that the size for cont_ is too small, as pop_front() could not consume the incomplete message filling cont_.
		DEBUG_ASSERT(cont_.begin() < head_);
		auto const size_of_unparsed_block_to_move= tail_ - head_;
		std::memmove(cont_.data(), &*head_, static_cast<size_type>(size_of_unparsed_block_to_move));
		head_= cont_.begin();
		tail_= head_ + size_of_unparsed_block_to_move;
		DEBUG_ASSERT(!empty());
		DEBUG_ASSERT(tail_ < cont_.end());
	} else if(head_ == cont_.end() && tail_ == cont_.end()) [[unlikely]] {
		clear();
		DEBUG_ASSERT(empty());
	}
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ < cont_.end());
	std::size_t bytes_read= generator(tail_, cont_.end());
	DEBUG_ASSERT(bytes_read <= cont_.size());
	tail_+= bytes_read;
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	return bytes_read;
}

template<std::size_t Size>
inline std::string_view
fifo_flyback_buffer<Size>::container() const noexcept(true) {
	DEBUG_ASSERT(head_ <= tail_);
	return {head_, tail_};
}

}}
