#ifndef LIBJMMCG_CORE_single_producer_single_consumer_queue_hpp
#define LIBJMMCG_CORE_single_producer_single_consumer_queue_hpp

/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "jthread.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <array>
#include <atomic>
#include <exception>
#include <functional>
#include <queue>
#include <type_traits>
#include <utility>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

template<
	class Invokable,	 ///< Something invokable, for example: "std::function<void()>".
	generic_traits::api_type::element_type API,
	typename Mdl>
	requires(std::is_invocable_v<Invokable>)
class single_producer_single_consumer_queue {
public:
	using container_type= std::queue<Invokable>;
	using element_type= typename container_type::value_type;

	template<class ErrHandler>
		requires(noexcept(ErrHandler()))
	[[nodiscard]] explicit single_producer_single_consumer_queue(ErrHandler&& err_handler);
	~single_producer_single_consumer_queue();

	template<class Op>
		requires(std::is_invocable_v<Op>)
	void emplace(Op&& op) noexcept(true);

private:
	using queues_t= std::array<container_type, 2>;
	using thread_t= ppd::jthread<API, Mdl>;
	using queue_ptr_t= typename thread_t::lock_traits::template atomic<typename queues_t::iterator>;

	std::function<void(std::exception_ptr const& e)> const err_handler_;
	queues_t queues_{};
	queue_ptr_t queue_to_fill_{queues_.begin()};
	std::atomic_flag has_work_{false};
	thread_t consumer_{"spsc" LIBJMMCG_ENQUOTE(__LINE__), [this](std::stop_token request_exit) {
								 process(std::move(request_exit));
							 }};

	void process(std::stop_token request_exit) noexcept(true);
};

}}}

#include "single_producer_single_consumer_queue_impl.hpp"

#endif
