#ifndef LIBJMMCG_CORE_SOCKET_WRAPPER_GLIBC_HPP
#define LIBJMMCG_CORE_SOCKET_WRAPPER_GLIBC_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_wrapper.hpp"
#include "syscall_wrapper.hpp"
#include "thread_wrapper.hpp"
#include "yet_another_enum_wrapper.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>
#include <string_view>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/types.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace glibc {

namespace basic {

/// A simple TCP/IP socket wrapper.
template<class LkT>
class wrapper {
public:
	using thread_traits= ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>;
	/// The lock-type to use to ensure that the underlying ::write()s occur atomically.
	/**
		The calls to the underlying socket ::write() may occur multiple times when under extreme load. This lock is used to ensure that the calls to write() occur atomically with respect to multiple threads.

		\see ::write()
	*/
	using write_lock_t= LkT;
	using atomic_t= typename write_lock_t::atomic_t;

	LIBJMMCG_MAKE_ENUM_TAG_VALUES(
		domain_t,
		int,
		(unknown, AF_UNSPEC),
		(ip_v4, AF_INET));

	LIBJMMCG_MAKE_ENUM_TAG_VALUES(
		type_t,
		int,
		(stream, SOCK_STREAM),
		(nonblocking, SOCK_NONBLOCK));

	using socket_priority= socket::socket_priority;

	using socket_type= int;

	domain_t::element_type const domain_;

	/// Wrap a TCP socket using the TCP/IP protocol.
	explicit wrapper(socket_type opened_skt) noexcept(false);

	/// Wrap a TCP socket using the TCP/IP protocol.
	explicit wrapper(type_t::element_type type= type_t::stream, domain_t::element_type domain= domain_t::ip_v4, int protocol= 0) noexcept(false);

	virtual ~wrapper() noexcept(true);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<
		class MsgT	 ///< The type of the message to write.
		>
	void write(MsgT const& message) noexcept(false);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<class V, std::size_t N>
	void write(std::array<V, N> const& message) noexcept(false);

	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this buffer, which may be grown to accommodate the message.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<
		class MsgT	 ///< The type of the message to read, that must be as-if a POD.
		>
	[[nodiscard]] bool
	read(MsgT& dest) noexcept(false);
	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this stack-based buffer, which must be sufficiently large to accommodate the message read, otherwise UB will result.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class V, std::size_t SrcSz>
	[[nodiscard]] bool
		read(V (&dest)[SrcSz]) noexcept(false);

	/**
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class MsgDetails, class V, std::size_t N>
	[[nodiscard]] bool
	read(std::array<V, N>& buff) noexcept(false);

	virtual void close() noexcept(false);

	[[nodiscard]] bool is_open() const noexcept(true);

	/// Set various options on the created socket to ensure more optimal behaviour.
	/**
	 * \todo Consider setting MSG_ZEROCOPY.
	 * \param timeout	Linger and socket timeout.
	 */
	virtual void set_options(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false);

	[[nodiscard]] virtual std::string to_string() const noexcept(false);

	socket_type native_handle();

	[[nodiscard]] std::string local_ip() const noexcept(false);

protected:
	mutable atomic_t mutex_;
	socket_type socket_;

	/**
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	[[nodiscard]] bool read_msg(std::byte* dest, std::size_t msg_size) noexcept(false);

private:
	/**
	 * <a href="https://stackoverflow.com/questions/8829238/how-can-i-trap-a-signal-sigpipe-for-a-socket-that-closes#8829311"/>
	 */
	void ignore_sigpipe_for_a_socket_that_closes() noexcept(false);
};

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, wrapper<LkT> const& ec) noexcept(false);

}

namespace client {

/// A simple client wrapper.
template<class LkT>
class wrapper : public basic::wrapper<LkT> {
public:
	using base_t= basic::wrapper<LkT>;
	using domain_t= typename base_t::domain_t;
	using type_t= typename base_t::type_t;
	using socket_type= typename base_t::socket_type;
	using base_t::base_t;

	virtual void connect(char const* addr, std::uint16_t port) noexcept(false);
	virtual void connect(struct sockaddr* addr, socklen_t port) noexcept(false);
};

}

namespace server {

/// A simple server wrapper.
template<class LkT>
class wrapper : public basic::wrapper<LkT> {
public:
	using base_t= basic::wrapper<LkT>;
	using domain_t= typename base_t::domain_t;
	using socket_t= base_t;
	using type_t= typename base_t::type_t;
	using socket_type= typename base_t::socket_type;
	using thread_traits= typename base_t::thread_traits;
	using base_t::set_options;

	enum : int {
		max_backlog_connections= 1
	};

	/// Wrap a TCP socket using the TCP/IP protocol.
	wrapper(char const* addr, std::uint16_t port, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, type_t::element_type type= type_t::stream, domain_t::element_type domain= domain_t::ip_v4) noexcept(false);
	~wrapper() noexcept(true) override;

	void run() noexcept(false);
	void stop() noexcept(false);
	[[nodiscard]] bool stopped() const noexcept(true);

	template<class RecvProcMsgs>
	void
	async_accept(RecvProcMsgs&& f) noexcept(false);

	void set_options(base_t& skt) const noexcept(false);

private:
	class recv_msg_ops_t;

	std::atomic_flag exit_{};
	std::atomic_flag exited_{};

	std::shared_ptr<recv_msg_ops_t> recv_msg_ops{};
};

}

}}}}

#include "socket_wrapper_glibc_impl.hpp"

#endif
