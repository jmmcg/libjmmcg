#ifndef LIBJMMCG_CORE_SOCKET_WRAPPER_ASIO_HPP
#define LIBJMMCG_CORE_SOCKET_WRAPPER_ASIO_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_wrapper.hpp"

#include <boost/asio.hpp>

#include <chrono>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace asio {

/// A simple TCP/IP socket wrapper using boost::asio.
/**
	Developed from:
	<a href="https://www.boost.org/doc/libs/1_71_0/doc/html/boost_asio/example/cpp11/echo/blocking_tcp_echo_client.cpp"/>
*/
template<class LkT>
class socket_wrapper final {
public:
	using socket_t= boost::asio::ip::tcp::socket;
	using socket_priority= socket::socket_priority;
	/// The lock-type to use to ensure that the underlying ::write()s occur atomically.
	/**
		The calls to the underlying socket ::write() may occur multiple times when under extreme load. This lock is used to ensure that the calls to write() occur atomically with respect to multiple threads.

		\see ::write()
	*/
	using write_lock_t= LkT;
	using atomic_t= typename write_lock_t::atomic_t;

	/// Wrap a TCP socket using the TCP/IP protocol.
	/**
		\param	io_context	The I/O context that underlies the socket.
	*/
	explicit socket_wrapper(boost::asio::io_context& io_context);
	/// Wrap a TCP socket using the TCP/IP protocol.
	/**
		\param	socket	The socket to be taken control of.
	*/
	explicit socket_wrapper(socket_t&& socket);

	void connect(boost::asio::ip::tcp::endpoint const& endpoint);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<
		class MsgT	 ///< The type of the message to write.
		>
	void write(MsgT const& message) noexcept(false);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<class V, std::size_t N>
	void write(std::array<V, N> const& message) noexcept(false);

	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this buffer, which may be grown to accommodate the message.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<
		class MsgT	 ///< The type of the message to read, that must be as-if a POD.
		>
	[[nodiscard]] bool
	read(MsgT& dest) noexcept(false);
	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this stack-based buffer, which must be sufficiently large to accommodate the message read, otherwise UB will result.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class V, std::size_t SrcSz>
	[[nodiscard]] bool
		read(V (&dest)[SrcSz]) noexcept(false);

	template<class MsgDetails, class V, std::size_t N>
	[[nodiscard]] bool
	read(std::array<V, N>& buff) noexcept(false);

	bool is_open() const;

	void close();

	void set_options(std::size_t, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu);

	std::string to_string() const noexcept(false);

	[[nodiscard]] std::string local_ip() const noexcept(false);

protected:
	mutable atomic_t mutex_;

private:
	socket_t socket_;
};

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, socket_wrapper<LkT> const& ec) noexcept(false);

}}}}

#include "socket_wrapper_asio_impl.hpp"

#endif
