#ifndef LIBJMMCG_CORE_LFSR_COUNTER_HPP
#define LIBJMMCG_CORE_LFSR_COUNTER_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "int128_compatibility.hpp"
#include "random_interval.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <bitset>
#include <chrono>
#include <cstdint>
#include <numeric>
#include <random>
#include <type_traits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace lfsr {

/**
 * From:
 * -# <a href="https://en.wikipedia.org/wiki/Linear-feedback_shift_register"/>
 * -# <a href="https://docs.xilinx.com/v/u/en-US/xapp052"/>
 */
template<std::size_t N, std::size_t Tap0, std::size_t Tap1, std::size_t Tap2>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U) && Tap0 <= N && Tap1 <= N && Tap2 <= N)
struct fibonacci {
	using element_type= std::conditional_t<N != 128U, std::bitset<N>, libjmmcg::uint128_t>;

	enum : std::size_t {
		size= N	 ///< In bits.
	};

	[[nodiscard]] static consteval libjmmcg::uint128_t max() noexcept(true);

	/**
	 * See <a href="https://en.wikipedia.org/wiki/Linear-feedback_shift_register"/> regarding the implementation detials.
	 *
	 * @param v The value to compute the lfsr for.
	 * @return The computed value. Depending upon the specific values of the Taps, this may not be maximal.
	 */
	[[nodiscard]] static constexpr element_type next(element_type const v) noexcept(true);
};

/**
 * From:
 * -# <a href="https://en.wikipedia.org/wiki/Linear-feedback_shift_register"/>
 * -# <a href="https://docs.xilinx.com/v/u/en-US/xapp052"/>
 */
template<std::size_t N>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U))
struct maximal {
	using element_type= std::conditional_t<N != 128U, std::bitset<N>, libjmmcg::uint128_t>;
	using period_type= libjmmcg::uint128_t;

	enum : std::size_t {
		size= N	 ///< In bits.
	};

	[[nodiscard]] static consteval libjmmcg::uint128_t max() noexcept(true);
	[[nodiscard]] static constexpr element_type next(element_type const start_state) noexcept(true);
	[[nodiscard]] static period_type period(element_type const start_state) noexcept(true);
};

/// Start the LFSR using a cryptographically secure, hardware device, if possible, to obfuscate the exact LFSR sequence used.
/**
 *
 * \see std::random_device
 */
template<std::size_t N>
class secure final {
public:
	using lfsr_t= maximal<N>;
	using element_type= typename lfsr_t::element_type;

	enum : std::size_t {
		size= lfsr_t::size	///< In bits.
	};

	/// Create an LFSR sequence generator.
	/**
	 * Note: This is not secure, if the seed is unwisely chosen, e.g. using the current time would be an example of such a poor choice.
	 *
	 * @param seed Start the LFSR from a value derived from the given seed.
	 */
	[[nodiscard]] explicit secure(unsigned int seed) noexcept(true);
	/// Create an LFSR sequence generator.
	/**
	 * This should be cryptographically-secure, if std::random_device::entropy() returns a value greater than zero.
	 *
	 * \see std::random_device, std::random_device::entropy()
	 */
	[[nodiscard]] secure() noexcept(false);

	[[nodiscard]] constexpr element_type next() noexcept(true);

private:
	element_type lfsr_current_value_;
};

}
}}

#include "lfsr_counter_impl.hpp"

#endif
