#ifndef LIBJMMCG_CORE_LOCK_MEM_HPP
#define LIBJMMCG_CORE_LOCK_MEM_HPP

/******************************************************************************
** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A class to lock and unlock a region of memory via RAII as a type of scoped-lock.
/**
	Note that the dtor may throw, as the region may not be able to be unlocked: this could be a serious issue as non-pageable memory is a system resource, which if not correctly released could crash a server. Thus it was considered that making the dtor noexcept(true) was unsuitable: this would either mean swallowing the error, i.e. ignoring it, or causing std::terminate to be called. Neither option was considered suitable for any serious program as it would be easier to debug and more robust in case of such failures if the exception were allowed to propagate.
*/
class lock_mem_range;

/// A class to lock and unlock all of the memory a process has mapped into RAM via RAII as a type of scoped-lock.
/**
	Note that the dtor may throw, as the region may not be able to be unlocked: this could be a serious issue as non-pageable memory is a system resource, which if not correctly released could crash a server. Thus it was considered that making the dtor noexcept(true) was unsuitable: this would either mean swallowing the error, i.e. ignoring it, or causing std::terminate to be called. Neither option was considered suitable for any serious program as it would be easier to debug and more robust in case of such failures if the exception were allowed to propagate.
*/
class lock_all_proc_mem;

} }

#ifdef WIN32
#elif defined(__unix__)
#	include "../unix/lock_mem.hpp"
#endif

#endif
