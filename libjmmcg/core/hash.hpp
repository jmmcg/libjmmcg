/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "blatant_old_msvc_compiler_hacks.hpp"

#include <boost/cstdint.hpp>
#include <boost/static_assert.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <functional>
#include <iterator>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Various hashing functions.
/**
	\see boost::hash, std::hash
*/
namespace hashers {

/// An implementation of the MurmurHash2 hash function.
/**
	From <a href="https://simonhf.wordpress.com/2010/09/25/murmurhash160//"/>.
*/
template<
	typename A	 ///< The STL-container type of the item to be hashed.
	>
struct murmur2 {
private:
	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.
	static inline constexpr const int m= 0x5bd1e995;
	static inline constexpr const int r= 24;

public:
	using result_type= std::size_t;
	using argument_type= A;
	static_assert(sizeof(result_type) <= sizeof(std::size_t));

	/// Generate the hash of the value.
	/**
		\param	d	An STL-container type holding the value to be hashed, that must be aligned.
		\param	seed	The seed value for hashing.
		\return	The hash of the value.
	*/
	[[nodiscard]] std::size_t __fastcall operator()(const argument_type& d, const result_type seed= 0) const noexcept(true) {
		BOOST_STATIC_ASSERT(sizeof(int) == 4);
		typename argument_type::size_type len= d.size() * sizeof(typename argument_type::value_type);
		// Initialize the hash to a 'random' value.
		result_type h= seed ^ len;
		// Mix 4 bytes at a time into the hash.
		unsigned char const* data= reinterpret_cast<unsigned char const*>(&*d.begin());
		for(; len >= 4; len-= 4) {
			unsigned int k= *reinterpret_cast<unsigned int const*>(data);
			k*= m;
			k^= k >> r;
			k*= m;

			h*= m;
			h^= k;

			data+= 4;
		}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
		// Handle the last few bytes of the input array.
		switch(len) {
		case 3:
			h^= data[2] << 16;
		case 2:
			h^= data[1] << 8;
		case 1:
			h^= data[0];
			h*= m;
		default:
			break;
		};
#pragma GCC diagnostic pop

		// Do a few final mixes of the hash to ensure the last few bytes are well-incorporated.
		h^= h >> 13;
		h*= m;
		h^= h >> 15;
		return h;
	}
};

/// An implementation of the Hsieh hash function.
/**
	From <a href="http://www.azillionmonkeys.com/qed/hash.html"/>.

	\see mpl::hsieh
*/
template<
	typename A	 ///< The STL-container type of the item to be hashed.
	>
struct hsieh {
	using result_type= std::size_t;
	using argument_type= A;
	static_assert(sizeof(result_type) <= sizeof(std::size_t));

private:
	using hashed_unit_t= std::int16_t;

	static constexpr hashed_unit_t __fastcall get16bits(typename argument_type::const_iterator d) noexcept(true) {
		return *reinterpret_cast<hashed_unit_t const*>(&*d);
	}

public:
	/// Generate the hash of the value.
	/**
		\param	d	An STL-container type holding the value to be hashed.
		\return	The hash of the value.
	*/
	[[nodiscard]] std::size_t __fastcall operator()(const argument_type& d) const noexcept(true) {
		if(!d.empty()) [[likely]] {
			typename argument_type::size_type len= d.size();
			const unsigned short rem= static_cast<unsigned short>(len & 3);
			unsigned long hash= static_cast<unsigned long>(len);
			typename argument_type::const_iterator data= d.begin();
			len>>= 2;

			/* Main loop */
			for(; len > 0; --len) {
				hash+= get16bits(data);
				typename argument_type::const_iterator it(std::next(data, sizeof(hashed_unit_t)));
				const unsigned long tmp= (get16bits(it) << 11) ^ hash;
				hash= (hash << 16) ^ tmp;
				std::advance(data, 2 * sizeof(hashed_unit_t));
				hash+= hash >> 11;
			}

			/* Handle end cases */
			switch(rem) {
			case 3:
				hash+= get16bits(data);
				hash^= hash << 16;
				hash^= data[sizeof(hashed_unit_t)] << 18;
				hash+= hash >> 11;
				break;
			case 2:
				hash+= get16bits(data);
				hash^= hash << 11;
				hash+= hash >> 17;
				break;
			case 1:
				hash+= *data;
				hash^= hash << 10;
				hash+= hash >> 1;
			}

			/* Force "avalanching" of final 127 bits */
			hash^= hash << 3;
			hash+= hash >> 5;
			hash^= hash << 4;
			hash+= hash >> 17;
			hash^= hash << 25;
			hash+= hash >> 6;

			return static_cast<result_type>(hash);
		} else [[unlikely]] {
			return 0;
		}
	}
};

namespace mpl {

namespace private_ {

using hashed_unit_t= std::int16_t;

template<typename A>
inline constexpr hashed_unit_t __fastcall get16bits(A& d) noexcept(true) {
	return *reinterpret_cast<hashed_unit_t const*>(&*d);
}

template<unsigned long len, class C>
struct core_hash;

template<class C>
struct core_hash<0, C> {
	static constexpr unsigned long result(typename C::const_iterator&, unsigned long hash) noexcept(true) {
		return hash;
	}
};

template<unsigned long len, class C>
struct core_hash {
	static unsigned long result(typename C::const_iterator& data, unsigned long hash) noexcept(true) {
		hash+= get16bits(data);
		typename C::const_iterator it(std::next(data, sizeof(hashed_unit_t)));
		const unsigned long tmp= (get16bits(it) << 11) ^ hash;
		hash= (hash << 16) ^ tmp;
		std::advance(data, 2 * sizeof(hashed_unit_t));
		return core_hash<len - 1, C>::result(data, hash + (hash >> 11));
	}
};

template<unsigned long rem, class C>
struct end_cases;

template<class C>
struct end_cases<3, C> {
	static constexpr unsigned long result(const typename C::const_iterator data, unsigned long hash) noexcept(true) {
		hash+= get16bits(data);
		hash^= hash << 16;
		hash^= data[sizeof(hashed_unit_t)] << 18;
		return hash + (hash >> 11);
	}
};

template<class C>
struct end_cases<2, C> {
	static constexpr unsigned long result(const typename C::const_iterator data, unsigned long hash) noexcept(true) {
		hash+= get16bits(data);
		hash^= hash << 11;
		return hash + (hash >> 17);
	}
};

template<class C>
struct end_cases<1, C> {
	static constexpr unsigned long result(const typename C::const_iterator data, unsigned long hash) noexcept(true) {
		hash+= *data;
		hash^= hash << 10;
		return hash + (hash >> 1);
	}
};

template<class C>
struct end_cases<0, C> {
	static constexpr unsigned long result(const typename C::const_iterator, unsigned long hash) noexcept(true) {
		return hash;
	}
};
}

/// An implementation of the Hsieh hash function.
/**
	From <a href="http://www.azillionmonkeys.com/qed/hash.html"/>.
	Using template meta-programming to unroll the hashing loop.

	\see hashing::hsieh
*/
template<
	typename A,	  ///< The STL-container type of the item to be hashed.
	unsigned long L	///< The length of the item to be hashed.
	>
struct hsieh {
	using result_type= std::size_t;
	using argument_type= A;
	using hashed_unit_t= private_::hashed_unit_t;
	static inline constexpr const unsigned long length= L;
	static_assert(sizeof(result_type) <= sizeof(std::size_t));

private:
	static inline constexpr const unsigned long rem= length & 3;

public:
	/// Generate the hash of the value.
	/**
		\param	d	An STL-container type holding the value to be hashed.
		\return	The hash of the value.
	*/
	[[nodiscard]] std::size_t __fastcall operator()(const argument_type& d) const noexcept(true) {
		if(!d.empty()) [[likely]] {
			DEBUG_ASSERT(d.size() >= length);
			typename argument_type::const_iterator data(d.begin());
			const unsigned long hash_int= private_::core_hash<(length >> 2), argument_type>::result(data, length);
			unsigned long hash= private_::end_cases<rem, argument_type>::result(data, hash_int);
			/* Force "avalanching" of final 127 bits */
			hash^= hash << 3;
			hash+= hash >> 5;
			hash^= hash << 4;
			hash+= hash >> 17;
			hash^= hash << 25;
			return static_cast<result_type>(hash + (hash >> 6));
		} else [[unlikely]] {
			return 0;
		}
	}
};

}

}
}}
