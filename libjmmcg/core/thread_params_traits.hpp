#ifndef LIBJMMCG_CORE_THREAD_PARAMS_TRAITS_HPP
#define LIBJMMCG_CORE_THREAD_PARAMS_TRAITS_HPP

/******************************************************************************
**	Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#ifdef _MSC_VER
#	pragma warning(disable:4786)	  // identifier was truncated to '255' characters in the debug information
#endif

#include "ttypes.hpp"
#include "yet_another_enum_wrapper.hpp"

#include <fmt/format.h>

#include <thread>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/**
	These parameters allow the users to choose a thread pool that has the
	properties they would like. Note that not all combinations make sense,
	so will give compilation errors, as they are not implemented, and of the
	rest, some of those may not be implemented. Contact the author if you need
	a specific specialisation!
*/
namespace generic_traits {
enum class creation_cost {
	sequential_creation,
	expensive_to_create,	  ///< E.g. x86 ISA & Win32
	cheap_to_create	///< E.g. IBM BlueGene/[C|P] (or Cyclops)
};
enum class destruction_cost {
	sequential_destruction,
	expensive_to_destroy,	///< E.g. x86 ISA & Win32
	cheap_to_destroy	 ///< E.g. IBM BlueGene/[C|P] (or Cyclops)
};
enum class synchronisation_cost {	///< The cost of the base read-modify-write operation used to implement a mutex.
	sequential_sync,
	expensive_to_synchronise,	 ///< E.g. x86 ISA & Win32
	cheap_to_synchronise	  ///< E.g. IBM BlueGene/[C|P] (or Cyclops)
};
LIBJMMCG_MAKE_ENUM(return_data, int,
	joinable,	///< The work has a return value.
	nonjoinable	  ///< The work does not have a return value.
);
LIBJMMCG_MAKE_ENUM(api_type, int,
	no_api,	 ///< For sequential operation. i.e. no threading. Usually one would use the constant platform_api, which is suitably declared as one of the threading models, below.
	MS_Win32,	///< Usually "heavy-weight" threads, e.g. x86, alpha.
	posix_pthreads,	///< Usually "heavy-weight" threads, e.g. x86, alpha, SPARC. On Solaris, Linux, etc.
	IBM_cyclops	  ///< IBM BlueGene/[C|P] (or Cyclops), ~10^6-~10^9 lightweight threads, NUMA architecture.
);

/// The various memory access modes that the assorted locks may support.
/**
	To assist in allowing compile-time computation of the algorithmic order of the threading model.
*/
LIBJMMCG_MAKE_ENUM(memory_access_modes, int,
	erew_memory_access,	 ///< Exclusive-Read, Exclusive-Write memory access, as based on the PRAM model.
	crew_memory_access	///< Concurrent-Read, Exclusive-Write memory access, as based on the PRAM model.
);

}

/// A namespace to hold various traits relating to selecting the specific specialisation of thread_pool they would like.
/**
	These parameters allow the users to choose a thread_pool that has the properties they would like. Note that not all combinations make sense, so will give compilation errors, as they are not implemented, and of the rest, some of those may not be implemented. Contact the author if you need a specific specialisation!
*/
namespace pool_traits {
/// Various models of work distribution.
namespace work_distribution_mode_t {
namespace queue_model_t {
/// The thread_pool owns a single queue into which input_work is placed & from which thread_wk_t is distributed.
/**
	Work stealing from the queue; the adding work to and removing work from the queue is the limiting resource, possibly lock-free or with some sort of lock_type, that may have some sort of anon_semaphore to indicate to waiting pool_threads that it contains work.

	\see intrusive::slist, safe_colln
*/
struct pool_owns_queue {};

enum class stealing_mode_t {
	random	///< When a pool_thread runs out of work it chooses another pool_thread to steal from at random.
};

/// The pool_threads own a queue each into which work that thread places work it generates.
template<stealing_mode_t SM>
struct thread_owns_queue {
	static inline constexpr const stealing_mode_t stealing_mode= SM;
};
}

///. Master-slave; the master thread would be the limiting resource.
template<class QM= queue_model_t::pool_owns_queue>
struct one_thread_distributes {
	BOOST_MPL_ASSERT((std::is_same<QM, queue_model_t::pool_owns_queue>));
	using queue_model= queue_model_t::pool_owns_queue;
};

template<class QM>
struct worker_threads_get_work {
	using queue_model= QM;
};
}

/// Control the size of the thread pool in various ways.
LIBJMMCG_MAKE_ENUM(size_mode_t, int,
	sequential,	  ///< Specify a serial thread pool, i.e. no threading.
	fixed_size,	  ///< Specify n threads to be in the pool.
	time_average_size,	///< Allow some automated mechanism to control the pool size.
	tracks_to_max,	  ///< Have a maximum of n threads, but less or zero according to outstanding work.
	infinite	  ///< Specify a situation where a thread is spawned per job, with no limits.
);
/// Specify if the thread_pool can sort work by some form of priority, executing the highest first.
enum class priority_mode_t {
	normal,	 ///< No priority, a strict fifo.
	priority	  ///< The work items should have some form of partial ordering specified upon them.
};

}

/**
	Clearly you should provide some useful types for these when you specialise this class for an API!
*/
template<generic_traits::api_type::element_type API>
class thread_params {
public:
	typedef int stack_size_type;

	static inline constexpr const generic_traits::api_type::element_type api_type= API;
	static inline constexpr const stack_size_type max_stack_size= std::numeric_limits<stack_size_type>::max();

	typedef int handle_type;
	typedef int pid_type;
	typedef int tid_type;
	using username_type= std::array<char, 256>;	 ///< The size is a guess...

	typedef int processor_mask_type;

	static constexpr processor_mask_type all_cpus_mask() noexcept(true) {
		auto const temp{(std::uint32_t{1} << std::thread::hardware_concurrency()) - 1};
		DEBUG_ASSERT(temp <= std::numeric_limits<processor_mask_type>::max());
		return static_cast<processor_mask_type>(temp);
	}

	typedef int suspend_count;
	typedef int suspend_period_ms;

	typedef int security_type;
	typedef void core_work_fn_ret_t;
	typedef void core_work_fn_arg_t;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"

	typedef core_work_fn_ret_t(__cdecl core_work_fn_type)(void);

#pragma GCC diagnostic pop

	typedef int arglist_type;
	typedef void initflag_type;

	enum creation_flags {
		create_running,
		create_suspended
	};

	enum priority_type {
		lowest,
		idle,
		below_normal,
		normal,
		above_normal,
		highest,
		time_critical,
		unknown_priority
	};

	/**
		Note that these states are in a specific order - the higher the number, the more severe any error.
	*/
	LIBJMMCG_MAKE_ENUM(states, int,
		no_kernel_thread,	  ///< This is not a failure - the thread may not be started yet, or may have exited.
		suspended,
		active,

		// Error conditions now...
		thread_id_not_found,
		invalid_thread,
		deadlocked_with_another_thread,
		get_exit_code_failure,
		failed_to_cancel,
		null_this_pointer,
		stl_exception,
		unknown_exception,
		terminated,

		activating,
		exiting,

		cancelled,

		unknown);
	LIBJMMCG_MAKE_ENUM(thread_cancel_state, int, cancel_enable, cancel_disable);

	const security_type security;
	const stack_size_type stack_size;
	core_work_fn_type* const work_fn;
	arglist_type arglist;

	handle_type handle;
	tid_type id;

	explicit __stdcall thread_params(core_work_fn_type* const sa, const security_type se= 0, const stack_size_type ss= 0) noexcept(true)
		: security(se), stack_size(ss), work_fn(sa), arglist(), handle(), id() {
		DEBUG_ASSERT(work_fn);
	}

	const tstring to_string() const {
		tostringstream ss;
		ss << _T("API type: ") << generic_traits::api_type::to_string(api_type)
			<< _T(", work function ptr: 0x") << std::hex << work_fn
			<< _T(", argument list: 0x") << std::hex << arglist
			<< _T(", handle: 0x") << std::hex << handle
			<< _T(", ID: 0x") << std::hex << id;
		return ss.str();
	}
};

/**
 * \todo Relace with LIBJMMCG_MAKE_ENUM?
 *
 * See <a href="https://stackoverflow.com/questions/77751341/fmt-failed-to-compile-while-formatting-enum-after-upgrading-to-v10-and-above#77751342/>
 */
template<generic_traits::api_type::element_type API>
inline auto
format_as(typename thread_params<API>::priority_type t) {
	return fmt::underlying(t);
}

template<generic_traits::api_type::element_type API>
inline tostream& __fastcall
operator<<(tostream& os, thread_params<API> const& p) {
	os << p.to_string();
	return os;
}

template<generic_traits::creation_cost Cr, generic_traits::destruction_cost Dr, generic_traits::synchronisation_cost Sy>
struct model_traits {
	static inline constexpr const generic_traits::creation_cost creation= Cr;
	static inline constexpr const generic_traits::destruction_cost destruction= Dr;
	static inline constexpr const generic_traits::synchronisation_cost synchronisation= Sy;
};

/**
	These typedefs provide quick ways to get to the various implemented threading models.
*/
using sequential_mode= model_traits<
	generic_traits::creation_cost::sequential_creation,
	generic_traits::destruction_cost::sequential_destruction,
	generic_traits::synchronisation_cost::sequential_sync>;	 ///< I.e. single-threaded, no locks.
using heavyweight_threading= model_traits<
	generic_traits::creation_cost::expensive_to_create,
	generic_traits::destruction_cost::expensive_to_destroy,
	generic_traits::synchronisation_cost::expensive_to_synchronise>;	 /// E.g. x86 or SPARC.
using lightweight_threading= model_traits<
	generic_traits::creation_cost::cheap_to_create,
	generic_traits::destruction_cost::cheap_to_destroy,
	generic_traits::synchronisation_cost::cheap_to_synchronise>;	/// E.g. IBM BlueGene/[C|P] (or Cyclops)

}}}

#ifdef WIN32
#	include "../experimental/NT-based/NTSpecific/thread_params_traits.hpp"
#elif defined(__unix__)
#	include "../unix/thread_params_traits.hpp"
#endif

#endif
