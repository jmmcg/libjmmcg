/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline
csv_iterator::csv_iterator(char delim) noexcept(true)
: delim_(delim) {
}

inline
csv_iterator::csv_iterator(std::string const& data, char delim) noexcept(false)
: data_(&data), delim_(delim) {
	read();
}

inline csv_iterator::reference
csv_iterator::operator*() const noexcept(true) {
	return field_;
}

inline csv_iterator::pointer
csv_iterator::operator->() const noexcept(true) {
	return &field_;
}

inline csv_iterator
csv_iterator::operator++() noexcept(false) {
	read();
	return *this;
}

inline csv_iterator
csv_iterator::operator++(int) noexcept(false) {
	csv_iterator tmp=*this;
	read();
	return tmp;
}

inline bool
csv_iterator::operator==(csv_iterator const& i) const noexcept(true) {
	return (data_==i.data_ && valid_==i.valid_ && pos_==i.pos_)
		|| (!valid_ && !i.valid_);
}

inline bool
csv_iterator::operator!=(csv_iterator const& i) const noexcept(true) {
	return !(*this==i);
}

inline void
csv_iterator::read() noexcept(false) {
	valid_=data_ && (pos_<data_->size());
	if (valid_) {
		const std::size_t last_pos=pos_;
		pos_=data_->find(delim_, last_pos);
		if (pos_==std::string::npos) {
			pos_=data_->size();
		}
		DEBUG_ASSERT(pos_>last_pos);
		field_=data_->substr(last_pos, pos_-last_pos);
		++pos_;
	}
}

} }
