/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "private_/fixed_threads_container.hpp"

#include "private_/pool_thread.hpp"

#include <boost/ptr_container/ptr_vector.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

namespace private_ {

template<class S>
class mstr_accumulate_across_threads {
public:
	typedef S statistics_type;
	typedef typename statistics_type::ave_stats_type result_type;

	template<class PTT>
	static result_type __fastcall vertical_work(PTT const& pool) noexcept(true) {
		const typename PTT::read_lock_type lk(pool.pop_lock(), PTT::atomic_t::lock_traits::infinite_timeout());
		return std::accumulate(
			pool.colln().begin(),
			pool.colln().end(),
			result_type(),
			[](result_type acc, typename PTT::value_type const& v) {
				return acc.update(v->statistics().total_vertical_work().total_samples());
			});
	}
	template<class PTT>
	static result_type __fastcall hrz_work(PTT const& pool) noexcept(true) {
		const typename PTT::read_lock_type lk(pool.pop_lock(), PTT::atomic_t::lock_traits::infinite_timeout());
		return std::accumulate(
			pool.colln().begin(),
			pool.colln().end(),
			result_type(),
			[](result_type acc, typename PTT::value_type const& v) {
				return acc.update(v->statistics().total_hrz_work().total_samples());
			});
	}
};
template<class T>
class mstr_accumulate_across_threads<no_statistics<T>> {
public:
	typedef no_statistics<T> statistics_type;
	typedef typename statistics_type::ave_stats_type result_type;

	template<class PTT>
	static constexpr result_type
	vertical_work(PTT const&) noexcept(true) {
		return result_type();
	}
	template<class PTT>
	static constexpr result_type
	hrz_work(PTT const&) noexcept(true) {
		return result_type();
	}
};

}

/// This pool has an unlimited size, and uses a master to distribute the work to the worker threads.
/**
	i.e. it presumes an unlimited resource of threads. These threads exit after completing their work, but may
	be re-used if there is enough work being submitted to the pool, fast enough.
	Memory is dynamically allocated in the operation of the parallel algorithms, so a quality parallel memory-allocator is recommended such as Hoard or HeapLayers, if scalability is limited by excessive calls to the global operator new..
*/
template<
	class PTT>
class thread_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, PTT> final
	: public private_::thread_pool_queue_model<
		  pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		  pool_traits::size_mode_t::infinite,
		  PTT,
		  safe_colln<
			  std::list<
				  shared_ptr<
					  pool::private_::thread_types::slave<
						  PTT::result_traits_,
						  typename PTT::os_traits,
						  PTT>,
					  api_lock_traits<platform_api, sequential_mode>>>,
			  typename PTT::os_traits::lock_traits::critical_section_type>> {
public:
	using base_t= private_::thread_pool_queue_model<
		pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		pool_traits::size_mode_t::infinite,
		PTT,
		safe_colln<
			std::list<
				shared_ptr<
					pool::private_::thread_types::slave<
						PTT::result_traits_,
						typename PTT::os_traits,
						PTT>,
					api_lock_traits<platform_api, sequential_mode>>>,
			typename PTT::os_traits::lock_traits::critical_section_type>>;
	using pool_traits_type= typename base_t::pool_traits_type;
	using os_traits= typename base_t::os_traits;
	using thread_traits= typename base_t::thread_traits;
	using api_params_type= typename base_t::api_params_type;
	using pool_type= typename base_t::pool_type;
	using statistics_type= typename base_t::statistics_type;
	using work_distribution_mode= typename base_t::work_distribution_mode;
	using signalled_work_queue_type= typename base_t::signalled_work_queue_type;

private:
	void __fastcall create_worker(typename signalled_work_queue_type::value_type&& wk) {
		typename pool_type::value_type wk_thread(new typename pool_type::value_type::value_type(this->exit_requested_, std::forward<typename signalled_work_queue_type::value_type>(wk)));
		wk_thread->create_running();
		this->pool.push_back(std::move(wk_thread));
	}

public:
	explicit __stdcall thread_pool() noexcept(false)
		: base_t(0, 0), have_work(os_traits::lock_traits::atom_unset), purge_pool(1) {
	}

	/**
		The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
	*/
	__stdcall ~thread_pool() noexcept(false) {
		exit();
	}

	/// Obtain access to any statistics data collected by the operation of the thread_pool.
	/**
		Algorithmic complexity when specialised with no_statistics: constant time, otherwise O(pool_size()).
		Note that the value computed for the statistics_type::total_vertical_work() is guaranteed to be accurate. This type of thread_pool does not perform horizontal threading, because it spawns a thread per closure_base-derived closure, so can never suffer deadlock through resource starvation, therefore the value of statistics_type::total_hrz_work() is zero. Therefore, because of the vagaries of multi-threading the following holds:
		statistics_type::total_work_added()>=statistics_type::total_vertical_work()+statistics_type::total_hrz_work()
	*/
	statistics_type const __fastcall statistics() const noexcept(true) override {
		typedef private_::mstr_accumulate_across_threads<statistics_type> acc_t;

		statistics_type stats(statistics_);
		stats.add_vertical_work(acc_t::vertical_work(this->pool));
		stats.add_hrz_work(acc_t::hrz_work(this->pool));
		return stats;
	}

	void exit() noexcept(false) {
		purge_pool.exit();
		this->signalled_work_queue.clear();
		have_work.reset();
		this->exit_requested().set(pool_traits_type::template exit_requested_type<typename work_distribution_mode::queue_model>::states::exit_requested);
		// The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
		// The natural object-destruction order causes the threads in the pool to be destroyed too late, so the pool must be emptied now.
		this->pool.clear();
	}

private:
	static inline constexpr const generic_traits::api_type::element_type mscv_workaround_api_type= os_traits::thread_traits::api_params_type::api_type;

	typedef pool_aspects<
		generic_traits::return_data::element_type::nonjoinable,
		mscv_workaround_api_type,
		typename os_traits::thread_traits::model_type,
		pool_traits::normal_fifo>
		mscv_workaround_pool_aspect_t;
	typedef thread_pool<
		pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>,
		pool_traits::size_mode_t::fixed_size,
		mscv_workaround_pool_aspect_t>
		purge_pool_t;

	class purge_work : protected non_assignable {
	public:
		constexpr __stdcall purge_work(typename base_t::pool_type& p, statistics_type& s) noexcept(true)
			: pool(p), statistics_(s) {
		}
		__stdcall purge_work(const purge_work& p) noexcept(true)
			: pool(p.pool), statistics_(p.statistics_) {
		}

		void __fastcall process() {
			typedef typename base_t::pool_type::container_type::iterator pool_iter_t;
			typedef typename base_t::pool_type::value_type wk_thr_t;
			// Hand off my time-slice, to allow another thread to complete it's work.
			// Maybe that will be a thread from our pool.
			os_traits::thread_traits::sleep(0);
			// Just make sure we don't purge too much...
			while(pool.size() > 1) {
				// It is important to let this go out of scope here, not inside the lock, otherwise we'll deadlock e.g. when trying to add closure_base-derived closure elsewhere.
				wk_thr_t wk_thr;
				{
					const typename base_t::pool_type::write_lock_type lk(pool.pop_lock(), base_t::pool_type::atomic_t::lock_traits::infinite_timeout());
					if(pool.colln().size() > 1) {
						pool_iter_t i(pool.colln().begin());
						if(finished(*i)) {
							wk_thr= std::move(*i);
							pool.colln().pop_front();
						}
					}
				}
				pool.sync_size();
				if(wk_thr.get().get()) {
					statistics_.add_vertical_work(wk_thr->statistics().total_vertical_work());
				}
			}
		}

		bool __fastcall operator<(purge_work const&) const noexcept(true) {
			return true;
		}

	private:
		typename base_t::pool_type& pool;
		statistics_type & statistics_;

		static bool __fastcall finished(const typename base_t::pool_type::value_type& thread) noexcept(true) {
			return thread->state() >= os_traits::thread_traits::api_params_type::states::no_kernel_thread;
		}
	};

	mutable typename os_traits::lock_traits::anon_event_type have_work;
	purge_pool_t purge_pool;
	statistics_type statistics_;

	statistics_type& __fastcall set_statistics() noexcept(true) override {
		return statistics_;
	}

	void __fastcall purge_idle_workers() {
		if(purge_pool.queue_empty() && !this->pool_empty()) {
			// Need to iterate through the list of workers purging the idle ones, to prevent there getting too many of them. Also do this in a separate thread!
			// TODO there is a race condition with the pool exiting and this accessing a dangling reference....
			purge_pool << typename purge_pool_t::nonjoinable() << purge_work(this->pool, this->statistics_);
		}
	}

	void __fastcall add_nonjoinable_work(typename signalled_work_queue_type::value_type&& wk) final {
		create_worker(std::forward<typename signalled_work_queue_type::value_type>(wk));
		statistics_.added_work();
		statistics_.update_max_queue_len(this->queue_size());
		purge_idle_workers();
	}

	typename signalled_work_queue_type::value_type __fastcall add_joinable_work(typename signalled_work_queue_type::value_type&& wk) final {
		add_nonjoinable_work(std::forward<typename signalled_work_queue_type::value_type>(wk));
		return std::move(wk);
	}
};

/// This pool has a limited, specified size, and uses a master to distribute the work to the worker threads.
template<
	class PTT>
class thread_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::fixed_size, PTT>
	: public private_::thread_pool_queue_model<
		  pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		  pool_traits::size_mode_t::fixed_size,
		  PTT,
		  private_::fixed_pool_of_threads<
			  PTT,
			  pool::private_::thread_types::slave<
				  PTT::result_traits_,
				  typename PTT::os_traits,
				  PTT>>> {
public:
	using base_t= private_::thread_pool_queue_model<
		pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		pool_traits::size_mode_t::fixed_size,
		PTT,
		private_::fixed_pool_of_threads<
			PTT,
			boost::ptr_vector<
				pool::private_::thread_types::slave<
					PTT::result_traits_,
					typename PTT::os_traits,
					PTT>>>>;
	using pool_traits_type= typename base_t::pool_traits_type;
	using os_traits= typename base_t::os_traits;
	using thread_traits= typename base_t::thread_traits;
	using api_params_type= typename base_t::api_params_type;
	using pool_type= typename base_t::pool_type;
	using statistics_type= typename base_t::statistics_type;
	using work_distribution_mode= typename base_t::work_distribution_mode;
	using signalled_work_queue_type= typename base_t::signalled_work_queue_type;

	enum class erase_states {
		failed_to_erase,
		ignoring_result,
		erased_successfully
	};

	/// Create the thread pool.
	/**
		\param num_threads	The number of threads in the pool, which must be greater than zero.
	*/
	explicit __stdcall thread_pool(const typename pool_traits_type::pool_type::size_type num_threads) noexcept(false)
		: base_t(num_threads, 0) {
		DEBUG_ASSERT(this->max_num_threads_in_pool > 0);
		if(!this->max_num_threads_in_pool) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>("Cannot have an empty thread pool.", this));
		}
	}
	thread_pool(thread_pool const&)= delete;

	/**
		The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
	*/
	__stdcall ~thread_pool() noexcept(false) {
		exit();
	}

	/// Erase the specified, queued work.
	/**
		Note that if the work has started processing, it will not be erased.

		\param ec	The execution context of the work to erase.
		\return	The outcome of the erase request, which may be successful, or failed because the work may be being processed.

		\see erase_states
	*/
	template<typename ExecT_>
	erase_states __fastcall erase(ExecT_& ec) {
		erase_states ret= erase_states::failed_to_erase;
		if(!ec.erase()) {
			// i.e. we won't wait forever for a result from work that has been erased. (Although we may discard a calculated result. If we can't erase the work from the execution context, then wherever that work is, allow it to be processed to avoid deadlocking that waiting client.)
			ret= (this->work_queue.erase(ec.wk_queue_item()) ? erase_states::erased_successfully : erase_states::ignoring_result);
		}
		if(this->work_queue.empty()) {
			have_work.clear();
		}
		return ret;
	}

	/// Obtain access to any statistics data collected by the operation of the thread_pool.
	/**
		Algorithmic complexity when specialised with no_statistics: constant time, otherwise O(pool_size()).
		Note that the value computed for the statistics_type::total_vertical_work() is guaranteed to be accurate. This type of thread_pool does not perform horizontal threading, because it spawns a thread per closure_base-derived closure, so can never suffer deadlock through resource starvation, therefore the value of statistics_type::total_hrz_work() is zero. Therefore, because of the vagaries of multi-threading the following holds:
		statistics_type::total_work_added()>=statistics_type::total_vertical_work()+statistics_type::total_hrz_work()
	*/
	statistics_type const __fastcall statistics() const noexcept(true) {
		typedef private_::mstr_accumulate_across_threads<statistics_type> acc_t;

		statistics_type stats(statistics_);
		stats.processed_vertical_work(acc_t::vertical_work(this->pool));
		stats.processed_hrz_work(acc_t::hrz_work(this->pool));
		return stats;
	}

	void exit() noexcept(false) {
		have_work.clear();
		this->work_queue.clear();
		this->exit_requested().set();
		// The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
		this->pool.clear();
	}

private:
	mutable typename os_traits::lock_traits::anon_event_type have_work;
	statistics_type statistics_;

	statistics_type& __fastcall set_statistics() noexcept(true) {
		return statistics_;
	}

	/* TODO JMG: implement this!
			void __fastcall add_nonjoinable_work(typename signalled_work_queue_type::value_type &&wk) {
				thread_traits::sleep(0);
				this->work_queue.push_front(wk);
	// TODO JMG: Need to make the threads get the work according to our type...
				this->have_work.add();
				statistics_.added_work();
			}

			typename signalled_work_queue_type::value_type __fastcall add_joinable_work(typename signalled_work_queue_type::value_type &&wk) {
				thread_traits::sleep(0);
				this->work_queue.push_front(wk);
	// TODO JMG: Need to make the threads get the work according to our type...
				this->have_work.add();
				statistics_.added_work();
				return this->work_queue.front();
			}
	*/
};

/// This pool has a maximum specified size to which it will grow and reduce from, and uses a master to distribute the work to the worker threads.
/**
	\todo JMG: Need to complete this...
*/
template<
	class PTT>
class thread_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::tracks_to_max, PTT>
	: public private_::thread_pool_queue_model<
		  pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		  pool_traits::size_mode_t::tracks_to_max,
		  PTT,
		  safe_colln<
			  std::list<
				  shared_ptr<
					  pool::private_::thread_types::slave<
						  PTT::result_traits_,
						  typename PTT::os_traits,
						  PTT>,
					  api_lock_traits<platform_api, sequential_mode>>>,
			  typename PTT::os_traits::lock_traits::critical_section_type>>,
	  protected non_assignable {
public:
	using base_t= private_::thread_pool_queue_model<
		pool_traits::work_distribution_mode_t::one_thread_distributes<>,
		pool_traits::size_mode_t::tracks_to_max,
		PTT,
		safe_colln<
			std::list<
				shared_ptr<
					pool::private_::thread_types::slave<
						PTT::result_traits_,
						typename PTT::os_traits,
						PTT>,
					api_lock_traits<platform_api, sequential_mode>>>,
			typename PTT::os_traits::lock_traits::critical_section_type>>;
	using pool_traits_type= typename base_t::pool_traits_type;
	using os_traits= typename base_t::os_traits;
	using thread_traits= typename base_t::thread_traits;
	using api_params_type= typename base_t::api_params_type;
	using pool_type= typename base_t::pool_type;
	using statistics_type= typename base_t::statistics_type;
	using work_distribution_mode= typename base_t::work_distribution_mode;
	using signalled_work_queue_type= typename base_t::signalled_work_queue_type;

	enum class erase_states {
		failed_to_erase,
		ignoring_result,
		erased_successfully
	};

	/// Create the thread pool.
	/**
		\todo JMG: Need to complete this... What's the thread creation policy????

		\param num_threads	The maximum number of threads in the pool, which must be greater than zero.
	*/
	/*
			explicit __stdcall thread_pool(const typename pool_traits_type::pool_type::size_type num_threads) noexcept(false)
			: base_t(num_threads, 0) {
				DEBUG_ASSERT(this->max_num_threads_in_pool>0);
				if (!this->max_num_threads_in_pool) {
					BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Cannot have an empty thread pool. max_num_threads={}", num_threads), this));
				}
			}
	*/

	/**
		The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
	*/
	__stdcall ~thread_pool() noexcept(false) {
		exit();
	}

	/// Erase the specified, queued work.
	/**
		Note that if the work has started processing, it will not be erased.

		\param ec	The execution context of the work to erase.
		\return	The outcome of the erase request, which may be successful, or failed because the work may be being processed.

		\see erase_states
	*/
	template<typename ExecT_>
	erase_states __fastcall erase(ExecT_& ec) {
		erase_states ret= erase_states::failed_to_erase;
		if(!ec.erase()) {
			// i.e. we won't wait forever for a result from work that has been erased. (Although we may discard a calculated result. If we can't erase the work from the execution context, then wherever that work is, allow it to be processed to avoid deadlocking that waiting client.)
			ret= (this->work_queue.erase(ec.wk_queue_item()) ? erase_states::erased_successfully : erase_states::ignoring_result);
		}
		if(this->work_queue.empty()) {
			have_work.clear();
		}
		return ret;
	}

	/// Obtain access to any statistics data collected by the operation of the thread_pool.
	/**
		Algorithmic complexity when specialised with no_statistics: constant time, otherwise O(pool_size()).
		Note that the value computed for the statistics_type::total_vertical_work() is guaranteed to be accurate. This type of thread_pool does not perform horizontal threading, because it spawns a thread per closure_base-derived closure, so can never suffer deadlock through resource starvation, therefore the value of statistics_type::total_hrz_work() is zero. Therefore, because of the vagaries of multi-threading the following holds:
		statistics_type::total_work_added()>=statistics_type::total_vertical_work()+statistics_type::total_hrz_work()
	*/
	statistics_type const __fastcall statistics() const noexcept(true) {
		typedef private_::mstr_accumulate_across_threads<statistics_type> acc_t;

		statistics_type stats(statistics_);
		stats.processed_vertical_work(acc_t::vertical_work(this->pool));
		stats.processed_hrz_work(acc_t::hrz_work(this->pool));
		return stats;
	}

	void exit() noexcept(false) {
		have_work.clear();
		this->work_queue.clear();
		this->exit_requested().set();
		// The destruction of the collection of threads is sequential, but the threads themselves can exit in parallel, thus speeding up the clean-up of the pool.
		this->pool.clear();
	}

private:
	mutable typename os_traits::lock_traits::anon_event_type have_work;
	statistics_type statistics_;

	statistics_type& __fastcall set_statistics() noexcept(true) {
		return statistics_;
	}
	/*
			void __fastcall add_nonjoinable_work(typename signalled_work_queue_type::value_type &&wk) {
				this->work_queue.push_front(wk);
	// TODO JMG: Need to make the threads get the work according to our type...
				this->have_work.add();
				statistics_.added_work();
			}

			typename signalled_work_queue_type::value_type __fastcall add_joinable_work(typename signalled_work_queue_type::value_type &&wk) {
				this->work_queue.push_front(wk);
	// TODO JMG: Need to make the threads get the work according to our type...
				this->have_work.add();
				statistics_.added_work();
				return this->work_queue.front();
			}
	*/
};

}}}
