#ifndef LIBJMMCG_CORE_MEMORY_BUFFER_HPP
#define LIBJMMCG_CORE_MEMORY_BUFFER_HPP
/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/comparison.hpp>
#include <boost/mpl/greater.hpp>
#include <boost/mpl/int.hpp>

#include <memory>
#include <type_traits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace heap {

/// A very simple custom allocator that allocates a contiguous block of *uninitialised* items on the heap.
/**
	Note that the items allocated are guaranteed not to be initialised.
*/
template<
	class OST,
	class El>
class memory_buffer {
public:
	typedef OST os_traits;
	typedef El element_type;	///< The type of items to allocate, but not initialise.

private:
	using buff_info_t= std::unique_ptr<std::byte[]>;

public:
	using size_type= std::size_t;
	using const_iterator= std::byte const*;
	using iterator= std::byte*;

	static inline constexpr std::size_t stride= sizeof(element_type);	  ///< The sizeof(element_type) each item, specifically in bytes.

	BOOST_MPL_ASSERT((boost::mpl::greater<boost::mpl::int_<stride>, boost::mpl::int_<0>>));

	/**
		\param num_objs	The number of the items to be allocated.
	*/
	[[nodiscard]] explicit memory_buffer(size_type const num_objs) noexcept(false)
		: buff_info(new std::byte[num_objs * stride]), num_objs_(num_objs) {
		if(!buff_info.get()) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to allocate custom memory-buffer for the internal operation of the parallel algorithm. num_objs={}, stride={}", num_objs, stride), this));
		}
		DEBUG_ASSERT(std::fill_n(data(), stride * max_size(), static_cast<std::byte>(0)));
	}

	memory_buffer(memory_buffer const&)= delete;
	memory_buffer(memory_buffer&&)= delete;
	void operator=(memory_buffer const&)= delete;
	void operator=(memory_buffer&&)= delete;

	/**
		\return Note the iterator returned is a byte-pointer.
	*/
	const_iterator data() const noexcept(true) {
		return static_cast<const_iterator>(buff_info.get());
	}

	/**
		\return Note the iterator returned is a byte-pointer.
	*/
	iterator data() noexcept(true) {
		return static_cast<iterator>(buff_info.get());
	}

	/**
		\return	The size of the buffer in number of items.
	*/
	size_type __fastcall max_size() const noexcept(true) {
		return num_objs_;
	}

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall
	operator<<(tostream& os, memory_buffer const& t) noexcept(false) {
		os
			<< _T("buffer address=0x") << std::hex << t.data()
			<< _T(", size=") << std::dec << t.max_size()
			<< _T(", stride=") << memory_buffer::stride;
		return os;
	}

private:
	alignas(element_type) const buff_info_t buff_info;
	const size_type num_objs_;
};

}

namespace stack {

/// A very simple custom allocator that allocates a contiguous block of *uninitialised* items on the stack.
/**
	A simple char[] does not respect alignment, whereas this class ensures that the objects are suitably aligned.
	Note that the items allocated are guaranteed not to be initialised, which is how this differs from std::array, which, as I understand it, must default-initialise each std::array::value_type member according to 8.5.1p7 of the ISO Standard.

	\see std::array
*/
template<
	class El,
	std::size_t num_objs	  ///< The number of the items to be allocated.
	>
class memory_buffer {
public:
	typedef El element_type;	///< The type of items to allocate, but not initialise.

private:
	using aligned_type= std::byte[sizeof(element_type)];
	using buff_info_t= aligned_type[num_objs];

public:
	enum : std::size_t {
		stride= sizeof(element_type),	  ///< The sizeof(element_type) each item, specifically in bytes.
		max_size_= num_objs	 ///< The size of the buffer in number of items.
	};

	BOOST_MPL_ASSERT((boost::mpl::greater<boost::mpl::int_<num_objs>, boost::mpl::int_<0>>));
	BOOST_MPL_ASSERT((boost::mpl::greater<boost::mpl::int_<stride>, boost::mpl::int_<0>>));
	BOOST_MPL_ASSERT((boost::mpl::greater<boost::mpl::int_<max_size_>, boost::mpl::int_<0>>));

public:
	typedef typename std::size_t size_type;
	typedef unsigned char const* const_iterator;
	typedef unsigned char* iterator;

	[[nodiscard]] memory_buffer() noexcept(true) {
		DEBUG_ASSERT(std::fill_n(data(), stride * max_size(), static_cast<unsigned char>(0)));
	}

	~memory_buffer() noexcept(true) {}

	const_iterator data() const noexcept(true) {
		return reinterpret_cast<const_iterator>(buff_info);
	}

	iterator data() noexcept(true) {
		return reinterpret_cast<iterator>(buff_info);
	}

	/**
		\return	The size of the buffer in number of items.
	*/
	static constexpr size_type max_size() noexcept(true) {
		return max_size_;
	}

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream&
	operator<<(tostream& os, memory_buffer const& t) noexcept(false) {
		os
			<< _T("buffer address=0x") << std::hex << t.data()
			<< _T(", size=") << std::dec << t.max_size()
			<< _T(", stride=") << memory_buffer::stride;
		return os;
	}

private:
	alignas(std::alignment_of<element_type>::value) buff_info_t buff_info;

	memory_buffer(memory_buffer const&)= delete;
	memory_buffer(memory_buffer&&)= delete;
	void operator=(memory_buffer const&)= delete;
	void operator=(memory_buffer&&)= delete;
};

}
}}

#endif
