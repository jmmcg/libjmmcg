#ifndef LIBJMMCG_CORE_FUNCTIONAL_HPP
#define LIBJMMCG_CORE_FUNCTIONAL_HPP

// TITLE:
//
// AUTHOR:
// Created by J.M.McGuiness, E-mail: coder@hussar.me.uk
//
// DESCRIPTION:
// If these adaptor classes look similar to the STL variants, you'd be right.
// They are. But I need adaptors for functions with more than just 1 argument.
//
// LEGALITIES:
// Copyright © 2004 by J.M.McGuiness, all rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
/////////////////////////////////////////////////////////////////////////////

#include "core_config.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/*

  Member function functors.

*/

template<typename RetType, typename ObjType>
class const_mem_fun_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef RetType (__fastcall ObjType::*fun_type)(void) const;

	explicit __stdcall const_mem_fun_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall const_mem_fun_ref_t(const const_mem_fun_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~const_mem_fun_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p) const {
		return (p.*pmf)();
	}

	RetType __fastcall operator()(ObjType& p) const {
		return (p.*pmf)();
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1>
class const_mem_fun1_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1&) const;

	explicit __stdcall const_mem_fun1_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall const_mem_fun1_ref_t(const const_mem_fun1_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~const_mem_fun1_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p, const Arg1& b) const {
		return (p.*pmf)(b);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1& b) const {
		return (p.*pmf)(b);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1, typename Arg2>
class const_mem_fun2_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef typename Arg2 second_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1&, const Arg2&) const;

	explicit __stdcall const_mem_fun2_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall const_mem_fun2_ref_t(const const_mem_fun2_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~const_mem_fun2_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p, const Arg1& b, const Arg2& c) const {
		return (p.*pmf)(b, c);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1& b, const Arg2& c) const {
		return (p.*pmf)(b, c);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType>
class mem_fun_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef RetType (__fastcall ObjType::*fun_type)(void);

	explicit __stdcall mem_fun_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall mem_fun_ref_t(const mem_fun_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~mem_fun_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(void) const {
		return (obj.*pmf)();
	}

	RetType __fastcall operator()(void) {
		return (obj.*pmf)();
	}

private:
	const fun_type pmf;
	ObjType obj;
};

template<typename RetType, typename ObjType, typename Arg1>
class mem_fun1_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1&);

	explicit __stdcall mem_fun1_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall mem_fun1_ref_t(const mem_fun1_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~mem_fun1_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1& a) const {
		return (obj.*pmf)(a);
	}

	RetType __fastcall operator()(const Arg1& a) {
		return (obj.*pmf)(a);
	}

private:
	const fun_type pmf;
	ObjType obj;
};

/* TODO JMG: not apparently needed....?
	template<typename RetType,typename ObjType,typename Arg1,typename Arg2>
	class mem_fun2_ref_t {
	public:
		typedef typename ObjType object_type;
		typedef typename RetType result_type;
		typedef typename Arg1 first_argument_type;
		typedef typename Arg2 second_argument_type;
		typedef RetType (__fastcall ObjType::*fun_type)(const Arg1 &,const Arg2 &);

		explicit __stdcall mem_fun2_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall mem_fun2_ref_t(const mem_fun2_ref_t &p) noexcept(true)
		: pmf(p.pmf) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall ~mem_fun2_ref_t(void) noexcept(true) {
		}
		void operator=(const mem_fun2_ref_t &)=delete;

		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b) const {
			return (obj.*pmf)(a,b);
		}
		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b) {
			return (obj.*pmf)(a,b);
		}

	private:
		const fun_type pmf;
		ObjType obj;
	};

	template<typename RetType,typename ObjType,typename Arg1,typename Arg2,typename Arg3>
	class mem_fun3_ref_t {
	public:
		typedef typename ObjType object_type;
		typedef typename RetType result_type;
		typedef typename Arg1 first_argument_type;
		typedef typename Arg2 second_argument_type;
		typedef typename Arg3 third_argument_type;
		typedef RetType (__fastcall ObjType::*fun_type)(const Arg1 &,const Arg2 &,const Arg3 &);

		explicit __stdcall mem_fun3_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall mem_fun3_ref_t(const mem_fun3_ref_t &p) noexcept(true)
		: pmf(p.pmf) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall ~mem_fun3_ref_t(void) noexcept(true) {
		}
		void operator=(const mem_fun3_ref_t &)=delete;

		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b,const Arg3 &c) const {
			return (obj.*pmf)(a,b,c);
		}
		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b,const Arg3 &c) {
			return (obj.*pmf)(a,b,c);
		}

	private:
		const fun_type pmf;
		ObjType obj;
	};

	template<typename RetType,typename ObjType,typename Arg1,typename Arg2,typename Arg3,typename Arg4>
	class mem_fun4_ref_t {
	public:
		typedef typename ObjType object_type;
		typedef typename RetType result_type;
		typedef typename Arg1 first_argument_type;
		typedef typename Arg2 second_argument_type;
		typedef typename Arg3 third_argument_type;
		typedef typename Arg4 fourth_argument_type;
		typedef RetType (__fastcall ObjType::*fun_type)(const Arg1 &,const Arg2 &,const Arg3 &,const Arg4 &);

		explicit __stdcall mem_fun4_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall mem_fun4_ref_t(const mem_fun4_ref_t &p) noexcept(true)
		: pmf(p.pmf) {
			DEBUG_ASSERT(pmf);
		}
		__stdcall ~mem_fun4_ref_t(void) noexcept(true) {
		}

		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b,const Arg3 &c,const Arg4 &d) const {
			return (obj.*pmf)(a,b,c,d);
		}
		RetType __fastcall operator()(const Arg1 &a,const Arg2 &b,const Arg3 &c,const Arg4 &d) {
			return (obj.*pmf)(a,b,c,d);
		}

	private:
		const fun_type pmf;
		ObjType obj;
	};
*/
template<typename RetType, typename ObjType>
class copy_mem_fun_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef RetType (__fastcall ObjType::*fun_type)(void);

	explicit __stdcall copy_mem_fun_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall copy_mem_fun_ref_t(const copy_mem_fun_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~copy_mem_fun_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p) const {
		return (ObjType(p).*pmf)();
	}

	RetType __fastcall operator()(ObjType& p) const {
		return (ObjType(p).*pmf)();
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1>
class copy_mem_fun1_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1&);

	explicit __stdcall copy_mem_fun1_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall copy_mem_fun1_ref_t(const copy_mem_fun1_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~copy_mem_fun1_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p, const Arg1& b) const {
		return (ObjType(p).*pmf)(b);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1& b) const {
		return (ObjType(p).*pmf)(b);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1, typename Arg2>
class copy_mem_fun2_ref_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef typename Arg2 second_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1&, const Arg2&);

	explicit __stdcall copy_mem_fun2_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall copy_mem_fun2_ref_t(const copy_mem_fun2_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~copy_mem_fun2_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p, const Arg1& b, const Arg2& c) const {
		return (ObjType(p).*pmf)(b, c);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1& b, const Arg2& c) const {
		return (ObjType(p).*pmf)(b, c);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1>
class const_mem_fun1_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1) const;

	explicit __stdcall const_mem_fun1_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall const_mem_fun1_t(const const_mem_fun1_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~const_mem_fun1_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const ObjType& p, const Arg1 b) const {
		return (p.*pmf)(b);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1 b) const {
		return (p.*pmf)(b);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename ObjType, typename Arg1>
class mem_fun1_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1);

	explicit __stdcall mem_fun1_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall mem_fun1_t(const mem_fun1_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~mem_fun1_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1 a) const {
		return (obj.*pmf)(a);
	}

	RetType __fastcall operator()(const Arg1 a) {
		return (obj.*pmf)(a);
	}

private:
	fun_type pmf;
	ObjType obj;
};

template<typename RetType, typename ObjType, typename Arg1>
class copy_mem_fun1_t {
public:
	typedef typename ObjType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1 first_argument_type;
	typedef RetType (__fastcall ObjType::*fun_type)(const Arg1);

	explicit __stdcall copy_mem_fun1_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall copy_mem_fun1_t(const copy_mem_fun1_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~copy_mem_fun1_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(ObjType& p, const Arg1 b) const {
		return (p.*pmf)(b);
	}

	RetType __fastcall operator()(ObjType& p, const Arg1 b) {
		return (p.*pmf)(b);
	}

private:
	const fun_type pmf;
};

/*

  Non-member function functors.

*/

template<typename RetType, typename Arg1T>
class ptr_fun_ref_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T&);

	explicit __stdcall ptr_fun_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun_ref_t(const ptr_fun_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T& a) const {
		return (*pmf)(a);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename Arg1T, typename Arg2T>
class ptr_fun2_ref_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef typename Arg2T second_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T&, const Arg2T&);

	explicit __stdcall ptr_fun2_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun2_ref_t(const ptr_fun2_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun2_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T& a, const Arg2T& b) const {
		return (*pmf)(a, b);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename Arg1T, typename Arg2T, typename Arg3T>
class ptr_fun3_ref_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef typename Arg2T second_argument_type;
	typedef typename Arg3T third_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T&, const Arg2T&, const Arg3T&);

	explicit __stdcall ptr_fun3_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun3_ref_t(const ptr_fun3_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun3_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T& a, const Arg2T& b, const Arg3T& c) const {
		return (*pmf)(a, b, c);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename Arg1T, typename Arg2T, typename Arg3T, typename Arg4T>
class ptr_fun4_ref_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef typename Arg2T second_argument_type;
	typedef typename Arg3T third_argument_type;
	typedef typename Arg4T fourth_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T&, const Arg2T&, const Arg3T&);

	explicit __stdcall ptr_fun4_ref_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun4_ref_t(const ptr_fun4_ref_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun4_ref_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T& a, const Arg2T& b, const Arg3T& c, const Arg4T& d) const {
		return (*pmf)(a, b, c, d);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename Arg1T>
class ptr_fun_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T);

	explicit __stdcall ptr_fun_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun_t(const ptr_fun_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T a) const {
		return (*pmf)(a);
	}

private:
	const fun_type pmf;
};

template<typename RetType, typename Arg1T, typename Arg2T>
class ptr_fun2_t {
public:
	typedef typename RetType object_type;
	typedef typename RetType result_type;
	typedef typename Arg1T first_argument_type;
	typedef typename Arg2T second_argument_type;
	typedef RetType(__fastcall* fun_type)(const Arg1T, const Arg2T);

	explicit __stdcall ptr_fun2_t(const fun_type p) noexcept(true)
		: pmf(p) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ptr_fun2_t(const ptr_fun2_t& p) noexcept(true)
		: pmf(p.pmf) {
		DEBUG_ASSERT(pmf);
	}

	__stdcall ~ptr_fun2_t(void) noexcept(true) {
	}

	RetType __fastcall operator()(const Arg1T a, const Arg2T b) const {
		return (pmf)(a, b);
	}

private:
	const fun_type pmf;
};

/*

  Binders.

*/

template<typename ArgType>
struct NullCrack : std::unary_function<ArgType, ArgType> {
	typedef typename std::unary_function<ArgType, ArgType>::result_type result_type;
	typedef typename std::unary_function<ArgType, ArgType>::argument_type argument_type;

	const result_type& __fastcall operator()(const argument_type& r) const noexcept(true) {
		return r;
	}

	result_type& __fastcall operator()(argument_type& r) const noexcept(true) {
		return r;
	}
};

struct no_args {
};

struct one_arg {
};

struct two_args {
};

struct three_args {
};

struct four_args {
};

template<typename FnType>
class binder0args {
public:
	typedef no_args num_args;
	typedef typename FnType::object_type object_type;
	typedef typename FnType::result_type result_type;
	typedef FnType fun_type;

	__stdcall binder0args(const FnType& x) noexcept(true)
		: op(x) {
	}

	__stdcall binder0args(const binder0args& x) noexcept(true)
		: op(x.op) {
	}

	__stdcall ~binder0args(void) noexcept(true) {
	}

	const FnType& __fastcall Op(void) const noexcept(true) {
		return op;
	}

	result_type __fastcall operator()(void) const {
		return op();
	}

	result_type __fastcall operator()(void) {
		return op();
	}

private:
	const FnType op;
};

template<typename FnType, typename Arg1T, template<class> class Cracker1st= NullCrack>
class binder1st {
public:
	typedef one_arg num_args;
	typedef typename FnType::object_type object_type;
	typedef typename FnType::result_type result_type;
	typedef FnType fun_type;
	typedef Arg1T first_argument_type;
	typedef Cracker1st<first_argument_type> crkd_first_argument_type;

	const first_argument_type arg1;

	__stdcall binder1st(const FnType& x, const first_argument_type& v) noexcept(true)
		: op(x), arg1(v) {
	}

	__stdcall binder1st(const binder1st& x) noexcept(true)
		: op(x.op), arg1(x.arg1) {
	}

	__stdcall ~binder1st(void) noexcept(true) {
	}

	const FnType& __fastcall Op(void) const noexcept(true) {
		return op;
	}

	result_type __fastcall operator()(void) const {
		return op(crkd_first_argument_type()(arg1));
	}

	result_type __fastcall operator()(void) {
		return op(crkd_first_argument_type()(arg1));
	}

private:
	FnType op;
};

template<typename FnType, typename Arg1T, typename Arg2T, template<class> class Cracker1st= NullCrack, template<class> class Cracker2nd= NullCrack>
class binder2args {
public:
	typedef two_args num_args;
	typedef typename FnType::object_type object_type;
	typedef typename FnType::result_type result_type;
	typedef FnType fun_type;
	typedef Arg1T first_argument_type;
	typedef Arg2T second_argument_type;
	typedef Cracker1st<first_argument_type> crkd_first_argument_type;
	typedef Cracker2nd<second_argument_type> crkd_second_argument_type;

	first_argument_type arg1;
	const second_argument_type arg2;

	__stdcall binder2args(const FnType& x, const first_argument_type& v, const second_argument_type& b) noexcept(true)
		: op(x), arg1(v), arg2(b) {
	}

	__stdcall binder2args(const binder2args& x) noexcept(true)
		: op(x.op), arg1(x.arg1), arg2(x.arg2) {
	}

	__stdcall ~binder2args(void) noexcept(true) {
	}

	const FnType& __fastcall Op(void) const noexcept(true) {
		return op;
	}

	result_type __fastcall operator()(void) const {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2));
	}

	result_type __fastcall operator()(void) {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2));
	}

private:
	FnType op;
};

template<typename FnType, typename Arg1T, typename Arg2T, typename Arg3T, template<class> class Cracker1st= NullCrack, template<class> class Cracker2nd= NullCrack, template<class> class Cracker3rd= NullCrack>
class binder3args {
public:
	typedef three_args num_args;
	typedef typename FnType::object_type object_type;
	typedef typename FnType::result_type result_type;
	typedef FnType fun_type;
	typedef Arg1T first_argument_type;
	typedef Arg2T second_argument_type;
	typedef Arg3T third_argument_type;
	typedef Cracker1st<first_argument_type> crkd_first_argument_type;
	typedef Cracker2nd<second_argument_type> crkd_second_argument_type;
	typedef Cracker3rd<third_argument_type> crkd_third_argument_type;

	const first_argument_type arg1;
	const second_argument_type arg2;
	const third_argument_type arg3;

	__stdcall binder3args(const FnType& x, const first_argument_type& v, const second_argument_type& b, const third_argument_type& c) noexcept(true)
		: op(x), arg1(v), arg2(b), arg3(c) {
	}

	__stdcall binder3args(const binder3args& x) noexcept(true)
		: op(x.op), arg1(x.arg1), arg2(x.arg2), arg3(x.arg3) {
	}

	__stdcall ~binder3args(void) noexcept(true) {
	}

	const FnType& __fastcall Op(void) const noexcept(true) {
		return op;
	}

	result_type __fastcall operator()(void) const {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2), crkd_third_argument_type()(arg3));
	}

	result_type __fastcall operator()(void) {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2), crkd_third_argument_type()(arg3));
	}

private:
	const FnType op;
};

template<typename FnType, typename Arg1T, typename Arg2T, typename Arg3T, typename Arg4T, template<class> class Cracker1st= NullCrack, template<class> class Cracker2nd= NullCrack, template<class> class Cracker3rd= NullCrack, template<class> class Cracker4th= NullCrack>
class binder4args {
public:
	typedef four_args num_args;
	typedef typename FnType::object_type object_type;
	typedef typename FnType::result_type result_type;
	typedef FnType fun_type;
	typedef Arg1T first_argument_type;
	typedef Arg2T second_argument_type;
	typedef Arg3T third_argument_type;
	typedef Arg3T fourth_argument_type;
	typedef Cracker1st<first_argument_type> crkd_first_argument_type;
	typedef Cracker2nd<second_argument_type> crkd_second_argument_type;
	typedef Cracker3rd<third_argument_type> crkd_third_argument_type;
	typedef Cracker4th<fourth_argument_type> crkd_fourth_argument_type;

	const first_argument_type arg1;
	const second_argument_type arg2;
	const third_argument_type arg3;
	const fourth_argument_type arg4;

	__stdcall binder4args(const FnType& x, const first_argument_type& v, const second_argument_type& b, const third_argument_type& c, const fourth_argument_type& d) noexcept(true)
		: op(x), arg1(v), arg2(b), arg3(c), arg4(d) {
	}

	__stdcall binder4args(const binder4args& x) noexcept(true)
		: op(x.op), arg1(x.arg1), arg2(x.arg2), arg3(x.arg3), arg4(x.arg4) {
	}

	__stdcall ~binder4args(void) noexcept(true) {
	}

	const FnType& __fastcall Op(void) const noexcept(true) {
		return op;
	}

	result_type __fastcall operator()(void) const {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2), crkd_third_argument_type()(arg3), crkd_fourth_argument_type()(arg4));
	}

	result_type __fastcall operator()(void) {
		return op(crkd_first_argument_type()(arg1), crkd_second_argument_type()(arg2), crkd_third_argument_type()(arg3), crkd_fourth_argument_type()(arg4));
	}

private:
	const FnType op;
};

}}

#endif
