/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace rapid_insert_lookup {

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline __stdcall multimap<_Key, _Val, _Pred, _Cont>::multimap(const _Pred&, const allocator_type& al)
	: cont(al), is_sorted(true) {
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline __stdcall multimap<_Key, _Val, _Pred, _Cont>::multimap(const size_type n, const value_type& v, const _Pred&, const allocator_type& al)
	: cont(n, v, al), is_sorted(true) {
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline __stdcall multimap<_Key, _Val, _Pred, _Cont>::multimap(const multimap& m)
	: cont(m.cont), is_sorted(m.is_sorted) {
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline __stdcall multimap<_Key, _Val, _Pred, _Cont>::multimap(const const_iterator& b, const const_iterator& e, const _Pred&, const allocator_type& al)
	: cont(b, e, al), is_sorted(false) {
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline __stdcall multimap<_Key, _Val, _Pred, _Cont>::~multimap(void) {
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline multimap<_Key, _Val, _Pred, _Cont>& __fastcall multimap<_Key, _Val, _Pred, _Cont>::operator=(const multimap & m) {
	cont= m.cont;
	is_sorted= m.is_sorted;
	return *this;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::sort(void) const {
	std::sort(cont.begin(), cont.end(), value_compare());
	is_sorted= true;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::sort(void) {
	std::sort(cont.begin(), cont.end(), value_compare());
	is_sorted= true;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::begin(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multimap<_Key, _Val, _Pred, _Cont>*>(this)->sort();
	}
	return cont.begin();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::begin(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.begin();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_reverse_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::rbegin(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multimap<_Key, _Val, _Pred, _Cont>*>(this)->sort();
	}
	return cont.rbegin();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::reverse_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::rbegin(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.rbegin();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::end(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multimap<_Key, _Val, _Pred, _Cont>*>(this)->sort();
	}
	return cont.end();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::end(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.end();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_reverse_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::rend(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multimap<_Key, _Val, _Pred, _Cont>*>(this)->sort();
	}
	return cont.rend();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::reverse_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::rend(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.rend();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline const typename multimap<_Key, _Val, _Pred, _Cont>::size_type __fastcall multimap<_Key, _Val, _Pred, _Cont>::size(void) const noexcept(true) {
	return cont.size();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline const typename multimap<_Key, _Val, _Pred, _Cont>::size_type __fastcall multimap<_Key, _Val, _Pred, _Cont>::max_size(void) const noexcept(true) {
	return cont.max_size();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline bool __fastcall multimap<_Key, _Val, _Pred, _Cont>::empty(void) const noexcept(true) {
	return cont.empty();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::clear(void) {
	cont.clear();
	is_sorted= true;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::resize(const size_type new_sz, const value_type& v) {
	const size_type orig_size= cont.size();
	cont.resize(new_sz, v);
	// Only get unsorted if the underlying container grows in size.
	is_sorted= (orig_size >= cont.size());
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::reserve(const size_type new_sz) {
	const size_type orig_size= cont.size();
	cont.reserve(new_sz);
	// Only get unsorted if the underlying container grows in size.
	is_sorted= (orig_size >= cont.size());
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::allocator_type __fastcall multimap<_Key, _Val, _Pred, _Cont>::get_allocator(void) const {
	return cont.get_allocator();
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::InsertItr_ __fastcall multimap<_Key, _Val, _Pred, _Cont>::insert(const value_type& val) {
	// Note - we make no checks to determine if the key is already in the map. This could maen that "operator[](...)" won't let us reliably access all of those elements with the same key.
	cont.push_back(val);
	const InsertItr_ ret(std::make_pair(std::prev(cont.end()), true));
	is_sorted= false;
	return ret;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::insert(const iterator& i, const value_type& v) {
	// Note - we make no checks to determine if the key is already in the map. This could maen that "operator[](...)" won't let us reliably access all of those elements with the same key.
	const iterator ret(cont.insert(i, v));
	is_sorted= false;
	return ret;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline void __fastcall multimap<_Key, _Val, _Pred, _Cont>::insert(const const_iterator& f, const const_iterator& l) {
	// Note - we make no checks to determine if the key is already in the map. This could mean that "operator[](...)" won't let us reliably access all of those elements with the same key.
	cont.insert(end(), f, l);
	is_sorted= false;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::erase(const iterator& i) {
	return cont.erase(i);
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::erase(const iterator& f, const iterator& l) {
	return cont.erase(f, l);
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::find_first_key(const const_iterator& i) const {
	if(i != end()) {
		const_iterator iter(i);
		while(iter != cont.begin() && !key_compare()(std::prev(iter)->first, iter->first) && !key_compare()(iter->first, std::prev(iter)->first)) {
			--iter;
		}
		DEBUG_ASSERT(!key_compare()(i->first, iter->first) && !key_compare()(iter->first, i->first));
		return iter;
	}
	return i;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::find_first_key(const iterator& i) const {
	if(i != end()) {
		iterator iter(i);
		while(iter != cont.begin() && !key_compare()(std::prev(iter)->first, iter->first) && !key_compare()(iter->first, std::prev(iter)->first)) {
			--iter;
		}
		DEBUG_ASSERT(!key_compare()(i->first, iter->first) && !key_compare()(iter->first, i->first));
		return iter;
	}
	return i;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline const typename multimap<_Key, _Val, _Pred, _Cont>::size_type __fastcall multimap<_Key, _Val, _Pred, _Cont>::erase(const key_type& key) {
	size_type num_erased= 0;
	for(
		iterator iter(find_first_key(std::lower_bound(cont.begin(), cont.end(), std::make_pair(key, mapped_type()), value_compare())));
		iter != cont.end() && !key_compare()(key, iter->first);
		iter= find_first_key(std::lower_bound(cont.begin(), cont.end(), std::make_pair(key, mapped_type()), value_compare()))) {
		cont.erase(iter);
		++num_erased;
	}
	return num_erased;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::mapped_type& __fastcall multimap<_Key, _Val, _Pred, _Cont>::operator[](const key_type & key) {
	if(!is_sorted) {
		sort();
	}
	const iterator iter(find_first_key(std::lower_bound(cont.begin(), cont.end(), std::make_pair(key, mapped_type()), value_compare())));
	if(iter == cont.end()) {
		cont.push_back(std::make_pair(key, mapped_type()));
		is_sorted= false;
		return std::prev(cont.end())->second;
	}
	return iter->second;
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::const_iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::find(const key_type& key) const {
	if(!is_sorted) {
		const_cast<multimap<_Key, _Val, _Pred, _Cont>*>(this)->sort();
	}
	const const_iterator i(find_first_key(std::lower_bound(cont.begin(), cont.end(), std::make_pair(key, mapped_type()), value_compare())));
	if(i != cont.end()) {
		const bool is_same_key(!key_compare()(i->first, key) && !key_compare()(key, i->first));
		return is_same_key ? i : cont.end();
	} else {
		return i;
	}
}

template<typename _Key, typename _Val, typename _Pred, typename _Cont>
inline typename multimap<_Key, _Val, _Pred, _Cont>::iterator __fastcall multimap<_Key, _Val, _Pred, _Cont>::find(const key_type& key) {
	if(!is_sorted) {
		sort();
	}
	const iterator i(find_first_key(std::lower_bound(cont.begin(), cont.end(), std::make_pair(key, mapped_type()), value_compare())));
	if(i != cont.end()) {
		const bool is_same_key(!key_compare()(i->first, key) && !key_compare()(key, i->first));
		return is_same_key ? i : cont.end();
	} else {
		return i;
	}
}

// multiset

template<typename _Key, typename _Pred, typename _Cont>
inline __stdcall multiset<_Key, _Pred, _Cont>::multiset(const _Pred&, const allocator_type& al)
	: cont(al), is_sorted(true) {
}

template<typename _Key, typename _Pred, typename _Cont>
inline __stdcall multiset<_Key, _Pred, _Cont>::multiset(const size_type n, const value_type& v, const _Pred&, const allocator_type& al)
	: cont(n, v, al), is_sorted(true) {
}

template<typename _Key, typename _Pred, typename _Cont>
inline __stdcall multiset<_Key, _Pred, _Cont>::multiset(const multiset& m)
	: cont(m.cont), is_sorted(m.is_sorted) {
}

template<typename _Key, typename _Pred, typename _Cont>
inline __stdcall multiset<_Key, _Pred, _Cont>::multiset(const const_iterator& b, const const_iterator& e, const _Pred&, const allocator_type& al)
	: cont(b, e, al), is_sorted(false) {
}

template<typename _Key, typename _Pred, typename _Cont>
inline __stdcall multiset<_Key, _Pred, _Cont>::~multiset(void) {
}

template<typename _Key, typename _Pred, typename _Cont>
inline multiset<_Key, _Pred, _Cont>& __fastcall multiset<_Key, _Pred, _Cont>::operator=(const multiset & m) {
	cont= m.cont;
	is_sorted= m.is_sorted;
	return *this;
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::sort(void) const {
	std::sort(cont.begin(), cont.end(), value_compare());
	is_sorted= true;
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::sort(void) {
	std::sort(cont.begin(), cont.end(), value_compare());
	is_sorted= true;
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_iterator __fastcall multiset<_Key, _Pred, _Cont>::begin(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multiset<_Key, _Pred, _Cont>*>(this)->sort();
	}
	return cont.begin();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::begin(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.begin();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_reverse_iterator __fastcall multiset<_Key, _Pred, _Cont>::rbegin(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multiset<_Key, _Pred, _Cont>*>(this)->sort();
	}
	return cont.rbegin();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::reverse_iterator __fastcall multiset<_Key, _Pred, _Cont>::rbegin(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.rbegin();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_iterator __fastcall multiset<_Key, _Pred, _Cont>::end(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multiset<_Key, _Pred, _Cont>*>(this)->sort();
	}
	return cont.end();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::end(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.end();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_reverse_iterator __fastcall multiset<_Key, _Pred, _Cont>::rend(void) const noexcept(true) {
	if(!is_sorted) {
		const_cast<multiset<_Key, _Pred, _Cont>*>(this)->sort();
	}
	return cont.rend();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::reverse_iterator __fastcall multiset<_Key, _Pred, _Cont>::rend(void) noexcept(true) {
	if(!is_sorted) {
		sort();
	}
	return cont.rend();
}

template<typename _Key, typename _Pred, typename _Cont>
inline const typename multiset<_Key, _Pred, _Cont>::size_type __fastcall multiset<_Key, _Pred, _Cont>::size(void) const noexcept(true) {
	return cont.size();
}

template<typename _Key, typename _Pred, typename _Cont>
inline const typename multiset<_Key, _Pred, _Cont>::size_type __fastcall multiset<_Key, _Pred, _Cont>::max_size(void) const noexcept(true) {
	return cont.max_size();
}

template<typename _Key, typename _Pred, typename _Cont>
inline bool __fastcall multiset<_Key, _Pred, _Cont>::empty(void) const noexcept(true) {
	return cont.empty();
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::clear(void) {
	cont.clear();
	is_sorted= true;
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::resize(const size_type new_sz, const value_type& v) {
	const size_type orig_size= cont.size();
	cont.resize(new_sz, v);
	// Only get unsorted if the underlying container grows in size.
	is_sorted= (orig_size >= cont.size());
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::reserve(const size_type new_sz) {
	const size_type orig_size= cont.size();
	cont.reserve(new_sz);
	// Only get unsorted if the underlying container grows in size.
	is_sorted= (orig_size >= cont.size());
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::allocator_type __fastcall multiset<_Key, _Pred, _Cont>::get_allocator(void) const {
	return cont.get_allocator();
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::InsertItr_ __fastcall multiset<_Key, _Pred, _Cont>::insert(const value_type& val) {
	// Note - we make no checks to determine if the key is already in the map. This could maen that "operator[](...)" won't let us reliably access all of those elements with the same key.
	cont.push_back(val);
	const InsertItr_ ret(std::make_pair(std::prev(cont.end()), true));
	is_sorted= false;
	return ret;
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::insert(const iterator& i, const value_type& v) {
	// Note - we make no checks to determine if the key is already in the map. This could maen that "operator[](...)" won't let us reliably access all of those elements with the same key.
	const iterator ret(cont.insert(i, v));
	is_sorted= false;
	return ret;
}

template<typename _Key, typename _Pred, typename _Cont>
inline void __fastcall multiset<_Key, _Pred, _Cont>::insert(const const_iterator& f, const const_iterator& l) {
	// Note - we make no checks to determine if the key is already in the map. This could maen that "operator[](...)" won't let us reliably access all of those elements with the same key.
	cont.insert(end(), f, l);
	is_sorted= false;
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::erase(const iterator& i) {
	return cont.erase(i);
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::erase(const iterator& f, const iterator& l) {
	return cont.erase(f, l);
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_iterator __fastcall multiset<_Key, _Pred, _Cont>::find_first_key(const const_iterator& i) const {
	if(i != end()) {
		const_iterator iter(i);
		while(iter != cont.begin() && !key_compare()(*std::prev(iter), *iter) && !key_compare()(*iter, *std::prev(iter))) {
			--iter;
		}
		DEBUG_ASSERT(!key_compare()(*i, *iter) && !key_compare()(*iter, *i));
		return iter;
	}
	return i;
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::find_first_key(const iterator& i) const {
	if(i != end()) {
		iterator iter(i);
		while(iter != cont.begin() && !key_compare()(*std::prev(iter), *iter) && !key_compare()(*iter, *std::prev(iter))) {
			--iter;
		}
		DEBUG_ASSERT(!key_compare()(*i, *iter) && !key_compare()(*iter, *i));
		return iter;
	}
	return i;
}

template<typename _Key, typename _Pred, typename _Cont>
inline const typename multiset<_Key, _Pred, _Cont>::size_type __fastcall multiset<_Key, _Pred, _Cont>::erase(const key_type& key) {
	size_type num_erased= 0;
	for(
		iterator iter(find_first_key(std::lower_bound(cont.begin(), cont.end(), key, value_compare())));
		iter != cont.end() && !key_compare()(key, *iter);
		iter= find_first_key(std::lower_bound(cont.begin(), cont.end(), key, value_compare()))) {
		cont.erase(iter++);
		++num_erased;
	}
	return num_erased;
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::const_iterator __fastcall multiset<_Key, _Pred, _Cont>::find(const key_type& key) const {
	if(!is_sorted) {
		const_cast<multiset<_Key, _Pred, _Cont>*>(this)->sort();
	}
	const const_iterator i(find_first_key(std::lower_bound(cont.begin(), cont.end(), key, value_compare())));
	if(i != cont.end()) {
		const bool is_same_key(!key_compare()(i->first, key) && !key_compare()(key, i->first));
		return is_same_key ? i : cont.end();
	} else {
		return i;
	}
}

template<typename _Key, typename _Pred, typename _Cont>
inline typename multiset<_Key, _Pred, _Cont>::iterator __fastcall multiset<_Key, _Pred, _Cont>::find(const key_type& key) {
	if(!is_sorted) {
		sort();
	}
	const iterator i(find_first_key(std::lower_bound(cont.begin(), cont.end(), key, value_compare())));
	if(i != cont.end()) {
		const bool is_same_key(!key_compare()(i->first, key) && !key_compare()(key, i->first));
		return is_same_key ? i : cont.end();
	} else {
		return i;
	}
}

}}}
