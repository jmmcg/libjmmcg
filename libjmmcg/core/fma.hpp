#ifndef LIBJMMCG_CORE_FMA_HPP
#define LIBJMMCG_CORE_FMA_HPP
/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <cmath>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// The set of contained classes implements a DSEL that allows one to more naturally make use of the fma() operation using the natural mathematical operators of which std::fma() implements: * and + (or -).
namespace fma {

namespace private_ {
struct dbl_mul;
struct dbl_add;
struct dbl_sub;
struct dbl_mul_add;
}

/// To use this functionality convert one of the doubles used in the multiply operation to this class. It will then automatically implement the m*x+c into a fused multiply-add operation.
/**
	Commutativity & associativity of the equation is supported, as is nesting and m*x-c.

	\see std::fma()
*/
struct dbl {
	const double lhs_;

	/// Explicitly allow implicit conversions.
	constexpr dbl(double const l) noexcept(true);
	/// Allow nesting of std::fma() operations.
	dbl(private_::dbl_mul_add const &l) noexcept(true);

	constexpr bool operator==(const double r) const noexcept(true);
	constexpr bool operator==(const dbl r) const noexcept(true);
	constexpr private_::dbl_mul operator*(const double r) const noexcept(true);
	constexpr private_::dbl_mul operator*(const dbl r) const noexcept(true);
	constexpr private_::dbl_add operator+(const double r) const noexcept(true);
	constexpr private_::dbl_sub operator-(const double r) const noexcept(true);
};

/**
	Allow associativity regarding the additions.
*/
constexpr private_::dbl_mul_add
operator+(const dbl a, private_::dbl_mul const &d) noexcept(true);

/**
	Allow associativity regarding the additions.
*/
constexpr private_::dbl_mul_add
operator-(const dbl a, private_::dbl_mul const &d) noexcept(true);

/**
	Allow associativity regarding the multiplies.
*/
constexpr private_::dbl_mul
operator*(const double l, dbl const r) noexcept(true);

std::ostream &
operator<<(std::ostream &os, dbl const d) noexcept(false);

namespace private_ {

struct dbl_mul {
	const double lhs_;
	const double rhs_;

	constexpr dbl_mul(double const &l, double const &r) noexcept(true);

	/// Allow sequences of multiplies then the add.
	constexpr dbl_mul operator*(const double r) const noexcept(true);
	constexpr dbl_mul operator*(const dbl r) const noexcept(true);

	constexpr dbl_mul_add operator+(const double a) const noexcept(true);
	constexpr dbl_mul_add operator+(const dbl a) const noexcept(true);

	/// Also admit fused-multiply-subtract.
	constexpr dbl_mul_add operator-(const double a) const noexcept(true);
	constexpr dbl_mul_add operator-(const dbl a) const noexcept(true);
};

struct dbl_mul_add {
	const dbl_mul mul_;
	const double add_;

	constexpr dbl_mul_add(dbl_mul const &m, double const a) noexcept(true);
	constexpr dbl_mul_add(dbl_add const &a, double const m) noexcept(true);
	constexpr dbl_mul_add(dbl_sub const &a, double const m) noexcept(true);

	/// Allow implicit conversion back to double to ease use within formulae.
	operator double () const noexcept(true);
};

struct dbl_add {
	const double lhs_;
	const double rhs_;

	constexpr dbl_add(double const l, double const r) noexcept(true);

	/// Allow sequences of multiplies then the add.
};

struct dbl_sub {
	const double lhs_;
	const double rhs_;

	explicit constexpr dbl_sub(double const l, double const r) noexcept(true);

	/// Allow sequences of multiplies then the add.
};

/**
	Allow associativity regarding the additions.
*/
constexpr dbl_mul_add
operator+(const double a, dbl_mul const &m) noexcept(true);
/**
	Allow associativity regarding the subtractions.
*/
constexpr dbl_mul_add
operator-(const double a, dbl_mul const &m) noexcept(true);

double &
operator+=(double &a, dbl_mul const &m) noexcept(true);

double &
operator-=(double &a, dbl_mul const &m) noexcept(true);

}

} } }

#include "fma_impl.hpp"

#endif
