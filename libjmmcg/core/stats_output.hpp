#ifndef LIBJMMCG_CORE_STATS_OUTPUT_HPP
#define LIBJMMCG_CORE_STATS_OUTPUT_HPP

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "line_iterator.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <fstream>
#include <memory>
#include <vector>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace stats_to_csv {

namespace private_ {

inline auto
find_run_num(char const* const stats_name) noexcept(false) {
	std::ifstream os(stats_name);
	line_iterator line(os), eof;
	std::vector<std::string> lines;
	while(line != eof) {
		lines.emplace_back(std::move(*line));
		++line;
	}
	if(lines.empty()) {
		return 1ul;
	} else {
		const auto& last_entry= lines.rbegin()->empty() ? ++lines.rbegin() : lines.rbegin();
		DEBUG_ASSERT(!last_entry->empty());
		const auto slash_at= last_entry->find("\\");
		DEBUG_ASSERT(slash_at < last_entry->size());
		auto end= std::next(&*last_entry->begin(), slash_at);
		const unsigned long last_number= std::strtoul(last_entry->data(), &end, 10);
		return last_number + 1u;
	}
}

struct file_wrapper final {
	std::ofstream stats;

	[[nodiscard]] file_wrapper(char const* const stats_name, unsigned long run_number) noexcept(false)
		: stats(stats_name, std::ios_base::app) {
		stats
			<< run_number << "\\n" LIBJMMCG_VERSION_NUMBER "\\n" LIBJMMCG_CXX_COMPILER_ID "v" LIBJMMCG_CXX_COMPILER_VERSION;
	}

	~file_wrapper() noexcept(true) {
		stats << std::endl;
	}
};

}

/**
	This file must only be included once, otherwise there should be obvious ODR issues (the linker will complain about multiple definitions of this variable.
	This variable is managed by other means.

	\see setup(), teardown(), wrapper
*/
static std::unique_ptr<private_::file_wrapper> handle;

/**
	This can be used in this manner:

		boost::unit_test::fixture(boost::bind(&stats_to_csv::setup, "count_setbits.csv"), &stats_to_csv::teardown)

	or:

		*boost::unit_test::fixture<libjmmcg::stats_to_csv::wrapper>(std::string("count_setbits.csv"))

	or:

		*make_fixture("count_setbits.csv")

	\see handle, teardown(), make_fixture(), BOOST_AUTO_TEST_SUITE
*/
inline void
setup(char const* const stats_name) noexcept(false) {
	handle.reset(new private_::file_wrapper(stats_name, private_::find_run_num(stats_name)));
}

/**
	\see setup()
*/
inline void
teardown() noexcept(true) {
	handle.reset();
}

/**
	\see handle, setup(), teardown(), make_fixture()
*/
struct wrapper final {
	[[nodiscard]] explicit wrapper(char const* stats_name) noexcept(false) {
		setup(stats_name);
	}

	[[nodiscard]] explicit wrapper(std::string const& stats_name) noexcept(false) {
		setup(stats_name.c_str());
	}

	~wrapper() noexcept(true) {
		teardown();
	}
};

#ifdef BOOST_TEST_INCLUDED
/**
	This file needs to be included after: <boost/test/included/unit_test.hpp> or an equivalent for this function to work.

	\see wrapper, BOOST_AUTO_TEST_SUITE
*/
inline auto
make_fixture(char const* stats_name) noexcept(false) {
	return boost::unit_test::fixture<wrapper>(stats_name);
}
#endif

}}}

#endif
