#ifndef LIBJMMCG_CORE_DYNAMIC_CAST_HPP
#define LIBJMMCG_CORE_DYNAMIC_CAST_HPP
/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
	The purpose of this class is to give an opportunity to programmers to replace the useless "std::bad_cast" thrown when performing a "dynamic_cast<To &>(obj)". This class will catch the exception, wrap it up with lots more info, then re-throw an exception_type derived from Excpt and std::bad_cast. For example it will give:

	1. The class types cast from and to.
	2. The address of the object on which the cast was attempted.
	3. The file and line of where the check was done. (So you can more easily track down the errors!)
	4. Extra potentially handy info.
*/
template<
	typename To>
struct checked_dynamic_cast {
	typedef To result_type;
	/**
		So that you can still catch this type with your unaltered catches of std::bad_cast, but better to alter them so that you can get at the greater info that is provided.

		\see std::bad_cast
	*/
	struct exception_type : virtual public std::runtime_error, virtual public std::bad_cast, virtual public boost::exception {
		explicit exception_type(std::string&& r)
			: std::runtime_error(std::move(r)) {}
	};

	template<typename From>
	result_type&
	operator()(From& obj, const unsigned long l, const char* fun_name, const char* file) noexcept(false) {
		try {
			return dynamic_cast<result_type&>(obj);
		} catch(std::bad_cast const& e) {
			BOOST_THROW_EXCEPTION(throw_exception<exception_type>(fmt::format("{} C++ RTTI type to cast from: '{}', C++ RTTI type to cast to: '{}', address of object being cast=0x{}", e.what(), boost::core::demangle(typeid(From).name()), boost::core::demangle(typeid(result_type).name()), &obj), &checked_dynamic_cast<result_type>::operator()<From>));
		}
	}
};

}}

/// Use the macro "JMMCG_CHK_DYNAMIC_CAST(To, obj)" as your drop-in replacement for where you'd place statements like: "To &a=dynamic_cast<To>(obj);". Note that this macro cannot replace expression forms.
#define CHK_DYNAMIC_CAST(To, obj) \
	jmmcg::LIBJMMCG_VER_NAMESPACE::checked_dynamic_cast<To>()((obj), __LINE__, __PRETTY_FUNCTION__, __FILE__)

#endif
