/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline std::string_view
nonce_using_epoch::get() noexcept(true) {
	buffer_.fill('\0');
	auto const time_now= std::chrono::system_clock::now();
	auto const since_epoch= time_now.time_since_epoch();
	auto const millis= std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
	auto const now= millis.count();	 // just like java (new Date()).getTime();}
	auto end= fmt::format_to(buffer_.begin(), "{}", now);
	std::string_view nonce{buffer_.begin(), end};
	DEBUG_ASSERT(!nonce.empty());
	return nonce;
}

inline std::string
encode_as_sha256(std::string_view str) noexcept(true) {
	std::array<unsigned char, SHA256_DIGEST_LENGTH> hash;
	::SHA256(reinterpret_cast<unsigned char const*>(str.begin()), str.size(), hash.data());
	std::ostringstream ss;
	for(auto digit: hash) {
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned>(digit);
	}
	return ss.str();
}

}}
