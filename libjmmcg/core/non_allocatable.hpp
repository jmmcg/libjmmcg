#ifndef LIBJMMCG_CORE_NON_ALLOCATABLE_HPP
#define LIBJMMCG_CORE_NON_ALLOCATABLE_HPP
/******************************************************************************
** Copyright © 2008 by J.M.McGuiness, jmmcg@sourceforge.net & coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"
#include "debug_defines.hpp"

#include <new>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Used to try to help ensure that an object can only be allocated on the stack.
class non_newable {
	static void * operator new(std::size_t) noexcept(false)=delete;
	static void * operator new(std::size_t, const std::nothrow_t&) noexcept(true)=delete;
	static void * operator new[](std::size_t) noexcept(false)=delete;
	static void * operator new[](std::size_t, const std::nothrow_t&) noexcept(true)=delete;
	static void * operator new(std::size_t, void *) noexcept(false)=delete;
	static void * operator new[](std::size_t, void *) noexcept(false)=delete;
};

class non_deleteable {
	static void operator delete(void *) noexcept(true)=delete;
	static void operator delete(void *, const std::nothrow_t&) noexcept(true)=delete;
	static void operator delete[](void *) noexcept(true)=delete;
	static void operator delete[](void *, const std::nothrow_t&) noexcept(true)=delete;
	static void operator delete(void *, void *) noexcept(true)=delete;
	static void operator delete[](void *, void *) noexcept(true)=delete;
};

class non_allocatable : protected non_newable, protected non_deleteable {
};

class non_addressable {
	void operator&()=delete;
};

} }

#endif
