/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {
namespace ppd { namespace private_ {
/*
	This is a hack to reduce the amount of reference counting we have to do in pop_front_1_nochk_nolk().
*/
template<class Cont>
struct pop_front_t {
	Cont& cont;

	explicit constexpr pop_front_t(Cont& c) noexcept(true)
		: cont(c) {}
	~pop_front_t() noexcept(true) {
		cont.pop_front();
	}
};
/*
	This is a hack to reduce the amount of reference counting we have to do in pop_front_1_nochk_nolk().
*/
template<class Cont>
struct pop_t {
	Cont& cont;

	constexpr pop_t(Cont& c) noexcept(true)
		: cont(c) {}
	~pop_t() noexcept(true) {
		cont.pop();
	}
};

}}

template<class Lk>
inline typename ppd::signalling<Lk>::atomic_t::lock_result_type
ppd::signalling<Lk>::remove() noexcept(noexcept(have_work_->lock(0))) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	// This uses try_lock(), which won't block if the signalled size is empty. Note that the signalled size and the actual number of items in the collection is not an invariant under multi-threading conditions.
	return have_work_->lock(0);
}

template<class Lk>
inline void
ppd::signalling<Lk>::remove(typename atomic_t::count_type const c) noexcept(false) {
	for(typename atomic_t::count_type i= 0; i < c; ++i) {
		[[maybe_unused]] const typename atomic_t::lock_result_type ret= remove();
		DEBUG_ASSERT(ret.second != atomic_t::lock_result_type::second_type::atom_abandoned);
	}
}

template<class Lk>
inline typename ppd::signalling<Lk>::atomic_t::lock_result_type
ppd::signalling<Lk>::try_remove() noexcept(noexcept(have_work_->try_lock())) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	return have_work_->try_lock();
}

template<class Lk>
inline void
ppd::signalling<Lk>::clear() noexcept(noexcept(have_work_->clear())) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	have_work_->clear();
}

template<class Lk>
inline typename ppd::signalling<Lk>::atomic_t::count_type
ppd::signalling<Lk>::count() const noexcept(noexcept(have_work_->count())) {
	DEBUG_ASSERT(dynamic_cast<atomic_t const*>(have_work_));
	return have_work_->count();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln() noexcept(noexcept(container_type()) && noexcept(have_work_type()))
	: container_type(), have_work() {
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(typename have_work_type::atomic_t& ev)
	: container_type(), have_work(ev) {
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(std::initializer_list<value_type> v)
	: container_type(v), have_work() {
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(size_type const sz, value_type const& v)
	: container_type(sz, v), have_work() {
}

template<typename C, typename M, typename WL, class Sig, class MLk>
template<class T1, class T2>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(size_type const sz, T1 const&, T2 const&)
	: container_type(sz), have_work() {
	have_work.add(container_type::size());
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(const container_type& t)
	: container_type(t), have_work() {
	have_work.add(container_type::size());
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>::safe_colln(const safe_colln& t) noexcept(false)
	: container_type(), have_work(t.have_work) {
	const read_lock_type lk(t.pop_lock(), lock_traits::infinite_timeout());
	container_type::insert(container_type::begin(), t.begin(), t.end());
	have_work.add(container_type::size());
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline ppd::safe_colln<C, M, WL, Sig, MLk>&
ppd::safe_colln<C, M, WL, Sig, MLk>::operator=(const safe_colln& t) noexcept(false) {
	const read_lock_type lock1(t.pop_lock(), lock_traits::infinite_timeout());
	const write_lock_type lock2(push_lock(), lock_traits::infinite_timeout());
	have_work.clear();
	container_type::operator=(t);
	have_work.add(container_type::size());
	return *this;
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline bool
ppd::safe_colln<C, M, WL, Sig, MLk>::empty() const noexcept(true) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::empty();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline
	typename ppd::safe_colln<C, M, WL, Sig, MLk>::size_type
	ppd::safe_colln<C, M, WL, Sig, MLk>::sync_size() const noexcept(false) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	if(static_cast<size_type>(have_work.count()) < container_type::size()) {
		have_work.add(container_type::size() - have_work.count());
	}
	return container_type::size();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline
	typename ppd::safe_colln<C, M, WL, Sig, MLk>::size_type
	ppd::safe_colln<C, M, WL, Sig, MLk>::size() const noexcept(true) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::size();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline
	typename ppd::safe_colln<C, M, WL, Sig, MLk>::value_type
	ppd::safe_colln<C, M, WL, Sig, MLk>::operator[](size_type s) const noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::operator[](s);
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::push_back(value_type const& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_back(v);
	have_work.add();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::push_back(value_type&& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_back(v);
	have_work.add();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::push_front(const value_type& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_front(v);
	have_work.add();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::push_front(value_type&& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_front(v);
	have_work.add();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline
	typename ppd::safe_colln<C, M, WL, Sig, MLk>::size_type
	ppd::safe_colln<C, M, WL, Sig, MLk>::erase(const value_type& v) noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	size_type elems_erased= 0;
	typename container_type::container_type::iterator b;
	while((b= std::find(this->c.begin(), this->c.end(), v)) != this->c.end()) {
		this->c.erase(b);
		++elems_erased;
	}
	DEBUG_ASSERT(elems_erased <= std::numeric_limits<typename have_work_type::atomic_t::count_type>::max());
	have_work.remove(static_cast<typename have_work_type::atomic_t::count_type>(elems_erased));
	return elems_erased;
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::reserve(size_type sz) noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	container_type::reserve(sz);
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::clear() noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	if(!container_type::empty()) {
		have_work.clear();
		container_type().swap(*this);
	}
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline bool
ppd::safe_colln<C, M, WL, Sig, MLk>::operator==(safe_colln const& sc) const noexcept(true) {
	const read_lock_type lk(sc.pop_lock(), lock_traits::infinite_timeout());
	const read_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
	return this->colln() == sc.colln();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
template<typename M1, typename WL1, class Sig1, class MLk1>
inline bool
ppd::safe_colln<C, M, WL, Sig, MLk>::operator==(safe_colln<C, M1, WL1, Sig1, MLk1> const& sc) const noexcept(true) {
	const typename safe_colln<C, M1, WL1, Sig1, MLk1>::read_lock_type lk(sc.pop_lock(), safe_colln<C, M1, WL1, Sig1, MLk1>::lock_traits::infinite_timeout());
	const read_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
	return this->colln() == sc.colln();
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::resize_noinit_nolk(const size_type sz) noexcept(false) {
	const size_type cont_sz= container_type::size();
	if(cont_sz < sz) {
		container_type::reserve(sz);
		std::fill_n(std::back_inserter(colln()), sz - cont_sz, value_type());
		have_work.add(sz - cont_sz);
	} else if(cont_sz > sz) {
		DEBUG_ASSERT((cont_sz - sz) <= std::numeric_limits<typename have_work_type::atomic_t::count_type>::max());
		have_work.remove(static_cast<typename have_work_type::atomic_t::count_type>(cont_sz - sz));
		const typename container_type::iterator new_end(std::next(container_type::begin(), sz));
		container_type::erase(new_end, container_type::end());
	}
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::resize(const size_type sz) noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	container_type::resize(sz);
	const size_type cont_sz= container_type::size();
	if(cont_sz < sz) {
		have_work.add(sz - cont_sz);
	} else if(cont_sz > sz) {
		DEBUG_ASSERT((cont_sz - sz) <= std::numeric_limits<typename have_work_type::atomic_t::count_type>::max());
		have_work.remove(static_cast<typename have_work_type::atomic_t::count_type>(cont_sz - sz));
	}
}

template<typename C, typename M, typename WL, class Sig, class MLk>
inline void
ppd::safe_colln<C, M, WL, Sig, MLk>::swap(safe_colln& t) noexcept(false) {
	// TODO
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::queue<QT, M, WL, Sig, ValRet, MLk>::queue() noexcept(noexcept(container_type()) && noexcept(have_work_type()))
	: container_type(), have_work() {
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::queue<QT, M, WL, Sig, ValRet, MLk>::queue(typename have_work_type::atomic_t& ev)
	: container_type(), have_work(ev) {
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::queue<QT, M, WL, Sig, ValRet, MLk>::queue(const queue& t) noexcept(false)
	: container_type(), have_work(t.have_work) {
	const read_lock_type lock2(t.pop_lock(), lock_traits::infinite_timeout());
	const read_lock_type lock1(t.push_lock(), lock_traits::infinite_timeout());
	container_type::insert(container_type::begin(), t.begin(), t.end());
	have_work.add(container_type::size());
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::queue<QT, M, WL, Sig, ValRet, MLk>&
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::operator=(const queue& t) noexcept(false) {
	const read_lock_type lock1(t.pop_lock(), atomic_t::lock_traits::infinite_timeout());
	const write_lock_type lock2(pop_lock(), atomic_t::lock_traits::infinite_timeout());
	have_work.clear();
	container_type::operator=(t);
	have_work.add(container_type::size());
	return *this;
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline bool
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::empty() const noexcept(true) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::empty();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::size_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::sync_size() const noexcept(false) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	if(have_work.count() < container_type::size()) {
		have_work.add(container_type::size() - have_work.count());
	}
	return container_type::size();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::size_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::size() const noexcept(true) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::size();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::front() const noexcept(false) {
	// Guarantee that only one item at a time can be removed.
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	if(!container_type::empty()) {
		return container_type::front();
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Queue empty.", &ppd::queue<QT, M, WL, Sig, ValRet, MLk>::front));
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::push_back(value_type const& v) noexcept(true) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_back(v);
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::push_back(value_type&& v) noexcept(true) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_back(std::move(v));
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::push_front(const value_type& v) noexcept(true) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_front(v);
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::push_front(value_type&& v) noexcept(true) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	container_type::push_front(std::move(v));
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::size_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::erase(const value_type& v) noexcept(true) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	const size_type elems_erased(container_type::erase(v));
	DEBUG_ASSERT(elems_erased <= std::numeric_limits<typename have_work_type::atomic_t::count_type>::max());
	have_work.remove(static_cast<typename have_work_type::atomic_t::count_type>(elems_erased));
	return elems_erased;
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::clear() noexcept(true) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	if(!container_type::empty()) {
		have_work.clear();
	}
	container_type::clear();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_1_nochk_nolk() noexcept(noexcept(have_work.remove())) {
	typedef private_::pop_front_t<container_type> pop_t;

	have_work.remove();
	const pop_t pop(*this);
	return container_type::front();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_1_nochk_nosig() noexcept(true) {
	typedef private_::pop_front_t<container_type> pop_t;

	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	const pop_t pop(*this);
	return container_type::front();
}

#pragma GCC diagnostic push
// There's no sensible way to provide a simple specialisation that can remove the warning, which is due to possible use of GSS(k) batching in the queue, so we have to just ignore this warning.
#pragma GCC diagnostic ignored "-Wmissing-braces"
template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nochk_nolk() noexcept(false) {
	return value_ret_type{
		pop_front_1_nochk_nolk()};
}
#pragma GCC diagnostic pop

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nolk() noexcept(false) {
	if(!container_type::empty()) {
		return this->pop_front_nochk_nolk();
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Queue empty.", &ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nolk));
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::queue<QT, M, WL, Sig, ValRet, MLk>::pop_front() noexcept(false) {
	const write_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return pop_front_nolk();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::funky_queue() noexcept(noexcept(container_type()) && noexcept(have_work_type()))
	: container_type(), have_work() {
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::funky_queue(typename have_work_type::atomic_t& ev)
	: container_type(), have_work(ev) {
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::funky_queue(const funky_queue& t) noexcept(false)
	: container_type(), have_work(t.have_work) {
	const read_lock_type lock2(t.pop_lock(), lock_traits::infinite_timeout());
	const read_lock_type lock1(t.push_lock(), lock_traits::infinite_timeout());
	container_type::insert(container_type::begin(), t.begin(), t.end());
	have_work.add(container_type::size());
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>&
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::operator=(const funky_queue& t) noexcept(false) {
	const scoped_lock<atomic_t, atomic_t, atomic_t, atomic_t> wait(t.push_lock(), t.pop_lock(), push_lock(), pop_lock());
	have_work.clear();
	container_type::operator=(t);
	have_work.add(container_type::size());
	return *this;
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline bool
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::empty() const noexcept(true) {
	const read_lock_type lock2(pop_lock(), lock_traits::infinite_timeout());
	const read_lock_type lock1(push_lock(), lock_traits::infinite_timeout());
	return container_type::empty();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::size_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::sync_size() const noexcept(false) {
	const read_lock_type lock2(pop_lock(), lock_traits::infinite_timeout());
	const read_lock_type lock1(push_lock(), lock_traits::infinite_timeout());
	if(have_work.count() < container_type::size()) {
		have_work.add(container_type::size() - have_work.count());
	}
	return container_type::size();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::size_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::size() const noexcept(true) {
	const read_lock_type lk(pop_lock(), lock_traits::infinite_timeout());
	return container_type::size();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::clear() noexcept(false) {
	// Note that this takes out write locks on the funky_queue.
	const scoped_lock<atomic_t, atomic_t> wait(push_lock(), pop_lock());
	have_work.clear();
	container_type::clear();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline bool
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::erase(value_type const& v) noexcept(false) {
	// Note that this takes out write locks on the funky_queue.
	const scoped_lock<atomic_t, atomic_t> wait(push_lock(), pop_lock());
	const typename container_type::iterator it(std::find(container_type::begin(), container_type::end(), v));
	if(it != container_type::end()) {
		[[maybe_unused]] const typename have_work_type::atomic_t::lock_result_type ret= have_work.remove();
		DEBUG_ASSERT(ret.second != have_work_type::lock_traits::atom_abandoned);
		container_type::erase(it);
		return true;
	} else {
		return false;
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::front() const noexcept(false) {
	// Guarantee that only one item at a time can be removed.
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		return container_type::front();
	} else {
		const write_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
		if(!container_type::empty()) {
			return container_type::front();
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Funky queue empty.", &ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::front));
		}
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::back() const noexcept(false) {
	// Guarantee that only one item at a time can be removed.
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		return container_type::back();
	} else {
		const write_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
		if(!container_type::empty()) {
			return container_type::back();
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Funky queue empty.", &ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::back));
		}
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push(value_type const& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		container_type::push(v);
	} else {
		const write_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
		container_type::push(v);
	}
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push(value_type&& v) noexcept(false) {
	const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		container_type::push(v);
	} else {
		const write_lock_type lk1(pop_lock(), lock_traits::infinite_timeout());
		container_type::push(v);
	}
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_back(value_type const& v) noexcept(false) {
	if(size() >= serialise_size) {
		const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
		container_type::push_back(v);
	} else {
		const scoped_lock<atomic_t, atomic_t> wait(push_lock(), pop_lock());
		container_type::push_back(v);
	}
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_back(value_type&& v) noexcept(false) {
	if(size() >= serialise_size) {
		const write_lock_type lk(push_lock(), lock_traits::infinite_timeout());
		container_type::push_back(v);
	} else {
		const scoped_lock<atomic_t, atomic_t> wait(push_lock(), pop_lock());
		container_type::push_back(v);
	}
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type&
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::back_nolk() noexcept(true) {
	return container_type::back();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type const&
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::back_nolk() const noexcept(true) {
	return container_type::back();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_back_nolk(const value_type& e) noexcept(false) {
	container_type::push_back(e);
	DEBUG_ASSERT(!empty());
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_back_nolk(value_type&& e) noexcept(false) {
	container_type::push_back(e);
	DEBUG_ASSERT(!empty());
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_nolk(const value_type& e) noexcept(false) {
	container_type::push(e);
	DEBUG_ASSERT(!empty());
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::push_nolk(value_type&& e) noexcept(false) {
	container_type::push(e);
	DEBUG_ASSERT(!empty());
	have_work.add();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_nochk_nolk() noexcept(false) {
	[[maybe_unused]] const typename have_work_type::atomic_t::lock_result_type ret= have_work.remove();
	DEBUG_ASSERT(ret.second != have_work_type::lock_traits::atom_abandoned);
	container_type::pop();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_1_nochk_nolk() noexcept(noexcept(have_work.remove())) {
	typedef private_::pop_front_t<container_type> pop_t;

	have_work.remove();
	const pop_t pop(*this);
	return container_type::front();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_1_nochk_nosig() noexcept(true) {
	typedef private_::pop_front_t<container_type> pop_t;

	const write_lock_type excl_push_lk(pop_lock(), lock_traits::infinite_timeout());
	const pop_t pop(*this);
	return container_type::front();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nochk_nolk() noexcept(false) {
	return value_ret_type{
		pop_front_1_nochk_nolk()};
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_nolk() noexcept(false) {
	typedef private_::pop_t<container_type> pop_t;

	[[maybe_unused]] const typename have_work_type::atomic_t::lock_result_type ret= have_work.remove();
	DEBUG_ASSERT(ret.second != have_work_type::lock_traits::atom_abandoned);
	const pop_t pop(*this);
	return container_type::front();
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nolk() noexcept(false) {
	if(container_type::size() >= serialise_size) {
		return this->pop_front_nochk_nolk();
	} else {
		const write_lock_type excl_push_lk(push_lock(), lock_traits::infinite_timeout());
		if(!container_type::empty()) {
			return this->pop_front_nochk_nolk();
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Funky queue empty.", &ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front_nolk));
		}
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop() noexcept(false) {
	const write_lock_type excl_pop_lk(pop_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		return pop_nolk();
	} else {
		const write_lock_type excl_push_lk(push_lock(), lock_traits::infinite_timeout());
		return pop_nolk();
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline typename ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::value_ret_type
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front() noexcept(false) {
	const write_lock_type excl_pop_lk(pop_lock(), lock_traits::infinite_timeout());
	if(container_type::size() >= serialise_size) {
		return this->pop_front_nochk_nolk();
	} else {
		const write_lock_type excl_push_lk(push_lock(), lock_traits::infinite_timeout());
		if(!container_type::empty()) {
			return this->pop_front_nochk_nolk();
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Funky queue empty.", &ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::pop_front));
		}
	}
}

template<typename QT, typename M, typename WL, class Sig, class ValRet, class MLk>
inline void
ppd::funky_queue<QT, M, WL, Sig, ValRet, MLk>::remove(const value_type& e) noexcept(false) {
	// Note that this takes out write locks on the queue.
	const scoped_lock<atomic_t, atomic_t> wait(push_lock(), pop_lock());
	DEBUG_ASSERT(!container_type::empty());
	[[maybe_unused]] const typename have_work_type::atomic_t::atomic_state_type ret= have_work.remove();
	DEBUG_ASSERT(ret != have_work_type::atomic_t::atom_abandoned);
	container_type::remove(e);
}

}}
