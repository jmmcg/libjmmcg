/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline optimizing_fifo_flyback_buffer::optimizing_fifo_flyback_buffer(size_type min_capacity, size_type max_capacity) noexcept(false)
	: max_capacity_(max_capacity), cont_(min_capacity, initialisation_value), head_{cont_.begin()}, tail_{cont_.begin()} {
	DEBUG_ASSERT(min_capacity < max_capacity_);
	DEBUG_ASSERT(empty());
}

inline constexpr bool
optimizing_fifo_flyback_buffer::empty() const noexcept(true) {
	return size() <= 0U;
}

inline constexpr typename optimizing_fifo_flyback_buffer::size_type
optimizing_fifo_flyback_buffer::size() const noexcept(true) {
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	auto const size= static_cast<size_type>(tail_ - head_);
	DEBUG_ASSERT(size <= cont_.size());
	return size;
}

inline constexpr typename optimizing_fifo_flyback_buffer::size_type
optimizing_fifo_flyback_buffer::capacity() const noexcept(true) {
	return cont_.size();
}

inline constexpr typename optimizing_fifo_flyback_buffer::size_type
optimizing_fifo_flyback_buffer::max_capacity() const noexcept(true) {
	return max_capacity_;
}

inline void constexpr optimizing_fifo_flyback_buffer::clear() noexcept(true) {
	head_= tail_= cont_.begin();
	DEBUG_ASSERT(empty());
#ifndef NDEBUG
	std::fill(head_, cont_.end(), initialisation_value);
#endif
}

template<class ConsumeOp>
inline optimizing_fifo_flyback_buffer::size_type
optimizing_fifo_flyback_buffer::pop_front(ConsumeOp&& consume_op) noexcept(false) {
	if(!empty()) [[likely]] {
		size_type bytes_claimed_consumed{};
		try {
			auto const head= head_;
			auto const tail= tail_;
			bytes_claimed_consumed= consume_op(head_, tail_);
			if(head != head_ || tail != tail_) [[unlikely]] {
				if(empty()) [[likely]] {
					return size();
				} else {
					BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Consumer claimed to consume more bytes, {}, than were available {} as the underlying buffer was changed.", bytes_claimed_consumed, size()), &optimizing_fifo_flyback_buffer::pop_front<ConsumeOp>));
				}
			}
		} catch(...) {
			clear();
			throw;
		}
		if(bytes_claimed_consumed <= size()) [[likely]] {
			head_+= bytes_claimed_consumed;
			DEBUG_ASSERT(cont_.begin() <= head_);
			DEBUG_ASSERT(head_ <= tail_);
			DEBUG_ASSERT(tail_ <= cont_.end());
			if(head_ == cont_.end()) {
				DEBUG_ASSERT(tail_ == cont_.end());
				clear();
			}
			return bytes_claimed_consumed;
		} else {
			// This might throw because in the call-chain in consume_op(), there may be a call to flyback_buffer::clear()...
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Consumer claimed to consume more bytes, {}, than were available {}.", bytes_claimed_consumed, size()), &optimizing_fifo_flyback_buffer::pop_front<ConsumeOp>));
		}
	} else {
		return size_type{};
	}
}

template<class Generator>
inline optimizing_fifo_flyback_buffer::size_type
optimizing_fifo_flyback_buffer::generate_n(Generator&& generator) noexcept(false) {
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	// Are we full?
	if(size() >= capacity()) [[unlikely]] {
		// Buffer full: it must grow!
		if(capacity() < max_capacity()) {
			size_type const old_tail_index= cont_.size();
			// Aaaaaaaaand: fuck the D-cache.
			cont_.resize(max_capacity_, initialisation_value);
			head_= cont_.begin();
			tail_= cont_.begin() + old_tail_index;
			DEBUG_ASSERT(!empty());
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Buffer full. Failed to grow the underlying buffer as the maximum capacity of {} has been reached.", max_capacity()), &optimizing_fifo_flyback_buffer::generate_n<Generator>));
		}
	} else if(tail_ == cont_.end()) [[unlikely]] {
		// We need to move the data to the beginning.
		DEBUG_ASSERT(cont_.begin() < head_);
		auto const size_of_unparsed_block_to_move= tail_ - head_;
		std::memmove(cont_.data(), &*head_, static_cast<size_type>(size_of_unparsed_block_to_move));
		head_= cont_.begin();
		tail_= head_ + size_of_unparsed_block_to_move;
		DEBUG_ASSERT(!empty());
		DEBUG_ASSERT(tail_ < cont_.end());
	} else if(head_ == cont_.end() && tail_ == cont_.end()) [[unlikely]] {
		clear();
		DEBUG_ASSERT(empty());
	}
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ < cont_.end());
	std::size_t bytes_read= generator(tail_, cont_.end());
	DEBUG_ASSERT(bytes_read <= cont_.size());
	tail_+= bytes_read;
	DEBUG_ASSERT(cont_.begin() <= head_);
	DEBUG_ASSERT(head_ <= tail_);
	DEBUG_ASSERT(tail_ <= cont_.end());
	return bytes_read;
}

}}
