#ifndef LIBJMMCG_CORE_THREAD_API_TRAITS_HPP
#define LIBJMMCG_CORE_THREAD_API_TRAITS_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#ifdef _MSC_VER
#	pragma warning(disable:4786)	  // identifier was truncated to '255' characters in the debug information
#	define NOMINMAX
#	undef max
#	undef min
#endif

#include "atomic_counter.hpp"
#include "locking.hpp"

#include <array>
#include <limits>
#include <mutex>

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {

namespace ppd {

template<generic_traits::api_type::element_type API, typename Mdl>
struct api_lock_traits;

namespace no_locking {
struct critical_section;
class anon_event;

enum atomic_state_type {
	atom_set= 0,
	atom_abandoned,
	atom_already_set,
	atom_max_recurse,
	atom_deadlocked,
	atom_perm_error,
	atom_failed,
	atom_interrupted,
	atom_unset,
	atom_errno
};

/**
 * \todo Replace with LIBJMMCG_MAKE_ENUM?
 *
 * Or use "template <> struct fmt::formatted<SomeType> : fmt::ostream_formatter {};", per Martin Janzen's suggestion.
 *
 * See <a href="https://stackoverflow.com/questions/77751341/fmt-failed-to-compile-while-formatting-enum-after-upgrading-to-v10-and-above#77751342/>
 */
inline auto
format_as(atomic_state_type t) {
	return fmt::underlying(t);
}

}

/// The various lock-types for sequential use, with no threading.
template<>
struct api_lock_traits<platform_api, sequential_mode> {
	static inline constexpr const generic_traits::api_type::element_type api_type= platform_api;
	typedef sequential_mode model_type;

	enum atomic_state_type {
		atom_set= 0,
		atom_abandoned,
		atom_already_set,
		atom_max_recurse,
		atom_deadlocked,
		atom_perm_error,
		atom_failed,
		atom_interrupted,
		atom_nomemory,
		atom_unset,
		atom_errno
	};

	typedef int handle_type;

	typedef no_locking::critical_section anon_mutex_type;
	typedef no_locking::critical_section nonrecursive_anon_mutex_type;
	typedef no_locking::critical_section recursive_anon_mutex_type;
	typedef no_locking::anon_event anon_event_type;
	using anon_spin_event_type= no_locking::critical_section;
	typedef no_locking::critical_section critical_section_type;
	typedef no_locking::critical_section recursive_critical_section_type;
	typedef no_locking::critical_section mutex_type;
	typedef no_locking::critical_section recursive_mutex_type;
	typedef no_locking::anon_event event_type;
	typedef no_locking::anon_event anon_semaphore_type;
	typedef no_locking::anon_event semaphore_type;
	/**
		This counter-type may not use a mutex_type to protect the counter, it may use the underlying API to provide a lock-free atomic counter. It is 2-5 times faster than the atomic_counter_type, also the CPU load on two processors is close to 100% in certain performance tests.
	*/
	template<class V>
	using atomic_counter_type= atomic_ctr<V, api_lock_traits<api_type, model_type>>;
	template<class V>
	using noop_atomic_ctr= noop_atomic_ctr_base<V, api_lock_traits<api_type, model_type>>;
	template<class V>
	using atomic= atomic_ctr<V, api_lock_traits<api_type, model_type>>;
	using timeout_type= int;

	template<class... T>
	struct scoped_lock {
		explicit constexpr scoped_lock(T&...) noexcept(true) {}
	};

	static constexpr timeout_type infinite_timeout() noexcept(true) {
		return std::numeric_limits<timeout_type>::max();
	}
};

/**
 * \todo Relace with LIBJMMCG_MAKE_ENUM?
 *
 * See <a href="https://stackoverflow.com/questions/77751341/fmt-failed-to-compile-while-formatting-enum-after-upgrading-to-v10-and-above#77751342/>
 */
inline auto
format_as(api_lock_traits<platform_api, sequential_mode>::atomic_state_type t) {
	return fmt::underlying(t);
}

template<>
struct api_lock_traits<generic_traits::api_type::no_api, sequential_mode> : public api_lock_traits<platform_api, sequential_mode> {
};

namespace no_locking {

typedef api_lock_traits<platform_api, sequential_mode> lock_traits;

struct critical_section final : public lock::lockable<lock_traits> {
	//			typedef no_locking::lock_traits lock_traits;
	typedef lock::lockable<lock_traits> base_t;
	using lock_traits= base_t::lock_traits;
	typedef lock_traits::atomic_state_type atomic_state_type;
	typedef lock_traits::timeout_type timeout_type;
	typedef lock::in_process<critical_section> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<critical_section> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	explicit critical_section(const atomic_state_type= lock_traits::atom_set) noexcept(true) {
	}

	critical_section(critical_section const&)= delete;

	static constexpr atomic_state_type __fastcall set() noexcept(true) {
		return lock_traits::atom_set;
	}

	atomic_state_type __fastcall lock() noexcept(true) override {
		return lock_traits::atom_set;
	}

	atomic_state_type __fastcall lock(const timeout_type) noexcept(true) override {
		return lock_traits::atom_set;
	}

	atomic_state_type __fastcall unlock() noexcept(true) override {
		return lock_traits::atom_unset;
	}

	static void decay() noexcept(true) {}
};

template<class V>
struct atomic_ctr final : public lock_traits {
	typedef no_locking::lock_traits lock_traits;
	typedef lock_traits base_t;
	typedef lock_traits::atomic_state_type atomic_state_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	explicit constexpr atomic_ctr(const atomic_state_type= atom_set) noexcept(true) {
	}

	atomic_ctr(atomic_ctr const&)= delete;
};

class anon_event final : public lock::lockable_settable<lock_traits> {
public:
	typedef no_locking::lock_traits lock_traits;
	typedef lock::lockable<lock_traits> base_t;
	typedef lock_traits::atomic_state_type atomic_state_type;
	typedef atomic_state_type lock_result_type;
	typedef lock_traits::timeout_type timeout_type;
	typedef std::size_t count_type;
	typedef lock::in_process<anon_event> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	explicit anon_event(const atomic_state_type f= lock_traits::atom_unset) noexcept(true)
		: fudge(f) {
	}

	anon_event(anon_event const&)= delete;

	atomic_state_type __fastcall set(void) noexcept(true) override {
		fudge= lock_traits::atom_set;
		return fudge;
	}

	atomic_state_type __fastcall reset(void) noexcept(true) override {
		fudge= lock_traits::atom_unset;
		return fudge;
	}

	lock_result_type __fastcall lock() noexcept(true) override {
		fudge= lock_traits::atom_set;
		return fudge;
	}

	lock_result_type __fastcall lock(const timeout_type) noexcept(true) override {
		fudge= lock_traits::atom_set;
		return fudge;
	}

	lock_result_type __fastcall unlock() noexcept(true) override {
		fudge= lock_traits::atom_unset;
		return fudge;
	}

	static constexpr count_type __fastcall count() noexcept(true) {
		return 0;
	}

	static lock_type::atomic_t& __fastcall locker() noexcept(true) {
		static lock_type::atomic_t lk;
		return lk;
	}

private:
	atomic_state_type fudge;
};

}

template<generic_traits::api_type::element_type API, typename Mdl>
struct api_threading_traits {
	typedef thread_params<API> api_params_type;
	typedef api_lock_traits<API, Mdl> lock_traits;
	typedef Mdl model_type;

	static void raise(int sig) noexcept(true);
	/// Create a new OS-level thread of the appropriate type.
	/**
		\return true is the OS-level thread was successfully created, false otherwise.
	*/
	static bool __fastcall create(const typename api_params_type::creation_flags creflag, api_params_type&) noexcept(true);
	static typename api_params_type::suspend_count __fastcall suspend(api_params_type const&) noexcept(true);
	static typename api_params_type::suspend_count __fastcall resume(api_params_type const&) noexcept(true);
	static typename api_params_type::states::element_type __fastcall state(api_params_type&) noexcept(true);
	static bool __fastcall is_running(api_params_type&) noexcept(true);
	/// Exit the currently running thread.
	static void __fastcall exit(typename api_params_type::states::element_type&) noexcept(true);
	/// Request or force that the target thread exit.
	/**
		Note that this may result in undefined behaviour depending upon how the underlying OS copes with cleaning the state loaded by the thread.
	*/
	static void __fastcall terminate(typename api_params_type::handle_type) noexcept(false);
	/// Ensure that any possible clean up that may need to be done of thread state may be performed after the thread has exited.
	static void __fastcall cleanup(typename api_params_type::handle_type) noexcept(false);
	static void __fastcall set_kernel_priority(typename api_params_type::handle_type, const typename api_params_type::priority_type) noexcept(false);
	static typename api_params_type::priority_type __fastcall get_kernel_priority(typename api_params_type::handle_type) noexcept(false);
	static void __fastcall set_kernel_affinity(typename api_params_type::handle_type const, typename api_params_type::processor_mask_type const) noexcept(false);
	static typename api_params_type::processor_mask_type __fastcall get_kernel_affinity(typename api_params_type::handle_type const) noexcept(false);
	static typename api_params_type::pid_type __fastcall get_current_process_id() noexcept(true);
	static typename api_params_type::tid_type __fastcall get_current_thread_id() noexcept(true);
	static typename api_params_type::handle_type __fastcall get_current_thread() noexcept(true);
	static typename api_params_type::username_type __fastcall get_current_username() noexcept(true);
	static void __fastcall set_cancelstate(typename api_params_type::thread_cancel_state::element_type state) noexcept(false);
	/// Sort-of threading, as it suspends the current thread of execution.
	/**
		\param	period	The period to sleep in ms. A value of zero should cause the thread to yield, so that it is no longer in a running state. (The rest of its time-slice is re-scheduled to run at some later period.)
	*/
	static void __fastcall sleep(const typename api_params_type::suspend_period_ms period) noexcept(false);
	static typename api_params_type::states::element_type __fastcall wait_thread_exit(api_params_type&, const typename lock_traits::timeout_type period) noexcept(false);
	/// Dump a backtrace to std::cerr if SIGABRT is raised, once this function is called.
	static void __fastcall set_backtrace_on_signal() noexcept(true);
	static inline constexpr std::size_t max_thread_name_length= 16;	///< Including the ternimating '\0'.
	using thread_name_t= std::array<char, max_thread_name_length>;
	static void __fastcall set_name(typename api_params_type::handle_type, thread_name_t const& name) noexcept(false);
	static thread_name_t __fastcall get_name(typename api_params_type::handle_type) noexcept(false);

	struct cancellability {
		cancellability() noexcept(false) {
			set_cancelstate(api_params_type::thread_cancel_state::cancel_enable);
		}

		~cancellability() noexcept(false) {
			set_cancelstate(api_params_type::thread_cancel_state::cancel_disable);
		}
	};
};

}
}}

#include "thread_api_traits_impl.hpp"

#include "hp_timer.hpp"

#ifdef WIN32
#	include "../experimental/NT-based/NTSpecific/thread_api_traits.hpp"
#	include "../experimental/NT-based/NTSpecific/NTLocking.hpp"
#elif defined(__unix__)
#	include "../unix/thread_api_traits.hpp"
#	include "../unix/posix_locking.hpp"
#endif

#endif
