/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace asio {

template<class LkT>
inline void
socket_wrapper<LkT>::set_options(std::size_t /*min_message_size*/, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority /*priority*/, std::size_t /*incoming_cpu*/) {
	// TODO "protocol not available":	socket_.set_option(boost::asio::socket_base::send_low_watermark(min_message_size));
	DEBUG_ASSERT(max_message_size <= std::numeric_limits<int>::max());
	socket_.set_option(boost::asio::socket_base::send_buffer_size(static_cast<int>(max_message_size)));
	// TODO "protocol not available":	socket_.set_option(boost::asio::socket_base::receive_low_watermark(min_message_size));
	socket_.set_option(boost::asio::socket_base::receive_buffer_size(static_cast<int>(max_message_size)));
	DEBUG_ASSERT(std::chrono::duration_cast<std::chrono::seconds>(timeout) >= std::chrono::seconds(1));
	DEBUG_ASSERT(std::chrono::duration_cast<std::chrono::seconds>(timeout).count() <= std::numeric_limits<int>::max());
	socket_.set_option(boost::asio::socket_base::linger(true, static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(timeout).count())));
	socket_.set_option(boost::asio::ip::tcp::no_delay(true));
}

template<class LkT>
inline socket_wrapper<LkT>::socket_wrapper(boost::asio::io_context& io_context)
	: socket_(io_context) {
}

template<class LkT>
inline socket_wrapper<LkT>::socket_wrapper(socket_t&& socket)
	: socket_(std::move(socket)) {
}

template<class LkT>
inline void
socket_wrapper<LkT>::connect(boost::asio::ip::tcp::endpoint const& endpoint) {
	socket_.connect(endpoint);
}

template<class LkT>
template<class MsgT>
inline void
socket_wrapper<LkT>::write(MsgT const& message) noexcept(false) {
	boost::system::error_code io_error;
	const write_lock_t lk(mutex_);
	if constexpr(MsgT::has_static_size) {
		using raw_buff_t= std::byte const[sizeof(MsgT)];
		DEBUG_ASSERT(message.length() == sizeof(MsgT));
		[[maybe_unused]] const std::size_t bytes_written= boost::asio::write(socket_, boost::asio::buffer(reinterpret_cast<raw_buff_t&>(message)), io_error);
		if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
			return;	 // Connection closed cleanly by peer.
		} else if(io_error) [[unlikely]] {
			BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
		}
		DEBUG_ASSERT(bytes_written == sizeof(MsgT));
	} else {
		[[maybe_unused]] const std::size_t bytes_written= boost::asio::write(socket_, boost::asio::buffer(reinterpret_cast<std::byte const*>(&message), message.length()), io_error);
		if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
			return;	 // Connection closed cleanly by peer.
		} else if(io_error) [[unlikely]] {
			BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
		}
		DEBUG_ASSERT(bytes_written <= sizeof(MsgT));
		DEBUG_ASSERT(bytes_written == message.length());
	}
}

template<class LkT>
template<class V, std::size_t N>
inline void
socket_wrapper<LkT>::write(std::array<V, N> const& message) noexcept(false) {
	boost::system::error_code io_error;
	const write_lock_t lk(mutex_);
	[[maybe_unused]] const std::size_t bytes_written= boost::asio::write(socket_, boost::asio::buffer(message), io_error);
	if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
		return;	 // Connection closed cleanly by peer.
	} else if(io_error) [[unlikely]] {
		BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
	}
	DEBUG_ASSERT(bytes_written == sizeof(V) * N);
}

template<class LkT>
template<class MsgT>
inline bool
socket_wrapper<LkT>::read(MsgT& dest) noexcept(false) {
	boost::system::error_code io_error;
	if constexpr(MsgT::has_static_size) {
		using raw_buff_t= std::byte[sizeof(MsgT)];
		[[maybe_unused]] const std::size_t bytes_read= boost::asio::read(socket_, boost::asio::buffer(reinterpret_cast<raw_buff_t&>(dest)), io_error);
		if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
			return true;	// Connection closed cleanly by peer.
		} else if(io_error) [[unlikely]] {
			BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
		}
		DEBUG_ASSERT(bytes_read > 0);
		DEBUG_ASSERT(bytes_read == sizeof(MsgT));
		DEBUG_ASSERT(dest.length() <= sizeof(MsgT));
	} else {
		constexpr const std::size_t header_t_sz= MsgT::header_t_size;
		[[maybe_unused]] const std::size_t bytes_read= boost::asio::read(socket_, boost::asio::buffer(reinterpret_cast<std::byte*>(&dest), header_t_sz), io_error);
		if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
			return true;	// Connection closed cleanly by peer.
		} else if(io_error) [[unlikely]] {
			BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
		}
		DEBUG_ASSERT(bytes_read > 0);
		DEBUG_ASSERT(bytes_read == header_t_sz);
		auto const* hdr= reinterpret_cast<typename MsgT::Header_t const*>(&dest);
		const std::size_t length= hdr->length();
		if(length >= header_t_sz && length <= sizeof(MsgT)) {
			const std::size_t body_size= length - header_t_sz;
			[[maybe_unused]] const std::size_t bytes_read_body= boost::asio::read(socket_, boost::asio::buffer(reinterpret_cast<std::byte*>(&dest) + header_t_sz, body_size), io_error);
			if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
				return true;	// Connection closed cleanly by peer.
			} else if(io_error) [[unlikely]] {
				BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
			}
			DEBUG_ASSERT(bytes_read_body == body_size);
		} else {
			return true;
		}
	}
	DEBUG_ASSERT(dest.is_valid());
	return false;
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
socket_wrapper<LkT>::read(V (&dest)[SrcSz]) noexcept(false) {
	boost::system::error_code io_error;
	[[maybe_unused]] const std::size_t bytes_read= boost::asio::read(socket_, boost::asio::buffer(dest), io_error);
	if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
		return true;	// Connection closed cleanly by peer.
	} else if(io_error) [[unlikely]] {
		BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
	}
	DEBUG_ASSERT(bytes_read == sizeof(V) * SrcSz);
	return false;
}

template<class LkT>
template<class MsgDetails, class V, std::size_t N>
inline bool
socket_wrapper<LkT>::read(std::array<V, N>& buff) noexcept(false) {
	using msg_details_t= MsgDetails;

	boost::system::error_code io_error;
	DEBUG_ASSERT(socket_.is_open());
	BOOST_MPL_ASSERT_RELATION(msg_details_t::max_msg_size, >=, msg_details_t::header_t_size);
	BOOST_MPL_ASSERT_RELATION(msg_details_t::max_msg_size, <=, N * sizeof(V));
	[[maybe_unused]] std::size_t bytes_read= boost::asio::read(socket_, boost::asio::buffer(buff, msg_details_t::header_t_size), io_error);
	if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) [[unlikely]] {
		return true;	// Connection closed cleanly by peer.
	} else if(io_error) [[unlikely]] {
		BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
	}
	DEBUG_ASSERT(bytes_read <= N);
	DEBUG_ASSERT(bytes_read <= msg_details_t::max_msg_size);
	DEBUG_ASSERT(bytes_read == msg_details_t::header_t_size);
	auto const* hdr= reinterpret_cast<typename msg_details_t::Header_t const*>(buff.data());
	const std::size_t length= hdr->length();
	if(length >= msg_details_t::header_t_size && length <= msg_details_t::max_msg_size) [[likely]] {
		const std::size_t body_size= length - msg_details_t::header_t_size;
		if(body_size > 0) {
			bytes_read= boost::asio::read(socket_, boost::asio::buffer(&*std::next(buff.begin(), msg_details_t::header_t_size), body_size), io_error);
			if(io_error == boost::asio::error::eof || io_error == boost::asio::error::connection_reset || io_error == boost::asio::error::broken_pipe) {
				return true;	// Connection closed cleanly by peer.
			} else if(io_error) {
				BOOST_THROW_EXCEPTION(boost::system::system_error(io_error));	 // Some other error.
			}
			DEBUG_ASSERT(bytes_read <= msg_details_t::max_msg_size);
			DEBUG_ASSERT(bytes_read == body_size);
		}
		DEBUG_ASSERT(hdr->is_valid());
		return false;
	} else [[unlikely]] {
		return true;
	}
}

template<class LkT>
inline bool
socket_wrapper<LkT>::is_open() const {
	return socket_.is_open();
}

template<class LkT>
inline void
socket_wrapper<LkT>::close() {
	socket_.set_option(boost::asio::socket_base::linger(true, 1));
	boost::system::error_code ec;
	socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
}

template<class LkT>
inline std::string
socket_wrapper<LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<< "socket_=" << const_cast<boost::asio::ip::tcp::socket&>(socket_).native_handle() << ", local IP: " << local_ip();
	return ss.str();
}

template<class LkT>
inline std::string
socket_wrapper<LkT>::local_ip() const noexcept(false) {
	const boost::asio::ip::address addr= socket_.local_endpoint().address();
	return addr.to_string();
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, socket_wrapper<LkT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}}}}
