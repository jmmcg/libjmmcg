#ifndef LIBJMMCG_CORE_CSV_ITERATOR_HPP
#define LIBJMMCG_CORE_CSV_ITERATOR_HPP

/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <istream>
#include <iterator>
#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 * Taken from "Generic Programming and the STL" by M.H.Austern.
 */
class csv_iterator {
public:
	using iterator_category= std::input_iterator_tag;
	using value_type= std::string;
	using difference_type= ptrdiff_t;
	using pointer= std::string const*;
	using reference= std::string const&;

	static inline constexpr const char default_delimiter= ',';

	[[nodiscard]] explicit csv_iterator(char delim= default_delimiter) noexcept(true);
	[[nodiscard]] explicit csv_iterator(std::string const& data, char delim= default_delimiter) noexcept(false);

	reference operator*() const noexcept(true);
	pointer operator->() const noexcept(true);

	csv_iterator operator++() noexcept(false);
	[[nodiscard]] csv_iterator operator++(int) noexcept(false);

	bool operator==(csv_iterator const& i) const noexcept(true);
	bool operator!=(csv_iterator const& i) const noexcept(true);

private:
	std::string const* data_{nullptr};
	char delim_;
	std::size_t pos_{};
	bool valid_{};
	std::string field_{};

	void read() noexcept(false);
};

}}

#include "csv_iterator_impl.hpp"

#endif
