#ifndef LIBJMMCG_CORE_SHARED_MEM_HPP
#define LIBJMMCG_CORE_SHARED_MEM_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A class to wrap POSIX (inter-process) shared-memory.
/**
 * \see boost::interprocess::shared_memory_object, boost::interprocess::mapped_region
 */
class shared_mem;

}}

#ifdef WIN32
#elif defined(__unix__)
#	include "../unix/shared_mem.hpp"
#endif

#endif
