#ifndef LIBJMMCG_CORE_TUPLE_HPP
#define LIBJMMCG_CORE_TUPLE_HPP
/******************************************************************************
** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <tuple>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Concatenate an arbitrary number of tuples together to make a singe tuple as a compile-time meta-function.
/**
 * The T1-elements of the second tuple are concatenated after the T0-elements of the first tuple to form the resultant tuple, and so-forth. Order of the elements is preserved.
 * Compile-time algorithmic complexity: O(T0+T1+...)
 *
 * \see std::tuple_cat()
 */
template<class...>
struct tuple_cat;
template<class... A0s, class... A1s>
struct tuple_cat<std::tuple<A0s...>, std::tuple<A1s...>> {
	using type= std::tuple<A0s..., A1s...>;
};
template<class... A0s, class... A1s, class... Ts>
struct tuple_cat<std::tuple<A0s...>, std::tuple<A1s...>, Ts...> {
private:
	using rhs= typename tuple_cat<std::tuple<A1s...>, Ts...>::type;
	template<class...>
	struct detuple;
	template<class... T1s, class... T2s>
	struct detuple<std::tuple<T1s...>, T2s...> {
		using type= std::tuple<T2s..., T1s...>;
	};

public:
	using type= typename detuple<rhs, A0s...>::type;
};

}}

#endif
