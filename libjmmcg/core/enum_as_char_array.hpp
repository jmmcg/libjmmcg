#ifndef LIBJMMCG_CORE_ENUM_AS_CHAR_ARRAY_HPP
#define LIBJMMCG_CORE_ENUM_AS_CHAR_ARRAY_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, libjmmcg@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/mpl/assert.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <array>
#include <climits>
#include <cstdint>
#include <type_traits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Construct enum-tag values from a sequence of chars, and extract from the tag-value the string that was encoded.
namespace enum_tags {

/// A function to convert a string-literal or C-style string or array of chars into one of these magic enum-tags-as-a-string.
/**
 * \see LIBJMMCG_MAKE_ENUM_TAG_VALUES
 */
template<class EnumT, std::size_t N>
	requires((N - 1) <= sizeof(EnumT))
constexpr inline EnumT
convert(const char (&str)[N]) noexcept(true) {
	using u_t= typename std::underlying_type<EnumT>::type;

	std::size_t n= N;
	u_t ret= u_t{};
	std::size_t const ignore_any_terminal_NULL= (str[N - 1] == '\0' ? 1U : 0U);
	while(n > ignore_any_terminal_NULL) {
		std::size_t const orig_n= n;
		ret|= static_cast<u_t>(static_cast<u_t>(str[N - orig_n]) << (CHAR_BIT * (orig_n - 1 - ignore_any_terminal_NULL)));
		--n;
	}
	DEBUG_ASSERT(ret != u_t{});
	return static_cast<EnumT>(ret);
}

/// A function to convert, even faster, a correctly sized array of chars into one of these magic enum-tags-as-a-string.
/**
 * \param	s	A correctly sized array of chars means that the number of characters must exactly match the number of chars in the size of the underlying type of the enum.
 */
template<class EnumT>
constexpr inline EnumT
convert(std::array<char, sizeof(EnumT)> const& str) noexcept(true) {
	using u_t= typename std::underlying_type<EnumT>::type;

	union conv_t {
		u_t ret{};
		std::array<char, sizeof(EnumT)> str;
	} conv;

	DEBUG_ASSERT(*str.rbegin() != '\0');
	conv.str= str;
	DEBUG_ASSERT(conv.ret != u_t{});
	std::reverse(conv.str.begin(), conv.str.end());
	return static_cast<EnumT>(conv.ret);
}

namespace mpl {

namespace private_ {

template<char... Chars>
	requires(sizeof...(Chars) > 0)
struct select_underlying_type {
	enum : std::size_t {
		size= ((Chars != '\0' ? 1 : 0) + ...),
		size_of_Chars= sizeof...(Chars)
	};

	BOOST_MPL_ASSERT_RELATION(size, >, 0);
	BOOST_MPL_ASSERT_RELATION(size, <=, size_of_Chars);
	static_assert(size_of_Chars == size || size_of_Chars == (size + 1));
	using type=
		typename std::conditional<
			(size <= 1),
			std::uint8_t,
			typename std::conditional<
				(size <= 2),
				std::uint16_t,
				typename std::conditional<
					(size <= 4),
					std::uint32_t,
					typename std::conditional<
						(size <= 8),
						std::uint64_t,
						void>::type>::type>::type>::type;
};

template<
	auto EnumTag,
	typename std::underlying_type<decltype(EnumTag)>::type i= (static_cast<typename std::underlying_type<decltype(EnumTag)>::type>(EnumTag) >> CHAR_BIT),
	bool is_digit= (static_cast<bool>(i))>
	requires(std::is_enum<decltype(EnumTag)>::value)
struct count_digits {
	enum : std::size_t {
		value= 1U + count_digits<EnumTag, (i >> CHAR_BIT)>::value
	};
};

template<
	auto EnumTag,
	typename std::underlying_type<decltype(EnumTag)>::type i>
struct count_digits<EnumTag, i, false> {
	enum : std::size_t {
		value= 1
	};
};

}

/// Convert the chars into a suitable enum-tag.
/**
	\see to_array
*/
template<char... Chars>
	requires(sizeof...(Chars) > 0)
class to_tag {
public:
	using element_type= typename private_::select_underlying_type<Chars...>::type;

	enum : std::size_t {
		size= ((Chars != '\0' ? 1 : 0) + ...),
		size_of_Chars= sizeof...(Chars)
	};

	BOOST_MPL_ASSERT_RELATION(size, >, 0);
	BOOST_MPL_ASSERT_RELATION(size, <=, size_of_Chars);
	static_assert(
		size_of_Chars == 1 ? sizeof(element_type) == sizeof(std::uint8_t)
								 : size <= sizeof(element_type));
	static_assert(
		size == 1 ? sizeof(element_type) == sizeof(std::uint8_t)
					 : size <= sizeof(element_type));
	static_assert(
		size_of_Chars == 2 && size == 1 ? sizeof(element_type) == sizeof(std::uint8_t)
												  : size <= sizeof(element_type));
	static_assert(
		size_of_Chars == 2 && size == 2 ? size == sizeof(std::uint16_t)
												  : size <= sizeof(element_type));

private:
	[[nodiscard]] static consteval element_type put_char_in_place(char v, std::size_t& n) noexcept(true) {
		constexpr std::size_t total_chars= sizeof...(Chars);
		constexpr auto total_chars_no_NULLs= (std::size_t(Chars != '\0') + ...);
		BOOST_MPL_ASSERT_RELATION(total_chars_no_NULLs, <=, total_chars);
		constexpr std::size_t deduct_one_as_has_terminal_NULL= total_chars - total_chars_no_NULLs;
		BOOST_MPL_ASSERT_RELATION(deduct_one_as_has_terminal_NULL, <=, 1);
		std::size_t n_has_a_NULL= (n >= 2 ? n - 1 - deduct_one_as_has_terminal_NULL : n - 1);
		--n;
		return static_cast<element_type>(static_cast<element_type>(v) << (CHAR_BIT * n_has_a_NULL));
	}

	[[nodiscard]] static consteval element_type as_tag_value() noexcept(true) {
		std::size_t n= sizeof...(Chars);
		return element_type(
			((put_char_in_place(Chars, n)) | ...));
	}

public:
	static inline constexpr element_type value= as_tag_value();
};

/// Convert the suitably-created enum-tag into a char array.
/**
	\see to_tag
*/
template<auto EnumTag>
	requires(std::is_enum<decltype(EnumTag)>::value)
class to_array {
public:
	enum : std::size_t {
		size= private_::count_digits<EnumTag>::value	  ///< The size of the extracted string, excluding the terminal NULL.
	};

	static_assert(size <= sizeof(std::underlying_type_t<decltype(EnumTag)>));

	enum : typename std::underlying_type<decltype(EnumTag)>::type {
		value_as_int= static_cast<typename std::underlying_type<decltype(EnumTag)>::type>(EnumTag)
	};

	using const_element_type= const std::array<char, size + 1>;	  ///< The type of the const char-array of the extracted string.
	using element_type= std::array<char, size + 1>;	  ///< The type of the char-array of the extracted string.
	using element_type_no_null= std::array<char, size>;	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const auto tag= EnumTag;	///< The enum-tag associated the with the encoded string.

private:
	static consteval element_type_no_null to_string_no_null() noexcept(true) {
		element_type_no_null ret;
		for(std::size_t i= 0; i < ret.size(); ++i) {
			ret[(ret.size() - 1 - i)]= static_cast<element_type_no_null::value_type>(static_cast<std::underlying_type_t<decltype(EnumTag)>>(EnumTag) >> (i * CHAR_BIT));
			DEBUG_ASSERT(ret[(ret.size() - 1 - i)] != '\0');
		}
		DEBUG_ASSERT(*ret.rbegin() != '\0');
		return ret;
	}

	static consteval element_type to_string() noexcept(true) {
		element_type ret;
		for(std::size_t i= 0; i < ret.size() - 1; ++i) {
			ret[(ret.size() - 2 - i)]= static_cast<element_type::value_type>(static_cast<std::underlying_type_t<decltype(EnumTag)>>(EnumTag) >> (i * CHAR_BIT));
		}
		*ret.rbegin()= '\0';
		return ret;
	}

public:
	static inline constexpr const element_type_no_null value_no_null= to_string_no_null();
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value= to_string();
};

/**
	\test Verify the operation of the enum-as-char-arrays.
*/
namespace tests {

enum class enum_tags_as_strs0 : std::uint8_t {
	e1= to_tag<'0'>::value,
	e2= to_tag<'1', '\0'>::value
};

static_assert((to_array<enum_tags_as_strs0::e1>::tag) == enum_tags_as_strs0::e1, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs0::e1>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs0::e1>::value[0]), ==, '0');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs0::e2>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs0::e2>::value[0]), ==, '1');

enum class enum_tags_as_strs1 : std::uint16_t {
	e1= to_tag<'0', '0'>::value,
	e2= to_tag<'1', '2'>::value,
	e3= to_tag<'3'>::value,
	e4= to_tag<'4', '\0'>::value
};

static_assert((to_array<enum_tags_as_strs1::e1>::tag) == enum_tags_as_strs1::e1, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e1>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e1>::value[0]), ==, '0');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e1>::value[1]), ==, '0');

static_assert((to_array<enum_tags_as_strs1::e2>::tag) == enum_tags_as_strs1::e2, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e2>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e2>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e2>::value[1]), ==, '2');

static_assert((to_array<enum_tags_as_strs1::e3>::tag) == enum_tags_as_strs1::e3, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e3>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e3>::value[0]), ==, '3');

static_assert((to_array<enum_tags_as_strs1::e4>::tag) == enum_tags_as_strs1::e4, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e4>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1::e4>::value[0]), ==, '4');

enum class enum_tags_as_strs2 : uint64_t {
	e9= to_tag<'9', '8', '7', '6', '5', '4', '3', '\0'>::value,
	e8= to_tag<'2', '.', '7', '1', '8', '2', '8', '1'>::value,
	e7= to_tag<'1', '2', '3', '4', '5', '6', '7'>::value,
	e6= to_tag<'1', '2', '3', '4', '5', '6'>::value,
	e5= to_tag<'1', '2', '3', '4', '5'>::value,
	e4= to_tag<'1', '2', '3', '4'>::value,
	e3= to_tag<'3', '1', '4'>::value,
	e2= to_tag<'3', '1'>::value,
	e1= to_tag<'3'>::value
};

static_assert((to_array<enum_tags_as_strs2::e9>::tag) == enum_tags_as_strs2::e9, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::size), ==, 7);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[0]), ==, '9');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[1]), ==, '8');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[2]), ==, '7');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[3]), ==, '6');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[4]), ==, '5');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[5]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e9>::value[6]), ==, '3');

static_assert((to_array<enum_tags_as_strs2::e8>::tag) == enum_tags_as_strs2::e8, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::size), ==, 8);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[0]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[1]), ==, '.');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[2]), ==, '7');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[3]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[4]), ==, '8');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[5]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[6]), ==, '8');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e8>::value[7]), ==, '1');

static_assert((to_array<enum_tags_as_strs2::e7>::tag) == enum_tags_as_strs2::e7, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::size), ==, 7);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[4]), ==, '5');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[5]), ==, '6');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e7>::value[6]), ==, '7');

static_assert((to_array<enum_tags_as_strs2::e6>::tag) == enum_tags_as_strs2::e6, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::size), ==, 6);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[4]), ==, '5');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e6>::value[5]), ==, '6');

static_assert((to_array<enum_tags_as_strs2::e5>::tag) == enum_tags_as_strs2::e5, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::size), ==, 5);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e5>::value[4]), ==, '5');

static_assert((to_array<enum_tags_as_strs2::e4>::tag) == enum_tags_as_strs2::e4, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e4>::size), ==, 4);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e4>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e4>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e4>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e4>::value[3]), ==, '4');

static_assert((to_array<enum_tags_as_strs2::e3>::tag) == enum_tags_as_strs2::e3, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e3>::size), ==, 3);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e3>::value[0]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e3>::value[1]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e3>::value[2]), ==, '4');

static_assert((to_array<enum_tags_as_strs2::e2>::tag) == enum_tags_as_strs2::e2, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e2>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e2>::value[0]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e2>::value[1]), ==, '1');

static_assert((to_array<enum_tags_as_strs2::e1>::tag) == enum_tags_as_strs2::e1, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e1>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2::e2>::value[0]), ==, '3');

}

}

}
}}

#endif
