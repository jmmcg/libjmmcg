/******************************************************************************
** Copyright © 20017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "hp_timer.hpp"

#include "logging.hpp"

#include <iomanip>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class scoped_thread_settings final {
public:
	using api_threading_traits= ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>;

	scoped_thread_settings() noexcept(true) try
		: old_priority(api_threading_traits::get_kernel_priority(api_threading_traits::get_current_thread())) {
		api_threading_traits::set_kernel_priority(api_threading_traits::get_current_thread(), api_threading_traits::api_params_type::priority_type::time_critical);
	} catch(...) {
		LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Exception caught and ignored: " << boost::current_exception_diagnostic_information();
	}

	~scoped_thread_settings() noexcept(true) {
		try {
			api_threading_traits::set_kernel_priority(api_threading_traits::get_current_thread(), old_priority);
		} catch(...) {
			LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Exception caught and ignored: " << boost::current_exception_diagnostic_information();
		}
	}

private:
	api_threading_traits::api_params_type::priority_type const old_priority;
};

std::ostream&
cpu_timer::private_::operator<<(std::ostream& s, cpu_timer::private_::ticks_per_microsec_details const& tpm) {
	s << "min=" << std::setprecision(5) << tpm.min << "MHz, mean=" << tpm.mean << "MHz, mean-ave. dev.=" << std::setprecision(2) << 100 * tpm.mean_average_dev / tpm.mean << "%, max=" << std::setprecision(5) << tpm.max << "Mhz";
	return s;
}

cpu_timer::private_::ticks_per_microsec_details
cpu_timer::private_::get_mean_ticks_per_microsec() noexcept(false) {
	constexpr const unsigned num_reps= 10u;
	using samples_t= std ::array<double, num_reps>;
	[[maybe_unused]] const scoped_thread_settings settings;
	const auto ticks_per_microsec= []() {
		element_type e{};
		const auto start_microsec= std::chrono::high_resolution_clock::now();
		{
			const out_of_order time(e);
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		const auto end_microsec= std::chrono::high_resolution_clock::now();
		DEBUG_ASSERT(end_microsec > start_microsec);
		const std::chrono::duration<double, std::micro> microsec_dur= end_microsec - start_microsec;
		return static_cast<double>(e) / microsec_dur.count();
	};
	samples_t samples;
	double min{std::numeric_limits<double>::max()};
	double mean{};
	double max{std::numeric_limits<double>::min()};
	for(unsigned i= 0; i < num_reps; ++i) {
		samples[i]= ticks_per_microsec();
		min= std::min(min, samples[i]);
		if(i == 0) {
			mean= samples[i];
		} else {
			mean+= samples[i];
		}
		max= std::max(max, samples[i]);
	}
	mean/= num_reps;
	const double mean_average_dev= std::accumulate(
												 samples.begin(),
												 samples.end(),
												 double{},
												 [&mean](auto const acc, auto const sample) {
													 return acc + std::abs(sample - mean);
												 })
											 / num_reps;
	return ticks_per_microsec_details{min, mean, max, mean_average_dev};
}

}}
