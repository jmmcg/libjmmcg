#ifndef LIBJMMCG_CORE_encode_as_sha256_hpp
#define LIBJMMCG_CORE_encode_as_sha256_hpp

/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <fmt/format.h>

#include <array>
#include <chrono>
#include <iomanip>
#include <numeric>
#include <sstream>
#include <string>
#include <string_view>

#include <openssl/sha.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class nonce_using_epoch {
public:
	[[nodiscard]] std::string_view get() noexcept(true);

private:
	using container_type= std::array<char, std::numeric_limits<std::chrono::milliseconds::rep>::digits10>;

	container_type buffer_{};
};

[[nodiscard]] std::string
encode_as_sha256(std::string_view str) noexcept(true);

}}

#include "encode_as_sha256_impl.hpp"

#endif
