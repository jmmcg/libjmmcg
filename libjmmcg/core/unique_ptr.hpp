#ifndef LIBJMMCG_CORE_UNIQUE_PTR_HPP
#define LIBJMMCG_CORE_UNIQUE_PTR_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "deleter.hpp"
#include "thread_api_traits.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A unique pointer-type that has threading specified as a trait.
/**
	I don't use boost::scoped_ptr nor std::unique_ptr as they are insufficiently flexible for my purposes: the multi-threading model they use is not a trait, and the deleter is part of the type, which won't work in a container in the way I need.
	This class uses a lock-free implementation ultimately based upon std::atomic.

	\see boost::scoped_ptr, std::unique_ptr, libjmmcg::unique_ptr
*/
template<
	class V,	  ///< The type to be controlled, which would also specify the threading model which, by default, is single-threaded, so no cost would be paid.
	class LkT	///< The lock_traits that provide the (potentially) atomic operations to be used upon pointers to the value_type.
	>
class unique_ptr final {
public:
	using value_type= V;	  ///< A convenience typedef to the type to be controlled.
	using element_type= value_type;	 ///< A convenience typedef to the type to be controlled.
	using lock_traits= LkT;	  ///< This does not have to be the same as the element_type, as it may not contain any lock_traits, or we may want the flexibility to deal with the type differently in this case.
	/// The (potentially) lock-free pointer type.
	/**
		We need the operations on the contained pointer (to the managed object) to be atomic because the move operations might occur on more than one thread, and therefore there is a race condition on non-SMP architectures, which is avoided if (in the implementation) the operations on the contained pointer are atomic.
	*/
	using atomic_ptr_t= typename lock_traits::template atomic<value_type*>;
	/// Make sure the correct object-deletion mechanism is used.
	using deleter_t= typename value_type::deleter_t;
	/**
		If you get a compilation issue here, check you aren't using std::default_delete, but libjmmcg::default_delete instead.

		\see libjmmcg::default_delete
	*/
	using no_deletion= noop_dtor<typename value_type::deleter_t::element_type>;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_ptr_t::memory_access_mode;

	/**
		Release the contained object from control.
	*/
	atomic_ptr_t __fastcall release() noexcept(true);

	constexpr __stdcall unique_ptr() noexcept(true) {}

	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall constexpr unique_ptr(value_type* ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero. The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall unique_ptr(V1* ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall unique_ptr(atomic_ptr_t&& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	template<class V1, template<class> class At>
	[[nodiscard]] __stdcall unique_ptr(At<V1*>&& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall constexpr unique_ptr(std::unique_ptr<value_type, deleter_t>&& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero. The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall unique_ptr(std::unique_ptr<V1, typename V1::deleter_t>&& ptr) noexcept(true);
	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	[[nodiscard]] explicit __stdcall unique_ptr(unique_ptr<V2, LkT2>&& s) noexcept(true);

	[[nodiscard]] constexpr unique_ptr(unique_ptr&& s) noexcept(true);

	/**
		\see reset()
	*/
	__stdcall ~unique_ptr() noexcept(true);

	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	void
	operator=(unique_ptr<V2, LkT2>&& s) noexcept(true);

	void
	operator=(unique_ptr&& s) noexcept(true);

	void __fastcall reset() noexcept(true);

	constexpr bool __fastcall
	operator==(const unique_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator!=(const unique_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator<(const unique_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator>(const unique_ptr& s) const noexcept(true);
	explicit constexpr
	operator bool() const noexcept(true);

	constexpr const atomic_ptr_t& __fastcall get() const noexcept(true);
	atomic_ptr_t& __fastcall get() noexcept(true);
	constexpr const value_type& __fastcall
	operator*() const noexcept(true);
	value_type& __fastcall
	operator*() noexcept(true);
	constexpr value_type const* __fastcall
	operator->() const noexcept(true);
	value_type* __fastcall
	operator->() noexcept(true);

	void swap(unique_ptr& s) noexcept(true);
	template<class V1>
	__fastcall void
	swap(unique_ptr<V1, LkT>& s) noexcept(true);
	template<class V2, class LkT2>
	__fastcall void
	swap(unique_ptr<V2, LkT2>& s) noexcept(true);

	tstring to_string() const noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend tostream& __fastcall operator<<(tostream& os, unique_ptr const& ptr) noexcept(false) {
		os << ptr.to_string();
		return os;
	}

private:
	template<class, class>
	friend class unique_ptr;
	atomic_ptr_t data_;
};

}}

#include "unique_ptr_impl.hpp"

#endif
