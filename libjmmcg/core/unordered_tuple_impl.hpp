/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, libjmmcg@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class ... MappedTypes>
inline
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::unordered_tuple() noexcept((noexcept(MappedTypes{}) && ...))
: mapped_cont{{}} {
	if constexpr ((noexcept(MappedTypes{}) && ...)) {
		(
			[this]() {
				void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
				new (ptr) MappedTypes();
			}()
			, ...
		);
	} else {
		try {
			(
				[this]() {
					void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
					new (ptr) MappedTypes();
				}()
				, ...
			);
		} catch (...) {
			(
				[this]() {
					void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
					MappedTypes * obj_ctord=reinterpret_cast<MappedTypes *>(ptr);
					if (obj_ctord) {
						obj_ctord->~MappedTypes();
						obj_ctord=nullptr;
					}
				}()
				, ...
			);
			throw;
		}
	}
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class ... MappedTypes>
template<class... MappedCtorArgs, typename std::enable_if<(sizeof...(MappedCtorArgs)>1), bool>::type> inline
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::unordered_tuple(MappedCtorArgs &&... args) noexcept((noexcept(MappedTypes{args}) && ...))
: mapped_cont{{}} {
	BOOST_MPL_ASSERT_RELATION(size, ==, sizeof...(MappedCtorArgs));
	if constexpr ((noexcept(MappedTypes{args}) && ...)) {
		(
			[this](auto &&arg) {
				void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
				new (ptr) MappedTypes(std::forward<MappedCtorArgs>(arg));
			}(args)
			, ...
		);
	} else {
		try {
			(
				[this](auto &&arg) {
					void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
					new (ptr) MappedTypes(std::forward<MappedCtorArgs>(arg));
				}(args)
				, ...
			);
		} catch (...) {
			(
				[this]() {
					void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
					MappedTypes * obj_ctord=reinterpret_cast<MappedTypes *>(ptr);
					if (obj_ctord) {
						obj_ctord->~MappedTypes();
						obj_ctord=nullptr;
					}
				}()
				, ...
			);
			throw;
		}
	}
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class ... MappedTypes> inline
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::~unordered_tuple() noexcept(true) {
	(
		[this]() {
			void *ptr=mapped_cont.data()+mapped_index_colln_t::find(ExtractHash<MappedTypes>::value);
			MappedTypes * obj_ctord=reinterpret_cast<MappedTypes *>(ptr);
			if (obj_ctord) {
				obj_ctord->~MappedTypes();
				obj_ctord=nullptr;
			}
		}()
		, ...
	);
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class ... MappedTypes> inline
constexpr typename unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::mapped_type const &
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::operator[](const key_type mic) const noexcept(true) {
	std::size_t const offset=mapped_index_colln_t::find(mic);
	DEBUG_ASSERT(offset<mapped_cont.size());
	mapped_type const &ret=reinterpret_cast<mapped_type const &>(mapped_cont[offset]);
	return ret;
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class ... MappedTypes> inline
constexpr typename unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::mapped_type &
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::operator[](const key_type mic) noexcept(true) {
	std::size_t const offset=mapped_index_colln_t::find(mic);
	DEBUG_ASSERT(offset<mapped_cont.size());
	mapped_type &ret=reinterpret_cast<mapped_type &>(mapped_cont[offset]);
	return ret;
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class... MappedTypes>
template<class T, class BinaryOperation>
inline T
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::accumulate(T init, BinaryOperation op) const noexcept(false) {
	auto accum{init};
	(
		[this, op, &accum]() {
			mapped_type const &mapped_obj=this->operator[](ExtractHash<MappedTypes>::value);
			accum=op(accum, mapped_obj);
		}(),
		...
	);
	return accum;
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class... MappedTypes>
template<class Generator>
inline void
unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>::generate(Generator g) noexcept(false) {
	(g(this->operator[](ExtractHash<MappedTypes>::value)), ...);
}

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class... MappedTypes>
inline std::ostream &
operator<<(std::ostream &os, unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...> const &t) noexcept(false) {
	using unordered_tuple_t=unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...>;

	os<<t.accumulate(
			"Size="+libjmmcg::tostring(unordered_tuple_t::size)+", [",
			[](auto const &v, auto const &rhs) {
				return v+rhs.to_string()+", ";
			}
		)<<"]";
	return os;
}

} }
