/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

template<generic_traits::api_type::element_type API, class Mdl>
inline jthread<API, Mdl>::mutator::mutator(mutator_t m)
	: mutator_(std::move(m)) {
}

template<generic_traits::api_type::element_type API, class Mdl>
inline void
jthread<API, Mdl>::mutator::operator()(std::atomic_flag& exit_requested, set_thread_name_thread_worker_context&) noexcept(false) {
	mutator_.operator()(exit_requested);
}

template<generic_traits::api_type::element_type API, class Mdl>
inline jthread<API, Mdl>::jthread(typename mutator::mutator_t proc_fn) noexcept(false)
	: thread_(set_thread_name_thread_worker_context([](set_thread_name_thread_worker_context::thread_t&) {}), mutator(std::move(proc_fn))) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline jthread<API, Mdl>::jthread(std::function<void()> proc_fn) noexcept(false)
	: thread_(
		set_thread_name_thread_worker_context([](set_thread_name_thread_worker_context::thread_t&) {}),
		mutator(
			[fn= std::move(proc_fn)](std::atomic_flag& exit_requested) {
				try {
					fn.operator()();
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
				} catch(...) {
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
					throw;
				}
			})) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline jthread<API, Mdl>::jthread(typename thread_traits::thread_name_t const& name, typename thread_traits::api_params_type::priority_type const cpu_priority, typename thread_traits::api_params_type::processor_mask_type const mask, typename mutator::mutator_t proc_fn) noexcept(false)
	: thread_(
		set_thread_name_thread_worker_context([name, cpu_priority, mask](set_thread_name_thread_worker_context::thread_t& thread) {
			thread.set_name(name);
			thread.kernel_priority(cpu_priority);
			thread.kernel_affinity(mask);
		}),
		mutator(std::move(proc_fn))) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline jthread<API, Mdl>::jthread(typename thread_traits::thread_name_t const& name, typename thread_traits::api_params_type::priority_type const cpu_priority, typename thread_traits::api_params_type::processor_mask_type const mask, std::function<void()> proc_fn) noexcept(false)
	: thread_(
		set_thread_name_thread_worker_context([name, cpu_priority, mask](set_thread_name_thread_worker_context::thread_t& thread) {
			thread.set_name(name);
			thread.kernel_affinity(mask);
			thread.kernel_priority(cpu_priority);
		}),
		mutator(
			[fn= std::move(proc_fn)](std::atomic_flag& exit_requested) {
				try {
					fn.operator()();
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
				} catch(...) {
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
					throw;
				}
			})) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
template<std::size_t NameSize>
	requires(NameSize <= jthread<API, Mdl>::thread_traits::max_thread_name_length)
inline jthread<API, Mdl>::jthread(char const (&name)[NameSize], typename mutator::mutator_t proc_fn) noexcept(false)
	: thread_(
		set_thread_name_thread_worker_context([name](set_thread_name_thread_worker_context::thread_t& thread) {
			thread.set_name(name);
		}),
		mutator(std::move(proc_fn))) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
template<std::size_t NameSize>
	requires(NameSize <= jthread<API, Mdl>::thread_traits::max_thread_name_length)
inline jthread<API, Mdl>::jthread(char const (&name)[NameSize], std::function<void()> proc_fn) noexcept(false)
	: thread_(
		set_thread_name_thread_worker_context([name](set_thread_name_thread_worker_context::thread_t& thread) {
			thread.set_name(name);
		}),
		mutator(
			[fn= std::move(proc_fn)](std::atomic_flag& exit_requested) {
				try {
					fn.operator()();
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
				} catch(...) {
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
					throw;
				}
			})) {
	thread_.create_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline bool
jthread<API, Mdl>::is_running() const noexcept(true) {
	return thread_.is_running();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline
	typename jthread<API, Mdl>::thread_traits::api_params_type::priority_type
	jthread<API, Mdl>::kernel_priority() const noexcept(false) {
	return thread_.kernel_priority();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline void
jthread<API, Mdl>::kernel_priority(const typename thread_traits::api_params_type::priority_type priority) noexcept(false) {
	thread_.kernel_priority(priority);
}

template<generic_traits::api_type::element_type API, class Mdl>
inline
	typename jthread<API, Mdl>::thread_traits::api_params_type::processor_mask_type
	jthread<API, Mdl>::kernel_affinity() const noexcept(false) {
	return thread_.kernel_affinity();
}

template<generic_traits::api_type::element_type API, class Mdl>
inline void
jthread<API, Mdl>::kernel_affinity(const typename thread_traits::api_params_type::processor_mask_type& mask) noexcept(false) {
	thread_.kernel_affinity(mask);
}

template<generic_traits::api_type::element_type API, class Mdl>
inline void
jthread<API, Mdl>::set_name(typename thread_traits::thread_name_t const& name) noexcept(false) {
	thread_.set_name(name);
}

template<generic_traits::api_type::element_type API, typename Mdl>
template<std::size_t NameSize>
	requires(NameSize <= jthread<API, Mdl>::thread_traits::max_thread_name_length)
inline void
jthread<API, Mdl>::set_name(char const (&name)[NameSize]) noexcept(false) {
	typename thread_traits::thread_name_t name_internal{};
	std::copy_n(name, NameSize, name_internal.begin());
	set_name(std::move(name_internal));
}

}}}
