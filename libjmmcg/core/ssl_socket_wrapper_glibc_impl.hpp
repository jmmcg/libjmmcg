/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {
namespace socket {
namespace glibc {
namespace client {
namespace ssl {

template<class LkT>
class wrapper<LkT>::init_SSL_t final {
public:
	init_SSL_t()
		: settings_(OPENSSL_INIT_new()) {
		JMMCG_SYSCALL_WRAPPER("Failed to initialise SSL.", ::OPENSSL_init_ssl, OPENSSL_INIT_LOAD_SSL_STRINGS, settings_);
		JMMCG_SYSCALL_WRAPPER("Failed to initialise crypto.", ::OPENSSL_init_crypto, OPENSSL_INIT_LOAD_CRYPTO_STRINGS | OPENSSL_INIT_ADD_ALL_CIPHERS | OPENSSL_INIT_ADD_ALL_DIGESTS | OPENSSL_INIT_LOAD_CONFIG, settings_);
	}

	~init_SSL_t() {
		::OPENSSL_INIT_free(settings_);
	}

private:
	OPENSSL_INIT_SETTINGS* const settings_;
};

template<class LkT>
class wrapper<LkT>::SSL_context_wrapper final {
public:
	SSL_CTX* const ctx_;

	[[nodiscard]] SSL_context_wrapper(verify_SSL_certificates::element_type verify_SSL, std::optional<std::filesystem::path> const& filepath_to_pem) noexcept(false)
		: ctx_(JMMCG_SYSCALL_WRAPPER("Allocating a new SSL context failed.", ::SSL_CTX_new, ::TLS_client_method())) {
		JMMCG_SYSCALL_WRAPPER("Failed to configure the client to abort the handshake if certificate verification fails.", ::SSL_CTX_set_verify, ctx_, verify_SSL, nullptr);
		JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Setting the default trusted certificate store failed.", ::SSL_CTX_set_default_verify_paths, ctx_);
		::SSL_CTX_set_min_proto_version(ctx_, version_);
		if(filepath_to_pem.has_value()) {
			JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Failed to load and chain the specified PEM file into the context.", ::SSL_CTX_use_certificate_chain_file, ctx_, filepath_to_pem->c_str());
			JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Failed to load the private-key(s) specified PEM file into the context.", ::SSL_CTX_use_PrivateKey_file, ctx_, filepath_to_pem->c_str(), SSL_FILETYPE_PEM);
			JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Failed to verify the PEM file loaded into the context.", ::SSL_CTX_check_private_key, ctx_);
		}
	}

	~SSL_context_wrapper() {
		::SSL_CTX_free(ctx_);
	}
};

template<class LkT>
class wrapper<LkT>::SSL_socket_wrapper final {
public:
	SSL_socket_wrapper(SSL_context_wrapper& ctx, std::string const& hostname, std::string const& service, domain_t::element_type domain) noexcept(false)
		: ssl_(JMMCG_SYSCALL_WRAPPER("Allocating a new SSL state failed.", ::SSL_new, ctx.ctx_)) {
		BIO* const bio= create_socket_bio(hostname, service, domain);
		DEBUG_ASSERT(bio);
		JMMCG_SYSCALL_WRAPPER("Failed to set the BIO socket in the SSL context.", ::SSL_set_bio, ssl_, bio, bio);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
		::SSL_set_tlsext_host_name(ssl_, hostname.data());
#pragma GCC diagnostic pop
		JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Failed to ensure checking during certificate verification that the server has supplied a certificate for the hostname that we were expecting.", ::SSL_set1_host, ssl_, hostname.data());
	}

	~SSL_socket_wrapper() {
		try {
			close();
		} catch(...) {
			// Ignore.
		}
		::SSL_clear(ssl_);
		::SSL_free(ssl_);
	}

	void
	connect(std::string const& hostname, std::string const& service) noexcept(false) {
		int err{};
		/* Do the handshake with the server */
		while((err= ::SSL_connect(ssl_)) != 1) {
			if(handle_io_failure(err) == 1) [[likely]] {
				continue;	// Retry.
			} else {
				try {
					close();
				} catch(...) {
					// Ignore.
				}
				const int ssl_err= ::SSL_get_error(ssl_, err);
				::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
				BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::invalid_argument>(fmt::format("Error creating SSL connection. hostname: '{}', service: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", hostname, service, ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_connect", &wrapper<LkT>::wrapper::SSL_socket_wrapper::connect, err));
			}
		}
	}

	void
	set_options(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false) {
		DEBUG_ASSERT(timeout >= std::chrono::milliseconds{1});
		DEBUG_ASSERT(std::chrono::duration_cast<std::chrono::seconds>(timeout).count() <= std::numeric_limits<int>::max());
		socket_type const socket= native_handle();
		const ::linger details{
			1,	  // Enabled.
			static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(timeout).count())};	 // Seconds.
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_LINGER.", ::setsockopt, socket, SOL_SOCKET, SO_LINGER, &details, sizeof(::linger));
		// man implies milliseconds, then mentions minutes, [RFC0793] "USER TIMEOUT" does not directly specify units, only notes in 3.8.Interfaces/User/TCP Interface/ TCP User Commands/Open: "The present global default is five minutes." Which implies units on minutes. Values over 200 seem to "fail", which implies the units are not milliseconds!
		//	auto sock_timeout= static_cast<unsigned int>(timeout.count());
		unsigned int sock_timeout= 0;	  // Use system default.
		JMMCG_SYSCALL_WRAPPER("Unable to set TCP_USER_TIMEOUT.", ::setsockopt, socket, SOL_SOCKET, TCP_USER_TIMEOUT, &sock_timeout, sizeof(unsigned int));
		int val= 1;
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_REUSEADDR.", ::setsockopt, socket, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
		JMMCG_SYSCALL_WRAPPER("Unable to set TCP_NODELAY.", ::setsockopt, socket, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val));
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_SNDBUF.", ::setsockopt, socket, SOL_SOCKET, SO_SNDBUF, &max_message_size, sizeof(max_message_size));
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_RCVBUF.", ::setsockopt, socket, SOL_SOCKET, SO_RCVBUF, &max_message_size, sizeof(max_message_size));
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_RCVLOWAT.", ::setsockopt, socket, SOL_SOCKET, SO_RCVLOWAT, &min_message_size, sizeof(min_message_size));
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_PRIORITY.", ::setsockopt, socket, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
		JMMCG_SYSCALL_WRAPPER("Unable to set SO_INCOMING_CPU.", ::setsockopt, socket, SOL_SOCKET, SO_INCOMING_CPU, &incoming_cpu, sizeof(incoming_cpu));
	}

	[[nodiscard]] bool
	is_open() const noexcept(true) {
		try {
			int error_code{};
			socklen_t error_code_size= sizeof(error_code);
			JMMCG_SYSCALL_WRAPPER("Unable to get the state of the socket.", ::getsockopt, native_handle(), SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);
			return error_code == 0;
		} catch(...) {
			return false;
		}
	}

	void
	close() noexcept(false) {
		int ret{};
		while((ret= ::SSL_shutdown(ssl_)) != 1) {
			if(ret < 0 && handle_io_failure(ret) == 1) {
				continue;	// Retry...
			} else {
				break;
			}
		}
		error_buffer_.clear();
	}

	template<class MsgT>
	void
	write(MsgT const& message) noexcept(false) {
		DEBUG_ASSERT(message.is_valid());
		if constexpr(MsgT::has_static_size) {
			DEBUG_ASSERT(message.length() == sizeof(MsgT));
			auto const& buff= reinterpret_cast<std::array<std::byte, sizeof(MsgT)> const&>(message);
			write(buff);
		} else {
			DEBUG_ASSERT(!message.empty());
			std::size_t bytes_written{};
			int err{};
			while(!(err= ::SSL_write_ex(ssl_, message, message.length(), &bytes_written))) {
				if(handle_io_failure(0) == 1) [[likely]] {
					continue;	// Retry.
				} else {
					const int ssl_err= ::SSL_get_error(ssl_, err);
					::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", boost::core::demangle(typeid(MsgT).name()), ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_write_ex", &wrapper::SSL_socket_wrapper::write<MsgT>, err));
				}
			}
			DEBUG_ASSERT(bytes_written > 0);
			DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) <= sizeof(MsgT));
			DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) == message.length());
		}
	}

	template<class V, std::size_t N>
	void
	write(std::array<V, N> const& message) noexcept(false) {
		std::size_t bytes_written{};
		int err{};
		while(!(err= ::SSL_write_ex(ssl_, message.data(), sizeof(V) * N, &bytes_written))) {
			if(handle_io_failure(0) == 1) [[likely]] {
				continue;	// Retry.
			} else {
				const int ssl_err= ::SSL_get_error(ssl_, err);
				::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
				BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", boost::core::demangle(typeid(std::array<V, N>).name()), ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_write_ex", &wrapper::SSL_socket_wrapper::write<V, N>, err));
			}
		}
	}

	[[nodiscard]] bool
	read_msg(std::byte* dest, std::size_t msg_size) noexcept(false) {
		if(msg_size > 0) {
			std::size_t bytes_read= 0;
			bool eof= false;
			while(!eof && !::SSL_read_ex(ssl_, dest, msg_size, &bytes_read)) {
				switch(handle_io_failure(0)) {
				case 1:
					continue;	// Retry.
				case 0:
					eof= true;
					continue;
				case -1:
				default:
					DEBUG_ASSERT(bytes_read < msg_size);
					return true;
				}
			}
			DEBUG_ASSERT(bytes_read <= msg_size);
		}
		return false;
	}

	[[nodiscard]] std::string local_ip(domain_t::element_type const domain) const noexcept(false) {
		struct sockaddr_in name {};

		socket_type const socket= native_handle();
		socklen_t namelen= sizeof(name);
		JMMCG_SYSCALL_WRAPPER("Unable to get the local-name of the local, connected socket.", ::getsockname, socket, reinterpret_cast<struct sockaddr*>(&name), &namelen);
		std::string buffer(2 * (domain == domain_t::ip_v4 ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN), '\0');
		JMMCG_SYSCALL_WRAPPER("Failed to convert the locally connected socket name to a human-readable string.", ::inet_ntop, domain, &name.sin_addr, buffer.data(), buffer.size());
		auto last_printing_char_iter= std::find_if(buffer.rbegin(), buffer.rend(), [](auto const& v) {
			return std::isprint(v);
		});
		DEBUG_ASSERT(last_printing_char_iter != buffer.rend());
		--last_printing_char_iter;
		auto const end_of_data_index= std::distance(last_printing_char_iter, buffer.rend());
		DEBUG_ASSERT(end_of_data_index >= 0);
		buffer.resize(static_cast<std::string::size_type>(end_of_data_index));
		return buffer;
	}

	[[nodiscard]] std::chrono::seconds default_timeout() const noexcept(true) {
		return std::chrono::seconds(::SSL_get_default_timeout(ssl_));
	}

private:
	std::string error_buffer_;
	SSL* const ssl_;

	[[nodiscard]] socket_type
	native_handle() const noexcept(false) {
		socket_type const socket= JMMCG_SYSCALL_WRAPPER("Failed to get hold of the underlying file descriptor for the socket.", ::SSL_get_fd, ssl_);
		DEBUG_ASSERT(socket > 0);
		return socket;
	}

	[[nodiscard]] static std::string
	to_string(int code) {
		std::string buff(1024, '\0');
		::ERR_error_string_n(static_cast<unsigned long>(code), buff.data(), buff.size());
		if(auto found= buff.find('\0'); found != std::string::npos) {
			buff.resize(found);
		}
		return buff;
	}

	static int
	store_SSL_internal_error(const char* error_string, std::size_t error_string_length, void* data) noexcept(true) {
		DEBUG_ASSERT(data);
		std::string& error_buffer= *reinterpret_cast<std::string*>(data);
		error_buffer.append(error_string, error_string_length);
		return 0;
	}

	[[nodiscard]] std::string
	get_certificates_in_use() const {
		struct X509_deletor final {
			X509* cert;

			explicit X509_deletor(SSL* ssl)
				:	 // Get the certificate of the server.
				  cert(::SSL_get_peer_certificate(ssl)) {
			}

			~X509_deletor() {
				// Free the malloc'ed certificate copy.
				::X509_free(cert);
			}
		};

		X509_deletor cert(ssl_);
		if(cert.cert != NULL) {
			std::ostringstream certificates("Server certificates:\n");
			free_ptr<char*> line;
			line= make_free_ptr(X509_NAME_oneline(X509_get_subject_name(cert.cert), 0, 0));
			certificates << "Subject: " << line.get() << "\n";
			line= make_free_ptr(X509_NAME_oneline(X509_get_issuer_name(cert.cert), 0, 0));
			certificates << "Issuer: " << line.get() << "\n";
			return certificates.str();
		} else {
			return {"No client certificates configured."};
		}
	}

	static BIO*
	create_socket_bio(std::string const& hostname, std::string const& port, domain_t::element_type domain) noexcept(false) {
		if(!hostname.empty() && !port.empty()) {
			struct BIO_addrinfo_t {
				using element_type= BIO_ADDRINFO;

				element_type* const addrinfo;

				BIO_addrinfo_t(std::string const& hostname, std::string const& port, domain_t::element_type domain) noexcept(false)
					: addrinfo([&hostname, &port, domain]() {
						  element_type* addrinfo_tmp= nullptr;
						  JMMCG_SYSCALL_WRAPPER_FC(SSL_failure_code, "Failed to lookup IP address info for the server.", ::BIO_lookup_ex, hostname.data(), port.data(), BIO_LOOKUP_CLIENT, domain, type_t::stream, 0, &addrinfo_tmp);
						  DEBUG_ASSERT(addrinfo_tmp);
						  return addrinfo_tmp;
					  }()) {
				}

				~BIO_addrinfo_t() {
					::BIO_ADDRINFO_free(addrinfo);
				}
			};

			socket_type sock= -1;
			{
				BIO_addrinfo_t BIO_addrinfo(hostname, port, domain);
				// Loop through all the possible addresses for the server and find one we can connect to.
				for(typename BIO_addrinfo_t::element_type const* ai= BIO_addrinfo.addrinfo; ai != nullptr; ai= ::BIO_ADDRINFO_next(ai)) {
					// Create a TCP socket. We could equally use non-OpenSSL calls such as "socket" here for this and the subsequent connect and close functions. But for portability reasons and also so that we get errors on the OpenSSL stack in the event of a failure we use OpenSSL's versions of these functions.
					sock= ::BIO_socket(::BIO_ADDRINFO_family(ai), type_t::stream, 0, 0);
					if(sock == -1) {
						continue;
					}
					// Connect the socket to the server's address.
					if(!::BIO_connect(sock, BIO_ADDRINFO_address(ai), BIO_SOCK_NODELAY)) {
						::BIO_closesocket(sock);
						sock= -1;
						continue;
					}
					// Set to nonblocking mode.
					if(!::BIO_socket_nbio(sock, 1)) {
						sock= -1;
						continue;
					}
					// We have a connected socket so break out of the loop.
					break;
				}
			}
			/* If sock is -1 then we've been unable to connect to the server */
			if(sock != -1) {
				BIO* const bio= [](socket_type sock) {
					try {
						return JMMCG_SYSCALL_WRAPPER("Failed to create a BIO to wrap the socket.", ::BIO_new, ::BIO_s_socket());
					} catch(...) {
						JMMCG_SYSCALL_WRAPPER("Failed to close the BIO.", ::BIO_closesocket, sock);
						throw;
					}
				}(sock);
				DEBUG_ASSERT(bio);
				// Associate the newly created BIO with the underlying socket. By passing BIO_CLOSE here the socket will be automatically closed when the BIO is freed. Alternatively you can use BIO_NOCLOSE, in which case you must close the socket explicitly when it is no longer needed.
				::BIO_set_fd(bio, sock, BIO_CLOSE);
				return bio;
			} else {
				BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Unable to connect to the hostname ({}) at port ({}).", hostname, port), &SSL_socket_wrapper::create_socket_bio));
			}
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Both the hostname ({}) and port ({}) must not be empty.", hostname, port), &SSL_socket_wrapper::create_socket_bio));
		}
	}

	template<int ReadOrWrite>
		requires(ReadOrWrite == SSL_ERROR_WANT_READ || ReadOrWrite == SSL_ERROR_WANT_WRITE)
	void
	wait_for_activity() noexcept(false) {
		socket_type const socket= native_handle();
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(socket, &fds);
		int width= socket + 1;

		struct timeval timeout {
			default_timeout().count(),	  // sec
				0	 // usec
		};

		/*
		 * Wait until the socket is writeable or readable. We use select here
		 * for the sake of simplicity and portability, but you could equally use
		 * poll/epoll or similar functions
		 *
		 * NOTE: For the purposes of this demonstration code this effectively
		 * makes this demo block until it has something more useful to do. In a
		 * real application you probably want to go and do other work here (e.g.
		 * update a GUI, or service other connections).
		 *
		 * Let's say for example that you want to update the progress counter on
		 * a GUI every 100ms. One way to do that would be to add a 100ms timeout
		 * in the last parameter to "select" below. Then, when select returns,
		 * you check if it did so because of activity on the file descriptors or
		 * because of the timeout. If it is due to the timeout then update the
		 * GUI and then restart the "select".
		 */
		if constexpr(ReadOrWrite == SSL_ERROR_WANT_WRITE) {
			::select(width, nullptr, &fds, nullptr, &timeout);
		} else {
			BOOST_MPL_ASSERT_RELATION(ReadOrWrite, ==, SSL_ERROR_WANT_READ);
			::select(width, &fds, nullptr, nullptr, &timeout);
		}
	}

	int
	handle_io_failure(int res) noexcept(false) {
		switch(::SSL_get_error(ssl_, res)) {
		case SSL_ERROR_WANT_READ:
			// Temporary failure. Wait until we can read and try again.
			wait_for_activity<SSL_ERROR_WANT_READ>();
			return 1;
		case SSL_ERROR_WANT_WRITE:
			// Temporary failure. Wait until we can write and try again .
			wait_for_activity<SSL_ERROR_WANT_WRITE>();
			return 1;
		case SSL_ERROR_ZERO_RETURN:
			// EOF.
			return 0;
		case SSL_ERROR_SYSCALL:
			return -1;
		case SSL_ERROR_SSL:
			if(long const err= ::SSL_get_verify_result(ssl_); err != X509_V_OK) {
				const int ssl_err= ::SSL_get_error(ssl_, static_cast<int>(err));
				::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
				BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. SSL_get_error error={}, SSL_get_error translation='{}', X509 verify certificate error='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", ssl_err, to_string(ssl_err), ::X509_verify_cert_error_string(::SSL_get_verify_result(ssl_)), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_get_verify_result", &SSL_socket_wrapper::handle_io_failure, static_cast<int>(err)));
			} else {
				return -1;
			}
		default:
			return -1;
		}
	}
};

template<class LkT>
inline wrapper<LkT>::wrapper(domain_t::element_type domain, verify_SSL_certificates::element_type verify_SSL, std::optional<std::filesystem::path> const& filepath_to_pem)
	: domain_(domain), verify_SSL_(verify_SSL), ctx_(verify_SSL_, filepath_to_pem) {
	BOOST_MPL_ASSERT_RELATION(type_, ==, type_t::nonblocking);
}

template<class LkT>
inline void
wrapper<LkT>::connect(std::string const& hostname, std::string const& service) {
	auto&& ssl= std::make_unique<SSL_socket_wrapper>(ctx_, hostname, service, domain_);
	DEBUG_ASSERT(ssl.get());
	ssl->connect(hostname, service);
	const write_lock_t lk(this->mutex_);
	ssl_= std::move(ssl);
}

template<class LkT>
inline bool
wrapper<LkT>::is_open() const noexcept(true) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	if(ssl) {
		return ssl->is_open();
	} else {
		return false;
	}
}

template<class LkT>
inline void
wrapper<LkT>::close() noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl.swap(ssl_);
	}
	if(ssl) {
		::SSL_CTX_set_timeout(ctx_.ctx_, ssl->default_timeout().count());
		ssl_= SSL_socket_wrapper_ptr_t();
	}
}

template<class LkT>
template<class MsgT>
inline void
wrapper<LkT>::write(MsgT const& message) noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	if(ssl) {
		ssl->write(message);
	}
}

template<class LkT>
template<class V, std::size_t N>
inline void
wrapper<LkT>::write(std::array<V, N> const& message) noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	if(ssl) {
		ssl->write(message);
	}
}

template<class LkT>
inline bool
wrapper<LkT>::read_msg(std::byte* dest, std::size_t msg_size) noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	if(ssl) {
		return ssl->read_msg(dest, msg_size);
	} else {
		return true;
	}
}

template<class LkT>
template<class MsgT>
inline bool
wrapper<LkT>::read(MsgT& dest) noexcept(false) {
	if constexpr(MsgT::has_static_size) {
		DEBUG_ASSERT(dest.length() <= sizeof(MsgT));
		BOOST_MPL_ASSERT_RELATION(sizeof(MsgT), <=, SSIZE_MAX);
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), sizeof(MsgT));
		DEBUG_ASSERT(failed || dest.is_valid());
		return failed;
	} else {
		constexpr const std::size_t header_t_sz= MsgT::header_t_size;
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), header_t_sz);
		if(!failed) [[likely]] {
			auto const* hdr= reinterpret_cast<typename MsgT::Header_t const*>(&dest);
			const std::size_t length= hdr->length();
			if(length >= header_t_sz && length <= SSIZE_MAX && length <= sizeof(MsgT)) {
				const std::size_t body_size= length - header_t_sz;
				if(body_size > 0) {
					return read_msg(reinterpret_cast<std::byte*>(&dest) + header_t_sz, length - header_t_sz);
				} else {
					DEBUG_ASSERT(hdr->is_valid());
					return failed;
				}
			} else {
				BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to read the message from the socket. Type of message to read={}, SSL cipher in use: '{}'", boost::core::demangle(typeid(MsgT).name()), ::SSL_get_cipher(ssl_)), &wrapper::read<MsgT>));
			}
		} else [[unlikely]] {
			return failed;
		}
	}
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
wrapper<LkT>::read(V (&dest)[SrcSz]) noexcept(false) {
	BOOST_MPL_ASSERT_RELATION(sizeof(V) * SrcSz, <=, SSIZE_MAX);
	return read_msg(reinterpret_cast<std::byte*>(dest), sizeof(V) * SrcSz);
}

template<class LkT>
template<class MsgDetails, class V, std::size_t N>
inline bool
wrapper<LkT>::read(std::array<V, N>& buff) noexcept(false) {
	using msg_details_t= MsgDetails;

	BOOST_MPL_ASSERT_RELATION(msg_details_t::max_msg_size, >=, msg_details_t::header_t_size);

	const bool failed= read_msg(reinterpret_cast<std::byte*>(buff.data()), msg_details_t::header_t_size);
	if(!failed) [[likely]] {
		auto const* hdr= reinterpret_cast<typename msg_details_t::Header_t const*>(buff.data());
		const std::size_t length= hdr->length();
		if(length >= msg_details_t::header_t_size && length <= SSIZE_MAX && length <= (N * sizeof(V))) [[likely]] {
			const std::size_t body_size= length - msg_details_t::header_t_size;
			if(body_size > 0) {
				return read_msg(reinterpret_cast<std::byte*>(buff.data()) + msg_details_t::header_t_size, length - msg_details_t::header_t_size);
			} else {
				DEBUG_ASSERT(hdr->is_valid());
				return false;
			}
		} else [[unlikely]] {
			return true;
		}
	} else [[unlikely]] {
		return failed;
	}
}

template<class LkT>
inline void
wrapper<LkT>::set_options(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	if(ssl) {
		ssl->set_options(min_message_size, max_message_size, timeout, priority, incoming_cpu);
		::SSL_CTX_set_timeout(ctx_.ctx_, std::chrono::duration_cast<std::chrono::seconds>(timeout).count());
	}
}

template<class LkT>
inline std::string
wrapper<LkT>::local_ip() const noexcept(false) {
	SSL_socket_wrapper_ptr_t ssl;
	{
		const write_lock_t lk(this->mutex_);
		ssl= ssl_;
	}
	DEBUG_ASSERT(ssl.get());
	return ssl->local_ip(domain_);
}

}
}
}
}
}
}
