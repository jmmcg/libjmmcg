/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace private_ {
template<std::size_t I>
struct finder {
	template<class E, class T>
	static constexpr auto get(E e, T const& t) noexcept(true) {
		auto const& v= std::get<I>(t);
		return std::get<0>(v) == e ? std::get<1>(v) : finder<I - 1>::get(e, t);
	}
};
template<>
struct finder<0> {
	template<class E, class T>
	static constexpr auto get([[maybe_unused]] E e, T const& t) noexcept(true) {
		auto const& v= std::get<0>(t);
		DEBUG_ASSERT(std::get<0>(v) == e);
		return std::get<1>(v);
	}
};
template<std::size_t I>
struct loop {
	template<class T, class Fn>
	static constexpr void get(T const& t, Fn&& f) noexcept(false) {
		auto const& v= std::get<I>(t);
		f(v);
		loop<I - 1>::get(t, std::move(f));
	}
};
template<>
struct loop<0> {
	template<class T, class Fn>
	static constexpr void get(T const& t, Fn&& f) noexcept(false) {
		auto const& v= std::get<0>(t);
		f(v);
	}
};
}

inline std::string_view
exit_codes::to_string(codes::element_type e) noexcept(false) {
	return private_::finder<std::tuple_size<decltype(descriptions)>::value - 1>::get(e, descriptions);
}

inline std::string
exit_codes::to_string() noexcept(false) {
	std::ostringstream os;
	os << "Exit-code descriptions (error-conditions are output to 'std::cerr'):\n";
	auto print_element= [&os](auto const& desc) {
		os << fmt::format("[{}] {}: {}\n", static_cast<unsigned>(std::get<0>(desc)), codes::to_string(std::get<0>(desc)), std::get<1>(desc));
	};
	private_::loop<std::tuple_size<decltype(descriptions)>::value - 1>::get(descriptions, std::move(print_element));
	return os.str();
}

}}
