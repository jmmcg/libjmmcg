#ifndef LIBJMMCG_CORE_APPLICATION_HPP
#define LIBJMMCG_CORE_APPLICATION_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"
#include "exit_codes.hpp"
#include "yet_another_enum_wrapper.hpp"

#include <boost/asio/ip/address.hpp>
#include <boost/core/verbose_terminate_handler.hpp>
#include <boost/program_options.hpp>

#include <atomic>
#include <csignal>
#include <iostream>
#include <string>

namespace boost { namespace asio { namespace ip {

/// Extract a v4 TCP/IP address from the supplied stream.
/**
	Boost.asio.ip lacks stream-in operators, so isn't compatible with boost.program_options by default. Rather an omission in my opinion...
*/
inline std::istream&
operator>>(std::istream& is, boost::asio::ip::address& addr) {
	std::string s;
	is >> s;
	DEBUG_ASSERT(!s.empty());
	addr= boost::asio::ip::make_address(s);
	return is;
}

}}}

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Derive from this to assist with signal handling in one's executable.
class application {
public:
	using element_type= std::sig_atomic_t;
	/**
	 * All of these handled signals will cause the application to exit.
	 */
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(handled_signals,
		int,
		(interrupt, SIGINT),
		(hangup, SIGHUP),
		(quit, SIGQUIT),	 ///< Otherwise known as  "kill -15".
		(terminate, SIGTERM)	  ///< Otherwise known as "kill" or "kill -9".
	);

	static inline constexpr char error_message_prefix[]= "ERROR";
	static inline constexpr char copyright[]= "Copyright © 2023 by J.M.McGuiness";
	static inline constexpr char email_address[]= "coder@hussar.me.uk.";
	static inline constexpr char url[]= "http://libjmmcg.sf.net/";
	static inline constexpr char license[]= "Distributed under the terms of the GPL v2.1.";
	static inline constexpr char wireshark_info[]= "Note that Wireshark has a built-in FIX-protocol translator: see the \"Analyze/Enabled Protocols...\" dialog in Wireshark or https://www.wireshark.org/docs/dfref/f/fix.html.";

	static std::string make_help_message(char const* email= email_address) noexcept(false);
	static boost::program_options::options_description make_program_options(std::string const& description) noexcept(false);
	static exit_codes::codes::element_type check_basic_options(boost::program_options::variables_map const& vm, boost::program_options::options_description const& options, std::ostream& os) noexcept(false);
	static exit_codes::codes::element_type check_basic_options(boost::program_options::variables_map const& vm, boost::program_options::options_description const& options, char const* extra_details, std::ostream& os) noexcept(false);

	friend std::ostream& operator<<(std::ostream& os, application const&) noexcept(false);

	static std::atomic_flag& exit_requested() noexcept(true) {
		return exit_requested_;
	}

protected:
	/**
	 * By default SIGINT & SIGQUIT are trapped.
	 */
	[[nodiscard]] application() noexcept(true);
	application(application const&)= delete;
	virtual ~application() noexcept(true);

	/// Detect if a signal has been sent to the application.
	/**
	 * For example in a loop one may test this to check if CTRL-C has been pressed and exit the loop thus permitting C++ dtors to run.
	 *
	 * \return Non-zero if a signal was trapped.
	 */
	[[nodiscard]] static element_type signal_status() noexcept(true);

private:
	static inline volatile element_type signal_status_{};
	static inline std::atomic_flag exit_requested_{};

	static void signal_handler(int signal) noexcept(true);
};

}}

#include "application_impl.hpp"

#endif
