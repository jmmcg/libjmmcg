#ifndef LIBJMMCG_CORE_THREAD_SAFE_ADAPTORS_HPP
#define LIBJMMCG_CORE_THREAD_SAFE_ADAPTORS_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

#include <boost/mpl/assert.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/// Atomically count the amount of work that there is to do, and provide access to the lock on the containing collection.
/**
	This class adds a constant-time counter to the safe_colln or queue or funky_queue.
	This uses standard locks.
	\todo I suppose I could use some kind of enable_if to detect if the container has a size() member-method, and only use this if it doesn't. That's a micro-optimisation to do.
*/
template<class Lk>
class no_signalling {
public:
	typedef api_lock_traits<platform_api, sequential_mode>::anon_event_type atomic_t;
	typedef Lk locker_type;
	typedef typename locker_type::lock_traits lock_traits;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (locker_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && atomic_t::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																			  ? generic_traits::memory_access_modes::crew_memory_access
																																			  : generic_traits::memory_access_modes::erew_memory_access);

	constexpr no_signalling() noexcept(true)
		: lock_(), have_work_() {
	}
	explicit no_signalling(atomic_t& ev) noexcept(true)
		: lock_(), have_work_(&ev) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	}
	constexpr no_signalling(no_signalling const& s) noexcept(true)
		: lock_(), have_work_(s.have_work_) {
	}

	atomic_t& __fastcall have_work() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		return *have_work_;
	}
	locker_type& __fastcall locker() const noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<locker_type const*>(&lock_));
		return lock_;
	}
	locker_type& __fastcall locker() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<locker_type*>(&lock_));
		return lock_;
	}

	void __fastcall add() noexcept(true) {
		if(have_work_) {
			have_work_->set();
		}
	}
	void __fastcall add(typename atomic_t::count_type const c) noexcept(true) {
		if(have_work_) {
			for(typename atomic_t::count_type i= 0; i < c; ++i) {
				have_work_->set();
			}
		}
	}
	typename atomic_t::atomic_state_type __fastcall remove() noexcept(true) {
		if(have_work_) {
			return have_work_->lock();
		} else {
			return atomic_t::lock_traits::atom_unset;
		}
	}
	void __fastcall remove(typename atomic_t::count_type const c) noexcept(true) {
		if(have_work_) {
			for(typename atomic_t::count_type i= 0; i < c; ++i) {
				have_work_->lock();
			}
		}
	}
	typename atomic_t::atomic_state_type __fastcall try_remove() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		return have_work_->try_lock();
	}
	static constexpr void clear() noexcept(true) {
	}
	static constexpr typename atomic_t::count_type __fastcall count() noexcept(true) {
		return 0;
	}

private:
	mutable locker_type lock_;
	atomic_t* have_work_;
};

/// A flag to atomically signal if the container contains work or not and also count the amount of work that there is to do.
/**
	This uses standard locks.
*/
template<class Lk>
class signalling {
public:
	typedef Lk atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename atomic_t::locker_type locker_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

private:
	atomic_t* have_work_;

public:
	constexpr __stdcall signalling() noexcept(true)
		: have_work_() {
	}
	explicit signalling(atomic_t& ev) noexcept(true)
		: have_work_(&ev) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	}
	__stdcall signalling(signalling const& s) noexcept(true)
		: have_work_(s.have_work_) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
	}

	signalling(signalling&&)= delete;
	void operator=(signalling const&)= delete;
	void operator=(signalling&&)= delete;

	constexpr atomic_t& __fastcall have_work() const noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		return *have_work_;
	}
	atomic_t& __fastcall have_work() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		return *have_work_;
	}
	constexpr locker_type& __fastcall locker() const noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t const*>(have_work_));
		return have_work_->locker();
	}
	locker_type& __fastcall locker() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		return have_work_->locker();
	}

	void add() noexcept(false) {
		DEBUG_ASSERT(dynamic_cast<atomic_t*>(have_work_));
		typename lock_traits::atomic_state_type const ret= have_work_->set_nolk(atomic_t::states::new_work_arrived);
		if(ret != lock_traits::atom_set) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Could not add more work to the atomic object.", static_cast<void (signalling::*)()>(&signalling::add)));
		}
	}
	void __fastcall add(typename atomic_t::count_type const c) noexcept(false) {
		for(typename atomic_t::count_type i= 0; i < c; ++i) {
			add();
		}
	}
	typename atomic_t::lock_result_type __fastcall remove() noexcept(noexcept(have_work_->lock(0)));
	void __fastcall remove(typename atomic_t::count_type const c) noexcept(false);
	typename atomic_t::lock_result_type __fastcall try_remove() noexcept(noexcept(have_work_->try_lock()));
	void clear() noexcept(noexcept(have_work_->clear()));
	typename atomic_t::count_type __fastcall count() const noexcept(noexcept(have_work_->count()));
};

/// An adaptor for a container that attempts to add some thread-safety to assist in making thread-safe programs.
/**
	By default the adapted container does not use a read-write lock.
	Note that if the read_lock_type and write_lock_types are the same, i.e. an exclusive lock were used, then the adaptor will exhibit EREW semantics. If a read-writer lock is used for them ,then it will exhibit CREW semantics.

	\see queue
*/
template<
	typename C,
	typename M,
	typename WL= typename M::write_lock_type,
	class Sig= no_signalling<M>,
	class MLk= typename lock::any_order::all<M::lock_traits::api_type, typename M::lock_traits::model_type, M, M> >
class safe_colln : private C	 ///< We want to be able to pass it as a "C (container)", but we also don't want to expose the unprotected, base functionality.
{
public:
	typedef C container_type;	 ///< The container to be adapted.
	typedef Sig have_work_type;	///< Used to enable functionality to atomically signal if the container contains work or not.
	typedef M atomic_t;	 ///< The underlying lock object to use that will be locked in some (EREW or CREW or other) manner.
	typedef WL write_lock_type;	///< The type of write-lock to use. This allows the possibility of using a read-write lock.
	typedef typename atomic_t::read_lock_type read_lock_type;	///< The type of read lock to use, by default the write lock. This allows the possibility of using a read-write lock.
	typedef MLk lock_all_type;	  ///< The multi-lock type to use to ensured that operations on combined safe_collns are thread-safe. Note that locking them in any order reduces the likelihood of deadlock at the cost of performance.
	typedef typename atomic_t::lock_traits lock_traits;
	typedef api_threading_traits<lock_traits::api_type, typename lock_traits::model_type> thread_traits;
	typedef typename container_type::reference reference;
	typedef typename container_type::const_reference const_reference;
	typedef typename container_type::size_type size_type;
	typedef typename container_type::value_type value_type;
	using exit_requested_type= typename have_work_type::atomic_t;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (write_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && read_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && lock_all_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  // TODO: some do and some don't have this as a member...			&& value_type::memory_access_mode==generic_traits::memory_access_modes::crew_memory_access
																																				  && have_work_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																			  ? generic_traits::memory_access_modes::crew_memory_access
																																			  : generic_traits::memory_access_modes::erew_memory_access);

	BOOST_MPL_ASSERT((std::is_same<typename write_lock_type::atomic_t, atomic_t>));

	/// A flag to atomically signal if the container contains work or not, how much work and the underlying lock, to assist in writing thread-safe code.
	mutable have_work_type have_work;

	/// A flag to atomically signal if the container contains work or not, how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() const noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() const noexcept(true) {
		return have_work.locker();
	}

	__stdcall safe_colln() noexcept(noexcept(container_type()) && noexcept(have_work_type()));
	explicit safe_colln(typename have_work_type::atomic_t&);
	explicit safe_colln(std::initializer_list<value_type>);
	explicit __stdcall safe_colln(size_type const sz, value_type const& v= value_type());
	template<class T1, class T2>
	__stdcall safe_colln(size_type const sz, T1 const&, T2 const&);
	explicit __stdcall safe_colln(const container_type&);
	__stdcall safe_colln(const safe_colln&) noexcept(false);
	__stdcall ~safe_colln()= default;
	safe_colln& __fastcall operator=(const safe_colln&) noexcept(false);

	bool __fastcall empty() const noexcept(true);
	size_type __fastcall sync_size() const noexcept(false);
	size_type __fastcall size() const noexcept(true);

	value_type __fastcall operator[](size_type s) const noexcept(false);

	void __fastcall push_back(value_type const& v) noexcept(false);
	void __fastcall push_back(value_type&& v) noexcept(false);

	void __fastcall push_front(const value_type& v) noexcept(false);

	void __fastcall push_front(value_type&& v) noexcept(false);

	size_type __fastcall erase(const value_type& v) noexcept(false);

	void __fastcall reserve(size_type sz) noexcept(false);

	void __fastcall clear() noexcept(false);

	void __fastcall swap(safe_colln& t) noexcept(false);

	/// Resize the container to the requested size, but try to minimise (re-)initialising or deleting any of the existing elements.
	/**
		Current C++03 & C++11 containers have an implicit sequential order of initialisation or re-initialisation of the elements they contain. This enforces a O(n) complexity on resize(). To minimise this (re-)initialisation of existing elements, only initialise new elements added to the container, or delete the excess.

		\todo Ideally I want to have an "uninitialized resize()" (reserve() does not set the size), so that I can initialise the elements of the container in the order I wish, using a parallel fill_n() for example.

		\see resize(), reserve()
	*/
	void __fastcall resize_noinit_nolk(const size_type sz) noexcept(false);

	/// Resize the container to the requested size.
	/**
		\see resize_noinit_nolk(), resize(), reserve()
	*/
	void __fastcall resize(const size_type sz) noexcept(false);

	bool __fastcall operator==(safe_colln const&) const noexcept(true);
	template<typename M1, typename WL1, class Sig1, class MLk1>
	bool __fastcall
	operator==(safe_colln<C, M1, WL1, Sig1, MLk1> const&) const noexcept(true);

	container_type const& colln() const noexcept(true) {
		return static_cast<container_type const&>(*this);
	}
	container_type& colln() noexcept(true) {
		return static_cast<container_type&>(*this);
	}
};

/// An adaptor to add thread-safety assistance, specifically for queues.
/**
	Note that this adaptor relies on the standardised behaviour of a sequence (or an adaptor thereof) with respect to invalidating iterators when items are added to or removed from removed from the container. Basically only std::list is guaranteed to satisfy these requirements, but std::queue often does, but that is implementation-dependent.
	This queue operates with one big fat lock.
	The iterators are not exposed to assist with writing thread-safe code.
	Note that if the read_lock_type and write_lock_types are the same, i.e. an exclusive lock were used, then the adaptor will exhibit EREW semantics. If a read-writer lock is used for them, then it will exhibit CREW semantics.

	\see safe_colln, funky_queue
*/
template<
	typename QT,
	typename M,
	typename WL= typename M::write_lock_type,
	class Sig= no_signalling<M>,	 ///< \todo Should be a template type to ensure that M is a unique type.
	class ValRet= typename QT::value_type,
	class MLk= typename lock::any_order::all<M::lock_traits::api_type, typename M::lock_traits::model_type, M, M> >
class queue : protected QT {
public:
	typedef QT container_type;	  ///< The queue to be adapted, usually std::list or std::queue.
	typedef Sig have_work_type;	///< Used to enable functionality to atomically signal if the container contains work or not.
	typedef M atomic_t;	 ///< The underlying lock object to use.
	typedef WL write_lock_type;	///< The type of write-lock to use. This allows the possibility of using a read-write lock.
	typedef typename atomic_t::read_lock_type read_lock_type;	///< The type of read lock to use, by default the write lock. This allows the possibility of using a read-write lock.
	typedef MLk lock_all_type;	  ///< The multi-lock type to use to ensured that operations on combined queues are thread-safe. Note that locking them in any order reduces the likelihood of deadlock at the cost of performance.
	typedef typename write_lock_type::lock_traits lock_traits;
	typedef api_threading_traits<lock_traits::api_type, typename lock_traits::model_type> thread_traits;
	typedef typename container_type::reference reference;
	typedef typename container_type::const_reference const_reference;
	typedef typename container_type::size_type size_type;
	typedef typename container_type::value_type value_type;
	typedef ValRet value_ret_type;	///< The type to return when removing items from the queue.

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (write_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && read_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && lock_all_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && value_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && have_work_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																			  ? generic_traits::memory_access_modes::crew_memory_access
																																			  : generic_traits::memory_access_modes::erew_memory_access);

	BOOST_MPL_ASSERT((std::is_same<typename write_lock_type::atomic_t, atomic_t>));

	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	mutable have_work_type have_work;

	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() const noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() noexcept(true) {
		return have_work.locker();
	}
	/// A flag to atomically signal if the container contains work or not, and how much work and the underlying lock, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() const noexcept(true) {
		return have_work.locker();
	}

	__stdcall queue() noexcept(noexcept(container_type()) && noexcept(have_work_type()));
	explicit queue(typename have_work_type::atomic_t&);
	__stdcall queue(queue const&) noexcept(false);
	__stdcall ~queue() noexcept(true)= default;
	queue& __fastcall operator=(queue const&) noexcept(false);

	bool __fastcall empty() const noexcept(true);
	size_type __fastcall sync_size() const noexcept(false);
	size_type __fastcall size() const noexcept(true);

	value_type __fastcall front() const noexcept(false);

	void __fastcall push_back(value_type const& v) noexcept(true);
	void __fastcall push_back(value_type&& v) noexcept(true);

	value_ret_type __fastcall pop_front() noexcept(false);
	void __fastcall push_front(const value_type& v) noexcept(true);
	void __fastcall push_front(value_type&& v) noexcept(true);

	size_type __fastcall erase(const value_type& v) noexcept(true);

	void __fastcall clear() noexcept(true);

	container_type const& colln() const noexcept(true) {
		return static_cast<container_type const&>(*this);
	}
	container_type& colln() noexcept(true) {
		return static_cast<container_type&>(*this);
	}

	value_ret_type __fastcall pop_front_nolk() noexcept(false);

	value_type __fastcall pop_front_1_nochk_nolk() noexcept(noexcept(have_work.remove()));
	value_type __fastcall pop_front_1_nochk_nosig() noexcept(true);

protected:
	virtual value_ret_type __fastcall pop_front_nochk_nolk() noexcept(false);
};

/// An adaptor to add thread-safety assistance, specifically for queues.
/**
	Note that this adaptor relies on the standardised behaviour of a sequence (or an adaptor thereof) with respect to invalidating iterators when items are added to or removed from removed from the container. Basically only std::list is guaranteed to satisfy these requirements, but std::queue often does, but that is implementation-dependent.
	This queue operates two locks, a pop & a push lock, that operate independently as long as the queue is large enough.
	The operations push() and push_back() have a push lock and are thus serialised.
	The operations pop() and pop_front() have a pop lock and are thus serialised.
	When the queue is too short, these pairs of operations are also mutually serialised.
	By default the adapted queue does not use a read-write lock.
	The iterators are not exposed to assist with writing thread-safe code.
	Note that if the read_lock_type and write_lock_types are the same, i.e. an exclusive lock were used, then the adaptor will exhibit EREW semantics. If a read-writer lock is used for them, then it will exhibit CREW semantics.

	\see safe_colln, queue
*/
template<
	typename QT,
	typename M,
	typename WL= typename M::write_lock_type,
	class Sig= no_signalling<M>,
	class ValRet= typename QT::value_type,
	class MLk= typename lock::any_order::all<M::lock_traits::api_type, typename M::lock_traits::model_type, M, M> >
class funky_queue : private QT {
public:
	typedef QT container_type;	  ///< The queue to be adapted, usually std::list or std::queue.
	typedef Sig have_work_type;	///< Used to enable functionality to atomically signal if the container contains work or not.
	typedef M atomic_t;	 ///< The underlying lock object to use.
	typedef WL write_lock_type;	///< The type of write-lock to use. This allows the possibility of using a read-write lock.
	typedef typename atomic_t::read_lock_type read_lock_type;	///< The type of read lock to use, by default the write lock. This allows the possibility of using a read-write lock.
	typedef MLk lock_all_type;	  ///< The multi-lock type to use to ensured that operations on combined queues are thread-safe. Note that locking them in any order reduces the likelihood of deadlock at the cost of performance.
	typedef typename write_lock_type::lock_traits lock_traits;
	template<class... T>
	using scoped_lock= typename lock_traits::template scoped_lock<T...>;
	typedef api_threading_traits<lock_traits::api_type, typename lock_traits::model_type> thread_traits;
	typedef typename container_type::reference reference;
	typedef typename container_type::const_reference const_reference;
	typedef typename container_type::size_type size_type;
	typedef typename container_type::value_type value_type;
	typedef ValRet value_ret_type;	///< The type to return when removing items from the queue.

	static inline constexpr const size_type serialise_size= 2;	 ///< The size of the queue, below which the push & pop operations serialise.

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (write_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && read_lock_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && lock_all_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && value_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && have_work_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																			  ? generic_traits::memory_access_modes::crew_memory_access
																																			  : generic_traits::memory_access_modes::erew_memory_access);

	BOOST_MPL_ASSERT((std::is_same<typename write_lock_type::atomic_t, atomic_t>));

	/// A flag to atomically signal if the container contains work or not, and how much work.
	mutable have_work_type have_work;

	/// The underlying locks, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() noexcept(true) {
		return pop_lock_;
	}
	/// The underlying locks, to assist in writing thread-safe code.
	atomic_t& __fastcall pop_lock() const noexcept(true) {
		return pop_lock_;
	}
	/// The underlying locks, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() noexcept(true) {
		return push_lock_;
	}
	/// The underlying locks, to assist in writing thread-safe code.
	atomic_t& __fastcall push_lock() const noexcept(true) {
		return push_lock_;
	}

	__stdcall funky_queue() noexcept(noexcept(container_type()) && noexcept(have_work_type()));
	explicit funky_queue(typename have_work_type::atomic_t&);
	__stdcall funky_queue(funky_queue const&) noexcept(false);
	__stdcall ~funky_queue()= default;
	funky_queue& __fastcall operator=(funky_queue const&) noexcept(false);

	bool __fastcall empty() const noexcept(true);
	size_type __fastcall sync_size() const noexcept(false);
	size_type __fastcall size() const noexcept(true);

	void __fastcall clear() noexcept(false);
	/**
		This function is provided to assist with writing thread-safe code.

		\return	Returns true if a value was erased, otherwise false.
	*/
	bool __fastcall erase(value_type const&) noexcept(false);

	/**
		If the queue is long enough, then this function will not block the operation of push() or push_back().

		\return	A copy of the value that is on the front of the queue.
	*/
	value_type __fastcall front() const noexcept(false);
	/**
		If the queue is long enough, then this function will not block the operation of pop() or pop_back().

		\return	A copy of the value that is on the back of the queue.
	*/
	value_type __fastcall back() const noexcept(false);

	/**
		If the queue is long enough, then this function will not block the operation of pop() or pop_back().

		\param v	The value to be added.
	*/
	void __fastcall push(value_type const& v) noexcept(false);
	/**
		If the queue is long enough, then this function will not block the operation of pop() or pop_back().

		\param v	The value to be added.
	*/
	void __fastcall push(value_type&& v) noexcept(false);
	/**
		If the queue is long enough, then this function will not block the operation of pop() or pop_back().

		\param v	The value to be added.
	*/
	void __fastcall push_back(value_type const& v) noexcept(false);
	/**
		If the queue is long enough, then this function will not block the operation of pop() or pop_back().

		\param v	The value to be added.
	*/
	void __fastcall push_back(value_type&& v) noexcept(false);

	/**
		If the queue is long enough, then this function will not block the operation of push() or push_front().

		\return	The value popped off the queue.
	*/
	value_ret_type __fastcall pop() noexcept(false);
	/**
		If the queue is long enough, then this function will not block the operation of push() or push_front().

		\return	The value popped off the queue.
	*/
	value_ret_type __fastcall pop_front() noexcept(false);

	/**
		\param	e	The item to be removed from the container_type.
	*/
	void __fastcall remove(const value_type& e) noexcept(false);

	container_type const& colln() const noexcept(true) {
		return static_cast<container_type const&>(*this);
	}
	container_type& colln() noexcept(true) {
		return static_cast<container_type&>(*this);
	}

	value_ret_type __fastcall pop_front_nolk() noexcept(false);

	value_type __fastcall pop_front_1_nochk_nolk() noexcept(noexcept(have_work.remove()));
	value_type __fastcall pop_front_1_nochk_nosig() noexcept(true);

protected:
	value_type& __fastcall back_nolk() noexcept(true);
	virtual value_ret_type __fastcall pop_front_nochk_nolk() noexcept(false);

private:
	mutable atomic_t push_lock_;
	mutable atomic_t pop_lock_;

	value_ret_type __fastcall pop_nolk() noexcept(false);

	value_type const& __fastcall back_nolk() const noexcept(true);
	void __fastcall push_back_nolk(const value_type& e) noexcept(false);
	void __fastcall push_back_nolk(value_type&& e) noexcept(false);
	void __fastcall push_nolk(const value_type& e) noexcept(false);
	void __fastcall push_nolk(value_type&& e) noexcept(false);
	void __fastcall pop_nochk_nolk() noexcept(false);
};

}}}

#include "thread_safe_adaptors_impl.hpp"

#endif
