/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "latency_timestamps.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <fstream>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

void
latency_timestamps_itf::timestamp::to_csv(std::ostream& os) const noexcept(false) {
	os << std::setprecision(std::numeric_limits<double>::digits10 + 1) << cpu_timer::TSC_to_microsec(start)
		<< ',' << std::setprecision(std::numeric_limits<double>::digits10 + 1) << cpu_timer::TSC_to_microsec(end);
}

latency_timestamps::latency_timestamps(size_type num_tses) noexcept(false)
	: timestamps{new timestamp[num_tses]},
	  current_timestamp(timestamps.get()),
	  num_timestamps(num_tses) {
	DEBUG_ASSERT(current_timestamp != nullptr);
}

void
latency_timestamps::to_csv(std::ostream& os) const noexcept(false) {
	os
		<< boost::core::demangle(typeid(timer_t).name())
		<< ",Number of entries," << size()
		<< ",microsec\n";
	for(element_type const* stamp= timestamps.get(); stamp != current_timestamp; ++stamp) {
		DEBUG_ASSERT(std::less<element_type const*>()(stamp, current_timestamp));
		DEBUG_ASSERT(stamp);
		DEBUG_ASSERT(stamp->is_valid());
		stamp->to_csv(os);
		os << '\n';
	}
}

void
latency_timestamps::write_to_csv_file(std::ostream& os, char const* const root) const noexcept(false) {
	write_to_csv_file_fullpath(os, make_filename(root) + ".csv");
}

void
latency_timestamps::write_to_named_csv_file(std::ostream& os, std::string const& base_filename) const noexcept(false) {
	write_to_csv_file_fullpath(os, base_filename + ".csv");
}

void
latency_timestamps::write_to_csv_file_fullpath(std::ostream& os, std::string const& filename) const noexcept(false) {
	if(num_timestamps > 0) {
		os << "Writing " << size() << " latencies to '" << filename << "', (this might take some time)..." << std::endl;
		std::ofstream csv_file(filename.c_str());
		to_csv(csv_file);
		os << "... Written." << std::endl;
	}
}

}}
