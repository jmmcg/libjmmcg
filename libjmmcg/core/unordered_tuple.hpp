#ifndef LIBJMMCG_CORE_UNORDERED_TUPLE_HPP
#define LIBJMMCG_CORE_UNORDERED_TUPLE_HPP

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, libjmmcg@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "blatant_old_msvc_compiler_hacks.hpp"
#include "max_min.hpp"
#include "non_allocatable.hpp"
#include "ttypes.hpp"

#include <boost/mpl/long.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/size.hpp>

#include <algorithm>
#include <cstdint>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// For those situations where one is so lucky as to be able to define one of these, here's a forward-declaration as an extension-point to the library...
/**
 * See the cmake-target compute_mph_mask which builds an executable that may be used to attempt to generate a perfect, minimal hash from a set of Values. The output is a C++ header file containing the instantiated hash totally specific to those Values.
 * If the compiler gives errors noting attempting to use this declaration, then it is likely the appropriate cmake-script will need updating according to the start-states in the hash::msm used. The error message given, whilst long, lists exactly what needs to be updated in the cmake script.
 *
 * \see compute_mph_mask, hash::msm
 */
template<auto... Values>
struct perfect_hash;

/// A unordered container for a disparate list of types for which the keys form a perfect hash, computable at compile-time.
/**
	This class performs zero memory allocations. All of the data is maintained on the stack at the cost of some additional space. There is padding added between each instance of a MappedType to ensure that those instances avoid false sharing in any cache level.

	Tests with using "Frozen" vs "Eternal" for the constexpr unordered_map used internally reveal that, in the tested case, Frozen appears to be over 2x faster than Eternal, vs if the Hasher is the custom minimal_perfect_hash, the latter is significantly faster and requires less space, but must be custom created.

	\todo Frozen is unable to hash the MIC codes.

	\see libjmmcg::function
*/
template<
	class KeyType,	  ///< The type of the keys used to identify the particular, unique, instance of a MappedType.
	class MappedType,	  ///< A base-class common to all of the MappedTypes. For the clock-cycle concious, do not worry! Both gcc & clang of numerous versions are able to de-virtualise all of the examples I and Vladimir Arnost attempted and clang even managed to inline suitable functions. (This appears to be because the full types must be known at the point of instantiation, due to the use of templates.)
	class Hasher,	 ///< The constexpr hash functor to be applied to the keys of KeyType.
	template<class>
	class ExtractHash,	///< A template-algorithm to extract the particular key to be hashed from the MappedType, that will be hashed using the Hasher. Must have a static, constexpr member called value that should contain the key extracted from the type.
	class... MappedTypes	  ///< The disparate list of types to be contained, which will be used to generate a hash with ExtractHash and Hasher. Moreover there is no guarantee that the order of the MappedTypes on the stack will reflect any ordering of the keys, if it exists.
	>
class unordered_tuple final : private non_newable {
private:
	using mapped_buffer_element= std::byte;

	enum : std::size_t {
		tmp_max_size= varadic_max(sizeof(MappedTypes)...),
		mapped_elem_stride= ((tmp_max_size + sizeof(std::max_align_t)) / sizeof(std::max_align_t)) * sizeof(std::max_align_t),	 ///< False sharing size.
	};

	/**
	 *		As we do not know the alignment of each of the objects to be constructed, be very conservative and 64-bit align all of them. This might waste memory, but that is life... At least they'll be better aligned to avoid false sharing.
	 */
	BOOST_MPL_ASSERT_RELATION(mapped_elem_stride % sizeof(std::max_align_t), ==, 0);
	static_assert(
		(std::is_base_of<MappedType, MappedTypes>::value && ...),
		"All of the MappedTypes must derived from MappedType.");

public:
	using key_type= KeyType;
	using mapped_type= MappedType;

	enum : std::size_t {
		size= sizeof...(MappedTypes),
		max_size= static_cast<std::size_t>(mapped_elem_stride) * size
	};

	BOOST_MPL_ASSERT_RELATION(size, >, 0);
	using all_elements_t= std::tuple<MappedTypes...>;

	static_assert(sizeof(key_type) <= sizeof(unsigned long long), "Internal check failure - key_type size is too wide (in bits), must be less than 'unsigned long long'.");
	/**
	 * Check that we have the same number of unique hashes as number of input types, thus each input type may be uniquely identified.
	 */
	BOOST_MPL_ASSERT_RELATION(
		(boost::mpl::size<
			boost::mpl::set<
				typename boost::mpl::integral_c<
					unsigned long long,
					static_cast<unsigned long long>(ExtractHash<MappedTypes>::value)>::type...>>::value),
		==,
		size);
	/**
	 * The hash function must be a (possibly minimal) perfect hash function. The program 'compute_mph_mask' could be useful for this purpose. Note that this object is constructed from a finite set of types that map to unique hash values. This dictionary is known at compile-time, by definition of C++, therefore a minimal, perfect hash should be computable.
	 *
	 * \see Hasher
	 * \see compute_mph_mask
	 */
	static_assert(((Hasher::result(static_cast<typename Hasher::key_type>(ExtractHash<MappedTypes>::value)) < Hasher::denominator) && ...), "The hashes of all of the mapped types must be less than the denominator. Has the (minimal) perfect hash been correctly generated?");

	unordered_tuple() noexcept((noexcept(MappedTypes{}) && ...));
	/**
		If the construction of any instance of a mapped_type fails, then any already constructed instance of a mapped_type will be correctly deleted, and that failing exception re-thrown.
		Each of the MappedTypes is either default constructed or constructed with exactly, successively, one of the {arg, args...} parameters passed to this ctor. Thus there must be the same number of MappedTypes as the number of ctor arguments passed in. i.e. sizeof...(MappedTypes)==sizeof...(args)+1.
	*/
	template<
		class... MappedCtorArgs,
		typename std::enable_if<(sizeof...(MappedCtorArgs) > 1), bool>::type= true>
	[[nodiscard]] unordered_tuple(MappedCtorArgs&&... args) noexcept((noexcept(MappedTypes{args}) && ...));
	~unordered_tuple() noexcept(true);

	/// Find the instance of mapped_type associated with the instance of key_type.
	/**
		Algorithmic complexity: O(1)
		Note that great effort has been made to ensure that this is as fast as possible.

		\param key	The instance of key_type associated with a mapped_type.
		\return	A reference to the associated instance of mapped_type.
	 */
	REALLY_FORCE_INLINE constexpr mapped_type const& operator[](const key_type key) const noexcept(true);
	/// Find the instance of mapped_type associated with the instance of key_type.
	/**
		Algorithmic complexity: O(1)
		Note that great effort has been made to ensure that this is as fast as possible.

		\param key	The instance of key_type associated with a mapped_type.
		\return	A reference to the associated instance of mapped_type.
	 */
	REALLY_FORCE_INLINE constexpr mapped_type& operator[](const key_type key) noexcept(true);

	/// Computes the sum of the given value init and the elements in the range of MappedTypes.
	/**
	 * \param init	Initial value of the sum.
	 * \param op	Binary operation function object that will be applied. The binary operator takes the current accumulation value a (initialized to init) and the value of the current element b.
	 *
	 * \see std::accumulate()
	 */
	template<class T, class BinaryOperation>
	T
	accumulate(T init, BinaryOperation op) const noexcept(false);

	/// Assigns each element in the range of MappedTypes a value generated by the given function object g.
	/**
	 * \param g	The generator function object that will be called.
	 *
	 * \see std::generate()
	 */
	template<class Generator>
	void
	generate(Generator g) noexcept(false);

private:
	template<class T, bool b>
	struct get_underlying {
		using type= T;
	};

	template<class T>
	struct get_underlying<T, true> {
		using type= typename std::underlying_type<T>::type;
	};
	template<class>
	class idx_for_mapped_index_colln;

	template<std::size_t... Indices>
	class idx_for_mapped_index_colln<std::index_sequence<Indices...>> final {
		using raw_key_type= typename get_underlying<key_type, std::is_enum<key_type>::value>::type;
		static_assert(sizeof(typename Hasher::key_type) >= sizeof(raw_key_type), "The keys are too large for the supplied hash function so hash function supplied is invalid.");

	public:
		REALLY_FORCE_INLINE [[gnu::pure]] static constexpr std::size_t
		find(key_type key) noexcept(true) {
			std::size_t const pos= Hasher::result(static_cast<typename Hasher::key_type>(key));
			DEBUG_ASSERT(pos < static_cast<std::size_t>(Hasher::denominator));
			return pos * mapped_elem_stride;
		}
	};

	using mapped_index_colln_t= idx_for_mapped_index_colln<std::make_index_sequence<size>>;
	BOOST_MPL_ASSERT_RELATION(static_cast<unsigned long long>(size), <=, static_cast<unsigned long long>(Hasher::denominator));

	/// The type of the underlying buffer that will contain the instances of mapped_types.
	/**
	 * As the hash may be one-less than perfect, then we have to add the padding, so use the value computed by the hash to ensure the buffer is big enough.
	 */
	using mapped_cont_t= std::array<mapped_buffer_element, static_cast<std::size_t>(mapped_elem_stride) * Hasher::denominator>;

	enum : std::size_t {
		objs_size= (0 + ... + sizeof(MappedTypes))
	};

	BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(objs_size), <=, static_cast<std::size_t>(max_size));

	/**
		 Avoid false sharing...
	 */
	ALIGN_TO_L1_CACHE mapped_cont_t mapped_cont{};
};

template<class KeyType, class MappedType, class Hasher, template<class> class ExtractHash, class... MappedTypes>
std::ostream&
operator<<(std::ostream& os, unordered_tuple<KeyType, MappedType, Hasher, ExtractHash, MappedTypes...> const& t) noexcept(false);

}}

#include "unordered_tuple_impl.hpp"

#endif
