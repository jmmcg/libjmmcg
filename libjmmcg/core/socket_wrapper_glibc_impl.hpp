/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace glibc {

namespace basic {

template<class LkT>
inline void
wrapper<LkT>::set_options(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false) {
	DEBUG_ASSERT(timeout >= std::chrono::milliseconds{1});
	DEBUG_ASSERT(std::chrono::duration_cast<std::chrono::seconds>(timeout).count() <= std::numeric_limits<int>::max());
	const ::linger details{
		1,	  // Enabled.
		static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(timeout).count())};	 // Seconds.
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_LINGER.", ::setsockopt, socket_, SOL_SOCKET, SO_LINGER, &details, sizeof(::linger));
	// man implies milliseconds, then mentions minutes, [RFC0793] "USER TIMEOUT" does not directly specify units, only notes in 3.8.Interfaces/User/TCP Interface/ TCP User Commands/Open: "The present global default is five minutes." Which implies units on minutes. Values over 200 seem to "fail", which implies the units are not milliseconds!
	//	auto sock_timeout= static_cast<unsigned int>(timeout.count());
	unsigned int sock_timeout= 0;	  // Use system default.
	JMMCG_SYSCALL_WRAPPER("Unable to set TCP_USER_TIMEOUT.", ::setsockopt, socket_, SOL_SOCKET, TCP_USER_TIMEOUT, &sock_timeout, sizeof(unsigned int));
	int val= 1;
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_REUSEADDR.", ::setsockopt, socket_, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	JMMCG_SYSCALL_WRAPPER("Unable to set TCP_NODELAY.", ::setsockopt, socket_, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val));
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_SNDBUF.", ::setsockopt, socket_, SOL_SOCKET, SO_SNDBUF, &max_message_size, sizeof(max_message_size));
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_RCVBUF.", ::setsockopt, socket_, SOL_SOCKET, SO_RCVBUF, &max_message_size, sizeof(max_message_size));
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_RCVLOWAT.", ::setsockopt, socket_, SOL_SOCKET, SO_RCVLOWAT, &min_message_size, sizeof(min_message_size));
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_PRIORITY.", ::setsockopt, socket_, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_INCOMING_CPU.", ::setsockopt, socket_, SOL_SOCKET, SO_INCOMING_CPU, &incoming_cpu, sizeof(incoming_cpu));
}

template<class LkT>
inline void
wrapper<LkT>::close() noexcept(false) {
	::linger details{
		1,	  // Enabled.
		1};
	JMMCG_SYSCALL_WRAPPER("Unable to set SO_LINGER.", ::setsockopt, socket_, SOL_SOCKET, SO_LINGER, &details, sizeof(::linger));
	unsigned int sock_timeout= 1;
	JMMCG_SYSCALL_WRAPPER("Unable to set TCP_USER_TIMEOUT.", ::setsockopt, socket_, SOL_SOCKET, TCP_USER_TIMEOUT, &sock_timeout, sizeof(unsigned int));
	// Perform a TCP half-close: no longer permit sending, but received until the connected client closes. See section 18.5 in W.R.Stevens "TCP/IP Illustrated. The Protocols", Vol 1, 1st Edition.
	::shutdown(socket_, SHUT_WR);
}

template<class LkT>
inline void
wrapper<LkT>::ignore_sigpipe_for_a_socket_that_closes() noexcept(false) {
	JMMCG_SYSCALL_WRAPPER("Could not ignore SIGPIPE.", ::signal, SIGPIPE, SIG_IGN);
}

template<class LkT>
inline wrapper<LkT>::wrapper(socket_type opened_skt) noexcept(false)
	: domain_(domain_t::unknown),
	  socket_(opened_skt) {
	DEBUG_ASSERT(socket_);
	ignore_sigpipe_for_a_socket_that_closes();
}

template<class LkT>
inline wrapper<LkT>::wrapper(type_t::element_type type, domain_t::element_type domain, int protocol) noexcept(false)
	: domain_(domain),
	  socket_(JMMCG_SYSCALL_WRAPPER("Unable to create the socket.", ::socket, domain, static_cast<typename type_t::underlying_type_t>(type) | SOCK_CLOEXEC, protocol)) {
	ignore_sigpipe_for_a_socket_that_closes();
}

template<class LkT>
inline wrapper<LkT>::~wrapper() noexcept(true) {
	::close(socket_);
}

template<class LkT>
template<class MsgT>
inline void
wrapper<LkT>::write(MsgT const& message) noexcept(false) {
	const typename thread_traits::cancellability set;
	DEBUG_ASSERT(message.is_valid());
	ssize_t bytes_written= 0;
	const write_lock_t lk(mutex_);
	if constexpr(MsgT::has_static_size) {
		DEBUG_ASSERT(message.length() == sizeof(MsgT));
		// Must be done in a loop because in very rare circumstances under high loads, we can get partial writes.
		while(static_cast<std::size_t>(bytes_written) < sizeof(MsgT)) {
			const ssize_t ret_code= ::write(socket_, reinterpret_cast<std::byte const*>(&message) + bytes_written, sizeof(MsgT) - bytes_written);
			if(ret_code > 0) [[likely]] {
				bytes_written+= ret_code;
			} else if(ret_code < 0) [[unlikely]] {
				const int err= errno;
				BOOST_MPL_ASSERT_RELATION(EAGAIN, ==, EWOULDBLOCK);
				switch(err) {
				case EINTR:
				case EISCONN:
				case ETIMEDOUT:
					break;
				case EAGAIN:
				case EBADF:
				case EIO:
				case EPIPE:
				case ENETDOWN:
				case ENETUNREACH:
				case ENETRESET:
				case ECONNABORTED:
				case ECONNRESET:
				case ENOTCONN:
				case ESHUTDOWN:
					return;
				default:
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send='{}', size={}, bytes written={}", boost::core::demangle(typeid(MsgT).name()), sizeof(MsgT), bytes_written), "write", static_cast<void (wrapper::*)(MsgT const&)>(&wrapper<LkT>::write<MsgT>)));
				}
			} else {
				DEBUG_ASSERT(ret_code == 0);	 // EOF
				return;
			}
		}
		DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) == sizeof(MsgT));
	} else {
		// Must be done in a loop because in very rare circumstances under high loads, we can get partial writes.
		while(static_cast<std::size_t>(bytes_written) < message.length()) {
			const ssize_t ret_code= ::write(socket_, reinterpret_cast<std::byte const*>(&message) + bytes_written, message.length() - bytes_written);
			if(ret_code > 0) [[likely]] {
				bytes_written+= ret_code;
			} else if(ret_code < 0) [[unlikely]] {
				const int err= errno;
				BOOST_MPL_ASSERT_RELATION(EAGAIN, ==, EWOULDBLOCK);
				switch(err) {
				case EINTR:
				case EISCONN:
				case ETIMEDOUT:
					break;
				case EAGAIN:
				case EBADF:
				case EIO:
				case EPIPE:
				case ENETDOWN:
				case ENETUNREACH:
				case ENETRESET:
				case ECONNABORTED:
				case ECONNRESET:
				case ENOTCONN:
				case ESHUTDOWN:
					return;
				default:
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send='{}', size={}, bytes written={}", boost::core::demangle(typeid(MsgT).name()), message.length(), bytes_written), "write", static_cast<void (wrapper::*)(MsgT const&)>(&wrapper<LkT>::write<MsgT>)));
				}
			} else {
				DEBUG_ASSERT(ret_code == 0);	 // EOF
				return;
			}
		}
		DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) <= sizeof(MsgT));
		DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) == message.length());
	}
}

template<class LkT>
template<class V, std::size_t N>
inline void
wrapper<LkT>::write(std::array<V, N> const& message) noexcept(false) {
	const typename thread_traits::cancellability set;
	ssize_t bytes_written= 0;
	const write_lock_t lk(mutex_);
	// Must be done in a loop because in very rare circumstances under high loads, we can get partial writes.
	while(static_cast<std::size_t>(bytes_written) < (sizeof(V) * N)) {
		const ssize_t ret_code= ::write(socket_, reinterpret_cast<std::byte const*>(message.data()) + bytes_written, sizeof(V) * N - bytes_written);
		if(ret_code > 0) [[likely]] {
			bytes_written+= ret_code;
		} else if(ret_code < 0) [[unlikely]] {
			const int err= errno;
			BOOST_MPL_ASSERT_RELATION(EAGAIN, ==, EWOULDBLOCK);
			switch(err) {
			case EINTR:
			case EISCONN:
			case ETIMEDOUT:
				break;
			case EAGAIN:
			case EBADF:
			case EIO:
			case EPIPE:
			case ENETDOWN:
			case ENETUNREACH:
			case ENETRESET:
			case ECONNABORTED:
			case ECONNRESET:
			case ENOTCONN:
			case ESHUTDOWN:
				return;
			default:
				BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send='{}', number of bytes to send={}, bytes written={}", boost::core::demangle(typeid(std::array<V, N>).name()), N, bytes_written), "write", static_cast<void (wrapper::*)(std::array<V, N> const&)>(&wrapper<LkT>::write<V, N>)));
			}
		} else {
			DEBUG_ASSERT(ret_code == 0);	 // EOF
			return;
		}
	}
	DEBUG_ASSERT(bytes_written > 0);
	DEBUG_ASSERT(static_cast<std::size_t>(bytes_written) == sizeof(V) * N);
}

template<class LkT>
inline bool
wrapper<LkT>::read_msg(std::byte* dest, std::size_t msg_size) noexcept(false) {
	if(msg_size > 0) {
		const typename thread_traits::cancellability set;
		// Try to avoid repeated calls to the kernel, etc, whilst receiving the message: do it in one lot, hopefully...
		// Sadly this seems to slow it down by about 7%...
		//		std::size_t bytes_to_read=msg_size-1;
		//		::setsockopt(socket_, SOL_SOCKET, SO_RCVLOWAT, &bytes_to_read, sizeof(bytes_to_read));
		std::size_t bytes_read= 0;
		while(bytes_read < msg_size) [[unlikely]] {
			const ssize_t ret_code= ::read(socket_, dest + bytes_read, msg_size - bytes_read);
			if(ret_code > 0) [[likely]] {
				DEBUG_ASSERT((bytes_read + ret_code) <= msg_size);
				bytes_read+= ret_code;
			} else if(ret_code < 0) [[unlikely]] {
				const int err= errno;
				BOOST_MPL_ASSERT_RELATION(EAGAIN, ==, EWOULDBLOCK);
				switch(err) {
				case EINTR:
				case EISCONN:
				case ETIMEDOUT:
					break;
				case EAGAIN:
				case EBADF:
				case EFAULT:
				case EIO:
				case ENETDOWN:
				case ENETUNREACH:
				case ENETRESET:
				case ECONNABORTED:
				case ECONNRESET:
				case ENOTCONN:
				case ESHUTDOWN:
					return true;
				default:
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to read the portion of the message from the socket. destination buffer=0x{}, number of bytes to read={}, bytes read={}", static_cast<void const*>(dest), msg_size, bytes_read), "read", &wrapper::read_msg));
				}
			} else {
				DEBUG_ASSERT(ret_code == 0);	 // EOF
				return true;
			}
		}
		DEBUG_ASSERT(bytes_read == msg_size);
	}
	return false;
}

template<class LkT>
template<class MsgT>
inline bool
wrapper<LkT>::read(MsgT& dest) noexcept(false) {
	if constexpr(MsgT::has_static_size) {
		DEBUG_ASSERT(dest.length() <= sizeof(MsgT));
		BOOST_MPL_ASSERT_RELATION(sizeof(MsgT), <=, SSIZE_MAX);
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), sizeof(MsgT));
		DEBUG_ASSERT(failed || dest.is_valid());
		return failed;
	} else {
		constexpr const std::size_t header_t_sz= MsgT::header_t_size;
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), header_t_sz);
		if(!failed) [[likely]] {
			auto const* hdr= reinterpret_cast<typename MsgT::Header_t const*>(&dest);
			const std::size_t length= hdr->length();
			if(length >= header_t_sz && length <= SSIZE_MAX && length <= sizeof(MsgT)) {
				const std::size_t body_size= length - header_t_sz;
				if(body_size > 0) {
					return read_msg(reinterpret_cast<std::byte*>(&dest) + header_t_sz, length - header_t_sz);
				} else {
					DEBUG_ASSERT(hdr->is_valid());
					return failed;
				}
			} else {
				BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to read the message from the socket. destination buffer=0x{}, type of message to read='{}', into buffer of size={}, from the header the number of bytes to read={}, type of header read='{}'", static_cast<void const*>(&dest), boost::core::demangle(typeid(MsgT).name()), sizeof(MsgT), length, boost::core::demangle(typeid(typename MsgT::Header_t).name())), &wrapper::read<MsgT>));
			}
		} else [[unlikely]] {
			return failed;
		}
	}
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
wrapper<LkT>::read(V (&dest)[SrcSz]) noexcept(false) {
	BOOST_MPL_ASSERT_RELATION(sizeof(V) * SrcSz, <=, SSIZE_MAX);
	return read_msg(dest, sizeof(V) * SrcSz);
}

template<class LkT>
template<class MsgDetails, class V, std::size_t N>
inline bool
wrapper<LkT>::read(std::array<V, N>& buff) noexcept(false) {
	using msg_details_t= MsgDetails;

	BOOST_MPL_ASSERT_RELATION(msg_details_t::max_msg_size, >=, msg_details_t::header_t_size);

	const bool failed= read_msg(reinterpret_cast<std::byte*>(buff.data()), msg_details_t::header_t_size);
	if(!failed) [[likely]] {
		auto const* hdr= reinterpret_cast<typename msg_details_t::Header_t const*>(buff.data());
		const std::size_t length= hdr->length();
		if(length >= msg_details_t::header_t_size && length <= SSIZE_MAX && length <= (N * sizeof(V))) [[likely]] {
			const std::size_t body_size= length - msg_details_t::header_t_size;
			if(body_size > 0) {
				return read_msg(reinterpret_cast<std::byte*>(buff.data()) + msg_details_t::header_t_size, length - msg_details_t::header_t_size);
			} else {
				DEBUG_ASSERT(hdr->is_valid());
				return false;
			}
		} else [[unlikely]] {
			return true;
		}
	} else [[unlikely]] {
		return failed;
	}
}

template<class LkT>
inline bool
wrapper<LkT>::is_open() const noexcept(true) {
	try {
		int error_code{};
		socklen_t error_code_size= sizeof(error_code);
		JMMCG_SYSCALL_WRAPPER("Unable to get the state of the socket.", ::getsockopt, socket_, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);
		return error_code == 0;
	} catch(...) {
		return false;
	}
}

template<class LkT>
inline std::string
wrapper<LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<< "socket_=" << socket_ << ", local IP: " << local_ip();
	return ss.str();
}

template<class LkT>
inline typename wrapper<LkT>::socket_type
wrapper<LkT>::native_handle() {
	return socket_;
}

template<class LkT>
inline std::string
wrapper<LkT>::local_ip() const noexcept(false) {
	struct sockaddr_in name {};

	socklen_t namelen= sizeof(name);
	JMMCG_SYSCALL_WRAPPER("Unable to get the local-name of the local, connected socket.", ::getsockname, socket_, reinterpret_cast<struct sockaddr*>(&name), &namelen);
	std::string buffer(2 * INET_ADDRSTRLEN, '\0');
	JMMCG_SYSCALL_WRAPPER("Failed to convert the locally connected socket name to a human-readable string.", ::inet_ntop, AF_INET, &name.sin_addr, buffer.data(), buffer.size());
	auto last_printing_char_iter= std::find_if(buffer.rbegin(), buffer.rend(), [](auto const& v) {
		return std::isprint(v);
	});
	DEBUG_ASSERT(last_printing_char_iter != buffer.rend());
	--last_printing_char_iter;
	auto const end_of_data_index= std::distance(last_printing_char_iter, buffer.rend());
	DEBUG_ASSERT(end_of_data_index >= 0);
	buffer.resize(end_of_data_index);
	return buffer;
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, wrapper<LkT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}

namespace client {

template<class LkT>
inline void
wrapper<LkT>::connect(char const* addr, std::uint16_t port) noexcept(false) {
	::sockaddr_in addr_in{};
	addr_in.sin_family= this->domain_;
	::inet_pton(this->domain_, addr, &addr_in.sin_addr);
	addr_in.sin_port= ::htons(port);
	try {
		JMMCG_SYSCALL_WRAPPER(fmt::format("Unable to connect socket to '{}:{}'.", addr, port), ::connect, this->socket_, reinterpret_cast<::sockaddr const*>(&addr_in), sizeof(::sockaddr_in));
	} catch(...) {
		this->close();
		throw;
	}
}

template<class LkT>
inline void
wrapper<LkT>::connect(struct sockaddr* addr, socklen_t addrlen) noexcept(false) {
	std::array<char, NI_MAXHOST> host_ip{};
	std::array<char, NI_MAXSERV> numeric_service{};
	JMMCG_SYSCALL_WRAPPER("Unable to get IP details for the supplied address.", ::getnameinfo, addr, addrlen, host_ip.begin(), host_ip.size(), numeric_service.begin(), numeric_service.size(), NI_NUMERICHOST | NI_NUMERICSERV);
	JMMCG_SYSCALL_WRAPPER(fmt::format("Unable to connect socket to host: {}; service: {}.", std::string_view(host_ip.begin(), std::find(host_ip.begin(), host_ip.end(), '\0')), std::string_view(numeric_service.begin(), std::find(numeric_service.begin(), numeric_service.end(), '\0'))), ::connect, this->socket_, addr, addrlen);
}

}

namespace server {

template<class LkT>
class wrapper<LkT>::recv_msg_ops_t {
public:
	std::function<void(typename socket_t::socket_type /* connected accept_socket */)> fn;

	template<class RecvProcMsgs>
	constexpr recv_msg_ops_t(RecvProcMsgs&& f) noexcept(false)
		: fn(std::forward<RecvProcMsgs>(f)) {
	}
};

template<class LkT>
inline wrapper<LkT>::wrapper(char const* addr, std::uint16_t port, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, type_t::element_type type, domain_t::element_type domain) noexcept(false)
	: base_t(type, domain) {
	::sockaddr_in addr_in{};
	addr_in.sin_family= this->domain_;
	::inet_pton(this->domain_, addr, &addr_in.sin_addr);
	addr_in.sin_port= htons(port);
	std::fill_n(addr_in.sin_zero, sizeof(addr_in.sin_zero), 0);
	try {
		JMMCG_SYSCALL_WRAPPER(fmt::format("Unable to bind socket to '{}:{}'.", addr, port), ::bind, this->socket_, reinterpret_cast<::sockaddr const*>(&addr_in), sizeof(::sockaddr_in));
		JMMCG_SYSCALL_WRAPPER(fmt::format("Unable to listen to socket bound to '{}:{}'.", addr, port), ::listen, this->socket_, max_backlog_connections);
	} catch(...) {
		this->close();
		throw;
	}
	base_t::set_options(min_message_size, max_message_size, timeout, priority, incoming_cpu);
}

template<class LkT>
inline bool
wrapper<LkT>::stopped() const noexcept(true) {
	return exited_.test(std::memory_order_relaxed);
}

template<class LkT>
inline void
wrapper<LkT>::stop() noexcept(false) {
	exit_.test_and_set(std::memory_order_seq_cst);
	this->close();
}

template<class LkT>
inline wrapper<LkT>::~wrapper() noexcept(true) {
	stop();
}

template<class LkT>
inline void
wrapper<LkT>::run() noexcept(false) {
	class set_exited {
	public:
		explicit constexpr set_exited(std::atomic_flag& exited) noexcept(true)
			: exited_(exited) {
		}

		~set_exited() noexcept(true) {
			exited_.test_and_set(std::memory_order_seq_cst);
		}

	private:
		std::atomic_flag& exited_;
	};

	const set_exited exited(exited_);
	while(!exit_.test(std::memory_order_relaxed)) {
		::pollfd fds= {
			this->socket_,
			POLLIN | POLLPRI,
			0};
		const int poll_ret= JMMCG_SYSCALL_WRAPPER("Unable to accept a client connection, re-trying.", ::poll, &fds, 1, 0);
		if(poll_ret == 0) {
			thread_traits::sleep(0);
			continue;
		} else {
			DEBUG_ASSERT(poll_ret == 1);
			::sockaddr_in client_addr{};
			::socklen_t len= sizeof(::sockaddr_in);
			const int accept_skt= JMMCG_SYSCALL_WRAPPER("Unable to accept a client connection, re-trying.", ::accept, this->socket_, reinterpret_cast<::sockaddr*>(&client_addr), &len);
			DEBUG_ASSERT(len == sizeof(sockaddr_in));
			DEBUG_ASSERT(client_addr.sin_port != 0);
			const std::shared_ptr<recv_msg_ops_t> recv_msg_ops_cpy(recv_msg_ops);
			if(recv_msg_ops_cpy.get()) {
				recv_msg_ops_cpy->fn(accept_skt);
			}
		}
	}
}

template<class LkT>
inline void
wrapper<LkT>::set_options(base_t& skt) const noexcept(false) {
	::linger details{
		0,
		0};
	socklen_t buff_sz= sizeof(details);
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_LINGER.", ::getsockopt, this->socket_, SOL_SOCKET, SO_LINGER, &details, &buff_sz);
	int snd_msg_sz= 0;
	buff_sz= sizeof(snd_msg_sz);
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_SNDBUF.", ::getsockopt, this->socket_, SOL_SOCKET, SO_SNDBUF, &snd_msg_sz, &buff_sz);
	int rcv_msg_sz= 0;
	buff_sz= sizeof(rcv_msg_sz);
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_RCVBUF.", ::getsockopt, this->socket_, SOL_SOCKET, SO_RCVBUF, &rcv_msg_sz, &buff_sz);
	int rcv_msg_lowat= 0;
	buff_sz= sizeof(rcv_msg_lowat);
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_RCVLOWAT.", ::getsockopt, this->socket_, SOL_SOCKET, SO_RCVLOWAT, &rcv_msg_lowat, &buff_sz);
	buff_sz= sizeof(socket_priority);
	socket_priority priority= socket_priority::low;
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_PRIORITY.", ::getsockopt, this->socket_, SOL_SOCKET, SO_PRIORITY, &priority, &buff_sz);
	int incoming_cpu= 0;
	buff_sz= sizeof(incoming_cpu);
	JMMCG_SYSCALL_WRAPPER("Unable to get SO_INCOMING_CPU.", ::getsockopt, this->socket_, SOL_SOCKET, SO_INCOMING_CPU, &incoming_cpu, &buff_sz);
	skt.set_options(rcv_msg_lowat, std::max(snd_msg_sz, rcv_msg_sz), std::chrono::milliseconds(details.l_linger), priority, incoming_cpu);
}

template<class LkT>
template<class RecvProcMsgs>
inline void
wrapper<LkT>::async_accept(RecvProcMsgs&& f) noexcept(false) {
	recv_msg_ops= std::make_shared<recv_msg_ops_t>(std::forward<RecvProcMsgs>(f));
}

}

}}}}
