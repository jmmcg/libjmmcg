/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline application::element_type
application::signal_status() noexcept(true) {
	return signal_status_;
}

inline application::application() noexcept(true) {
	for(auto i: handled_signals::range()) {
		std::signal(i, application::signal_handler);
	}
	std::set_terminate(boost::core::verbose_terminate_handler);
}

inline application::~application() noexcept(true) {
	for(auto i: handled_signals::range()) {
		std::signal(i, SIG_DFL);
	}
}

inline std::string
application::make_help_message(char const* email_addr) noexcept(false) {
	return std::string{" For more details, see the documentation that came with the distribution. "} + std::string{copyright} + ", " + email_addr + ". Source-code: " + url + " . " + license + "\n" + exit_codes::to_string() + "\nSignals handled: " + std::string{application::handled_signals::type_info_name()} + "\nArguments";
}

inline boost::program_options::options_description
application::make_program_options(std::string const& description) noexcept(false) {
	boost::program_options::options_description options(description);
	options.add_options()("help", "Print this help message.")("version", "Print the version details and build number of this program.");
	return options;
}

inline exit_codes::codes::element_type
application::check_basic_options(boost::program_options::variables_map const& vm, boost::program_options::options_description const& options, std::ostream& os) noexcept(false) {
	return check_basic_options(vm, options, nullptr, os);
}

inline exit_codes::codes::element_type
application::check_basic_options(boost::program_options::variables_map const& vm, boost::program_options::options_description const& options, char const* extra_details, std::ostream& os) noexcept(false) {
	if(vm.count("help")) {
		os << options << std::endl;
		return exit_codes::codes::exit_print_help;
	} else if(vm.count("version")) {
		os << LIBJMMCG_DETAILED_VERSION_INFO << (extra_details ? extra_details : "") << std::endl;
		return exit_codes::codes::exit_print_version;
	} else {
		return exit_codes::codes::exit_success;
	}
}

inline void
application::signal_handler(int signal) noexcept(true) {
	signal_status_= signal;
	exit_requested_.test_and_set();
	exit_requested_.notify_all();
}

inline std::ostream&
operator<<(std::ostream& os, application const& a) noexcept(false) {
	os << "Signals handled: " << application::handled_signals::type_info_name() << ". Current signal status=" << a.signal_status_;
	return os;
}

}}
