#ifndef LIBJMMCG_CORE_THREAD_POOL_SEQUENTIAL_HPP
#define LIBJMMCG_CORE_THREAD_POOL_SEQUENTIAL_HPP
/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "private_/thread_pool.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	/// Parallel Pixie Dust or PPD is the name of the thread library within libjmmcg.
	/**
		Why is it called "ppd"? It stands for "Parallel Pixie Dust", a term coined at the <a href="http://www.accu.org/">ACCU</a> by, if I recall correctly, Hubert Matthews in around 2001, whilst I was in conversation with him about thread libraries and their ability to shield a programmer from the issues of threading.
	*/
	namespace ppd {

	/// This compatibility-pool has an unlimited size, and uses a master to execute the work.
	/**
		There is no threading at all in this pool. It tries to be as fast as directly executing the methods
		on the closure_base-derived closure class. The idea of this class is that you can use it to provide both single &
		multi-threaded implementations within your objects and code, by just changing a typedef or a trait.
	*/
	template<
		class P
	>
	class thread_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, P>
	: public private_::sequential_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, P> {
	public:
		typedef private_::sequential_pool<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, P> base_t;
		typedef typename base_t::api_params_type api_params_type;
	};

	/// This compatibility-pool has an unlimited size, and the "worker thread"s directly execute the work.
	/**
		There is no threading at all in this pool. It tries to be as fast as directly executing the methods
		on the closure_base-derived closure class. The idea of this class is that you can use it to provide both single &
		multi-threaded implementations within your objects and code, by just changing a typedef or a trait.
	*/
	template<
		class P
	>
	class thread_pool<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, P>
	: public private_::sequential_pool<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, P> {
	public:
		typedef private_::sequential_pool<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, P> base_t;
		typedef typename base_t::os_traits os_traits;
		typedef typename base_t::api_params_type api_params_type;
		typedef typename base_t::pool_type pool_type;
		typedef typename base_t::pool_size_type pool_size_type;

		enum class erase_states {
			failed_to_erase,
			ignoring_result,
			erased_successfully
		};

		constexpr __stdcall thread_pool()
		: base_t() {
		}
		explicit __stdcall thread_pool(const pool_size_type sz)
		: base_t(sz) {
		}

		template<typename ExecT>
		static constexpr erase_states __fastcall
		erase(ExecT &) {
			return erase_states::failed_to_erase;
		}
	};

	} } }

#endif
