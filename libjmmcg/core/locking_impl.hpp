/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock {

template<class Lk>
inline typename lockable<Lk>::atomic_state_type
lockable<Lk>::try_lock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<lockable*>(this));
	return this->lock(0);
}

template<class Lk>
inline boost_lk_compat<Lk>::boost_lk_compat(atomic_t& l) noexcept(true)
	: lk(l) {
}

template<class Lk>
inline bool
boost_lk_compat<Lk>::try_lock() noexcept(noexcept(lk.try_lock())) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&lk));
	return lk.try_lock() == lock_traits::atom_set;
}

template<class Lk>
inline void
boost_lk_compat<Lk>::lock() noexcept(noexcept(lk.lock())) {
	[[maybe_unused]] const typename atomic_t::atomic_state_type ret= lk.lock();
	DEBUG_ASSERT(ret == atomic_t::atom_set);
}

template<class Lk>
inline void
boost_lk_compat<Lk>::unlock() noexcept(noexcept(lk.unlock())) {
	lk.unlock();
}

template<class LockObject>
inline scope_lock<LockObject>::scope_lock(atomic_t& lo) noexcept(true)
	: locker(lo) {
}

template<class LockObject>
inline typename scope_lock<LockObject>::atomic_state_type
scope_lock<LockObject>::try_lock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&locker));
	return locker.try_lock();
}

template<class LockObject>
inline typename scope_lock<LockObject>::atomic_state_type
scope_lock<LockObject>::lock() noexcept(false) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&locker));
	return locker.lock();
}

template<class LockObject>
inline typename scope_lock<LockObject>::atomic_state_type
scope_lock<LockObject>::lock(const timeout_type period) noexcept(false) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&locker));
	return locker.lock(period);
}

template<class LockObject>
inline typename scope_lock<LockObject>::atomic_state_type
scope_lock<LockObject>::unlock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&locker));
	return locker.unlock();
}

template<class LockObject>
inline scope_lock<LockObject>::scope_lock(atomic_t& lo, const timeout_type period) noexcept(true)
	: locker(lo) {
	[[maybe_unused]] const typename atomic_t::atomic_state_type ret= lock(period);
	DEBUG_ASSERT(ret == atomic_t::lock_traits::atom_set);
}

template<class LockObject>
inline scope_lock<LockObject>::~scope_lock() noexcept(true) {
	unlock();
}

template<class LockObject>
inline void
scope_lock<LockObject>::decay() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&locker));
	return locker.decay();
}

template<class LockObject>
inline in_process<LockObject>::in_process(atomic_t& lo, const timeout_type period) noexcept(true)
	: scope_lock<LockObject>(lo, period) {
}

template<class LockObject>
inline in_process<LockObject>::~in_process() noexcept(true) {
}

template<class LockObject>
inline typename in_process<LockObject>::atomic_state_type
in_process<LockObject>::try_lock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	return this->locker.try_lock();
}

template<class LockObject>
inline typename in_process<LockObject>::atomic_state_type
in_process<LockObject>::lock() noexcept(false) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	return this->locker.lock();
}

template<class LockObject>
inline typename in_process<LockObject>::atomic_state_type
in_process<LockObject>::lock(const timeout_type period) noexcept(false) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	return this->locker.lock(period);
}

template<class LockObject>
inline typename in_process<LockObject>::atomic_state_type
in_process<LockObject>::unlock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	return this->locker.unlock();
}

template<class LockObject>
inline void
in_process<LockObject>::decay() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	return this->locker.decay();
}

template<class LockObject>
inline in_process_unlockable<LockObject>::in_process_unlockable(atomic_t& lo) noexcept(true)
	: locker(lo) {
}

template<class LockObject>
inline in_process_unlockable<LockObject>::in_process_unlockable(atomic_t& lo, const timeout_type period) noexcept(true)
	: locker(lo) {
	const typename atomic_t::atomic_state_type ret= this->locker.lock(period);
	if(ret == atomic_t::lock_traits::atom_set) {
		locked.test_and_set(std::memory_order_seq_cst);
	} else {
		locked.clear(std::memory_order_seq_cst);
	}
}

template<class LockObject>
inline typename in_process_unlockable<LockObject>::atomic_state_type
in_process_unlockable<LockObject>::unlock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	locked.clear(std::memory_order_seq_cst);
	return this->locker.unlock();
}

template<class LockObject>
inline in_process_unlockable<LockObject>::~in_process_unlockable() noexcept(true) {
	if(locked.test(std::memory_order_relaxed)) {
		unlock();
		locked.clear(std::memory_order_seq_cst);
	}
}

template<class LockObject>
inline typename in_process_unlockable<LockObject>::atomic_state_type
in_process_unlockable<LockObject>::try_lock() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	const typename atomic_t::atomic_state_type ret= this->locker.try_lock();
	if(ret == atomic_t::lock_traits::atom_set) {
		locked.test_and_set(std::memory_order_seq_cst);
	} else {
		locked.clear(std::memory_order_seq_cst);
	}
	return ret;
}

template<class LockObject>
inline void
in_process_unlockable<LockObject>::decay() noexcept(true) {
	DEBUG_ASSERT(dynamic_cast<atomic_t*>(&this->locker));
	locked.clear(std::memory_order_seq_cst);
	return this->locker.decay();
}

namespace any_order {

template<generic_traits::api_type::element_type API_, typename Mdl_, class Lk1, class Lk2>
inline all<API_, Mdl_, Lk1, Lk2>::all(first_argument_type& l1, second_argument_type& l2) noexcept(noexcept(boost::lock(std::declval<first_argument_type>(), std::declval<second_argument_type>())))
	: lk1(l1), lk2(l2) {
	boost::lock(lk1, lk2);
}

template<generic_traits::api_type::element_type API_, typename Mdl_, class Lk1, class Lk2>
inline all<API_, Mdl_, Lk1, Lk2>::~all() noexcept(true) {
	lk1.unlock();
	lk2.unlock();
}

template<generic_traits::api_type::element_type API_, typename Mdl_, class Lk1, class Lk2>
inline try_one<API_, Mdl_, Lk1, Lk2>::try_one(Lk1& l1, Lk2& l2) noexcept(true)
	: lk1(l1), lk2(l2) {
}

template<generic_traits::api_type::element_type API_, typename Mdl_, class Lk1, class Lk2>
inline typename try_one<API_, Mdl_, Lk1, Lk2>::result_type
try_one<API_, Mdl_, Lk1, Lk2>::try_lock() noexcept(noexcept(boost::try_lock(std::declval<Lk1>(), std::declval<Lk2>()))) {
	return boost::try_lock(lk1, lk2);
}

}

}}}}
