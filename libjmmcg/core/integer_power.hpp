#ifndef LIBJMMCG_CORE_INTEGER_POWER_HPP
#define LIBJMMCG_CORE_INTEGER_POWER_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"
#include "debug_defines.hpp"

#include <boost/static_assert.hpp>

#include <functional>

#ifdef _MSC_VER
#	pragma warning(push)
#	pragma warning(disable:4127)	  ///< Conditional expression is constant.
#endif

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// The result is true if the value V is positive, otherwise false.
template<
	auto V	///< The value to test.
	>
struct sign {
	static inline constexpr bool result= (V >= 0);
};

/// The result is the modulus of the value, V.
template<
	auto V	///< The value to test.
	>
struct modulus {
	static inline constexpr const auto result= (sign<V>::result ? V : -V);
};

/// Invert the value V if the boolean A is false.
template<
	typename V,	  ///< The value to potentially invert.
	bool A	///< Condition upon which to invert.
	>
struct invert_if {
	using result_type= V;
	using argument_type= V;

	static constexpr result_type __fastcall result(argument_type a) noexcept(true) {
		return a;
	}
};

template<typename V>
struct invert_if<V, false> {
	using result_type= V;
	using argument_type= V;

	static constexpr result_type __fastcall result(argument_type a) noexcept(true) {
		return result_type(1 / a);
	}
};

/// An implementation of the binary-right-to-left method of exponentiation for raising a number to positive, integer power.
/**
	This is an implementation of the binary-right-to-left exponentiation algorithm from
	"The Art of Computer Programming, Vol 2, Seminumerical Algorithms", Knuth.
	The runtime-algorithmic complexity of this function is O(M), where M is the algorithmic complexity of multiplication on the type V.
	The compile-time algorithmic complexity of this function is O(lg(power)+v(power)), where v(power) is the number of 1s in the binary representation of power.
*/
namespace binary_right_to_left {

/// For types for which operator*() cannot be computed at compile-time, this variant unrolls the exponentiation at compile-time.
namespace dyn {

template<
	typename V,	  ///< The type of the value to exponentiate.
	unsigned long long P	  ///< The power to which it should be raised. This should be positive.
	>
struct pow;

template<
	typename V	 ///< The type of the value to exponentiate.
	>
struct pow<V, 0ULL> {
	using result_type= V;
	using first_argument_type= V;

	[[nodiscard]] static constexpr result_type __fastcall result(first_argument_type res, const first_argument_type) noexcept(true) {
		return result_type(res);
	}

	[[nodiscard]] static constexpr result_type __fastcall result(const first_argument_type) noexcept(true) {
		return result_type(1);
	}
};

template<
	typename V	 ///< The type of the value to exponentiate.
	>
struct pow<V, 1ULL> {
	using result_type= V;
	using first_argument_type= V;

	[[nodiscard]] static constexpr result_type __fastcall result(first_argument_type res, const first_argument_type a) noexcept(true) {
		return result_type(res * a);
	}

	[[nodiscard]] static constexpr result_type __fastcall result(const first_argument_type a) noexcept(true) {
		return result_type(a);
	}
};

template<
	typename V,	  ///< The type of the value to exponentiate.
	unsigned long long P	  ///< The power to which it should be raised. This should be positive.
	>
struct pow {
	using result_type= V;
	using first_argument_type= V;

	static inline constexpr unsigned long long power= P;	 ///< The positive power to which the value should be raised.

	[[nodiscard]] static constexpr result_type __fastcall result(first_argument_type res, const first_argument_type z) noexcept(true) {
		BOOST_STATIC_ASSERT(power >= 0);
		if(power & 1) {
			res*= z;
		}
		return pow<result_type, (power >> 1)>::result(res, z * z);
	}

	/**
		The runtime-algorithmic complexity of this function is O(M), where M is the algorithmic complexity of multiplication on the type V.
		The compile-time algorithmic complexity of this function is O(lg(power)+v(power)), where v(power) is the number of 1s in the binary representation of power.
	*/
	[[nodiscard]] static constexpr result_type __fastcall result(const first_argument_type a) noexcept(true) {
		BOOST_STATIC_ASSERT(power >= 0);
		return pow<result_type, power>::result(1, a);
	}
};

}

/// For types for which operator*() can be computed at compile-time, this variant computes the entire exponentiation at compile-time.
namespace mpl {

namespace private_ {

template<
	unsigned long long P,
	auto Res,
	auto V>
struct pow;

template<
	auto Res,
	auto V>
struct pow<0ULL, Res, V> {
	enum : decltype(Res) {
		result= Res
	};
};

template<
	unsigned long long P,
	auto Res,
	auto V>
struct pow {
	static inline constexpr auto result= pow<(P >> 1), ((P & 1) ? Res * V : Res), V * V>::result;
};

}

template<
	auto V,	 ///< The value to exponentiate.
	long long P	  ///< The power to which the value should be raised. Negative powers are computed as 1/V^|P|.
	>
struct pow;

template<
	auto V	///< The value to exponentiate.
	>
struct pow<V, 0ULL> {
	static inline constexpr const long long power= 0;	 ///< The power to which the value should be raised. Negative powers are computed as 1/V^|P|.
	static inline constexpr const auto value= V;	  ///< The value to exponentiate.
	static inline constexpr const decltype(V) result= 1;
};

template<
	auto V	///< The value to exponentiate.
	>
struct pow<V, 1ULL> {
	static inline constexpr const long long power= 1;	 ///< The power to which the value should be raised. Negative powers are computed as 1/V^|P|.
	static inline constexpr const auto value= V;	  ///< The value to exponentiate.
	static inline constexpr const decltype(V) result= value;
};

/// The class to compute the result of raising a value V to an integer power P, at compile-time.
/**
	The runtime algorithmic-complexity of this function is O(1).
	The compile-time algorithmic complexity of this function is O(lg(power)+v(power)), where v(power) is the number of 1s in the binary representation of power.
*/
template<
	auto V,	 ///< The value to exponentiate.
	long long P	  ///< The power to which the value should be raised. Negative powers are computed as 1/V^|P|.
	>
struct pow {
	static inline constexpr const long long power= P;	 ///< The power to which the value should be raised.
	static inline constexpr const auto value= V;	  ///< The value to exponentiate. Negative powers are computed as 1/V^|P|.

private:
	using element_type= typename std::conditional<sign<V>::result, unsigned long long, long long>::type;

	static inline constexpr unsigned long long mod_power= modulus<power>::result;
	static inline constexpr auto int_result= private_::pow<(mod_power >> 1), ((mod_power & 1) ? element_type(value) : element_type(1)), element_type(value) * element_type(value)>::result;

public:
	static inline constexpr auto result= invert_if<element_type, sign<P>::result>::result(int_result);
};

namespace test {

BOOST_MPL_ASSERT_RELATION((pow<0, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<1, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<2, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<3, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<-1, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<-2, 0>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<-3, 0>::result), ==, 1);

BOOST_MPL_ASSERT_RELATION((pow<0, 1>::result), ==, 0);
BOOST_MPL_ASSERT_RELATION((pow<1, 1>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<2, 1>::result), ==, 2);
BOOST_MPL_ASSERT_RELATION((pow<3, 1>::result), ==, 3);
BOOST_MPL_ASSERT_RELATION((pow<-1, 1>::result), ==, -1);
BOOST_MPL_ASSERT_RELATION((pow<-2, 1>::result), ==, -2);
BOOST_MPL_ASSERT_RELATION((pow<-3, 1>::result), ==, -3);

BOOST_MPL_ASSERT_RELATION((pow<0, 2>::result), ==, 0);
BOOST_MPL_ASSERT_RELATION((pow<1, 2>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<2, 2>::result), ==, 4);
BOOST_MPL_ASSERT_RELATION((pow<3, 2>::result), ==, 9);
BOOST_MPL_ASSERT_RELATION((pow<-1, 2>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<-2, 2>::result), ==, 4);
BOOST_MPL_ASSERT_RELATION((pow<-3, 2>::result), ==, 9);

BOOST_MPL_ASSERT_RELATION((pow<0, 3>::result), ==, 0);
BOOST_MPL_ASSERT_RELATION((pow<1, 3>::result), ==, 1);
BOOST_MPL_ASSERT_RELATION((pow<2, 3>::result), ==, 8);
BOOST_MPL_ASSERT_RELATION((pow<3, 3>::result), ==, 27);
BOOST_MPL_ASSERT_RELATION((pow<-1, 3>::result), ==, -1);
BOOST_MPL_ASSERT_RELATION((pow<-2, 3>::result), ==, -8);
BOOST_MPL_ASSERT_RELATION((pow<-3, 3>::result), ==, -27);

BOOST_MPL_ASSERT_RELATION((pow<36, 1>::result), ==, 36ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 2>::result), ==, 1296ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 3>::result), ==, 46656ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 4>::result), ==, 1679616ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 5>::result), ==, 60466176ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 6>::result), ==, 2176782336ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 7>::result), ==, 78364164096ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 8>::result), ==, 2821109907456ULL);
BOOST_MPL_ASSERT_RELATION((pow<36, 9>::result), ==, 101559956668416ULL);

}

}

}

/// At compile-time, using the binary-right-to-left method, unroll the exponentiation of raising the value V to the integer power of P.
/**
	The runtime-algorithmic complexity of this function is O(M), where M is the algorithmic complexity of multiplication on the type V.
	The compile-time algorithmic complexity of this function is O(lg(power)+v(power)), where v(power) is the number of 1s in the binary representation of power.

	\param	v	The value to be exponentiated.
*/
template<
	long long P,	///< The integer power. Negative powers are computed as 1/V^|P|.
	typename V	 ///< The type of the value to be exponentiated.
	>
[[gnu::const]] inline constexpr V __fastcall pow(const V v) {
	return invert_if<V, sign<P>::result>::result(binary_right_to_left::dyn::pow<V, modulus<P>::result>::result(v));
}

}}

#ifdef _MSC_VER
#	pragma warning(pop)
#endif

#endif
