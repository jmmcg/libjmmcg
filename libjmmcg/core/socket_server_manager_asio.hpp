#ifndef LIBJMMCG_CORE_SERVER_MANAGER_ASIO_HPP
#define LIBJMMCG_CORE_SERVER_MANAGER_ASIO_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_wrapper_asio.hpp"

#include <functional>
#include <memory>

#ifdef __GCC__
#	pragma GCC diagnostic push
#	pragma GCC diagnostic ignored "-Wsuggest-final-methods"
#	pragma GCC diagnostic ignored "-Wsuggest-final-types"
#endif

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace server_manager { namespace asio {

/// A simple TCP/IP socket wrapper using socket_wrapper for servers.
/**
	Developed from:
	<a href="https://www.boost.org/doc/libs/1_71_0/doc/html/boost_asio/example/cpp11/echo/blocking_tcp_echo_client.cpp"/>

	\see socket::asio::socket_wrapper
*/
template<class LkT>
class manager {
public:
	using acceptor_t= boost::asio::ip::tcp::acceptor;
	using socket_t= socket::asio::socket_wrapper<LkT>;
	using socket_priority= typename socket_t::socket_priority;
	using thread_traits= ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>;
	/// If the current processor detects a failure (via an exception thrown) when running the run() method, it will use an instance of this call-back so that this error may be alternatively handled.
	/**
	 * Note that a reference to this operation is taken, therefore it should be re-entrant, i.e. thread-safe. Moreover the operation must not leak any exceptions otherwise the behaviour is undefined.
	 *
	 * \param svr_details	The details of the server from which the error originated.
	 * \param eptr	Any exception-pointer that was caught in the process of running the server.
	 */
	using report_error_fn_t= std::function<void(std::string svr_details, std::exception_ptr eptr)>;

	/// A wrapper for a new client connection to the server.
	class session : public std::enable_shared_from_this<session> {
	public:
		using ptr_type= std::shared_ptr<session>;

		explicit session(boost::asio::ip::tcp::socket&& socket)
			: socket_(std::move(socket)) {
			// Stuff to set once the socket is open...
		}

		virtual ~session()= default;

		/// For loop-back connections, such as simulators.
		template<class RecvProcMsgs>
		bool process_loopback(RecvProcMsgs proc_fn) noexcept(false) {
			return proc_fn(*this, *this);
		}

		/// For forwarding connections, such as the translator.
		template<class RecvProcMsgs>
		bool process_forwarding(RecvProcMsgs proc_fn) noexcept(false) {
			return proc_fn(*this);
		}

		virtual void start() {
		}

		virtual void stop() {
			socket_.close();
		}

		socket_t& socket() noexcept(true) {
			return socket_;
		}

		[[nodiscard]] std::string to_string() const noexcept(false);

		static ptr_type make(boost::asio::ip::tcp::socket&& accept_socket, report_error_fn_t const&) noexcept(false) {
			return std::make_shared<typename ptr_type::element_type>(std::move(accept_socket));
		}

	protected:
		socket_t socket_;
	};
	using server_to_client_flow_t= std::function<void(typename session::ptr_type)>;

	/// Create a new connection to the specified TCP socket using the TCP/IP protocol.
	/**
		\param	addr	The IPv4 or IPv6 address to which the connection should be made.
		\param	port_num	The port number to which the connection should be made.
	*/
	manager(std::atomic_flag& exit_requested, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, server_to_client_flow_t&& server_to_client_flow);
	virtual ~manager()= default;

	/// Blocking wait for a new connection from a client.
	void run();

	void stop();

	static void set_options(acceptor_t& acceptor, socket_t& skt);

	[[nodiscard]] virtual std::string to_string() const noexcept(false);

protected:
	struct reset_client;
	std::atomic_flag& exit_requested_;
	boost::asio::io_context io_context{};
	acceptor_t acceptor;
	server_to_client_flow_t server_to_client_flow_;

	void set_options(socket_t& skt);
};

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, typename manager<LkT>::tcp_connection const& ec) noexcept(false);

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, manager<LkT> const& ec) noexcept(false);

/// A simple TCP/IP socket wrapper using boost::asio for loop-back servers.
template<
	class ProcessingRules,	 ///< The translation rules to use to convert messages received from the client to send to the server.
	class SvrHBs,
	class LkT>
class loopback : public manager<LkT> {
public:
	using base_t= manager<LkT>;
	using session= typename base_t::session;
	using msg_processor_t= msg_processor<ProcessingRules>;
	using exchg_links_t= msg_processor_t;
	using proc_rules_t= typename msg_processor_t::proc_rules_t;
	using src_msg_details_t= typename proc_rules_t::src_msg_details_t;
	using client_msg_buffer_t= typename src_msg_details_t::msg_buffer_t;
	/// Start sending heartbeats upon a connection.
	using heartbeats_t= SvrHBs;
	using socket_t= typename base_t::socket_t;
	using socket_priority= typename base_t::socket_priority;
	using server_to_client_flow_t= typename base_t::server_to_client_flow_t;
	using report_error_fn_t= typename base_t::report_error_fn_t;
	using base_t::base_t;

	loopback(std::atomic_flag& exit_requested, report_error_fn_t& report_error, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, proc_rules_t const& proc_rules, libjmmcg::latency_timestamps_itf& ts, server_to_client_flow_t&& server_to_client_flow);

	std::string to_string() const noexcept(false) override;

private:
	class send_heartbeats;

	report_error_fn_t& report_error_;
	msg_processor_t processor;

	/// Non-blocking call to wait for new connections from a client.
	/**
		\param 	proc_fn	The operations to perform upon receiving a connection request from a client.
	*/
	template<class RecvProcMsgs>
	void start_accept(RecvProcMsgs proc_fn) noexcept(false);

	/// In a single thread, handle any connections from the client connected on the specified socket and forward those messages on.
	/**
		\param src_cxn	The client socket, e.g. a FIX client.
		\param dest_skt	The destination socket, e.g. an exchange connection.
		\return False to continue processing messages, true otherwise.
	*/
	bool read_and_process_msgs(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
};

/// A simple TCP/IP socket wrapper using a socket wrapper for forwarding servers.
template<class ExchgCxns, class LkT>
class forwarding : public manager<LkT> {
public:
	using base_t= manager<LkT>;
	using exchg_links_t= ExchgCxns;
	using src_msg_details_t= typename exchg_links_t::src_msg_details_t;
	using client_msg_buffer_t= typename src_msg_details_t::msg_buffer_t;
	using proc_rules_t= exchg_links_t;
	using session= typename base_t::session;
	using socket_t= typename base_t::socket_t;
	using socket_priority= typename base_t::socket_priority;
	using server_to_client_flow_t= typename base_t::server_to_client_flow_t;
	using report_error_fn_t= typename base_t::report_error_fn_t;

	forwarding(std::atomic_flag& exit_requested, report_error_fn_t& report_error, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, exchg_links_t& exchg_links, libjmmcg::latency_timestamps_itf& ts, server_to_client_flow_t&& server_to_client_flow);

private:
	report_error_fn_t& report_error_;
	exchg_links_t& exchg_links_;

	/// Non-blocking call to wait for new connections from a client.
	/**
		\param 	proc_fn	The operations to perform upon receiving a connection request from a client.
	*/
	template<class RecvProcMsgs>
	void start_accept(RecvProcMsgs proc_fn) noexcept(false);

	/// In a single thread, handle any connections from the client connected on the specified socket and forward those messages on.
	/**
		\param src_cxn	The client socket, e.g. a FIX client.
		\return False to continue processing messages, true otherwise.
	*/
	bool read_and_process_msgs(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
};

}}}}}

#include "socket_server_manager_asio_impl.hpp"

#ifdef __GCC__
#	pragma GCC diagnostic pop
#endif

#endif
