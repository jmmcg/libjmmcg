#ifndef LIBJMMCG_CORE_TEMP_FILE_HPP
#define LIBJMMCG_CORE_TEMP_FILE_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "lfsr_counter.hpp"

#include <charconv>
#include <filesystem>
#include <fstream>
#include <thread>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Wrap up into a class creating a temporary file, so that it is deleted upon object destruction.
/**
	This class is completely ANSI. Note that the temporary file is created
	in the system temporary directory, to which the user must have access.
*/
class temp_file {
public:
	static inline constexpr char separator= '_';
	static inline constexpr std::size_t base_for_names= 36;

	const std::filesystem::path filepath_;

	[[nodiscard]] temp_file() noexcept(false);
	[[nodiscard]] explicit temp_file(std::string const& prefix, std::string const& extension) noexcept(false);
	temp_file(const temp_file&)= delete;
	~temp_file();

	[[nodiscard]] std::iostream& stream() noexcept(true);

private:
	using lfsr_t= libjmmcg::lfsr::secure<64U>;
	static inline lfsr_t lfsr_;
	std::fstream temp_file_;

	[[nodiscard]] static std::filesystem::path get_file_name(std::string const& prefix, std::string const& extension) noexcept(false);
};

inline temp_file::temp_file() noexcept(false)
	: temp_file("", "") {
}

inline temp_file::temp_file(std::string const& prefix, std::string const& extension) noexcept(false)
	: filepath_(get_file_name(prefix, extension)), temp_file_(filepath_, std::ios_base::binary | std::ios_base::trunc | std::ios_base::in | std::ios_base::out) {
}

inline temp_file::~temp_file() {
	try {
		temp_file_.close();
	} catch(...) {
		// Ignore any errors...
	}
	std::filesystem::remove(filepath_);
}

inline std::iostream&
temp_file::stream() noexcept(true) {
	return temp_file_;
}

inline std::filesystem::path
temp_file::get_file_name(std::string const& prefix, std::string const& extension) noexcept(false) {
	std::string temp_filename_buffer;
	temp_filename_buffer.reserve(prefix.size() + lfsr_t::size);
	temp_filename_buffer= prefix;
	temp_filename_buffer.resize(temp_filename_buffer.size() + +lfsr_t::size);
	auto const lfsr_value= lfsr_.next();
	const unsigned long lfsr_as_num= lfsr_value.to_ulong();
	auto const& end_of_number= std::to_chars(&*std::next(temp_filename_buffer.begin(), prefix.size()), &*temp_filename_buffer.end(), lfsr_as_num, base_for_names);
	std::filesystem::path tmp_file(&*temp_filename_buffer.begin(), end_of_number.ptr);
	tmp_file.replace_extension(extension);
	return std::filesystem::temp_directory_path() / tmp_file;
}

}}

#endif
