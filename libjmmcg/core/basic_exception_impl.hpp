/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class Exception>
NEVER_INLINE auto
throw_exception(std::string&& s, boost::stacktrace::stacktrace&& stacktrace) {
	return boost::enable_error_info(Exception(std::move(s))) << errinfo_stacktrace(std::move(stacktrace)) << errinfo_revision(std::string_view{libjmmcg_detailed_version_info});
}
template<class Exception, class Data>
NEVER_INLINE auto
throw_exception(std::string&& s, Data, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception>(std::move(s), std::move(stacktrace)) << boost::errinfo_type_info_name(boost::core::demangle(typeid(Data).name()));
}

template<class Exception>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception>(std::move(s), std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}
template<class Exception, class Data>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], Data d, int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, Data>(std::move(s), d, std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}

}}
