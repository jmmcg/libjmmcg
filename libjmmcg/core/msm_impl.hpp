/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace msm {

template<class States, class EndStates>
template<States Start, class Event, EndStates Next>
class unroll::row_types<States, EndStates>::row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr row() noexcept(true)=default;
	template<
		class Arg,
		class =typename std::enable_if<!std::is_convertible<Arg, row>::value>::type
	> explicit constexpr
	row(Arg &&arg) noexcept(noexcept(event_t(std::forward<Arg>(arg))))
	: ev(std::forward<Arg>(arg)) {}
	constexpr row(row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states
	process(states, Params &&...p) noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states
	process(states, Params &&...p) const noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

private:
	event_t ev;
};

template<class States, class EndStates>
template<States Start, class Event, EndStates Next, class Guard>
class unroll::row_types<States, EndStates>::g_row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	using guard_t=Guard;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr g_row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, g_row>::value>::type
	> explicit constexpr
	g_row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr g_row(g_row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states
	process(Params &&...p) noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states
	process(Params &&...p) const noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

private:
	event_t ev;
};

template<class Deriv>
template<class Row, class... Rows>
class unroll::state_transition_table<Deriv>::rows : public rows<Rows...> {
public:
	enum : std::size_t {
		size=sizeof...(Rows)+1
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg, Args &&...args) noexcept(
		noexcept(rows<Rows...>(std::forward<Args>(args)...))
		 && noexcept(a_transition(std::forward<Arg>(arg)))
	)
	: rows<Rows...>(std::forward<Args>(args)...), transition(std::forward<Arg>(arg)) {
		// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
		BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, size);
	}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: rows<Rows...>(r), transition(r.transition) {
	}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states process(states s, Params &&...p) const noexcept(noexcept(std::declval<a_transition>().process(s, std::forward<Params>(p)...))) {
		return s==a_transition::start ? transition.process(s, std::forward<Params>(p)...) : rows<Rows...>::process(s, std::forward<Params>(p)...);
	}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states process(states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(s, std::forward<Params>(p)...))) {
		return s==a_transition::start ? transition.process(s, std::forward<Params>(p)...) : rows<Rows...>::process(s, std::forward<Params>(p)...);
	}

private:
	a_transition transition;
};

template<class Deriv>
template<class Row>
class unroll::state_transition_table<Deriv>::rows<Row> {
public:
	enum : std::size_t {
		size=1
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg) noexcept(noexcept(a_transition(std::forward<Arg>(arg))))
	: transition(std::forward<Arg>(arg)) {}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: transition(r.transition) {}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states process(states s, Params &&...p) const noexcept(noexcept(std::declval<a_transition>().process(s, std::forward<Params>(p)...))) {
		return transition.process(s, std::forward<Params>(p)...);
	}

	template<class... Params> REALLY_FORCE_INLINE
	constexpr end_states process(states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(s, std::forward<Params>(p)...))) {
		return transition.process(s, std::forward<Params>(p)...);
	}

private:
	a_transition transition;
};

template<class STT>
template<class Arg, class... Args, class> inline constexpr
unroll::machine<STT>::machine(Arg &&arg, Args &&...args) noexcept(
	false // TODO noexcept(rows_t(std::forward<Arg>(arg), std::forward<Args>(args)...))
)
: rows_(std::forward<Arg>(arg), std::forward<Args>(args)...) {
	// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
	BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, rows_t::size);
}

template<class STT>
inline constexpr
unroll::machine<STT>::machine(machine const &m) noexcept(noexcept(rows_t(std::declval<rows_t>())))
: rows_(m.rows_) {
}

template<class STT>
template<class... Params> inline constexpr typename unroll::machine<STT>::end_states
unroll::machine<STT>::process(states s, Params &&...p) const noexcept(
	false // TODO noexcept(std::declval<rows_t>().process(s, std::forward<Params>(p)...))
) {
	return rows_.process(s, std::forward<Params>(p)...);
}

template<class STT>
template<class... Params> inline constexpr typename unroll::machine<STT>::end_states
unroll::machine<STT>::process(states s, Params &&...p) noexcept(
	false // TODO noexcept(std::declval<rows_t>().process(s, std::forward<Params>(p)...))
) {
	return rows_.process(s, std::forward<Params>(p)...);
}

template<class States, class EndStates>
template<States Start, class Event, EndStates Next>
class jump_table::row_types<States, EndStates>::row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, row>::value>::type
	> explicit constexpr
	row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr row(row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) const noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

private:
	event_t ev;
};

template<class States, class EndStates>
template<States Start, class Event, EndStates Next, class Guard>
class jump_table::row_types<States, EndStates>::g_row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	using guard_t=Guard;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr g_row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, g_row>::value>::type
	> explicit constexpr
	g_row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr g_row(g_row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) const noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

private:
	event_t ev;
};

template<class Deriv>
template<class Row>
class jump_table::state_transition_table<Deriv>::rows_details<Row> {
public:
	enum : std::size_t {
		stride=0
	};
};

template<class Deriv>
template<class Row1, class Row2>
class jump_table::state_transition_table<Deriv>::rows_details<Row1, Row2> {
public:
	BOOST_MPL_ASSERT_RELATION((static_cast<std::size_t>(Row2::start)-static_cast<std::size_t>(Row1::start)), >, 0);
	enum : std::size_t {
		stride=static_cast<std::size_t>(Row2::start)-static_cast<std::size_t>(Row1::start)
	};
};

template<class Deriv>
template<class Row1, class Row2, class... Rows>
class jump_table::state_transition_table<Deriv>::rows_details<Row1, Row2, Rows...> {
	enum : std::size_t {
		stride_before=rows_details<Row1, Row2>::stride,
		stride_after=rows_details<Row2, Rows...>::stride
	};
	BOOST_MPL_ASSERT_RELATION(stride_before, ==, stride_after);

public:
	enum : std::size_t {
		stride=stride_before
	};
};

template<class Deriv>
struct jump_table::state_transition_table<Deriv>::rows_base {
};

template<class Deriv>
template<class Row>
class jump_table::state_transition_table<Deriv>::rows<Row> : public rows_base {
public:
	enum : std::size_t {
		size=1,
		stride=1
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg) noexcept(noexcept(a_transition(std::forward<Arg>(arg))))
	: transition(std::forward<Arg>(arg)) {
	}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: transition(r.transition) {
	}

	template<class... Params>
	static constexpr end_states process(rows_base &row, [[maybe_unused]] states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(std::forward<Params>(p)...))) {
		DEBUG_ASSERT(s==rows::a_transition::start);
		return static_cast<rows &>(row).transition.process(std::forward<Params>(p)...);
	}

	template<class... Params>
	struct fn_table_t final : private non_newable {
		using process_fn_ptr_t=end_states (*)(rows_base &, states, Params &&...);
		using element_type=std::array<process_fn_ptr_t, size>;

		ALIGN_TO_L1_CACHE static inline constexpr element_type fn_table{
			&rows::process<Params...>
		};
	};

private:
	a_transition transition;
};

template<class Deriv>
template<class Row, class... Rows>
class jump_table::state_transition_table<Deriv>::rows : public rows<Rows...> {
public:
	enum : std::size_t {
		size=sizeof...(Rows)+1,
		stride=rows_details<Row, Rows...>::stride
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg, Args &&...args) noexcept(
		noexcept(rows<Rows...>(std::forward<Args>(args)...))
		 && noexcept(a_transition(std::forward<Arg>(arg)))
	)
	: rows<Rows...>(std::forward<Args>(args)...), transition(std::forward<Arg>(arg)) {
		// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
		BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, size);
	}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: rows<Rows...>(r), transition(r.transition) {
	}

	template<class... Params>
	static constexpr end_states process(rows_base &row, [[maybe_unused]] states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(std::forward<Params>(p)...))) {
		DEBUG_ASSERT(s==static_cast<rows &>(row).a_transition::start);
		return static_cast<rows &>(row).transition.process(std::forward<Params>(p)...);
	}

	template<class... Params>
	struct fn_table_t final : private non_newable {
		using process_fn_ptr_t=end_states (*)(rows_base &, states, Params &&...);
		using element_type=std::array<process_fn_ptr_t, size>;

		/**
		 * This is very sad: the table may be constexpr, but is not really... So it must be loaded at initialisation-time, thus is in the data-cache, not instruction-cache.
		 * Thus significant performance has been lost. Hence why I do not like constexpr & prefer enums (where one can): it leads one into thinking it is compile-time, when it is not.
		 */
		ALIGN_TO_L1_CACHE static inline constexpr element_type fn_table{
			&rows<Row>::template process<Params...>,
			&rows<Rows>::template process<Params...>...
		};
	};

private:
	a_transition transition;
};

template<class STT>
template<class Arg, class... Args, class> inline constexpr
jump_table::machine<STT>::machine(Arg &&arg, Args &&...args) noexcept(noexcept(rows_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
: rows_(std::forward<Arg>(arg), std::forward<Args>(args)...) {
	// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
	BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, rows_t::size);
}

template<class STT>
inline constexpr
jump_table::machine<STT>::machine(machine const &m) noexcept(noexcept(rows_t(std::declval<rows_t>())))
: rows_(m.rows_) {
}

template<class STT>
template<class... Params> inline constexpr typename jump_table::machine<STT>::end_states
jump_table::machine<STT>::process(states s, Params &&...p) const noexcept(false) {
	using fn_table_t=typename rows_t::template fn_table_t<Params...>;
	constexpr auto offset=static_cast<typename std::underlying_type<states>::type>(element_type::transition_table::rows::a_transition::start);
	const auto index=static_cast<typename std::underlying_type<states>::type>(s)/element_type::transition_table::rows::stride-offset;
	DEBUG_ASSERT(index>=0);
	return (*fn_table_t::fn_table[index])(rows_, s, std::forward<Params>(p)...);
}

template<class STT>
template<class... Params> inline constexpr typename jump_table::machine<STT>::end_states
jump_table::machine<STT>::process(states s, Params &&...p) noexcept(false) {
	using fn_table_t=typename rows_t::template fn_table_t<Params...>;
	constexpr auto offset=static_cast<typename std::underlying_type<states>::type>(element_type::transition_table::rows::a_transition::start);
	const auto index=static_cast<typename std::underlying_type<states>::type>(s)/element_type::transition_table::rows::stride-offset;
	DEBUG_ASSERT(index>=0);
	return (*fn_table_t::fn_table[index])(rows_, s, std::forward<Params>(p)...);
}

template<class States, class EndStates>
template<States Start, class Event, EndStates Next>
class computed_goto::row_types<States, EndStates>::row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, row>::value>::type
	> explicit constexpr
	row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr row(row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) const noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		return ev.template process<start, next>(std::forward<Params>(p)...);
	}

private:
	event_t ev;
};

template<class States, class EndStates>
template<States Start, class Event, EndStates Next, class Guard>
class computed_goto::row_types<States, EndStates>::g_row final {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	using guard_t=Guard;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr g_row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, g_row>::value>::type
	> explicit constexpr
	g_row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr g_row(g_row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

	template<class... Params>
	constexpr end_states
	process(Params &&...p) const noexcept(
		noexcept(guard_t(std::forward<Params>(p)...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Params>(p)...))
	) {
		if (guard_t(std::forward<Params>(p)...)) {
			return ev.template process<start, next>(std::forward<Params>(p)...);
		} else {
			return start;
		}
	}

private:
	event_t ev;
};

template<class Deriv>
template<class Row>
class computed_goto::state_transition_table<Deriv>::rows_details<Row> {
public:
	enum : std::size_t {
		stride=0
	};
};

template<class Deriv>
template<class Row1, class Row2>
class computed_goto::state_transition_table<Deriv>::rows_details<Row1, Row2> {
public:
	BOOST_MPL_ASSERT_RELATION((static_cast<std::size_t>(Row2::start)-static_cast<std::size_t>(Row1::start)), >, 0);
	enum : std::size_t {
		stride=static_cast<std::size_t>(Row2::start)-static_cast<std::size_t>(Row1::start)
	};
};

template<class Deriv>
template<class Row1, class Row2, class... Rows>
class computed_goto::state_transition_table<Deriv>::rows_details<Row1, Row2, Rows...> {
	enum : std::size_t {
		stride_before=rows_details<Row1, Row2>::stride,
		stride_after=rows_details<Row2, Rows...>::stride
	};
	BOOST_MPL_ASSERT_RELATION(stride_before, ==, stride_after);

public:
	enum : std::size_t {
		stride=stride_before
	};
};

template<class Deriv>
template<class Row>
class computed_goto::state_transition_table<Deriv>::rows<Row> {
public:
	enum : std::size_t {
		size=1,
		stride=1
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg) noexcept(noexcept(a_transition(std::forward<Arg>(arg))))
	: transition(std::forward<Arg>(arg)) {}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: transition(r.transition) {}

	template<class... Params>
	static constexpr end_states process(rows &row, [[maybe_unused]] states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(std::forward<Params>(p)...))) {
		DEBUG_ASSERT(s==rows::a_transition::start);
		return row.transition.process(std::forward<Params>(p)...);
	}

	template<class... Params>
	struct fn_details_t {
		using process_fn_ptr_t=end_states (*)(rows &, states, Params &&...);

		static inline constexpr process_fn_ptr_t base_fn_ptr=&rows::process<Params...>;
		static inline constexpr const std::size_t stride=0;
	};

private:
	a_transition transition;
};

template<class Deriv>
template<class Row, class... Rows>
class computed_goto::state_transition_table<Deriv>::rows : public rows<Rows...> {
public:
	enum : std::size_t {
		size=sizeof...(Rows)+1,
		stride=rows_details<Row, Rows...>::stride
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	constexpr rows() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, rows>::value>::type
	> explicit constexpr
	rows(Arg &&arg, Args &&...args) noexcept(
		noexcept(rows<Rows...>(std::forward<Args>(args)...))
		 && noexcept(a_transition(std::forward<Arg>(arg)))
	)
	: rows<Rows...>(std::forward<Args>(args)...), transition(std::forward<Arg>(arg)) {
		// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
		BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, size);
	}
	constexpr rows(rows const &r) noexcept(noexcept(a_transition(std::declval<a_transition>())))
	: rows<Rows...>(r), transition(r.transition) {
	}

	template<class... Params>
	static constexpr end_states process(rows &row, [[maybe_unused]] states s, Params &&...p) noexcept(noexcept(std::declval<a_transition>().process(std::forward<Params>(p)...))) {
		DEBUG_ASSERT(s==rows::a_transition::start);
		return row.transition.process(std::forward<Params>(p)...);
	}

	template<class... Params>
	struct fn_details_t {
		using process_fn_ptr_t=end_states (*)(rows &, states, Params &&...);

		static inline constexpr process_fn_ptr_t base_fn_ptr=&rows::process<Params...>;
		static inline constexpr auto next_fn_ptr=&rows<Rows...>::template process<Params...>;
		static inline const std::size_t stride=reinterpret_cast<std::size_t>(next_fn_ptr)-reinterpret_cast<std::size_t>(base_fn_ptr);
	};

private:
	a_transition transition;
};

template<class STT>
template<class Arg, class... Args, class> inline constexpr
computed_goto::machine<STT>::machine(Arg &&arg, Args &&...args) noexcept(noexcept(rows_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
: rows_(std::forward<Arg>(arg), std::forward<Args>(args)...) {
	// If this fails, then insufficient ctor-arguments have been passed to the machine ctor.
	BOOST_MPL_ASSERT_RELATION(sizeof...(Args)+1, ==, rows_t::size);
}

template<class STT>
inline constexpr
computed_goto::machine<STT>::machine(machine const &m) noexcept(noexcept(rows_t(std::declval<rows_t>())))
: rows_(m.rows_) {
}

template<class STT>
template<class... Params> inline constexpr typename computed_goto::machine<STT>::end_states
computed_goto::machine<STT>::process(states s, Params &&...p) const noexcept(false) {
	using fn_details_t=typename rows_t::template fn_details_t<Params...>;
	constexpr auto offset=static_cast<typename std::underlying_type<states>::type>(element_type::transition_table::rows::a_transition::start);
	const auto index=static_cast<typename std::underlying_type<states>::type>(s)/element_type::transition_table::rows::stride-offset;
	DEBUG_ASSERT(index>=0);
	const std::size_t ptr_offset=index*fn_details_t::stride;
	DEBUG_ASSERT(ptr_offset>=0);
	const typename fn_details_t::process_fn_ptr_t process_fn_ptr=fn_details_t::base_fn_ptr+ptr_offset;
	return (*process_fn_ptr)(rows_, s, std::forward<Params>(p)...);
}

template<class STT>
template<class... Params> inline constexpr typename computed_goto::machine<STT>::end_states
computed_goto::machine<STT>::process(states s, Params &&...p) noexcept(false) {
	using fn_details_t=typename rows_t::template fn_details_t<Params...>;
	constexpr auto offset=static_cast<typename std::underlying_type<states>::type>(element_type::transition_table::rows::a_transition::start);
	const auto index=static_cast<typename std::underlying_type<states>::type>(s)/element_type::transition_table::rows::stride-offset;
	DEBUG_ASSERT(index>=0);
	const std::size_t ptr_offset=index*fn_details_t::stride;
	DEBUG_ASSERT(ptr_offset>=0);
	const typename fn_details_t::process_fn_ptr_t process_fn_ptr=fn_details_t::base_fn_ptr+ptr_offset;
	return (*process_fn_ptr)(rows_, s, std::forward<Params>(p)...);
}

template<class States, class EndStates>
template<States Start, class Event, EndStates Next>
class hash::row_types<States, EndStates>::row {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, row>::value>::type
	> explicit constexpr
	row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr row(row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class...Args> REALLY_FORCE_INLINE
	end_states process(Args &&...args) const noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Args>(args)...))
	) {
		return ev.template process<start, next>(std::forward<Args>(args)...);
	}

	template<class...Args> REALLY_FORCE_INLINE
	end_states process(Args &&...args) noexcept(
		noexcept(std::declval<event_t>().template process<start, next>(std::forward<Args>(args)...))
	) {
		return ev.template process<start, next>(std::forward<Args>(args)...);
	}

private:
	event_t ev;
};

template<class States, class EndStates>
template<States Start, class Event, EndStates Next, class Guard>
class hash::row_types<States, EndStates>::g_row {
public:
	using states=States;
	using end_states=EndStates;
	using event_t=Event;
	using guard_t=Guard;
	static inline constexpr const states start=Start;
	static inline constexpr const end_states next=Next;

	constexpr g_row() noexcept(true)=default;
	template<
		class Arg,
		class... Args,
		class =typename std::enable_if<!std::is_convertible<Arg, g_row>::value>::type
	> explicit constexpr
	g_row(Arg &&arg, Args &&...args) noexcept(noexcept(event_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
	: ev(std::forward<Arg>(arg), std::forward<Args>(args)...) {}
	constexpr g_row(g_row const &r) noexcept(noexcept(event_t(std::declval<event_t>())))
	: ev(r.ev) {}

	template<class...Args> REALLY_FORCE_INLINE
	end_states process(Args &&...args) const noexcept(
		noexcept(guard_t(args...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Args>(args)...))
	) {
		if (guard_t(args...)) {
			return ev.template process<start, next>(std::forward<Args>(args)...);
		} else {
			return start;
		}
	}

	template<class...Args> REALLY_FORCE_INLINE
	end_states process(Args &&...args) noexcept(
		noexcept(guard_t(args...))
		|| noexcept(std::declval<event_t>().template process<start, next>(std::forward<Args>(args)...))
	) {
		if (guard_t(args...)) {
			return ev.template process<start, next>(std::forward<Args>(args)...);
		} else {
			return start;
		}
	}

private:
	event_t ev;
};

/// How to get the state out of the specified row.
template<class Deriv>
template<class Row>
struct hash::state_transition_table<Deriv>::get_state_as_hash final {
	static inline constexpr const typename Row::states value=Row::start;
};

template<class Deriv>
template<class Row, class... Rows>
struct hash::state_transition_table<Deriv>::rows {
	enum : std::size_t {
		size=sizeof...(Rows)+1
	};
	using a_transition=Row;
	using states=typename a_transition::states;
	using end_states=typename a_transition::end_states;

	template<class Fn>
	struct make_row_wrapper_t {
		using type=typename mpl::constrained_override_type::to_member_function_type<
			end_states,
			Fn
		>::type;
	};
	template<class, class...>
	struct make_row_wrappers_t;
	template<class Fn, class... Fns>
	struct make_row_wrappers_t<std::tuple<Fn, Fns...>> {
		using type=std::tuple<
			typename make_row_wrapper_t<Fn>::type,
			typename make_row_wrapper_t<Fns>::type...
		>;
	};
	template<class Fn, class... Fns, class... Rows1>
	struct make_row_wrappers_t<std::tuple<Fn, Fns...>, Rows1...> {
		using other_overloads=typename make_row_wrappers_t<Rows1...>::type;
		using row_overloads=std::tuple<
			typename make_row_wrapper_t<Fn>::type,
			typename make_row_wrapper_t<Fns>::type...
		>;
		using type=typename tuple_cat<row_overloads, other_overloads>::type;
	};
	template<class>
	struct detuple_make_row;
	template<class... Fns>
	struct detuple_make_row<std::tuple<Fns...>> {
		using type=typename mpl::constrained_override_type::compile_time::result_type<
			Fns...
		>;
	};

	using wrapped_first_row_tuple=typename make_row_wrappers_t<
		typename Row::event_t::arguments_types::second_type,
		typename Rows::event_t::arguments_types::second_type...
	>::type;
	using wrapped_first_row_t=typename detuple_make_row<wrapped_first_row_tuple>::type;

	template<class, class>
	struct detuple_make_rows;
	template<class Rows1, class... Fns>
	struct detuple_make_rows<Rows1, std::tuple<Fns...>> {
		using type=mpl::constrained_override_type::private_::finalizer<
			mpl::constrained_override_type::compile_time::private_::concrete_type<
				typename wrapped_first_row_t::abstract_base_type,
				Rows1,
				Fns...
			>
		>;
	};

	using states_to_actions_table_t=unordered_tuple<
		states,
		typename wrapped_first_row_t::abstract_base_type,
		perfect_hash<
			static_cast<unsigned>(static_cast<typename is_enum<states>::type>(get_state_as_hash<Row>::value)),
			static_cast<unsigned>(static_cast<typename is_enum<states>::type>(get_state_as_hash<Rows>::value))...
		>,
		get_state_as_hash,
		typename wrapped_first_row_t::template final_type<Row>,
		typename detuple_make_rows<
			Rows,
			typename make_row_wrappers_t<typename Rows::event_t::arguments_types::second_type...>::type
		>::type...
	>;
};

template<class STT>
template<class Arg, class... Args, class> constexpr inline
hash::machine<STT>::machine(Arg &&arg, Args &&...args) noexcept(noexcept(states_to_actions_table_t(std::forward<Arg>(arg), std::forward<Args>(args)...)))
: tbl(std::forward<Arg>(arg), std::forward<Args>(args)...) {
}

template<class STT>
inline constexpr
hash::machine<STT>::machine(machine const &m) noexcept(noexcept(states_to_actions_table_t(std::declval<states_to_actions_table_t>())))
: tbl(m.tbl) {
}

template<class STT>
template<class... Params> inline typename hash::machine<STT>::end_states
hash::machine<STT>::process(states s, Params &&...p) const noexcept(
	noexcept(
		false // std::declval<states_to_actions_table_t>()[s].process(p...)
	)
) {
	return tbl[s].process(std::forward<Params>(p)...);
}

template<class STT>
template<class... Params> inline typename hash::machine<STT>::end_states
hash::machine<STT>::process(states s, Params &&...p) noexcept(
	noexcept(
		false // std::declval<states_to_actions_table_t>()[s].process(p...)
	)
) {
	return tbl[s].process(std::forward<Params>(p)...);
}

} } }
