/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_safe_adaptors.hpp"

#include "core_config.h"

#include <queue>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

	/// A thread-safety-assisted adaptor for a priority_queue.
	/**
		The algorithmic complexities of the operations is based upon those of the input collection C.
		By default the adapted collection does not use a read-write lock.
		Note that if the read_lock_type and write_lock_types are the same, i.e. an exclusive lock were used, then the adaptor will exhibit EREW semantics. If a read-writer lock is used for them ,then it will exhibit CREW semantics.

		\see safe_colln
	*/
	template<
		typename C,	///< The collection to be adapted, usually std::set or std::multiset.
		typename M,	///< The underlying lock object to use that will be locked in some (EREW or CREW or other) manner.
		typename WL,	///< The type of write-lock to use. This allows the possibility of using a read-write lock.
		class Sig,	///< Used to enable functionality to atomically signal if the collection contains work or not.
		class ValRet=typename C::value_type	///< The type to return from remove items from the queue. This can be used to return a collection of items from the queue, in the order in which they were inserted.
	>
	class priority_queue : public safe_colln<C, M, WL, Sig, typename lock::any_order::all<M::lock_traits::api_type, typename M::lock_traits::model_type, M, M> > {
	public:
		typedef safe_colln<C, M, WL, Sig, typename lock::any_order::all<M::lock_traits::api_type, typename M::lock_traits::model_type, M, M> > base_t;
		typedef typename base_t::thread_traits thread_traits;
		typedef typename base_t::container_type container_type;
		typedef typename base_t::write_lock_type write_lock_type;
		typedef typename base_t::read_lock_type read_lock_type;
		typedef typename base_t::atomic_t atomic_t;
		typedef typename container_type::reference reference;
		typedef typename container_type::const_reference const_reference;
		typedef typename container_type::size_type size_type;
		typedef typename container_type::value_type value_type;
		typedef ValRet value_ret_type;

		constexpr priority_queue() noexcept(true)
		: base_t() {}
		explicit priority_queue(typename base_t::have_work_type::atomic_t &a) noexcept(true)
		: base_t(a) {}

		void __fastcall push_back(value_type const &v) noexcept(true) {
			const write_lock_type lk(this->push_lock(), atomic_t::lock_traits::infinite_timeout());
			this->colln().push(v);
			this->have_work.add();
		}
		void __fastcall push_back(value_type &&v) noexcept(true) {
			const write_lock_type lk(this->push_lock(), atomic_t::lock_traits::infinite_timeout());
			this->colln().push(v);
			this->have_work.add();
		}

		value_type __fastcall pop_front_1_nochk_nolk() noexcept(true) {
			const value_type ret(this->colln().top());
			[[maybe_unused]] const typename base_t::have_work_type::atomic_t::lock_result_type work_lk_res=this->have_work.remove();
			DEBUG_ASSERT(work_lk_res.second!=base_t::have_work_type::lock_traits::atom_abandoned);
			this->colln().pop();
			return ret;
		}
		/**
			This is used to return a collection of items from the signalled_work_queue, in the order in which they were inserted. At least one item will be returned, and if there are sufficient items in the signalled_work_queue, then max_size items will be returned. This implies that the thread that extracts items from the queue does the work in batching them.
			This should only be used carefully! It isn't thread-safe!

			\return	A batch of either one or max_size items.
		*/
		value_ret_type __fastcall pop_front_nochk_nolk() noexcept(true) {
			value_ret_type ret;
			typename value_ret_type::size_type i=0;
			do {
				ret[i]=this->pop_front_1_nochk_nolk();
			} while (++i<ret.size() && this->colln().size()>=ret.size());
			std::fill_n(ret.begin()+i, ret.size()-i, typename value_ret_type::value_type());
			return ret;
		}
		value_type __fastcall pop_front_1_nochk_nosig() noexcept(true) {
			const write_lock_type lk(this->pop_lock(), atomic_t::lock_traits::infinite_timeout());
			const value_type ret(this->colln().top());
			this->colln().pop();
			return ret;
		}
		value_ret_type __fastcall pop_front_nolk() noexcept(true) {
			return this->pop_front_nochk_nolk();
		}
		/**
			\return	The value popped off the queue.
		 */
		value_ret_type __fastcall pop_front() noexcept(true) {
			const write_lock_type lk(this->pop_lock(), atomic_t::lock_traits::infinite_timeout());
			return this->pop_front_nolk();
		}
	};

} } }
