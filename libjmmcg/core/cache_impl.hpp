/******************************************************************************
** Copyright © 2005 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace cache {

	template<typename Val_> inline constexpr
	lru<Val_>::lru(void)
	: age(),range_value() {
	}

	template<typename Val_> inline constexpr
	lru<Val_>::lru(const value_type &v)
	: age(),range_value(v) {
	}

	template<typename Val_> inline
	lru<Val_>::lru(const lru &l)
	: age(l.age),range_value(l.range_value) {
	}

	template<typename Val_> inline
	lru<Val_>::~lru(void) {
	}

	template<typename Val_> inline lru<Val_> &
	lru<Val_>::operator=(const lru &l) {
		age=l.age;
		range_value=l.range_value;
		return *this;
	}

	template<typename Val_> inline const typename lru<Val_>::value_type &
	lru<Val_>::value(void) const noexcept(true) {
		age=clock();
		return range_value;
	}

	template<typename Val_> inline typename lru<Val_>::value_type &
	lru<Val_>::value(void) noexcept(true) {
		age=clock();
		return range_value;
	}

	template<typename Val_> inline clock_t
	lru<Val_>::accessed(void) const noexcept(true) {
		return age;
	}

	inline constexpr
	basic_statistics::basic_statistics(void) noexcept(true)
	: total_accesses_(),total_hits_(),total_flushes_() {
	}

	inline
	basic_statistics::basic_statistics(const basic_statistics &bs) noexcept(true)
	: total_accesses_(bs.total_accesses_),total_hits_(bs.total_hits_),total_flushes_(bs.total_flushes_) {
	}

	inline
	basic_statistics::~basic_statistics(void) noexcept(true) {
	}

	inline basic_statistics &
	basic_statistics::operator=(const basic_statistics &bs)noexcept(true)  {
		total_accesses_=bs.total_accesses_;
		total_hits_=bs.total_hits_;
		total_flushes_=bs.total_flushes_;
		return *this;
	}

	inline basic_statistics::accesses_type
	basic_statistics::accesses(void) const noexcept(true) {
		return total_accesses_;
	}

	inline basic_statistics::hits_type
	basic_statistics::hits(void) const noexcept(true) {
		return total_hits_;
	}

	inline basic_statistics::hits_type
	basic_statistics::flushes(void) const noexcept(true) {
		return total_flushes_;
	}

	inline basic_statistics::creations_type
	basic_statistics::outstanding_fills(void) const noexcept(true) {
///\todo JMG need to compute this.
		return 0;
	}

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	inline tostream &
	operator<<(tostream &os, const basic_statistics &stats) {
		return os<<
			_T("Basic cache statistics:\n")
			_T("\tHit rate=")<<(static_cast<double>(stats.total_hits_)/static_cast<double>(stats.total_accesses_))<<_T("\n")
			_T("\tTotal number of accesses=")<<stats.total_accesses_<<_T("\n")
			_T("\tTotal number of hits=")<<stats.total_hits_<<_T("\n")
			_T("\tTotal number of flushes=")<<stats.total_flushes_<<_T("\n")
			_T("\tTotal number of current, outstanding fills=")<<stats.outstanding_fills()<<std::endl;
	}

	inline void
	basic_statistics::accessed(void) noexcept(true) {
		++total_accesses_;
	}

	inline void
	basic_statistics::hit(void) noexcept(true) {
		++total_hits_;
	}

	inline void
	basic_statistics::flushed(void) noexcept(true) {
		++total_flushes_;
	}

	inline void
	basic_statistics::reset(void) noexcept(true) {
		total_accesses_=0;
		total_hits_=0;
		total_flushes_=0;
	}

	template<class Factory> inline
	base<Factory>::base(const base &r)
	: data_factory_(r.data_factory_) {
	}

	template<class Factory> inline
	base<Factory>::~base(void) noexcept(true) {
	}

	template<class Factory> inline base<Factory> &
	base<Factory>::operator=(const base &r) {
		data_factory_=r.data_factory_;
		return *this;
	}

	template<class Factory> inline const typename base<Factory>::factory_type &
	base<Factory>::data_factory(void) const noexcept(true) {
		return data_factory_;
	}

	template<class Factory, class FP, class ThrT, class Stats> inline
	ro<Factory, FP, ThrT, Stats>::ro(const ro &r)
	: base<Factory>(r), low_water_mark(r.low_water_mark), high_water_mark(r.high_water_mark), serialize(), flusher_pool(), populate_pool(r.populate_pool.pool_size()), internal_cache(), statistics_(r.statistics_) {
		const lock_t lock(r.serialize);
		internal_cache=r.internal_cache;
	}

	template<class Factory, class FP, class ThrT, class Stats> inline
	ro<Factory, FP, ThrT, Stats>::~ro(void) noexcept(true) {
	}

	template<class Factory, class FP, class ThrT, class Stats> inline ro<Factory, FP, ThrT, Stats> &
	ro<Factory, FP, ThrT, Stats>::operator=(const ro &r) {
		const lock_t lock_this(serialize,lock_traits::infinite_timeout());
		base<Factory>::operator=(r);
		low_water_mark=r.low_water_mark;
		high_water_mark=r.high_water_mark;
		internal_cache.reserve(low_water_mark);
		const lock_t lock_that(r.serialize,lock_traits::infinite_timeout());
		internal_cache=r.internal_cache;
		statistics_=r.statistics_;
		return *this;
	}

	template<class Factory, class FP, class ThrT, class Stats> inline bool
	ro<Factory, FP, ThrT, Stats>::empty(void) const noexcept(true) {
		const lock_t lock(serialize,lock_traits::infinite_timeout());
		return internal_cache.empty();
	}

	template<class Factory, class FP, class ThrT, class Stats> inline const typename ro<Factory, FP, ThrT, Stats>::size_type
	ro<Factory, FP, ThrT, Stats>::size(void) const noexcept(true) {
		const lock_t lock(serialize,lock_traits::infinite_timeout());
		return internal_cache.size();
	}

	template<class Factory, class FP, class ThrT, class Stats> inline void
	ro<Factory, FP, ThrT, Stats>::clear(void) {
		const lock_t lock(serialize,lock_traits::infinite_timeout());
		internal_cache.clear();
		statistics_.reset();
	}

	template<class Factory, class FP, class ThrT, class Stats> inline const typename ro<Factory, FP, ThrT, Stats>::mapped_type
	ro<Factory, FP, ThrT, Stats>::operator[](const key_type &id) {
		const typename colln_t::const_iterator i(internal_cache.find(id));
		statistics_.accessed();
		if (i!=internal_cache.end()) {
			statistics_.hit();
			return i->second;
		} else {
			typedef typename base_t::find_range_data find_range_type;

			const lock_t lock(serialize, lock_traits::infinite_timeout());
			auto const &get_range_data=populate_pool<<typename thread_traits::populate_pool_t::joinable()<<find_range_type(base<Factory>::data_factory(), id);

			typedef typename base_t::flush_cache flush_cache_t;
			flusher_pool<<flush_nonjoinable()<<flush_cache_t(*this);

			const std::pair<typename colln_t::const_iterator, bool> inserted_data(
				internal_cache.insert(
					typename colln_t::value_type(
						id,
						typename colln_t::value_type::second_type(get_range_data->data)
					)
				)
			);
			DEBUG_ASSERT(inserted_data.second);
			return inserted_data.first->second;
		}
	}

	template<class Factory, class FP, class ThrT, class Stats> inline void
	ro<Factory, FP, ThrT, Stats>::flush(void) {
		if (internal_cache.size()>high_water_mark) {
			lock_t lock(serialize);
			if (lock.try_lock()==lock_traits::atom_set) {
				while (internal_cache.size()>low_water_mark) {
					// Need to recalculate this in here, because the criterion may change...
					const typename victimization_traits::criterion_t criterion(victimization_traits::select(internal_cache.begin(),internal_cache.end()));
					const typename colln_t::iterator a_victim(std::find_if(internal_cache.begin(),internal_cache.end(),criterion));
					DEBUG_ASSERT(a_victim!=internal_cache.end());
					internal_cache.erase(a_victim);
				}
				statistics_.flushed();
			}
		}
	}

	template<class Factory, class FP, class ThrT, class Stats> inline const Stats &
	ro<Factory, FP, ThrT, Stats>::statistics(void) const noexcept(true) {
		return statistics_;
	}

} } }
