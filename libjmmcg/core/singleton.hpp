#ifndef LIBJMMCG_CORE_SINGLETON_HPP
#define LIBJMMCG_CORE_SINGLETON_HPP
/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace singleton {

	/// A so-called "Meyers" singleton.
	/**
		Note that this implementation specifically leaks, and does not clean up, even at atexit(), any resource it allocates.
		Based upon the "Meyers Singleton" from [1], that reference gives full details of the undefined & implementation-defined "features" of this type of singleton.

		[1] "Modern C++ Design" by A. Alexandrescu, Chapter 6 "Implementing Singletons", page 133
	*/
	template<class V>
	struct leaked_meyers {
		typedef V element_type;	///< The type of the object "managed".
		typedef void argument_type;
		typedef argument_type first_argument_type;
		typedef element_type result_type;

		/// Obtain the unique instance of the object.
		/**
			\return The unique instance of the object, initialised and allocated upon the heap, upon first calling, assuming sufficient resources exist.
		*/
		static [[nodiscard]] element_type & instance() noexcept(false) {
			static element_type * const val=new element_type();
			return *val;
		}
	};

} } }

#endif
