#ifndef LIBJMMCG_CORE_SOCKET_CLIENT_MANAGER_HPP
#define LIBJMMCG_CORE_SOCKET_CLIENT_MANAGER_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_wrapper.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

namespace asio {

/// A simple TCP/IP socket wrapper using boost::asio.
template<class LkT>
class client_manager {
public:
	using socket_t= socket_wrapper<LkT>;
	using socket_priority= typename socket_t::socket_priority;

	/// Create a new connection to the specified TCP socket using the TCP/IP protocol.
	/**
		\param	min_message_size	The smallest size of message that shall be sent or received using the socket.
		\param	max_message_size	The largest size of message that shall be sent or received using the socket.
		\param	timeout	The linger timeout.
		\param	conn_pol	The connection policy that may be applied to attempt to connect to the specified endpoint.
	*/
	template<class ConnPol>
	client_manager(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, ConnPol const& conn_pol);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<
		class MsgT	 ///< The type of the message to write.
		>
	void write(MsgT const& message);

	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this buffer, which may be grown to accommodate the message.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<
		class MsgT	 ///< The type of the message to read, that must be as-if a POD.
		>
	[[nodiscard]] bool
	read(MsgT& dest);
	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this stack-based buffer, which must be sufficiently large to accommodate the message read, otherwise UB will result.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class V, std::size_t SrcSz>
	[[nodiscard]] bool
		read(V (&dest)[SrcSz]);

	void stop();

	socket_t& socket() noexcept(true) {
		return socket_;
	}

	std::string to_string() const noexcept(false);

	[[nodiscard]] std::string local_ip() const noexcept(false);

private:
	boost::asio::io_context io_context;
	socket_t socket_;
};

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, client_manager<LkT> const& ec) noexcept(false);

}

namespace glibc {

/// A simple TCP/IP socket wrapper using the glibc POSIX API.
template<class LkT>
class client_manager {
public:
	using socket_t= client::wrapper<LkT>;
	using socket_priority= typename socket_t::socket_priority;

	/// Create a new connection to the specified TCP socket using the TCP/IP protocol.
	/**
		\param	min_message_size	The smallest size of message that shall be sent or received using the socket.
		\param	max_message_size	The largest size of message that shall be sent or received using the socket.
		\param	timeout	The linger timeout.
		\param	conn_pol	The connection policy that may be applied to attempt to connect to the specified endpoint.
	*/
	template<class ConnPol>
	client_manager(std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, ConnPol const& conn_pol);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<
		class MsgT	 ///< The type of the message to write.
		>
	void write(MsgT const& message);

	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this buffer, which may be grown to accommodate the message.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<
		class MsgT	 ///< The type of the message to read, that must be as-if a POD.
		>
	[[nodiscard]] bool
	read(MsgT& dest);
	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this stack-based buffer, which must be sufficiently large to accommodate the message read, otherwise UB will result.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class V, std::size_t SrcSz>
	[[nodiscard]] bool
		read(V (&dest)[SrcSz]);

	socket_t& socket() {
		return socket_;
	}

	void stop() noexcept(true);

	std::string to_string() const noexcept(false);

	[[nodiscard]] std::string local_ip() const noexcept(false);

private:
	socket_t socket_;
};

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, client_manager<LkT> const& ec) noexcept(false);

}

}}}

#include "socket_client_manager_impl.hpp"

#endif
