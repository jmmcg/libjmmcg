#ifndef LIBJMMCG_CORE_HP_TIMER_HPP
#define LIBJMMCG_CORE_HP_TIMER_HPP

/******************************************************************************
** Copyright © 2008 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_api_traits.hpp"
#include "thread_params_traits.hpp"

#include <boost/date_time/posix_time/posix_time_types.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <chrono>
#include <numeric>
#include <thread>

#ifdef _MSC_VER
#else
#	if defined(i386) || defined(__i386__) || defined(__x86_64__)
#		include <cpuid.h>
#	else
#		if(__ARM_ARCH >= 6)	  // V6 is the earliest arch that has a standard cyclecount
[[nodiscard]] inline std::uint64_t
__rdtsc() noexcept(true) {
	std::uint32_t pmccntr;
	std::uint32_t pmuseren;
	std::uint32_t pmcntenset;
	// Read the user mode perf monitor counter access permissions.
	asm volatile("mrc p15, 0, %0, c9, c14, 0"
					 : "=r"(pmuseren));
	if(pmuseren & 1) {	// Allows reading perfmon counters for user mode code.
		asm volatile("mrc p15, 0, %0, c9, c12, 1"
						 : "=r"(pmcntenset));
		if(pmcntenset & 0x80000000ul) {	 // Is it counting?
			asm volatile("mrc p15, 0, %0, c9, c13, 0"
							 : "=r"(pmccntr));
			// The counter is set up to count every 64th cycle
			return static_cast<std::uint64_t>(pmccntr) * 64ULL;
		}
	}
	return 0;
}

[[nodiscard]] inline std::uint64_t
__rdtscp(unsigned int*) noexcept(true) {
	return __rdtsc();
}

[[nodiscard]] inline std::uint64_t
__cpuid(int, unsigned, unsigned, unsigned, unsigned) noexcept(true) {
	return __rdtsc();
}
#		endif
#	endif
#endif

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A class for providing timings in high-performance counter resolution.
template<ppd::generic_traits::api_type::element_type API, typename Mdl>
class hp_timer;

/// Measure a time interval via RAII.
/**
	\see hp_timer
*/
template<typename T>
struct hp_interval {
	typedef T timer_t;	///< The underlying timer-type.

	timer_t const& timer;	///< The timer to use.
	const typename timer_t::time_utc_t start;	  ///< The time this object was created.

	/// Construct an interval-measurer on the stack.
	/**
		\param	t	The timer to use.
		\param	i	A reference to the interval to be set in the dtor.
	*/
	[[nodiscard]] explicit __stdcall hp_interval(timer_t const& t, typename timer_t::time_utc_t& i) noexcept(true)
		: timer(t), start(t.current_time()), interval(i) {
	}

	hp_interval(hp_interval const&)= delete;
	/**
		Set the interval in units of hp_timer::value_type, using the timer given in the ctor.
	*/
	__stdcall ~hp_interval() noexcept(true);

private:
	typename timer_t::time_utc_t& interval;
};

/// Use the TSC timer to measure intervals.
/**
	To determine is the TSC is available or reliable, on Linux the contents of:
		/sys/devices/system/clocksource/clocksource0/available_clocksource
	should be examined. This also indicates if the TSCs between processors are synchronised [1], section 17.16.1.

	[1] "Intel® 64 and IA-32 Architectures Developer's Manual", https://www-ssl.intel.com/content/www/us/en/architecture-and-technology/64-ia-32-architectures-software-developer-manual-325462.html
*/
namespace cpu_timer {

using element_type= std::uint64_t;

/// Measure a time interval using the CPU instruction RDTSC.
/**
	Note that the use of RDTSC means that the interval will be inaccurate as super-scalar processor may reschedule the instructions before & after the RDTSC instructions. But this is very fast.
*/
class out_of_order {
public:
	REALLY_FORCE_INLINE static element_type now() noexcept(true) {
		return __rdtsc();
	}

	REALLY_FORCE_INLINE static element_type get_start() noexcept(true) {
		return now();
	}

	REALLY_FORCE_INLINE static element_type get_end() noexcept(true) {
		return now();
	}

	REALLY_FORCE_INLINE [[nodiscard]] explicit out_of_order(element_type& d) noexcept(true)
		: start(get_start()), diff(d) {
	}

	REALLY_FORCE_INLINE ~out_of_order() noexcept(true) {
		const element_type end(now());
		DEBUG_ASSERT(end >= start);
		diff= end - start;
	}

private:
	const element_type start;
	element_type& diff;
};

/// Measure a time interval using derivatives of the CPU instruction RDTSC.
/**
	The design of this code is directly influenced from [1]. Note that instructions are emitted to ensure that the pipeline in the CPU is serialised at the start of the interval and partially serialised at the end, so the interval measurement is fairly accurate. Note that this costs approximately 20 clock cycles.

	[1] "How to Benchmark Code Execution Times on Intel® IA-32 and IA-64 Instruction Set Architectures", <a href="http://www.intel.com/content/dam/www/public/us/en/documents/white-papers/ia-32-ia-64-benchmark-code-execution-paper.pdf"/>
*/
class in_order {
public:
	REALLY_FORCE_INLINE static element_type get_start() noexcept(true) {
		unsigned eax, ebx, ecx, edx;

		__cpuid(1, eax, ebx, ecx, edx);
		return __rdtsc();
	}

	REALLY_FORCE_INLINE static element_type get_end() noexcept(true) {
		unsigned int cpu;
		return __rdtscp(&cpu);
	}

	REALLY_FORCE_INLINE static element_type now() noexcept(true) {
		return get_end();
	}

	/**
		Note that this serialises the CPU.
	*/
	REALLY_FORCE_INLINE [[nodiscard]] explicit in_order(element_type& d) noexcept(true)
		: start(get_start()), diff(d) {
	}

	/**
		Note that instructions issued before this dtor will complete before the dtor, but those issued after may be scheduled before the dtor, if the super-scalar CPU has sufficient resources.
	 */
	REALLY_FORCE_INLINE ~in_order() noexcept(true) {
		const element_type end(get_end());
		DEBUG_ASSERT(end >= start);
		diff= end - start;
	}

private:
	const element_type start;
	element_type& diff;
};

namespace private_ {
struct ticks_per_microsec_details {
	const double min;
	const double mean;
	const double max;
	const double mean_average_dev;
};

std::ostream&
operator<<(std::ostream& s, ticks_per_microsec_details const& tpm);

ticks_per_microsec_details
get_mean_ticks_per_microsec() noexcept(false);

static const element_type start_ticks= out_of_order::get_start();
static const boost::posix_time::ptime start_UTC= boost::posix_time::microsec_clock::universal_time();
/**
	Example values from a run:
	min: 2595.7MHz
	mean: 2596.9MHz
	mean-average deviation: 0.03%
	max: 2597.4MHz
	For an AMD Opteron at 2.6GHz (with frequency scaling enabled).
*/
static const ticks_per_microsec_details ticks_per_microsec= get_mean_ticks_per_microsec();
}

[[nodiscard, gnu::pure]] inline double
TSC_to_microsec(element_type ticks) noexcept(true) {
	DEBUG_ASSERT(ticks > private_::start_ticks);
	const element_type ticks_since_start= ticks - private_::start_ticks;
	const double microsecs_since_start= static_cast<double>(ticks_since_start) / private_::ticks_per_microsec.mean;
	DEBUG_ASSERT(microsecs_since_start >= 0);
	return microsecs_since_start;
}

[[nodiscard]] inline boost::posix_time::ptime
TSC_to_UTC(element_type ticks) noexcept(true) {
	const boost::posix_time::ptime curr_UTC= private_::start_UTC + boost::posix_time::microseconds(static_cast<unsigned long long>(TSC_to_microsec(ticks)));
	DEBUG_ASSERT(curr_UTC > private_::start_UTC);
	return curr_UTC;
}

/// Busy wait for a certain period.
/**
	\param	delay	In microseconds.
	\return Delay achieved in microseconds.
*/
inline double
pause_for_usec(double delay) noexcept(true) {
	auto const begin= TSC_to_microsec(cpu_timer::in_order::get_start());
	double delay_achieved;
	do {
		auto const current= in_order::get_end();
		DEBUG_ASSERT(static_cast<double>(current) > begin);
		delay_achieved= TSC_to_microsec(current) - begin;
	} while(delay_achieved < delay);
	DEBUG_ASSERT(delay_achieved >= delay);
	return delay_achieved;
}

}

}}

#ifdef WIN32
#	include "../experimental/NT-based/NTSpecific/hp_timer.hpp"
#elif defined(__unix__)
#	include "../unix/hp_timer.hpp"
#endif

#endif
