#ifndef LIBJMMCG_CORE_TYPE_TRAITS_HPP
#define LIBJMMCG_CORE_TYPE_TRAITS_HPP
/******************************************************************************
** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <string>
#include <type_traits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 *	Hack to avoid using std::is_enum on types that are not enums in non-dependant circumstances.
 */
template<class T, bool= std::is_enum<T>::value>
struct is_enum : std::false_type {
	using type= T;
};

template<class T>
struct is_enum<T, true> : std::true_type {
	using type= typename std::underlying_type<T>::type;
};

template<typename T, typename= void>
struct is_container : std::false_type {};

template<typename T>
struct is_container<std::basic_string<T>> : std::false_type {};

template<typename T>
struct is_container<T, std::void_t<decltype(std::declval<T>().begin()), decltype(std::declval<T>().end())>> : std::true_type {};

namespace private_ {

template<class T>
using has_static_result_fun2_t= decltype(T::result(std::declval<decltype(T::value)>(), std::declval<decltype(T::value)>()));

template<class, class= std::void_t<>>
struct has_static_result_fun2 : std::false_type {};

template<class T>
struct has_static_result_fun2<T, std::void_t<has_static_result_fun2_t<T>>> : std::true_type {};

}

template<class T>
inline constexpr bool has_static_result_fun2_v= private_::has_static_result_fun2<T>::value;

template<class, class= std::void_t<>>
struct has_member_element_type_t : std::false_type {};

template<class T>
struct has_member_element_type_t<T, std::void_t<typename T::element_type>> : std::true_type {};

template<class T>
inline constexpr bool has_member_element_type_v= has_member_element_type_t<T>::value;

namespace tests {

static_assert(!has_member_element_type_v<struct test>);

struct has_element_type_test {
	using element_type= int;
};

static_assert(has_member_element_type_v<has_element_type_test>);

}

}}

#endif
