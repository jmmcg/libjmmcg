#ifndef LIBJMMCG_CORE_OPTIMIZING_FIFO_FLYBACK_BUFFER_HPP
#define LIBJMMCG_CORE_OPTIMIZING_FIFO_FLYBACK_BUFFER_HPP
/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

#include <fmt/format.h>

#include <iterator>
#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A buffer similar to a circular buffer, but is limited by on raw char-pointer semantics.
/**
 * The operations that write into this buffer use raw char-pointers, so no funky smart-iterators from ring buffers.
 *
 * Thus upon getting filled the buffer moves unread data to the beginning, to restart filling. If that fails it grows.
 *
 * \note:
 * =# It is highly likely that the portion of the D-cache that the underlying container_type pas to will be slowly, but surely, invalidated, as the data is usually written, read then thrown away. This effectively invalidates that portion of the D-cache. At about 32Kbytes, it is rather precious to waste liek that.
 *
 * \see ::read(),::SSL_read_ex(), std::memmove(), boost::optimizig_circular_buffer
 */
class optimizing_fifo_flyback_buffer {
public:
	using container_type= std::string;
	using value_type= typename container_type::value_type;
	using size_type= typename container_type::size_type;
	using const_iterator= typename container_type::const_iterator;	  ///< Raw char-pointer semantics.
	using iterator= typename container_type::iterator;	  ///< Raw char-pointer semantics.

	static inline constexpr container_type::value_type initialisation_value= '\177';

	const size_type max_capacity_;

	optimizing_fifo_flyback_buffer(size_type min_capacity, size_type max_capacity) noexcept(false);
	optimizing_fifo_flyback_buffer(optimizing_fifo_flyback_buffer const&)= delete;

	[[nodiscard]] constexpr bool empty() const noexcept(true);
	[[nodiscard]] constexpr size_type size() const noexcept(true);
	[[nodiscard]] constexpr size_type capacity() const noexcept(true);
	[[nodiscard]] constexpr size_type max_capacity() const noexcept(true);

	void constexpr clear() noexcept(true);

	/// Remove an arbitrary amount of data from the buffer starting from the head up to the tail, updating the head as necessary.
	/**
	 * This method not thread-safe.
	 *
	 * @param consume_op THe operation to be used to consume the data that is to be popped. E.g. a parser may be used. If this throws an exception, clear() is called and the exception re-thrown.
	 */
	template<class ConsumeOp>
	size_type pop_front(ConsumeOp&& consume_op) noexcept(false);

	/// Add an arbitrary amount of data created via the generator. Adds to the tail, updates head and tail as necessary..
	/**
	 * This method not thread-safe.
	 *
	 * If the tail has reached the end, and the head is not at the beginning, then the data is std::memmove()d to the beginning of the buffer, with head & tail updated and the generator re-applied, until the generator returns no more data or the underlying container is full (or a failure), which is then resize()d to max_capacity() and the generator re-applied until it returns no more data (or a failure).
	 *
	 * @param generator The operation to be used to generate the data to put into the buffer the data that is to be popped. E.g. ::read() may be used.
	 * \return The number of bytes generated, or an exception if the generator failed.
	 *
	 * \see max_capacity(), std::string::resize(), std::memmove(), ::Read(), ::SSL_read(), ::SSL_read_ex()
	 */
	template<class Generator>
	size_type generate_n(Generator&& generator) noexcept(false);

private:
	container_type cont_;
	container_type::iterator head_;
	container_type::iterator tail_;
};

}}

#include "optimizing_fifo_flyback_buffer_impl.hpp"

#endif
