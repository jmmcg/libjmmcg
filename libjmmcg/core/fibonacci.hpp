#ifndef LIBJMMCG_CORE_FIBONACCI_HPP
#define LIBJMMCG_CORE_FIBONACCI_HPP

/******************************************************************************
** Copyright © 2013 by J.M.Hearne-McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <tuple>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace mpl {

	/// Return the Fibonacci number of the input.
	/**
		Complexity: O(N)

		\param	N	The number for which the Fibonacci number should be computed.
		\return	Return the Fibonacci number.
	*/
	template<unsigned long long N>
	struct fibonacci {
		typedef unsigned long long element_type;
		static inline constexpr const element_type n=N;
		static inline constexpr const element_type value=fibonacci<N-1>::value+fibonacci<N-2>::value;
	};
	template<>
	struct fibonacci<1ull> {
		typedef unsigned long long element_type;
		static inline constexpr const element_type n=1;
		static inline constexpr const element_type value=1;
	};
	template<>
	struct fibonacci<0ull> {
		typedef unsigned long long element_type;
		static inline constexpr const element_type n=1;
		static inline constexpr const element_type value=1;
	};

}

namespace dyn {

	/**
		The algorithm is:
		-# fib(2)=fib(2-1)+fib(2-2)=fib(1)+fib(0)=1+1
		-# fib(3)=fib(3-1)+fib(3-2)=fib(2)+fib(1)
		-# 1, 1, 2,     3,     5,     8, ...
		-# 1, 1, (1+1), (1+2), (2+3), (3+5), ...
	 */
	struct fibonacci {
		using element_type=unsigned long long;

		/// Return the Fibonacci number of the input.
		/**
			Complexity: O(N)

			\param	N	The number for which the Fibonacci number should be computed.
			\return	Return the Fibonacci number.
		*/
		static constexpr element_type result(element_type N) noexcept(true);
	};

	/**
	 * This code is courtesy of Paul Evans; accepted with thanks!
	 * 
	 *	The algorithm is: consider transformation Tpq on variables a and b of successive values of the Fibonacci series starting with (a, b) = (1, 1):
	 *	-# Tpq    : a <- bq + aq + ap and b <- bp + aq
	 *	-# Tpq**2 : a <- (bp + aq)*q + (bq + aq + ap)*q + (bq + aq + ap)*p and b <- (bp + aq)*p + (bq + aq + ap)*q
	 * -# Tpq**2 : a <- bpq + aqq + bqq + aqq + apq + bqp + aqp + app and b <- bpp + aqp + bqq + aqq + apq
	 * -# Tpq**2 : a <- 2*bpq + 2*aqq + 2*apq + app + bqq and b <- bpp + bqq + 2*apq + aqq
	 * -# Tpq**2 : a <- 2*bpq + bqq + 2*apq + 2*aqq + app and b <- bpp + bqq + 2*apq + aqq
	 * -# Tpq**2 : a <- b*(2*pq + qq) + a*(2*pq + qq) + a*(qq + pp) and b <- b*(qq + pp) + a*(2*pq + qq)
	 * -# therefore p' = qq + pp
	 * -# and q' = 2*pq + qq
	 */
	struct fibonacci_pe {
		using element_type=unsigned long long;

		/// Return the Fibonacci number of the input.
		/**
		 * Complexity: O(log(N))
		 * 
		 * \param  N   The number for which the Fibonacci number should be computed.
		 * \return Return the Fibonacci number.
		 */
		static constexpr element_type result(element_type n) noexcept(true);
	};

}

} }

#include "fibonacci_impl.hpp"

#endif
