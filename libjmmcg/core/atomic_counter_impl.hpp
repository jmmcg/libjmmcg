/******************************************************************************
** Copyright © 2007 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

template<class V, class LkT> inline constexpr
atomic_ctr_gen<V, LkT>::atomic_ctr_gen() noexcept(noexcept(value_type()) && noexcept(atomic_t()))
: count(), locker() {
}

template<class V, class LkT> inline constexpr
atomic_ctr_gen<V, LkT>::atomic_ctr_gen(const value_type v) noexcept(noexcept(value_type(std::declval<value_type>())) && noexcept(atomic_t()))
: count(v), locker() {
}

template<class V, class LkT> inline constexpr
atomic_ctr_gen<V, LkT>::atomic_ctr_gen(const atomic_ctr_gen &a) noexcept(false)
: count(a.count), locker() {
}

template<class V, class LkT>
template<class V1>
inline constexpr
atomic_ctr_gen<V, LkT>::atomic_ctr_gen(const atomic_ctr_gen<V1, LkT> &a) noexcept(false)
: count(dynamic_cast<value_type>(a.count)), locker() {
	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
}

template<class V, class LkT>
template<class V1>
inline
atomic_ctr_gen<V, LkT>::atomic_ctr_gen(atomic_ctr_gen<V1, LkT> &&a) noexcept(true)
: count(), locker() {
	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	swap(a);
}

template<class V, class LkT> inline
atomic_ctr_gen<V, LkT>::~atomic_ctr_gen() noexcept(true) {
}

template<class V, class LkT> inline void
atomic_ctr_gen<V, LkT>::swap(atomic_ctr_gen &a) noexcept(true) {
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::write_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	std::swap(count, a.count);
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, void>::type
atomic_ctr_gen<V, LkT>::swap(atomic_ctr_gen<V1, LkT> &a) noexcept(true) {
// This is too brutal: it doesn't detect common bases....			static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::write_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	std::swap(count, a.count);
}

template<class V, class LkT> inline void
atomic_ctr_gen<V, LkT>::operator=(const atomic_ctr_gen &a) noexcept(true) {
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::read_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	count=a.count;
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, void>::type
atomic_ctr_gen<V, LkT>::operator=(const atomic_ctr_gen<V1, LkT> &a) noexcept(true) {
	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::read_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	count=dynamic_cast<value_type>(a.get());
}

template<class V, class LkT> inline void
atomic_ctr_gen<V, LkT>::operator=(const value_type &v) noexcept(true) {
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	count=v;
}

template<class V, class LkT> inline constexpr typename atomic_ctr_gen<V, LkT>::value_type const &
atomic_ctr_gen<V, LkT>::get() const noexcept(true) {
	return count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator==(const value_type v) const noexcept(true) {
	return count==v;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator==(const atomic_ctr_gen &a) const noexcept(true) {
	return count==a.count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator!=(const value_type v) const noexcept(true) {
	return !operator==(v);
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator!=(const atomic_ctr_gen &a) const noexcept(true) {
	return !operator==(a);
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator!() const noexcept(true) {
	return operator==(0);
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator<(const value_type v) const noexcept(true) {
	return count<v;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator<(const atomic_ctr_gen &a) const noexcept(true) {
	return count<a.count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator<(base_t const &a) const noexcept(true) {
	return *this<dynamic_cast<atomic_ctr_gen const &>(a);
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator>(const value_type v) const noexcept(true) {
	return count>v;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator>(const atomic_ctr_gen &a) const noexcept(true) {
	return count>a.count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator>(const base_t &a) const noexcept(true) {
	return *this>dynamic_cast<atomic_ctr_gen const &>(a);
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator<=(const value_type v) const noexcept(true) {
	return count<=v;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator<=(const atomic_ctr_gen &a) const noexcept(true) {
	return count<=a.count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator>=(const atomic_ctr_gen &a) const noexcept(true) {
	return count>=a.count;
}

template<class V, class LkT> inline constexpr bool
atomic_ctr_gen<V, LkT>::operator>=(const value_type v) const noexcept(true) {
	return count>=v;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator++() noexcept(true) {
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	const value_type ret=++count;
	return ret;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator++(int) noexcept(true) {
	const value_type orig(count);
	++*this;
	return orig;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator--() noexcept(true) {
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	const value_type ret=--count;
	return ret;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator--(int) noexcept(true) {
	const value_type orig(count);
	--*this;
	return orig;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator+=(const value_type v) noexcept(true) {
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	count+=v;
	return count;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator+=(const atomic_ctr_gen &a) noexcept(true) {
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::read_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	count+=a.count;
	return count;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator-=(const value_type v) noexcept(true) {
	const typename atomic_t::write_lock_type lock(locker,atomic_t::lock_traits::infinite_timeout());
	count-=v;
	return count;
}

template<class V, class LkT> inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::operator-=(const atomic_ctr_gen &a) noexcept(true) {
	const typename atomic_t::write_lock_type lock_this(locker, atomic_t::lock_traits::infinite_timeout());
	const typename atomic_t::read_lock_type lock_that(a.locker, atomic_t::lock_traits::infinite_timeout());
	count-=a.count;
	return count;
}

template<class V, class LkT>
template<class BinOp>
inline
typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::apply(typename BinOp::second_argument_type const &a, BinOp const &op) noexcept(noexcept(op.operator()(std::declval<value_type>(), std::declval<typename BinOp::second_argument_type>()))) {
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	return count=op.operator()(count, a);
}

template<class V, class LkT>
template<class V1>
inline
typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::apply(V1 const &a, std::plus<V1> const &) noexcept(true) {
	return *this+=a;
}

template<class V, class LkT>
template<class V1>
inline
typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::apply(V1 const &a, std::minus<V1> const &) noexcept(true) {
	return *this-=a;
}

template<class V, class LkT>
inline bool
atomic_ctr_gen<V, LkT>::compare_exchange_strong(value_type &expected, value_type const desired) noexcept(true) {
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	if (count==expected) {
		count=desired;
		return true;
	} else {
		expected=count;
		return false;
	}
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, bool>::type
atomic_ctr_gen<V, LkT>::compare_exchange_strong(value_type &expected, V1 const desired) noexcept(true) {
	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	const typename atomic_t::write_lock_type lock(locker, atomic_t::lock_traits::infinite_timeout());
	if (count==expected) {
		count=desired;
		return true;
	} else {
		expected=count;
		return false;
	}
}

template<class V, class LkT>
template<class Op>
inline typename atomic_ctr_gen<V, LkT>::value_type
atomic_ctr_gen<V, LkT>::assign(Op &&op) noexcept(true) {
	value_type expected;
	value_type desired;
	do {
		expected=get();
		desired=op.operator()(expected);
	} while (!compare_exchange_strong(expected, desired));
	return expected;
}

template<class V, class LkT> inline constexpr
atomic_ctr_opt<V, LkT>::atomic_ctr_opt() noexcept(true)
: count() {
}

template<class V, class LkT> inline
atomic_ctr_opt<V, LkT>::atomic_ctr_opt(const value_type v) noexcept(true)
: count(v) {
}

template<class V, class LkT> constexpr inline
atomic_ctr_opt<V, LkT>::atomic_ctr_opt(const atomic_ctr_opt &a) noexcept(true)
: count(a.get()) {
}

template<class V, class LkT>
template<class V1>
constexpr inline
atomic_ctr_opt<V, LkT>::atomic_ctr_opt(const atomic_ctr_opt<V1, LkT> &a) noexcept(true)
: count(dynamic_cast<value_type>(a.get())) {
// This is too brutal: it doesn't detect common bases....	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
}

template<class V, class LkT>
template<class V1>
inline
atomic_ctr_opt<V, LkT>::atomic_ctr_opt(atomic_ctr_opt<V1, LkT> &&a) noexcept(true)
: count() {
// This is too brutal: it doesn't detect common bases....		static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	swap(a);
}

template<class V, class LkT> inline
atomic_ctr_opt<V, LkT>::~atomic_ctr_opt() noexcept(true) {
}

template<class V, class LkT> inline void
atomic_ctr_opt<V, LkT>::swap(atomic_ctr_opt &a) noexcept(true) {
	const value_type lhs_orig=count.exchange(a.get());
	a.count.store(lhs_orig);
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, void>::type
atomic_ctr_opt<V, LkT>::swap(atomic_ctr_opt<V1, LkT> &a) noexcept(true) {
// This is too brutal: it doesn't detect common bases....			static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	value_type const tmp=dynamic_cast<value_type>(a.get());
	a.count.exchange(dynamic_cast<typename atomic_ctr_opt<V1, LkT>::value_type>(count.load()));
	count.exchange(tmp);
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, void>::type
atomic_ctr_opt<V, LkT>::operator=(const atomic_ctr_opt<V1, LkT> &a) noexcept(true) {
// This is too brutal: it doesn't detect common bases....		static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	count=dynamic_cast<value_type>(a.get());
}

template<class V, class LkT> inline void
atomic_ctr_opt<V, LkT>::operator=(const atomic_ctr_opt &a) noexcept(true) {
	count=a.get();
}

template<class V, class LkT> inline void
atomic_ctr_opt<V, LkT>::operator=(const value_type &v) noexcept(true) {
	count=v;
}

template<class V, class LkT> constexpr inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::get() const noexcept(true) {
	return count.load();
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator==(const value_type v) const noexcept(true) {
	return count==v;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator==(const atomic_ctr_opt &a) const noexcept(true) {
	return count==a.count;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator!=(const value_type v) const noexcept(true) {
	return !operator==(v);
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator!=(const atomic_ctr_opt &a) const noexcept(true) {
	return !operator==(a);
}

template<class V, class LkT> constexpr inline
atomic_ctr_opt<V, LkT>::operator bool() const noexcept(true) {
	return count!=value_type();
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator<(const value_type v) const noexcept(true) {
	return count<v;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator<(const atomic_ctr_opt &a) const noexcept(true) {
	return count<a.count;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator<(const base_t &a) const noexcept(true) {
	return *this<dynamic_cast<atomic_ctr_opt const &>(a);
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator>(const value_type v) const noexcept(true) {
	return count>v;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator>(const atomic_ctr_opt &a) const noexcept(true) {
	return count>a.count;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator>(const base_t &a) const noexcept(true) {
	return *this>dynamic_cast<atomic_ctr_opt const &>(a);
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator<=(const value_type v) const noexcept(true) {
	return count<=v;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator<=(const atomic_ctr_opt &a) const noexcept(true) {
	return count<=a.count;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator>=(const atomic_ctr_opt &a) const noexcept(true) {
	return count>=a.count;
}

template<class V, class LkT> constexpr inline bool
atomic_ctr_opt<V, LkT>::operator>=(const value_type v) const noexcept(true) {
	return count>=v;
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator++() noexcept(true) {
	if constexpr (std::is_integral<V>::value) {
		return ++count;
	} else {
		static_assert(std::is_floating_point<V>::value);
		return count+=1;
	};
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator++(int) noexcept(true) {
	if constexpr (std::is_integral<V>::value) {
		return count++;
	} else {
		static_assert(std::is_floating_point<V>::value);
		const value_type old=count;
		count+=1;
		return old;
	};
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator--() noexcept(true) {
	if constexpr (std::is_integral<V>::value) {
		return --count;
	} else {
		static_assert(std::is_floating_point<V>::value);
		return count-=1;
	};
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator--(int) noexcept(true) {
	if constexpr (std::is_integral<V>::value) {
		return count--;
	} else {
		static_assert(std::is_floating_point<V>::value);
		const value_type old=count;
		count-=1;
		return old;
	};
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator+=(const value_type v) noexcept(true) {
	return count+=v;
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator+=(const atomic_ctr_opt &a) noexcept(true) {
	return *this+=a.count;
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator-=(const value_type v) noexcept(true) {
	return count-=v;
}

template<class V, class LkT> inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::operator-=(const atomic_ctr_opt &a) noexcept(true) {
	return *this-=a.count;
}

template<class V, class LkT>
inline bool
atomic_ctr_opt<V, LkT>::compare_exchange_strong(value_type &expected, value_type desired) noexcept(true) {
	return count.compare_exchange_strong(expected, desired);
}

template<class V, class LkT>
template<class V1, class V2>
inline typename std::enable_if<std::is_pointer<V2>::value, bool>::type
atomic_ctr_opt<V, LkT>::compare_exchange_strong(value_type &expected, V1 desired) noexcept(true) {
	static_assert(std::is_base_of<typename std::remove_pointer<value_type>::type, typename std::remove_pointer<V1>::type>::value, "The two types must have the same base.");
	return count.compare_exchange_strong(expected, desired);
}

template<class V, class LkT>
template<class BinOp>
inline
typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::apply(typename BinOp::second_argument_type const &a, BinOp const &op) noexcept(noexcept(op.operator()(std::declval<value_type>(), std::declval<typename BinOp::second_argument_type>()))) {
	value_type expected=get();
	value_type desired;
	do {
		desired=op.operator()(expected, a);
	} while (!compare_exchange_strong(expected, desired));
	return desired;
}

template<class V, class LkT>
template<class V1>
inline
typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::apply(V1 const &a, std::plus<V1> const &) noexcept(true) {
	return *this+=a;
}

template<class V, class LkT>
template<class V1>
inline
typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::apply(V1 const &a, std::minus<V1> const &) noexcept(true) {
	return *this-=a;
}

template<class V, class LkT>
template<class Op>
inline typename atomic_ctr_opt<V, LkT>::value_type
atomic_ctr_opt<V, LkT>::assign(Op &&op) noexcept(true) {
	value_type expected=get();
	value_type desired;
	do {
		desired=op.operator()(expected);
	} while (!compare_exchange_strong(expected, desired));
	return expected;
}

} } }
