#ifndef LIBJMMCG_CORE_SHARED_PTR_HPP
#define LIBJMMCG_CORE_SHARED_PTR_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "atomic_counter.hpp"
#include "exception.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace intrusive {
template<class, class>
class slist;
}

/// The intrusive counter that an object must also inherit from for the shared_ptr class to work.
/**
	A client must inherit from this class to allow the shared_ptr to operate. If the client also wants to specify at run-time which counter or deletion method is to be used, then some base in their hierarchy must also inherit from sp_counter_itf_type.

	By default the counter is a "simple" (cough) sequential counter, with the threading model specified via the lock_traits.

	\see sp_counter_itf_type, shared_ptr
*/
template<
	class Val,
	class LkT,
	template<class> class Del= default_delete,	///< The deleter to be used to delete the contained pointer.
	template<class> class AtCtr= LkT::template atomic_counter_type>
class sp_counter_type : public AtCtr<Val> {
public:
	using base_t= sp_counter_itf_type<Val>;
	using value_type= typename base_t::value_type;
	using atomic_ctr_t= AtCtr<value_type>;
	using lock_traits= LkT;	  ///< This does not have to be the same as the atomic_ctr_t, as we may want the flexibility to deal with the type differently in this case.
	/// Make sure the correct object deletion mechanism is used.
	/**
		Note this this allows the user to specify multiple features:
		1. That the correct deleter is used with the allocator.
		2. That if the static type of the object is known at compile-time the deleter can be used with little or no performance loss.
		3. That the deleter may be dynamically specified in a derived class, allowing the share_ptr to contain objects that also vary by the specific deleter that the derived type will use. For example there may be a collection of base-pointers, the derived objects of which may be dynamically, placement-new or stack-allocated and upon extracting them from the collection, the shared_ptr class can accommodate deleting all of them because of the flexibility introduced in sp_counter_itf_type::deleter().

		\see sp_counter_itf_type::deleter_t, sp_counter_itf_type::deleter()
	*/
	typedef Del<sp_counter_type> deleter_t;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= atomic_ctr_t::memory_access_mode;

	static_assert((std::is_integral<typename atomic_ctr_t::value_type>::value && std::is_signed<typename atomic_ctr_t::value_type>::value), "The input type must be a signed integral type as defined in 3.9.1 of the Standard.");

	virtual ~sp_counter_type() noexcept(true)= default;

	/// Call the correct deleter_t object to delete the object.
	/**
		Note that we are calling the dtor on the object from within a virtual function, which is allowed. (The converse is not, of course.)
	*/
	void deleter() override {
		deleter_t().operator()(this);
	}

	value_type sp_count() const noexcept(true) override final {
		return ctr_.get();
	}

	typename atomic_ctr_t::value_type sp_acquire() noexcept(true) override final {
		const typename atomic_ctr_t::value_type ret= ++ctr_;
		return ret;
	}

	bool sp_release() noexcept(true) override final {
		DEBUG_ASSERT(ctr_ > 0);
		return --ctr_ == 0;
	}

	virtual bool __fastcall operator<(const value_type v) const noexcept(true) override {
		return ctr_ < v;
	}

	bool __fastcall operator>(const value_type v) const noexcept(true) override {
		return ctr_ > v;
	}

	bool __fastcall operator>=(const value_type v) const noexcept(true) override final {
		return ctr_ >= v;
	}

protected:
	constexpr sp_counter_type() noexcept(true) {}

private:
	template<class, class>
	friend class shared_ptr;
	template<class, class>
	friend class intrusive::slist;

	mutable atomic_ctr_t ctr_;
};

/// A shared pointer-type that has threading specified as a trait, and uses an intrusive count to save on calls to the memory allocator, which could cause excessive contention on the memory manager in a multi-threaded environment.
/**
	I don't use boost::shared_ptr nor std::shared_ptr as they are insufficiently flexible for my purposes: the multi-threading model they use is not a trait, moreover their counters & deleters can only be specified at compile-time, not also at run-time.
	This class uses a lock-free implementation based upon the careful use of the sp_counter_type that uses a specialised atomic_counter_type.
	For performance tests see 'examples/shared_ptr_parallel.cpp'.

	\see sp_counter_itf_type, sp_counter_type
*/
template<
	class V,	  ///< The type to be controlled, which must implement the sp_counter_itf_type interface, which would also specify the threading model which, by default, is single-threaded, so no cost would be paid. This allows the counter-type to be specified at either compile or run-time.
	class LkT	///< The lock_traits that provide the (potentially) atomic operations to be used upon pointers to the value_type.
	>
class shared_ptr final {
public:
	using value_type= V;	  ///< A convenience typedef to the type to be controlled.
	using element_type= value_type;	 ///< A convenience typedef to the type to be controlled.
	using lock_traits= LkT;	  ///< This does not have to be the same as the element_type, as it may not contain any lock_traits, or we may want the flexibility to deal with the type differently in this case.
	/// The (potentially) lock-free pointer type.
	/**
		We need the operations on the contained pointer (to the managed object) to be atomic because the move operations might occur on more than one thread, and therefore there is a race condition on non-SMP architectures, which is avoided if (in the implementation) the operations on the contained pointer are atomic.
	*/
	using atomic_ptr_t= typename lock_traits::template atomic<value_type*>;
	/// The counter interface to be used on the controlled type, which allows the counter-type to be specified at compile or run-time.
	using base_ctr_type= sp_counter_itf_type<long>;
	using ctr_type= typename V::atomic_ctr_t;
	/// Make sure the correct object-deletion mechanism is used.
	/**
		\see sp_counter_itf_type::deleter_t, sp_counter_itf_type::deleter(), ~shared_ptr()
	*/
	using deleter_t= typename value_type::deleter_t;
	/// The no-op atomic counter does nothing, therefore the shared_ptr must not manage the memory of such objects...
	/**
		\see noop_atomic_ctr
	*/
	using no_ref_counting= typename lock_traits::template noop_atomic_ctr<typename ctr_type::value_type>;
	/**
		If you get a compilation issue here, check you aren't using std::default_delete, but libjmmcg::default_delete instead.

		\see libjmmcg::default_delete
	*/
	using no_deletion= noop_dtor<typename value_type::deleter_t::element_type>;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (atomic_ptr_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																				 && ctr_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																			 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																			 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, value_type>));
	static_assert(!std::is_same<base_ctr_type, no_ref_counting>::value || std::is_same<deleter_t, no_deletion>::value, "If not refcounted, then the object must not have a defined deleter, as it should be stack-based.");

	/**
		Decrement the reference count & possibly delete the object, via calling value_type::delete() which uses the value_type::deleter_t method to delete the object & release the memory.
	*/
	atomic_ptr_t __fastcall release() noexcept(true);

	constexpr __stdcall shared_ptr() noexcept(true) {}

	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall shared_ptr(value_type* ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero. The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall shared_ptr(V1* ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall shared_ptr(atomic_ptr_t const& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall shared_ptr(sp_counter_itf_type<V1> const& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	template<class V1, template<class> class At>
	[[nodiscard]] explicit __stdcall shared_ptr(At<V1*> const& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall shared_ptr(std::unique_ptr<value_type, deleter_t>& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero. The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall shared_ptr(std::unique_ptr<V1, typename V1::deleter_t>& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero.
	*/
	[[nodiscard]] explicit __stdcall shared_ptr(std::unique_ptr<value_type, deleter_t>&& ptr) noexcept(true);
	/**
		\param ptr	Note that this ptr could have a non-zero reference count, and this ctor will take ownership of the ptr, respecting that reference count, only deleting the ptr if it reaches zero. The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V1>
	[[nodiscard]] explicit __stdcall shared_ptr(std::unique_ptr<V1, typename V1::deleter_t>&& ptr) noexcept(true);
	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	[[nodiscard]] explicit JMMCG_MSVC_STDCALL_HACK
	shared_ptr(const shared_ptr<V2, LkT2>& s) noexcept(true);
	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	[[nodiscard]] explicit JMMCG_MSVC_STDCALL_HACK
	shared_ptr(shared_ptr<V2, LkT2>&& s) noexcept(true);

	[[nodiscard]] shared_ptr(const shared_ptr& s) noexcept(true);
	[[nodiscard]] shared_ptr(shared_ptr&& s) noexcept(true);

	/**
		\see release()
	*/
	__stdcall ~shared_ptr() noexcept(true);

	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	void
	operator=(const shared_ptr<V2, LkT2>& s) noexcept(true);
	/**
		Note that the same deleter and threading model must be specified.

		\param s	The type of the parameter must be the same type or a class non-privately derived from value_type.
	*/
	template<class V2, class LkT2>
	void
	operator=(shared_ptr<V2, LkT2>&& s) noexcept(true);

	void
	operator=(const shared_ptr& s) noexcept(true);
	void
	operator=(shared_ptr&& s) noexcept(true);

	void __fastcall reset() noexcept(true);

	constexpr bool __fastcall
	operator==(const shared_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator!=(const shared_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator<(const shared_ptr& s) const noexcept(true);
	constexpr bool __fastcall
	operator>(const shared_ptr& s) const noexcept(true);
	explicit constexpr
	operator bool() const noexcept(true);

	constexpr const atomic_ptr_t& __fastcall get() const noexcept(true);
	atomic_ptr_t& __fastcall get() noexcept(true);
	constexpr const value_type& __fastcall
	operator*() const noexcept(true);
	value_type& __fastcall
	operator*() noexcept(true);
	constexpr value_type const* __fastcall
	operator->() const noexcept(true);
	value_type* __fastcall
	operator->() noexcept(true);

	void swap(shared_ptr& s) noexcept(true);

	tstring to_string() const noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend tostream& __fastcall operator<<(tostream& os, shared_ptr const& ptr) noexcept(false) {
		os << ptr.to_string();
		return os;
	}

private:
	template<class, class>
	friend class shared_ptr;
	atomic_ptr_t data_;
};

}}

#include "shared_ptr_impl.hpp"

#endif
