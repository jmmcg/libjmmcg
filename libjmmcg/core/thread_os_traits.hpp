#ifndef LIBJMMCG_CORE_THREAD_OS_TRAITS_HPP
#define LIBJMMCG_CORE_THREAD_OS_TRAITS_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_api_traits.hpp"

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {

namespace ppd {

/// This class is used to encapsulate the OS-specific threading traits, mutexes, etc for the "thread_pool<...>" and other classes.
template<generic_traits::api_type::element_type API, typename Mdl>
struct thread_os_traits {
	typedef api_threading_traits<API, Mdl> thread_traits;
	typedef typename thread_traits::lock_traits lock_traits;

	/// Used to assist passing exceptions between threads.
	/**
		Note that this class is not thread-safe - it presumes that access to it, although from different threads, will always be sequential. (i.e. not simultaneous.)
		It also propagates any contained exception only once - to the calling thread. (Thus if it were truly multi-thread-safe it should propagate the exception to all calling threads.)
		But then the whole paradigm of the execution context is that only one thread at a time should wait on the results from it.

		\todo Make use of std::exception_ptr or similar to pass any thrown exception.
	*/
	class thread_exception {
	public:
		/**
			We only want one thread to throw the exception, despite the fact that many may be connected to it, see specification of throw_if_set().

			Note that this implementation can use raw pointers and operate correctly on SMP architectures that support the MESI or MOESI architectures (and may work correctly with other implementations), without needing to use more complex operations. But we use allow the potential for the more complex operations here to support those more hairy architectures.
		*/
		typedef std::exception_ptr exception_t;

		constexpr thread_exception() noexcept(true)
			: thread_except() {
		}
		/**
			Don't throw any registered exception here, as it could lead to huge wobbling great nasty memory leaks.
			Just hope that someone called the throw_if_set() before hand.
			(e.g. in get_results() or destructor in the execution context or in the destructor of the thread wrapper.)

			\see throw_if_set()
			\see execution_context_stack_type
		*/
		~thread_exception() noexcept(true) {
			thread_except= exception_t();
		}

		template<class Ex>
		void __fastcall set(Ex const& e) const noexcept(true) {
			thread_except= std::make_exception_ptr(e);
			DEBUG_ASSERT(thread_except);
		}
		void __fastcall set(std::exception_ptr e) const noexcept(true) {
			thread_except= e;
			DEBUG_ASSERT(thread_except);
		}

		/// Throw any contained exception, but only once.
		/**
			Note that this function is not atomic - but then it is an error to allow multiple threads to access this class simultaneously.
		*/
		void __fastcall throw_if_set() const noexcept(false) {
			if(thread_except) {
				const exception_t except_to_throw(thread_except);
				thread_except= exception_t();
				// See - the exception is thrown only once. Otherwise it could also get thrown here & in the destructor of the thread-class.
				std::rethrow_exception(except_to_throw);
			}
		}

		/**
			\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
		*/
		friend tostream& __fastcall
		operator<<(tostream& os, thread_exception const& te) noexcept(false) {
			if(te.thread_except != nullptr) {
				os << _T("Exception has been thrown in thread.");
			} else {
				os << _T("No exception thrown.");
			}
			return os;
		}

	private:
		mutable exception_t thread_except;
	};
};

}}}

#endif
