/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock { namespace rw {

	template<class L> inline constexpr
	decaying_write_impl<L>::decaying_write_impl(atomic_t &l) noexcept(true)
	: rw_lk(l) {
	}

	template<class L>
	inline typename decaying_write_impl<L>::atomic_state_type
	decaying_write_impl<L>::lock(const timeout_type) noexcept(false) {
		return lock();
	}

	template<class L>
	inline typename decaying_write_impl<L>::atomic_state_type
	decaying_write_impl<L>::unlock() noexcept(true) {
		return rw_lk.unlock();
	}

	template<class L>
	inline void
	decaying_write_impl<L>::decay() noexcept(true) {
		rw_lk.decay();
	}

	template<class T> inline constexpr
	decaying_write_impl<locker<T> >::decaying_write_impl(atomic_t &l) noexcept(true)
	: rw_lk(l), exclusive_w_lk(rw_lk.writer_lk) {
	}

	template<class T> inline constexpr
	decaying_write_impl<locker<T> >::decaying_write_impl(decaying_write_impl<locker<T> > const &dw) noexcept(true)
	: rw_lk(dw.rw_lk), exclusive_w_lk(rw_lk.writer_lk) {
	}

	template<class T>
	inline typename decaying_write_impl<locker<T> >::atomic_state_type
	decaying_write_impl<locker<T> >::lock() noexcept(false) {
		exclusive_w_lk.lock();
		const typename atomic_t::atomic_state_type ret=rw_lk.readers_free.lock();
		DEBUG_ASSERT(ret==lock_traits::atom_set);
		DEBUG_ASSERT(rw_lk.readers==0);
		return ret;
	}

	template<class T>
	inline typename decaying_write_impl<locker<T> >::atomic_state_type
	decaying_write_impl<locker<T> >::lock(const timeout_type) noexcept(false) {
		return lock();
	}

	template<class T>
	inline typename decaying_write_impl<locker<T> >::atomic_state_type
	decaying_write_impl<locker<T> >::unlock() noexcept(true) {
		exclusive_w_lk.unlock();
		if (!decayed.test(std::memory_order_relaxed)) {
			const typename atomic_t::atomic_state_type ret=rw_lk.readers_free.set();
			DEBUG_ASSERT(ret==lock_traits::atom_set);
			return ret;
		} else {
			if ((--rw_lk.readers)==0) {
				rw_lk.readers=typename atomic_t::atomic_counter_type(0);
				DEBUG_ASSERT(rw_lk.readers==0);
				const typename atomic_t::atomic_state_type ret=rw_lk.readers_free.set();
				DEBUG_ASSERT(ret==lock_traits::atom_set);
				return ret;
			}
			DEBUG_ASSERT(rw_lk.readers>=0);
		}
		return lock_traits::atom_unset;
	}

	template<class T>
	inline void
	decaying_write_impl<locker<T> >::decay() noexcept(true) {
		if (!decayed.test(std::memory_order_relaxed)) {
			DEBUG_ASSERT(rw_lk.readers==0);
			++rw_lk.readers;
			decayed.test_and_set(std::memory_order_seq_cst);
			[[maybe_unused]] const typename atomic_t::atomic_state_type ret=exclusive_w_lk.unlock();
			DEBUG_ASSERT(ret==lock_traits::atom_unset);
		}
	}

	template<class T> inline
	locker<T>::locker() noexcept(false)
	: readers(), readers_free(lock_traits::atom_set) {
	}

	template<class T> inline typename locker<T>::atomic_state_type
	locker<T>::lock() noexcept(false) {
		if ((readers++)==0) {
			[[maybe_unused]] const typename locker::atomic_state_type ret=readers_free.lock();
			DEBUG_ASSERT(ret==lock_traits::atom_set);
		}
		return lock_traits::atom_set;
	}

	template<class T> inline typename locker<T>::atomic_state_type
	locker<T>::unlock() noexcept(true) {
		if ((--readers)==0) {
			readers=atomic_counter_type(0);
			DEBUG_ASSERT(readers==0);
			return readers_free.set();
		}
		if (readers<0) {
			readers=atomic_counter_type(0);
		}
		DEBUG_ASSERT(readers>=0);
		return lock_traits::atom_unset;
	}

	template<class T> inline
	locker<T>::read_lock_type::read_lock_type(atomic_t &l, const timeout_type) noexcept(false)
	: lk(l) {
		[[maybe_unused]] const typename atomic_t::atomic_state_type ret=lk.lock();
		DEBUG_ASSERT(ret==lock_traits::atom_set);
	}

	template<class T> inline
	locker<T>::read_lock_type::~read_lock_type() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t *>(&lk));
		lk.unlock();
	}

	template<class T> inline
	locker<T>::write_lock_type::write_lock_type(atomic_t &l, const timeout_type) noexcept(false)
	: exclusive_w_lk(l.writer_lk, lock_traits::infinite_timeout()), rw_lk(l) {
		[[maybe_unused]] const typename atomic_t::atomic_state_type ret=rw_lk.readers_free.lock();
		DEBUG_ASSERT(ret==lock_traits::atom_set);
		DEBUG_ASSERT(rw_lk.readers==0);
	}

	template<class T> inline
	locker<T>::write_lock_type::~write_lock_type() noexcept(true) {
		DEBUG_ASSERT(dynamic_cast<atomic_t *>(&rw_lk));
		[[maybe_unused]] const typename atomic_t::atomic_state_type ret=rw_lk.readers_free.set();
		DEBUG_ASSERT(ret==lock_traits::atom_set);
	}

	template<class T> inline
	locker<T>::decaying_write_lock_type::decaying_write_lock_type(atomic_t &l, const timeout_type) noexcept(false)
	: base_t(l) {
		base_t::lock();
	}

	template<class T> inline
	locker<T>::decaying_write_lock_type::~decaying_write_lock_type() noexcept(true) {
		base_t::unlock();
	}

	template<class T> inline void
	locker<T>::decaying_write_lock_type::decay() noexcept(true) {
		base_t::decay();
	}

} } } } }
