/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "ttypes.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <list>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class PeanoCurve {
private:
	enum elements {
		green_right_curve= 0,
		red_right_curve,
		green_left_curve,
		red_left_curve,
		green_straight,
		red_straight
	};

public:
	enum directions {
		north= 0,
		east,
		south,
		west
	};

	struct curve_element {
		elements elem;
		directions dir;

		curve_element(const elements e, const directions d) noexcept(true)
			: elem(e), dir(d) {
		}
	};

	typedef std::list<curve_element> curve;

	PeanoCurve(void) {
		ZeroCurve();
	}

	explicit PeanoCurve(const curve& c, const unsigned long d= 0)
		: peano(c), depth(d) {
	}

	~PeanoCurve(void) {
	}

	void Create(const unsigned long d) {
		if(depth > d) {
			ZeroCurve();
		}
		if(depth < d) {
			for(unsigned long i= depth; i < d; ++i) {
				curve::iterator element(peano.end());
				do {
					ReplaceElement(--element);
				} while(element != peano.begin());
			}
			depth= d;
		}
		DEBUG_ASSERT(!peano.empty());
	}

	unsigned long Depth(void) const noexcept(true) {
		return depth;
	}

	const curve& Curve(void) const noexcept(true) {
		return peano;
	}

private:
	typedef curve_element replace_element[4];
	typedef replace_element dir_replacements[6];

	static const dir_replacements replacements[];
	curve peano;
	unsigned long depth;

	void ZeroCurve(void) {
		peano.clear();
		peano.push_back(curve_element(red_right_curve, east));
		peano.push_back(curve_element(green_right_curve, south));
		peano.push_back(curve_element(red_right_curve, west));
		peano.push_back(curve_element(green_right_curve, north));
		depth= 0;
	}

	void ReplaceElement(curve::iterator& element) {
		const dir_replacements& replacement_curves= replacements[element->dir];
		const replace_element& replacement_curve= replacement_curves[element->elem];
		*element= replacement_curve[3];
		element= peano.insert(element, replacement_curve[2]);
		element= peano.insert(element, replacement_curve[1]);
		element= peano.insert(element, replacement_curve[0]);
	}
};

}}
