#ifndef LIBJMMCG_CORE_TTYPES_HPP
#define LIBJMMCG_CORE_TTYPES_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"
#include "debug_defines.hpp"
#include "int128_compatibility.hpp"

#include <boost/exception/diagnostic_information.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <cinttypes>
#include <cmath>

#ifdef _MSC_VER

#	include <tchar.h>

#endif	//	_MSC_VER

#include <algorithm>
#include <functional>
#include <istream>
#include <iterator>
#include <ostream>
#include <sstream>
#include <string>

#ifndef _TCHAR_DEFINED
#	define TCHAR jmmcg::LIBJMMCG_VER_NAMESPACE::tchar;
#	define _TCHAR_DEFINED
#	ifdef UNICODE
#		define _JMMCG_WIDE_CHARS
#	endif
#endif

#undef _T

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

// Macros to make easy work of making your code wide-char compliant.
#ifdef _JMMCG_WIDE_CHARS
typedef std::wchar_t tchar;
#	define _T(str) L##str
#	define tsscanf swscanf
#	define tstrlen wstrlen
#else
typedef char tchar;
#	define _T(str) str
#	define tsscanf sscanf
#	define tstrlen strlen
#endif
// And to wrap some of the more used types to make them char-size independant.
typedef std::basic_string<tchar> tstring;
typedef std::basic_stringstream<tchar> tstringstream;
typedef std::basic_istringstream<tchar> tistringstream;
typedef std::basic_ostringstream<tchar> tostringstream;
typedef std::basic_istream<tchar> tistream;
typedef std::basic_ostream<tchar> tostream;

#if defined(_MSC_VER) && (_MSC_VER < 1300)

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
inline tostream& __fastcall
operator<<(tostream& o, const __int64& i) {
	tchar buff[34];
	int radix= 10;
	if((o.flags() & std::ios::basefield) == std::ios::oct) {
		radix= 8;
	} else if((o.flags() & std::ios::basefield) == std::ios::hex) {
		radix= 16;
	}
	::_i64tot_s(i, buff, sizeof(buff), radix);
	o << buff;
	return o;
}

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
inline tistream& __fastcall
operator>>(tistream& o, __int64& i) {
	tstring buff;
	o >> buff;
	i= ::_ttoi64(buff.c_str());
	return o;
}

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
inline tostream& __fastcall
operator<<(tostream& o, const unsigned __int64& i) {
	tchar buff[34];
	int radix= 10;
	if((o.flags() & std::ios::basefield) == std::ios::oct) {
		radix= 8;
	} else if((o.flags() & std::ios::basefield) == std::ios::hex) {
		radix= 16;
	}
	::_ui64tot_s(i, buff, sizeof(buff), radix);
	o << buff;
	return o;
}

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
inline tistream& __fastcall
operator>>(tistream& o, unsigned __int64& i) {
	tstring buff;
	o >> buff;
	i= ::_ttoi64(buff.c_str());
	return o;
}

#endif	// _MSC_VER

/// Returns the number of digits in the base 10 representation of an uint64_t.
/**
	Useful for preallocating buffers and such. It is also used internally, see below. Measurements suggest that defining a separate overload for 32-bit integers is not worthwhile.

	From <a href="https://github.com/facebook/folly">Folly</a>.
*/
[[gnu::pure]] constexpr inline std::uint32_t
num_digits_in_base10(std::uint64_t v) noexcept(true) {
#ifdef __x86_64__
	// For this arch we can get a little help from specialized CPU instructions which can count leading zeroes; 64 minus that is appx. log (base 2).  Use that to approximate base-10 digits (log_10) and then adjust if needed.

	// 10^i, defined for i:[0, ..., 19).  This is 20*8==160 bytes, which fits neatly into 5 cache lines (assuming a cache line size of 64).
	ALIGN_TO_L1_CACHE constexpr const std::uint64_t powersOf10[]= {
		1ULL,
		10ULL,
		100ULL,
		1000ULL,
		10000ULL,
		100000ULL,
		1000000ULL,
		10000000ULL,
		100000000ULL,
		1000000000ULL,
		10000000000ULL,
		100000000000ULL,
		1000000000000ULL,
		10000000000000ULL,
		100000000000000ULL,
		1000000000000000ULL,
		10000000000000000ULL,
		100000000000000000ULL,
		1000000000000000000ULL,
		10000000000000000000ULL,
	};

	// "count leading zeroes" operation not valid; for 0; special case this.
	if(!v) [[unlikely]] {
		return 1;
	}
	// Bits is in the ballpark of log_2(v).
	const std::uint64_t leadingZeroes= static_cast<std::uint64_t>(__builtin_clzll(v));
	const auto bits= sizeof(std::uint64_t) * 8 - 1 - leadingZeroes;

	// Approximate log_10(v) == log_10(2) * bits.
	// Integer magic below: 77/256 is appx. 0.3010 (log_10(2)).
	// The +1 is to make this the ceiling of the log_10 estimate.
	const std::uint32_t minLength= static_cast<std::uint32_t>(1U + ((bits * 77U) >> 8U));

	// Return that log_10 lower bound, plus adjust if input >= 10^(that bound)
	// in case there's a small error and we misjudged length.
	return minLength + static_cast<std::uint32_t>(v >= powersOf10[minLength]);
#else
	std::uint32_t result= 1;
	for(;;) {
		if(LIKELY(v < 10))
			return result;
		if(LIKELY(v < 100))
			return result + 1;
		if(LIKELY(v < 1000))
			return result + 2;
		if(LIKELY(v < 10000))
			return result + 3;
		// Skip ahead by 4 orders of magnitude
		v/= 10000U;
		result+= 4;
	}
#endif
}

/// Copies the ASCII base 10 representation of v into the buffer and returns the number of bytes written.
/**
 Assumes the buffer points to num_digits_in_base10(v) bytes of valid memory. Note that uint64 needs at most 20 bytes, uint32_t needs at most 10 bytes, std::uint16_t needs at most 5 bytes, and so on. Measurements suggest that defining a separate overload for 32-bit integers is not worthwhile.

	This primitive is unsafe because it makes the size assumption and does not write the terminal '\0'.

	From <a href="https://github.com/facebook/folly">Folly</a>.
*/
[[gnu::pure]] inline std::uint32_t
uint64ToBufferUnsafe(std::uint64_t v, char* const buffer, [[maybe_unused]] std::size_t sz) noexcept(true) {
	DEBUG_ASSERT(sz > 0);
	auto const digits_to_write= num_digits_in_base10(v);
	DEBUG_ASSERT(digits_to_write <= sz);
	// WARNING: using size_t or pointer arithmetic for pos slows down the loop below 20x. This is because several 32-bit ops can be done in parallel, but only fewer 64-bit ones.
	std::uint32_t pos= digits_to_write - 1;
	while(v >= 10) {
		// Keep these together so a peephole optimization "sees" them and computes them in one shot.
		const auto q= v / 10;
		const auto r= static_cast<std::uint32_t>(v % 10);
		buffer[pos--]= static_cast<char>('0' + r);
		v= q;
	}
	DEBUG_ASSERT(pos == 0);
	// Last digit is trivial to handle.
	buffer[pos]= static_cast<char>('0' + static_cast<char>(v));
	return digits_to_write;
}

/// Returns the ASCII base 10 representation in the buffer.
/**
	\param buffer	The ACSII base 10 representation of the natural number.
	\param sz	The length of the buffer.

	From <a href="https://github.com/facebook/folly">Folly</a>.
*/
[[gnu::pure]] inline std::uint64_t
folly_ascii_to_int(char const* buffer, std::uint32_t sz) noexcept(true) {
	DEBUG_ASSERT(buffer);
	DEBUG_ASSERT(sz > 0);
	ALIGN_TO_L1_CACHE constexpr const std::uint64_t pow10[]= {
		10000000000000000000ULL,
		1000000000000000000ULL,
		100000000000000000ULL,
		10000000000000000ULL,
		1000000000000000ULL,
		100000000000000ULL,
		10000000000000ULL,
		1000000000000ULL,
		100000000000ULL,
		10000000000ULL,
		1000000000ULL,
		100000000ULL,
		10000000ULL,
		1000000ULL,
		100000ULL,
		10000ULL,
		1000ULL,
		100ULL,
		10ULL,
		1ULL};
	char const* const e= buffer + sz;
	std::uint64_t result= 0;
	auto i= sizeof(pow10) / sizeof(pow10[0]) - sz;
	for(; buffer != e; ++buffer) {
		DEBUG_ASSERT(std::isdigit(*buffer));
		const std::uint32_t digit= std::uint32_t(*buffer) - '0';
		DEBUG_ASSERT(digit < 10U);
		result+= digit * pow10[i++];
	}
	DEBUG_ASSERT(std::strtoull(e - sz, nullptr, 10) == result);
	return result;
}

/// Returns the ASCII base 10 representation in the buffer.
/**
	\param buffer	The ACSII base 10 representation of the natural number.
	\param sz	The length of the buffer.
*/
template<unsigned Base>
[[gnu::pure]] inline std::uint64_t
ascii_to_int_baseline(char const* const buffer, std::size_t sz) noexcept(true) {
	DEBUG_ASSERT(buffer);
	DEBUG_ASSERT(sz > 0);
	char const* digit= buffer + sz;
	std::uint64_t result= 0;
	std::uint64_t units= 1;
	while(--digit != buffer) {
		DEBUG_ASSERT(std::isdigit(*digit));
		const std::uint32_t d= std::uint32_t(*digit) - '0';
		DEBUG_ASSERT(d < 10U);
		result+= d * units;
		units*= Base;
	}
	DEBUG_ASSERT(std::isdigit(*digit));
	result+= static_cast<std::uint64_t>((*digit) - '0') * units;
	DEBUG_ASSERT(std::strtoull(buffer, nullptr, Base) == result);
	return result;
}

// Function to convert anything to a tstring, as long as "operator<<(...)" is defined for it.
template<typename T>
inline tstring __fastcall tostring(const T& val) {
	tostringstream ss;
	ss << val;
	return ss.str();
}

template<std::size_t N>
inline tstring __fastcall tostring(const std::array<tstring::value_type, N>& val) {
	tostringstream ss;
	if(std::find(val.begin(), val.end(), _T('\0')) != val.end()) {
		ss << val.data();
	} else {
		std::copy(val.begin(), val.end(), std::ostream_iterator<tstring::value_type>(ss));
	}
	return ss.str();
}

template<>
inline tstring __fastcall tostring<tstring>(const tstring& val) {
	return val;
}

template<typename A, typename Fmt>
inline tstring __fastcall tostring(const A& a, const Fmt& format) {
	tostringstream ss;
	ss << format << a;
	return ss.str();
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	Sz	The size of the input buffer.
*/
template<std::size_t Sz>
[[gnu::pure]] constexpr inline std::size_t
tostring(std::uint64_t v, char (&buff)[Sz]) noexcept(true) {
	return uint64ToBufferUnsafe(v, buff, Sz);
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted, zero-padded, right-justified.
	\param	Sz	The size of the input buffer.
*/
template<std::uint8_t Base, std::size_t Sz>
[[gnu::pure]] constexpr inline std::size_t
tostring_zero_pad_right_justify(std::uint32_t v, char (&buff)[Sz]) noexcept(true) {
	for(std::size_t i= Sz; i; --i) {
		auto const q= v / Base;
		auto const r= static_cast<char>(v % Base);
		buff[i - 1]= static_cast<char>(static_cast<std::uint8_t>('0') + r);
		v= q;
	}
	return Sz;
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	Sz	The size of the input buffer.
*/
template<std::size_t Sz>
[[gnu::pure]] constexpr inline std::size_t
tostring(std::int64_t v, char (&buff)[Sz]) noexcept(true) {
	if(v < 0) {
		buff[0]= '-';
		return uint64ToBufferUnsafe(-v, buff + 1, Sz - 1);
	} else {
		return uint64ToBufferUnsafe(v, buff, Sz);
	}
}
template<class V>
inline std::size_t
tostring(V v, char* const buff, std::size_t sz) noexcept(true)
	= delete;

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	sz	The size of the input buffer.
*/
template<>
[[gnu::pure]] inline std::size_t
tostring<int>(int v, char* const buff, std::size_t sz) noexcept(true) {
	if(v < 0) {
		buff[0]= '-';
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(-v), buff + 1, sz - 1);
	} else {
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(v), buff, sz);
	}
}

/// Convert the input value to a string stored in the supplied buffer.
/**
 *		\param v	The integer, decimal input value.
 *		\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
 *		\param	sz	The size of the input buffer.
 */
template<>
[[gnu::pure]] inline std::size_t
tostring<long>(long v, char* const buff, std::size_t sz) noexcept(true) {
	if(v < 0) {
		buff[0]= '-';
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(-v), buff + 1, sz - 1);
	} else {
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(v), buff, sz);
	}
}

/// Convert the input value to a string stored in the supplied buffer.
/**
 *		\param v	The integer, decimal input value.
 *		\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
 *		\param	sz	The size of the input buffer.
 */
template<>
[[gnu::pure]] inline std::size_t
tostring<std::size_t>(std::size_t v, char* const buff, std::size_t sz) noexcept(true) {
	return uint64ToBufferUnsafe(v, buff, sz);
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	sz	The size of the input buffer.
*/
[[gnu::pure]] inline std::size_t
tostring(std::uint32_t v, char* const buff, std::size_t sz) noexcept(true) {
	return uint64ToBufferUnsafe(v, buff, sz);
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	sz	The size of the input buffer.
*/
[[gnu::pure]] inline std::size_t
tostring(std::int64_t v, char* const buff, std::size_t sz) noexcept(true) {
	if(v < 0) {
		buff[0]= '-';
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(-v), buff + 1, sz - 1);
	} else {
		return uint64ToBufferUnsafe(static_cast<std::uint64_t>(v), buff, sz);
	}
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The integer, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal integers shall be converted.
	\param	sz	The size of the input buffer.
*/
[[gnu::pure]] inline std::size_t
tostring(std::uint64_t v, char* const buff, std::size_t sz) noexcept(true) {
	return uint64ToBufferUnsafe(v, buff, sz);
}

/// Convert the input value to a string stored in the supplied buffer.
/**
	\param v	The floating-point, decimal input value.
	\param	buff	A buffer that must be large enough into which the value, represented as ASCII, decimal, floating-point number shall be converted.
	\param	sz	The size of the input buffer.

	\todo This is pretty slow. Write an optimised version for just decimal conversion.
*/
inline std::size_t
tostring(double v, char* const buff, std::size_t sz) noexcept(true) {
	return static_cast<std::size_t>(std::snprintf(buff, sz, "%f", v));
}

// Function to convert anything from a tstring, as long as "operator>>(...)" is defined for it.
template<typename T>
inline void __fastcall fromstring(const tstring& str, T& val) {
	tstringstream ss;
	ss << str;
	ss >> val;
}

template<>
inline void __fastcall fromstring<tstring>(const tstring& str, tstring& val) {
	val= str;
}
template<class V>
[[gnu::pure]] typename std::enable_if<std::is_integral<V>::value || std::is_floating_point<V>::value, V>::type
fromstring(char const* const, std::size_t) noexcept(true)
	= delete;

/// Convert the decimal value contained in the input string into an integral value.
/**
 *		\param	buff	The input buffer that must contain the decimal value only.
 *		\return	The scanned value.
 */
template<>
[[gnu::pure]] inline int
fromstring<int>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<int>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
 *		\param	buff	The input buffer that must contain the decimal value only.
 *		\return	The scanned value.
 */
template<>
[[gnu::pure]] inline unsigned int
fromstring<unsigned int>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<unsigned int>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
 *		\param	buff	The input buffer that must contain the decimal value only.
 *		\return	The scanned value.
 */
template<>
[[gnu::pure]] inline long
fromstring<long>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<long>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
	\param	buff	The input buffer that must contain the decimal value only.
	\return	The scanned value.
*/
template<>
[[gnu::pure]] inline unsigned long
fromstring<unsigned long>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<unsigned long>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
	\param	buff	The input buffer that must contain the decimal value only.
	\return	The scanned value.
*/
template<>
[[gnu::pure]] inline long long
fromstring<long long>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<long long>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
	\param	buff	The input buffer that must contain the decimal value only.
	\return	The scanned value.
*/
template<>
[[gnu::pure]] inline unsigned long long
fromstring<unsigned long long>(char const* const begin, std::size_t sz) noexcept(true) {
	return static_cast<unsigned long long>(ascii_to_int_baseline<10u>(begin, sz));
}

/// Convert the decimal value contained in the input string into an integral value.
/**
	\param	buff	The input buffer that must contain the decimal value only, including a decimal point.
	\return	The scanned value.
*/
template<>
[[gnu::pure]] inline double
fromstring<double>(char const* const begin, std::size_t sz) noexcept(true) {
	constexpr unsigned Base= 10;
	char const* decimal_point= begin;
	using diff_t= decltype(std::distance(begin, decimal_point));
	static_assert(std::numeric_limits<std::uint32_t>::max() <= std::numeric_limits<diff_t>::max());
	while(std::distance(begin, decimal_point) < static_cast<diff_t>(sz) && *decimal_point != '.') {
		++decimal_point;
	}
	char const* whole= decimal_point;
	double result= 0;
	int units;
	if(std::distance(begin, decimal_point) < static_cast<diff_t>(sz)) {
		const std::ptrdiff_t num_frac_digits= static_cast<std::ptrdiff_t>(sz - static_cast<std::size_t>(decimal_point - begin));
		char const* fraction= decimal_point + num_frac_digits;
		DEBUG_ASSERT(std::abs(num_frac_digits) <= std::numeric_limits<std::ptrdiff_t>::max());
		units= static_cast<int>(num_frac_digits);
		while(--fraction != decimal_point) {
			DEBUG_ASSERT(std::isdigit(*fraction));
			--units;
			result+= ((*fraction) - '0') / __builtin_powi(Base, units);
		}
	}
	units= 1;
	while(--whole != begin) {
		DEBUG_ASSERT(std::isdigit(*whole));
		result+= ((*whole) - '0') * units;
		units*= Base;
	}
	DEBUG_ASSERT(std::isdigit(*whole));
	result+= ((*whole) - '0') * units;
	DEBUG_ASSERT(std::abs(std::strtod(begin, nullptr) - result) < 0.000001);
	return result;
}

template<typename T>
inline void __fastcall delete_ptr(T* ptr) {
	// T should be a pointer type really....
	delete ptr;
}

/// A simple class to turn a character into upper case according to the current locale settings.
template<typename T>
class upperletter {
public:
	upperletter()
		: ct(std::use_facet<std::ctype<T> >(std::locale())) {
	}

	T operator()(const T c) const {
		return ct.toupper(c);
	}

private:
	std::ctype<T> const& ct;
};

/// A simple class to turn a character into lowercase according to the current locale settings.
template<typename T>
class lowerletter {
public:
	lowerletter()
		: ct(std::use_facet<std::ctype<T> >(std::locale())) {
	}

	T operator()(const T c) const {
		return ct.tolower(c);
	}

private:
	std::ctype<T> const& ct;
};

/// Turn the input string into an uppercase variant according to the current locale settings.
inline tstring
toupper(const tstring& l) {
	tstring u;
	std::transform(l.begin(), l.end(), std::back_inserter(u), upperletter<tstring::value_type>());
	return u;
}

/// Turn the input string into a lowercase variant according to the current locale settings.
inline tstring
tolower(const tstring& u) {
	tstring l;
	std::transform(u.begin(), u.end(), std::back_inserter<tstring>(l), lowerletter<tstring::value_type>());
	return l;
}

/// Turn the input range into an uppercase variant according to the current locale settings.
template<class Iter>
inline Iter
toupper(const Iter b, const Iter e, Iter o) {
	std::transform(b, e, o, upperletter<typename std::iterator_traits<Iter>::value_type>());
	return o;
}

/// Turn the input range into a lowercase variant according to the current locale settings.
template<class Iter>
inline Iter
tolower(const Iter b, const Iter e, Iter o) {
	std::transform(b, e, o, lowerletter<typename std::iterator_traits<Iter>::value_type>());
	return o;
}

}}

namespace std {

inline jmmcg::LIBJMMCG_VER_NAMESPACE::tostream&
operator<<(jmmcg::LIBJMMCG_VER_NAMESPACE::tostream& os, std::byte b) noexcept(false) {
	os << static_cast<unsigned>(b);
	return os;
}

template<std::size_t Sz>
inline jmmcg::LIBJMMCG_VER_NAMESPACE::tostream& __fastcall
operator<<(jmmcg::LIBJMMCG_VER_NAMESPACE::tostream& o, std::array<jmmcg::LIBJMMCG_VER_NAMESPACE::tstring::value_type, Sz> const& val) {
	if(std::find(val.begin(), val.end(), _T('\0')) != val.end()) {
		o << val.data();
	} else {
		o.write(val.data(), Sz);
	}
	return o;
}

inline std::ostream& __fastcall
operator<<(std::ostream& o, std::exception_ptr ex) noexcept(false) {
	try {
		if(ex) {
			std::rethrow_exception(ex);
		} else {
			o << "No exception information.";
		}
	} catch(std::exception const& ex) {
		o << boost::diagnostic_information(ex);
	} catch(...) {
		o << "Unknown exception. Details: " << boost::current_exception_diagnostic_information();
	}
	return o;
}

}

#endif
