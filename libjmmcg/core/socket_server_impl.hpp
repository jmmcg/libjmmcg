/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

template<class SvrMgr>
inline svr<SvrMgr>::svr(ctor_args const& args, typename svr_mgr_t::proc_rules_t const& proc_rules, report_error_fn_t& report_error, libjmmcg::latency_timestamps_itf& ts, thread_t::thread_traits::thread_name_t const& svr_name, server_to_client_flow_t&& server_to_client_flow) noexcept(false)
	: base_t(),
	  address(args.address),
	  port_number(args.port_num),
	  report_error_(report_error),
	  manager(
		  exit_requested_,
		  report_error_,
		  address,
		  port_number,
		  proc_rules_t::src_msg_details_t::min_msg_size,
		  proc_rules_t::src_msg_details_t::max_msg_size,
		  args.timeout,
		  args.qos_priority,
		  args.mask.get_cpu(),
		  proc_rules,
		  ts,
		  std::move(server_to_client_flow)),
	  io_thread(svr_name, args.cpu_priority, args.mask, [this](std::atomic_flag& thread_exit_requested) {
		  try {
			  this->manager.run();
			  exit_requested_.test_and_set(std::memory_order_seq_cst);
			  exit_requested_.notify_all();
			  thread_exit_requested.test_and_set(std::memory_order_seq_cst);
			  thread_exit_requested.notify_all();
		  } catch(...) {
			  exit_requested_.test_and_set(std::memory_order_seq_cst);
			  exit_requested_.notify_all();
			  thread_exit_requested.test_and_set(std::memory_order_seq_cst);
			  thread_exit_requested.notify_all();
			  report_error_(to_string(), std::current_exception());
		  }
	  }) {
}

template<class SvrMgr>
inline svr<SvrMgr>::~svr() noexcept(true) {
	stop();
}

template<class SvrMgr>
inline void
svr<SvrMgr>::stop() noexcept(true) {
	exit_requested_.test_and_set(std::memory_order_seq_cst);
	manager.stop();
}

template<class SvrMgr>
inline int
svr<SvrMgr>::main(int argc, char const* const* argv) noexcept(true) {
	try {
		using thread_traits= thread_t::thread_traits;

		boost::program_options::options_description all(
			application::make_program_options(
				std::string{
					"A simple exchange-simulator that listens to an IPADDR:PORT combination. The executable name indicates the message version implemented. For details regarding the properties of the simulator see the documentation that came with the distribution. "}
				+ application::wireshark_info + application::make_help_message()));
		all.add_options()("address", boost::program_options::value<boost::asio::ip::address>()->default_value(boost::asio::ip::address_v4::loopback()), "IP address (in v4 format; thus the network interface to which it is bound) to which the server should listen.")("port", boost::program_options::value<unsigned short>()->required(), "An unused port to which the server should listen.")("processor", boost::program_options::value<unsigned short>()->required()->default_value(libisimud::exchanges::common::thread_traits::exchange_simulator_thread.core), "The core to which the server should be bound.");
		boost::program_options::options_description locale("Locale options.");
		locale.add_options()("locale", boost::program_options::value<std::string>()->default_value(std::string(default_locale)), "Sets the locale for the server. This locale should have been installed by the 'scripts/postinst.in' when the program was installed.");
		all.add(locale);
		boost::program_options::options_description logging("Logging options.");
		logging.add_options()("log_level", boost::program_options::value<boost::log::trivial::severity_level>()->default_value(libjmmcg::log::level_severity_type::trace, std::string(boost::log::trivial::to_string(boost::log::trivial::trace)) + ", <" + libjmmcg::log::all_severities_to_string() + ">"), "Sets the level from which to log. Only log messages equal-to or higher will be logged. Default is to log everything.");
		all.add(logging);
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(auto const exit_code= application::check_basic_options(vm, all, std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		} else {
			boost::program_options::notify(vm);
			DEBUG_ASSERT(argc > 0);
			thread_traits::set_backtrace_on_signal();
			libjmmcg::log::set_global_level(vm["log_level"].as<libjmmcg::log::level_severity_type>());
			{
				[[maybe_unused]] auto const locale_err= std::setlocale(LC_ALL, vm["locale"].as<std::string>().c_str());
				DEBUG_ASSERT(locale_err != nullptr);
			}
			proc_rules_t proc_rules;
			no_latency_timestamps ts(0U);
			DEBUG_ASSERT(argv[0][0] != '\0');
			const std::string svr_name(std::string{argv[0]}.substr(0, thread_traits::max_thread_name_length - 1));
			DEBUG_ASSERT(svr_name.size() < thread_traits::max_thread_name_length);
			thread_traits::thread_name_t name_internal{};
			std::copy_n(svr_name.begin(), svr_name.size(), name_internal.begin());
			std::atomic<std::size_t> number_of_reported_errors{};
			typename svr::report_error_fn_t report_error= [&number_of_reported_errors] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
				auto const number_of_reported_errors_tmp= ++number_of_reported_errors;
				try {
					LIBJMMCG_LOG_TRIVIAL(error) << error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr;
				} catch(...) {
					LIBJMMCG_LOG_TRIVIAL(error) << error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed whilst running the server. Server details: " << svr_details << ", exception: " << boost::current_exception_diagnostic_information();
				}
			};
			svr sim(
				svr::ctor_args{
					vm["address"].as<boost::asio::ip::address>(),
					vm["port"].as<unsigned short>(),
					svr::socket_t::socket_priority::low,
					vm["processor"].as<unsigned short>(),
					thread_traits::api_params_type::priority_type::normal},
				proc_rules,
				report_error,
				ts,
				name_internal);
			// TODO		const lock_all_proc_mem locker(lock_all_proc_mem::flags::current|lock_all_proc_mem::flags::on_fault);
			LIBJMMCG_LOG_TRIVIAL(info) << "Current process ID: " << std::dec << boost::interprocess::ipcdetail::get_current_process_id();
			LIBJMMCG_LOG_TRIVIAL(info) << sim;
			thread_traits::set_kernel_affinity(
				thread_traits::get_current_thread(),
				thread_traits::api_params_type::processor_mask_type(0));
			thread_traits::set_kernel_priority(
				thread_traits::get_current_thread(),
				thread_traits::api_params_type::priority_type::idle);
			sim.exit_requested().wait(false);
			LIBJMMCG_LOG_TRIVIAL(info) << sim;
			return number_of_reported_errors.load() ? exit_codes::codes::exit_could_not_forward_message : exit_codes::codes::exit_success;
		}
	} catch(...) {
		LIBJMMCG_LOG_TRIVIAL(error) << "Unknown exception. Details: " << boost::current_exception_diagnostic_information();
		return exit_codes::codes::exit_unknown_exception;
	}
}

template<class SvrMgr>
inline std::string
svr<SvrMgr>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<< boost::core::demangle(typeid(*this).name())
		<< "\n"
		<< static_cast<base_t const&>(*this)
		<< ", address=" << address
		<< ", port_number: " << port_number
		<< ", exit=" << exit_requested_.test(std::memory_order_relaxed)
		<< ", manager: " << manager;
	return ss.str();
}

template<class SvrMgr>
inline std::ostream&
operator<<(std::ostream& os, svr<SvrMgr> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}}}
