/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class FailureTraits, class... FnArgs>
template<class HandleError>
inline auto
syscall::traits_base<FailureTraits, FnArgs...>::report(fn_ret_code ret, HandleError&& handle_error) noexcept(false) {
	if constexpr(has_static_result_fun2_v<failure_code>) {
		if(failure_code::result(ret, reinterpret_cast<fn_ret_code>(failure_code::value))) [[unlikely]] {
			handle_error();
		}
	} else {
		if(failure_detection::result(ret, reinterpret_cast<fn_ret_code>(failure_code::value))) [[unlikely]] {
			handle_error();
		}
	}
	return ret;
}

template<class FailureTraits, class... Args>
template<class LazyEvalErrMsg>
void
syscall::traits_base<FailureTraits, Args...>::report_error(int err, ::boost::source_location const& src_loc, LazyEvalErrMsg&& err_msg, fn_ret_code (*fn)(Args...), Args&&... args) noexcept(false) {
	std::string args_str= [](Args&&... args) {
		if constexpr(sizeof...(Args) > 0) {
			return ([](Args&& arg) {
				return fmt::format(
					argument_format_string,
					boost::core::demangle(typeid(Args).name()),
					[](Args&& arg) {
						if constexpr(std::is_pointer_v<Args>) {
							return reinterpret_cast<void const*>(arg);
						} else {
							return arg;
						}
					}(std::forward<Args>(arg)));
			}(std::forward<Args>(args))
					  + ...);
		} else {
			return std::string{};
		}
	}(std::forward<Args>(args)...);
	return ::boost::throw_exception(throw_crt_exception<std::runtime_error, ppd::platform_api, ppd::heavyweight_threading>(err_msg(), src_loc.function_name(), fn, err) << errinfo_parameters(std::move(args_str)), src_loc);
}

template<class FailureTraits, class... Args>
template<class LazyEvalErrMsg>
inline auto
syscall::simple_report_traits<FailureTraits, Args...>::select_error(int err, ::boost::source_location&& src_loc, LazyEvalErrMsg&& err_msg, fn_ret_code (*fn)(Args...), Args&&... args) noexcept(false) {
	return base_t::report_error(err, std::move(src_loc), std::move(err_msg), fn, std::forward<Args>(args)...);
}

template<class CaseStatements, class FailureTraits, class... Args>
template<class LazyEvalErrMsg>
inline constexpr auto
syscall::switch_traits<CaseStatements, FailureTraits, Args...>::select_error(int err, ::boost::source_location&& src_loc, LazyEvalErrMsg&& err_msg, fn_ret_code (*fn)(Args...), Args&&... args) {
	enum : std::size_t {
		num_statements= std::tuple_size<std::decay_t<case_statements>>::value
	};

	BOOST_MPL_ASSERT_RELATION(num_statements, >, 1);
	private_::unroller<num_statements - 1, case_statements>::result(
		err,
		[err, &src_loc, &err_msg, fn, args...]() {
			base_t::report_error(err, src_loc, std::forward<LazyEvalErrMsg>(err_msg), fn, args...);
		});
}

template<
	template<class, class...>
	class Traits,
	template<class, template<class> class, template<class> class>
	class FailureTraits,
	template<class>
	class FailureCode,
	template<class>
	class FailureDetection,
	class LazyEvalErrMsg,
	class FnReturnType,
	class... FnArgs,
	class... PassedInArgs>
	requires(sizeof...(FnArgs) == sizeof...(PassedInArgs))
inline FnReturnType
syscall::process(::boost::source_location&& src_loc, LazyEvalErrMsg&& err_msg, FnReturnType (*fn)(FnArgs...), PassedInArgs... args) noexcept(false) {
	if constexpr(std::is_same<FnReturnType, void>::value) {
		fn(static_cast<FnArgs>(args)...);
	} else {
		using traits_t= Traits<
			FailureTraits<
				FnReturnType,
				FailureCode,
				FailureDetection>,
			FnArgs...>;
		auto ret= fn(static_cast<FnArgs>(args)...);
		return traits_t::report(ret,
			[ret, &src_loc, &err_msg, fn, args...]() {
				if constexpr(std::is_same<decltype(ret), int>::value) {
					traits_t::select_error(ret, std::move(src_loc), std::forward<LazyEvalErrMsg>(err_msg), fn, static_cast<FnArgs>(args)...);
				} else {
					traits_t::select_error(errno, std::move(src_loc), std::forward<LazyEvalErrMsg>(err_msg), fn, static_cast<FnArgs>(args)...);
				}
			});
	}
}

}}
