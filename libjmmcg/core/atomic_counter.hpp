#ifndef LIBJMMCG_CORE_ATOMIC_CTR_HPP
#define LIBJMMCG_CORE_ATOMIC_CTR_HPP

/******************************************************************************
** Copyright © 2007 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "deleter.hpp"
#include "thread_params_traits.hpp"

#include <atomic>

#ifdef __GCC__
#	pragma GCC diagnostic push
#	pragma GCC diagnostic ignored "-Wsuggest-final-methods"
#endif

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// An optional interface for the intrusive sp_counter_type.
/**
	If a client wishes to dynamically, as opposed to statically, specify the increment, decrement or deletion methods, then they must inherit from this class in some base within their object hierarchy. For example if one has a mix of objects in a collection, in which some are are heap allocated, others allocated via placement new in a pre-allocated buffer, i.e. a custom memory-manager.

	\see sp_counter_type, shared_ptr
*/
template<
	class V	 ///< The type of the value to be atomically controlled.
	>
class sp_counter_itf_type {
public:
	typedef V value_type;
	/// The default deletion method for objects that inherit from this intrusively-counted type, which is just heap-deletion, if the objects are heap-allocated.
	/**
		\see deleter()
	*/
	typedef default_delete<sp_counter_itf_type> deleter_t;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::crew_memory_access;

	virtual ~sp_counter_itf_type() {}

	virtual value_type sp_count() const noexcept(true) {
		return value_type(1);
	}

	virtual value_type sp_acquire() noexcept(true) {
		return value_type(1);
	}

	virtual bool sp_release() noexcept(true) {
		return true;
	}

	/**
		This is a hack: we could use a dynamic_cast to check this, but in GCC dynamic_casts involve strcmps which may be slowish, so use a virtual thunk instead, which should be faster.
	*/
	virtual bool sp_noop_ctr() const noexcept(true) {
		return true;
	}

	virtual bool __fastcall operator<(const value_type) const noexcept(true) {
		return false;
	}

	virtual bool __fastcall operator>(const value_type) const noexcept(true) {
		return true;
	}

	virtual bool __fastcall operator>=(const value_type) const noexcept(true) {
		return true;
	}

	constexpr bool operator==(const value_type v) const noexcept(true) {
		return !(*this < v) && !(*this > v);
	}

	/// Call the correct deleter_t object to delete the object.
	/**
		Note that we are calling the dtor on the object from within a virtual function, which is allowed. (The converse is not, of course.)
	*/
	virtual void deleter() {
		deleter_t().operator()(this);
	}

	virtual tstring
	sp_to_string() const noexcept(false) {
		tostringstream os;
		os << _T("count=") << this->sp_count();
		return os.str();
	}

	friend tostream&
	operator<<(tostream& os, sp_counter_itf_type const& v) {
		os << v.sp_to_string();
		return os;
	}

protected:
	constexpr sp_counter_itf_type() noexcept(true) {}
};

namespace ppd {

template<generic_traits::api_type::element_type API, typename Mdl>
struct api_lock_traits;

/// A general-purpose, atomic counter class, similar to the atomic integer types in C++11 ch 29.
/**
	Note that this class is a valid value-type.
	Also note that this class reduces to a simple V-type, if it is single threaded, so costs nothing.
*/
template<
	class V,	  ///< The type of the value for the counter. Note that this must be a type that is atomically manipulated on the target architecture, such as int or long. i.e. no word-tearing.
	class LkT>
class atomic_ctr_gen : public virtual sp_counter_itf_type<V> {
public:
	typedef sp_counter_itf_type<V> base_t;
	typedef LkT lock_traits;
	typedef typename lock_traits::recursive_critical_section_type atomic_t;
	typedef typename base_t::value_type value_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

public:
	constexpr atomic_ctr_gen() noexcept(noexcept(value_type()) && noexcept(atomic_t()));
	explicit constexpr atomic_ctr_gen(const value_type v) noexcept(noexcept(value_type(std::declval<value_type>())) && noexcept(atomic_t()));
	constexpr atomic_ctr_gen(const atomic_ctr_gen&) noexcept(false);
	template<class V1>
	constexpr atomic_ctr_gen(const atomic_ctr_gen<V1, LkT>&) noexcept(false);
	template<class V1>
	atomic_ctr_gen(atomic_ctr_gen<V1, LkT>&&) noexcept(true);
	~atomic_ctr_gen() noexcept(true);
	void swap(atomic_ctr_gen& rhs) noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type swap(atomic_ctr_gen<V1, LkT>& rhs) noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type __fastcall operator=(const atomic_ctr_gen<V1, LkT>& v) noexcept(true);
	void __fastcall operator=(const atomic_ctr_gen&) noexcept(true);
	void __fastcall operator=(const value_type&) noexcept(true);

	constexpr bool sp_noop_ctr() const noexcept(true) override {
		return false;
	}

	constexpr value_type const& __fastcall get() const noexcept(true);

	value_type sp_count() const noexcept(true) override {
		return get();
	}

	constexpr bool __fastcall operator!() const noexcept(true);

	constexpr bool __fastcall operator==(const value_type) const noexcept(true);
	constexpr bool __fastcall operator==(const atomic_ctr_gen&) const noexcept(true);
	constexpr bool __fastcall operator!=(const value_type) const noexcept(true);
	constexpr bool __fastcall operator!=(const atomic_ctr_gen&) const noexcept(true);
	constexpr bool __fastcall operator<(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator<(const atomic_ctr_gen&) const noexcept(true);
	// TODO		constexpr bool __fastcall operator<(sp_counter_itf_type const &) const noexcept(true);
	constexpr bool __fastcall operator<(base_t const&) const noexcept(true);
	constexpr bool __fastcall operator>(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator>(const atomic_ctr_gen&) const noexcept(true);
	// TODO		constexpr bool __fastcall operator>(sp_counter_itf_type const &) const noexcept(true);
	constexpr bool __fastcall operator>(base_t const&) const noexcept(true);
	constexpr bool __fastcall operator<=(const value_type) const noexcept(true);
	constexpr bool __fastcall operator<=(const atomic_ctr_gen&) const noexcept(true);
	constexpr bool __fastcall operator>=(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator>=(const atomic_ctr_gen&) const noexcept(true);

	/**
		\return	Atomically increment and return that incremented value.
	*/
	value_type __fastcall operator++() noexcept(true);
	[[nodiscard]] value_type __fastcall operator++(int) noexcept(true);
	/**
		\return	Atomically decrement and return that incremented value.
	*/
	value_type __fastcall operator--() noexcept(true);
	value_type __fastcall operator--(int) noexcept(true);

	value_type __fastcall operator+=(const value_type) noexcept(true);
	value_type __fastcall operator+=(const atomic_ctr_gen&) noexcept(true);
	value_type __fastcall operator-=(const value_type) noexcept(true);
	value_type __fastcall operator-=(const atomic_ctr_gen&) noexcept(true);

	template<class V1= value_type>
	typename std::enable_if<std::is_pointer<V1>::value, V1>::type
	operator->() const noexcept(true) {
		return count;
	}

	/// Apply the binary operation op using the second argument supplied and this as the first, updating this and return that resultant value.
	/**
	 * Note that this is performed in thread-safe manner using locks.

	 * \param a	The second argument to the binary operation.
	 * \param op	The binary operation to be applied.
	 * \return The result of op(*this, a), applying it to this.
	 */
	template<class BinOp>
	value_type __fastcall apply(typename BinOp::second_argument_type const& a, BinOp const& op) noexcept(noexcept(op.operator()(std::declval<value_type>(), std::declval<typename BinOp::second_argument_type>())));
	template<class V1>
	value_type __fastcall apply(V1 const& a, std::plus<V1> const&) noexcept(true);
	template<class V1>
	[[nodiscard]] value_type __fastcall apply(V1 const& a, std::minus<V1> const&) noexcept(true);
	bool compare_exchange_strong(value_type& expected, value_type const desired) noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, bool>::type
	compare_exchange_strong(value_type& expected, V1 const desired) noexcept(true);
	/// Perform the operation op on this and assign the result to this.
	/**
	 * Note that the operation and assignment is performed using locks.
	 *
	 * \return The value that was actually replaced, that was formerly in this at the time of replacement.
	 */
	template<class Op>
	value_type
	assign(Op&& op) noexcept(true);

protected:
	template<class V1, class LkT1>
	friend class atomic_ctr_gen;

	value_type count;
	mutable atomic_t locker;
};

/// A specialisation for sequential operation to optimise performance.
template<
	class V,	  ///< The type of the value for the counter. Note that this must be a type that is atomically manipulated on the target architecture, such as int or long. i.e. no word-tearing.
	generic_traits::api_type::element_type API>
class atomic_ctr_gen<V, api_lock_traits<API, sequential_mode>> : public virtual sp_counter_itf_type<V> {
public:
	typedef api_lock_traits<API, sequential_mode> lock_traits;
	typedef typename lock_traits::recursive_critical_section_type atomic_t;
	typedef V value_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

public:
	constexpr atomic_ctr_gen() noexcept(true)
		: count() {
	}

	explicit constexpr atomic_ctr_gen(const value_type val) noexcept(true)
		: count(val) {
	}

	constexpr atomic_ctr_gen(const atomic_ctr_gen& lkctr) noexcept(true)
		: count(lkctr.count) {
	}

	template<class V1>
	constexpr atomic_ctr_gen(const atomic_ctr_gen<V1, api_lock_traits<API, sequential_mode>>& lkctr) noexcept(true)
		: count(lkctr.count) {
	}

	~atomic_ctr_gen() noexcept(true) {}

	void swap(atomic_ctr_gen& rhs) noexcept(true) {
		std::swap(count, rhs.count);
	}

	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type
	swap(atomic_ctr_gen<V1, api_lock_traits<API, sequential_mode>>& rhs) noexcept(true) {
		std::swap(count, rhs.count);
	}

	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type __fastcall operator=(const atomic_ctr_gen<V1, api_lock_traits<API, sequential_mode>>& v) noexcept(true) {
		count= dynamic_cast<value_type>(v.count);
	}

	void operator=(const atomic_ctr_gen& lkctr) noexcept(true) {
		count= lkctr.count;
	}

	void __fastcall operator=(const value_type& v) noexcept(true) {
		count= v;
	}

	constexpr bool sp_noop_ctr() const noexcept(true) override {
		return true;
	}

	constexpr value_type __fastcall get() const noexcept(true) {
		return count;
	}

	value_type sp_count() const noexcept(true) override {
		return get();
	}

	constexpr bool __fastcall operator==(const value_type val) const noexcept(true) {
		return count == val;
	}

	constexpr bool __fastcall operator==(const atomic_ctr_gen& val) const noexcept(true) {
		return count == val.count;
	}

	constexpr bool __fastcall operator!=(const value_type val) const noexcept(true) {
		return !operator==(val);
	}

	constexpr bool __fastcall operator!=(const atomic_ctr_gen& val) const noexcept(true) {
		return !operator==(val);
	}

	explicit constexpr operator bool() const noexcept(true) {
		return count != value_type();
	}

	value_type __fastcall operator++() noexcept(true) {
		return ++count;
	}

	[[nodiscard]] value_type __fastcall operator++(int) noexcept(true) {
		const value_type ret(count);
		++count;
		return ret;
	}

	value_type __fastcall operator--() noexcept(true) {
		return --count;
	}

	value_type __fastcall operator--(int) noexcept(true) {
		const value_type ret(count);
		--count;
		return ret;
	}

	constexpr bool __fastcall operator<(const value_type val) const noexcept(true) override {
		return count < val;
	}

	constexpr bool __fastcall operator<(const atomic_ctr_gen& val) const noexcept(true) {
		return count < val.count;
	}

	/*		constexpr bool __fastcalloperator<(sp_counter_itf_type const &val) const noexcept(true) {
				DEBUG_ASSERT(dynamic_cast<atomic_ctr_gen const *>(&val));
				return *this<dynamic_cast<atomic_ctr_gen const &>(val);
			}
	*/
	constexpr bool __fastcall operator>(const value_type val) const noexcept(true) override {
		return count > val;
	}

	constexpr bool __fastcall operator>(const atomic_ctr_gen& val) const noexcept(true) {
		return count > val.count;
	}

	/*		constexpr bool __fastcall operator>(sp_counter_itf_type const &) const noexcept(true) {
				DEBUG_ASSERT(dynamic_cast<atomic_ctr_gen const *>(&val));
				return *this>dynamic_cast<atomic_ctr_gen const &>(val);
			}
	*/
	constexpr bool __fastcall operator<=(const value_type val) const noexcept(true) {
		return count <= val;
	}

	constexpr bool __fastcall operator<=(const atomic_ctr_gen& val) const noexcept(true) {
		return count <= val.count;
	}

	constexpr bool __fastcall operator>=(const value_type val) const noexcept(true) override {
		return count >= val;
	}

	constexpr bool __fastcall operator>=(const atomic_ctr_gen& val) const noexcept(true) {
		return count >= val.count;
	}

	value_type __fastcall operator+=(const value_type v) noexcept(true) {
		return count+= v;
	}

	value_type __fastcall operator+=(const atomic_ctr_gen& v) noexcept(true) {
		return count+= v.count;
	}

	value_type __fastcall operator-=(const value_type v) noexcept(true) {
		return count-= v;
	}

	value_type __fastcall operator-=(const atomic_ctr_gen& v) noexcept(true) {
		return count-= v.count;
	}

	template<class V1= value_type>
	typename std::enable_if<std::is_pointer<V1>::value, V1>::type
	operator->() const noexcept(true) {
		return count;
	}

	/// Apply the binary operation op using the second argument supplied and this as the first, updating this and return that resultant value.
	/**
	 * \param a	The second argument to the binary operation.
	 * \param op	The binary operation to be applied.
	 * \return The result of op(*this, a), applying it to this.
	 */
	template<class BinOp>
	value_type __fastcall apply(typename BinOp::second_argument_type const& a, BinOp const& op) noexcept(noexcept(op.operator()(std::declval<value_type>(), std::declval<typename BinOp::second_argument_type>()))) {
		return count= op.operator()(count, a);
	}

	template<class V1>
	value_type __fastcall apply(V1 const& a, std::plus<V1> const&) noexcept(true) {
		return *this+= a;
	}

	template<class V1>
	[[nodiscard]] value_type __fastcall apply(V1 const& a, std::minus<V1> const&) noexcept(true) {
		return *this-= a;
	}

	bool compare_exchange_strong(value_type&, value_type const desired) noexcept(true) {
		count= desired;
		return true;
	}

	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, bool>::type
	compare_exchange_strong(value_type&, V1 const desired) noexcept(true) {
		count= desired;
		return true;
	}

	/// Perform the operation op on this and assign the result to this.
	/**
	 * \return The value that was actually replaced, that was formerly in this at the time of replacement.
	 */
	template<class Op>
	value_type
	assign(Op&& op) noexcept(true) {
		value_type expected= get();
		compare_exchange_strong(expected, op(expected));
		return expected;
	}

protected:
	value_type count;
};

/// An atomic counter class for integer types, similar to the atomic integer types in C++11 ch 29.
/**
	Note that this class is a valid value-type.
	Also note that this class reduces to a simple V-type, if it is single threaded, so costs nothing.
*/
template<
	class V,	  ///< The type of the value for the counter. Note that this must be a type that is atomically manipulated on the target architecture, such as int or long. i.e. no word-tearing.
	class LkT>
class atomic_ctr_opt : public virtual sp_counter_itf_type<V> {
public:
	using base_t= sp_counter_itf_type<V>;
	using lock_traits= LkT;
	using value_type= typename base_t::value_type;
	using atomic_t= std::atomic<value_type>;
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (atomic_t::is_always_lock_free ? generic_traits::memory_access_modes::crew_memory_access : generic_traits::memory_access_modes::erew_memory_access);

	static_assert(atomic_t::is_always_lock_free, "For PPD, the atomic type must always be lock-free.");

	constexpr __stdcall atomic_ctr_opt() noexcept(true);
	explicit atomic_ctr_opt(const value_type) noexcept(true);
	constexpr atomic_ctr_opt(const atomic_ctr_opt&) noexcept(true);
	template<class V1>
	constexpr atomic_ctr_opt(const atomic_ctr_opt<V1, LkT>&) noexcept(true);
	template<class V1>
	atomic_ctr_opt(atomic_ctr_opt<V1, LkT>&&) noexcept(true);
	~atomic_ctr_opt() noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type __fastcall operator=(const atomic_ctr_opt<V1, LkT>& v) noexcept(true);
	void __fastcall operator=(const atomic_ctr_opt&) noexcept(true);
	void __fastcall operator=(const value_type&) noexcept(true);

	/**
		\note This is not lockfree! So it is not thread-safe.
	*/
	void swap(atomic_ctr_opt&) noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, void>::type swap(atomic_ctr_opt<V1, LkT>& rhs) noexcept(true);

	[[gnu::const]] constexpr bool sp_noop_ctr() const noexcept(true) override {
		return false;
	}

	constexpr value_type __fastcall get() const noexcept(true);

	value_type sp_count() const noexcept(true) override {
		return get();
	}

	explicit constexpr operator bool() const noexcept(true);

	constexpr bool __fastcall operator==(const value_type) const noexcept(true);
	constexpr bool __fastcall operator==(const atomic_ctr_opt&) const noexcept(true);
	constexpr bool __fastcall operator!=(const value_type) const noexcept(true);
	constexpr bool __fastcall operator!=(const atomic_ctr_opt&) const noexcept(true);
	constexpr bool __fastcall operator<(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator<(const atomic_ctr_opt&) const noexcept(true);
	// TODO		constexpr bool __fastcall operator<(sp_counter_itf_type const &) const noexcept(true);
	constexpr bool __fastcall operator<(base_t const&) const noexcept(true);
	constexpr bool __fastcall operator>(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator>(const atomic_ctr_opt&) const noexcept(true);
	// TODO		constexpr bool __fastcall operator>(sp_counter_itf_type const &) const noexcept(true);
	constexpr bool __fastcall operator>(base_t const&) const noexcept(true);
	constexpr bool __fastcall operator<=(const value_type) const noexcept(true);
	constexpr bool __fastcall operator<=(const atomic_ctr_opt&) const noexcept(true);
	constexpr bool __fastcall operator>=(const value_type) const noexcept(true) override;
	constexpr bool __fastcall operator>=(const atomic_ctr_opt&) const noexcept(true);

	/**
		\return	Atomically increment and return that incremented value.
	*/
	value_type __fastcall operator++() noexcept(true);
	[[nodiscard]] value_type __fastcall operator++(int) noexcept(true);
	/**
		\return	Atomically decrement and return that incremented value.
	*/
	value_type __fastcall operator--() noexcept(true);
	value_type __fastcall operator--(int) noexcept(true);

	value_type __fastcall operator+=(const value_type) noexcept(true);
	value_type __fastcall operator+=(const atomic_ctr_opt&) noexcept(true);
	value_type __fastcall operator-=(const value_type) noexcept(true);
	value_type __fastcall operator-=(const atomic_ctr_opt&) noexcept(true);

	template<class V1= value_type>
	typename std::enable_if<std::is_pointer<V1>::value, V1>::type
	operator->() const noexcept(true) {
		return count.load();
	}

	/// Apply the binary operation op using the second argument supplied and this as the first, updating this and return that resultant value.
	/**
	 * Note that this is performed atomically, without any locking.

	 * \param a	The second argument to the binary operation.
	 * \param op	The binary operation to be applied.
	 * \return The result of op(*this, a), applying it to this.
	 */
	template<class BinOp>
	value_type __fastcall apply(typename BinOp::second_argument_type const& a, BinOp const& op) noexcept(noexcept(op.operator()(std::declval<value_type>(), std::declval<typename BinOp::second_argument_type>())));
	template<class V1>
	value_type __fastcall apply(V1 const& a, std::plus<V1> const&) noexcept(true);
	template<class V1>
	[[nodiscard]] value_type __fastcall apply(V1 const& a, std::minus<V1> const&) noexcept(true);
	bool compare_exchange_strong(value_type& expected, value_type const desired) noexcept(true);
	template<class V1, class V2= V1>
	typename std::enable_if<std::is_pointer<V2>::value, bool>::type
	compare_exchange_strong(value_type& expected, V1 const desired) noexcept(true);
	/// Perform the operation op on this and assign the result to this.
	/**
	 * Note that the operation and assignment is performed atomically.
	 *
	 * \return The value that was actually replaced, that was formerly in this at the time of replacement.
	 */
	template<class Op>
	value_type
	assign(Op&& op) noexcept(true);

protected:
	template<class V1, class LkT1>
	friend class atomic_ctr_opt;

	atomic_t count;
};

/// A general-purpose, atomic-wrapper class, similar to the atomic types in C++11 ch 29, with some important differences.
/**
	Unlike std::atomic:
	-# Works for any type: integer-types & pointers (to anything) are atomic, others use a mutex, so are not; this guaranteed by design.
	-# Has a uniform interface.
	-# Also note that this class reduces to a simple value-type, if it is single threaded, so costs nothing.
	-# Note that this class is a valid value-type.
	-# If the LkT lock-traits is sequential, no mutexes nor atomic operations are used, enhancing performance.

	\see std::atomic
*/
template<
	class V,	  ///< The type of the value for the counter. Note that this must be a type that is atomically manipulated on the target architecture, such as int or long. i.e. no word-tearing.
	class LkT>
using atomic_ctr= typename std::conditional<
	std::is_arithmetic<V>::value,
	atomic_ctr_opt<V, LkT>,
	typename std::conditional<
		std::is_pointer<V>::value,
		atomic_ctr_opt<V, LkT>,
		atomic_ctr_gen<V, LkT>>::type>::type;

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
template<class V, class LkT>
inline tostream&
operator<<(tostream& os, atomic_ctr<V, LkT> const& a) {
	os << a.get();
	return os;
}

/// An atomic counter class that actually does nothing, doesn't count at all.
/**
	Note that this class is a valid value-type.
*/
template<
	class V,	  ///< The type of the value for the counter.
	class LkT>
class noop_atomic_ctr_base : public virtual sp_counter_itf_type<V> {
public:
	typedef sp_counter_itf_type<V> base_t;
	typedef LkT lock_traits;
	typedef typename lock_traits::recursive_critical_section_type atomic_t;
	typedef typename base_t::value_type value_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	constexpr noop_atomic_ctr_base() noexcept(true)= default;

	[[gnu::const]] value_type sp_count() const noexcept(true) override {
		return value_type(1);
	}

	[[gnu::const]] constexpr value_type get() const noexcept(true) {
		return value_type(1);
	}

	[[gnu::const]] bool sp_noop_ctr() const noexcept(true) override {
		return true;
	}

	[[gnu::const]] bool __fastcall operator<(const value_type) const noexcept(true) override {
		return false;
	}

	[[gnu::const]] bool __fastcall operator>(const value_type) const noexcept(true) override {
		return true;
	}

	[[gnu::const]] bool __fastcall operator>=(const value_type) const noexcept(true) override {
		return true;
	}

	value_type __fastcall operator++() noexcept(true) {
		return std::numeric_limits<value_type>::max();
	}

	value_type __fastcall operator--() noexcept(true) {
		return std::numeric_limits<value_type>::min();
	}
};

}
}}

#include "atomic_counter_impl.hpp"

#ifdef __GCC__
#	pragma GCC diagnostic pop
#endif

#endif
