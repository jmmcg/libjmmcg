#ifndef LIBJMMCG_CORE_RANDOM_INTERVAL_HPP
#define LIBJMMCG_CORE_RANDOM_INTERVAL_HPP
/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"
#include "int128_compatibility.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <fmt/format.h>

#include <chrono>
#include <random>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline std::random_device::result_type
seed_for_RNGs() noexcept(false) {
	std::random_device random_dev;
	if(random_dev.entropy() > 0.0) {
		return random_dev();
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>("The std::random_device reports that it is not a true random device, required to generate the random values.", &seed_for_RNGs));
	}
}

template<class ValueT>
class random_interval final {
private:
	using underlying_value_t= std::conditional_t<
		std::is_same_v<ValueT, std::chrono::milliseconds>,
		std::chrono::milliseconds::rep,
		ValueT>;

public:
	using element_type= ValueT;
	using distribution_type= std::conditional_t<
		std::is_integral_v<underlying_value_t>,
		std::uniform_int_distribution<underlying_value_t>,
		std::uniform_real_distribution<underlying_value_t>>;

	[[nodiscard]] random_interval(underlying_value_t min, underlying_value_t max) noexcept(false)
		: distribution_(min, max) {
		if(this->min() > this->max()) [[unlikely]] {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("The minimum of the range must be less-than or equal-to the maximum of the range. Supplied: {}.", to_string()), this));
		}
	}

	[[nodiscard]] bool empty() const noexcept(true) {
		return distribution_.min() >= distribution_.max();
	}

	[[nodiscard]] element_type min() const noexcept(true) {
		return element_type(distribution_.min());
	}

	[[nodiscard]] element_type max() const noexcept(true) {
		return element_type(distribution_.max());
	}

	[[nodiscard]] element_type get() noexcept(true) {
		auto const val= static_cast<element_type>(distribution_(generator_));
		DEBUG_ASSERT(val >= min());
		DEBUG_ASSERT(val <= max());
		return val;
	}

	[[nodiscard]] std::string to_string() const noexcept(false) {
		return fmt::format("[{}, ..., {}]", min(), max());
	}

private:
	static inline std::mt19937_64 generator_{seed_for_RNGs()};
	distribution_type distribution_;
};

template<class ValueT>
class random_range final {
public:
	using distribution_type= random_interval<ValueT>;
	using element_type= typename distribution_type::element_type;

	[[nodiscard]] random_range(element_type const min, element_type const max, element_type const step) noexcept(false)
		: distribution_(static_cast<typename distribution_type::element_type>(min),
			  static_cast<typename distribution_type::element_type>(max)),
		  max_(max),
		  step_(step) {
		if(step_ > (this->max() - this->min())) [[unlikely]] {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("The 'step' ({}) is too large, must be within the range {}.", step_, to_string()), this));
		}
		DEBUG_ASSERT(distribution_.max() <= static_cast<typename distribution_type::element_type>(this->max()));
	}

	[[nodiscard]] bool empty() const noexcept(true) {
		return distribution_.empty();
	}

	[[nodiscard]] element_type min() const noexcept(true) {
		return static_cast<element_type>(distribution_.min());
	}

	[[nodiscard]] element_type max() const noexcept(true) {
		return max_;
	}

	[[nodiscard]] element_type step() const noexcept(true) {
		return step_;
	}

	[[nodiscard]] element_type get() noexcept(false) {
		auto const val= distribution_.get();
		DEBUG_ASSERT(val >= min());
		DEBUG_ASSERT(val <= max());
		return (static_cast<element_type>(val - distribution_.min()) / step_) * step_ + static_cast<element_type>(distribution_.min());
	}

	[[nodiscard]] std::string to_string() const noexcept(false) {
		return fmt::format("[{}, ..., {}]/{}", distribution_.min(), max(), step());
	}

private:
	distribution_type distribution_;
	element_type const max_;
	element_type const step_;
};

}}

#endif
