/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "filename.hpp"

#include <boost/filesystem.hpp>
#include <boost/process/environment.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

std::string
make_filename(std::string&& root, char const* const mask) noexcept(false) {
	DEBUG_ASSERT(mask);
	return boost::filesystem::path(std::move(root)).filename().string()
			 + boost::filesystem::unique_path(mask).string()
			 + '.' + std::to_string(boost::this_process::get_id());
}

std::string
make_filename(char const* const root, char const* const mask) noexcept(false) {
	DEBUG_ASSERT(root);
	DEBUG_ASSERT(mask);
	return make_filename(std::string(root), mask);
}

}}
