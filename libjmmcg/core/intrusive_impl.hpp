/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace intrusive {

template<class V, class LkT>
inline
stack<V, LkT>::stack(stack &&s) noexcept(true)
: head_(), size_ctr() {
	head_.swap(s.head_);
	size_ctr.swap(s.size_ctr);
}

template<class V, class LkT>
inline typename stack<V, LkT>::iterator
stack<V, LkT>::begin() noexcept(true) {
	return iterator(typename iterator::value_type(dynamic_cast<typename iterator::node_ptr_t::value_type *>(head_.get())));
}

template<class V, class LkT>
inline typename stack<V, LkT>::const_iterator
stack<V, LkT>::begin() const noexcept(true) {
	return const_iterator(typename const_iterator::value_type(dynamic_cast<typename iterator::node_ptr_t::value_type *>(head_.get())));
}

template<class V, class LkT>
inline typename stack<V, LkT>::iterator
stack<V, LkT>::end() noexcept(true) {
	return private_::end<V, LkT>();
}

template<class V, class LkT>
inline typename stack<V, LkT>::const_iterator
stack<V, LkT>::end() const noexcept(true) {
	return private_::end<V const, LkT>();
}

template<class V, class LkT>
inline bool
stack<V, LkT>::empty() const noexcept(true) {
	// TODO	DEBUG_ASSERT((!head_.get()) || (head_.get()));
	return size_ctr==0;
}

template<class V, class LkT>
inline typename stack<V, LkT>::size_type
stack<V, LkT>::size() const noexcept(true) {
	return size_ctr.get();
}

template<class V, class LkT>
inline typename stack<V, LkT>::size_type
stack<V, LkT>::size_n() const noexcept(true) {
	size_type ret=0;
	for (auto i=begin(); i!=end(); ++i) {
		++ret;
	}
	return ret;
}

template<class V, class LkT>
inline typename stack<V, LkT>::value_type
stack<V, LkT>::top() noexcept(true) {
	DEBUG_ASSERT(head_.get()==nullptr || head_->sp_count()>0);
	return value_type(head_.get());
}

template<class V, class LkT>
inline typename stack<V, LkT>::value_type
stack<V, LkT>::top() const noexcept(true) {
	DEBUG_ASSERT(head_.get()==nullptr || head_->sp_count()>0);
	return value_type(head_.get());
}

template<class V, class LkT>
inline void
stack<V, LkT>::decrement_size() noexcept(true) {
	size_ctr.assign(
		[](size_type s) {
			return s>size_type{} ? --s : size_type{};
		}
	);
}

template<class V, class LkT>
inline typename stack<V, LkT>::node_details_t::atomic_ptr_t::value_type
stack<V, LkT>::unlink_node(typename node_details_t::atomic_ptr_t &node) noexcept(true) {
	return node.assign(
		[](typename node_details_t::atomic_ptr_t::value_type ptr) {
			if (ptr) {
				DEBUG_ASSERT(dynamic_cast<typename node_details_t::base_t *>(ptr));
				return ptr->next.get();
			} else {
				return typename node_details_t::atomic_ptr_t::value_type{};
			}
		}
	);
}

template<class V, class LkT>
inline void
stack<V, LkT>::pop() noexcept(true) {
	decrement_size();
	// Unlink the node to be deleted first, to ensure only one thread at a time reduces the reference count.
	value_type unlinked_node(unlink_node(head_));
	if (unlinked_node) {
		// Remove the list ownership and function ownership.
		unlinked_node->sp_release();
		DEBUG_ASSERT(unlinked_node->sp_count()>=1);
	}
}

template<class V, class LkT>
inline void
stack<V, LkT>::erase(iterator v) noexcept(true) {
	if (!empty()) {
		if (v==begin()) {
			pop();
		} else if (v!=end()) {
			--size_ctr;
			DEBUG_ASSERT(v.node);
			// We atomically remove the next node, the one we wish to unlink from the stack...
			value_type unlinked_node(unlink_node(v.node.get()));
			DEBUG_ASSERT(unlinked_node->sp_count()>=2);
			// Remove the list ownership.
			DEBUG_ASSERT(unlinked_node.get());
			unlinked_node->sp_release();
			DEBUG_ASSERT(unlinked_node->sp_count()>=1);
		}
	}
}

template<class V, class LkT>
inline typename stack<V, LkT>::size_type
stack<V, LkT>::erase(const_reference v) noexcept(true) {
	iterator start=begin();
	while (start!=end() && *start!=v) {
		++start;
	}
	if (start!=end()) {
		erase(start);
		return 1;
	} else {
		return 0;
	}
}

template<class V, class LkT>
inline void
stack<V, LkT>::pop_front() noexcept(true) {
	pop();
}

template<class V, class LkT>
inline void
stack<V, LkT>::clear() noexcept(true) {
	while (!empty()) {
		erase(begin());
	}
	DEBUG_ASSERT(empty());
}

template<class V, class LkT>
inline
stack<V, LkT>::~stack() noexcept(true) {
	clear();
}

template<class V, class LkT>
inline void
stack<V, LkT>::extend_left_from_front(value_type v) noexcept(true) {
	DEBUG_ASSERT(v->sp_count()>=1);
	// Acquire a reference count for the list, which will now own it also.
	v->sp_acquire();
	head_.assign(
		[&v](typename node_details_t::atomic_ptr_t::value_type old_head) {
			DEBUG_ASSERT(dynamic_cast<typename node_details_t::atomic_ptr_t::value_type>(v.get().get()));
			v->next=old_head;
			DEBUG_ASSERT(v->sp_count()>=1);
			return v.get().get();
		}
	);
	// If a pop_front() occurs, then the sp_count() can only be at least 1 as the v on the stack is the only guarantee.
	DEBUG_ASSERT(v->sp_count()>=1);
	++size_ctr;
}

template<class V, class LkT>
inline void
stack<V, LkT>::push(value_type const &v) noexcept(true) {
	extend_left_from_front(v);
}

template<class V, class LkT>
inline void
stack<V, LkT>::push(value_type &&v) noexcept(true) {
	extend_left_from_front(std::forward<value_type>(v));
}

template<class V, class LkT>
inline void
stack<V, LkT>::push_front(value_type const &v) noexcept(true) {
	push(v);
}

template<class V, class LkT>
inline void
stack<V, LkT>::push_front(value_type &&v) noexcept(true) {
	push(std::forward<value_type>(v));
}

template<class V, class LkT>
inline typename stack<V, LkT>::value_type
stack<V, LkT>::pop_top_nochk() noexcept(true) {
	decrement_size();
	// Unlink the node to be deleted first, to ensure only one thread at a time reduces the reference count.
	value_type unlinked_node(unlink_node(head_));
	if (unlinked_node) {
		// Remove the list ownership and function ownership.
		unlinked_node->sp_release();
		DEBUG_ASSERT(unlinked_node->sp_count()>=1);
	}
	return unlinked_node;
}

template<class V, class LkT>
inline
slist<V, LkT>::slist(slist &&s) noexcept(true) {
	s.head_.sp_acquire();
	collection_.head_.swap(s.head_);
	collection_.head_.sp_release();
	tail_.swap(s.tail_);
	collection_.size_ctr.swap(s.size_ctr);
}

template<class V, class LkT>
inline typename slist<V, LkT>::iterator
slist<V, LkT>::begin() noexcept(true) {
	return collection_.begin();
}

template<class V, class LkT>
inline typename slist<V, LkT>::const_iterator
slist<V, LkT>::begin() const noexcept(true) {
	return collection_.begin();
}

template<class V, class LkT>
inline typename slist<V, LkT>::iterator
slist<V, LkT>::end() noexcept(true) {
	return collection_.end();
}

template<class V, class LkT>
inline typename slist<V, LkT>::const_iterator
slist<V, LkT>::end() const noexcept(true) {
	return collection_.end();
}

template<class V, class LkT>
inline bool
slist<V, LkT>::empty() const noexcept(true) {
	return collection_.empty();
}

template<class V, class LkT>
inline typename slist<V, LkT>::size_type
slist<V, LkT>::size() const noexcept(true) {
	return collection_.size();
}

template<class V, class LkT>
inline typename slist<V, LkT>::size_type
slist<V, LkT>::size_n() const noexcept(true) {
	return collection_.size_n();
}

template<class V, class LkT>
inline void
slist<V, LkT>::clear() noexcept(true) {
	tail_=typename node_details_t::atomic_ptr_t{};
	collection_.clear();
}

template<class V, class LkT>
inline typename slist<V, LkT>::value_type
slist<V, LkT>::front() noexcept(true) {
	return collection_.top();
}

template<class V, class LkT>
inline typename slist<V, LkT>::value_type
slist<V, LkT>::front() const noexcept(true) {
	return collection_.top();
}

template<class V, class LkT>
inline typename slist<V, LkT>::value_type
slist<V, LkT>::back() noexcept(true) {
	DEBUG_ASSERT(tail_.sp_count()>0);
	return value_type(static_cast<typename value_type::value_type *>(tail_.get()));
}

template<class V, class LkT>
inline typename slist<V, LkT>::value_type
slist<V, LkT>::back() const noexcept(true) {
	DEBUG_ASSERT(tail_.sp_count()>0);
	return value_type(static_cast<typename value_type::value_type const *>(tail_.get()));
}

template<class V, class LkT>
inline void
slist<V, LkT>::push(value_type const &v) noexcept(true) {
	collection_.push(v);
	typename node_details_t::atomic_ptr_t::value_type expected{collection_.head_.get()};
	// Do this only once, as then only one thread will perform the operation to set the tail_-pointer to v once and once only, but only if tail_ is empty.
	tail_.compare_exchange_strong(expected, v.get().get());
}

template<class V, class LkT>
inline void
slist<V, LkT>::push(value_type &&v) noexcept(true) {
	typename node_details_t::atomic_ptr_t::value_type expected{};
	// Do this only once, as then only one thread will perform the operation to set the tail_-pointer to v once and once only, but only if tail_ is empty.
	tail_.compare_exchange_strong(expected, v.get().get());
	collection_.push(std::forward<value_type>(v));
}

template<class V, class LkT>
inline void
slist<V, LkT>::push_front(value_type const &v) noexcept(true) {
	push(v);
}

template<class V, class LkT>
inline void
slist<V, LkT>::push_front(value_type &&v) noexcept(true) {
	push(std::forward<value_type>(v));
}

template<class V, class LkT>
inline typename stack<V, LkT>::node_details_t::atomic_ptr_t::value_type
slist<V, LkT>::add_on_right_of(typename node_details_t::atomic_ptr_t node_ptr, value_type v) noexcept(true) {
	DEBUG_ASSERT(v->sp_count()>=0);
	// Acquire a reference count for the list, which will now own it also.
	v->sp_acquire();
	DEBUG_ASSERT(v->sp_count()>=1);
	auto updated_node=node_ptr.assign(
		[&v](typename node_details_t::atomic_ptr_t::value_type) {
			DEBUG_ASSERT(dynamic_cast<typename node_details_t::atomic_ptr_t::value_type>(v.get().get()));
			return v.get().get();
		}
	);
	DEBUG_ASSERT(node_ptr->sp_count()>=1);
	DEBUG_ASSERT(v->sp_count()>=1);
	++collection_.size_ctr;
	return updated_node;
}

template<class V, class LkT>
inline void
slist<V, LkT>::push_back(value_type const &v) noexcept(true) {
	typename node_details_t::atomic_ptr_t old_tail(
		tail_.assign(
			[&v](typename node_details_t::base_t::atomic_ptr_t::value_type) {
				return static_cast<typename node_details_t::base_t::atomic_ptr_t::value_type>(v.get().get());
			}
		)
	);
	// Are we going from empty to one element?
	typename node_details_t::atomic_ptr_t::value_type expected{};
	collection_.head_.compare_exchange_strong(expected, v.get().get());
	if (old_tail) {
		// Are we going from one element to many?
		auto updated_node=add_on_right_of(old_tail, v);
		updated_node->next=v.get().get();
	} else {
		// Are we going from empty to one element?
		add_on_right_of(collection_.head_, v);
	}
}

template<class V, class LkT>
inline void
slist<V, LkT>::push_back(value_type &&v) noexcept(true) {
	typename node_details_t::atomic_ptr_t old_tail(
		tail_.assign(
			[&v](typename node_details_t::base_t::atomic_ptr_t::value_type) {
				return static_cast<typename node_details_t::base_t::atomic_ptr_t::value_type>(v.get().get());
			}
		)
	);
	// Are we going from empty to one element?
	typename node_details_t::atomic_ptr_t::value_type expected{};
	collection_.head_.compare_exchange_strong(expected, v.get().get());
	if (old_tail) {
		// Are we going from one element to many?
		auto updated_node=add_on_right_of(old_tail, std::forward<value_type>(v));
		updated_node->next=v.get().get();
	} else {
		// Are we going from empty to one element?
		add_on_right_of(collection_.head_, std::forward<value_type>(v));
	}
}

template<class V, class LkT>
inline void
slist<V, LkT>::pop() noexcept(true) {
	collection_.pop_front();
	typename node_details_t::atomic_ptr_t::value_type expected{collection_.head_.get()};
	// Do this only once, as then only one thread will perform the operation to set the tail_-pointer to nullptr once and once only, but only if head_->next==nullptr.
	tail_.compare_exchange_strong(expected, typename node_details_t::atomic_ptr_t::value_type{});
}

template<class V, class LkT>
inline void
slist<V, LkT>::pop_front() noexcept(true) {
	pop();
}

template<class V, class LkT>
inline bool
slist<V, LkT>::tail_reachable_from_head() const noexcept(true) {
	bool ret=collection_.empty();
	for (const_iterator i(collection_.begin()); i!=collection_.end(); ++i) {
		if (i.node.get()==tail_.get()) {
			ret=true;
			break;
		}
	}
	return ret;
}

} } }
