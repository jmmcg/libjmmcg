/******************************************************************************
** Copyright © 2025 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "logging.hpp"

#include <boost/mpl/assert.hpp>

#include <algorithm>
#include <array>
#include <experimental/iterator>
#include <sstream>

static jmmcg::LIBJMMCG_VER_NAMESPACE::log::level_severity_type global_log_level{jmmcg::LIBJMMCG_VER_NAMESPACE::log::level_severity_type::trace};

std::string
jmmcg::LIBJMMCG_VER_NAMESPACE::log::all_severities_to_string(std::string const& separator) {
	BOOST_MPL_ASSERT_RELATION(level_severity_type::trace, <=, level_severity_type::fatal);
	const std::array levels_as_strings{
		boost::log::trivial::to_string(level_severity_type::trace),
		boost::log::trivial::to_string(level_severity_type::debug),
		boost::log::trivial::to_string(level_severity_type::info),
		boost::log::trivial::to_string(level_severity_type::warning),
		boost::log::trivial::to_string(level_severity_type::error),
		boost::log::trivial::to_string(level_severity_type::fatal)};
	std::ostringstream os;
	std::copy(levels_as_strings.begin(), levels_as_strings.end(), std::experimental::make_ostream_joiner(os, separator));
	return os.str();
}

void
jmmcg::LIBJMMCG_VER_NAMESPACE::log::set_global_level(level_severity_type level) noexcept(true) {
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::trace);
	global_log_level= level;
}

jmmcg::LIBJMMCG_VER_NAMESPACE::log::level_severity_type
jmmcg::LIBJMMCG_VER_NAMESPACE::log::get_global_level() noexcept(true) {
	return global_log_level;
}
