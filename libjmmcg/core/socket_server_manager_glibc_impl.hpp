/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace server_manager { namespace glibc {

template<class LkT>
inline std::string
manager<LkT>::session::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << socket_;
	return ss.str();
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, typename manager<LkT>::session const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

template<class LkT>
struct manager<LkT>::reset_client {
public:
	reset_client(manager& m, typename session::ptr_type client_connection) noexcept(true)
		: m_(m) {
		DEBUG_ASSERT(client_connection->socket().is_open());
		m_.server_to_client_flow_(client_connection);
	}
	~reset_client() noexcept(true) {
		m_.server_to_client_flow_(typename session::ptr_type{});
	}

private:
	manager& m_;
};

template<class LkT>
inline manager<LkT>::manager(std::atomic_flag& exit_requested, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, server_to_client_flow_t&& server_to_client_flow)
	: exit_requested_(exit_requested),
	  io_context(addr.to_string().c_str(), port_num, min_message_size, max_message_size, timeout, priority, incoming_cpu),
	  server_to_client_flow_(std::move(server_to_client_flow)) {
}

template<class LkT>
inline void
manager<LkT>::run() {
	io_context.run();
}

template<class LkT>
inline void
manager<LkT>::stop() {
	exit_requested_.test_and_set(std::memory_order_seq_cst);
	exit_requested_.notify_all();
	io_context.stop();
	while(!io_context.stopped()) {
		std::this_thread::yield();
	}
}

template<class LkT>
inline void
manager<LkT>::set_options(io_context_t& io_context, socket_t& skt) {
	io_context.set_options(skt);
}

template<class LkT>
inline void
manager<LkT>::set_options(socket_t& skt) {
	set_options(io_context, skt);
}

template<class LkT>
inline std::string
manager<LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << io_context;
	return ss.str();
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, manager<LkT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

template<class ProcessingRules, class SvrHBs, class LkT>
class loopback<ProcessingRules, SvrHBs, LkT>::send_heartbeats final : public manager<LkT>::session {
public:
	using base_t= typename manager<LkT>::session;
	using session= base_t;

	[[nodiscard]] send_heartbeats(typename socket_t::socket_type accept_socket, typename heartbeats_t::report_error_fn_t& report_error)
		: base_t(accept_socket), report_error_(report_error) {
	}

	~send_heartbeats() {
		this->stop();
	}

	static typename session::ptr_type make(typename socket_t::socket_type accept_socket, typename heartbeats_t::report_error_fn_t& report_error) noexcept(false) {
		return std::make_shared<send_heartbeats>(accept_socket, report_error);
	}

	void start() override {
		DEBUG_ASSERT(!heatbeating);
		heatbeating.reset(new heartbeats_t(this->socket_, report_error_));
	}

	void stop() override {
		if(heatbeating) {
			heatbeating.reset();
		}
		base_t::stop();
	}

private:
	typename heartbeats_t::report_error_fn_t& report_error_;
	std::unique_ptr<heartbeats_t> heatbeating;
};

template<class ProcessingRules, class SvrHBs, class LkT>
inline loopback<ProcessingRules, SvrHBs, LkT>::loopback(std::atomic_flag& exit_requested, report_error_fn_t& report_error, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds, socket_priority priority, std::size_t incoming_cpu, proc_rules_t const& proc_rules, libjmmcg::latency_timestamps_itf& ts, server_to_client_flow_t&& server_to_client_flow)
	: base_t(exit_requested, addr, port_num, min_message_size, max_message_size, (heartbeats_t::max_missed_heartbeats * heartbeats_t::heartbeat_interval), priority, incoming_cpu, std::move(server_to_client_flow)),
	  report_error_(report_error),
	  processor(proc_rules) {
	start_accept(
		[this, &ts](session& src_cxn, session& dest_cxn) {
			return this->read_and_process_msgs(src_cxn, dest_cxn, ts);
		});
}

template<class ProcessingRules, class SvrHBs, class LkT>
inline std::string
loopback<ProcessingRules, SvrHBs, LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << base_t::to_string() << ", processor: " << processor;
	return ss.str();
}

template<class ProcessingRules, class SvrHBs, class LkT>
template<class RecvProcMsgs>
inline void
loopback<ProcessingRules, SvrHBs, LkT>::start_accept(RecvProcMsgs proc_fn) noexcept(false) {
	this->io_context.async_accept(
		[this, proc_fn](typename socket_t::socket_type accept_socket) {
			bool client_finished= false;
			{
				auto client_connection(send_heartbeats::make(accept_socket, report_error_));
				DEBUG_ASSERT(client_connection.get());
				this->set_options(client_connection->socket());
				const typename base_t::reset_client resetter(*this, client_connection);
				client_finished= client_connection->process_loopback(proc_fn);
			}
			if(client_finished) {
				start_accept(proc_fn);
			}
		});
}

template<class ProcessingRules, class SvrHBs, class LkT>
inline bool
loopback<ProcessingRules, SvrHBs, LkT>::read_and_process_msgs(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	if(!this->exit_requested_.test(std::memory_order_relaxed)) [[likely]] {
		src_cxn.start();	 // Should really only do this once a logon message received...
		while(!this->exit_requested_.test(std::memory_order_relaxed)) [[likely]] {
			DEBUG_ASSERT(src_cxn.socket().is_open());
			// TODO consider putting this into shared-memory...
			ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
			if(!processor.read_msg_into_buff(src_cxn.socket(), buff)) [[likely]] {
				const libjmmcg::latency_timestamps_itf::period t(ts);
				if(processor.read_and_process_a_msg(buff, src_cxn.socket(), dest_cxn.socket(), ts)) [[unlikely]] {
					return true;
				}
			} else [[unlikely]] {
				return true;
			}
		}
	}
	return true;
}

template<class ExchgCxns, class LkT>
inline forwarding<ExchgCxns, LkT>::forwarding(std::atomic_flag& exit_requested, report_error_fn_t& report_error, boost::asio::ip::address const& addr, unsigned short port_num, std::size_t min_message_size, std::size_t max_message_size, std::chrono::milliseconds timeout, socket_priority priority, std::size_t incoming_cpu, exchg_links_t& exchg_links, libjmmcg::latency_timestamps_itf& ts, server_to_client_flow_t&& server_to_client_flow)
	: base_t(exit_requested, addr, port_num, min_message_size, max_message_size, timeout, priority, incoming_cpu, std::move(server_to_client_flow)),
	  report_error_(report_error),
	  exchg_links_(exchg_links) {
	start_accept(
		[this, &ts](session& src_cxn) {
			return this->read_and_process_msgs(src_cxn, ts);
		});
}

template<class ExchgCxns, class LkT>
template<class RecvProcMsgs>
inline void
forwarding<ExchgCxns, LkT>::start_accept(RecvProcMsgs proc_fn) noexcept(false) {
	this->io_context.async_accept(
		[this, proc_fn](typename socket_t::socket_type accept_socket) {
			bool exit= false;
			{
				auto client_connection(base_t::session::make(accept_socket, report_error_));
				DEBUG_ASSERT(client_connection.get());
				this->set_options(client_connection->socket());
				const typename base_t::reset_client resetter(*this, client_connection);
				exit= client_connection->process_forwarding(proc_fn);
			}
			if(exit) {
				start_accept(proc_fn);
			}
		});
}

template<class ExchgCxns, class LkT>
inline bool
forwarding<ExchgCxns, LkT>::read_and_process_msgs(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	if(!this->exit_requested_.test(std::memory_order_relaxed)) [[likely]] {
		src_cxn.start();	 // Should really only do this once a logon message has been received...
		while(!this->exit_requested_.test(std::memory_order_relaxed)) [[likely]] {
			DEBUG_ASSERT(src_cxn.socket().is_open());
			if(exchg_links_.read_and_process_a_msg(src_cxn, ts)) [[unlikely]] {
				return true;
			}
		}
	}
	return true;
}

}}}}}
