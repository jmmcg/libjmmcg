#ifndef LIBJMMCG_CORE_MEMOPS_HPP
#define LIBJMMCG_CORE_MEMOPS_HPP

/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "blatant_old_msvc_compiler_hacks.hpp"
#include "int128_compatibility.hpp"
#include "max_min.hpp"

#include <boost/mpl/assert.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <memory>
#include <string_view>

#if defined(i386) || defined(__i386__) || defined(__x86_64__)
#	include <immintrin.h>
#endif

/**
	\file	memops.hpp	The objective of these template specialisations is to allow the compiler to correctly select the appropriate, high-speed operation.
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Equivalent to the memcpy operations.
/**
	\param dest	A random-access iterator into a contiguous array of memory, note that the locations will not be pre-initialised, i.e. placement-new will only be used.
	\param src	A random-access iterator into a contiguous array of memory.
	\param n	The number of elements to copy. Must comprise a valid range.

	\see std::memcpy(), std::wmemcpy()
*/
template<class Iter1, class Iter2>
inline void
memcpy(Iter1 dest, Iter2 src, std::size_t n) noexcept(true);
template<>
inline void
memcpy<char*, char const*>(char* dest, char const* src, std::size_t n) noexcept(true);
template<>
inline void
memcpy<wchar_t*, wchar_t const*>(wchar_t* dest, wchar_t const* src, std::size_t n) noexcept(true);

/// Equivalent to the memmove operations.
/**
	\param dest	A random-access iterator into a contiguous array of memory, note that the locations will not be pre-initialised, i.e. placement-new will only be used.
	\param src	A random-access iterator into a contiguous array of memory.
	\param n	The number of elements to move. Must comprise a valid range.

	\see std::memmove(), std::wmemmove()
*/
template<class Iter1, class Iter2>
inline void
memmove(Iter1 dest, Iter2 src, std::size_t n) noexcept(true);
template<>
inline void
memmove<char*, char const*>(char* dest, char const* src, std::size_t n) noexcept(true);
template<>
inline void
memmove<wchar_t*, wchar_t const*>(wchar_t* dest, wchar_t const* src, std::size_t n) noexcept(true);

/// Equivalent to the memset operations.
/**
	\param dest	A random-access iterator into a contiguous array of memory.
	\param i	The value with which to initialise the elements in the range.
	\param n	The number of elements to set. Must comprise a valid range.

	\see std::memset(), std::wmemset()
*/
template<class Iter, class V>
inline typename std::enable_if<std::is_same<typename std::iterator_traits<Iter>::value_type, V>::value>::type
memset(Iter dest, V i, std::size_t n) noexcept(true);
template<>
inline void
memset<char*, char>(char* dest, char i, std::size_t n) noexcept(true);
template<>
inline void
memset<wchar_t*, wchar_t>(wchar_t* dest, wchar_t i, std::size_t n) noexcept(true);

/// Equivalent to the std::memcmp() operations.
/**
	\param src1	A random-access iterator into a contiguous array of memory.
	\param src2	A random-access iterator into a contiguous array of memory.
	\param n	The number of elements to compare. Must comprise a valid range.
	\return true iff the two sequences accessed via the iterators compare equal, element-by-element wise.

	\see std::memcmp(), std::wmemcmp()
*/
template<class Iter>
inline bool
memcmp(Iter src1, Iter src2, std::size_t n) noexcept(true);
template<>
inline bool
memcmp<char const*>(char const* src1, char const* src2, std::size_t n) noexcept(true);
template<>
inline bool
memcmp<wchar_t const*>(wchar_t const* src1, wchar_t const* src2, std::size_t n) noexcept(true);

template<class Val, std::size_t SrcSz, std::size_t DestSz>
void
memcpy(Val const (&src)[SrcSz], Val (&dest)[DestSz]) noexcept(false);

/// Copy the source array to the destination (which may overlap), statically unrolled.
/**
	This method uses 512-bit, 256-bit, 128-bit, 64-bit, 32-bit, 16-bit then byte copies, unrolled to attempt to reduce the number of copy operations performed.

	Algorithmic complexity: O(min(SrcSz, DestSz)/512)

	Now ASAN reports mis-aligned accesses, [1], which could occur using MOV instructions [2] as in 64-bit mode alignment checking of ring 3 can be enabled. But on Intel & AMD mis-aligned access to memory-operands is not an issue in ring 3, so ubsan is conservative (obeying the Standard).

	[1] <a href="https://developers.redhat.com/blog/2014/10/16/gcc-undefined-behavior-sanitizer-ubsan/"/>
	[2] <a href="https://www.felixcloutier.com/x86/mov"/>

	\param src	The source C-style char-array.
	\param dest	The destination C-style char-array.

	\see std::memcpy()
*/
template<
	std::size_t SrcSz,	///< The size of the source array in bytes.
	std::size_t DestSz	///< The size of the destination array in bytes.
	>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr void
memcpy_opt(std::byte const (&src)[SrcSz], std::byte (&dest)[DestSz]) noexcept(true);
template<
	std::size_t SrcSz,	///< The size of the source array in bytes.
	std::size_t DestSz	///< The size of the destination array in bytes.
	>
inline constexpr void
memcpy_opt(char const (&src)[SrcSz], char (&dest)[DestSz]) noexcept(true);

/// Compare the source array to the destination (which may overlap), statically unrolled.
/**
	This method uses 512-bit, 256-bit, 128-bit, 64-bit, 32-bit, 16-bit then byte copies, unrolled to attempt to reduce the number of copy operations performed.

	Algorithmic complexity: O(min(SrcSz, DestSz)/512)

	Now ASAN reports mis-aligned accesses, [1], which could occur using MOV instructions [2] as in 64-bit mode alignment checking of ring 3 can be enabled. But on Intel & AMD mis-aligned access to memory-operands is not an issue in ring 3, so ubsan is conservative (obeying the Standard).

	[1] <a href="https://developers.redhat.com/blog/2014/10/16/gcc-undefined-behavior-sanitizer-ubsan/"/>
	[2] <a href="https://www.felixcloutier.com/x86/mov"/>

	\param src	The first C-style char-array.
	\param second	The second C-style char-array.
	\return True if they are identical arrays, otherwise false.

	\see std::memcmp()
*/
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
	requires(FirstSz > 0 && SecondSz > 0)
inline constexpr bool
memcmp_opt(std::byte const (&first)[FirstSz], std::byte const (&second)[SecondSz]) noexcept(true);
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
inline constexpr bool
memcmp_opt(char const (&first)[FirstSz], char const (&second)[SecondSz]) noexcept(true);
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
inline constexpr bool
memcmp_opt(std::array<char, FirstSz> const& first, std::array<char, SecondSz> const& second) noexcept(true);

/// Find the first occurrence of the character in the string.
/**
	This is based upon algorithms from: <a href="https://www.strchr.com/strcmp_and_strlen_using_sse_4.2"/>.

	Algorithmic complexity: O(min(FirstSz)/256)

	\param haystack	The C-style char-array.
	\param needle	The character to find.
	\return Zero if the character was not found in the first, otherwise the first occurrence of the character in the string.

	\see std::strchr()
*/
template<
	char const needle,
	std::size_t FirstSz	 ///< The size of the first array in bytes.
	>
	requires(FirstSz > 0)
[[gnu::pure]] inline constexpr std::byte const*
strchr_opt(std::byte const (&haystack)[FirstSz]) noexcept(true);
template<
	char const needle,
	std::size_t FirstSz	 ///< The size of the first array in bytes.
	>
[[gnu::pure]] inline constexpr char const*
strchr_opt(char const (&haystack)[FirstSz]) noexcept(true);

/// Find if the second string is a sub-string of the first.
/**
	This is based upon algorithms from: <a href="https://github.com/WojciechMula/sse4-strstr/blob/master/avx2-strstr-v2.cpp"/> & <a href="http://0x80.pl/articles/simd-strfind.html#generic-sse-avx2"/>.

	Algorithmic complexity: O(min(FirstSz, SecondSz)/256)

	\param haystack	The first C-style char-array, that must be in whole 32-byte units.
	\param needle	The second C-style char-array, up to a maximum size of 32 bytes.
	\return Zero if the second string was not found in the first, otherwise the offset of the beginning of that string in the first.

	\see std::strstr()
*/
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
	requires(FirstSz > 0) && ((FirstSz % 32) == 0) && (SecondSz > 1) && (SecondSz <= 32)
[[gnu::pure]] inline constexpr std::byte const* strstr_opt(std::byte const (&haystack)[FirstSz], std::byte const (&needle)[SecondSz]) noexcept(true);
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
[[gnu::pure]] inline constexpr char const*
strstr_opt(char const (&haystack)[FirstSz], char const (&needle)[SecondSz]) noexcept(true);
template<
	std::size_t FirstSz,	  ///< The size of the first array in bytes.
	std::size_t SecondSz	  ///< The size of the second array in bytes.
	>
[[gnu::pure]] inline constexpr char const*
strstr_opt(std::array<char, FirstSz> const& haystack, std::array<char, SecondSz> const& needle) noexcept(true);

template<
	std::size_t SrcSz,	///< The size of the source array in bytes.
	std::size_t DestSz	///< The size of the destination array in bytes.
	>
inline constexpr void
memcpy_opt(std::array<char, SrcSz> const& src, std::array<char, DestSz>& dest) noexcept(true);
template<
	std::size_t SrcSz,	///< The size of the source array in bytes.
	std::size_t DestSz	///< The size of the destination array in bytes.
	>
inline constexpr void
memcpy_opt(std::array<std::byte, SrcSz> const& src, std::array<std::byte, DestSz>& dest) noexcept(true);

template<
	std::size_t Sz	  ///< The size of the arrays in bytes.
	>
inline bool
memcmp(std::array<char, Sz> const& src1, std::array<char, Sz> const& src2) noexcept(true);
template<
	std::size_t Sz	  ///< The size of the arrays in bytes.
	>
inline bool
memcmp(std::array<std::byte, Sz> const& src1, std::array<std::byte, Sz> const& src2) noexcept(true);

template<
	std::size_t Sz	  ///< The size of the arrays in bytes.
	>
inline bool
operator==(std::array<char, Sz> const& src1, std::array<char, Sz> const& src2) noexcept(true);
template<
	std::size_t Sz	  ///< The size of the arrays in bytes.
	>
inline bool
operator==(std::array<std::byte, Sz> const& src1, std::array<std::byte, Sz> const& src2) noexcept(true);

/// Copy the source array to the destination (which may overlap) byte-by-byte, statically unrolled.
/**
	Algorithmic complexity: O(min(SrcSz, DestSz))

	\param src	The source C-style array.
	\param dest	The destination C-style array.

	\see std::memcpy()
*/
template<
	std::size_t SrcSz,	///< The size of the source array in bytes.
	std::size_t DestSz	///< The size of the destination array in bytes.
	>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr void
memcpy_slow(char const (&src)[SrcSz], char (&dest)[DestSz]) noexcept(true);

/**
 * These copy()s all pad the remainder of the output buffer with '\0's as needed.
 */
template<class T>
inline T
copy(std::string_view src) noexcept(true);

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr std::array<char, DestSz>
copy(std::array<char, SrcSz> const& src) noexcept(true);

template<
	std::size_t SrcSz,
	std::size_t DestSz>
	requires(SrcSz > 0 && DestSz > 0)
inline constexpr std::array<std::byte, DestSz>
copy(std::array<std::byte, SrcSz> const& src) noexcept(true);

}}

#include "memops_impl.hpp"

#endif
