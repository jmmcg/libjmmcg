#ifndef LIBJMMCG_CORE_BASIC_EXCEPTION_HPP
#define LIBJMMCG_CORE_BASIC_EXCEPTION_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "blatant_old_msvc_compiler_hacks.hpp"

#include <boost/core/demangle.hpp>
#include <boost/exception/all.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/stacktrace.hpp>
#include <boost/throw_exception.hpp>

#include <cstring>
#include <stdexcept>
#include <string>
#include <string_view>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

using errinfo_parameters= boost::error_info<struct errinfo_parameters_, std::string>;
using errinfo_revision= boost::error_info<struct errinfo_revision_, const std::string_view>;
using errinfo_stacktrace= boost::error_info<struct errinfo_stacktrace_, boost::stacktrace::stacktrace>;

/// Create an exception object with extra, useful information.
/**
 * \Note: The exception automatically contains a message, the type information and a stacktrace. To get details regarding the symbols in the backtrace, see <a href="https://www.boost.org/doc/libs/1_83_0/doc/html/stacktrace/configuration_and_build.html"/>.
 *
 * @tparam Exception The type of the ISO C++ exception to throw.
 * @tparam Data The type information for the current context, passed as a parameter.
 * @param s The string message to be passed to the exception, returned by std::exception::what().
 * @return A streamable object suitable for either streaming in more boost:err_info details. Also suitable for throwing via BOOST_THROW_EXCEPTION(...)
 *
 * \see std::exception::what(), boost:err_info, BOOST_THROW_EXCEPTION, boost::stacktrace::stacktrace
 */
template<class Exception>
NEVER_INLINE auto
throw_exception(std::string&& s, boost::stacktrace::stacktrace&& stacktrace= boost::stacktrace::stacktrace());
template<class Exception, class Data>
NEVER_INLINE auto
throw_exception(std::string&& s, Data, boost::stacktrace::stacktrace&& stacktrace= boost::stacktrace::stacktrace());

/// Create an exception object with C-library function call details including the rrno and further extra, useful information.
/**
 * \Note: The exception automatically contains the C-function name and errno, also converted to a string, amongst other details.
 *
 * @tparam Exception The type of the ISO C++ exception to throw.
 * @param s The string message to be passed to the exception, returned by std::exception::what().
 * @param d The type information for the current context, passed as a parameter.
 * @param crt_fn_name	The name of the C-function called.
 * @param err	The errno is automatically stored.
 * @return A streamable object suitable for either streaming in more boost:err_info details. Also suitable for throwing via BOOST_THROW_EXCEPTION(...)
 *
 * \see throw_exception
 */
template<class Exception>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], int err= errno, boost::stacktrace::stacktrace&& stacktrace= boost::stacktrace::stacktrace());
template<class Exception, class Data>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], Data d, int err= errno, boost::stacktrace::stacktrace&& stacktrace= boost::stacktrace::stacktrace());

}}

#include "basic_exception_impl.hpp"

#endif
