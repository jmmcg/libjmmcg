#ifndef LIBJMMCG_CORE_PRIVATE_MANAGE_CONTAINER_ARGS_HPP
#define LIBJMMCG_CORE_PRIVATE_MANAGE_CONTAINER_ARGS_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"
#include "../rw_locking.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

	template<class Colln>
	struct input_safe_colln {
		typedef Colln container_type;
		typedef typename container_type::container_type::const_iterator iterator;
		typedef typename container_type::size_type size_type;

		explicit constexpr input_safe_colln(container_type const &c) noexcept(true)
		: cont(c) {
		}

		void __fastcall lock() const noexcept(true) {
			cont.pop_lock().lock();
		}

		void __fastcall unlock() const noexcept(true) {
			cont.pop_lock().unlock();
		}

		iterator __fastcall begin() const noexcept(true) {
			return cont.colln().begin();
		}
		iterator __fastcall end() const noexcept(true) {
			return cont.colln().end();
		}
		typename container_type::size_type __fastcall size() const noexcept(true) {
			return cont.colln().size();
		}

	private:
		container_type const &cont;
	};

	template<class Colln, class Iter=typename Colln::const_iterator>
	struct input_safe_range {
		typedef Colln container_type;
		typedef Iter iterator;
		typedef typename container_type::size_type size_type;

		constexpr input_safe_range(iterator const &b, iterator const &e) noexcept(true)
		: beg(b), en(e) {
		}

		static constexpr void __fastcall lock() noexcept(true) {
		}

		static constexpr void __fastcall unlock() noexcept(true) {
		}

		iterator __fastcall begin() noexcept(true) {
			return beg;
		}
		iterator __fastcall end() const noexcept(true) {
			return en;
		}
		constexpr typename iterator::difference_type __fastcall size() const noexcept(true) {
			return std::distance(beg, en);
		}

	private:
		iterator const beg;
		iterator const en;
	};

	template<class Colln, class Iter=typename Colln::iterator>
	struct output_safe_range {
		typedef Colln container_type;
		typedef Iter iterator;
		typedef typename container_type::size_type size_type;

		constexpr output_safe_range(iterator const &b, iterator const &e) noexcept(true)
		: beg(b), en(e) {
		}

		static constexpr void __fastcall lock() noexcept(true) {
		}

		static constexpr void __fastcall unlock() noexcept(true) {
		}

		iterator __fastcall begin() noexcept(true) {
			return beg;
		}
		iterator __fastcall end() noexcept(true) {
			return en;
		}
		constexpr typename iterator::difference_type __fastcall size() const noexcept(true) {
			return std::distance(beg, en);
		}

	private:
		iterator const beg;
		iterator const en;
	};

	template<class Colln>
	class output_safe_colln_rw_lk {
	public:
		typedef Colln container_type;
		typedef lock::rw::decaying_write_impl<typename container_type::atomic_t> atomic_t;
		typedef typename container_type::container_type::iterator iterator;
		typedef typename container_type::size_type size_type;

		explicit __stdcall output_safe_colln_rw_lk(container_type &c) noexcept(true)
		: lk(c.push_lock()), cont(c) {
		}

		void __fastcall lock() noexcept(true) {
			lk.lock();
		}
		void __fastcall unlock() noexcept(true) {
			lk.unlock();
		}

		void __fastcall decay() noexcept(true) {
			lk.decay();
		}

		iterator __fastcall begin() noexcept(true) {
			return cont.colln().begin();
		}
		iterator __fastcall end() noexcept(true) {
			return cont.colln().end();
		}
		typename container_type::size_type __fastcall size() const noexcept(true) {
			return cont.colln().size();
		}
		void resize_output(typename container_type::size_type sz) noexcept(false) {
			cont.resize_noinit_nolk(sz);
		}

	private:
		atomic_t lk;
		container_type &cont;
	};

	template<class Colln>
	struct output_safe_colln_simple_lk {
		typedef Colln container_type;
		typedef typename container_type::container_type::iterator iterator;
		typedef typename container_type::size_type size_type;

		explicit constexpr output_safe_colln_simple_lk(container_type &c) noexcept(true)
		: cont(c) {
		}

		void __fastcall lock() const noexcept(true) {
			cont.push_lock().lock();
		}

		void __fastcall unlock() const noexcept(true) {
			cont.push_lock().unlock();
		}

		iterator __fastcall begin() noexcept(true) {
			return cont.colln().begin();
		}
		iterator __fastcall end() noexcept(true) {
			return cont.colln().end();
		}
		typename container_type::size_type __fastcall size() const noexcept(true) {
			return cont.colln().size();
		}
		void resize_output(size_type const sz) noexcept(true) {
			cont.resize_noinit_nolk(sz);
		}

	private:
		container_type &cont;
	};

	template<class CollnIn>
	struct one_container {
		typedef input_safe_colln<CollnIn> input_t;
		typedef typename input_t::iterator in_iterator;
		typedef typename input_t::size_type size_type;

		input_t const input1;

		explicit constexpr one_container(CollnIn const &c_in) noexcept(true)
		: input1(c_in) {
		}

		void __fastcall lock() noexcept(true) {
			input1.lock();
		}

		void __fastcall unlock() noexcept(true) {
			input1.unlock();
		}
		static constexpr void resize_output(size_type const) noexcept(true) {
		}
	};

	template<class Colln>
	struct one_output_container_rw_lk {
		typedef output_safe_colln_rw_lk<Colln> input_t;
		typedef input_t output_t;
		typedef typename input_t::iterator in_iterator;
		typedef typename output_t::size_type size_type;

		input_t input1;

		explicit constexpr one_output_container_rw_lk(Colln &c) noexcept(true)
		: input1(c) {
		}

		void __fastcall lock() noexcept(true) {
			input1.lock();
		}

		void __fastcall unlock() noexcept(true) {
			input1.unlock();
		}
		void resize_output(size_type const sz) noexcept(true) {
			input1.resize_output(sz);
			input1.decay();
		}
	};

	template<class Colln>
	struct one_output_container_simple_lk {
		typedef output_safe_colln_simple_lk<Colln> input_t;
		typedef input_t output_t;
		typedef typename input_t::iterator in_iterator;
		typedef typename output_t::size_type size_type;

		input_t input1;

		explicit constexpr one_output_container_simple_lk(Colln &c) noexcept(true)
		: input1(c) {
		}

		void __fastcall lock() noexcept(true) {
			input1.lock();
		}

		void __fastcall unlock() noexcept(true) {
			input1.unlock();
		}
		void resize_output(size_type const sz) noexcept(true) {
			input1.resize_output(sz);
			input1.decay();
		}
	};

	template<class CollnIn, class CollnOut>
	struct two_containers {
		typedef input_safe_colln<CollnIn> input_t;
		typedef output_safe_colln_rw_lk<CollnOut> output_t;
		typedef typename input_t::iterator in_iterator;
		typedef typename output_t::iterator out_iterator;
		typedef typename output_t::size_type size_type;

		input_t const input1;
		output_t output;

		constexpr two_containers(CollnIn const &c_in, CollnOut &c_out) noexcept(true)
		: input1(c_in), output(c_out) {
		}

		void __fastcall lock() noexcept(true) {
			input1.lock();
			output.lock();
		}

		void __fastcall unlock() noexcept(true) {
			input1.unlock();
			output.unlock();
		}
		void resize_output(size_type const sz) noexcept(false) {
			output.resize_output(sz);
			output.decay();
			DEBUG_ASSERT(output.size()>=input1.size());
		}
	};

	template<class CollnIn, class CollnOut, class IterIn, class IterOut>
	struct two_ranges {
		typedef input_safe_range<CollnIn, IterIn> input_t;
		typedef output_safe_range<CollnOut, IterOut> output_t;
		typedef IterIn in_iterator;
		typedef IterOut out_iterator;
		typedef typename output_t::size_type size_type;

		input_t input1;
		output_t output;

		__stdcall two_ranges(in_iterator const &b1, in_iterator const &e1, out_iterator const &b2) noexcept(true)
		: input1(b1, e1), output(b2, std::next(b2, std::distance(b1, e1))) {
		}

		static constexpr void __fastcall lock() noexcept(true) {
		}

		static constexpr void __fastcall unlock() noexcept(true) {
		}
		static constexpr void resize_output(size_type const) noexcept(true) {
		}
	};

	template<class CollnIn, class CollnOut, class IterIn, class IterOut>
	struct two_out_ranges {
		typedef output_safe_range<CollnIn, IterIn> input_t;
		typedef output_safe_range<CollnOut, IterOut> output_t;
		typedef IterIn in_iterator;
		typedef IterOut out_iterator;
		typedef typename output_t::size_type size_type;

		input_t input1;
		output_t output;

		__stdcall two_out_ranges(in_iterator const &b1, in_iterator const &e1, out_iterator const &b2) noexcept(true)
		: input1(b1, e1), output(b2, std::next(b2, std::distance(b1, e1))) {
		}

		static constexpr void __fastcall lock() noexcept(true) {
		}

		static constexpr void __fastcall unlock() noexcept(true) {
		}
		static constexpr void resize_output(size_type const) noexcept(true) {
		}
	};

	template<class CollnIn1, class CollnIn2, class CollnOut>
	struct three_containers {
		typedef input_safe_colln<CollnIn1> input1_t;
		typedef input_safe_colln<CollnIn2> input2_t;
		typedef output_safe_colln_rw_lk<CollnOut> output_t;
		typedef typename input1_t::iterator in_iterator;
		typedef typename input2_t::iterator in2_iterator;
		typedef typename output_t::iterator out_iterator;
		typedef typename output_t::size_type size_type;

		input1_t const input1;
		input2_t const input2;
		output_t output;

		constexpr three_containers(CollnIn1 const &c1_in, CollnIn2 const &c2_in, CollnOut &c_out) noexcept(true)
		: input1(c1_in), input2(c2_in), output(c_out) {
		}

		void __fastcall lock() noexcept(true) {
			input1.lock();
			input2.lock();
			output.lock();
		}

		void __fastcall unlock() noexcept(true) {
			input1.unlock();
			input2.unlock();
			output.unlock();
		}
		void resize_output(size_type const sz) noexcept(false) {
			output.resize_output(sz);
			output.decay();
			DEBUG_ASSERT(output.size()>=input1.size());
		}
	};

} } } }

#endif
