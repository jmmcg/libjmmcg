#ifndef LIBJMMCG_CORE_PRIVATE_THREAD_BASE_HPP
#define LIBJMMCG_CORE_PRIVATE_THREAD_BASE_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../core/exception.hpp"
#include "../../core/logging.hpp"
#include "../../core/thread_os_traits.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

/// This class is used to implement a raw class that wraps using a raw thread.
/**
	This provides the thread-as-an-active-class threading model.
	It is very basic and provides a very bare minumium. But it does ensure that any exceptions thrown by the process() method are captured and stored. If not examined, any such exception will be throw in the dtor. Moreover when the object exits, the thread is guaranteed to be exited. Therefore any classes that derive from this *must* ensure that wait_thread_exit() will be called in that ultimate, derived, dtor before that dtor exits, to ensure that the process() function does not attempt to access deleted memory in the derived class(es).

	\see ppd::wrapper, std::thread, std::jthread
*/
template<generic_traits::api_type::element_type API, typename Mdl>
class thread_base {
public:
	typedef thread_os_traits<API, Mdl> os_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef typename os_traits::lock_traits lock_traits;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= lock_traits::critical_section_type::memory_access_mode;

	const typename thread_traits::api_params_type::suspend_period_ms exit_wait_period;

	/// Start the underlying kernel thread.
	virtual void __fastcall create_running() noexcept(false);

	const typename thread_traits::api_params_type& __fastcall params() const noexcept(true) {
		return thread_params;
	}

	const typename thread_traits::api_params_type::suspend_count __fastcall suspend() noexcept(true);
	const typename thread_traits::api_params_type::suspend_count __fastcall resume() noexcept(true);
	const typename thread_traits::api_params_type::states::element_type __fastcall state() const noexcept(true);
	bool __fastcall is_running() const noexcept(true);

	typename thread_traits::api_params_type::priority_type __fastcall kernel_priority() const noexcept(false);
	void __fastcall kernel_priority(const typename thread_traits::api_params_type::priority_type priority) noexcept(false);
	typename thread_traits::api_params_type::processor_mask_type __fastcall kernel_affinity() const noexcept(false);
	void __fastcall kernel_affinity(const typename thread_traits::api_params_type::processor_mask_type& mask) noexcept(false);
	void __fastcall set_name(typename thread_traits::thread_name_t const& name) noexcept(false);
	template<std::size_t NameSize>
		requires(NameSize <= thread_traits::max_thread_name_length)
	void __fastcall set_name(char const (&name)[NameSize]) noexcept(false);
	typename thread_traits::thread_name_t __fastcall get_name() const noexcept(false);

	virtual const tstring __fastcall to_string() const noexcept(false);

	/**
		We don't gobble exceptions, because thread_pools might want to know if a member fails...
		TODO Is this true?: Note that if this method throws an exception, wait_thread_exit() may lock up unless the situation is managed further.
	*/
	virtual void __fastcall request_exit() const noexcept(false);

protected:
	/// This is to allow exceptions thrown in the contained thread to be (hopefully) propagated back to another thread on which that exception can be caught & handled.
	typename os_traits::thread_exception exception_thrown_in_thread;
	// Ensure that assignment to the thread params is atomic.
	mutable typename lock_traits::critical_section_type thread_params_lock;
	mutable typename thread_traits::api_params_type thread_params;

	/// Create the wrapper object. Note that the underlying kernel thread will not have been started.
	/**
		\param exit_wait_p	The length of time, in ms, that the dtor will wait for the thread to exit before terminating the thread. Such termination may cause undefined behaviour to result, depending upon the details of how the underlying OS manages thread termination.
	*/
	explicit __stdcall thread_base(const typename thread_traits::api_params_type::suspend_period_ms exit_wait_p) noexcept(false);
	explicit __stdcall thread_base(const thread_base& tb) noexcept(true);

	/// Thread base dtor.
	/**
		Don't throw any registered "exception_thrown_in_thread" here, because evil leaks will happen in the thread pool. YUCK.
		Ignore any bad exit states from the thread too. Tough.
		Ensure that wait_thread_exit() is called from the most-derived dtor, otherwise UB will happen.

		\see wait_thread_exit()
	*/
	virtual __stdcall ~thread_base() noexcept(false);

	/**
		This must be called from the most-derived class, otherwise you could get "pure virtual call" errors, because process() could be called, or other dependent class members could be deleted before the thread exists.
	*/
	void __fastcall wait_thread_exit() noexcept(false);
	/// This is the main "work" function. Implement it to do some work in the thread.
	/**
		Any exceptions thrown will be caught in the core_work_fn() and reported to the user (or calling thread) by re-throwing, at the latest, in the destructor.
	*/
	virtual typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false)= 0;

private:
	static const typename thread_traits::api_params_type::states::element_type __fastcall state_nolk(typename thread_traits::api_params_type const& params) noexcept(true);

	/**
		Perform some sanity checks then forwards to the process() function which actually does the work.
	*/
	typename thread_traits::api_params_type::states::element_type __fastcall process_chk() noexcept(false) {
		DEBUG_ASSERT(this->thread_params.id == thread_traits::get_current_thread_id());
		const auto state= this->process();
		// Can't lock here, because we'll deadlock in wait_thread_exit(), also can't set thread.id to zero, otherwise we'll race with wait_thread_exit(), and possibly crash in it.
		thread_params.state= state;
		return state;
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"

	/// This is the core function that is called by the the os-level thread to eventually do the work.
	/**
		It simply forwards to the execute_chk() function which actually does the work.
	*/
	static typename thread_traits::api_params_type::core_work_fn_ret_t __cdecl core_work_fn(typename thread_traits::api_params_type::core_work_fn_arg_t) noexcept(false);

#pragma GCC diagnostic pop
};

/**
	\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
*/
template<generic_traits::api_type::element_type API, typename Mdl>
inline tostream& __fastcall
operator<<(tostream& os, thread_base<API, Mdl> const& th) {
	return os << th.to_string();
}

/// Ensure that the deleter is not called for work that is stack-allocated.
/**
	Determine if the work should be deleted at construction time, so that if the object goes out of scope by the time the dtor is run, we already know it should not be deleted, and therefore do not do anything.
*/
template<class E>
class eval_shared_deleter_t {
public:
	using element_type= E;
	using atomic_ptr_t= typename element_type::atomic_ptr_t;
	using work_complete_t= typename element_type::value_type::work_complete_t;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (atomic_ptr_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && work_complete_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	explicit eval_shared_deleter_t(element_type& e) noexcept(true)
		: elem(), stack_based(e->sp_noop_ctr()) {
		// Ensure we don't re-try processing work that threw an exception.
		e.get().swap(elem);
		DEBUG_ASSERT(!dynamic_cast<typename atomic_ptr_t::value_type>(e.get().get()));
		DEBUG_ASSERT(stack_based || dynamic_cast<typename atomic_ptr_t::value_type>(elem.get()));
		DEBUG_ASSERT(stack_based || !dynamic_cast<typename element_type::no_ref_counting*>(elem.get()));
		DEBUG_ASSERT(stack_based || dynamic_cast<typename element_type::ctr_type*>(elem.get()));
		DEBUG_ASSERT(stack_based || *dynamic_cast<typename element_type::ctr_type*>(elem.get()) >= 1);
	}

	~eval_shared_deleter_t() noexcept(true) {
		// execution_contexts (which are stack-based) may be already deleted, so the RTTI object-type may be long gone, good job we pre-computed it...
		if(!stack_based) {
			// Ok, on the heap, so we own it, so de-ref the f*cker.
			element_type del;
			del.get().swap(elem);
			// TODO: There is a race condition with the nonjoinable_buff(...) transfers in subdivide_n_gen_wk, in which the heap::memory_buff may be deleted before this should occur...
			DEBUG_ASSERT(!elem.get());
			DEBUG_ASSERT(!dynamic_cast<typename element_type::no_ref_counting*>(del.get().get()));
			DEBUG_ASSERT(dynamic_cast<typename element_type::ctr_type*>(del.get().get()));
			DEBUG_ASSERT(*dynamic_cast<typename element_type::ctr_type*>(del.get().get()) >= 1);
		}
	}

	template<class UpdStats, class EdgeAnn>
	void
	process_the_work(UpdStats&& update_stats, EdgeAnn const e_details) noexcept(false) {
		// Ensure we don't re-try processing work that threw an exception.
		if(elem.get()) {
			// TODO Set start & end times of processing op, so that time-order of nodes in DFG can be seen.
			if(elem->result_traits() == generic_traits::return_data::element_type::joinable) {
				// Make sure that we signal that the work has been completed before the related execution_context might be deleted from the stack, .i.e well before the dtor of this object.
				const set_work_complete work_done(*elem->work_complete());
				elem->process_joinable(e_details);
			} else {
				elem->process_nonjoinable(e_details);
			}
			update_stats();
		}
	}

private:
	class set_work_complete {
	public:
		explicit __stdcall set_work_complete(work_complete_t& wc) noexcept(true)
			: work_complete(wc) {
		}

		set_work_complete(set_work_complete const&)= delete;

		__stdcall ~set_work_complete() noexcept(true) {
			work_complete.set();
		}

	private:
		work_complete_t& work_complete;
	};

	atomic_ptr_t elem;
	const bool stack_based;	  ///< We need to evaluate this at object construction time, because when the dtor is called, the object no longer may be valid (another thread may have let the containing execution_context go out of scope), so we can't determine them, at run-time, if it should be deleted or not.
};

}}}}

#include "thread_base_impl.hpp"

#ifdef WIN32
#	include "../experimental/NT-based/NTSpecific/thread_base_impl.hpp"
#else
#endif

#endif
