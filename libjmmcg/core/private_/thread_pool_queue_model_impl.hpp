/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline unsigned long __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::min_time(generic_traits::memory_access_modes::element_type mode) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const pool_size_type num_threads= this->pool_size();
		const double log_ps= std::log(static_cast<double>(num_threads));
		const double ideal_min_time= ws / static_cast<double>(num_threads) + log_ps;
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(std::ceil(ideal_min_time));
		} else {
			return static_cast<unsigned long>(std::ceil(ideal_min_time * log_ps));
		}
	} else {
		return 0UL;
	}
}

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class T>
inline unsigned long __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::min_time(T const&) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const pool_size_type num_threads= this->pool_size();
		const double log_ps= std::log(static_cast<double>(num_threads));
		const double ideal_min_time= ws / static_cast<double>(num_threads) + log_ps;
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && T::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(std::ceil(ideal_min_time));
		} else {
			return static_cast<unsigned long>(std::ceil(ideal_min_time * log_ps));
		}
	} else {
		return 0UL;
	}
}

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline unsigned long __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::min_processors(generic_traits::memory_access_modes::element_type mode) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const double ideal_num_processors= std::ceil(ws / std::log(ws));
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(ideal_num_processors);
		} else {
			return static_cast<unsigned long>(ideal_num_processors * std::log(static_cast<double>(this->pool_size())));
		}
	} else {
		return 0UL;
	}
}

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class T>
inline unsigned long __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::min_processors(T const&) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const double ideal_num_processors= std::ceil(ws / std::log(ws));
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && T::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(ideal_num_processors);
		} else {
			return static_cast<unsigned long>(ideal_num_processors * std::log(static_cast<double>(this->pool_size())));
		}
	} else {
		return 0UL;
	}
}

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline typename thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::queue_size_type __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::batch_size(queue_size_type const sz) const noexcept(true) {
	// We need to ensure that the number of async_wk items generated is a power of two, and the number of threads in the pool may not be. So compute the next largest power of two just larger than or equal to that.
	const pool_size_type ps= this->pool_size();
	const double log2_ps= std::log(double(ps)) / std::log(2);
	const pool_size_type ceil_log2_ps= static_cast<pool_size_type>(std::ceil(log2_ps));
	const pool_size_type logical_pool_size= (ceil_log2_ps == log2_ps ? ps : 1 << ceil_log2_ps);
	return std::max(
		static_cast<queue_size_type>(1),
		sz / std::max(logical_pool_size, static_cast<pool_size_type>(1)));
}

template<
	template<class>
	class QM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline tostream& __fastcall thread_pool_queue_model<QM<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, Ps, PTT, Pt>::to_stream(tostream& os) const noexcept(false) {
	os
		<< static_cast<base1_t const&>(*this);
	//		<<_T(", queue details: ")<<static_cast<base_t const &>(*this)
	//		<<_T(", pool details: ")<<pool;
	return os;
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline unsigned long __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::min_time(generic_traits::memory_access_modes::element_type mode) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const pool_size_type num_threads= this->pool_size();
		const double log_ps= std::log(static_cast<double>(num_threads));
		const double ideal_min_time= ws / static_cast<double>(num_threads) + log_ps;
		if(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
			&& mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(std::ceil(ideal_min_time));
		} else {
			return static_cast<unsigned long>(std::ceil(ideal_min_time * log_ps));
		}
	} else {
		return 0UL;
	}
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class T>
inline unsigned long __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::min_time(T const&) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const pool_size_type num_threads= this->pool_size();
		const double log_ps= std::log(static_cast<double>(num_threads));
		const double ideal_min_time= ws / num_threads + log_ps;
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && T::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(std::ceil(ideal_min_time));
		} else {
			return static_cast<unsigned long>(std::ceil(ideal_min_time * log_ps));
		}
	} else {
		return 0UL;
	}
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline unsigned long __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::min_processors(generic_traits::memory_access_modes::element_type mode) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const double ideal_num_processors= std::ceil(ws / std::log(ws));
		if(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
			&& mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(ideal_num_processors);
		} else {
			return static_cast<unsigned long>(ideal_num_processors * std::log(static_cast<double>(this->pool_size())));
		}
	} else {
		return 0UL;
	}
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class T>
inline unsigned long __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::min_processors(T const&) const noexcept(true) {
	const double ws= double(this->queue().size());
	if(ws > 0.0) {
		const double ideal_num_processors= std::ceil(ws / std::log(ws));
		if constexpr(memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
						 && T::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access) {
			return static_cast<unsigned long>(ideal_num_processors);
		} else {
			return static_cast<unsigned long>(ideal_num_processors * std::log(static_cast<double>(this->pool_size())));
		}
	} else {
		return 0UL;
	}
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline typename thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::queue_size_type __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::batch_size(queue_size_type const sz) const noexcept(true) {
	// We need to ensure that the number of async_wk items generated is a power of two, and the number of threads in the pool may not be. So compute the next largest power of two just larger than or equal to that.
	const pool_size_type ps= this->pool_size();
	const double log2_ps= std::log(double(ps)) / std::log(2);
	const pool_size_type ceil_log2_ps= static_cast<pool_size_type>(std::ceil(log2_ps));
	const pool_size_type logical_pool_size= (ceil_log2_ps == log2_ps ? ps : 1 << ceil_log2_ps);
	return std::max(
		static_cast<queue_size_type>(1),
		sz / std::max(logical_pool_size, static_cast<pool_size_type>(1)));
}

template<
	pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t SM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
inline tostream& __fastcall thread_pool_queue_model<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<SM>>, Ps, PTT, Pt>::to_stream(tostream& os) const noexcept(false) {
	os
		<< static_cast<base1_t const&>(*this);
	//		<<_T(", queue details: ")<<static_cast<base_t const &>(*this)
	//		<<_T(", pool details: ")<<pool;
	return os;
}

template<
	class DM1,
	pool_traits::size_mode_t::element_type Ps1,
	typename PTT1,
	class Pt1>
inline tostream& __fastcall
operator<<(tostream& os, thread_pool_queue_model<DM1, Ps1, PTT1, Pt1> const& t) {
	return t.to_stream(os);
}

}}}}
