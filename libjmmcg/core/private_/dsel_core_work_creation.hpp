#ifndef LIBJMMCG_CORE_PRIVATE_DSEL_CORE_WORK_CREATION_HPP
#define LIBJMMCG_CORE_PRIVATE_DSEL_CORE_WORK_CREATION_HPP

/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_work_closure.hpp"

#include <boost/type_traits/function_traits.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

/**
	If the is an error in thread_pool_base::create_direct in the instantiation of decltype because this class is selected as the best match, it is possible that he incorrect operator<<() overload is being selected in "thread_desl_types.hpp".
*/
template<class Cls, class CFG>
struct crack_process_fn_traits {
	static inline constexpr const bool no_except= false;
	typedef Cls class_type;
	typedef typename Cls::result_type argument_type;
	static inline constexpr const bool is_const= false;
};
template<class Cls, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)() const noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void() const noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef void argument_type;
	static inline constexpr const bool is_const= true;
	template<void (__fastcall Cls::*Fn)() const noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_void_static_const<Cls, no_except, Fn, CFG> result;
	};
};
template<class Cls, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)() noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void() noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef void argument_type;
	static inline constexpr const bool is_const= false;
	template<void (__fastcall Cls::*Fn)() noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_void_static<Cls, no_except, Fn, CFG> result;
	};
};
template<class Cls, class ResT, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)(ResT&) const noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void(ResT&) noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef ResT argument_type;
	static inline constexpr const bool is_const= true;
	template<void (__fastcall Cls::*Fn)(ResT&) const noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_static_const<Cls, ResT, no_except, Fn, CFG> result;
	};
};
template<class Cls, class ResT, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)(ResT&) noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void(ResT&) noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef ResT argument_type;
	static inline constexpr const bool is_const= false;
	template<void (__fastcall Cls::*Fn)(ResT&) noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_static<Cls, ResT, no_except, Fn, CFG> result;
	};
};
template<class Cls, class ResT, class A2, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)(ResT&, A2 const&) const noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void(ResT&, A2 const&) noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef ResT argument_type;
	typedef A2 second_argument_type;
	static inline constexpr const bool is_const= true;
	template<void (__fastcall Cls::*Fn)(ResT&, A2 const&) const noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_static_cfg_const<Cls, ResT, A2, no_except, Fn, CFG> result;
	};
};
template<class Cls, class ResT, class A2, class CFG, bool NoExcept>
struct crack_process_fn_traits<void (__fastcall Cls::*)(ResT&, A2 const&) noexcept(NoExcept), CFG> {
	static inline constexpr const bool no_except= NoExcept;
	typedef boost::function_traits<void(ResT&, A2 const&) noexcept(no_except)> process_fn_type;
	typedef Cls class_type;
	typedef ResT argument_type;
	typedef A2 second_argument_type;
	static inline constexpr const bool is_const= false;
	template<void (__fastcall Cls::*Fn)(ResT&, A2 const&) noexcept(no_except)>
	struct queue_item {
		typedef closure::closure_static_cfg<Cls, ResT, A2, no_except, Fn, CFG> result;
	};
};
template<class IsMemFn, class PFP, class Wk, class CFG>
struct choose_process_fn {
	typedef crack_process_fn_traits<PFP, CFG> type;
	typedef typename type::class_type class_type;
	typedef typename type::argument_type result_type;
	BOOST_MPL_ASSERT((std::is_same<class_type, typename std::remove_const<typename std::remove_reference<Wk>::type>::type>));
	typedef PFP process_fn_ptr;
	static inline constexpr const bool is_const= type::is_const;
	static inline constexpr const bool no_except= type::no_except;
};
template<class PFP, class Wk, class CFG>
struct choose_process_fn<std::false_type, PFP, Wk, CFG> {
	typedef Wk class_type;
	typedef typename Wk::result_type result_type;
	static inline constexpr const bool is_const= false;
	static inline constexpr const bool no_except= false;
};
template<class Wk, class FnType, class CFG>
class get_process_fn_traits {
private:
	using process_fn_ptr= FnType;
	using has_process_fn= typename std::is_member_function_pointer<process_fn_ptr>::type;
	using got_result_type= choose_process_fn<has_process_fn, process_fn_ptr, Wk, CFG>;

public:
	using type= got_result_type;
};

template<class V>
struct noop {
	typedef V argument_type;
	typedef argument_type result_type;

	constexpr result_type operator()(argument_type const& a) const noexcept(true) {
		return a;
	}
};

/// This library-internal class just creates a thread_wk_t for the user's closure_base-derived closure.
/**
	Note: one cannot give "exception-specifications" to non-joinable work, as you can't call get_results() on it, because the non-joinable transfer doesn't return an execution_context. (A constructor isn't used because you can't return a value from the constructor, and a member-template isn't used because of MSVC++ v12.00.8168 compiler limitations.)

	\param	InpWk	The closure_base-derived closure should contain a non-overloaded function called process() or process(result_type &), possibly const-qualified.

	\see thread_wk_t
*/
template<class P, class InpWk, class FnType, FnType FnPtr>
struct create_direct {
	using pool_traits_type= P;
	using os_traits= typename pool_traits_type::os_traits;
	using cfg_type= typename pool_traits_type::cfg_type;
	/**
		\todo It would be good to make use of the is_const member to determine if the mutation is pure. If so then all sorts of nice things could be done like memoization of the result of the mutation, i.e. place this object type into a hash_map at compile-time, and place the result into there too, and for later mutations of the same object instance to short-cut needing to do all the using a thread to generate the result malarky, as long as we save the object instance state sufficiently.
	*/
	using process_fn_traits= typename get_process_fn_traits<InpWk, FnType, cfg_type>::type;
	using result_type= typename process_fn_traits::result_type;
	using process_fn_ptr= typename process_fn_traits::process_fn_ptr;
	using closure_t= typename process_fn_traits::type::template queue_item<FnPtr>::result;
};

}}}}

#endif
