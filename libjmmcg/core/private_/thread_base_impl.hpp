/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

// Implementation details..... Don't look below here!!! ;)

//	"thread_base" implementation...

template<generic_traits::api_type::element_type API, typename Mdl>
inline bool
thread_base<API, Mdl>::is_running() const noexcept(true) {
	return thread_params.state.load() == thread_traits::api_params_type::states::active;
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline const typename thread_base<API, Mdl>::thread_traits::api_params_type::states::element_type
thread_base<API, Mdl>::state_nolk(typename thread_traits::api_params_type const& params) noexcept(true) {
	if(params.state.load() != thread_traits::api_params_type::states::no_kernel_thread) {
		const typename thread_traits::api_params_type::states::element_type current_state= params.state.load();
		if(current_state == thread_traits::api_params_type::states::active) {
			return params.state.load() == thread_traits::api_params_type::states::active ? current_state : thread_traits::api_params_type::states::suspended;
		} else {
			return current_state;
		}
	} else {
		return thread_traits::api_params_type::states::no_kernel_thread;
	}
}

/* TODO JMG: can't generically suspend, because how do I suspend sequential traits?
	template<generic_traits::api_type::element_type API, typename Mdl> inline const typename thread_base<API, Mdl>::thread_traits::api_params_type::suspend_count
	thread_base<API, Mdl>::suspend() noexcept(true) {
		if (this->state()<thread_traits::api_params_type::no_kernel_thread) {
			thread_params_lock.lock();
			// Critical sections don't like double-unlocking....
			bool locked=true;
			if (thread_traits::get_current_thread_id()==thread_params.id) {
				// We want to suspend ourselves - better release the lock, otherwise we'll get deadlocks.
				thread_params_lock.unlock();
				locked=false;
			}
			const typename thread_traits::api_params_type::suspend_count sus_ct=thread_traits::suspend(thread_params);
			if (locked) {
				thread_params_lock.unlock();
			}
			return sus_ct;
		} else {
			return 0;
		}
	}
*/
template<generic_traits::api_type::element_type API, typename Mdl>
inline const typename thread_base<API, Mdl>::thread_traits::api_params_type::suspend_count
thread_base<API, Mdl>::resume() noexcept(true) {
	if(this->state() < thread_traits::api_params_type::no_kernel_thread) {
		const typename lock_traits::critical_section_type::write_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
		return thread_traits::resume(thread_params);
	} else {
		return 0;
	}
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::create_running() noexcept(false) {
	{
		const typename lock_traits::critical_section_type::write_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
		if(state_nolk(thread_params) >= thread_traits::api_params_type::states::no_kernel_thread) {
			if(!thread_traits::create(thread_traits::api_params_type::create_running, thread_params)) {
				DEBUG_ASSERT(!"Failed to create thread!");
				BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Failed to create and start the thread.", &thread_base<API, Mdl>::create_running));
			} else {
				// Although a kernel thread may not yet have been created, pretend one has to ensure that an immediate call to is_running() returns true, not false, whilst we await the creation of the kernel thread.
				thread_params.state= thread_traits::api_params_type::states::activating;
			}
		}
	}
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline const typename thread_base<API, Mdl>::thread_traits::api_params_type::states::element_type
thread_base<API, Mdl>::state() const noexcept(true) {
	const typename lock_traits::critical_section_type::write_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	return state_nolk(thread_params);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline
	typename thread_base<API, Mdl>::thread_traits::api_params_type::priority_type __fastcall thread_base<API, Mdl>::kernel_priority() const noexcept(false) {
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	return thread_traits::get_kernel_priority(thread_params.id);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::kernel_priority(const typename thread_traits::api_params_type::priority_type priority) noexcept(false) {
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	thread_traits::set_kernel_priority(thread_params.id, priority);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline
	typename thread_base<API, Mdl>::thread_traits::api_params_type::processor_mask_type
	thread_base<API, Mdl>::kernel_affinity() const noexcept(false) {
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	return thread_traits::get_kernel_affinity(thread_params.id);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::kernel_affinity(const typename thread_traits::api_params_type::processor_mask_type& mask) noexcept(false) {
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	thread_traits::set_kernel_affinity(thread_params.id, mask);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::set_name(typename thread_traits::thread_name_t const& name) noexcept(false) {
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	thread_traits::set_name(thread_params.id, name);
}

template<generic_traits::api_type::element_type API, typename Mdl>
template<std::size_t NameSize>
	requires(NameSize <= thread_base<API, Mdl>::thread_traits::max_thread_name_length)
inline void
thread_base<API, Mdl>::set_name(char const (&name)[NameSize]) noexcept(false) {
	typename thread_traits::thread_name_t name_internal{};
	std::copy_n(name, NameSize, name_internal.begin());
	//		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	thread_traits::set_name(thread_params.id, name_internal);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline typename thread_base<API, Mdl>::thread_traits::thread_name_t
thread_base<API, Mdl>::get_name() const noexcept(false) {
	// TODO		const typename lock_traits::critical_section_type::read_lock_type lock(thread_params_lock, lock_traits::infinite_timeout());
	return thread_traits::get_name(thread_params.id);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline const tstring
thread_base<API, Mdl>::to_string() const noexcept(false) {
	std::ostringstream str;
	str << boost::core::demangle(typeid(*this).name())
		 << "\nException thrown: " << exception_thrown_in_thread
		 << "\nParams: " << thread_params.to_string()
		 << "\nExit wait period=" << exit_wait_period << _T("ms");
	return str.str();
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline thread_base<API, Mdl>::thread_base(const typename thread_traits::api_params_type::suspend_period_ms exit_wait_p) noexcept(false)
	: exit_wait_period(exit_wait_p), thread_params(core_work_fn, nullptr, thread_traits::api_params_type::max_stack_size) {
	// No need to lock here - creating a new object.
	thread_params.arglist= static_cast<typename thread_traits::api_params_type::arglist_type>(this);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline thread_base<API, Mdl>::thread_base(const thread_base& tb) noexcept(true)
	: exit_wait_period(tb.exit_wait_period), exception_thrown_in_thread(tb.exception_thrown_in_thread), thread_params(tb.thread_params) {
	// No need to lock here - creating a new object.
	thread_params.arglist= static_cast<typename thread_traits::api_params_type::arglist_type>(this);
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline thread_base<API, Mdl>::~thread_base() noexcept(false) {
	// Despite that, what the hell - throw this and be damned...
	if(state_nolk(thread_params) == thread_traits::api_params_type::states::null_this_pointer) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("The thread registered an abnormal exit state (a NULL 'this' pointer). state={}", thread_traits::api_params_type::states::to_string(this->state())), this));
	}
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::wait_thread_exit() noexcept(false) {
	switch(thread_params.state) {
	case thread_traits::api_params_type::states::no_kernel_thread:
	case thread_traits::api_params_type::states::exiting:
		break;
	default:
		if(state_nolk(thread_params) == thread_traits::api_params_type::states::suspended) {
			thread_traits::resume(thread_params);
		}
		thread_traits::wait_thread_exit(thread_params, exit_wait_period);
		if(state_nolk(thread_params) == thread_traits::api_params_type::states::active || state_nolk(thread_params) == thread_traits::api_params_type::states::suspended) {
			// O.k. we tried to let it die naturally. No more Mr.Nice Guy...
			LIBJMMCG_LOG_TRIVIAL(trace) << boost::source_location() << " Terminating thread: " << *this;
			thread_traits::terminate(thread_params.id);
		}
		thread_traits::cleanup(thread_params.id);
		DEBUG_ASSERT(state_nolk(thread_params) >= thread_traits::api_params_type::states::no_kernel_thread);
		thread_params.id= 0;
		break;
	}
}

template<generic_traits::api_type::element_type API, typename Mdl>
inline void
thread_base<API, Mdl>::request_exit() const noexcept(false) {
	if(thread_params.id) {
		thread_traits::terminate(thread_params.id);
	}
}

/* Not used, just here as a template for code you might need...
	template<generic_traits::api_type::element_type API, typename Mdl> typename thread_base<API, Mdl>::thread_traits::api_params_type::core_work_fn_ret_t
	thread_base<API, Mdl>::core_work_fn(thread_traits::api_params_type::core_work_fn_arg_t ptr) noexcept(false) {
		DEBUG_ASSERT(ptr);
		thread_base<API, Mdl> * const pthis=reinterpret_cast<thread_base<API, Mdl> *>(ptr);
		DEBUG_ASSERT(pthis);
		if (pthis) {
			try {
				const api_threading_traits<API, Mdl>::api_params_type::states ret=pthis->process();
				pthread_params.id=0;
				return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(ret);
			} catch (abi::__forced_unwind &) {
				throw;
			} catch (...) {
				DEBUG_ASSERT(!"Unknown exception caught.");
				pthis->exception_thrown_in_thread.set(std::current_exception());
			}
			return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<API, Mdl>::api_params_type::unknown_exception);
		}
		// In reality, no errors are actively reported to the user for this case, as the pthis was null.
		// So we almost silently fail, and hope that they aren't sooooo dumb as to not notice that their work isn't being processed...
		// This is because the only "opportunity" I have to report it is as an exception in the destructor. Hooo-boy would that be evil...
		// Just think of the memory leaks.... So I don't do that. That's the breaks....
		return reinterpret_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<API, Mdl>::api_params_type::null_this_pointer);
	}
*/

}}}}
