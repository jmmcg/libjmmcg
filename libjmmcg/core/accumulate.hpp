#ifndef LIBJMMCG_CORE_ACCUMULATE_HPP
#define LIBJMMCG_CORE_ACCUMULATE_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <cstdint>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace private_ {

template<std::size_t I>
	requires (I>=1)
struct accumulate_impl {
	template<std::size_t N, class V, class Op>
		requires (N>I)
	static auto
	result(std::array<V, N> const &arr, Op op) {
		return op(arr[I], accumulate_impl<I-1>::result(arr, op));
	}
};
template<>
struct accumulate_impl<1u> {
	template<std::size_t N, class V, class Op>
		requires (N>=2)
	static auto
	result(std::array<V, N> const &arr, Op op) {
		return op(arr[1], arr[0]);
	}
};

}

template<std::size_t N, class V, class Op>
	requires (N>=2)
inline auto
accumulate(std::array<V, N> const &arr, Op op) {
	return private_::accumulate_impl<N-1>::result(arr, op);
}

} }

#endif
