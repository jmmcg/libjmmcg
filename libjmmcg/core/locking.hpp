#ifndef LIBJMMCG_CORE_LOCKING_HPP
#define LIBJMMCG_CORE_LOCKING_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_params_traits.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/static_assert.hpp>
#include <boost/thread/locks.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock {

template<class LkT>
struct lockable {
	using lock_traits= LkT;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;

	virtual atomic_state_type __fastcall lock() noexcept(false)= 0;
	virtual atomic_state_type __fastcall lock(const timeout_type) noexcept(false)= 0;
	virtual atomic_state_type __fastcall try_lock() noexcept(true);
	virtual atomic_state_type __fastcall unlock() noexcept(true)= 0;
};

template<class LkT>
struct settable {
	using lock_traits= LkT;
	typedef typename lock_traits::atomic_state_type atomic_state_type;

	virtual atomic_state_type __fastcall set() noexcept(true)= 0;
	virtual atomic_state_type __fastcall reset() noexcept(true)= 0;
};

template<class LkT>
struct lockable_settable : public lockable<LkT>, public settable<LkT> {
	using lock_traits= LkT;
	typedef typename lock_traits::atomic_state_type atomic_state_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;
};

/// An adaptor to turn ppd::lockable in boost::lockable objects.
/**
	Mainly this required because boost assumes (naughtily, in my opinion) the calling convention, whereas libjmmcg specifies a calling convention, which is particularly important on Windows as on that platform the default calling convention is ... erm ... variable, at best it is good, but on old compilers, technically speaking, utter pants.
*/
template<class Lk>
class boost_lk_compat {
public:
	typedef Lk atomic_t;	  ///< The type of the lock to be adapted.
	typedef typename atomic_t::lock_traits lock_traits;

private:
	atomic_t& lk;

public:
	[[nodiscard]] explicit __stdcall boost_lk_compat(atomic_t& l) noexcept(true);
	boost_lk_compat(boost_lk_compat const&)= delete;

	bool try_lock() noexcept(noexcept(lk.try_lock()));
	void lock() noexcept(noexcept(lk.lock()));
	void unlock() noexcept(noexcept(lk.unlock()));
};

template<class LockObject>
class scope_lock : public lockable<typename LockObject::lock_traits> {
public:
	typedef lockable<typename LockObject::lock_traits> base_t;
	typedef LockObject atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;
	typedef atomic_state_type lock_result_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

protected:
	[[nodiscard]] explicit __stdcall scope_lock(atomic_t& lo) noexcept(true);
	[[nodiscard]] __stdcall scope_lock(atomic_t& lo, const timeout_type period) noexcept(true);
	scope_lock(scope_lock const&)= delete;
	virtual __stdcall ~scope_lock() noexcept(true);

	lock_result_type __fastcall try_lock() noexcept(true) override;
	lock_result_type __fastcall lock() noexcept(false) override;
	lock_result_type __fastcall lock(const timeout_type period) noexcept(false) override;

	lock_result_type __fastcall unlock() noexcept(true) override;
	void decay() noexcept(true);

protected:
	atomic_t& locker;
};

/// A class to control access to resource, possibly using the stack, using the RAII idiom.
/**
	Use this lock for multiple threads in the same process space.
	Class to manipulate an in-process lock on the stack, with locking on
	construction, release on destruction.
	(The "LockObject &" will be valid.)
*/
template<class LockObject>
class in_process : public scope_lock<LockObject> {
public:
	typedef scope_lock<LockObject> base_t;
	typedef typename base_t::atomic_t atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;
	typedef atomic_state_type lock_result_type;

	/// Get control of the lock for the resource.
	/**
		Note that this may lock the resource.

		\param lo	The lock for the resource.
		\param period	The period that should be waited for attempting to gain the lock on the resource.

		\see infinite_timeout()
	*/
	[[nodiscard]] explicit __stdcall in_process(atomic_t& lo, const timeout_type period= lock_traits::infinite_timeout()) noexcept(true);
	/// Release the lock on the resource.
	__stdcall ~in_process() noexcept(true);

	void decay() noexcept(true);

protected:
	lock_result_type __fastcall try_lock() noexcept(true) override;
	lock_result_type __fastcall lock() noexcept(false) override;
	/**
		\param period	The period that should be waited for attempting to gain the lock on the resource.

		\see infinite_timeout()
	*/
	lock_result_type __fastcall lock(const timeout_type period) noexcept(false) override;

	lock_result_type __fastcall unlock() noexcept(true) override;
};

/// A class to control access to resource, possibly using the stack, using the RAII idiom.
/**
 *		Use this lock for multiple threads in the same process space.
 *		Class to manipulate an in-process lock on the stack, with locking on
 *		construction, release on destruction.
 *		(The "LockObject &" will be valid.)
 */
template<class LockObject>
class in_process_unlockable : public lockable<typename LockObject::lock_traits> {
public:
	typedef lockable<typename LockObject::lock_traits> base_t;
	typedef LockObject atomic_t;
	typedef typename atomic_t::lock_traits lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::timeout_type timeout_type;
	typedef atomic_state_type lock_result_type;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= atomic_t::memory_access_mode;

	[[nodiscard]] explicit __stdcall in_process_unlockable(atomic_t& lo) noexcept(true);
	/// Get control of the lock for the resource.
	/**
	 *			Note that this may lock the resource.
	 *
	 *			\param lo	The lock for the resource.
	 *			\param period	The period that should be waited for attempting to gain the lock on the resource.
	 *
	 *			\see infinite_timeout()
	 */
	[[nodiscard]] __stdcall in_process_unlockable(atomic_t& lo, const timeout_type period) noexcept(true);
	in_process_unlockable(in_process_unlockable const&)= delete;
	/// Release the lock on the resource.
	virtual __stdcall ~in_process_unlockable() noexcept(true);

	lock_result_type __fastcall try_lock() noexcept(true) override;
	lock_result_type __fastcall unlock() noexcept(true) override;
	void decay() noexcept(true);

private:
	atomic_t& locker;
	std::atomic_flag locked{};

	lock_result_type __fastcall lock() noexcept(true) override {
		return lock_traits::atom_set;
	}
	lock_result_type __fastcall lock(const timeout_type) noexcept(true) override {
		return lock_traits::atom_set;
	}
};

template<
	class St,	///< The various states that can be signalled.
	St UnSig,	///< The initial, unsignalled state.
	St Pri,	 ///< A single state that takes priority over all other states, so that if set, it will always be returned.
	typename M,
	typename WL= typename M::write_lock_type,
	typename Mtx= M>
class new_event_signal {
public:
	using lock_traits= typename M::lock_traits;
	typedef typename lock_traits::atomic_state_type atomic_state_type;
	typedef typename lock_traits::anon_semaphore_type anon_semaphore_type;
	typedef typename anon_semaphore_type::count_type count_type;
	typedef Mtx locker_type;	///< The underlying lock object to use that will be locked in some (EREW or CREW or other) manner.
	typedef typename locker_type::write_lock_type write_lock_type;	  ///< The type of write-lock to use. This allows the possibility of using a read-write lock.
	typedef typename locker_type::read_lock_type read_lock_type;	///< The type of read lock to use, by default the write lock. This allows the possibility of using a read-write lock.
	typedef anon_semaphore_type atomic_t;
	typedef St states;
	typedef typename std::pair<states, atomic_state_type> lock_result_type;
	static inline constexpr const states unsignalled= UnSig;
	static inline constexpr const states priority= Pri;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= (atomic_t::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																				  && locker_type::memory_access_mode == generic_traits::memory_access_modes::crew_memory_access
																																			  ? generic_traits::memory_access_modes::crew_memory_access
																																			  : generic_traits::memory_access_modes::erew_memory_access);

private:
	atomic_t semaphore;

public:
	BOOST_MPL_ASSERT((std::is_same<typename write_lock_type::atomic_t, locker_type>));

	[[nodiscard]] new_event_signal() noexcept(noexcept(atomic_t(lock_traits::atom_unset)))
		: semaphore(lock_traits::atom_unset), state_(unsignalled) {
	}
	new_event_signal(new_event_signal const&)= delete;

	/**
		This locker() function is used in the thread_safe_adaptors collections to save on excess locking within those containers.

		\see safe_colln
		\see queue
		\see funky_queue
	*/
	locker_type& __fastcall locker() noexcept(true) {
		return lock_;
	}

	atomic_state_type __fastcall set_nolk(states const s) noexcept(noexcept(semaphore.set())) {
		if(state_ != priority) {
			state_= s;
		}
		return semaphore.set();
	}
	atomic_state_type __fastcall set(states const s) noexcept(false) {
		const write_lock_type lk(lock_, locker_type::lock_traits::infinite_timeout());
		return set_nolk(s);
	}
	atomic_state_type __fastcall reset() noexcept(true) {
		const write_lock_type lk(lock_, locker_type::lock_traits::infinite_timeout());
		const atomic_state_type ret= semaphore.reset();
		if(ret == lock_traits::atom_unset) {
			state_= unsignalled;
		}
		return ret;
	}
	void clear() noexcept(noexcept(semaphore.try_lock())) {
		while(semaphore.try_lock() == lock_traits::atom_set)
			;
		state_= unsignalled;
	}

	lock_result_type __fastcall lock() noexcept(noexcept(semaphore.lock())) {
		const atomic_state_type l= semaphore.lock();
		return lock_result_type(state_, l);
	}
	lock_result_type __fastcall lock(const typename lock_traits::timeout_type t) noexcept(noexcept(semaphore.lock(t))) {
		const atomic_state_type l= semaphore.lock(t);
		return lock_result_type(state_, l);
	}
	lock_result_type __fastcall try_lock() noexcept(noexcept(semaphore.try_lock())) {
		const atomic_state_type l= semaphore.try_lock();
		return lock_result_type(state_, l);
	}
	lock_result_type __fastcall unlock() noexcept(noexcept(semaphore.unlock())) {
		const atomic_state_type l= semaphore.unlock();
		return lock_result_type(state_, l);
	}
	count_type __fastcall count() const noexcept(noexcept(semaphore.count())) {
		return semaphore.count();
	}

private:
	states state_;
	locker_type lock_;
};

/// This namespace is for containing lock types that lock two lockable objects jointly, atomically.
namespace any_order {

/// Lock all of the lockable objects, and wait until they are both locked.
/**
	\todo JMG: implement a specialisation for Win32 API using "WaitForMultipleObjects()".

	\see boost::lock()
*/
template<
	generic_traits::api_type::element_type API_,	  ///< Allow specialisation for the various APIs that might support alternative implementations.
	typename Mdl_,
	class Lk1,
	class Lk2>
class all {
public:
	typedef Lk1 first_argument_type;
	typedef Lk2 second_argument_type;
	typedef first_argument_type atomic1_t;
	typedef second_argument_type atomic2_t;

	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == atomic2_t::lock_traits::api_type);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, typename atomic2_t::lock_traits::model_type>));
	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == API_);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, Mdl_>));

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (atomic1_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && atomic2_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	[[nodiscard]] __stdcall all(first_argument_type& l1, second_argument_type& l2) noexcept(noexcept(boost::lock(std::declval<first_argument_type>(), std::declval<second_argument_type>())));
	all(all const&)= delete;
	__stdcall ~all() noexcept(true);

private:
	lock::boost_lk_compat<first_argument_type> lk1;
	lock::boost_lk_compat<second_argument_type> lk2;
};
template<
	generic_traits::api_type::element_type API_,
	class Lk1,
	class Lk2>
class all<API_, sequential_mode, Lk1, Lk2> {
public:
	typedef Lk1 first_argument_type;
	typedef Lk2 second_argument_type;
	typedef first_argument_type atomic1_t;
	typedef second_argument_type atomic2_t;

	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == atomic2_t::lock_traits::api_type);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, typename atomic2_t::lock_traits::model_type>));
	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == API_);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, sequential_mode>));

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::erew_memory_access;

	[[nodiscard]] constexpr all(first_argument_type&, second_argument_type&) noexcept(true) {}
	all(all const&)= delete;
};

/// Try to lock at least one of the lockable objects.
/**
	\todo JMG: implement a specialisation for Win32 API using "WaitForMultipleObjects()".

	\see boost::try_lock()
*/
template<
	generic_traits::api_type::element_type API_,	  ///< Allow specialisation for the various APIs that might support alternative implementations.
	typename Mdl_,
	class Lk1,
	class Lk2>
class try_one {
public:
	typedef Lk1 first_argument_type;
	typedef Lk2 second_argument_type;
	typedef first_argument_type atomic1_t;
	typedef second_argument_type atomic2_t;
	typedef int result_type;

	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == atomic2_t::lock_traits::api_type);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, typename atomic2_t::lock_traits::model_type>));
	BOOST_STATIC_ASSERT(atomic1_t::lock_traits::api_type == API_);
	BOOST_MPL_ASSERT((std::is_same<typename atomic1_t::lock_traits::model_type, Mdl_>));

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (atomic1_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && atomic2_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

public:
	[[nodiscard]] __stdcall try_one(Lk1& l1, Lk2& l2) noexcept(true);
	try_one(try_one const&)= delete;

	result_type __fastcall try_lock() noexcept(noexcept(boost::try_lock(std::declval<Lk1>(), std::declval<Lk2>())));

private:
	lock::boost_lk_compat<Lk1> lk1;
	lock::boost_lk_compat<Lk2> lk2;
};

}

}}}}

#include "anon_spin_event.hpp"
#include "locking_impl.hpp"

#endif
