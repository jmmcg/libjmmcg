#ifndef LIBJMMCG_CORE_FUNCTION_HPP
#define LIBJMMCG_CORE_FUNCTION_HPP

/******************************************************************************
** Copyright © 2025 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <array>
#include <cstddef>
#include <functional>
#include <span>
#include <type_traits>
#include <utility>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace private_ {

template<class Invokable>
struct get_ret_args_types;

template<class Invokable, class Ret, class... Params>
struct get_ret_args_types<Ret (Invokable::*)(Params...) const> {
	using return_type= Ret;
	using args_type= std::tuple<Params...>;
};

template<class Functor>
struct get_ret_args_types_functor {
	using element_type= Functor;
	using functor_type= decltype(&element_type::operator());
	using types= get_ret_args_types<functor_type>;
};

template<
	class Ret,
	class... Params>
struct invokable_details final {
	struct invoke_wrapper_base {
		using return_type= Ret;
		using args_type= std::tuple<Params...>;

		virtual ~invoke_wrapper_base()= default;

		virtual void clone(std::span<std::byte> buffer) const= 0;
		virtual void move(std::span<std::byte> buffer)= 0;

		virtual return_type operator()(Params&&...) const= 0;
		virtual return_type operator()(Params&&...)= 0;
	};

	/**
	 * EBO does not occur, so this should really add 1 to it, so the user does not have to for such obscure reasons.
	 * This is not done because the sizeof([](){...})==1 curiously and confusingly. Then what about std::function? A general functor? When may or may not EBO occur? So I gave up and left it to the user to "magically" get it right.
	 */
	static inline constexpr std::size_t max_size= sizeof(invoke_wrapper_base) + alignof(invoke_wrapper_base);
};

}

/// A drop-in replacement for std::function that emplaces the invokable into a pre-allocated buffer.
/**
 * \see std::function
 */
template<
	class Invokable,	 ///< The invokable operation to be performed.
	std::size_t Size	 ///< The size of the buffer into which the operation shall be stored.
	>
class function;

template<
	class Ret,
	class... Params,
	std::size_t Size>
class function<Ret(Params...), Size> final {
public:
	static inline constexpr std::size_t max_size= Size;

private:
	template<class Invokable, std::size_t Size1>
	friend class function;

	using invoke_wrapper_base= typename private_::invokable_details<Ret, Params...>::invoke_wrapper_base;

	template<class Invokable, class InvokableDetails= typename private_::get_ret_args_types_functor<Invokable>::types>
		requires((sizeof(Invokable) <= max_size)
					&& (std::is_same_v<typename invoke_wrapper_base::return_type, typename InvokableDetails::return_type>
						 || std::is_base_of_v<typename invoke_wrapper_base::return_type, typename InvokableDetails::return_type>)
					&& std::is_same_v<typename invoke_wrapper_base::args_type, typename InvokableDetails::args_type>)
	class invoke_wrapper_t final : public invoke_wrapper_base {
	public:
		using element_type= Invokable;

		[[nodiscard]] explicit invoke_wrapper_t(element_type inv) noexcept(noexcept(element_type(std::declval<element_type>())))
			: invokable_(std::move(inv)) {
		}

		invoke_wrapper_t(invoke_wrapper_t const& inv) noexcept(noexcept(element_type(std::declval<element_type>())))
			: invokable_(inv.invokable_) {
		}

		invoke_wrapper_t(invoke_wrapper_t&& inv) noexcept(noexcept(element_type(std::declval<element_type>())))
			: invokable_(std::move(inv.invokable_)) {
		}

		~invoke_wrapper_t() final= default;

		void clone(std::span<std::byte> buffer) const noexcept(noexcept(element_type(std::declval<element_type>()))) final {
			DEBUG_ASSERT(sizeof(invoke_wrapper_t) <= buffer.size());
			::new(buffer.data()) invoke_wrapper_t(*this);
		}

		void move(std::span<std::byte> buffer) noexcept(noexcept(element_type(std::move(std::declval<element_type>())))) final {
			DEBUG_ASSERT(sizeof(invoke_wrapper_t) <= buffer.size());
			::new(buffer.data()) invoke_wrapper_t(std::move(*this));
		}

		typename invoke_wrapper_base::return_type operator()(Params&&... args) const noexcept(noexcept(std::declval<element_type>().operator()(std::forward<Params>(std::declval<Params>())...))) final {
			return invokable_(std::forward<Params>(args)...);
		}

		typename invoke_wrapper_base::return_type operator()(Params&&... args) noexcept(noexcept(std::declval<element_type>().operator()(std::forward<Params>(std::declval<Params>())...))) final {
			return invokable_(std::forward<Params>(args)...);
		}

	private:
		element_type invokable_;
	};

public:
	static inline constexpr std::size_t capacity= max_size + private_::invokable_details<Ret, Params...>::max_size;

	using element_type= invoke_wrapper_base;
	using container_type= std::array<std::byte, capacity>;

	template<class Invokable>
		requires(!std::is_same_v<Invokable, function>
					&& (sizeof(Invokable) <= max_size))
	[[nodiscard]] explicit function(Invokable inv) noexcept(noexcept(invoke_wrapper_t<Invokable>(std::declval<Invokable>()))) {
		::new(buffer_.data()) invoke_wrapper_t<Invokable>(std::forward<Invokable>(inv));
		DEBUG_ASSERT(!empty());
	}

	template<std::size_t Size1>
		requires(Size1 < max_size)
	[[nodiscard]] function(function<Ret(Params...), Size1> const& f) noexcept(noexcept(std::declval<invoke_wrapper_base const*>()->clone(std::span<typename container_type::value_type>()))) {
		reinterpret_cast<invoke_wrapper_base const*>(f.buffer_.data())->clone(std::span(buffer_));
		DEBUG_ASSERT(!empty());
	}

	function(function const& f) noexcept(noexcept(std::declval<invoke_wrapper_base const*>()->clone(std::span<typename container_type::value_type>()))) {
		reinterpret_cast<invoke_wrapper_base const*>(f.buffer_.data())->clone(std::span(buffer_));
		DEBUG_ASSERT(!empty());
	}

	function(function&& f) noexcept(noexcept(std::declval<invoke_wrapper_base*>()->move(std::span<typename container_type::value_type>()))) {
		reinterpret_cast<invoke_wrapper_base*>(f.buffer_.data())->move(std::span(buffer_));
		DEBUG_ASSERT(!empty());
	}

	~function() noexcept(noexcept(std::declval<element_type>().~element_type())) {
		if(!empty()) [[likely]] {
			reinterpret_cast<element_type*>(buffer_.data())->~element_type();
			buffer_= container_type{};
		}
	}

	typename invoke_wrapper_base::return_type operator()(Params&&... args) const noexcept(noexcept(std::declval<element_type>().operator()(std::forward<Params>(std::declval<Params>())...))) {
		DEBUG_ASSERT(!empty());
		return reinterpret_cast<element_type const*>(buffer_.data())->operator()(std::forward<Params>(args)...);
	}

	typename invoke_wrapper_base::return_type operator()(Params&&... args) noexcept(noexcept(std::declval<element_type>().operator()(std::forward<Params>(std::declval<Params>())...))) {
		DEBUG_ASSERT(!empty());
		return reinterpret_cast<element_type*>(buffer_.data())->operator()(std::forward<Params>(args)...);
	}

	/**
	 * Algorithmic complexity:  O(1)
	 *
	 * \note
	 * -#  Note that this hash is based upon speed. It uses the address of the current object. Thus a dangling pointer may return a hash that compares equal. This would be undefined behaviour, of course.
	 * -# This also means that the hash of each instance will be unequal, thus if a hash is saved, then restored, it would be invalid, i.e. would be unlikely to compare equal to a dynamically-generated hash, returned via calling this function.
	 * -# The returned hash would not compare equal across process spaces, thus should only be used when allocated in suitably-mapped shared-memory, e.g. libjmmcg::shared_mem.
	 *
	 * @return A guaranteed perfect hash.
	 *
	 * \see std::hash
	 */
	[[nodiscard]] std::size_t hash() const noexcept(true) {
		return reinterpret_cast<std::size_t>(buffer_.begin());
	}

private:
	alignas(invoke_wrapper_base) container_type buffer_{};

	[[nodiscard]] bool empty() const noexcept(true) {
		union {
			void const* ptr= nullptr;
			std::array<std::byte, sizeof(void*)> buff_;
		} conv;

		std::copy_n(buffer_.begin(), std::min(buffer_.size(), conv.buff_.size()), conv.buff_.begin());
		return conv.ptr == nullptr;
	}
};

namespace private_ {

template<class Ret, class ParamsAsTuple, std::size_t Size>
struct make_function;

template<class Ret, class... Params, std::size_t Size>
struct make_function<Ret, std::tuple<Params...>, Size> {
	using type= function<Ret(Params...), Size>;
};

}

template<class Lambda>
struct make_function_t {
	using types= typename private_::get_ret_args_types_functor<Lambda>::types;
	using return_type= typename types::return_type;
	using args_type= typename types::args_type;
	using type= typename private_::make_function<return_type, args_type, sizeof(Lambda)>::type;
};

/// Create a libjmmcg::function object, deducing the correct buffer size.
/**
 * \see libjmmcg::function
 */
template<class Lambda>
[[nodiscard]] inline auto
make_function(Lambda op) noexcept(noexcept(typename make_function_t<Lambda>::type(std::declval<Lambda>()))) {
	using function_type= typename make_function_t<Lambda>::type;
	return function_type(std::forward<Lambda>(op));
}

}}

/**
 * \see libjmmcg::function::hash(), std::hash
 */
template<class Invokable, std::size_t Size>
struct std::hash<jmmcg::LIBJMMCG_VER_NAMESPACE::function<Invokable, Size>> {
	std::size_t operator()(jmmcg::LIBJMMCG_VER_NAMESPACE::function<Invokable, Size> const& fn) const noexcept(true) {
		return fn.hash();
	}
};

#endif
