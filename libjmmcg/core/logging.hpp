#ifndef LIBJMMCG_CORE_LOGGING_HPP
#define LIBJMMCG_CORE_LOGGING_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace log {

using level_severity_type= boost::log::trivial::severity_level;

std::string
all_severities_to_string(std::string const& separator= "|");

/**
 * \see get_global_level()
 */
void
set_global_level(level_severity_type level) noexcept(true);
/**
 * \see set_global_level()
 */
level_severity_type
get_global_level() noexcept(true);

}}}

/// A drop-in replacement for BOOST_LOG_TRIVIAL that is faster, but less flexible.
/**
 * BOOST_LOG_TRIVIAL is bizarrely slow when testing the severity level, to ascertain if it should log or not. This is a simple wrapper that speeds it up a huge amount. Set the log-level with set_global_log_level().
 *
 * \see set_global_log_level(), BOOST_LOG_TRIVIAL
 */
#define LIBJMMCG_LOG_TRIVIAL(level)                                                                                                            \
	if((jmmcg::LIBJMMCG_VER_NAMESPACE::log::level_severity_type::level) >= jmmcg::LIBJMMCG_VER_NAMESPACE::log::get_global_level()) [[unlikely]] \
	BOOST_LOG_TRIVIAL(level)

#endif
