/******************************************************************************
** Copyright © 2024 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {
namespace ppd {

template<class Invokable, generic_traits::api_type::element_type API, typename Mdl>
	requires(std::is_invocable_v<Invokable>)
template<class ErrHandler>
	requires(noexcept(ErrHandler()))
inline single_producer_single_consumer_queue<Invokable, API, Mdl>::single_producer_single_consumer_queue(ErrHandler&& err_handler)
	: err_handler_(std::move(err_handler)) {
}

template<class Invokable, generic_traits::api_type::element_type API, typename Mdl>
	requires(std::is_invocable_v<Invokable>)
inline single_producer_single_consumer_queue<Invokable, API, Mdl>::~single_producer_single_consumer_queue() {
	consumer_.request_stop();
	has_work_.test_and_set();
	has_work_.notify_all();
}

template<class Invokable, generic_traits::api_type::element_type API, typename Mdl>
	requires(std::is_invocable_v<Invokable>)
template<class Op>
	requires(std::is_invocable_v<Op>)
inline void
single_producer_single_consumer_queue<Invokable, API, Mdl>::emplace(Op&& op) noexcept(true) {
	auto queue= queue_to_fill_.load();
	DEBUG_ASSERT(queue != queues_.end());
	queue->emplace(std::move(op));
	has_work_.test_and_set();
	has_work_.notify_all();
}

template<class Invokable, generic_traits::api_type::element_type API, typename Mdl>
	requires(std::is_invocable_v<Invokable>)
inline void
single_producer_single_consumer_queue<Invokable, API, Mdl>::process(std::stop_token request_exit) noexcept(true) {
	auto process_items= [this](container_type& cont) noexcept(true) {
		try {
			while(!cont.empty()) {
				auto op= cont.front();
				cont.pop();
				op();
			}
		} catch(...) {
			err_handler_(std::current_exception());
		}
	};

	while(!request_exit.stop_requested()) [[likely]] {
		auto nexty= [this](typename queues_t::iterator i) noexcept(true) {
			auto n= std::next(i);
			return n != queues_.end() ? n : queues_.begin();
		};
		auto queue= queue_to_fill_.load();
		queue_to_fill_= nexty(queue);
		process_items(*queue);
		has_work_.wait(false);
		if(!request_exit.stop_requested()) [[likely]] {
			has_work_.clear();
		} else {
			return;
		}
	}
}

}
}
}
