#ifndef LIBJMMCG_CORE_INTRUSIVE_HPP
#define LIBJMMCG_CORE_INTRUSIVE_HPP

/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "shared_ptr.hpp"

#include <algorithm>
#include <iterator>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace intrusive {

namespace private_ {
template<class, class>
class slist_iterator_internal;
}

template<class, class>
class stack;
template<class, class>
class slist;

/// Singly-linked intrusive node details. Inherit from this to provide the linkage structure and object-lifetime semantics of the list, which are specifically shared_ptr semantics.
/**
	The use of the shared_ptr is very important: it solves the ABA problem that this implementation would otherwise suffer from.
*/
template<
	class LkT>
class node_details_itf : virtual public sp_counter_itf_type<long> {
public:
	using base_t= sp_counter_itf_type<long>;
	using lock_traits= LkT;
	using atomic_ctr_t= base_t;
	using atomic_ptr_t= typename lock_traits::template atomic<node_details_itf*>;

	atomic_ptr_t next;

	virtual ~node_details_itf() noexcept(true) {}

	virtual tstring
	to_string() const noexcept(false) {
		return "";
	}

protected:
	node_details_itf() noexcept(true)
		: next() {}
};

namespace private_ {

/// Hold the first node in the slist.
template<
	class LkT>
class node_details final : public node_details_itf<LkT>, public sp_counter_type<typename node_details_itf<LkT>::atomic_ctr_t::value_type, typename node_details_itf<LkT>::lock_traits> {
public:
	using base_t= node_details_itf<LkT>;
	using base2_t= sp_counter_type<typename base_t::atomic_ctr_t::value_type, typename base_t::lock_traits>;
	using atomic_ctr_t= typename base2_t::atomic_ctr_t;
	using lock_traits= typename base_t::lock_traits;

	constexpr node_details() noexcept(true)
		: base_t() {}
	~node_details() noexcept(true)= default;
	void operator=(node_details const&)= delete;
	void operator=(node_details&&)= delete;

	tstring
	to_string() const noexcept(false) override {
		return base2_t::sp_to_string();
	}
};

template<class V, class LkT>
struct end;

/**
	If you don't like the cut-down interface of this container, blame YAGNI & TDD.
*/
template<class V, class LkT>
class slist_iterator_internal {
public:
	using lock_traits= LkT;
	typedef std::forward_iterator_tag iterator_category;
	typedef std::ptrdiff_t difference_type;
	typedef shared_ptr<V, lock_traits> value_type;
	typedef shared_ptr<V, lock_traits> pointer;
	typedef const shared_ptr<V, lock_traits> const_pointer;
	typedef pointer& reference;
	typedef const_pointer& const_reference;

	typedef typename value_type::deleter_t deleter_t;
	typedef typename value_type::ctr_type ctr_type;

private:
	using node_ptr_t= shared_ptr<node_details_itf<lock_traits>, lock_traits>;

	BOOST_MPL_ASSERT((std::is_base_of<typename node_ptr_t::value_type, typename value_type::value_type>));

public:
	explicit __stdcall slist_iterator_internal(const_reference n) noexcept(true)
		: node(n) {
	}
	__stdcall slist_iterator_internal(slist_iterator_internal const& n) noexcept(true)
		: node(n.node) {
	}
	__stdcall slist_iterator_internal& operator=(slist_iterator_internal const& n) noexcept(true) {
		node= n.node;
		return *this;
	}

	bool __fastcall operator==(slist_iterator_internal const& n) const noexcept(true) {
		return node == n.node;
	}
	bool __fastcall operator!=(slist_iterator_internal const& n) const noexcept(true) {
		return !operator==(n);
	}
	bool __fastcall operator==(end<V, LkT> const& n) const noexcept(true);

	slist_iterator_internal& __fastcall operator++() noexcept(true) {
		node= node_ptr_t(node ? node->next : typename node_ptr_t::atomic_ptr_t());
		return *this;
	}
	[[nodiscard]] slist_iterator_internal __fastcall operator++(int) noexcept(true) {
		const slist_iterator_internal tmp(*this);
		++*this;
		return tmp;
	}
	pointer __fastcall operator*() noexcept(true) {
		return pointer(node);
	}
	const_pointer __fastcall operator*() const noexcept(true) {
		return const_pointer(node);
	}
	pointer __fastcall operator->() noexcept(true) {
		return pointer(node);
	}
	const_pointer __fastcall operator->() const noexcept(true) {
		return const_pointer(node);
	}

	friend tostream&
	operator<<(tostream& os, slist_iterator_internal const& i) {
		os << "node=" << i.node;
		return os;
	}

private:
	friend class stack<V, LkT>;
	friend class slist<V, LkT>;

	/**
		Note how node is one-before-the-node-we-want, which allows us to slist::erase() in constant time.
	*/
	node_ptr_t node;
};
template<class V, class LkT>
class slist_iterator_internal<V const, LkT> {
public:
	using lock_traits= LkT;
	typedef std::forward_iterator_tag iterator_category;
	typedef std::ptrdiff_t difference_type;
	typedef shared_ptr<V, lock_traits> value_type;
	typedef shared_ptr<V, lock_traits> pointer;
	typedef const shared_ptr<V, lock_traits> const_pointer;
	typedef pointer& reference;
	typedef const_pointer& const_reference;

	typedef typename value_type::deleter_t deleter_t;
	typedef typename value_type::ctr_type ctr_type;

private:
	using node_ptr_t= shared_ptr<node_details_itf<lock_traits>, lock_traits>;

	BOOST_MPL_ASSERT((std::is_base_of<typename node_ptr_t::value_type, typename value_type::value_type>));

public:
	explicit __stdcall slist_iterator_internal(const_reference n) noexcept(true)
		: node(n) {
	}
	explicit __stdcall slist_iterator_internal(node_ptr_t n) noexcept(true)
		: node(n) {
	}
	__stdcall slist_iterator_internal(slist_iterator_internal const& n) noexcept(true)
		: node(n.node) {
	}

	bool __fastcall operator==(slist_iterator_internal const& n) const noexcept(true) {
		return node == n.node;
	}
	bool __fastcall operator!=(slist_iterator_internal const& n) const noexcept(true) {
		return !operator==(n);
	}

	slist_iterator_internal& __fastcall operator++() noexcept(true) {
		node= node_ptr_t(node ? node->next : typename node_ptr_t::atomic_ptr_t());
		return *this;
	}
	[[nodiscard]] slist_iterator_internal __fastcall operator++(int) noexcept(true) {
		const slist_iterator_internal tmp(*this);
		++*this;
		return tmp;
	}
	const_pointer __fastcall operator*() const noexcept(true) {
		return const_pointer(node);
	}
	const_pointer __fastcall operator->() const noexcept(true) {
		return const_pointer(node);
	}

	friend tostream&
	operator<<(tostream& os, slist_iterator_internal const& i) {
		os << "node=" << i.node;
		return os;
	}

private:
	friend class stack<V, LkT>;
	friend class slist<V, LkT>;

	node_ptr_t node;
};

template<class V, class LkT>
struct end final : public slist_iterator_internal<V, LkT> {
	typedef slist_iterator_internal<V, LkT> base_t;
	typedef typename base_t::value_type value_type;

	constexpr __stdcall end()
		: base_t(value_type()) {
	}
	constexpr bool __fastcall operator==(end const&) const noexcept(true) {
		return true;
	}
	constexpr bool __fastcall operator!=(end const& n) const noexcept(true) {
		return !operator==(n);
	}
};

template<class V, class LkT>
inline bool
slist_iterator_internal<V, LkT>::operator==(end<V, LkT> const& n) const noexcept(true) {
	return node.get().get() ? node->next == n.node.get().get() : !node.get();
}

}

template<class V, class LkT>
class slist;

/// An implementation of a singly-linked, hybrid, intrusive, pointer-based stack, that depending upon the lock_traits chosen would be lock-free.
/**
	When inserting a node, no memory allocations are required, which is good in a multi-threading environment as it reduces calls to any memory allocator.
	If you don't like the interface of this container, blame YAGNI & TDD and the funky use of atomic operations.

	\see boost::stack, std::stack
*/
template<
	class V,	  ///< The type of the contained objects, which must be intrusive, and publicly inherit from node_details.
	class LkT	///< The lock_traits that providing the (potentially) atomic operations to be used upon pointers to the value_type.
	>
class stack {
public:
	using node_details_t= private_::node_details<LkT>;
	using atomic_ptr_t= typename node_details_t::base_t::atomic_ptr_t;
	using lock_traits= typename node_details_t::lock_traits;
	typedef std::size_t size_type;
	typedef shared_ptr<V, LkT> value_type;
	typedef typename lock_traits::template atomic_counter_type<unsigned long> size_ctr_t;
	typedef private_::slist_iterator_internal<V, LkT> iterator;
	typedef private_::slist_iterator_internal<V const, LkT> const_iterator;
	typedef typename iterator::difference_type difference_type;
	typedef typename iterator::pointer pointer;
	typedef typename iterator::reference reference;
	typedef typename const_iterator::const_pointer const_pointer;
	typedef typename const_iterator::const_reference const_reference;

	typedef typename value_type::deleter_t deleter_t;
	typedef typename value_type::ctr_type ctr_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (atomic_ptr_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && size_ctr_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	BOOST_MPL_ASSERT((std::is_base_of<typename node_details_t::base_t, typename value_type::value_type>));

	[[nodiscard]] __stdcall stack() noexcept(true)= default;
	stack(stack const&)= delete;
	stack(stack&&) noexcept(true);
	/**
		Algorithmic complexity: O(n)
	*/
	__stdcall ~stack() noexcept(true);

	/**
		Algorithmic complexity: O(1)
	*/
	iterator __fastcall begin() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	const_iterator __fastcall begin() const noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	iterator __fastcall end() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	const_iterator __fastcall end() const noexcept(true);
	/// Return true if the container is empty, false otherwise.
	/**
		Algorithmic complexity: O(1)

		\return	True if the container is empty, false otherwise.
	*/
	bool __fastcall empty() const noexcept(true);
	/// Atomically return the number of elements in the container.
	/**
		Note that all write operations should have been completed, otherwise the value returned will not be consistent.
		Algorithmic complexity: O(1)

		\return	The number of elements in the container.

		\see size_n()
	*/
	size_type __fastcall size() const noexcept(true);
	/// Return the number of elements in the container via chasing the pointers.
	/**
		Note that this is not atomic. All write-operations must have been completed.
		Mainly used for verifying list integrity in testing. Not much use for anything else.
		Algorithmic complexity: O(n)

		\return	The number of elements in the container.

		\see size()
	*/
	size_type __fastcall size_n() const noexcept(true);
	/// Remove the element from the container.
	/**
		Algorithmic complexity: O(1)

		\param v	Iterator to the element to be removed.
	*/
	void __fastcall erase(iterator v) noexcept(true);
	/// Non-atomically remove the element from the container.
	/**
		Algorithmic complexity: O(n).

		\param v	The value equivalent to the element to be removed.
		\return The number of elements erased from the list, at most 1.
	*/
	size_type __fastcall erase(const_reference v) noexcept(true);
	/// Remove all of the elements from the container.
	/**
		Calls the dtor for each element within the container.
		Algorithmic complexity: O(n)
	*/
	void __fastcall clear() noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\return The top of the stack.
	*/
	value_type __fastcall top() noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\return The top of the stack.
	*/
	value_type __fastcall top() const noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push(value_type const& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push(value_type&& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_front(value_type const& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_front(value_type&& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	void __fastcall pop() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	void __fastcall pop_front() noexcept(true);
	/**
		Algorithmic complexity: O(1)
		This method assumes that only one thread is popping at a time, and that the container is not empty.
	*/
	value_type __fastcall pop_top_nochk() noexcept(true);

private:
	friend class slist<V, LkT>;
	typename node_details_t::atomic_ptr_t head_{};
	size_ctr_t size_ctr{};

	void decrement_size() noexcept(true);
	typename node_details_t::atomic_ptr_t::value_type
	unlink_node(typename node_details_t::atomic_ptr_t& node) noexcept(true);

	/// Atomically insert the node in the value v to create the new head of the list.
	/**
		Algorithmic complexity: O(1)

		\param v	The node in the value to be added, i.e. add this value to the collection..

		\see push()
	*/
	void extend_left_from_front(value_type v) noexcept(true);
};

/// An implementation of a singly-linked, hybrid, intrusive, pointer-based list, that depending upon the lock_traits chosen would be lock-free.
/**
	When inserting a node, no memory allocations are required, which is good in a multi-threading environment as it reduces calls to any memory allocator.
	If you don't like the interface of this container, blame YAGNI & TDD and the funky use of atomic operations.

	\see boost::instrusive::slist, boost::slist, std::list
*/
template<
	class V,	  ///< The type of the contained objects, which must be intrusive, and publicly inherit from node_details.
	class LkT	///< The lock_traits that providing the (potentially) atomic operations to be used upon pointers to the value_type.
	>
class slist {
public:
	typedef stack<V, LkT> container_type;
	typedef typename container_type::value_type value_type;
	typedef typename container_type::lock_traits lock_traits;
	typedef typename container_type::size_ctr_t size_ctr_t;
	typedef typename container_type::size_type size_type;
	typedef typename container_type::iterator iterator;
	typedef typename container_type::const_iterator const_iterator;
	typedef typename container_type::difference_type difference_type;
	typedef typename container_type::pointer pointer;
	typedef typename container_type::reference reference;
	typedef typename container_type::const_pointer const_pointer;
	typedef typename container_type::const_reference const_reference;

	typedef typename container_type::deleter_t deleter_t;
	typedef typename container_type::ctr_type ctr_type;

	__stdcall slist() noexcept(true)= default;
	slist(slist&&) noexcept(true);
	/**
		Algorithmic complexity: O(n)
	*/
	__stdcall ~slist() noexcept(true)= default;

	/**
		Algorithmic complexity: O(1)
	*/
	iterator __fastcall begin() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	const_iterator __fastcall begin() const noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	iterator __fastcall end() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	const_iterator __fastcall end() const noexcept(true);
	/// Return true if the container is empty, false otherwise.
	/**
		Algorithmic complexity: O(1)

		\return	True if the container is empty, false otherwise.
	*/
	bool __fastcall empty() const noexcept(true);
	/// Atomically return the number of elements in the container.
	/**
		Note that all write operations should have been completed, otherwise the value returned will not be consistent.
		Algorithmic complexity: O(1)

		\return	The number of elements in the container.

		\see size_n()
	*/
	size_type __fastcall size() const noexcept(true);
	/// Return the number of elements in the container via chasing the pointers.
	/**
		Note that this is not atomic. All write-operations must have been completed.
		Mainly used for verifying list integrity in testing. Not much use for anything else.
		Algorithmic complexity: O(n)

		\return	The number of elements in the container.

		\see size()
	*/
	size_type __fastcall size_n() const noexcept(true);

	/**
	 * I do not believe it is possible to implement erase() in constant-time in general: one could try to be tricky with using nde->next in the iterator, but what about erasing the last node (tail_)? The penultimate node->next should be set to nullptr, but we do not have access to it, as it is before our iterator and this is a singly-linked list.
	 */
	void __fastcall erase(iterator) noexcept(true)= delete;
	/**
	 * This would essentially find the iterator that corresponds to the value, then calls the other erase() overload, which will not work in general. So this is not implemented.
	 */
	constexpr size_type __fastcall erase(const_reference) noexcept(true) {
		return 0;
	}
	/// Remove all of the elements from the container.
	/**
		\see stack::clear()
	*/
	void __fastcall clear() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	value_type __fastcall front() noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\return The front of the list.
	*/
	value_type __fastcall front() const noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	value_type __fastcall back() noexcept(true);
	/**
		Algorithmic complexity: O(1)
	*/
	value_type __fastcall back() const noexcept(true);
	/**
	 *			Algorithmic complexity: O(1)
	 *
	 *			\param v	The element to be added.
	 */
	void __fastcall push(value_type const& v) noexcept(true);
	/**
	 *			Algorithmic complexity: O(1)
	 *
	 *			\param v	The element to be added.
	 */
	void __fastcall push(value_type&& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_front(value_type const& v) noexcept(true);
	/**
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_front(value_type&& v) noexcept(true);
	/**
		Note that this is NOT atomic! So NOT thread-safe.
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_back(value_type const& v) noexcept(true);
	/**
		Note that this is NOT atomic! So NOT thread-safe.
		Algorithmic complexity: O(1)

		\param v	The element to be added.
	*/
	void __fastcall push_back(value_type&& v) noexcept(true);
	/**
	 *			Algorithmic complexity: O(1)
	 */
	void __fastcall pop() noexcept(true);
	/**
	 *			Algorithmic complexity: O(1)
	 */
	void __fastcall pop_front() noexcept(true);

	/// Verify that the tail_ pointer is reachable from head_ (head) of the list.
	/**
		Note that this is not atomic. All write-operations must have been completed.
		Algorithmic complexity: O(n)

		\return	True if the internal iterators are consistent.
	*/
	bool tail_reachable_from_head() const noexcept(true);

private:
	using node_details_t= typename container_type::node_details_t;

	container_type collection_{};
	/**
		Note that tail_ is not "end()", but the last valid node in the container, which allows us to push_back() in constant time.

		\see end(), push_back()
	*/
	typename node_details_t::atomic_ptr_t tail_{};

	/// Atomically insert the node in the value v into the collection at the location specified by node_ptr.
	/**
		Algorithmic complexity: O(1)

		\param node_ptr	The "iterator" immediately after which v should be added.
		\param v	The node in the value to be added, i.e. add this value to the collection..

		\see push()
	*/
	typename node_details_t::atomic_ptr_t::value_type add_on_right_of(typename node_details_t::atomic_ptr_t node_ptr, value_type v) noexcept(true);
};

}}}

#include "intrusive_impl.hpp"

#endif
