#ifndef ISIMUD_EXCHANGES_conversions_fix_to_batsboe_common_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_fix_to_batsboe_common_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"

#include "../../exchanges/FIX/common/conversions.hpp"
#include "../BATSBOE/common/messages.hpp"

#include "../common/isin.hpp"

#include <cstring>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

template<class Ret>
constexpr Ret
convert(std::string_view const&)
	= delete;

constexpr inline OrdType::element_type
convert_OrdType(std::string_view const& a) {
	DEBUG_ASSERT(a.size() == 1);
	return OrdType::to_enum(a);
}

constexpr inline TIF::element_type
convert_TIF(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return TIF::to_enum(a);
}

constexpr inline Side::element_type
convert_Side(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return Side::to_enum(a);
}

/**
	\todo We need to consider the symbology used, for example a conversion from ISIN to MIT native symbology.
*/
template<>
inline SecurityID_t
convert<SecurityID_t>(std::string_view const& a) {
	SecurityID_t ret;
	DEBUG_ASSERT(!a.empty());
	DEBUG_ASSERT(a.size() < ret.size());
	std::memcpy(ret.begin(), a.begin(), a.size());
	std::fill_n(ret.begin() + a.size(), ret.size() - a.size(), '\0');
	return ret;
}

/**
	Note that the SEDOL portion of the ISIN is used as the Symbology.
*/
template<>
inline Symbol_t
convert<Symbol_t>(std::string_view const& isin_ptrs) {
	BOOST_MPL_ASSERT_RELATION(sizeof(Symbol_t), >=, sizeof(exchanges::common::ISIN_t::SEDOL_t::padded_SEDOL::element_type));
	DEBUG_ASSERT(isin_ptrs.size() == sizeof(Symbol_t));
	Symbol_t ret;
	DEBUG_ASSERT(!isin_ptrs.empty());
	const exchanges::common::ISIN_t isin(reinterpret_cast<exchanges::common::ISIN_t::block_t const&>(isin_ptrs[0]));
	auto const& sedol= isin.details().sedol.padded_sedol.value;
	libjmmcg::memcpy_opt(sedol, ret);
	std::fill_n(ret.begin() + sedol.size(), ret.size() - sedol.size(), '\0');
	return ret;
}

template<>
inline Price_t
convert<Price_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<Price_t>(libjmmcg::fromstring<double>(a.begin(), a.size()) * implied_decimal_places);
}

template<>
inline Quantity_t
convert<Quantity_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<Quantity_t>(libjmmcg::fromstring<double>(a.begin(), a.size()) * implied_decimal_places);
}

}}}}}

#endif
