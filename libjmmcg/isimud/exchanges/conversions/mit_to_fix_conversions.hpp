#ifndef ISIMUD_EXCHANGES_conversions_mit_to_fix_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_mit_to_fix_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"
#include "../../exchanges/MIT/BIT/messages.hpp"
#include "../../exchanges/MIT/JSE/messages.hpp"
#include "../../exchanges/MIT/LSE/messages.hpp"
#include "../../exchanges/MIT/OSLO/messages.hpp"
#include "../../exchanges/MIT/TRQ/messages.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

/**
	Note that only ISIN symbology is supported.
*/
inline std::size_t
convert(MIT::common::SecurityID_t const& key, MIT::common::isin_mapping_data_const_reference ref_data, pointer buff, [[maybe_unused]] std::size_t sz) {
	DEBUG_ASSERT(!ref_data.empty());
	auto const& isin= ref_data.at(key);
	DEBUG_ASSERT(sizeof(MIT::common::ref_data::key_type::element_type) <= sz);
	libjmmcg::memcpy_opt(isin.isin_.data(), reinterpret_cast<MIT::common::ref_data::key_type::element_type::block_t&>(*buff));
	return sizeof(MIT::common::ref_data::key_type::element_type);
}

/**
	\todo What should be done with ExecType's that cannot be mapped?.
*/
inline ExecType
convert(MIT::common::ExecType::element_type const& a) noexcept(false) {
	switch(a) {
	case MIT::common::ExecType::New:
		return ExecType::New;
	case MIT::common::ExecType::Cancelled:
		return ExecType::Canceled;
	case MIT::common::ExecType::Replaced:
		return ExecType::Replaced;
	case MIT::common::ExecType::Rejected:
		return ExecType::Rejected;
	case MIT::common::ExecType::Suspended:
		return ExecType::Suspended;
	case MIT::common::ExecType::Expired:
		return ExecType::Expired;
	case MIT::common::ExecType::Restated:
		return ExecType::Restated;
	case MIT::common::ExecType::Trade:
		return ExecType::Trade;
	case MIT::common::ExecType::TradeCorrect:
		return ExecType::Trade_Correct;
	case MIT::common::ExecType::TradeCancel:
		return ExecType::Trade_Cancel;
	case MIT::common::ExecType::Triggered:
		return ExecType::Triggered_or_Activated_by_System;
	default:
		BOOST_THROW_EXCEPTION(libjmmcg::throw_api_exception<std::runtime_error>(fmt::format("MIT ExecType cannot be mapped to FIX: '{}'", (MIT::common::ExecType::contains(a) ? MIT::common::ExecType::to_string(a) : fmt::format("unknown value={}", static_cast<MIT::common::ExecType::underlying_type_t>(a)))), static_cast<ExecType (*)(MIT::common::ExecType::element_type const&)>(convert)));
	};
}

/**
	\todo What should be done with OrderStatus's that cannot be mapped?.
*/
inline OrdStatus
convert(MIT::common::OrderStatus::element_type const& a) noexcept(false) {
	switch(a) {
	case MIT::common::OrderStatus::New:
		return OrdStatus::New;
	case MIT::common::OrderStatus::Partiallyfilled:
		return OrdStatus::Partially_filled;
	case MIT::common::OrderStatus::Filled:
		return OrdStatus::Filled;
	case MIT::common::OrderStatus::Cancelled:
		return OrdStatus::Canceled;
	case MIT::common::OrderStatus::Expired:
		return OrdStatus::Expired;
	case MIT::common::OrderStatus::Rejected:
		return OrdStatus::Rejected;
	case MIT::common::OrderStatus::Suspended:
		return OrdStatus::Suspended;
	default:
		BOOST_THROW_EXCEPTION(libjmmcg::throw_api_exception<std::runtime_error>(fmt::format("MIT OrderStatus cannot be mapped to FIX: '{}'", (MIT::common::OrderStatus::contains(a) ? MIT::common::OrderStatus::to_string(a) : fmt::format("unknown value={}", static_cast<MIT::common::ExecType::underlying_type_t>(a)))), static_cast<OrdStatus (*)(MIT::common::OrderStatus::element_type const&)>(convert)));
	};
}

inline std::size_t
convert_MIT_price(MIT::common::Price_t const& a, pointer buff, std::size_t sz) {
	const double c= static_cast<double>(a) / MIT::common::implied_decimal_places;
	return libjmmcg::tostring(c, buff, sz);
}

/**
	\todo What should be done with Side's that cannot be mapped?.
*/
constexpr inline FIX::common::Side
convert(MIT::common::Side::element_type const& a) {
	switch(a) {
	case MIT::common::Side::Buy:
		return FIX::common::Side::Buy;
	case MIT::common::Side::Sell:
		return FIX::common::Side::Sell;
	default:
		BOOST_THROW_EXCEPTION(libjmmcg::throw_api_exception<std::runtime_error>(fmt::format("MIT Side cannot be mapped to FIX: '{}'", (MIT::common::Side::contains(a) ? MIT::common::Side::to_string(a) : fmt::format("unknown value={}", static_cast<MIT::common::Side::underlying_type_t>(a)))), static_cast<FIX::common::Side (*)(MIT::common::Side::element_type const&)>(convert)));
	};
}

template<>
template<>
inline __stdcall Message<v5_0sp2::ExecutionReportSpecific>::Message(MIT::BIT::MsgTypes::ExecutionReport_t const& msg, MIT::BIT::MsgTypes::isin_mapping_data_const_reference rd)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data= static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data= add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i= convert(msg.instrumentID(), rd, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= add_field_tag<FieldsFast::ExecType>(data);
	*data= static_cast<std::underlying_type<FIX::common::ExecType>::type>(convert(msg.execType()));
	++data;
	data= add_field_tag<FieldsFast::Price>(data);
	const std::size_t j= convert_MIT_price(msg.executedPrice(), data, this->data_.size() - (data - this->data_.begin()));
	data+= j;
	data= add_field_tag<FieldsFast::OrdStatus>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.orderStatus()));
	++data;
	data= add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k= convert(msg.executedQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= k;
	data= add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l= convert(msg.leavesQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= l;
	data= add_field_tag<FieldsFast::Side>(data);
	*data= static_cast<std::underlying_type<FIX::common::Side>::type>(convert(msg.side()));
	++data;
	data= write_ExDest(MIT::BIT::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	msg.orderRejectCode();
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::ExecutionReportSpecific>::Message(MIT::JSE::MsgTypes::ExecutionReport_t const& msg, MIT::JSE::MsgTypes::isin_mapping_data_const_reference rd)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data= static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data= add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i= convert(msg.instrumentID(), rd, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= add_field_tag<FieldsFast::ExecType>(data);
	*data= static_cast<std::underlying_type<FIX::common::ExecType>::type>(convert(msg.execType()));
	++data;
	data= add_field_tag<FieldsFast::Price>(data);
	const std::size_t j= convert_MIT_price(msg.executedPrice(), data, this->data_.size() - (data - this->data_.begin()));
	data+= j;
	data= add_field_tag<FieldsFast::OrdStatus>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.orderStatus()));
	++data;
	data= add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k= convert(msg.executedQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= k;
	data= add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l= convert(msg.leavesQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= l;
	data= add_field_tag<FieldsFast::Side>(data);
	*data= static_cast<std::underlying_type<FIX::common::Side>::type>(convert(msg.side()));
	++data;
	data= write_ExDest(MIT::JSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	msg.orderRejectCode();
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::ExecutionReportSpecific>::Message(MIT::LSE::MsgTypes::ExecutionReport_t const& msg, MIT::LSE::MsgTypes::isin_mapping_data_const_reference rd)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data= static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data= add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i= convert(msg.instrumentID(), rd, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= add_field_tag<FieldsFast::ExecType>(data);
	*data= static_cast<std::underlying_type<FIX::common::ExecType>::type>(convert(msg.execType()));
	++data;
	data= add_field_tag<FieldsFast::Price>(data);
	const std::size_t j= convert_MIT_price(msg.executedPrice(), data, this->data_.size() - (data - this->data_.begin()));
	data+= j;
	data= add_field_tag<FieldsFast::OrdStatus>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.orderStatus()));
	++data;
	data= add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k= convert(msg.executedQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= k;
	data= add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l= convert(msg.leavesQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= l;
	data= add_field_tag<FieldsFast::Side>(data);
	*data= static_cast<std::underlying_type<FIX::common::Side>::type>(convert(msg.side()));
	++data;
	data= write_ExDest(MIT::LSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	msg.orderRejectCode();
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::ExecutionReportSpecific>::Message(MIT::OSLO::MsgTypes::ExecutionReport_t const& msg, MIT::OSLO::MsgTypes::isin_mapping_data_const_reference rd)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data= static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data= add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i= convert(msg.instrumentID(), rd, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= add_field_tag<FieldsFast::ExecType>(data);
	*data= static_cast<std::underlying_type<FIX::common::ExecType>::type>(convert(msg.execType()));
	++data;
	data= add_field_tag<FieldsFast::Price>(data);
	const std::size_t j= convert_MIT_price(msg.executedPrice(), data, this->data_.size() - (data - this->data_.begin()));
	data+= j;
	data= add_field_tag<FieldsFast::OrdStatus>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.orderStatus()));
	++data;
	data= add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k= convert(msg.executedQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= k;
	data= add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l= convert(msg.leavesQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= l;
	data= add_field_tag<FieldsFast::Side>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.side()));
	++data;
	data= write_ExDest(MIT::OSLO::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	msg.orderRejectCode();
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::ExecutionReportSpecific>::Message(MIT::TRQ::MsgTypes::ExecutionReport_t const& msg, MIT::TRQ::MsgTypes::isin_mapping_data_const_reference rd)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data= static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data= add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i= convert(msg.instrumentID(), rd, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= add_field_tag<FieldsFast::ExecType>(data);
	*data= static_cast<std::underlying_type<FIX::common::ExecType>::type>(convert(msg.execType()));
	++data;
	data= add_field_tag<FieldsFast::Price>(data);
	const std::size_t j= convert_MIT_price(msg.executedPrice(), data, this->data_.size() - (data - this->data_.begin()));
	data+= j;
	data= add_field_tag<FieldsFast::OrdStatus>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.orderStatus()));
	++data;
	data= add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k= convert(msg.executedQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= k;
	data= add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l= convert(msg.leavesQty(), data, this->data_.size() - (data - this->data_.begin()));
	data+= l;
	data= add_field_tag<FieldsFast::Side>(data);
	*data= static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(convert(msg.side()));
	++data;
	data= write_ExDest(MIT::TRQ::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	msg.orderRejectCode();
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::BIT::MsgTypes::ExecutionReport_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::Reject>(msg);
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::BIT::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::JSE::MsgTypes::ExecutionReport_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::Reject>(msg);
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::JSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::LSE::MsgTypes::ExecutionReport_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::Reject>(msg);
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::LSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::OSLO::MsgTypes::ExecutionReport_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::Reject>(msg);
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::OSLO::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::TRQ::MsgTypes::ExecutionReport_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::Reject>(msg);
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::TRQ::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::CancelRejectedSpecific>::Message(MIT::BIT::MsgTypes::OrderCancelReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::OrderID>(data);
	libjmmcg::memcpy_opt(msg.orderID, reinterpret_cast<MIT::common::OrderID_t&>(*data));
	data+= msg.orderID.size() - 1;
	data= write_ExDest(MIT::BIT::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	cancelRejectReason_;
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::CancelRejectedSpecific>::Message(MIT::JSE::MsgTypes::OrderCancelReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::OrderID>(data);
	libjmmcg::memcpy_opt(msg.orderID, reinterpret_cast<MIT::common::OrderID_t&>(*data));
	data+= msg.orderID.size() - 1;
	data= write_ExDest(MIT::JSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	cancelRejectReason_;
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::CancelRejectedSpecific>::Message(MIT::LSE::MsgTypes::OrderCancelReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::OrderID>(data);
	libjmmcg::memcpy_opt(msg.orderID, reinterpret_cast<MIT::common::OrderID_t&>(*data));
	data+= msg.orderID.size() - 1;
	data= write_ExDest(MIT::LSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	cancelRejectReason_;
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::CancelRejectedSpecific>::Message(MIT::OSLO::MsgTypes::OrderCancelReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::OrderID>(data);
	libjmmcg::memcpy_opt(msg.orderID, reinterpret_cast<MIT::common::OrderID_t&>(*data));
	data+= msg.orderID.size() - 1;
	data= write_ExDest(MIT::OSLO::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	cancelRejectReason_;
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::CancelRejectedSpecific>::Message(MIT::TRQ::MsgTypes::OrderCancelReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::OrderID>(data);
	libjmmcg::memcpy_opt(msg.orderID, reinterpret_cast<MIT::common::OrderID_t&>(*data));
	data+= msg.orderID.size() - 1;
	data= write_ExDest(MIT::TRQ::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO	cancelRejectReason_;
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::BIT::MsgTypes::Reject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= write_ExDest(MIT::BIT::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::JSE::MsgTypes::Reject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= write_ExDest(MIT::JSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::LSE::MsgTypes::Reject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= write_ExDest(MIT::LSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::OSLO::MsgTypes::Reject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= write_ExDest(MIT::OSLO::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::OrderRejectedSpecific>::Message(MIT::TRQ::MsgTypes::Reject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= write_ExDest(MIT::TRQ::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(MIT::BIT::MsgTypes::BusinessReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::BIT::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(MIT::JSE::MsgTypes::BusinessReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::JSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(MIT::LSE::MsgTypes::BusinessReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::LSE::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

template<>
template<>
inline __stdcall Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(MIT::OSLO::MsgTypes::BusinessReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::OSLO::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	data+= i;
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}
template<>
template<>
inline __stdcall Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(MIT::TRQ::MsgTypes::BusinessReject_t const& msg)
	: Header_t() {
	underlying_fix_data_buffer::iterator data= set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<MIT::common::ClientOrderID_t&>(*data));
	data+= msg.clientOrderID().size() - 1;
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i= convert(msg.sequenceNumber, data, this->data_.size() - (data - this->data_.begin()));
	data+= i;
	data= write_ExDest(MIT::TRQ::MsgTypes::MIC_code, data);
	data= write_TransactTime(data);
	// TODO
	DEBUG_ASSERT(static_cast<size_type>(data - static_cast<pointer>(begin_string)) < sizeof(Header));
	finalise_msg(data);
}

}}}}}

#endif
