#ifndef ISIMUD_EXCHANGES_conversions_fix_to_mit_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_fix_to_mit_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"

#include "../../exchanges/FIX/common/conversions.hpp"

#include "../../exchanges/MIT/BIT/messages.hpp"
#include "../../exchanges/MIT/JSE/messages.hpp"
#include "../../exchanges/MIT/LSE/messages.hpp"
#include "../../exchanges/MIT/OSLO/messages.hpp"
#include "../../exchanges/MIT/TRQ/messages.hpp"

#include "core/memops.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

namespace common {

template<class Ret>
Ret
convert(std::string_view const&) [[gnu::pure]]
= delete;

constexpr inline OrderType::element_type
convert_OrderType(std::string_view const& a) {
	DEBUG_ASSERT(a.size() == 1);
	return OrderType::to_enum(a);
}

inline constexpr TIF::element_type
convert_TIF(std::string_view const& a) {
	DEBUG_ASSERT(a.size() == 1);
	return TIF::to_enum(a);
}

constexpr inline Side::element_type
convert_Side(std::string_view const& a) {
	DEBUG_ASSERT(a.size() == 1);
	return Side::to_enum(a);
}

/**
	Note that only ISIN symbology is supported.
*/
template<class MsgT>
constexpr inline SecurityID_t
convert_SecurityID_t(MsgT const& msg, MIT::common::isin_mapping_data_const_reference ref_data) {
	auto const& isin_ptrs= msg.template find<FIX::common::FieldsFast::SecurityID>();
	DEBUG_ASSERT(isin_ptrs.size() == sizeof(MIT::common::ref_data::key_type));
	const MIT::common::ref_data::key_type::element_type isin(reinterpret_cast<MIT::common::ref_data::key_type::element_type::block_t const&>(*isin_ptrs.data()));
	const MIT::common::ref_data::key_type key(isin);
	return ref_data.at(key);
}

template<>
inline Price_t
convert<Price_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<Price_t>(libjmmcg::fromstring<double>(a.begin(), a.size()) * implied_decimal_places);
}

template<>
inline __stdcall LogonRequest::LogonRequest(FIX::v5_0sp2::MsgTypes::LogonRequest_t const& msg) noexcept(true)
	: LogonRequest(
		libjmmcg::copy<typename LogonRequest::logon_args_t::UserName_t>(msg.find<FIX::common::FieldsFast::Username>()),
		libjmmcg::copy<typename LogonRequest::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::Password>()),
		libjmmcg::copy<typename LogonRequest::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::NewPassword>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall constexpr LogoutRequest::LogoutRequest(FIX::v5_0sp2::MsgTypes::LogoutRequest_t const&, Reason_t r) noexcept(true)
	: LogoutRequest(r) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall constexpr Heartbeat::Heartbeat(FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const&) noexcept(true)
	: Heartbeat() {
	DEBUG_ASSERT(is_valid());
}

}

namespace BIT {

template<>
inline __stdcall NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		common::convert_OrderType(msg.find<FIX::common::FieldsFast::OrdType>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

}

namespace JSE {

template<>
inline __stdcall Logon::Logon(FIX::v5_0sp2::MsgTypes::LogonRequest_t const& msg) noexcept(true)
	: Logon(
		libjmmcg::copy<typename Logon::logon_args_t::UserName_t>(msg.find<FIX::common::FieldsFast::Username>()),
		libjmmcg::copy<typename Logon::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::Password>()),
		libjmmcg::copy<typename Logon::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::NewPassword>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: NewOrder(
		static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		common::convert_OrderType(msg.find<FIX::common::FieldsFast::OrdType>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: OrderCancelReplaceRequest(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

}

namespace LSE {

template<>
inline __stdcall NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		common::convert_OrderType(msg.find<FIX::common::FieldsFast::OrdType>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

}

namespace OSLO {

template<>
inline __stdcall NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		common::convert_OrderType(msg.find<FIX::common::FieldsFast::OrdType>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

}

namespace TRQ {

template<>
inline __stdcall NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		common::convert_OrderType(msg.find<FIX::common::FieldsFast::OrdType>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

template<>
inline __stdcall OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& msg, isin_mapping_data_const_reference rd) noexcept(true)
	: base_t(
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
		common::convert_SecurityID_t(msg, rd),
		FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		common::convert_TIF(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		common::convert_Side(msg.find<FIX::common::FieldsFast::Side>())) {
	DEBUG_ASSERT(is_valid());
}

}

}}}}

#endif
