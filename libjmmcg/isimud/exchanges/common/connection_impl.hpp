/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<class MsgDetails, class ConnPol>
inline connection<MsgDetails, ConnPol>::connection(conn_pol_t const& conn_pol, skt_mgr_t::socket_priority priority, std::size_t incoming_cpu)
	: conn_pol_(conn_pol), link(msg_details_t::min_msg_size, msg_details_t::max_msg_size, (conn_pol_t::max_attempts * conn_pol_t::min_timeout), priority, incoming_cpu, conn_pol_) {
}

template<class MsgDetails, class ConnPol>
template<class Msg>
inline
	typename std::enable_if<
		!std::is_same<
			typename boost::mpl::find<typename MsgDetails::client_to_exchange_messages_t, Msg>::type,
			typename boost::mpl::end<typename MsgDetails::client_to_exchange_messages_t>::type>::value,
		void>::type
	connection<MsgDetails, ConnPol>::send(Msg const& msg) {
	link.write(msg);
}

template<class MsgDetails, class ConnPol>
template<class Msg>
inline
	typename std::enable_if<
		!std::is_same<
			typename boost::mpl::find<typename MsgDetails::exchange_to_client_messages_t, Msg>::type,
			typename boost::mpl::end<typename MsgDetails::exchange_to_client_messages_t>::type>::value,
		bool>::type
	connection<MsgDetails, ConnPol>::receive(Msg& msg) {
	return link.read(msg);
}

template<class MsgDetails, class ConnPol>
inline void
connection<MsgDetails, ConnPol>::stop() {
	link.stop();
}

template<class MsgDetails, class ConnPol>
inline std::string
connection<MsgDetails, ConnPol>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<< boost::core::demangle(typeid(*this).name())
		<< ",\nlink: " << link;
	return ss.str();
}

template<class MsgDetails, class ConnPol>
inline std::ostream&
operator<<(std::ostream& os, connection<MsgDetails, ConnPol> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::connection_processor(ctor_args const& cxn_details)
	: ref_data(std::get<3>(cxn_details)),
	  client_to_exchg_processor(client_to_exchg_proc_rules_t(*ref_data)),
	  exchg_to_client_processor(exchg_to_client_proc_rules_t(*ref_data)),
	  cxn_(std::get<0>(cxn_details), std::get<1>(cxn_details), std::get<2>(cxn_details)) {
	cxn_.send(typename exchg_msg_details_t::LogonRequest_t(cxn_.conn_pol_.logon_args));
	typename exchg_msg_details_t::LogonReply_t msg;
	// We could get a heartbeat: ignore it until the logon is obtained.
	do {
		if(cxn_.receive(msg)) {
			BOOST_THROW_EXCEPTION(libjmmcg::throw_api_exception<std::runtime_error>(fmt::format("Failed to receive the LogonReply message, another, unexpected, message was received instead. Received message-type='{}'", libjmmcg::tostring(msg.type())), this));
		}
	} while(static_cast<typename exchg_msg_details_t::MsgType_t>(msg.type()) != static_cast<typename exchg_msg_details_t::MsgType_t>(exchg_msg_details_t::LogonReply_t::static_type));
	const typename exchg_msg_details_t::LogonReply_t::respond process_response{};
	process_response(msg, [this]() {
		logged_on_.test_and_set(std::memory_order_seq_cst);
	});
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::~connection_processor() noexcept(false) {
	cxn_.send(typename exchg_msg_details_t::LogoutRequest_t(cxn_.conn_pol_.logoff_args));
	// TODO Don't check this because the socket may have closed whilst reading the message, so it may be gash...
	typename exchg_msg_details_t::Logout_t msg;
	bool failed= true;
	// We could get a heartbeat: ignore it until the logout is obtained.
	do {
		failed= cxn_.receive(msg);
	} while(!failed && static_cast<typename exchg_msg_details_t::MsgType_t>(msg.type()) != static_cast<typename exchg_msg_details_t::MsgType_t>(exchg_msg_details_t::Logout_t::static_type));
	DEBUG_ASSERT(failed || static_cast<typename exchg_msg_details_t::MsgType_t>(msg.type()) == static_cast<typename exchg_msg_details_t::MsgType_t>(exchg_msg_details_t::Logout_t::static_type));
	logged_on_.clear(std::memory_order_seq_cst);
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
template<class ClientCxn>
inline bool
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::read_and_process_a_msg(exchg_msg_buffer_t const& buff, ClientCxn& dest_cxn) noexcept(false) {
	return exchg_to_client_processor.read_and_process_a_msg(buff, cxn_.socket(), dest_cxn);
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
template<class ClientCxn>
inline bool
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::read_and_process_a_msg(exchg_msg_buffer_t const& buff, ClientCxn& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	const libjmmcg::latency_timestamps_itf::period t(ts);
	return exchg_to_client_processor.read_and_process_a_msg(buff, cxn_.socket(), dest_cxn);
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
template<class ClientCxn>
inline bool
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::read_and_process_a_msg(ClientCxn& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE exchg_msg_buffer_t buff;
	if(!exchg_to_client_processor.read_msg_into_buff(cxn_.socket(), buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		return exchg_to_client_processor.read_and_process_a_msg(buff, cxn_.socket(), dest_cxn, ts);
	} else {
		return true;
	}
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline bool
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::read_and_process_a_msg(client_msg_buffer_t const& buff, socket_t& dest_skt, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	return client_to_exchg_processor.read_and_process_a_msg(buff, dest_skt, cxn_.socket(), ts);
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
template<class Cxn>
inline bool
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::read_and_process_a_msg(Cxn& src_cxn, Cxn& dest_cxn) noexcept(false) {
	return client_to_exchg_processor.read_and_process_a_msg(src_cxn, dest_cxn);
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline void
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::stop() {
	cxn_.stop();
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline std::string
connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<< boost::core::demangle(typeid(*this).name())
		<< ",\nclient_to_exchg_processor: " << client_to_exchg_processor
		<< ",\nexchg_to_client_processor: " << exchg_to_client_processor
		<< ", cxn_: " << cxn_;
	return ss.str();
}

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline std::ostream&
operator<<(std::ostream& os, connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}}}}
