#ifndef ISIMUD_EXCHANGES_COMMON_EXCHANGE_CONNECTION_HPP
#define ISIMUD_EXCHANGES_COMMON_EXCHANGE_CONNECTION_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_traits.hpp"

#include "exchange_to_client_processor.hpp"
#include "iso_3166_country_codes.hpp"
#include "iso_4217_currency_codes.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// A class that implements bi-directional client-to-exchange message flow.
/**
	This class is implemented with two threads: one handles the client-to-exchange messages, the other that handles the exchange-to-client messages. This implies that for optimal performance two cores are required per instance of this class. i.e. optimally, a computer can run P/2 instances of this class, where P is the number of available cores.
	Ideally the computer should have at least two physical processors and two physical PCI buses. Each processor should be connected to only one PCI bus with processor zero connected to all of the peripherals (SATA, etc buses). At a minimum taskset, numactl (e.g. "numactl --preferred=1 --cpunodebind=1") or CPU groups (cgroups) should be used to isolate the OS to processor zero (including suitable interrupts), and the process that instantiates this class should be isolated to all processors excluding zero. Onto the PCI buses connected to those processors the network cards should be attached. The network cards should be DMA-capable at the minimum. Consideration should be given to using Solarflare (or equivalent) network cards and <a href="http://www.openonload.org/">OpenOnload</a>, with suitable configuration. Co-location should be a consideration.
*/
template<template<class> class ClientCxn, class... ExchgCxns>
class exchange_connection final {
public:
	/// A non-empty set of single connections to different specific exchanges.
	using exchg_links_t= exchange_to_client_processor<ExchgCxns...>;
	/// A single connection to a client.
	using client_link_t= ClientCxn<exchg_links_t>;
	/// A type containing the details of the client-side messages that will be sent and received to and from the client.
	using client_msg_details_t= typename client_link_t::src_msg_details_t;
	/// A type containing the details of the client-side messages that will be sent and received to and from the exchange.
	using socket_t= typename client_link_t::socket_t;
	using socket_priority= typename client_link_t::socket_priority;

	template<class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgCxns) == sizeof...(ExchCxnCtorArgs), bool>::type= true>
	exchange_connection(typename client_link_t::ctor_args const& client_cxn_details, typename exchg_links_t::report_error_fn_t& exchg_report_error, typename client_link_t::report_error_fn_t& client_report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, thread_traits::api_threading_traits::thread_name_t const& svr_name, ExchCxnCtorArgs&&... exchange_cxn_args);
	template<class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgCxns) == sizeof...(ExchCxnCtorArgs), bool>::type= true>
	exchange_connection(typename client_link_t::ctor_args const& client_cxn_details, typename exchg_links_t::report_error_fn_t& exchg_report_error, typename client_link_t::report_error_fn_t& client_report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, thread_traits::api_threading_traits::thread_name_t const& svr_name, std::tuple<ExchCxnCtorArgs...> exchange_cxn_args);
	~exchange_connection() noexcept(false);

	bool are_all_logged_on() const noexcept(true);

	std::atomic_flag& exit_requested() noexcept(true);

	std::string to_string() const noexcept(false);

private:
	/**
		Ensure that the exchange-to-client flow of messages shuts down as soon as possible.
	 */
	std::atomic_flag exit_requested_{};
	/**
		Ensure that we have a valid link to the exchange before we allow the client to connect. This is to stop the client sending messages before we have a valid link to the exchange.
	*/
	exchg_links_t exchg_links_;
	client_link_t client_link;
};

template<template<class> class ClientCxn, class... ExchgCxns>
inline std::ostream&
operator<<(std::ostream& os, exchange_connection<ClientCxn, ExchgCxns...> const& ec) noexcept(false);

}}}}

#include "exchange_connection_impl.hpp"

#endif
