/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<class ExchgLink>
class exchange_to_client_processor<ExchgLink>::flow_t final {
public:
	flow_t(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, exchg_link_t& exchg_link) noexcept(true);

	~flow_t() noexcept(false);

	/// A client has connected so make sure the connection is passed to the exchg_to_client_thread, so it can forward messages as necessary.
	void client_connected(client_connection_t client_connection) noexcept(true);

	std::string to_string() const noexcept(false);

private:
	struct context final : public libjmmcg::ppd::null_thread_worker_context {
		const typename thread_t::thread_traits::api_params_type::processor_mask_type cpu_core_;
		const thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority_;

		[[nodiscard]] context(std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority) noexcept(true)
			: cpu_core_(cpu_core), cpu_priority_(cpu_priority) {
		}

		void operator()(libjmmcg::ppd::null_thread_worker_context::thread_t& thread) const noexcept(false) {
			thread.kernel_priority(cpu_priority_);
			thread.kernel_affinity(cpu_core_);
			thread.set_name("exch2cli" LIBJMMCG_ENQUOTE(__LINE__));
		}
	};
	class mutator final {
	public:
		std::atomic_flag& exit_requested_;

		[[nodiscard]] mutator(std::atomic_flag& exit_requested, exchg_link_t& exchg_link, libjmmcg::latency_timestamps_itf& timestamps, report_error_fn_t& report_error) noexcept(true)
			: exit_requested_(exit_requested), timestamps_(timestamps), report_error_(report_error), exchg_link_(exchg_link) {
		}

		void operator()(std::atomic_flag& thread_exit_requested, context&) {
			while(!exit_requested_.test(std::memory_order_relaxed) && !thread_exit_requested.test(std::memory_order_relaxed)) [[likely]] {
				try {
					if(exchg_link_.read_and_process_a_msg(client_connection_, timestamps_)) [[unlikely]] {
						break;
					}
				} catch(...) {
					report_error_(exchg_link_.to_string(), client_connection_, std::current_exception());
				}
			}
			thread_exit_requested.test_and_set(std::memory_order_seq_cst);
			thread_exit_requested.notify_all();
			exit_requested_.test_and_set(std::memory_order_seq_cst);
			exit_requested_.notify_all();
		}

		void
		client_connected(client_connection_t client_connection) noexcept(true) {
			client_connection_= client_connection;
			// Could be a nullptr as the client may have nicely disconnected from us, so may not be open...
			DEBUG_ASSERT(!client_connection_ || client_connection_->socket().is_open());
		}

		friend inline std::ostream&
		operator<<(std::ostream& os, mutator const& m) noexcept(false) {
			if(m.client_connection_) {
				os << m.client_connection_->socket();
			} else {
				os << "no client connected.";
			}
			return os;
		}

	private:
		client_connection_t client_connection_{};
		libjmmcg::latency_timestamps_itf& timestamps_;
		report_error_fn_t& report_error_;
		exchg_link_t& exchg_link_;
	};
	using thread_t= libjmmcg::ppd::wrapper<mutator, libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading, context>;

	/**
		The thread that spins waiting for messages from the exchange to forward to the connected client.
	*/
	thread_t exchg_to_client_thread;
};

template<class ExchgLink>
template<class ExchCxnCtorArg>
inline exchange_to_client_processor<ExchgLink>::exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArg&& exchange_cxn_arg)
	: exchg_link(std::forward<ExchCxnCtorArg>(exchange_cxn_arg)),
	  exchange_to_client(e, report_error, cpu_core, cpu_priority, timestamps, exchg_link) {
}

template<class ExchgLink>
inline bool
exchange_to_client_processor<ExchgLink>::are_all_logged_on() const noexcept(true) {
	return exchg_link.is_logged_on();
}

template<class ExchgLink>
inline void
exchange_to_client_processor<ExchgLink>::exchange_to_client_processor::client_connected(client_connection_t client_connection) noexcept(true) {
	exchange_to_client.client_connected(client_connection);
}

template<class ExchgLink>
inline bool
exchange_to_client_processor<ExchgLink>::read_and_process_a_msg(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn.socket().template read<src_msg_details_t>(buff)(src_cxn, buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		return exchg_link.read_and_process_a_msg(buff, src_cxn.socket(), dest_cxn.socket());
	} else {
		return true;
	}
}

template<class ExchgLink>
inline bool
exchange_to_client_processor<ExchgLink>::read_and_process_a_msg(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn.socket().template read<src_msg_details_t>(buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		return exchg_link.read_and_process_a_msg(buff, src_cxn.socket(), ts);
	} else {
		return true;
	}
}

template<class ExchgLink>
inline bool
exchange_to_client_processor<ExchgLink>::read_and_process_a_msg(client_connection_t& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn->template read<src_msg_details_t>(buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		return exchg_link.read_and_process_a_msg(buff, src_cxn, dest_cxn.socket());
	} else {
		return true;
	}
}

template<class ExchgLink>
inline std::string
exchange_to_client_processor<ExchgLink>::to_string() const noexcept(false) {
	std::ostringstream os;
	os << "exchange connection: " << exchg_link << ", exchange-to-client details: " << exchange_to_client.to_string();
	return os.str();
}

template<class ExchgLink>
inline exchange_to_client_processor<ExchgLink>::flow_t::flow_t(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, exchg_link_t& exchg_link) noexcept(true)
	: exchg_to_client_thread(context(cpu_core, cpu_priority), e, exchg_link, timestamps, report_error) {
	exchg_to_client_thread.create_running();
}

template<class ExchgLink>
inline exchange_to_client_processor<ExchgLink>::flow_t::~flow_t() noexcept(false) {
	exchg_to_client_thread->exit_requested_.test_and_set(std::memory_order_seq_cst);
}

template<class ExchgLink>
inline void
exchange_to_client_processor<ExchgLink>::flow_t::client_connected(client_connection_t client_connection) noexcept(true) {
	exchg_to_client_thread->client_connected(client_connection);
}

template<class ExchgLink>
inline std::string
exchange_to_client_processor<ExchgLink>::flow_t::to_string() const noexcept(false) {
	std::ostringstream os;
	os << ", exchange-to-client core=" << common::thread_traits::exchange_to_client_thread << ", client link: " << *exchg_to_client_thread;
	return os.str();
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
class exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::flow_t final : public flow_itf {
public:
	flow_t(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, exchg_link_t& exchg_link) noexcept(true);

	~flow_t() noexcept(false);

	/// A client has connected so make sure the connection is passed to the exchg_to_client_thread, so it can forward messages as necessary.
	void client_connected(client_connection_t client_connection) noexcept(true) override;

	std::string to_string() const noexcept(false) override;

private:
	struct context : public libjmmcg::ppd::null_thread_worker_context {
		const typename thread_t::thread_traits::api_params_type::processor_mask_type cpu_core_;
		const thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority_;

		[[nodiscard]] context(std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority) noexcept(true)
			: cpu_core_(cpu_core), cpu_priority_(cpu_priority) {
		}

		void operator()(libjmmcg::ppd::null_thread_worker_context::thread_t& thread) const noexcept(false) {
			thread.kernel_priority(cpu_priority_);
			thread.kernel_affinity(
				typename thread_t::thread_traits::api_params_type::processor_mask_type(cpu_core_));
			thread.set_name("exch2cli" LIBJMMCG_ENQUOTE(__LINE__));
		}
	};
	class mutator final {
	public:
		std::atomic_flag& exit_requested_;

		[[nodiscard]] mutator(std::atomic_flag& exit_requested, exchg_link_t& exchg_link, libjmmcg::latency_timestamps_itf& timestamps, report_error_fn_t& report_error) noexcept(true)
			: exit_requested_(exit_requested), timestamps_(timestamps), report_error_(report_error), exchg_link_(exchg_link) {
		}

		void operator()(std::atomic_flag& thread_exit_requested, context&) {
			while(!exit_requested_.test(std::memory_order_relaxed) && !thread_exit_requested.test(std::memory_order_relaxed)) [[likely]] {
				try {
					if(exchg_link_.read_and_process_a_msg(client_connection_, timestamps_)) [[unlikely]] {
						break;
					}
				} catch(...) {
					report_error_(exchg_link_.to_string(), client_connection_, std::current_exception());
				}
			}
			thread_exit_requested.test_and_set(std::memory_order_seq_cst);
			thread_exit_requested.notify_all();
			exit_requested_.test_and_set(std::memory_order_seq_cst);
			exit_requested_.notify_all();
		}

		void
		client_connected(client_connection_t client_connection) noexcept(true) {
			client_connection_= client_connection;
			// Could be a nullptr as the client may have nicely disconnected from us, so may not be open...
			DEBUG_ASSERT(!client_connection_ || client_connection_->socket().is_open());
		}

		friend inline std::ostream&
		operator<<(std::ostream& os, mutator const& m) noexcept(false) {
			if(m.client_connection_) {
				os << m.client_connection_->socket();
			} else {
				os << "no client connected.";
			}
			return os;
		}

	private:
		client_connection_t client_connection_{};
		libjmmcg::latency_timestamps_itf& timestamps_;
		report_error_fn_t& report_error_;
		exchg_link_t& exchg_link_;
	};
	using thread_t= libjmmcg::ppd::wrapper<mutator, libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading, context>;

	/**
	 *		The thread that spins waiting for messages from the exchange to forward to the connected client.
	 */
	thread_t exchg_to_client_thread;
};

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::exchg_link_wrapper(ctor_args&& ctor_args) noexcept(false)
	: exchg_link_(std::move(std::get<5>(ctor_args))),
	  exchange_to_client_(
		  std::get<0>(ctor_args).get(),
		  std::get<1>(ctor_args),
		  std::get<2>(ctor_args),
		  std::get<3>(ctor_args),
		  std::get<4>(ctor_args).get(),
		  exchg_link_) {
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline std::string
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::to_string() const noexcept(false) {
	std::ostringstream os;
	os << "exchange connection: " << exchg_link_ << ", exchange-to-client details: " << exchange_to_client_.to_string();
	return os.str();
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchCxnCtorArg1, class ExchCxnCtorArg2, class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgLinks) == sizeof...(ExchCxnCtorArgs), bool>::type>
inline exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArg1&& exchange_cxn_arg1, ExchCxnCtorArg2&& exchange_cxn_arg2, ExchCxnCtorArgs&&... exchange_cxn_args)
	: exchg_links_(
		typename exchg_link_wrapper_itf::template ctor_args<ExchCxnCtorArg1>{
			e,
			report_error,
			cpu_core,
			cpu_priority,
			timestamps,
			exchange_cxn_arg1},
		typename exchg_link_wrapper_itf::template ctor_args<ExchCxnCtorArg2>{
			e,
			report_error,
			cpu_core,
			cpu_priority,
			timestamps,
			exchange_cxn_arg2},
		typename exchg_link_wrapper_itf::template ctor_args<ExchCxnCtorArgs>{
			e,
			report_error,
			cpu_core,
			cpu_priority,
			timestamps,
			exchange_cxn_args}...) {
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchCxnCtorArgs, std::size_t... Indices, typename std::enable_if<(sizeof...(ExchgLinks) + 2) == sizeof...(Indices), bool>::type>
inline exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArgs&& exchange_cxn_args, std::index_sequence<Indices...>)
	: exchange_to_client_processor(e, report_error, cpu_core, cpu_priority, timestamps, std::get<Indices>(exchange_cxn_args)...) {
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline bool
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::are_all_logged_on() const noexcept(true) {
	return exchg_links_.accumulate(
		true,
		[](auto const& v, auto const& rhs) {
			return v && rhs.exchg_link().is_logged_on();
		});
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline void
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchange_to_client_processor::client_connected(client_connection_t client_connection) noexcept(true) {
	exchg_links_.generate([client_connection](auto& v) {
		v.flow().client_connected(client_connection);
	});
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline bool
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::read_and_process_a_msg(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn.socket().template read<typename client_to_exchg_proc_rules_t::src_msg_details_t>(buff)(src_cxn, buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		auto const mic= MIC_as_str_to_type(buff);
		return exchg_links_[mic].exchg_link().read_and_process_a_msg(buff, src_cxn.socket(), dest_cxn.socket());
	} else {
		return true;
	}
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline bool
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::read_and_process_a_msg(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn.socket().template read<typename client_to_exchg_proc_rules_t::src_msg_details_t>(buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		auto const mic= MIC_as_str_to_type(buff);
		return exchg_links_[mic].exchg_link().read_and_process_a_msg(buff, src_cxn.socket(), ts);
	} else {
		return true;
	}
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline bool
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::read_and_process_a_msg(client_connection_t& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false) {
	// TODO consider putting this into shared-memory...
	ALIGN_TO_L1_CACHE client_msg_buffer_t buff;
	if(!src_cxn->template read<typename client_to_exchg_proc_rules_t::src_msg_details_t>(buff)) {
		const libjmmcg::latency_timestamps_itf::period t(ts);
		auto const mic= MIC_as_str_to_type(buff);
		return exchg_links_[mic].exchg_link().read_and_process_a_msg(buff, src_cxn, dest_cxn.socket());
	} else {
		return true;
	}
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline std::string
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::to_string() const noexcept(false) {
	std::ostringstream os;
	os << "exchange links: " << exchg_links_;
	return os.str();
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline std::string_view
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::find(client_msg_buffer_t const& msg) noexcept(true) {
	auto const& hdr= reinterpret_cast<typename src_msg_details_t::Header_t const&>(msg);
	DEBUG_ASSERT(hdr.is_valid());
	auto const target_exchange= hdr.find_ExDestination();
	return target_exchange;
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
inline mic_codes::ISO_10383_MIC_Codes
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::MIC_as_str_to_type(client_msg_buffer_t const& msg) noexcept(true) {
	auto const MIC_as_str= find(msg);
	DEBUG_ASSERT(!MIC_as_str.empty());
	DEBUG_ASSERT(MIC_as_str.size() == sizeof(mic_codes::mic_t));
	auto const& mic_str= reinterpret_cast<mic_codes::mic_t const&>(*MIC_as_str.data());
	auto const mic= libjmmcg::enum_tags::convert<mic_codes::ISO_10383_MIC_Codes>(mic_str);
	return mic;
}

template<class... ExchgLinks>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_processor<ExchgLinks...> const& s) noexcept(false) {
	os << s.to_string();
	return os;
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::flow_t ::flow_t(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, exchg_link_t& exchg_link) noexcept(true)
	: exchg_to_client_thread(context(cpu_core, cpu_priority), e, exchg_link, timestamps, report_error) {
	exchg_to_client_thread.create_running();
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::flow_t ::~flow_t() noexcept(false) {
	exchg_to_client_thread->exit_requested_.test_and_set(std::memory_order_seq_cst);
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline void
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::flow_t ::client_connected(client_connection_t client_connection) noexcept(true) {
	exchg_to_client_thread->client_connected(client_connection);
}

template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
template<class ExchgLinkT>
inline std::string
exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...>::exchg_link_wrapper<ExchgLinkT>::flow_t ::to_string() const noexcept(false) {
	std::ostringstream os;
	os << ", exchange-to-client core=" << common::thread_traits::exchange_to_client_thread << ", client link: " << *exchg_to_client_thread;
	return os.str();
}

}}}}
