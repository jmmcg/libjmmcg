<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
	
	The MIC codes should be downloaded from  https://www.iso20022.org/sites/default/files/ISO10383_MIC/
-->

<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:param name="FILTER_DOC" required="yes" as="xs:string"/>
<xsl:param name="fname_hdr" required="yes" as="xs:string"/>
<xsl:output omit-xml-declaration="yes" />
<xsl:template name="get-letters">
	<xsl:param name="input"/>
	<xsl:if test="string-length($input)">
		<xsl:text>'</xsl:text>
		<xsl:value-of select="substring($input, 1, 1)"/>
		<xsl:text>'</xsl:text>
		<xsl:if test="string-length($input)>1">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input" select="substring($input, 2)"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>
<xsl:template match="/">
<xsl:variable name="header_guard" select="upper-case(replace(tokenize($fname_hdr, '/')[last()], '[-\./]', '_'))"/>
#ifndef ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>
#define ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of  select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso20022.org/market-identifier-codes"/>
-->

#include &quot;common/isimud_config.h&quot;

#include &quot;core/enum_as_char_array.hpp&quot;

#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>array<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>cstdint<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>iostream<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tuple<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

#define ISIMUD_ISO_10383_MIC_XML_GENERATED_DATE "<xsl:value-of select="/dataroot/@generated"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common { namespace mic_codes {

inline constexpr const std::size_t number_of_mics=<xsl:value-of select="count(/dataroot/ISO10383_MIC[ISO_x0020_COUNTRY_x0020_CODE_x0020__x0028_ISO_x0020_3166_x0029_=document($FILTER_DOC)/countries/country/@alpha-2])"/>;

/// ISO 10383: MIC Codes, with the value of the tag having the MIC encoded in the tag-value.
enum class ISO_10383_MIC_Codes : std::uint32_t {
	<xsl:for-each select="/dataroot/ISO10383_MIC[ISO_x0020_COUNTRY_x0020_CODE_x0020__x0028_ISO_x0020_3166_x0029_=document($FILTER_DOC)/countries/country/@alpha-2]">
		/// COUNTRY=<xsl:value-of select="MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION"/>, MIC=<xsl:value-of select="MIC"/>.&nl;
		<xsl:if test="string-length(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION)>0">
			ISO_10383_<xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(MIC, '[^a-zA-Z0-9]', '_')"/>=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
			<xsl:call-template name="get-letters">
				<xsl:with-param name="input"><xsl:value-of select="MIC"/></xsl:with-param>
			</xsl:call-template><xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value,&nl;
		</xsl:if>
	</xsl:for-each>
};

struct hash {
	using element_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_10383_MIC_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
	static_assert(sizeof(element_type)<xsl:text disable-output-escaping="yes">&lt;</xsl:text>=sizeof(std::size_t));

	constexpr std::size_t operator()(ISO_10383_MIC_Codes const v) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v);
	}
	constexpr std::size_t operator()(ISO_10383_MIC_Codes const v, std::size_t const r) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v)^r;
	}
};

inline bool
operator==(ISO_10383_MIC_Codes const lhs, char const * const rhs) noexcept(true) {
	using base_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_10383_MIC_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
// May not be NULL-terminated:	DEBUG_ASSERT(std::strlen(rhs)<xsl:text disable-output-escaping="yes">&gt;</xsl:text>=sizeof(base_type));
	return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(lhs)==*reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type const *<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(rhs);
}

using mic_t=std::array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	char,
	sizeof(std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_10383_MIC_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type)
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

/// A function to extract the ISO Currency-Code as a character string.
template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_10383_MIC_Codes MICCode<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>mic_code_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>MICCode<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>mic_code_str;
	return os;
}

/// Insert the ISO Currency-Code, as a character string, into the specified stream.
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
operator<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os, ISO_10383_MIC_Codes MICCode) noexcept(false) {
	mic_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text>str=reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>mic_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text>(MICCode);
	for (auto c : str) {
		os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>c;
	}
	return os;
}

namespace private_ {

<xsl:for-each select="/dataroot/ISO10383_MIC[ISO_x0020_COUNTRY_x0020_CODE_x0020__x0028_ISO_x0020_3166_x0029_=document($FILTER_DOC)/countries/country/@alpha-2]">
	<xsl:if test="string-length(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION)>0">
struct mic_details_<xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(MIC, '[^a-zA-Z0-9]', '_')"/> {
	static inline constexpr const ISO_10383_MIC_Codes mic_code=ISO_10383_MIC_Codes::ISO_10383_<xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(MIC, '[^a-zA-Z0-9_,. ]', '_')"/>;
	static inline constexpr const char country_name[]={
		<xsl:choose>
			<xsl:when test="string-length(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char mic[]={
		<xsl:choose>
			<xsl:when test="string-length(MIC)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(MIC, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char operating_mic[]={
		<xsl:choose>
			<xsl:when test="string-length(OPERATING_x0020_MIC)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(OPERATING_x0020_MIC, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char institution_description[]={
		<xsl:choose>
			<xsl:when test="string-length(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char acronym[]={
		<xsl:choose>
			<xsl:when test="string-length(ACRONYM)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(ACRONYM, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char city[]={
		<xsl:choose>
			<xsl:when test="string-length(CITY)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(CITY, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char website[]={
		<xsl:choose>
			<xsl:when test="string-length(WEBSITE)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(WEBSITE, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char update_date[]={
		<xsl:choose>
			<xsl:when test="string-length(LAST_x0020_UPDATE_x0020_DATE)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(LAST_x0020_UPDATE_x0020_DATE, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char validation_date[]={
		<xsl:choose>
			<xsl:when test="string-length(LAST_x0020_VALIDATION_x0020_DATE)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(LAST_x0020_VALIDATION_x0020_DATE, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char status[]={
		<xsl:choose>
			<xsl:when test="string-length(STATUS)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(STATUS, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char creation_date[]={
		<xsl:choose>
			<xsl:when test="string-length(CREATION_x0020_DATE)>0">
			<xsl:call-template name="get-letters">
				<xsl:with-param name="input"><xsl:value-of select="replace(CREATION_x0020_DATE, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
			</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};
	static inline constexpr const char comments[]={
		<xsl:choose>
			<xsl:when test="string-length(COMMENTS)>0">
				<xsl:call-template name="get-letters">
					<xsl:with-param name="input"><xsl:value-of select="replace(COMMENTS, '[^a-zA-Z0-9_,. ]', '')"/></xsl:with-param>
				</xsl:call-template>,
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>'\0'
	};

	static std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
	to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
		os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>"country name: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>country_name
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", MIC: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>mic
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", operating MIC: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>operating_mic
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", institution description: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>institution_description;
		if constexpr (sizeof(acronym)<xsl:text disable-output-escaping="yes">&gt;</xsl:text>0 <xsl:text disable-output-escaping="yes">&amp;</xsl:text><xsl:text disable-output-escaping="yes">&amp;</xsl:text> acronym[0]!='\0') {
			os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", acronym: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>acronym;
		}
		os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", city: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>city
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", website: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>website
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", update date: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>update_date
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", validation date: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>validation_date
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", status: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>status
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", creation date: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>creation_date;
		if constexpr (sizeof(comments)<xsl:text disable-output-escaping="yes">&gt;</xsl:text>0) {
			os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", comments: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>comments;
		}
		return os;
	}
};

	</xsl:if>
</xsl:for-each>

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct mic_to_stream;

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class... MICDetails<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct mic_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>MICDetails...<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text> {
	template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_10383_MIC_Codes MIC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	static constexpr void
	result(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
		(
			[](std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
				if constexpr (MICDetails::mic_code==MIC) {
					MICDetails::to_stream(os);
				}
			}(os),
			...
		);
	}
};

}

/// A collection of the MIC-codes <xsl:text disable-output-escaping="yes">&amp;</xsl:text> their related details.
using collection=std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:for-each select="/dataroot/ISO10383_MIC[ISO_x0020_COUNTRY_x0020_CODE_x0020__x0028_ISO_x0020_3166_x0029_=document($FILTER_DOC)/countries/country/@alpha-2]">
		<xsl:if test="string-length(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION)>0">
	private_::mic_details_<xsl:value-of select="replace(MARKET_x0020_NAME-INSTITUTION_x0020_DESCRIPTION, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(MIC, '[^a-zA-Z0-9]', '_')"/><xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>&nl;
		</xsl:if>
	</xsl:for-each>
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

using mic_to_stream=private_::mic_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>collection<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

inline std::string
to_string() noexcept(false) {
	return std::string("Built with MICs generated on: '" ISIMUD_ISO_10383_MIC_XML_GENERATED_DATE "'.");
}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
