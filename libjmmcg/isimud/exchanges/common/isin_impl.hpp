/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

constexpr inline
ISIN_t::ISIN_t() noexcept(true)
: value{} {
}

constexpr inline
ISIN_t::ISIN_t(block_t const &b) noexcept(true) {
	libjmmcg::memcpy_opt(b, value.block);
}

constexpr inline
ISIN_t::ISIN_t(ISIN_t const &i) noexcept(true) {
	libjmmcg::memcpy_opt(i.value.block, value.block);
}

template<std::size_t Sz>
constexpr inline
ISIN_t::ISIN_t(char const (&b)[Sz]) noexcept(true) {
	BOOST_MPL_ASSERT_RELATION(sizeof(block_t), ==, Sz-1);
	libjmmcg::memcpy_opt(reinterpret_cast<block_t const &>(b), value.block);
}

constexpr inline ISIN_t&
ISIN_t::operator=(ISIN_t const &i) noexcept(true) {
	libjmmcg::memcpy_opt(i.value.block, value.block);
	return *this;
}

inline bool
ISIN_t::operator==(ISIN_t const &i) const noexcept(true) {
	return value.block==i.value.block;
}

inline bool
ISIN_t::operator<(ISIN_t const &i) const noexcept(true) {
	static_assert(sizeof(ISIN_t)<=sizeof(libjmmcg::uint128_t));
	union converter {
		static_assert(sizeof(libjmmcg::uint128_t)>=sizeof(ISIN_t));
		enum : std::size_t {
			conv_sz=sizeof(libjmmcg::uint128_t),
			isin_sz=sizeof(ISIN_t),
			num_chars_to_mask=(conv_sz-isin_sz)*8
		};

		libjmmcg::uint128_t conv;
		ISIN_t isin;

		explicit constexpr converter(ISIN_t const &i) noexcept(true)
		: isin(i) {
		}
	};
	constexpr const libjmmcg::uint128_t mask=(~libjmmcg::uint128_t{})>>(converter::num_chars_to_mask);
	converter cl(*this);
	cl.conv&=mask;
	DEBUG_ASSERT(cl.isin==*this);
	converter cr(i);
	cr.conv&=mask;
	DEBUG_ASSERT(cr.isin==i);
	return cl.conv<cr.conv;
}

inline std::string
ISIN_t::to_string() const noexcept(false) {
	return std::string{value.as_chars.begin(), value.as_chars.end()};
}

inline std::size_t
ISIN_t::hash() const noexcept(true) {
	union {
		std::array<std::size_t, (sizeof(details_t) + sizeof(std::size_t)) / sizeof(std::size_t)> as_size_t{};
		converter_t orig;
	} conv{};
	libjmmcg::memcpy_opt(value.block, conv.orig.block);
	std::size_t hash_value=libjmmcg::accumulate(conv.as_size_t, [](auto const &l, auto const &r) {return l^r;});
	DEBUG_ASSERT(hash_value!=0);
	return hash_value;
}

inline std::ostream &
operator<<(std::ostream &os, ISIN_t const &i) noexcept(false) {
	os<<i.value.as_chars;
	return os;
}

inline std::istream &
operator>>(std::istream &is, ISIN_t &i) noexcept(false) {
	std::string tmp;
	is>>tmp;
	DEBUG_ASSERT(tmp.size()<=sizeof(ISIN_t));
	libjmmcg::memcpy_opt(reinterpret_cast<ISIN_t::block_t const &>(*tmp.data()), i.value.block);
	return is;
}

} } } }
