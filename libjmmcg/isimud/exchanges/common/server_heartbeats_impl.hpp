/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<class MsgT, unsigned MissedHbs, unsigned HbInterval>
template<class ClientCxn, class MakeHB>
inline server_hb_t<MsgT, MissedHbs, HbInterval>::server_hb_t(ClientCxn& cxn, report_error_fn_t& report_error, MakeHB&& make_hb)
	: make_hb_msg(std::move(make_hb)),
	  report_error_(report_error),
	  heartbeat_sender_thr(
		  typename thread_t::thread_traits::thread_name_t{"heartbeats" LIBJMMCG_ENQUOTE(__LINE__)},
		  thread_traits::heartbeat_thread.priority,
		  typename thread_t::thread_traits::api_params_type::processor_mask_type(thread_traits::heartbeat_thread.core),
		  [this, &cxn](std::atomic_flag& exit_requested) {
			  try {
				  const hb_t hb{make_hb_msg()};
				  while(!exit_requested.test(std::memory_order_relaxed)) [[likely]] {
					  std::this_thread::sleep_for(heartbeat_interval);
					  if(!exit_requested.test(std::memory_order_relaxed)) [[likely]] {
						  cxn.write(hb);
					  }
				  }
			  } catch(...) {
				  if(!exit_requested.test(std::memory_order_relaxed)) {
					  report_error_(to_string() + error_message, std::current_exception());
				  }
			  }
			  exit_requested.test_and_set(std::memory_order_seq_cst);
			  exit_requested.notify_all();
		  }) {
}

template<class MsgT, unsigned MissedHbs, unsigned HbInterval>
inline std::string
server_hb_t<MsgT, MissedHbs, HbInterval>::to_string() const noexcept(false) {
	return fmt::format("max missed heartbeats={}, heartbeat interval={}, heartbeat core={}", max_missed_heartbeats, heartbeat_interval, thread_traits::heartbeat_thread.to_string());
}

}}}}
