#ifndef ISIMUD_EXCHANGES_COMMON_PROCESSING_RULES_HPP
#define ISIMUD_EXCHANGES_COMMON_PROCESSING_RULES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_type.hpp"

#include "core/msm.hpp"

#include <boost/mpl/front.hpp>
#include <boost/mpl/pop_front.hpp>

#include <map>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// A simple, MIT-protocol exchange simulator.
/**
	The behaviour of this simulator is a simplification derived from the specification in [1].
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class message_responses {
public:
	/// A type containing the details of the messages that will be received from the client.
	using src_msg_details_t= SrcMsgDetails;
	/// A type containing the details of the messages that will be sent to the exchange.
	using dest_msg_details_t= DestMsgDetails;
	using flow_msg_types= typename src_msg_details_t::client_to_exchange_messages_t;
	using socket_t= SktT;

protected:
	template<class EndStates>
	struct no_op {
		using end_states= EndStates;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		template<class... Args>
		constexpr explicit no_op(Args&&...) noexcept(true) {}

		template<auto state, auto next, class... Args>
		REALLY_FORCE_INLINE constexpr end_states
		process(Args&&...) const noexcept(true) {
			return next;
		}
	};
	/// Just default-construct the specified message-type and send it to the exchange socket.
	template<class OutMsg>
	struct just_send_to_exchg {
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;
		using dest_msg_t= OutMsg;

		template<class... Args>
		explicit constexpr just_send_to_exchg(Args&&...) noexcept(true) {}

		/**
			\param exchg_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const&, socket_t& exchg_skt, socket_t&) const {
			exchg_skt.write(dest_msg_t());
			return next;
		}

		/**
			\param exchg_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const&, socket_t& exchg_skt, socket_type::client_cxn_ptr_t&) const {
			exchg_skt.write(dest_msg_t());
			return next;
		}

		template<auto state, auto next>
		constexpr end_states
		process() const noexcept(true)
			= delete;
	};
	/// Construct the specified output message-type from the input message and send it to the client socket.
	template<class InMsg, class OutMsg>
	struct convert_then_send {
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;
		using src_msg_t= InMsg;
		using dest_msg_t= OutMsg;

		template<class... Args>
		explicit constexpr convert_then_send(Args&&...) noexcept(true) {}

		/**
			\param buff	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const {
			auto const& msg= reinterpret_cast<src_msg_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			client_skt.write(dest_msg_t(msg));
			return next;
		}
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& skt, socket_type::client_cxn_ptr_t& client_cxn) const {
			DEBUG_ASSERT(client_cxn);
			return process<state, next>(buff, skt, client_cxn->socket());
		}
	};
	/// Construct the specified output message-type from the input message plus reference data and send it to the client socket.
	template<class InMsg, class OutMsg>
	class convert_then_send_ref_data {
	public:
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;
		using src_msg_t= InMsg;
		using dest_msg_t= OutMsg;
		using isin_mapping_data_const_reference= typename dest_msg_details_t::isin_mapping_data_const_reference;

		explicit convert_then_send_ref_data(isin_mapping_data_const_reference rd) noexcept(true)
			: ref_data_(rd) {
			DEBUG_ASSERT(!ref_data_.empty());
		}
		constexpr convert_then_send_ref_data(convert_then_send_ref_data const& v) noexcept(true)
			: ref_data_(v.ref_data_) {
			DEBUG_ASSERT(!ref_data_.empty());
		}

		/**
		 *			\param buff	The message that was received, that shall be processed.
		 *			\param client_skt	The socket to which any responses should be written.
		 *			\return The next state from the declaration of the table.
		 */
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const {
			DEBUG_ASSERT(!ref_data_.empty());
			auto const& msg= reinterpret_cast<src_msg_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			client_skt.write(dest_msg_t(msg, ref_data_));
			return next;
		}
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_type::client_cxn_ptr_t& client_cxn) const {
			DEBUG_ASSERT(client_cxn);
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		isin_mapping_data_const_reference ref_data_;
	};
	/// Construct the specified output message-type from the input message plus reference data and send it to the client socket.
	template<class InMsg, class OutMsg, bool ToClient= true>
	class convert_then_send_seq_num {
	public:
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;
		using src_msg_t= InMsg;
		using dest_msg_t= OutMsg;
		using isin_mapping_data_const_reference= typename dest_msg_details_t::isin_mapping_data_const_reference;

		explicit convert_then_send_seq_num(typename dest_msg_details_t::SeqNum_t& sq) noexcept(true)
			: sequenceNumber(sq) {}
		template<class MSM>
		explicit convert_then_send_seq_num(std::tuple<typename dest_msg_details_t::SeqNum_t&, MSM> a) noexcept(true)
			: sequenceNumber(std::get<0>(a)) {}
		constexpr convert_then_send_seq_num(convert_then_send_seq_num const& v) noexcept(true)
			: sequenceNumber(v.sequenceNumber) {}

		/**
			\param buff	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt) const {
			auto const& msg= reinterpret_cast<src_msg_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			if constexpr(ToClient) {
				client_skt.write(dest_msg_t(msg, sequenceNumber));
			} else {
				auto seq_num= static_cast<typename src_msg_details_t::SeqNum_t>(sequenceNumber);
				exchg_skt.write(dest_msg_t(seq_num));
				sequenceNumber= seq_num;
			}
			return next;
		}
		/**
			\param buff	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_type::client_cxn_ptr_t& client_cxn) const {
			auto const& msg= reinterpret_cast<src_msg_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			if constexpr(ToClient) {
				DEBUG_ASSERT(client_cxn);
				client_cxn->socket().write(dest_msg_t(msg, sequenceNumber));
			} else {
				auto seq_num= static_cast<typename src_msg_details_t::SeqNum_t>(sequenceNumber);
				exchg_skt.write(dest_msg_t(seq_num));
				sequenceNumber= seq_num;
			}
			return next;
		}

	private:
		typename dest_msg_details_t::SeqNum_t& sequenceNumber;
	};
	template<class OutMsg, bool ToClient, class EndStates>
	struct send_specified_msg {
		using end_states= EndStates;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;
		using dest_msg_t= OutMsg;

		template<class... Args>
		explicit constexpr send_specified_msg(Args&&...) noexcept(true) {}

		/**
			\param client_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const&, socket_t& exchg_skt, socket_t& client_skt) const {
			if constexpr(ToClient) {
				// \todo Need to get the sequence number from the failed message for FIX.
				client_skt.write(dest_msg_t(typename dest_msg_t::SeqNum_t{}, dest_msg_t::unknown_msg, "Unknown message rejected."));
			} else {
				// \todo What about BATSBOE? The MIT messages seem not to give a stuff about sequence numbers...
				exchg_skt.write(dest_msg_t(typename dest_msg_t::SeqNum_t{}, dest_msg_t::unknown_msg, "Unknown message rejected."));
			}
			return next;
		}
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const&, socket_t& exchg_skt, socket_type::client_cxn_ptr_t& client_cxn) const {
			// \todo Need to get the sequence number from the failed message for FIX.
			// \todo What about BATSBOE? The MIT messages seem not to give a stuff about sequence numbers...
			if constexpr(ToClient) {
				DEBUG_ASSERT(client_cxn);
				client_cxn->socket().write(dest_msg_t(typename dest_msg_t::SeqNum_t{}, dest_msg_t::unknown_msg, "Unknown message rejected."));
			} else {
				exchg_skt.write(dest_msg_t(typename dest_msg_t::SeqNum_t{}, dest_msg_t::unknown_msg, "Unknown message rejected."));
			}
			return next;
		}
	};
	template<class MSM>
	class match_all_response {
	public:
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_type::client_cxn_ptr_t&> > >;

		explicit constexpr match_all_response(MSM const& msm) noexcept(true)
			: business_msm(msm) {}
		explicit constexpr match_all_response(std::tuple<typename dest_msg_details_t::SeqNum_t&, MSM> a) noexcept(true)
			: business_msm(std::get<1>(a)) {}
		constexpr match_all_response(match_all_response const&) noexcept(true)= default;

		/**
			\return False to continue processing messages, true otherwise.
		*/
		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt) const noexcept(false) {
			auto const& hdr= reinterpret_cast<typename src_msg_details_t::Header_t const&>(buff);
			const auto next_from_business= business_msm.process(hdr.type(), buff, exchg_skt, client_skt);
			return next_from_business;
		}
		/**
			\return False to continue processing messages, true otherwise.
		*/
		template<auto state, auto next>
		end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			// We have a message for a client. But the connection info has not got to us yet, so wait for it. We are guaranteed to get a client, because only messages from clients can cause messages to be sent to clients. (Unless there is an order that has been cancelled by a business reject & that was held overnight and cancelled before a client connected. >24hr holding periods for HFT are unknown, so ignore this eventuality for now...)
			while(!client_cxn) {
				std::this_thread::yield();
			}
			DEBUG_ASSERT(client_cxn);
			DEBUG_ASSERT(client_cxn->socket().is_open());
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		MSM const& business_msm;
	};

	message_responses()= default;
	virtual ~message_responses()= default;
};

/// A simple, MIT-protocol exchange simulator.
/**
	This server is single-threaded, and permits only one client connection at a time.
	The behaviour of this simulator is a simplification derived from the specification in [1].
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
template<class MsgDetails, class SktT>
class simulator_responses : public message_responses<MsgDetails, MsgDetails, SktT> {
public:
	using base_t= message_responses<MsgDetails, MsgDetails, SktT>;
	using msg_details_t= typename base_t::src_msg_details_t;
	using src_msg_details_t= msg_details_t;
	using socket_t= typename base_t::socket_t;
	using base_t::convert_then_send;
	using base_t::convert_then_send_ref_data;
	using base_t::send_specified_msg;
	using Price_t= typename std::conditional<std::is_integral<typename msg_details_t::Price_t>::value, typename msg_details_t::Price_t, std::uint64_t>::type;

	/// The only valid username.
	static inline constexpr const typename msg_details_t::UserName_t username{"usr"};
	/// The only valid password.
	static inline constexpr const typename msg_details_t::Password_t password{"fubar"};
	/// The only valid new password.
	static inline constexpr const typename msg_details_t::Password_t new_password{"snafu"};
	/// The magic quantity value.
	/**
		A value less than this will result in a full-filled ExecutionReport, otherwise a partial fill ExecutionReport will be returned, and the remaining quantity left "on the market".
	*/
	static inline constexpr const typename msg_details_t::SeqNum_t quantity_limit= 100;
	/// For limit orders, this is the price at which the order will be traded, otherwise it will be left "on the market".
	static inline constexpr const Price_t price= 42;
	/// For limit orders, this is the native, scaled price at which the order will be traded, otherwise it will be left "on the market", which is used within the simulator.
	static inline constexpr const Price_t scaled_price= 42 * msg_details_t::implied_decimal_places;
	BOOST_MPL_ASSERT_RELATION(scaled_price, >, 0);

	[[nodiscard]] virtual std::string to_string() const noexcept(false);

protected:
	/**
	 * A totally trivial order book for the simulator, to keep the code "self-evidently right".
	 *
	 * \see order_book
	 */
	using order_book_t= std::map<typename msg_details_t::ClientOrderID_t, typename msg_details_t::NewOrder_t>;
	order_book_t order_book;
	typename src_msg_details_t::SeqNum_t sequenceNumber{};
};

template<class MsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, simulator_responses<MsgDetails, SktT> const& ec) noexcept(false);

}}}}

#include "processing_rules_impl.hpp"

#endif
