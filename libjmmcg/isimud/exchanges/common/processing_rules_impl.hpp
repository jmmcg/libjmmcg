/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<class MsgDetails, class SktT> inline std::string
simulator_responses<MsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream os;
	os
		<<"\n\tUsername: '"<<username<<"'"
		"\n\tPassword: '"<<password<<"'"
		"\n\tNew password: '"<<new_password<<"'"
		"\n\tQuantity limit="<<quantity_limit
		<<"\n\tPrice="<<price
		<<"\n\tScaled price="<<scaled_price
		<<"\n\tCurrent sequence number="<<sequenceNumber
		<<"\n\tMessage details: ";
	MsgDetails::to_stream(os);
	return os.str();
}

template<class MsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, simulator_responses<MsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

} } } }
