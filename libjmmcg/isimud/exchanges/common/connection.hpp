#ifndef ISIMUD_EXCHANGES_COMMON_CONNECTION_HPP
#define ISIMUD_EXCHANGES_COMMON_CONNECTION_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "socket_type.hpp"

#include "core/latency_timestamps.hpp"
#include "core/msg_processor.hpp"

#include <boost/mpl/end.hpp>
#include <boost/mpl/find.hpp>
#include <boost/program_options.hpp>

#include <memory>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// A class that wraps a link to an exchange.
template<class MsgDetails, class ConnPol>
class LIBJMMCG_JMMCG_EXPORT connection {
public:
	/// A type containing the details of the messages that will be sent to and received from the exchange.
	using msg_details_t= MsgDetails;
	/// The connection policy that should be used to connect to the specified exchange.
	using conn_pol_t= ConnPol;
	using ctor_args= conn_pol_t;
	using skt_mgr_t= socket_type::skt_mgr_t;
	using socket_t= skt_mgr_t::socket_t;
	using socket_priority= skt_mgr_t::socket_priority;

	const conn_pol_t conn_pol_;

	/// Connect to the exchange using the connection policy specified.
	/**
		\param	conn_pol	The specified connection policy, that includes details of the exchange to which the link should be made.
	*/
	connection(conn_pol_t const& conn_pol, skt_mgr_t::socket_priority priority, std::size_t incoming_cpu);

	/// Send a message to the specified, connected, exchange.
	/**
		\param	msg	The client-side message to send to the connected exchange.
	*/
	template<
		class Msg>
	typename std::enable_if<
		!std::is_same<
			typename boost::mpl::find<typename msg_details_t::client_to_exchange_messages_t, Msg>::type,
			typename boost::mpl::end<typename msg_details_t::client_to_exchange_messages_t>::type>::value,
		void>::type
	send(Msg const& msg);

	/// Receive a message from the specified, connected, exchange.
	/**
		\param	msg	The particular type of client-side message to receive from the connected exchange. We must know the message that should be received, as specified by the protocol.
	*/
	template<
		class Msg>
	[[nodiscard]] typename std::enable_if<
		!std::is_same<
			typename boost::mpl::find<typename msg_details_t::exchange_to_client_messages_t, Msg>::type,
			typename boost::mpl::end<typename msg_details_t::exchange_to_client_messages_t>::type>::value,
		bool>::type
	receive(Msg& msg);

	void stop();

	skt_mgr_t::socket_t& socket() noexcept(true) {
		return link.socket();
	}

	template<class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return msg_details_t::template make_ctor_args<conn_pol_t, socket_t, ThrdT>(vm);
	}

	std::string to_string() const noexcept(false);

private:
	skt_mgr_t link;
};

template<class MsgDetails, class ConnPol>
inline std::ostream&
operator<<(std::ostream& os, connection<MsgDetails, ConnPol> const& ec) noexcept(false);

template<class ClientMsgBuffT, class ClientSktT>
class connection_processor_itf {
public:
	using client_msg_buffer_t= ClientMsgBuffT;

	virtual bool is_logged_on() const noexcept(true)= 0;
	virtual bool read_and_process_a_msg(client_msg_buffer_t const& buff, ClientSktT& dest_skt, libjmmcg::latency_timestamps_itf& ts) noexcept(false)= 0;
	virtual std::string to_string() const noexcept(false)= 0;

protected:
	connection_processor_itf()= default;
	~connection_processor_itf()= default;
};

/// A class that wraps a link from an exchange to a client.
template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
class connection_processor final : public connection_processor_itf<
												  typename libjmmcg::socket::msg_processor<ClientToExchangeProcessingRules>::proc_rules_t::src_msg_details_t::msg_buffer_t,
												  typename connection<typename libjmmcg::socket::msg_processor<ExchgToClientProcessingRules>::proc_rules_t::src_msg_details_t, ConnPol>::socket_t> {
public:
	using base_t= connection_processor_itf<
		typename libjmmcg::socket::msg_processor<ClientToExchangeProcessingRules>::proc_rules_t::src_msg_details_t::msg_buffer_t,
		typename connection<typename libjmmcg::socket::msg_processor<ExchgToClientProcessingRules>::proc_rules_t::src_msg_details_t, ConnPol>::socket_t>;
	using client_to_exchg_msg_processor_t= libjmmcg::socket::msg_processor<ClientToExchangeProcessingRules>;
	using client_to_exchg_proc_rules_t= typename client_to_exchg_msg_processor_t::proc_rules_t;
	using client_msg_buffer_t= typename client_to_exchg_proc_rules_t::src_msg_details_t::msg_buffer_t;
	using exchg_to_client_msg_processor_t= libjmmcg::socket::msg_processor<ExchgToClientProcessingRules>;
	using exchg_to_client_proc_rules_t= typename exchg_to_client_msg_processor_t::proc_rules_t;
	using exchg_msg_details_t= typename exchg_to_client_proc_rules_t::src_msg_details_t;
	using exchg_msg_buffer_t= typename exchg_msg_details_t::msg_buffer_t;
	using exchg_connection_t= connection<exchg_msg_details_t, ConnPol>;
	using conn_pol_t= typename exchg_connection_t::conn_pol_t;
	using socket_t= typename exchg_connection_t::socket_t;
	using socket_priority= typename exchg_connection_t::socket_priority;
	using ctor_args= std::tuple<
		typename exchg_connection_t::ctor_args,
		socket_priority,
		std::size_t,
		std::shared_ptr<typename exchg_msg_details_t::ref_data> >;

	explicit connection_processor(ctor_args const& details) noexcept(false);
	~connection_processor() noexcept(false);

	template<class ClientCxn>
	bool read_and_process_a_msg(exchg_msg_buffer_t const& buff, ClientCxn& dest_link) noexcept(false);
	template<class ClientCxn>
	bool read_and_process_a_msg(exchg_msg_buffer_t const& buff, ClientCxn& dest_link, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
	template<class ClientCxn>
	bool read_and_process_a_msg(ClientCxn& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
	bool read_and_process_a_msg(client_msg_buffer_t const& buff, socket_t& dest_skt, libjmmcg::latency_timestamps_itf& ts) noexcept(false) override;
	template<class Cxn>
	bool read_and_process_a_msg(Cxn& src_cxn, Cxn& dest_cxn) noexcept(false);

	socket_t& socket() noexcept(true) {
		DEBUG_ASSERT(cxn_.socket().is_open());
		return cxn_.socket();
	}

	void stop();

	bool is_logged_on() const noexcept(true) override {
		return logged_on_.test(std::memory_order_relaxed);
	}

	template<class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return exchg_connection_t::template make_ctor_args<ThrdT>(vm);
	}

	std::string to_string() const noexcept(false) override;

private:
	std::atomic_flag logged_on_{};
	std::shared_ptr<typename exchg_msg_details_t::ref_data> ref_data;
	client_to_exchg_msg_processor_t client_to_exchg_processor;
	exchg_to_client_msg_processor_t exchg_to_client_processor;
	/**
	 * This wraps the socket connected to the exchange.
	 */
	exchg_connection_t cxn_;
};

template<class ClientToExchangeProcessingRules, class ExchgToClientProcessingRules, class ConnPol>
inline std::ostream&
operator<<(std::ostream& os, connection_processor<ClientToExchangeProcessingRules, ExchgToClientProcessingRules, ConnPol> const& ec) noexcept(false);

}}}}

#include "connection_impl.hpp"

#endif
