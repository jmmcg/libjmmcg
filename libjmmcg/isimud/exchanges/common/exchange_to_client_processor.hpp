#ifndef ISIMUD_EXCHANGES_COMMON_EXCHANGE_TO_CLIENT_PROCESSOR_HPP
#define ISIMUD_EXCHANGES_COMMON_EXCHANGE_TO_CLIENT_PROCESSOR_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/socket_server_manager.hpp"
#include "core/thread_wrapper.hpp"
#include "core/ttypes.hpp"
#include "core/unordered_tuple.hpp"

#include <boost/exception_ptr.hpp>
#include <boost/mpl/assert.hpp>

#include <atomic>
#include <exception>
#include <functional>
#include <string>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<class... ExchgLinks>
class exchange_to_client_processor;

/// A specialisation of the processor for a connection to a single exchange.
template<class ExchgLink>
class exchange_to_client_processor<ExchgLink> final {
public:
	using exchg_link_t= ExchgLink;	///< The connection to the exchange. This can be specified as there is only one.
	using exchg_to_client_proc_rules_t= typename exchg_link_t::exchg_to_client_proc_rules_t;
	using client_to_exchg_proc_rules_t= typename exchg_link_t::client_to_exchg_proc_rules_t;
	using src_msg_details_t= typename client_to_exchg_proc_rules_t::src_msg_details_t;
	using socket_t= typename exchg_link_t::socket_t;
	/**
	 * We are assuming the same library is used for the client & the exchange connection...
	 */
	using session= typename libjmmcg::socket::server_manager::manager<socket_t>::session;
	using client_connection_t= typename session::ptr_type;
	/// If the current processor detects a failure (via an exception thrown) when trying to send the message to the connected client, it will use an instance of this call-back so that this error may be alternatively handled.
	/**
	 * Note that a reference to this operation is taken, therefore it should be re-entrant, i.e. thread-safe. Moreover the operation must not leak any exceptions otherwise the behaviour is undefined.
	 *
	 * \param exchg_link_details	The details of the exchange from which the message originated.
	 * \param client_cxn_ptr	An opaque smart-pointer to any client connected to that exchange, if there is one, otherwise default constructed.
	 * \param eptr	Any exception-pointer that was caught in the process of attempting to send the message from the exchange to the connected client.
	 */
	using report_error_fn_t= std::function<void(std::string exchg_link_details, client_connection_t client_cxn_ptr, std::exception_ptr eptr)>;

	template<class ExchCxnCtorArg>
	exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArg&& exchange_cxn_arg);

	bool are_all_logged_on() const noexcept(true);
	void client_connected(client_connection_t client_connection) noexcept(true);
	std::string to_string() const noexcept(false);

	bool read_and_process_a_msg(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
	bool read_and_process_a_msg(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);

private:
	enum class target_exchange_state {
		exists,
		no_exdest,
		unsupported_exchange
	};
	/**
	 * This class manages sending messages received from the exchange, translating them and returning them to the connected client. Naturally this is done on a separate, single thread.
	 */
	class flow_t;
	using client_msg_buffer_t= typename src_msg_details_t::msg_buffer_t;

	exchg_link_t exchg_link;
	flow_t exchange_to_client;

	bool read_and_process_a_msg(client_connection_t& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
};

/// A specialisation for a processor that connects to two or more exchanges.
/**
 * All of the exchange connections must process the same type of messages from the connected client, otherwise it is an error and unsupported. Whenever a client message arrives, if it has a target MIC (e.g. and ExDest field if it were in FIX format), it must be from the set defined by this object, otherwise it is undefined behaviour.
 *
 * One might be concerned about all the polymorphism: flow_itf, exchg_link_wrapper_itf and exchanges::common::connection_processor_itf but research wit Vladimir Arnost demonstrated that in all versions of clang and gcc they managed to de-virtualise the calls, with clang even inlining suitable functions! (We suspect it is because the use of templates requires that the class is fully declared at the point of instantiation, so there is no run-time polymorphism at all.)
 *
 *  \todo Check that the rest all use that same type otherwise the link has been mis-declared.
 */
template<class ExchgLink1, class ExchgLink2, class... ExchgLinks>
class exchange_to_client_processor<ExchgLink1, ExchgLink2, ExchgLinks...> final {
private:
	using exchg_link_t= ExchgLink1;	 ///< We use the first exchange connection to extract the type of the sources messages and type of socket to which any clients should connect.
	using exchg_to_client_proc_rules_t= typename exchg_link_t::exchg_to_client_proc_rules_t;
	using client_to_exchg_proc_rules_t= typename exchg_link_t::client_to_exchg_proc_rules_t;

public:
	using src_msg_details_t= typename client_to_exchg_proc_rules_t::src_msg_details_t;
	using socket_t= typename exchg_link_t::socket_t;
	/**
	 * We are assuming the same library is used for the client & the exchange connection...
	 */
	using session= typename libjmmcg::socket::server_manager::manager<socket_t>::session;
	using client_connection_t= typename session::ptr_type;
	/// If the current processor detects a failure (via an exception thrown) when trying to send the message to the connected client, it will use an instance of this call-back so that this error may be alternatively handled.
	/**
	 * Note that a reference to this operation is taken, therefore it should be re-entrant, i.e. thread-safe. Moreover the operation must not leak any exceptions otherwise the behaviour is undefined.
	 *
	 * \param exchg_link_details	The details of the exchange from which the message originated.
	 * \param client_cxn_ptr	An opaque smart-pointer to any client connected to that exchange, if there is one, otherwise default constructed.
	 * \param eptr	Any exception-pointer that was caught in the process of attempting to send the message from the exchange to the connected client.
	 */
	using report_error_fn_t= std::function<void(std::string exchg_link_details, client_connection_t client_cxn_ptr, std::exception_ptr eptr)>;

	template<class ExchCxnCtorArg1, class ExchCxnCtorArg2, class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgLinks) == sizeof...(ExchCxnCtorArgs), bool>::type= true>
	exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArg1&& exchange_cxn_arg1, ExchCxnCtorArg2&& exchange_cxn_arg2, ExchCxnCtorArgs&&... exchange_cxn_args);
	template<class ExchCxnCtorArgs, std::size_t... Indices, typename std::enable_if<(sizeof...(ExchgLinks) + 2) == sizeof...(Indices), bool>::type= true>
	exchange_to_client_processor(std::atomic_flag& e, report_error_fn_t& report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf& timestamps, ExchCxnCtorArgs&& exchange_cxn_args, std::index_sequence<Indices...>);

	bool are_all_logged_on() const noexcept(true);
	void client_connected(client_connection_t client_connection) noexcept(true);
	std::string to_string() const noexcept(false);

	bool read_and_process_a_msg(session& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
	bool read_and_process_a_msg(session& src_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);

private:
	enum class target_exchange_state {
		exists,
		no_exdest,
		unsupported_exchange
	};
	/// The buffer into which the incoming messages are received.
	/**
	 *		This should be per-thread, on the stack of the thread, suitably aligned, possibly to 64-bits. For maximum performance it should be a reference to the internal buffer of the network stack or even the hardware buffer in the card (assuming suitable kernel modules).
	 *		It is not recommended to use GCC <=v4.9.* due to sub-optimal code-generation in std::memcpy() (see the performance graphs of libjmmcg::stack_string.
	 */
	using client_msg_buffer_t= typename src_msg_details_t::msg_buffer_t;
	class flow_itf {
	public:
		virtual void client_connected(client_connection_t client_connection) noexcept(true)= 0;
		virtual std::string to_string() const noexcept(false)= 0;

	protected:
		flow_itf()= default;
		~flow_itf()= default;
	};
	class exchg_link_wrapper_itf {
	public:
		using exchg_link_itf_t= exchanges::common::connection_processor_itf<client_msg_buffer_t, socket_t>;
		template<class ExchgCtorArg>
		using ctor_args= std::tuple<
			std::reference_wrapper<std::atomic_flag>,
			report_error_fn_t,
			std::size_t,
			thread_traits::api_threading_traits::api_params_type::priority_type,
			std::reference_wrapper<libjmmcg::latency_timestamps_itf>,
			ExchgCtorArg>;

		virtual exchg_link_itf_t const& exchg_link() const noexcept(true)= 0;
		virtual exchg_link_itf_t& exchg_link() noexcept(true)= 0;
		virtual flow_itf const& flow() const noexcept(true)= 0;
		virtual flow_itf& flow() noexcept(true)= 0;
		virtual std::string to_string() const noexcept(false)= 0;

	protected:
		exchg_link_wrapper_itf()= default;
		~exchg_link_wrapper_itf()= default;
	};
	template<class ExchgLinkT>
	class exchg_link_wrapper final : public exchg_link_wrapper_itf {
	public:
		using base_t= exchg_link_wrapper_itf;
		using exchg_link_t= ExchgLinkT;	 ///< The connection to the exchange.
		using exchg_to_client_proc_rules_t= typename exchg_link_t::exchg_to_client_proc_rules_t;
		using ctor_args= typename base_t::template ctor_args<typename exchg_link_t::ctor_args>;
		/**
		 * This class manages sending messages received from the exchange, translating them and returning them to the connected client. Naturally this is done on a separate, single thread.
		 */
		class flow_t;

		explicit exchg_link_wrapper(ctor_args&& ctor_args) noexcept(false);

		exchg_link_t const& exchg_link() const noexcept(true) override {
			return exchg_link_;
		}
		exchg_link_t& exchg_link() noexcept(true) override {
			return exchg_link_;
		}
		flow_t const& flow() const noexcept(true) override {
			return exchange_to_client_;
		}
		flow_t& flow() noexcept(true) override {
			return exchange_to_client_;
		}
		std::string to_string() const noexcept(false) override;

	private:
		exchg_link_t exchg_link_;
		flow_t exchange_to_client_;
	};
	template<class ExchgLinkT>
	struct extract {
		static inline constexpr const typename exchanges::common::mic_codes::ISO_10383_MIC_Codes value= ExchgLinkT::exchg_to_client_proc_rules_t::src_msg_details_t::MIC_code;
	};

	using perfect_hash_t= libjmmcg::template perfect_hash<
		static_cast<unsigned>(extract<ExchgLink1>::value),
		static_cast<unsigned>(extract<ExchgLink2>::value),
		static_cast<unsigned>(extract<ExchgLinks>::value)...>;

public:
	using exchg_links_t= libjmmcg::unordered_tuple<
		exchanges::common::mic_codes::ISO_10383_MIC_Codes,
		exchg_link_wrapper_itf,
		perfect_hash_t,
		extract,
		exchg_link_wrapper<ExchgLink1>,
		exchg_link_wrapper<ExchgLink2>,
		exchg_link_wrapper<ExchgLinks>...>;

private:
	exchg_links_t exchg_links_;

	bool read_and_process_a_msg(client_connection_t& src_cxn, session& dest_cxn, libjmmcg::latency_timestamps_itf& ts) noexcept(false);
	static std::string_view find(client_msg_buffer_t const& msg) noexcept(true);
	static mic_codes::ISO_10383_MIC_Codes MIC_as_str_to_type(client_msg_buffer_t const& msg) noexcept(true);
};

template<class... ExchgLinks>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_processor<ExchgLinks...> const& s) noexcept(false);

}}}}

#include "exchange_to_client_processor_impl.hpp"

#endif
