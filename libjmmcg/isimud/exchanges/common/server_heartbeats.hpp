#ifndef ISIMUD_EXCHANGES_COMMON_SERVER_HEARTBEATS_HPP
#define ISIMUD_EXCHANGES_COMMON_SERVER_HEARTBEATS_HPP

/******************************************************************************
** Copyright © 2018 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_traits.hpp"

#include "core/function.hpp"
#include "core/jthread.hpp"

#include <boost/exception_ptr.hpp>
#include <fmt/chrono.h>

#include <atomic>
#include <chrono>
#include <functional>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// Section 5.2.2 "Heartbeats" of [1]. Generate heartbeats from the containing simulator.
/**
	The simulator generates heartbeats to which the client responds.
*/
template<class MsgT, unsigned MissedHbs, unsigned HbInterval>
class server_hb_t {
public:
	/// The specific heartbeat message type.
	using hb_t= MsgT;
	/// If the current processor detects a failure (via an exception thrown) when attempting to send a heart-beat, it will use an instance of this call-back so that this error may be alternatively handled.
	/**
	 * Note that a reference to this operation is taken, therefore it should be re-entrant, i.e. thread-safe. Moreover the operation must not leak any exceptions otherwise the behaviour is undefined.
	 *
	 * \param svr_hb_details	The details of the server from which the error originated.
	 * \param eptr	Any exception-pointer that was caught in the process of sending the heartbeats.
	 */
	using report_error_fn_t= std::function<void(std::string svr_hb_details, std::exception_ptr eptr)>;

	static inline constexpr unsigned max_missed_heartbeats= MissedHbs;
	static inline constexpr const std::chrono::seconds heartbeat_interval{HbInterval};
	static inline constexpr char error_message[]= " Heartbeasts to the exchange will stop. The exchange WILL disconnect after the criteria fail.";

	template<class ClientCxn, class MakeHB>
	[[nodiscard]] server_hb_t(ClientCxn& cxn, report_error_fn_t& report_error, MakeHB&& make_hb);

	std::string to_string() const noexcept(false);

	friend std::ostream& operator<<(std::ostream& os, server_hb_t const& s) noexcept(false) {
		os << s.to_string();
		return os;
	}

private:
	using thread_t= libjmmcg::ppd::jthread<libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading>;

	const libjmmcg::function<hb_t(), 1> make_hb_msg;
	report_error_fn_t& report_error_;
	thread_t heartbeat_sender_thr;
};

}}}}

#include "server_heartbeats_impl.hpp"

#endif
