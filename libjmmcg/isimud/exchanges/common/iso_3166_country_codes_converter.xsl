<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:param name="FILTER_DOC" required="yes" as="xs:string"/>
<xsl:param name="fname_hdr" required="yes" as="xs:string"/>
<xsl:output omit-xml-declaration="yes" />
<xsl:template name="replace-string">
	<xsl:param name="text"/>
	<xsl:param name="replace"/>
	<xsl:param name="with"/>
	<xsl:choose>
		<xsl:when test="contains($text,$replace)">
			<xsl:value-of select="substring-before($text,$replace)"/>
			<xsl:value-of select="$with"/>
			<xsl:call-template name="replace-string">
				<xsl:with-param name="text" select="substring-after($text,$replace)"/>
				<xsl:with-param name="replace" select="$replace"/>
				<xsl:with-param name="with" select="$with"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$text"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template name="get-letters">
	<xsl:param name="input"/>
	<xsl:choose>
		<xsl:when test="string-length($input)">
			static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>char<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(<xsl:text>L'</xsl:text>
			<xsl:call-template name="replace-string">
				<xsl:with-param name="text" select="substring($input, 1, 1)"/>
				<xsl:with-param name="replace" select='"&apos;"' />
				<xsl:with-param name="with" select='"\&apos;"'/>
			</xsl:call-template>
			<xsl:text>'</xsl:text>),
			<xsl:call-template name="get-letters">
				<xsl:with-param name="input" select="substring($input, 2)"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			'\0'
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="/">
<xsl:variable name="header_guard" select="upper-case(replace(tokenize($fname_hdr, '/')[last()], '[-\./]', '_'))"/>
#ifndef ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>
#define ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of  select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso.org/iso-3166-country-codes.html"/>
	- <a href="https://www.iso.org/publication/PUB500001.html"/>
-->

#include &quot;common/isimud_config.h&quot;

#include &quot;core/enum_as_char_array.hpp&quot;

#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>array<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>cstdint<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>iostream<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tuple<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

#define ISIMUD_ISO_3166_COUNTRY_CODES_XML_GENERATED_DATE "<xsl:value-of  select="current-dateTime()"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common { namespace ctry_codes {

namespace alpha_2 {

/// ISO 3166: Country Codes, with the value of the tag having the currency encoded in the tag-value.
enum class ISO_3166_Country_Codes : std::uint16_t {
	<xsl:for-each select="/countries/country[./@alpha-2=document($FILTER_DOC)/countries/country/@alpha-2]">
		/// name=<xsl:value-of select="@name"/>, alpha-2=<xsl:value-of select="@alpha-2"/>, alpha-3=<xsl:value-of select="@alpha-3"/>, country-code=<xsl:value-of select="@country-code"/>, iso_3166-2=<xsl:value-of select="@iso_3166-2"/>, region=<xsl:value-of select="@region"/>, sub-region=<xsl:value-of select="@sub-region"/>, region-code=<xsl:value-of select="@region-code"/>, sub-region-code=<xsl:value-of select="@sub-region-code"/>.&nl;
		ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@alpha-2"/></xsl:with-param>
		</xsl:call-template><xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value,&nl;
	</xsl:for-each>
};

struct hash {
	using element_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
	static_assert(sizeof(element_type)<xsl:text disable-output-escaping="yes">&lt;</xsl:text>=sizeof(std::size_t));

	constexpr std::size_t operator()(ISO_3166_Country_Codes const v) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v);
	}
	constexpr std::size_t operator()(ISO_3166_Country_Codes const v, std::size_t const r) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v)^r;
	}
};

inline bool
operator==(ISO_3166_Country_Codes const lhs, char const * const rhs) noexcept(true) {
	using base_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
// May not be NULL-terminated:	DEBUG_ASSERT(std::strlen(rhs)<xsl:text disable-output-escaping="yes">&gt;</xsl:text>=sizeof(base_type));
	return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(lhs)==*reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type const *<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(rhs);
}

using country_t=std::array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	char,
	sizeof(std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type)
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

/// A function to extract the ISO Country-Code as a character string.
template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>cc_code_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>cc_code_str;
	return os;
}

/// Insert the ISO Country-Code, as a character string, into the specified stream.
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
operator<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os, ISO_3166_Country_Codes CC) noexcept(false) {
	country_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text>str=reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>country_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text>(CC);
	for (auto c : str) {
		os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>c;
	}
	return os;
}

}

namespace alpha_3 {

/// ISO 3166: Country Codes, with the value of the tag having the currency encoded in the tag-value.
enum class ISO_3166_Country_Codes : std::uint32_t {
	<xsl:for-each select="/countries/country[./@alpha-2=document($FILTER_DOC)/countries/country/@alpha-2]">
		/// name=<xsl:value-of select="@name"/>, alpha-2=<xsl:value-of select="@alpha-2"/>, alpha-3=<xsl:value-of select="@alpha-3"/>, country-code=<xsl:value-of select="@country-code"/>, iso_3166-2=<xsl:value-of select="@iso_3166-2"/>, region=<xsl:value-of select="@region"/>, sub-region=<xsl:value-of select="@sub-region"/>, region-code=<xsl:value-of select="@region-code"/>, sub-region-code=<xsl:value-of select="@sub-region-code"/>.&nl;
		ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@alpha-3"/></xsl:with-param>
		</xsl:call-template><xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value,&nl;
	</xsl:for-each>
};

using country_t=std::array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	char,
	sizeof(std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type)-1
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

/// A function to extract the ISO Country-Code as a character string.
template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>cc_code_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>cc_code_str;
	return os;
}

}

namespace ctry_code {

/// ISO 3166: Country Codes, with the value of the tag having the currency encoded in the tag-value.
enum class ISO_3166_Country_Codes : std::uint32_t {
	<xsl:for-each select="/countries/country[./@alpha-2=document($FILTER_DOC)/countries/country/@alpha-2]">
		/// name=<xsl:value-of select="@name"/>, alpha-2=<xsl:value-of select="@alpha-2"/>, alpha-3=<xsl:value-of select="@alpha-3"/>, country-code=<xsl:value-of select="@country-code"/>, iso_3166-2=<xsl:value-of select="@iso_3166-2"/>, region=<xsl:value-of select="@region"/>, sub-region=<xsl:value-of select="@sub-region"/>, region-code=<xsl:value-of select="@region-code"/>, sub-region-code=<xsl:value-of select="@sub-region-code"/>.&nl;
		ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>=<xsl:value-of select="number(@country-code)"/>,&nl;
	</xsl:for-each>
};

using country_t=std::array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	char,
	sizeof(std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type)-1
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

/// A function to extract the ISO Country-Code as a character string.
template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_3166_Country_Codes CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>cc_code_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CC<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>cc_code_str;
	return os;
}

}

namespace private_ {

<xsl:for-each select="/countries/country[./@alpha-2=document($FILTER_DOC)/countries/country/@alpha-2]">
struct ctry_details_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/> {
	static inline constexpr const alpha_2::ISO_3166_Country_Codes alpha2=alpha_2::ISO_3166_Country_Codes::ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>;
	static inline constexpr const alpha_3::ISO_3166_Country_Codes alpha3=alpha_3::ISO_3166_Country_Codes::ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>;
	static inline constexpr const ctry_code::ISO_3166_Country_Codes country_code=ctry_code::ISO_3166_Country_Codes::ISO_3166_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/>;
	static inline constexpr const char name[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@name"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char alpha_2[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@alpha-2"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char alpha_3[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@alpha-3"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char country_code_str[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@country-code"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char iso_3166_2[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@iso_3166-2"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char region[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@region"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char sub_region[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@sub-region"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char region_code[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@region-code"/></xsl:with-param>
		</xsl:call-template>
	};
	static inline constexpr const char sub_region_code[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="@sub-region-code"/></xsl:with-param>
		</xsl:call-template>
	};

	static std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
	to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
		os
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>"name: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>name
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", alpha 2: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>alpha_2
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", alpha 3: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>alpha_3
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", country code: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>country_code_str
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", iso 3166 2 "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>iso_3166_2
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", region: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>region
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", sub region: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>sub_region
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", region code: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>region_code
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", sub region code: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>sub_region_code;
		return os;
	}
};

</xsl:for-each>

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct ctry_to_stream;

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class... CtryDetails<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct ctry_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CtryDetails...<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text> {
	template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ctry_code::ISO_3166_Country_Codes Ctry<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	static constexpr void
	result(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
		(
			[](std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
				if constexpr (CtryDetails::country_code==Ctry) {
					CtryDetails::to_stream(os);
				}
			}(os),
			...
		);
	}
};

}

/// A collection of the country-codes <xsl:text disable-output-escaping="yes">&amp;</xsl:text> their related details.
using collection=std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:for-each select="/countries/country[./@alpha-2=document($FILTER_DOC)/countries/country/@alpha-2]">
	private_::ctry_details_<xsl:value-of select="replace(@region, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(@name, '[^a-zA-Z0-9]', '_')"/><xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>&nl;
	</xsl:for-each>
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

using ctry_to_stream=private_::ctry_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>collection<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

inline std::string
to_string() noexcept(false) {
	return std::string("Built with country-codes generated on: '" ISIMUD_ISO_3166_COUNTRY_CODES_XML_GENERATED_DATE "'.");
}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
