<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:param name="FILTER_DOC" required="yes" as="xs:string"/>
<xsl:param name="fname_hdr" required="yes" as="xs:string"/>
<xsl:param name="ctry_codes_hdr" required="yes" as="xs:string"/>
<xsl:output omit-xml-declaration="yes" />
<xsl:template name="get-letters">
	<xsl:param name="input"/>
	<xsl:if test="string-length($input)">
		<xsl:text>'</xsl:text>
		<xsl:value-of select="substring($input, 1, 1)"/>
		<xsl:text>'</xsl:text>
		<xsl:if test="string-length($input)>1">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input" select="substring($input, 2)"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>
<xsl:template match="/">
<xsl:variable name="header_guard" select="upper-case(replace(tokenize($fname_hdr, '/')[last()], '[-\./]', '_'))"/>
#ifndef ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>
#define ISIMUD_EXCHANGES_COMMON_<xsl:value-of select="$header_guard"/>

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of  select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.currency-iso.org/dam/downloads/lists/list_one.xml"/>
	- <a href="https://www.iso.org/iso-4217-currency-codes.html"/>
	- <a href="https://www.currency-iso.org/en/home/amendments/secretariat.html"/>
-->

#include &quot;common/isimud_config.h&quot;

#include &quot;core/enum_as_char_array.hpp&quot;

#include &quot;<xsl:value-of select="$ctry_codes_hdr"/>&quot;

#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>array<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>cstdint<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>limits<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>iostream<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tuple<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

#define ISIMUD_ISO_4217_CURRENCY_CODES_XML_GENERATED_DATE "<xsl:value-of select="/ISO_4217/@Pblshd"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common { namespace ccy_codes {

/// The value used to represent invalid currencies or monetary units.
inline constexpr const std::size_t invalid_CcyMnrUnts=std::numeric_limits<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::size_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::max();

/// ISO 4217: Currency Codes, with the value of the tag having the currency encoded in the tag-value.
enum class ISO_4217_Currency_Codes : std::uint32_t {
	<xsl:for-each select="/ISO_4217/CcyTbl/CcyNtry[substring(Ccy, 1, 2)=document($FILTER_DOC)/countries/country/@alpha-2]">
		/// CcyNbr=<xsl:value-of select="CcyNbr"/>, CcyMnrUnts=<xsl:value-of select="CcyMnrUnts"/>.&nl;
		<xsl:if test="string-length(Ccy)>0">
			ISO_4217_<xsl:value-of select="replace(CtryNm, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(CcyNm, '[^a-zA-Z0-9]', '_')"/>=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
			<xsl:call-template name="get-letters">
				<xsl:with-param name="input"><xsl:value-of select="Ccy"/></xsl:with-param>
			</xsl:call-template>,'\0'<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value,&nl;
		</xsl:if>
	</xsl:for-each>
};

struct hash {
	using element_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
	static_assert(sizeof(element_type)<xsl:text disable-output-escaping="yes">&lt;</xsl:text>=sizeof(std::size_t));

	constexpr std::size_t operator()(ISO_4217_Currency_Codes const v) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v);
	}
	constexpr std::size_t operator()(ISO_4217_Currency_Codes const v, std::size_t const r) const noexcept(true) {
		return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>element_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(v)^r;
	}
};

inline bool
operator==(ISO_4217_Currency_Codes const lhs, char const * const rhs) noexcept(true) {
	using base_type=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
// May not be NULL-terminated:	DEBUG_ASSERT(std::strlen(rhs)<xsl:text disable-output-escaping="yes">&gt;</xsl:text>=sizeof(base_type));
	return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(lhs)==*reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>base_type const *<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(rhs);
}

using currency_t=std::array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	char,
	sizeof(std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type)
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

/// A function to extract the ISO Currency-Code as a character string.
template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes CCYCode<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>ccy_code_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CCYCode<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>ccy_code_str;
	return os;
}

/// Insert the ISO Currency-Code, as a character string, into the specified stream.
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
operator<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os, ISO_4217_Currency_Codes CCYCode) noexcept(false) {
	currency_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text>str=reinterpret_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>currency_t const <xsl:text disable-output-escaping="yes">&amp;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text>(CCYCode);
	for (auto c : str) {
		os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>c;
	}
	return os;
}

/// Obtain the two-character ISO 3166 Country-Code from a ISO 4217 Currency-code.
inline constexpr ctry_codes::alpha_2::ISO_3166_Country_Codes
get_ctry_code(ISO_4217_Currency_Codes code) noexcept(true) {
	using ctry_ul_t=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ctry_codes::alpha_2::ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
	using ccy_ul_t=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;
	enum : std::size_t {
		shift=sizeof(ccy_ul_t)-sizeof(ctry_ul_t)
	};

	const auto ccy_ul=static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ccy_ul_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(code);
	const auto ctry_ul=static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ctry_ul_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(ccy_ul<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text>shift);
	return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ctry_codes::alpha_2::ISO_3166_Country_Codes<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(ctry_ul);
}

namespace private_ {

<xsl:for-each select="/ISO_4217/CcyTbl/CcyNtry[substring(Ccy, 1, 2)=document($FILTER_DOC)/countries/country/@alpha-2]">
	<xsl:if test="string-length(Ccy)>0">
struct ccy_details_<xsl:value-of select="replace(CtryNm, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(CcyNm, '[^a-zA-Z0-9]', '_')"/> {
	enum : std::size_t {
		ccy_nbr=<xsl:choose>
				<xsl:when test="CcyNbr!='NaN'">
					<xsl:value-of select="number(CcyNbr)"/>
				</xsl:when>
				<xsl:otherwise>
					invalid_CcyMnrUnts
				</xsl:otherwise>
			</xsl:choose>,
		ccy_mnr_unts=<xsl:choose>
				<xsl:when test="CcyMnrUnts!='N.A.'">
					<xsl:value-of select="number(CcyMnrUnts)"/>
				</xsl:when>
				<xsl:otherwise>
					invalid_CcyMnrUnts
				</xsl:otherwise>
			</xsl:choose>
	};
	static inline constexpr const ISO_4217_Currency_Codes ccy=ISO_4217_Currency_Codes::ISO_4217_<xsl:value-of select="replace(CtryNm, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(CcyNm, '[^a-zA-Z0-9]', '_')"/>;
	static inline constexpr const char country_name[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="replace(CtryNm, '[^a-zA-Z0-9]', '')"/></xsl:with-param>
		</xsl:call-template>,'\0'
	};
	static inline constexpr const char currency_name[]={
		<xsl:call-template name="get-letters">
			<xsl:with-param name="input"><xsl:value-of select="replace(CcyNm, '[^a-zA-Z0-9]', '')"/></xsl:with-param>
		</xsl:call-template>,'\0'
	};

	static std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
	to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
		os
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>"country name: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>country_name
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>", currency name: "<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>currency_name;
		return os;
	}
};

	</xsl:if>
</xsl:for-each>

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct ccy_to_stream;

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>class... CcyDetails<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
struct ccy_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>CcyDetails...<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text> {
	template<xsl:text disable-output-escaping="yes">&lt;</xsl:text>ISO_4217_Currency_Codes Ccy<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	static constexpr void
	result(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
		(
			[](std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) {
				if constexpr (CcyDetails::ccy==Ccy) {
					CcyDetails::to_stream(os);
				}
			}(os),
			...
		);
	}
};

}

/// A collection of the currency-codes <xsl:text disable-output-escaping="yes">&amp;</xsl:text> their related details.
using collection=std::tuple<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:for-each select="/ISO_4217/CcyTbl/CcyNtry[substring(Ccy, 1, 2)=document($FILTER_DOC)/countries/country/@alpha-2]">
		<xsl:if test="string-length(Ccy)>0">
	private_::ccy_details_<xsl:value-of select="replace(CtryNm, '[^a-zA-Z0-9]', '_')"/>_<xsl:value-of select="replace(CcyNm, '[^a-zA-Z0-9]', '_')"/><xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>&nl;
		</xsl:if>
	</xsl:for-each>
<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

using ccy_to_stream=private_::ccy_to_stream<xsl:text disable-output-escaping="yes">&lt;</xsl:text>collection<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

inline std::string
to_string() noexcept(false) {
	return std::string("Built with currency-codes created on: '" ISIMUD_ISO_4217_CURRENCY_CODES_XML_GENERATED_DATE "'.");
}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
