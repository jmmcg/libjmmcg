/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/application.hpp"
#include "core/exception.hpp"
#include "core/line_iterator.hpp"
#include "core/stream_header_guard.hpp"

#include <boost/algorithm/string/split.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>
#include <map>

using namespace libjmmcg;

using row_t= std::vector<std::string>;
using table_t= std::map<std::int32_t, row_t>;
using parsed_csv_t= std::tuple<std::string, table_t, row_t::size_type, row_t::size_type>;

/// The reject code must always be in the first column.
static inline constexpr const row_t::size_type code_index= 0;
/// The maximum number of columns in the converted CSV.
static inline constexpr const row_t::size_type max_columns= 5;
/// The name of the enum that is created with tags composed from the printable characters from the string relating to the reject-code.
static inline constexpr const char reject_codes_enum_str[]= "reject_codes_enum";

/// Some of the lines in the dratted CSV don't end with commas, so ensure that they do to allow the comma-recognition code to work correctly.
/**
	If a terminal comma is omitted, that last column will be read as empty.
*/
inline void
fixup_csv_line(std::string& line) noexcept(false) {
	if(*line.rbegin() != ',') {
		line.push_back(',');
	}
}

/// Some of the reject-codes are prefixed with zeroes, which in C++ signifies an octal number, strip those zeroes to ensure the reject-codes are correctly treated as decimal.
inline std::string
deoctalify(std::string const& code) noexcept(false) {
	std::string ret(code);
	while(ret.size() > 1 && ret.front() == '0') {
		ret.erase(ret.begin());
	}
	return ret;
}

/// Sometimes the reject-code has trailing non-printable characters before the comma-separator. Strip it.
inline std::string
strip_trailing_junk(std::string const& code) noexcept(false) {
	std::string ret(code);
	while(ret.size() > 1 && !std::isdigit(*ret.begin())) {
		ret.erase(std::next(ret.begin(), ret.size() - 1));
	}
	return ret;
}

/// Suitably clean up the reject-code.
inline std::string
make_digits(std::string const& code) noexcept(false) {
	return strip_trailing_junk(deoctalify(code));
}

/// Convert the scanned line from the input CSV-file into a collection of strings, one string per column.
/**
	Also replace non-printable ASCII characters with underscores.

	\todo Consider using libjmmcg::csv_iterator
*/
inline row_t
make_row(std::string line) noexcept(false) {
	fixup_csv_line(line);
	DEBUG_ASSERT(*line.rbegin() == ',');
	row_t row;
	row.reserve(max_columns);
	std::string::size_type beg= 0;
	std::string::size_type end= 0;
	while((end= line.find(",", beg)) != std::string::npos) {
		row_t::value_type cell(std::next(line.begin(), beg), std::next(line.begin(), end));
		std::transform(
			cell.begin(),
			cell.end(),
			cell.begin(),
			[](std::string::value_type v) {
				return std::isalnum(v) || std::isspace(v) || std::ispunct(v) ? v : '_';
			});
		row.emplace_back(std::move(cell));
		beg= end;
		if(beg >= line.size()) {
			break;
		}
		++beg;
	}
	return row;
}

inline std::string
remove_quotes(std::string const& s) noexcept(false) {
	DEBUG_ASSERT(!s.empty());
	auto is_quote= [](char v) {
		return v == '\'' || v == '\"';
	};
	return std::string(
		is_quote(s[0]) ? std::next(s.begin()) : s.begin(),
		is_quote(*std::next(s.begin(), s.size() - 1)) ? std::next(s.begin(), s.size() - 1) : s.end());
}

/// Read in and parse the specified CSV-file.
/**
	Note that any lines with non-digit reject-codes will be silently ignored.
	All FixGateway reject-codes are ignored (as they cause duplicates) & the links are binary-only.

	\param	fname	The suitably-qualified path to the suitably-formatted CSV file. It must be comma separated, with no embedded commas.
	\param	ver_headers	The string that specifies the headers for the version-table.
	\return	An associative collection of reject-code to details, the version string and column numbers of the required details in the rows.
*/
inline parsed_csv_t
read_csv(std::string const& fname, std::string const& version_headers, std::string const& end_of_version_headers, std::vector<std::string> const& reject_code_headers, std::string const& reject_column_name) noexcept(false) {
	DEBUG_ASSERT(!reject_code_headers.empty());
	DEBUG_ASSERT(reject_code_headers.size() <= max_columns);
	const std::string::size_type num_version_headers= std::count(version_headers.begin(), version_headers.end(), ',');
	DEBUG_ASSERT(num_version_headers > 1);
	std::unique_ptr<std::ifstream> reject_codes(new std::ifstream(fname));
	auto reject_codes_str= [](auto reject_code_headers) {
		std::ostringstream os;
		std::copy(
			reject_code_headers.begin(),
			reject_code_headers.end(),
			std::ostream_iterator<std::string>(os, ","));
		return os.str();
	};
	if(reject_codes->is_open()) {
		reject_codes.reset();
		std::string line;
		bool found_version_headers= false;
		{
			std::ifstream reject_codes(fname);
			line_iterator read_line(reject_codes), eof;
			while(read_line != eof) {
				if(read_line->find(version_headers) != std::string::npos) {
					found_version_headers= true;
					break;
				}
				++read_line;
			}
			line= *read_line;
		}
		if(!found_version_headers) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Failed to find the last of the version headers. Filename: '{}', version_headers: '{}', end_of_version_headers: '{}', reject_code_headers: '{}', reject_column_name: '{}'", fname, version_headers, end_of_version_headers, reject_codes_str(reject_code_headers), reject_column_name), &read_csv));
		}
		const auto get_version= [fname, version_headers, reject_code_headers, reject_column_name, end_of_version_headers, &reject_codes_str]() {
			const auto check_ver= [fname, version_headers, end_of_version_headers, reject_code_headers, reject_column_name, &reject_codes_str](auto const& version) {
				if(version.empty() || !std::isalnum(version[0])) {
					BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Failed to find the version headers - incorrectly empty or starts with a non-printing character, so invalid. Filename: '{}', version_headers: '{}', end_of_version_headers: '{}', reject_code_headers: '{}', reject_column_name: '{}', version: '{}'", fname, version_headers, end_of_version_headers, reject_codes_str(reject_code_headers), reject_column_name, version), &read_csv));
				}
			};
			std::ifstream reject_codes(fname);
			std::string version("UNKNOWN - error in processing csv.");
			line_iterator read_line(reject_codes), eof;
			while(read_line != eof) {
				if(read_line->find(end_of_version_headers) != std::string::npos) {
					check_ver(version);
					break;
				}
				if(!read_line->empty() && std::isalnum((*read_line)[0])) {
					// Just in case the 'end_of_version_headers' is missing, e.g. TRQ...
					version= *read_line;
				}
				++read_line;
			}
			return version;
		};
		const std::string version= get_version();
		bool found_reject_code_headers= false;
		{
			std::ifstream reject_codes(fname);
			line_iterator read_line(reject_codes), eof;
			while(read_line != eof) {
				if(read_line->find(reject_code_headers[0]) != std::string::npos
					&& read_line->find(reject_code_headers[1]) != std::string::npos
					&& read_line->find(reject_code_headers[2]) != std::string::npos
					&& read_line->find(reject_code_headers[3]) != std::string::npos) {
					found_reject_code_headers= true;
					break;
				}
				++read_line;
			}
			line= *read_line;
		}
		if(found_reject_code_headers) {
			std::ifstream reject_codes(fname);
			table_t table;
			row_t row(make_row(line));
			DEBUG_ASSERT(!row.empty());
			DEBUG_ASSERT(row.size() >= (max_columns - 1));
			[[maybe_unused]] auto const& length_column= std::find(row.begin(), row.end(), reject_code_headers[1]);
			DEBUG_ASSERT(length_column != row.end());
			auto const& reason_column= std::find(row.begin(), row.end(), reject_column_name);
			DEBUG_ASSERT(reason_column != row.end());
			const row_t::size_type reason_index= reason_column - row.begin();
			DEBUG_ASSERT(reason_index > code_index);
			DEBUG_ASSERT(reason_index < max_columns);
			auto const& process_column= std::find(row.begin(), row.end(), reject_code_headers[3]);
			DEBUG_ASSERT(process_column != row.end());
			const row_t::size_type process_index= process_column - row.begin();
			DEBUG_ASSERT(process_index >= reason_index);
			std::string last_line_scanned;
			line_iterator read_line(reject_codes), eof;
			while(read_line != eof) {
				row_t row(make_row(*read_line));
				if(row.size() > process_index) {
					if(!row[code_index].empty()
						&& !row[reason_index].empty()
						&& !row[process_index].empty()
						&& row[process_index].find("FixGateway") == std::string::npos
						&& row[process_index].find("FIX Gateway") == std::string::npos
						&& row[process_index].find("FIX Trading") == std::string::npos
						&& row[process_index].find("FIXTradingGateway") == std::string::npos
						&& row[process_index].find("FIX/FAST") == std::string::npos
						&& row[process_index].find("FAST Gateway") == std::string::npos
						&& row[process_index].find("RNS Feed") == std::string::npos) {
						try {
							row[code_index]= make_digits(row[code_index]);
							table.insert(std::make_pair(boost::lexical_cast<std::int32_t>(row[code_index]), row));
						} catch(std::exception const&) {
							// Silently ignore any rubbish in the input CSV....
						}
					}
				} else if(row.size() > num_version_headers && !row[code_index].empty() && !row[num_version_headers].empty()) {
					last_line_scanned= *read_line;
				}
				row.clear();
				++read_line;
			}
			DEBUG_ASSERT(!table.empty());
			return parsed_csv_t(version, table, reason_index, process_index);
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Failed to find the reject-code headers. Filename: '{}', version_headers: '{}', end_of_version_headers: '{}', reject_code_headers: '{}', reject_column_name: '{}'", fname, version_headers, end_of_version_headers, reject_codes_str(reject_code_headers), reject_column_name), &read_csv));
		}
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>(fmt::format("Failed to open the reject-codes CSV file. Filename: '{}', version_headers: '{}', end_of_version_headers: '{}', reject_code_headers: '{}', reject_column_name: '{}'", fname, version_headers, end_of_version_headers, reject_codes_str(reject_code_headers), reject_column_name), &read_csv));
	}
}

/// Replace the non-alpha-numeric characters in the input string into underscores.
inline std::string
ASCII_ify(std::string const& arg) noexcept(false) {
	std::string ret(arg);
	std::transform(
		ret.begin(),
		ret.end(),
		ret.begin(),
		[](char v) {
			return std::isalnum(v) ? v : '_';
		});
	return ret;
}

/// Construct a suitable enum-tag from the parsed row.
inline std::string
reject_code_to_enum_tag(table_t::const_iterator row, const row_t::size_type reason_index, const row_t::size_type process_index) noexcept(false) {
	std::string tag(ASCII_ify(row->second[reason_index]));
	std::string process(ASCII_ify(row->second[process_index]));
	return fmt::format("{}_{}_{}", std::move(process), row->second[code_index], std::move(tag));
}

/// Apply a specified operator to all but the penultimate element in the collection, to which a different operation should be applied.
/**
	Algorithmic complexity: O(n)
	\param	c	The input collection.
	\param	o	The functor to be applied to all but the penultimate element in the input collection.
	\param	t	The functor to be applied to the penultimate element in the input collection.
*/
template<class Colln, class Op, class Trail>
inline void
fill(Colln& c, Op const& o, Trail const& t) noexcept(false) {
	auto row= c.begin();
	while(row != std::prev(c.end())) {
		o.operator()(row);
		t.operator()(row);
		++row;
	}
	o.operator()(row);
}

inline void
write_reject_code_enum(table_t const& table, const row_t::size_type reason_index, const row_t::size_type process_index, std::ostream& os) noexcept(false) {
	DEBUG_ASSERT(!table.empty());
	os << "using RejectCodes_t=std::int32_t;\n"
			"enum class "
		<< reject_codes_enum_str << " : RejectCodes_t {\n"
											 "\ttag_SUCCESS=0,\n";
	fill(
		table,
		[&os, reason_index, process_index](table_t::const_iterator row) {
			if(row->second.size() > reason_index) {
				if(row->first != 0) {	// Zero is output separately.
					const row_t::value_type cell(reject_code_to_enum_tag(row, reason_index, process_index));
					os << "\t" << cell << "=" << row->first;
				}
			}
		},
		[&os](table_t::const_iterator row) {
			if(row->first != 0) {	// Zero is output separately.
				os << ",\n";
			}
		});
	os << "\n};\n";
}

inline void
write_string_table(table_t const& table, const row_t::size_type reason_index, std::ostream& os) noexcept(false) {
	DEBUG_ASSERT(!table.empty());
	os << "\tstatic constexpr char const * const reject_code_strs[]={\n"
			"\t\t\"SUCCESS\",\n";
	fill(
		table,
		[&os, reason_index](table_t::const_iterator row) {
			if(row->second.size() > reason_index) {
				if(row->first != 0) {	// Zero is output separately.
					os << "\t\t\"" << row->second[reason_index] << "\"";
				}
			}
		},
		[&os](table_t::const_iterator row) {
			if(row->first != 0) {	// Zero is output separately.
				os << ",\n";
			}
		});
	os << "\n\t};\n";
}

inline void
write_code_to_reason(table_t const& table, const row_t::size_type reason_index, const row_t::size_type process_index, std::ostream& os) noexcept(false) {
	os << "inline std::ostream &\n"
			"operator<<(std::ostream &os, const "
		<< reject_codes_enum_str << " code) noexcept(true) {\n";
	write_string_table(table, reason_index, os);
	DEBUG_ASSERT(!table.empty());
	unsigned int reject_code_strs_index= 0;
	os << "\tswitch (code) {\n"
			"\t\tcase "
		<< reject_codes_enum_str << "::tag_SUCCESS:\n"
											 "\t\t\tos<<reject_code_strs["
		<< reject_code_strs_index << "];\n"
											  "\t\t\treturn os;\n";
	++reject_code_strs_index;
	fill(
		table,
		[reason_index, process_index, &os, &reject_code_strs_index](table_t::const_iterator row) {
			if(row->second.size() > reason_index) {
				if(row->first != 0) {	// Zero is output separately.
					const row_t::value_type cell(reject_code_to_enum_tag(row, reason_index, process_index));
					os << "\t\tcase " << reject_codes_enum_str << "::" << cell << ":\n"
																									  "\t\t\tos<<reject_code_strs["
						<< reject_code_strs_index << "];\n"
															  "\t\t\treturn os;";
				}
			}
			++reject_code_strs_index;
		},
		[&os](table_t::const_iterator row) {
			if(row->first != 0) {	// Zero is output separately.
				os << "\n";
			}
		});
	os << "\n\t\tdefault:\n"
			"\t\t\tDEBUG_ASSERT(!\"Unknown reject code.\");\n"
			"\t\t\tos<<\"Unknown reject code.\";\n"
			"\t\t\treturn os;\n"
			"\t};\n"
			"}\n";
}

inline void
write_header(parsed_csv_t const& parsed_csv, std::string const& fname, std::string const& nm_space, std::string const& exe) noexcept(false) {
	struct header_guard_t : libjmmcg::header_guard_t {
		header_guard_t(std::string const& nm_space, std::string const& xls_version, std::string const& exe, std::string const& fname, std::ostream& os) noexcept(false)
			: libjmmcg::header_guard_t(exe, nm_space, fname, os) {
			os
				<< "// Auto-generated header file from auto-downloaded and converted XLS from the web.\n"
					"// Source XLS version info: '"
				<< xls_version << "'\n"
										"\n"
										"#include \"common/isimud_config.h\"\n"
										"\n"
										"#pragma GCC diagnostic push\n"
										"#pragma GCC diagnostic ignored \"-Wsign-compare\"\n"
										"#pragma GCC diagnostic ignored \"-Wold-style-cast\"\n"
										"\n"
										"#include <libassert/assert.hpp>\n"
										"\n"
										"#pragma GCC diagnostic pop\n"
										"\n"
										"#include <cstdint>\n"
										"#include <iostream>\n"
										"\n"
										"namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace "
				<< nm_space << " {\n"
									"\n";
		}

		~header_guard_t() noexcept(false) {
			os << "} } } } }\n";
		}
	};

	std::ofstream cpp_header(fname.c_str());
	const header_guard_t header_guard(nm_space, std::get<0>(parsed_csv), exe, fname, cpp_header);
	write_reject_code_enum(std::get<1>(parsed_csv), std::get<2>(parsed_csv), std::get<3>(parsed_csv), cpp_header);
	write_code_to_reason(std::get<1>(parsed_csv), std::get<2>(parsed_csv), std::get<3>(parsed_csv), cpp_header);
}

/// For MIT-based protocols, auto-generate a C++ function to convert the reject codes to strings. The mapping comes from the downloaded XLS that has been converted to a CSV file.
int
main(int argc, char const* const* argv) noexcept(true) {
	try {
		boost::program_options::options_description general(
			application::make_program_options(
				"A program to convert a table of exchange reject-codes in CSV-format to a C++ header-file." + application::make_help_message("consultant@isimud.ltd.uk")));
		boost::program_options::options_description prog_opts("Program options.");
		prog_opts.add_options()("reject_codes", boost::program_options::value<std::string>()->required(), "The file-path of the reject codes.")("cpp_header", boost::program_options::value<std::string>()->required(), "The file-path of the output C++ header-file.")("namespace", boost::program_options::value<std::string>()->required(), "The namespace into which the contents of the header-file should be placed.")("reject_code_headers", boost::program_options::value<std::string>()->required(), "The text that identifies the column-headings for the reject-codes.")("reject_column_name", boost::program_options::value<std::string>()->required(), "The text that identifies the column name of the reject-codes.")("version_headers", boost::program_options::value<std::string>()->required(), "The text that identifies the column-headings for the version information.")("end_of_version_headers", boost::program_options::value<std::string>()->required(), "The text that identifies the column-headings delimiting the end of the version information. Some of the MIT-based exchanges convert to CSVs that have more than one table, this one follows the reject-code translations.");
		boost::program_options::options_description all("All options.");
		all.add(general).add(prog_opts);
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(auto const exit_code= application::check_basic_options(vm, all, std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		}
		boost::program_options::notify(vm);

		std::vector<std::string> reject_codes_headers;
		std::string reject_code_headers_as_csv_str(remove_quotes(vm["reject_code_headers"].as<std::string>()));
		const parsed_csv_t parsed_csv(
			read_csv(
				vm["reject_codes"].as<std::string>(),
				remove_quotes(vm["version_headers"].as<std::string>()),
				remove_quotes(vm["end_of_version_headers"].as<std::string>()),
				boost::split(
					reject_codes_headers,
					reject_code_headers_as_csv_str,
					[](auto const& v) {
						return v == ',';
					}),
				remove_quotes(vm["reject_column_name"].as<std::string>())));
		write_header(
			parsed_csv,
			vm["cpp_header"].as<std::string>(),
			vm["namespace"].as<std::string>(),
			argv[0]);
		return exit_codes::codes::exit_success;
	} catch(...) {
		std::cerr << "Unknown exception. Details: " << boost::current_exception_diagnostic_information() << std::endl;
		return exit_codes::codes::exit_unknown_exception;
	}
	return exit_codes::codes::exit_unknown_failure;
}
