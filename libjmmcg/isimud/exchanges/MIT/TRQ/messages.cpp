/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace TRQ {

std::ostream &
MsgTypes::to_stream(std::ostream &os) noexcept(false) {
	os<<"MIC details: "<<exchanges::common::mic_codes::to_string()<<"\n";
	exchanges::common::mic_codes::mic_to_stream::result<MIC_code>(os);
	os
		<<", version: '"<<ISIMUD_MIT_TRQ_EXCHANGE_VERSION<<"'"
		<<", minimum size of client-to-exchange message="<<min_size_client_to_exchange_msg
		<<", maximum size of client-to-exchange message="<<max_size_client_to_exchange_msg
		<<", minimum size of exchange-to-client message="<<min_size_exchange_to_client_msg
		<<", maximum size of exchange-to-client message="<<max_size_exchange_to_client_msg
		<<", minimum message size="<<min_msg_size
		<<", maximum message size="<<max_msg_size
		<<", header size="<<header_t_size
		<<", implied decimal places="<<implied_decimal_places;
	return os;
}

} } } } }
