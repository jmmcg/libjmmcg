/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "trq_sim.hpp"

template class libjmmcg::socket::svr<
	libjmmcg::socket::server_manager::loopback<
		libisimud::exchanges::MIT::common::simulator_responses<libisimud::exchanges::MIT::TRQ::MsgTypes, libisimud::exchanges::common::socket_type::socket_t>,
		libisimud::exchanges::MIT::common::server_hb_t<libisimud::exchanges::MIT::TRQ::MsgTypes::ServerHeartbeat_t>,
		libisimud::exchanges::common::socket_type::socket_t> >;
