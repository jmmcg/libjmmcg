#ifndef ISIMUD_EXCHANGES_MIT_LSE_MESSAGES_HPP
#define ISIMUD_EXCHANGES_MIT_LSE_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "mit_lse_config.h"
#include "reject_codes.hpp"

#include "../common/messages.hpp"
#include "../common/ref_data.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

/**
	From <a href="https://www.londonstockexchange.com/products-and-services/technical-library/millennium-exchange-technical-specifications/mit203160615.pdf">"MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 11.6,
17 August 2015</a>.
*/
namespace LSE {

/**
	Section: "8.4.1 New Order"
*/
struct [[gnu::packed]] NewOrderSpecific1 {
	common::SecurityID_t instrumentID_;
	common::MESQualifier::element_type mesQualifier_= common::MESQualifier::NonMES;
	const std::int8_t reservedField1= 0;

	explicit constexpr NewOrderSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
	common::MESQualifier::element_type mesQualifier() const noexcept(true) {
		return mesQualifier_;
	}
	void mesQualifier(common::MESQualifier::element_type m) noexcept(true) {
		mesQualifier_= m;
	}
};
struct [[gnu::packed]] NewOrderSpecific2 {
	using order_qty_t= std::int32_t;

	order_qty_t orderQty_;
	order_qty_t displayQty;

	explicit constexpr NewOrderSpecific2(order_qty_t ordQty) noexcept(true)
		: orderQty_(ordQty), displayQty(ordQty) {
	}
	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}
};
struct [[gnu::packed]] NewOrderSpecific3 {
	common::Anonymity::element_type anonymity= common::Anonymity::Anonymous;
	common::Price_t stoppedPrice= 0;
	common::PassiveOnlyOrder::element_type passiveOnlyOrder_= common::PassiveOnlyOrder::NoConstraint;
	common::ReservedField9_t reservedField;
	int32_t minimumQuantity= 0;

	common::PassiveOnlyOrder::element_type passiveOnlyOrder() const noexcept(true) {
		return passiveOnlyOrder_;
	}
	void passiveOnlyOrder(common::PassiveOnlyOrder::element_type poo) noexcept(true) {
		passiveOnlyOrder_= poo;
	}
	static consteval common::OrderSource::element_type orderSource() noexcept(true) {
		return common::OrderSource::MarketParticipantDealsOnOwnAccount;
	}
	static void orderSource(common::OrderSource::element_type) noexcept(true) {
	}
};

/**
	Section: "8.4.2 New Quote"
*/
struct [[gnu::packed]] NewQuoteSpecific1 {
	common::Price_t bidPrice;
	int32_t bidSize;
	common::Price_t askPrice;
	int32_t askSize;
};
struct [[gnu::packed]] NewQuoteSpecific2 {
	common::ReservedField10_t reservedField;
};

/**
	Section: "8.4.3 Order Cancel Replace Request"
*/
struct [[gnu::packed]] OrderCancelReplaceRequestSpecific {
	common::PassiveOnlyOrder::element_type passiveOnlyOrder_;
	common::ReservedField9_t reservedField;
	int32_t minimumQuantity= 0;

	common::PassiveOnlyOrder::element_type passiveOnlyOrder() const noexcept(true) {
		return passiveOnlyOrder_;
	}
	void passiveOnlyOrder(common::PassiveOnlyOrder::element_type poo) noexcept(true) {
		passiveOnlyOrder_= poo;
	}
	static consteval common::OrderSource::element_type orderSource() noexcept(true) {
		return common::OrderSource::MarketParticipantDealsOnOwnAccount;
	}
	static void orderSource(common::OrderSource::element_type) noexcept(true) {
	}
};

/**
	Section: "8.4.6 Execution Report"
*/
struct [[gnu::packed]] ExecutionReportSpecific1 {
	using RejectCode_t= mit_lse::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using order_qty_t= std::int32_t;

	order_qty_t executedQty;
	order_qty_t leavesQty;
	common::Container::element_type container;
	order_qty_t displayQty;
};
struct [[gnu::packed]] ExecutionReportSpecific2 {
	const char reservedField1= '\0';
	common::TypeOfTrade::element_type typeOfTrade;
	common::Capacity::element_type capacity;
	common::PriceDifferential::element_type priceDifferential;
	common::PublicOrderID_t publicOrderID;
	int32_t minimumQuantity= 0;
};

struct [[gnu::packed]] OrderMassCancelRequestSpecific1 {
	common::SecurityID_t instrumentID_;
	common::MESQualifier::element_type mesQualifier_= common::MESQualifier::NonMES;
	const std::int8_t reservedField1= 0;
	common::Segment_t segment{};

	explicit constexpr OrderMassCancelRequestSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
	common::MESQualifier::element_type mesQualifier() const noexcept(true) {
		return mesQualifier_;
	}
	void mesQualifier(common::MESQualifier::element_type m) noexcept(true) {
		mesQualifier_= m;
	}
};

struct [[gnu::packed]] LogonReply : public common::LogonReply<mit_lse::reject_codes_enum> {
	using base_t= common::LogonReply<mit_lse::reject_codes_enum>;
	using RejectCode_t= base_t::RejectCode_t;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using base_t::base_t;

	static inline constexpr const RejectCode_t logon_success= mit_lse::reject_codes_enum::tag_SUCCESS;
	static inline constexpr const RejectCode_t invalid_logon_details= mit_lse::reject_codes_enum::AuthServer_1_INVALID_USER_OR_CREDENTIALS;
	static inline constexpr const RejectCode_t unknown_user= mit_lse::reject_codes_enum::AuthServer_4_USER_NOT_FOUND;
};

struct [[gnu::packed]] NewOrder : public common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3> {
	using base_t= common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall NewOrder(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelReplaceRequest : public common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific> {
	using base_t= common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall OrderCancelReplaceRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelRequest : public common::OrderCancelRequest<NewOrderSpecific1> {
	using base_t= common::OrderCancelRequest<NewOrderSpecific1>;
	using base_t::base_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall OrderCancelRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct MsgTypes {
	static inline constexpr const exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON;

	using ref_data= common::ref_data;
	template<class Op>
	static ref_data create_ref_data(Op&& op) noexcept(true) {
		auto&& ref_data_src= op();
		return ref_data{ref_data_src};
	}
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	using MsgType_t= common::MsgType_t;
	using MsgTypes_t= common::MsgType::element_type;
	using UserName_t= common::UserName_t;
	using Password_t= common::Password_t;
	using SecurityID_t= common::SecurityID_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= NewOrderSpecific2::order_qty_t;
	using ClientOrderID_t= common::ClientOrderID_t;
	using OrderType= common::OrderType::element_type;
	using Side= common::Side::element_type;
	using TIF= common::TIF::element_type;
	using ExecType= common::ExecType::element_type;
	using AppID= common::AppID::element_type;
	using OrderStatus= common::OrderStatus::element_type;
	using logon_args_t= common::logon_args_t;

	using Header_t= common::Header;
	using NewOrder_t= LSE::NewOrder;
	using OrderCancelRequest_t= LSE::OrderCancelRequest;
	using OrderMassCancelRequest_t= common::OrderMassCancelRequest<OrderMassCancelRequestSpecific1>;
	using OrderCancelReplaceRequest_t= LSE::OrderCancelReplaceRequest;
	using NewQuote_t= common::NewQuote<NewQuoteSpecific1, NewQuoteSpecific2>;
	using LogonRequest_t= common::LogonRequest;
	using LogoutRequest_t= common::LogoutRequest;
	using ClientHeartbeat_t= common::Heartbeat;
	using ServerHeartbeat_t= common::Heartbeat;
	using MissedMessageRequest_t= common::MissedMessageRequest;
	using ExecutionReport_t= common::ExecutionReport<ExecutionReportSpecific1, ExecutionReportSpecific2>;
	using OrderCancelReject_t= common::OrderCancelReject<common::OrderCancelRejectSpecific<mit_lse::reject_codes_enum>>;
	using OrderMassCancelReport_t= common::OrderMassCancelReport<common::OrderMassCancelReportSpecific<mit_lse::reject_codes_enum>>;
	using BusinessReject_t= common::BusinessReject<common::BusinessRejectSpecific<mit_lse::reject_codes_enum, mit_lse::reject_codes_enum::MatchingEngine_9000_Unknown_instrument>>;
	using LogonReply_t= LSE::LogonReply;
	using MissedMessageRequestAck_t= common::MissedMessageRequestAck;
	using MissedMessageReport_t= common::MissedMessageReport;
	using Reject_t= common::Reject<mit_lse::reject_codes_enum, mit_lse::reject_codes_enum::NativeGateway_102_Message_not_supported>;
	using SystemStatus_t= common::SystemStatus;

	using Logout_t= LogoutRequest_t;

	using client_to_exchange_messages_t= boost::mpl::vector<
		NewOrder_t,
		OrderCancelRequest_t,
		OrderMassCancelRequest_t,
		OrderCancelReplaceRequest_t,
		NewQuote_t,
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		MissedMessageRequest_t>;

	using exchange_to_client_messages_t= boost::mpl::vector<
		ExecutionReport_t,
		OrderCancelReject_t,
		OrderMassCancelReport_t,
		BusinessReject_t,
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		MissedMessageRequestAck_t,
		MissedMessageReport_t,
		Reject_t,
		SystemStatus_t>;

	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= sizeof(typename LogonRequest_t::Header_t)
	};
	BOOST_MPL_ASSERT_RELATION(max_msg_size, >=, header_t_size);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const Price_t implied_decimal_places= common::implied_decimal_places;

	template<class ConnPolT, class SktT, class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return common::make_ctor_args<MsgTypes, ConnPolT, SktT, ThrdT>(vm);
	}

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test MIT LSE size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 80);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 38);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), ==, 24);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequest_t), ==, 9);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequestAck_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageReport_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Reject_t), ==, 59);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::SystemStatus_t), ==, 6);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrder_t), ==, 101);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewQuote_t), ==, 76);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReplaceRequest_t), ==, 116);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelRequest_t), ==, 73);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelRequest_t), ==, 46);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ExecutionReport_t), ==, 155);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReject_t), ==, 63);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelReport_t), ==, 56);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::BusinessReject_t), ==, 63);

}

}
}}}}

#include "messages_impl.hpp"

#endif
