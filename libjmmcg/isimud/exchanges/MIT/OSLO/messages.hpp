#ifndef ISIMUD_EXCHANGES_MIT_OSLO_MESSAGES_HPP
#define ISIMUD_EXCHANGES_MIT_OSLO_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "mit_oslo_config.h"
#include "reject_codes.hpp"

#include "../common/messages.hpp"
#include "../common/ref_data.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

/**
	See:
	-# <a href="https://www.oslobors.no/ob_eng/Oslo-Boers/Trading/Trading-systems/Millennium-Exchange/Technical-documentation"/>
	-# <a href="https://www.oslobors.no/obnewsletter/download/084172908123a0c01db388950fd0ecd3/file/file/OSLMIT%20203%20Native%20Trading%20Gateway%20-%20issue%205.2.pdf">"OSLMIT 203 Native Trading Gateway", Issue 5.2, 27 October 2016</a>.
	-# <a href="https://www.oslobors.no/ob_eng/obnewsletter/download/af04d9d4a7aff5dd1d2f1bf402618b4f/file/file/OSLMIT%20203%20Native%20Trading%20Gateway%20-%20issue%204.2.pdf">"Millennium Exchange - Oslo Børs cash equities and fixed income markets", OSLMIT 801 Reject Codes, Issue 4.2, 28 April 2015</a>.

	"OSLMIT 203 Native Trading Gateway", Issue 4.1, 12 January 2015

*/
namespace OSLO {

/**
	Section: "5.6.1 New Order"
*/
struct [[gnu::packed]] NewOrderSpecific1 {
	common::SecurityID_t instrumentID_;
	const std::int8_t reservedField1= 0;
	const std::int8_t reservedField2= 0;

	explicit constexpr NewOrderSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};
struct [[gnu::packed]] NewOrderSpecific2 {
	using order_qty_t= std::uint64_t;

	order_qty_t orderQty_;
	order_qty_t displayQty;

	explicit constexpr NewOrderSpecific2(order_qty_t ordQty) noexcept(true)
		: orderQty_(ordQty), displayQty(ordQty) {
	}
	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}
};
struct [[gnu::packed]] NewOrderSpecific3 {
	common::PassiveOnlyOrder::element_type passiveOnlyOrder_= common::PassiveOnlyOrder::NoConstraint;
	common::TargetBook::element_type targetBook= common::TargetBook::LitOrderBook;
	common::ExecInstruction::element_type execInstruction= common::ExecInstruction::Default;
	int32_t minimumQuantity= 0;
	const common::PegPriceType::element_type pegPriceType= common::PegPriceType::MidPoint;
	common::ReservedField12_t reservedField;

	common::PassiveOnlyOrder::element_type passiveOnlyOrder() const noexcept(true) {
		return passiveOnlyOrder_;
	}
	void passiveOnlyOrder(common::PassiveOnlyOrder::element_type poo) noexcept(true) {
		passiveOnlyOrder_= poo;
	}
	static consteval common::OrderSource::element_type orderSource() noexcept(true) {
		return common::OrderSource::MarketParticipantDealsOnOwnAccount;
	}
	static void orderSource(common::OrderSource::element_type) noexcept(true) {
	}
};

/**
	Section: "5.6.4 Order Modification Request"
*/
struct [[gnu::packed]] OrderCancelReplaceRequestSpecific {
	common::TargetBook::element_type targetBook;
	common::ExecInstruction::element_type execInstruction;
	std::int32_t minimumQuantity;
	const std::int16_t reservedField1= 0;
	common::ReservedField10_t reservedField2;
	common::PassiveOnlyOrder::element_type passiveOnlyOrder_;

	common::PassiveOnlyOrder::element_type passiveOnlyOrder() const noexcept(true) {
		return passiveOnlyOrder_;
	}
	void passiveOnlyOrder(common::PassiveOnlyOrder::element_type poo) noexcept(true) {
		passiveOnlyOrder_= poo;
	}
};

/**
	Section: "5.7.1 Execution Report"
*/
struct [[gnu::packed]] ExecutionReportSpecific1 {
	using RejectCode_t= mit_oslo::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using order_qty_t= std::uint64_t;

	order_qty_t executedQty;
	order_qty_t leavesQty;
	common::Container::element_type container;
	order_qty_t displayQty;
};
struct [[gnu::packed]] ExecutionReportSpecific2 {
	common::TargetBook::element_type targetBook;
	int32_t minimumQuantity;
	const std::int8_t reservedField1= 0;
	common::ReservedField4_t reservedField2;
	const std::uint8_t reservedField3= 0;
	common::Price_t avgPx;
	const common::Price_t reservedField4= 0;
	common::ReservedField20_t reservedField5;
	common::TypeOfTrade::element_type typeOfTrade;
	common::ReservedField20_t reservedField6;
	common::RestatementReason::element_type restatementReason;
	common::PriceDifferential::element_type priceDifferential;
	common::OrderID_t publicOrderID;
};

struct [[gnu::packed]] OrderMassCancelRequestSpecific1 {
	common::SecurityID_t instrumentID_;
	const std::int8_t reservedField1= 0;
	const std::int8_t reservedField2= 0;
	common::Segment_t segment{};

	explicit constexpr OrderMassCancelRequestSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};

struct [[gnu::packed]] LogonReply : public common::LogonReply<mit_oslo::reject_codes_enum> {
	using base_t= common::LogonReply<mit_oslo::reject_codes_enum>;
	using RejectCode_t= base_t::RejectCode_t;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using base_t::base_t;

	static inline constexpr const RejectCode_t logon_success= mit_oslo::reject_codes_enum::tag_SUCCESS;
	static inline constexpr const RejectCode_t invalid_logon_details= mit_oslo::reject_codes_enum::Native_Trading_Gateway__Invalid_User_ID_or_password_1;
	static inline constexpr const RejectCode_t unknown_user= mit_oslo::reject_codes_enum::Native_Trading_Gateway__Invalid_User_ID_or_password_1;
};

struct [[gnu::packed]] NewOrder : public common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3> {
	using base_t= common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall NewOrder(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelReplaceRequest : public common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific> {
	using base_t= common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall OrderCancelReplaceRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelRequest : public common::OrderCancelRequest<NewOrderSpecific1> {
	using base_t= common::OrderCancelRequest<NewOrderSpecific1>;
	using base_t::base_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall OrderCancelRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct MsgTypes {
	static inline constexpr const exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_BORS_XOSL;

	using ref_data= common::ref_data;
	template<class Op>
	static ref_data create_ref_data(Op&& op) noexcept(true) {
		auto&& ref_data_src= op();
		return ref_data{ref_data_src};
	}
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	using MsgType_t= common::MsgType_t;
	using MsgTypes_t= common::MsgType::element_type;
	using UserName_t= common::UserName_t;
	using Password_t= common::Password_t;
	using SecurityID_t= common::SecurityID_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= NewOrderSpecific2::order_qty_t;
	using ClientOrderID_t= common::ClientOrderID_t;
	using OrderType= common::OrderType::element_type;
	using Side= common::Side::element_type;
	using TIF= common::TIF::element_type;
	using ExecType= common::ExecType::element_type;
	using AppID= common::AppID::element_type;
	using OrderStatus= common::OrderStatus::element_type;
	using logon_args_t= common::logon_args_t;

	using Header_t= common::Header;
	using NewOrder_t= OSLO::NewOrder;
	using OrderCancelRequest_t= OSLO::OrderCancelRequest;
	using OrderMassCancelRequest_t= common::OrderMassCancelRequest<OrderMassCancelRequestSpecific1>;
	using OrderCancelReplaceRequest_t= OSLO::OrderCancelReplaceRequest;
	using LogonRequest_t= common::LogonRequest;
	using LogoutRequest_t= common::LogoutRequest;
	using ClientHeartbeat_t= common::Heartbeat;
	using ServerHeartbeat_t= common::Heartbeat;
	using MissedMessageRequest_t= common::MissedMessageRequest;
	using ExecutionReport_t= common::ExecutionReport<ExecutionReportSpecific1, ExecutionReportSpecific2>;
	using OrderCancelReject_t= common::OrderCancelReject<common::OrderCancelRejectSpecific<mit_oslo::reject_codes_enum>>;
	using OrderMassCancelReport_t= common::OrderMassCancelReport<common::OrderMassCancelReportSpecific<mit_oslo::reject_codes_enum>>;
	using BusinessReject_t= common::BusinessReject<common::BusinessRejectSpecific<mit_oslo::reject_codes_enum, mit_oslo::reject_codes_enum::Matching_Engine__Unknown_instrument_9000>>;
	using LogonReply_t= OSLO::LogonReply;
	using MissedMessageRequestAck_t= common::MissedMessageRequestAck;
	using MissedMessageReport_t= common::MissedMessageReport;
	using Reject_t= common::Reject<mit_oslo::reject_codes_enum, mit_oslo::reject_codes_enum::Native_Trading_Gateway__Message_not_supported_102>;
	using SystemStatus_t= common::SystemStatus;

	using Logout_t= LogoutRequest_t;

	using client_to_exchange_messages_t= boost::mpl::vector<
		NewOrder_t,
		OrderCancelRequest_t,
		OrderMassCancelRequest_t,
		OrderCancelReplaceRequest_t,
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		MissedMessageRequest_t>;

	using exchange_to_client_messages_t= boost::mpl::vector<
		ExecutionReport_t,
		OrderCancelReject_t,
		OrderMassCancelReport_t,
		BusinessReject_t,
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		MissedMessageRequestAck_t,
		MissedMessageReport_t,
		Reject_t,
		SystemStatus_t>;

	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= sizeof(typename LogonRequest_t::Header_t)
	};
	BOOST_MPL_ASSERT_RELATION(max_msg_size, >=, header_t_size);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const Price_t implied_decimal_places= common::implied_decimal_places;

	template<class ConnPolT, class SktT, class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return common::make_ctor_args<MsgTypes, ConnPolT, SktT, ThrdT>(vm);
	}

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test MIT OSLO size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 80);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 38);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Logout_t), ==, 24);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequest_t), ==, 9);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequestAck_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageReport_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Reject_t), ==, 59);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::SystemStatus_t), ==, 6);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrder_t), ==, 106);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReplaceRequest_t), ==, 129);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelRequest_t), ==, 73);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelRequest_t), ==, 46);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ExecutionReport_t), ==, 229);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReject_t), ==, 63);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelReport_t), ==, 56);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::BusinessReject_t), ==, 63);

}

}
}}}}

#include "messages_impl.hpp"

#endif
