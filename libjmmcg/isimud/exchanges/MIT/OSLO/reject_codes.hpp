#ifndef ISIMUD_EXCHANGES_MIT_REJECT_CODES_mit_oslo_reject_codes_hpp
#define ISIMUD_EXCHANGES_MIT_REJECT_CODES_mit_oslo_reject_codes_hpp
/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#include "common/isimud_config.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <cstdint>

// Auto-built header file from auto-downloaded and converted XLS from the web, so good luck trying to identify the source.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
// Source XLS version info: '3 '
// Version info: 2079
namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace mit_oslo {
using RejectCodes_t= std::int32_t;

enum reject_codes_enum : RejectCodes_t {
	tag_SUCCESS= 0,
	Native_Trading_Gateway__Invalid_User_ID_or_password_1= 1,
	Post_Trade_Gateway__Invalid_TrdSubType__829__4= 4,
	Post_Trade_Gateway__Not_Authorized_9= 9,
	Post_Trade_Gateway__Cannot_Match_Selection_Criteria_100= 100,
	Native_Trading_Gateway__Invalid_message_length_101= 101,
	Native_Trading_Gateway__Message_not_supported_102= 102,
	Native_Trading_Gateway__Invalid_message_version_103= 103,
	Native_Trading_Gateway__Login_request_being_processed_105= 105,
	Native_Trading_Gateway__Required_field_not_set_115= 115,
	Native_Trading_Gateway__Segment_not_set_201= 201,
	Native_Trading_Gateway__Invalid_request_type_203= 203,
	Post_Trade_Gateway__Request_Limit_for_Day_Reached_500= 500,
	MatchingEngine__Max_active_order_limit_reached_for_user_for_matching_thread_928= 928,
	Matching_Engine__Invalid_order_quantity__less_than_or_equal_to_zero__1000= 1000,
	Matching_Engine__Invalid_order_quantity__less_than_minimum_quantity__1001= 1001,
	Matching_Engine__Invalid_order_size__not_multiple_of_lot_size__1002= 1002,
	Matching_Engine__Invalid_order_quantity__greater_than_maximum_quantity__1003= 1003,
	Matching_Engine__Invalid_order_quantity_1004= 1004,
	Matching_Engine__Invalid_order_size____maximum_order_value__1005= 1005,
	Matching_Engine__Invalid_order_size____minimum_reserve_order_value__1007= 1007,
	Matching_Engine__Invalid_order_size____minimum_reserve_order_size__1008= 1008,
	Matching_Engine__Invalid_display_size____zero__1100= 1100,
	Matching_Engine__Invalid_display_quantity__greater_than_order_quantity__1101= 1101,
	Matching_Engine__Invalid_display_quantity__not_multiple_of_lot_size__1102= 1102,
	Matching_Engine__Invalid_display_size_1103= 1103,
	Matching_Engine__Iceberg_orders_disabled_for_instrument_1105= 1105,
	Matching_Engine__Orders_with_hidden_sizes_not_permitted_for_order_book_1108= 1108,
	Matching_Engine__Invalid_minimum_size____order_size__1150= 1150,
	Matching_Engine__Invalid_minimum_size__not_multiple_of_lot_size__1151= 1151,
	Matching_Engine__Minimum_fill_orders_disabled_for_instrument_1154= 1154,
	Matching_Engine__Orders_with_minimum_size_not_permitted_for_order_book_1155= 1155,
	Matching_Engine__Invalid_limit_price__not_multiple_of_tick__1201= 1201,
	Matching_Engine__Invalid_limit_price__price_band_breached__1202= 1202,
	Matching_Engine__Invalid_limit_price____maximum_price__1203= 1203,
	Matching_Engine__Invalid_limit_price____minimum_price__1204= 1204,
	Matching_Engine__Invalid_order_type__unknown__1400= 1400,
	Matching_Engine__Invalid_order_type__market_or_stop_order_with_a_limit_price__1401= 1401,
	Matching_Engine__Invalid_order_type__market_or_limit_order_with_a_stop_price__1402= 1402,
	Matching_Engine__Market_orders_disabled_for_instrument_1403= 1403,
	Matching_Engine__Stop_orders_disabled_for_instrument_1404= 1404,
	Matching_Engine__Market_to_limit_orders_disabled_for_instrument_1405= 1405,
	Matching_Engine__Market_orders_are_not_permitted_during_an_auction_call_1408= 1408,
	Matching_Engine__Invalid_type__only_limit_orders_permitted_for_order_book__1409= 1409,
	Matching_Engine__Invalid_TIF__unknown__1500= 1500,
	Matching_Engine__Invalid_expire_time__elapsed__1501= 1501,
	Matching_Engine__Invalid_expire_time__time_is_for_a_future_date__1502= 1502,
	Matching_Engine__Invalid_expire_date__elapsed__1503= 1503,
	Matching_Engine__Invalid_TIF__IOC___FOK_not_permitted_during_auction_calls__1506= 1506,
	Matching_Engine__Invalid_TIF__OPG_not_permitted_outside_opening_auction_call__1507= 1507,
	Matching_Engine__Invalid_TIF__invalid_date_format__1508= 1508,
	Matching_Engine__No_time_qualifier_specified_1509= 1509,
	Matching_Engine__Invalid_TIF__GFA_orders_not_supported__1511= 1511,
	Matching_Engine__Invalid_TIF__IOC___FOK_not_permitted_during_Pause_session__1515= 1515,
	Matching_Engine__GTD_orders_disabled_for_instrument_1516= 1516,
	Matching_Engine__GTT_orders_disabled_for_instrument_1517= 1517,
	Matching_Engine__FOK_orders_disabled_for_instrument_1518= 1518,
	Matching_Engine__GTC_orders_disabled_for_instrument_1519= 1519,
	Matching_Engine__OPG_orders_disabled_for_instrument_1520= 1520,
	Matching_Engine__ATC_orders_disabled_for_instrument_1521= 1521,
	Matching_Engine__Invalid_TIF__Not_permitted_for_order_book__1522= 1522,
	Matching_Engine__CPX_orders_disabled_for_instrument_1526= 1526,
	Matching_Engine__Maximum_parked_order_limit_reached_for_instrument_1527= 1527,
	Matching_Engine__IOC_orders_disabled_for_instrument_1528= 1528,
	Matching_Engine__Expired__end_of_day__1550= 1550,
	Matching_Engine__Invalid_clearing_details__details_not_provided_or_invalid__1700= 1700,
	Matching_Engine__User_not_registered_to_submit_interest_for_instrument_1800= 1800,
	Matching_Engine__User_not_registered_to_submit_interest_for__Owner_ID__1801= 1801,
	Matching_Engine___s_not_registered_to_submit_orders_for_instrument_1806= 1806,
	Matching_Engine__Invalid_side_1900= 1900,
	Matching_Engine__Invalid_order_status___d__1901= 1901,
	Matching_Engine__Received_Prior_to_First_Trading_Date_of_instrument_1902= 1902,
	Matching_Engine__Last_Trading_Date_of_instrument_elapsed_1903= 1903,
	Matching_Engine__Invalid_order_capacity_1904= 1904,
	Matching_Engine__Invalid_instrument_set_up__no_tick_structure__1905= 1905,
	Matching_Engine__Short_sales_disabled_for_instrument_1906= 1906,
	Matching_Engine__Named_orders_disabled_for_instrument_1907= 1907,
	Matching_Engine__Only_named_orders_permitted_for_order_book_1908= 1908,
	Matching_Engine__Invalid_side__Sell_short_sales_not_permitted_for_order_book__1915= 1915,
	Matching_Engine__Maximum_active_order_limit_reached_for_matching_thread_1927= 1927,
	Matching_Engine__Max_active_order_limit_reached_for_user_for_matching_thread_1928= 1928,
	Matching_Engine__Order_not_found__too_late_to_cancel_or_unknown_order__2000= 2000,
	Matching_Engine__User_not_registered_to_mass_cancel_interest_2001= 2001,
	Matching_Engine__User_not_registered_to_mass_cancel_interest_for_firm_2002= 2002,
	Matching_Engine__Unknown_user__submitting_Trader_ID__2003= 2003,
	Matching_Engine__Unknown_instrument_2004= 2004,
	Matching_Engine__Unknown_segment_2006= 2006,
	Matching_Engine__Unknown_firm_2007= 2007,
	Matching_Engine__Unknown_clearing_mnemonic_2008= 2008,
	Matching_Engine__Unknown_user__target_Owner_ID__2009= 2009,
	Matching_Engine__Unknown_user__target_Trader_ID__2010= 2010,
	Matching_Engine__Invalid_mass_cancel_type_2011= 2011,
	Matching_Engine__No_orders_for_instrument_underlying_2012= 2012,
	Matching_Engine__Unknown_Node_ID_2014= 2014,
	Matching_Engine__Other_2099= 2099,
	Matching_Engine__Invalid_order_quantity__less_than_filled_quantity__3000= 3000,
	Matching_Engine__Invalid_display_size____order_size__3100= 3100,
	Matching_Engine__Conversion_of_fully_hidden_order_to_iceberg_order_prohibited_3101= 3101,
	Matching_Engine__Conversion_of_fully_hidden_order_to_fully_visible_prohibited_3102= 3102,
	Matching_Engine__Conversion_of_iceberg_order_to_fully_hidden_order_prohibited_3103= 3103,
	Matching_Engine__Conversion_of_fully_visible_order_to_fully_hidden_prohibited_3104= 3104,
	Matching_Engine__Invalid_owner__different_from_original_order__3700= 3700,
	Matching_Engine__User_not_registered_to_manage_interest_for_instrument_3800= 3800,
	Matching_Engine__User_not_registered_to_manage_interest_for__s_3801= 3801,
	Matching_Engine__Invalid_side__different_from_original_order__3900= 3900,
	Matching_Engine__Order_type_may_not_be_amended_3906= 3906,
	Matching_Engine__User_not_registered_to_submit_quotes_for_instrument_4800= 4800,
	Matching_Engine__Quotes_disabled_for_instrument_4908= 4908,
	Matching_Engine__Mass_quotes_not_permitted_for_order_book_4909= 4909,
	Post_Trade_Gateway__Invalid_trade_price______zero__7000= 7000,
	Post_Trade_Gateway__Invalid_trade_size______zero__7001= 7001,
	Post_Trade_Gateway__Invalid_trade_time_7002= 7002,
	Post_Trade_Gateway__Invalid_off_book_trade_type_7003= 7003,
	Post_Trade_Gateway__Unknown_Trade_ID_7004= 7004,
	Post_Trade_Gateway__Unknown_executing_firm_7005= 7005,
	Post_Trade_Gateway__Unknown_contra_firm_7006= 7006,
	Post_Trade_Gateway__Unknown_executing_user__Owner_ID__7007= 7007,
	Post_Trade_Gateway__Unknown_executing_user__Trader_ID__7008= 7008,
	Post_Trade_Gateway__Unknown_contra_user__Owner_ID__7009= 7009,
	Post_Trade_Gateway__Unknown_contra_user__Default_User__7010= 7010,
	Post_Trade_Gateway__User_not_registered_to_submit_trade_reports_for_instrument_7011= 7011,
	Post_Trade_Gateway__User_not_registered_to_submit_third_party_trade_reports_7012= 7012,
	Post_Trade_Gateway__Executing_firm_not_registered_to_submit_trade_reports_7013= 7013,
	Post_Trade_Gateway__Contra_firm_not_registered_to_submit_trade_reports_7014= 7014,
	Post_Trade_Gateway__Not_registered_to_trade_report_for_executing_firm__Owner_ID__7015= 7015,
	Post_Trade_Gateway__Not_registered_to_submit_trade_reports_for_contra_firm_7016= 7016,
	Post_Trade_Gateway__User_not_authorised_to_pre_release_trade_7017= 7017,
	Post_Trade_Gateway__User_not_authorised_to_cancel_trade_7018= 7018,
	Post_Trade_Gateway__Trade_already_cancelled_7019= 7019,
	Post_Trade_Gateway__Trade_already_published_7020= 7020,
	Post_Trade_Gateway__Invalid_trade_reporting_model_7021= 7021,
	Post_Trade_Gateway__Unknown_executing_user__Default_User__7022= 7022,
	Post_Trade_Gateway__Trade_exempt_from_publication_7023= 7023,
	Post_Trade_Gateway__Executing_user__Owner_ID__not_from_executing_firm_7024= 7024,
	Post_Trade_Gateway__Contra_user__Owner_ID__not_from_contra_firm_7025= 7025,
	Post_Trade_Gateway__Unknown_instrument_7027= 7027,
	Post_Trade_Gateway__Invalid_order_capacity_7028= 7028,
	Post_Trade_Gateway__Invalid_contra_firm_7029= 7029,
	Post_Trade_Gateway__Invalid_settlement_date_7030= 7030,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__price__7031= 7031,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__size__7032= 7032,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__off_book_trade_type__7033= 7033,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__execution_time__7034= 7034,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__instrument__7035= 7035,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__settlement_date__7036= 7036,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_indicator__7037= 7037,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_delay__7038= 7038,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_delay__7039= 7039,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__contra_party__7040= 7040,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__side__7041= 7041,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__FirmTradeID__7042= 7042,
	Post_Trade_Gateway__Invalid_trade_size____maximum_size__7043= 7043,
	Post_Trade_Gateway__User_not_authorised_to_amend_trade_7045= 7045,
	Post_Trade_Gateway__Trade_already_confirmed_7046= 7046,
	Post_Trade_Gateway__Trade_confirmation_process_cancelled_7047= 7047,
	Post_Trade_Gateway__Trade_time_not_specified_7048= 7048,
	Post_Trade_Gateway__Pre_release_process_cancelled_7049= 7049,
	Post_Trade_Gateway__Cancellation_process_terminated_7050= 7050,
	Post_Trade_Gateway__Pre_release_process_initiated_7051= 7051,
	Post_Trade_Gateway__Cancellation_process_initiated_7052= 7052,
	Post_Trade_Gateway__Third_party_trade_reports_not_permitted_7053= 7053,
	Post_Trade_Gateway__Invalid_side_for_contra_firm_7054= 7054,
	Post_Trade_Gateway__Invalid_publication_indicator__trade_must_be_published__7055= 7055,
	Post_Trade_Gateway__Invalid_trade_size__must_be_an_integer__7056= 7056,
	Post_Trade_Gateway__Request_does_not_include_the_trade_term_to_be_amended_7057= 7057,
	Post_Trade_Gateway__Invalid_side_for_executing_firm_7058= 7058,
	Post_Trade_Gateway__Trade_is_not_confirmed_7059= 7059,
	Post_Trade_Gateway__Request_already_accepted_declined_7060= 7060,
	Post_Trade_Gateway__Duplicate_Firm_Trade_ID_7061= 7061,
	Post_Trade_Gateway__Trade_reporting_disabled_for_instrument_7500= 7500,
	Post_Trade_Gateway__Trade_reporting_disabled_for_instrument__no_order_book__7501= 7501,
	Post_Trade_Gateway__Off_book_trade_reporting_prohibited__not_in_session__7502= 7502,
	Post_Trade_Gateway__Instrument_halted_7503= 7503,
	Post_Trade_Gateway__Instrument_suspended_7504= 7504,
	Post_Trade_Gateway__Instrument_inactive_7505= 7505,
	Post_Trade_Gateway__Instrument_in_Post_Close_Session_7506= 7506,
	Post_Trade_Gateway__Received_Prior_to_First_Trading_Date_of_instrument_7507= 7507,
	Post_Trade_Gateway__Last_Trading_Date_of_instrument_elapsed_7508= 7508,
	Post_Trade_Gateway__Non_binary_trades_may_not_be_cancelled_7600= 7600,
	Post_Trade_Gateway__Cancel_request_for_trade_already_submitted_7601= 7601,
	Post_Trade_Gateway__Unknown_Trade_ID__for_instrument__7602= 7602,
	Post_Trade_Gateway__TradeHandlingInstr_not_specified_7800= 7800,
	Post_Trade_Gateway__FirmTradeID_not_specified_7801= 7801,
	Post_Trade_Gateway__TradeReportType_not_specified_7802= 7802,
	Post_Trade_Gateway__Off_book_trade_type_not_specified_7803= 7803,
	Post_Trade_Gateway__Price_not_specified_7804= 7804,
	Post_Trade_Gateway__Size_not_specified_7805= 7805,
	Post_Trade_Gateway__Order_capacity_not_specified__executing_firm__7806= 7806,
	Post_Trade_Gateway__Order_capacity_not_specified__contra_firm__7807= 7807,
	Post_Trade_Gateway__Executing_firm_not_specified_7808= 7808,
	Post_Trade_Gateway__Contra_firm_not_specified_7809= 7809,
	Post_Trade_Gateway__Invalid_TradeHandlingInstr_7810= 7810,
	Post_Trade_Gateway__Invalid_TradeReportType_7811= 7811,
	Post_Trade_Gateway__Invalid_TradeReportTransType_7812= 7812,
	Post_Trade_Gateway__TradeReportTransType_not_specified_7813= 7813,
	Post_Trade_Gateway__Invalid_TradePublishIndicator_7814= 7814,
	Post_Trade_Gateway__Invalid_NoSides_7815= 7815,
	Post_Trade_Gateway__Invalid_Side_7816= 7816,
	Post_Trade_Gateway__Invalid_NoPartyIDs_7817= 7817,
	Post_Trade_Gateway__Invalid_PartyIDSource_7818= 7818,
	Post_Trade_Gateway__Invalid_PartyRole_7819= 7819,
	Post_Trade_Gateway__Invalid_OrderCapacity_7820= 7820,
	Post_Trade_Gateway__TradeID_not_specified_7821= 7821,
	Post_Trade_Gateway__Instrument_not_specified_7822= 7822,
	Post_Trade_Gateway__User_not_authorised_to_amend_trade_for_this_customer_7823= 7823,
	Post_Trade_Gateway__PartyRole_not_specified_7824= 7824,
	Post_Trade_Gateway__NoPartyIDs_not_specified_7825= 7825,
	Post_Trade_Gateway__Trade_is_not_confirmed_7826= 7826,
	Post_Trade_Gateway__Trade_registration_request_was_rejected_withdrawn_7827= 7827,
	Post_Trade_Gateway__Invalid_instrument_set_up__no_delay_table__7900= 7900,
	Post_Trade_Gateway__Invalid_instrument_set_up__no_trade_sub_type_table__7901= 7901,
	Post_Trade_Gateway__Invalid_system_set_up__no_trade_type_table__7902= 7902,
	Post_Trade_Gateway__Invalid_instrument_set_up__empty_trade_type_table__7903= 7903,
	Post_Trade_Gateway__Invalid_instrument_set_up__unsupported_reporting_model__7904= 7904,
	Post_Trade_Gateway__Invalid_instrument_set_up__delay_not_defined_in_delay_table__7905= 7905,
	Post_Trade_Gateway__Invalid_instrument_set_up__undefined_share_class__7906= 7906,
	Post_Trade_Gateway__Invalid_instrument_set_up__threshold_range_not_defined__7907= 7907,
	Post_Trade_Gateway__Invalid_instrument_set_up__invalid_trading_cycle__7908= 7908,
	Matching_Engine__Duplicate_client_order_ID_8001= 8001,
	Matching_Engine__Unknown_instrument_9000= 9000,
	Matching_Engine__Unknown_order_book_9001= 9001,
	Matching_Engine__Instrument_halted_9002= 9002,
	Matching_Engine__Instrument_halted_or_suspended_9003= 9003,
	Matching_Engine__Instrument_halted__last_trading_day_reached__9004= 9004,
	Matching_Engine__Market_is_closed_9005= 9005,
	Matching_Engine__Instrument_halted__market_suspended__9006= 9006,
	Matching_Engine__Instrument_halted__invalid_trading_session__9007= 9007,
	Matching_Engine__Session_is_closed_9008= 9008,
	Matching_Engine__Instrument_halted__order_book_in_invalid_state__9009= 9009,
	Matching_Engine__Instrument_in_Post_Close_session_9011= 9011,
	Matching_Engine__Instrument_halted__invalid_set_up__9012= 9012,
	Matching_Engine__Instrument_halted__invalid_order_book_set_up__9013= 9013,
	Matching_Engine__Instrument_in_Pre_Trading_session_9014= 9014,
	Matching_Engine__Instrument_in_Closing_Price_Publication_session_9015= 9015,
	Matching_Engine__Unknown_user__Owner_ID__9100= 9100,
	Matching_Engine__Unknown_user__Trader_ID__9101= 9101,
	Matching_Engine__User_suspended_9102= 9102,
	Matching_Engine__User_inactive_9103= 9103,
	Matching_Engine__Invalid_user__not_attached_to_trading_firm__9104= 9104,
	Matching_Engine__Firm_inactive_9105= 9105,
	Matching_Engine__Invalid_trading_session__unknown__9200= 9200,
	Matching_Engine__Invalid_new_order_message_9201= 9201,
	Matching_Engine__Invalid_amend_order_message_9202= 9202,
	Matching_Engine__Invalid_cancel_order_message_9203= 9203,
	Native_Trading_Gateway__Required_field_missing_9900= 9900,
	Native_Trading_Gateway__Invalid_value_in_field_9901= 9901,
	Native_Trading_Gateway__Concurrent_login_limit_reached__9903= 9903,
	Native_Trading_Gateway__Logons_not_allowed_at_this_time_9906= 9906,
	Native_Trading_Gateway__Maximum_message_rate_exceeded_9990= 9990,
	Native_Trading_Gateway__Matching_partition_suspended_9998= 9998,
	Native_Trading_Gateway__System_suspended_9999= 9999,
	Drop_Copy_Gateway__No_open_orders_for_specified_Party_ID_10000= 10000,
	Drop_Copy_Gateway__Request_limit_for_day_reached_10001= 10001,
	Drop_Copy_Gateway__Order_download_not_permitted_for_specified_Party_ID__10003= 10003,
	Drop_Copy_Gateway__Not_authorised_to_request_an_open_order_download_10004= 10004,
	Drop_Copy_Gateway__Open_order_download_not_permitted_at_this_time_10005= 10005,
	Drop_Copy_Gateway__Unknown_Party_ID_10006= 10006,
	Drop_Copy_Gateway__No_open_orders_for_specified_instrument__10008= 10008,
	Drop_Copy_Gateway__Segment_not_specified_10009= 10009,
	Drop_Copy_Gateway__Unknown_Segment_10010= 10010,
	Drop_Copy_Gateway__No_open_orders_for_specified_segment_10011= 10011,
	Matching_Engine__Invalid_reserve_value____minimum_reserve_order_value__111000= 111000,
	Matching_Engine__Invalid_qty___max_order_qty__111001= 111001,
	Matching_Engine__Invalid_display_size____minimum_disclosed_size__111100= 111100,
	Matching_Engine__Invalid_display_size__pegged_orders_cannot_be_displayed__111101= 111101,
	Matching_Engine__Invalid_display_size____order_size__111102= 111102,
	Matching_Engine__Invalid_order__un_priced_order_with_hidden_quantity__111103= 111103,
	Matching_Engine__Invalid_order_type__named_orders_are_not_allowed__111400= 111400,
	Matching_Engine__Invalid_order_type__stop_stop_limit_orders_are_not_allowed__111401= 111401,
	Matching_Engine__Invalid_order_type__not_allowed_in_the_session__111402= 111402,
	Matching_Engine__Invalid_order_type__pegged_orders_cannot_be_stop_orders__111403= 111403,
	Matching_Engine__Invalid_amend__cannot_amend_order_type__111404= 111404,
	Matching_Engine__Invalid_order_type_for_user__Pegged_Order__111406= 111406,
	Matching_Engine__Invalid_order_type__mkt_orders_not_allowed__111407= 111407,
	Matching_Engine__Invalid_amend__cannot_amend_TIF__111500= 111500,
	Matching_Engine__Invalid_TIF__relevant_session_elapsed_not_found__111501= 111501,
	Matching_Engine__Invalid_TIF__not_allowed_for_the_session__111502= 111502,
	Matching_Engine__Invalid_TIF__maximum_order_duration_is_set__111503= 111503,
	Matching_Engine__Invalid_expiry_date__maximum_order_duration_is_violated__111504= 111504,
	Matching_Engine__Invalid_TIF__not_permitted_for_pegged_orders__111506= 111506,
	Matching_Engine__Invalid_TIF__GFS_market_to_limit_orders_not_permitted__111507= 111507,
	Matching_Engine__Invalid_TIF__GFS_stop_orders_not_permitted__111508= 111508,
	Matching_Engine__GFS_Orders_Disabled_for_Instrument_111509= 111509,
	Matching_Engine__Invalid_session__cannot_enter_orders_quotes__111600= 111600,
	Matching_Engine__Invalid_session__orders_are_not_allowed__111601= 111601,
	Matching_Engine__Invalid_session__Only_aggressive_orders_are_allowed__111602= 111602,
	Matching_Engine__Invalid_session__cannot_cancel_amend_orders_quotes__111800= 111800,
	Matching_Engine__Invalid_clearing_set_up__clearing_information_not_defined__111801= 111801,
	Matching_Engine__Invalid_account_type__unknown__111900= 111900,
	Matching_Engine__Invalid_capacity__unknown__111901= 111901,
	Matching_Engine__No_internal_mid_point_established_111902= 111902,
	Matching_Engine__Invalid_amend__hidden_to_iceberg_fully_visible__113100= 113100,
	Matching_Engine__Invalid_amend__iceberg_fully_visible_to_hidden__113101= 113101,
	Post_Trade___Invalid_capacity_type_117001= 117001,
	Post_Trade_Gateway__Invalid_price_band_117002= 117002,
	Post_Trade_Gateway__Invalid_Instrument_with_no_closing_price_maintained__117003= 117003,
	Post_Trade_Gateway__Invalid_settlement_date_117004= 117004,
	Post_Trade_Gateway__Invalid_instrument_without_a_delay_model_117005= 117005,
	Post_Trade_Gateway__Invalid_Instrument_with_invalid_delay_model_117006= 117006,
	Post_Trade_Gateway__Trade_Reporting_time_over_117007= 117007,
	Post_Trade_Gateway__Invalid_TrdSubType_829__for_zero_priced_trade_117008= 117008,
	Post_Trade_Gateway__Executing_Trader_Group_not_specified_117027= 117027,
	Post_Trade_Gateway__Counterparty_Trader_Group_not_specified_117028= 117028,
	Matching_Engine__Order_Expired_due_to_Self_Execution_Prevention_119200= 119200,
	Matching_Engine__Failed_maximum_order_value_validation_121001= 121001,
	Matching_Engine__Invalid_MES____order_size__121002= 121002,
	Matching_Engine__Invalid_MES__less_than_minimum_size__121003= 121003,
	Matching_Engine__Invalid_MES__negative__121004= 121004,
	Matching_Engine__Invalid_MES__not_a_multiple_of_lot_size__121005= 121005,
	Matching_Engine__MES_Should_be_greater_than_the_Minimum_Size_of_the_Book_121006= 121006,
	Matching_Engine__Minimum_Quantity_Cannot_be_nagative_121007= 121007,
	Matching_Engine__Order_Value_Cannot_exceed_the_maximum_value_121008= 121008,
	Matching_Engine__Invalid_Order_Sub_Type_121401= 121401,
	Matching_Engine__Non_Persistent_Orders_cannot_be_TQ_Uncross_only_121402= 121402,
	Matching_Engine__Invalid_Message_Type_for_Lit_Book_121403= 121403,
	Matching_Engine__Invalid_Order_Qualifier_121500= 121500,
	Matching_Engine__MES_not_allowed_for_persistent_orders_121902= 121902,
	Matching_Engine__Invalid_Execution_Instruction_for_Bid_Offer_Book_121904= 121904,
	Matching_Engine__Invalid_Peg_Price_Type_121905= 121905,
	Matching_Engine__IOC_FOK_orders_not_allowed_on_Primary_Peg_121907= 121907,
	Matching_Engine__Invalid_Execution_Instruction_for_Instrument_121908= 121908,
	Matching_Engine__Invalid_TIF__GFA__for____Continuous_Only____Matching_Instruction_121914= 121914,
	Matching_Engine__Invalid_request__Order_Cancellation_or_Amendment_not_allowed_121915= 121915,
	Matching_Engine__Invalid_amend__cannot_amend_MES__123201= 123201,
	Matching_Engine__Invalid_Book_Target_Book_in_the_Received_Order_129001= 129001,
	Matching_Engine__Attached_Instrument_Is_not_Dark_129500= 129500,
	Matching_Engine__Cannot_amend_Account_Type_129501= 129501,
	Matching_Engine__Cannot_amend_Capacity_129502= 129502,
	Matching_Engine__Invalid_display_quantity____zero__129503= 129503,
	Matching_Engine__Invalid_Limit_Price__must_be_greater_than_zero__130154= 130154,
	Matching_Engine__GFA_Orders_Disabled_for_Instrument_130252= 130252,
	Matching_Engine__MTL_Orders_Not_Allowed_During_Continuous_Trading_130253= 130253,
	Matching_Engine__ExpireTime_amendment_not_allowed_for_Order_s_TIF_130304= 130304,
	Matching_Engine__ExpireDate_amendment_not_allowed_for_Order_s_TIF_130305= 130305,
	Matching_Engine__Day_Orders_are_not_permitted_130306= 130306,
	Matching_Engine__Trading_Party_Only_Authorized_to_Submit_Limit_Orders_130451= 130451,
	Matching_Engine__Trading_Party_Only_Authorized_to_Submit_Market_Orders_130452= 130452,
	Matching_Engine__Trading_Party_Only_Authorized_to_One_Limit_Order_130453= 130453,
	Matching_Engine__Trading_Party_Not_Authorized_to_Submit_Named_Orders_130454= 130454,
	Matching_Engine__Submitting_Party_Should_Always_Be_an_Executing_Firm_130518= 130518,
	Matching_Engine__ClOrdID_not_Specified_130519= 130519,
	Matching_Engine__Maximum_number_of_active_Orders_per_book_limit_reached_130520= 130520,
	Matching_Engine__Cannot_amend_cancel_user_name_trader_group_130951= 130951,
	Matching_Engine__Invalid_Contra_Firm_131004= 131004,
	Matching_Engine__Cannot_Amend_Pre_Trade_Anonymity_131352= 131352,
	Post_Trade_Gateway__Maximum_delay_for_off_book_trade_submission_exceeded__131655= 131655,
	Post_Trade_Gateway__Invalid_Capacity_for_delay_publication_131657= 131657,
	Matching_Engine__Price_point_does_not_exist_on_contra_side_140151= 140151,
	Matching_Engine__Cross_Orders_disabled_for_Instrument_140251= 140251,
	Matching_Engine__Pegged_Orders_are_not_allowed_140252= 140252,
	Matching_Engine__Pegged_Orders_are_allowed_for_Normal_Book_only_140253= 140253,
	Matching_Engine__Admin_Session_In_Progress_140351= 140351,
	Matching_Engine__Special_Order_Entry_Not_Allowed_During_This_Session_140352= 140352,
	Matching_Engine__This_Session_Only_Allows_Management_of_Special_Order_140353= 140353,
	Matching_Engine__Trading_Party_Not_Authorized_to_Manage_Special_Order_140451= 140451,
	Post_Trade_Gateway__Current_Date_TrdType_O_OK_not_allowed_during_this_session_141651= 141651,
	Post_Trade_Gateway__Volume___1_Round_lot_allowed_only_for_TrdType_OL_DL_141652= 141652,
	Post_Trade_Gateway__Invalid_Firm_Contra_Firm___Executing_firm_is_the_same__141653= 141653,
	Post_Trade_Gateway__Invalid_Settlement_Date_End_date___Start_date_141654= 141654,
	Post_Trade_Gateway__Invalid_Settlement_Date_Start_End_date_not_specified_141655= 141655,
	Post_Trade_Gateway__Invalid_Settlement_Date_Start_End_date_not_valid_for_TrdTyp_141656= 141656,
	Post_Trade_Gateway__Invalid_Settlement_Date_Start_Date___Executed_date_141657= 141657,
	Post_Trade_Gateway__SettlCurrency_entered_is_not_a_valid_ISO_currency_code_141658= 141658,
	Post_Trade_Gateway__Trade_Cancellations_not_allowed__Post_Close__141659= 141659,
	Post_Trade_Gateway__Trade_Sub_Types_are_not_supported_as_per_the_Setup_141660= 141660,
	Post_Trade_Gateway__Trade_Cancellations_are_not_allowed_during_Post_Close_141661= 141661,
	Post_Trade_Gateway__Invalid_Instrument_Trade_Type_Capacity_combination_141662= 141662,
	Post_Trade_Gateway__Invalid_Tag_specified_in_TCR_141663= 141663,
	Post_Trade_Gateway__Discrepancy_in_trade_terms__Start_End_date__141664= 141664,
	Post_Trade_Gateway__Pre_Release_of_trades_not_allowed__Post_Close__141665= 141665,
	Post_Trade_Gateway__Settlement_date_not_specified_141666= 141666
};

inline char const*
reject_codes_to_string(const reject_codes_enum code) noexcept(true) {
	static constexpr char const* const reject_code_strs[]= {
		"SUCCESS",
		"Invalid User ID or password",
		"Invalid TrdSubType (829)",
		"Not Authorized",
		"Cannot Match Selection Criteria",
		"Invalid message length",
		"Message not supported",
		"Invalid message version",
		"Login request being processed",
		"Required field not set",
		"Segment not set",
		"Invalid request type",
		"Request Limit for Day Reached",
		"Max active order limit reached for user for matching thread",
		"Invalid order quantity (less than or equal to zero)",
		"Invalid order quantity (less than minimum quantity)",
		"Invalid order size (not multiple of lot size)",
		"Invalid order quantity (greater than maximum quantity)",
		"Invalid order quantity",
		"Invalid order size (> maximum order value)",
		"Invalid order size (< minimum reserve order value)",
		"Invalid order size (< minimum reserve order size)",
		"Invalid display size (< zero)",
		"Invalid display quantity (greater than order quantity)",
		"Invalid display quantity (not multiple of lot size)",
		"Invalid display size",
		"Iceberg orders disabled for instrument",
		"Orders with hidden sizes not permitted for order book",
		"Invalid minimum size (> order size)",
		"Invalid minimum size (not multiple of lot size)",
		"Minimum fill orders disabled for instrument",
		"Orders with minimum size not permitted for order book",
		"Invalid limit price (not multiple of tick)",
		"Invalid limit price (price band breached)",
		"Invalid limit price (> maximum price)",
		"Invalid limit price (< minimum price)",
		"Invalid order type (unknown)",
		"Invalid order type (market or stop order with a limit price)",
		"Invalid order type (market or limit order with a stop price)",
		"Market orders disabled for instrument",
		"Stop orders disabled for instrument",
		"Market to limit orders disabled for instrument",
		"Market orders are not permitted during an auction call",
		"Invalid type (only limit orders permitted for order book)",
		"Invalid TIF (unknown)",
		"Invalid expire time (elapsed)",
		"Invalid expire time (time is for a future date)",
		"Invalid expire date (elapsed)",
		"Invalid TIF (IOC & FOK not permitted during auction calls)",
		"Invalid TIF (OPG not permitted outside opening auction call)",
		"Invalid TIF (invalid date format)",
		"No time qualifier specified",
		"Invalid TIF (GFA orders not supported)",
		"Invalid TIF (IOC & FOK not permitted during Pause session)",
		"GTD orders disabled for instrument",
		"GTT orders disabled for instrument",
		"FOK orders disabled for instrument",
		"GTC orders disabled for instrument",
		"OPG orders disabled for instrument",
		"ATC orders disabled for instrument",
		"Invalid TIF (Not permitted for order book)",
		"CPX orders disabled for instrument",
		"Maximum parked order limit reached for instrument",
		"IOC orders disabled for instrument",
		"Expired (end of day)",
		"Invalid clearing details (details not provided or invalid)",
		"User not registered to submit interest for instrument",
		"User not registered to submit interest for (Owner ID)",
		"%s not registered to submit orders for instrument",
		"Invalid side",
		"Invalid order status (%d)",
		"Received Prior to First Trading Date of instrument",
		"Last Trading Date of instrument elapsed",
		"Invalid order capacity",
		"Invalid instrument set up (no tick structure)",
		"Short sales disabled for instrument",
		"Named orders disabled for instrument",
		"Only named orders permitted for order book",
		"Invalid side (Sell short sales not permitted for order book)",
		"Maximum active order limit reached for matching thread",
		"Max active order limit reached for user for matching thread",
		"Order not found (too late to cancel or unknown order)",
		"User not registered to mass cancel interest",
		"User not registered to mass cancel interest for firm",
		"Unknown user (submitting Trader ID)",
		"Unknown instrument",
		"Unknown segment",
		"Unknown firm",
		"Unknown clearing mnemonic",
		"Unknown user (target Owner ID)",
		"Unknown user (target Trader ID)",
		"Invalid mass cancel type",
		"No orders for instrument/underlying",
		"Unknown Node ID",
		"Other",
		"Invalid order quantity (less than filled quantity)",
		"Invalid display size (> order size)",
		"Conversion of fully hidden order to iceberg order prohibited",
		"Conversion of fully hidden order to fully visible prohibited",
		"Conversion of iceberg order to fully hidden order prohibited",
		"Conversion of fully visible order to fully hidden prohibited",
		"Invalid owner (different from original order)",
		"User not registered to manage interest for instrument",
		"User not registered to manage interest for %s",
		"Invalid side (different from original order)",
		"Order type may not be amended",
		"User not registered to submit quotes for instrument",
		"Quotes disabled for instrument",
		"Mass quotes not permitted for order book",
		"Invalid trade price (___ zero)",
		"Invalid trade size (___ zero)",
		"Invalid trade time",
		"Invalid off-book trade type",
		"Unknown Trade ID",
		"Unknown executing firm",
		"Unknown contra firm",
		"Unknown executing user (Owner ID)",
		"Unknown executing user (Trader ID)",
		"Unknown contra user (Owner ID)",
		"Unknown contra user (Default User)",
		"User not registered to submit trade reports for instrument",
		"User not registered to submit third party trade reports",
		"Executing firm not registered to submit trade reports",
		"Contra firm not registered to submit trade reports",
		"Not registered to trade report for executing firm (Owner ID)",
		"Not registered to submit trade reports for contra firm",
		"User not authorised to pre-release trade",
		"User not authorised to cancel trade",
		"Trade already cancelled",
		"Trade already published",
		"Invalid trade reporting model",
		"Unknown executing user (Default User)",
		"Trade exempt from publication",
		"Executing user (Owner ID) not from executing firm",
		"Contra user (Owner ID) not from contra firm",
		"Unknown instrument",
		"Invalid order capacity",
		"Invalid contra firm",
		"Invalid settlement date",
		"Discrepancy in trade terms (price)",
		"Discrepancy in trade terms (size)",
		"Discrepancy in trade terms (off-book trade type)",
		"Discrepancy in trade terms (execution time)",
		"Discrepancy in trade terms (instrument)",
		"Discrepancy in trade terms (settlement date)",
		"Discrepancy in trade terms (publication indicator)",
		"Discrepancy in trade terms (publication delay)",
		"Discrepancy in trade terms (publication delay)",
		"Discrepancy in trade terms (contra party)",
		"Discrepancy in trade terms (side)",
		"Discrepancy in trade terms (FirmTradeID)",
		"Invalid trade size (> maximum size)",
		"User not authorised to amend trade",
		"Trade already confirmed",
		"Trade confirmation process cancelled",
		"Trade time not specified",
		"Pre-release process cancelled",
		"Cancellation process terminated",
		"Pre-release process initiated",
		"Cancellation process initiated",
		"Third party trade reports not permitted",
		"Invalid side for contra firm",
		"Invalid publication indicator (trade must be published)",
		"Invalid trade size (must be an integer)",
		"Request does not include the trade term to be amended",
		"Invalid side for executing firm",
		"Trade is not confirmed",
		"Request already accepted/declined",
		"Duplicate Firm Trade ID",
		"Trade reporting disabled for instrument",
		"Trade reporting disabled for instrument (no order book)",
		"Off-book trade reporting prohibited (not in session)",
		"Instrument halted",
		"Instrument suspended",
		"Instrument inactive",
		"Instrument in Post Close Session",
		"Received Prior to First Trading Date of instrument",
		"Last Trading Date of instrument elapsed",
		"Non-binary trades may not be cancelled",
		"Cancel request for trade already submitted",
		"Unknown Trade ID (for instrument)",
		"TradeHandlingInstr not specified",
		"FirmTradeID not specified",
		"TradeReportType not specified",
		"Off-book trade type not specified",
		"Price not specified",
		"Size not specified",
		"Order capacity not specified (executing firm)",
		"Order capacity not specified (contra firm)",
		"Executing firm not specified",
		"Contra firm not specified",
		"Invalid TradeHandlingInstr",
		"Invalid TradeReportType",
		"Invalid TradeReportTransType",
		"TradeReportTransType not specified",
		"Invalid TradePublishIndicator",
		"Invalid NoSides",
		"Invalid Side",
		"Invalid NoPartyIDs",
		"Invalid PartyIDSource",
		"Invalid PartyRole",
		"Invalid OrderCapacity",
		"TradeID not specified",
		"Instrument not specified",
		"User not authorised to amend trade for this customer",
		"PartyRole not specified",
		"NoPartyIDs not specified",
		"Trade is not confirmed",
		"Trade registration request was rejected/withdrawn",
		"Invalid instrument set up (no delay table)",
		"Invalid instrument set up (no trade sub type table)",
		"Invalid system set up (no trade type table)",
		"Invalid instrument set up (empty trade type table)",
		"Invalid instrument set up (unsupported reporting model)",
		"Invalid instrument set up (delay not defined in delay table)",
		"Invalid instrument set up (undefined share class)",
		"Invalid instrument set up (threshold range not defined)",
		"Invalid instrument set up (invalid trading cycle)",
		"Duplicate client order ID",
		"Unknown instrument",
		"Unknown order book",
		"Instrument halted",
		"Instrument halted or suspended",
		"Instrument halted (last trading day reached)",
		"Market is closed",
		"Instrument halted (market suspended)",
		"Instrument halted (invalid trading session)",
		"Session is closed",
		"Instrument halted (order book in invalid state)",
		"Instrument in Post-Close session",
		"Instrument halted (invalid set up)",
		"Instrument halted (invalid order book set up)",
		"Instrument in Pre-Trading session",
		"Instrument in Closing Price Publication session",
		"Unknown user (Owner ID)",
		"Unknown user (Trader ID)",
		"User suspended",
		"User inactive",
		"Invalid user (not attached to trading firm)",
		"Firm inactive",
		"Invalid trading session (unknown)",
		"Invalid new order message",
		"Invalid amend order message",
		"Invalid cancel order message",
		"Required field missing",
		"Invalid value in field",
		"Concurrent login limit reached ",
		"Logons not allowed at this time",
		"Maximum message rate exceeded",
		"Matching partition suspended",
		"System suspended",
		"No open orders for specified Party ID",
		"Request limit for day reached",
		"Order download not permitted for specified Party ID ",
		"Not authorised to request an open order download",
		"Open order download not permitted at this time",
		"Unknown Party ID",
		"No open orders for specified instrument ",
		"Segment not specified",
		"Unknown Segment",
		"No open orders for specified segment",
		"Invalid reserve value (< minimum reserve order value)",
		"Invalid qty (>max order qty)",
		"Invalid display size (< minimum disclosed size)",
		"Invalid display size (pegged orders cannot be displayed)",
		"Invalid display size (> order size)",
		"Invalid order (un priced order with hidden quantity)",
		"Invalid order type (named orders are not allowed)",
		"Invalid order type (stop/stop limit orders are not allowed)",
		"Invalid order type (not allowed in the session)",
		"Invalid order type (pegged orders cannot be stop orders)",
		"Invalid amend (cannot amend order type)",
		"Invalid order type for user (Pegged Order)",
		"Invalid order type (mkt orders not allowed)",
		"Invalid amend (cannot amend TIF)",
		"Invalid TIF (relevant session elapsed/not found)",
		"Invalid TIF (not allowed for the session)",
		"Invalid TIF (maximum order duration is set)",
		"Invalid expiry date (maximum order duration is violated)",
		"Invalid TIF (not permitted for pegged orders)",
		"Invalid TIF (GFS market to limit orders not permitted)",
		"Invalid TIF (GFS stop orders not permitted)",
		"GFS Orders Disabled for Instrument",
		"Invalid session (cannot enter orders/quotes)",
		"Invalid session (orders are not allowed)",
		"Invalid session (Only aggressive orders are allowed)",
		"Invalid session (cannot cancel/amend orders/quotes)",
		"Invalid clearing set up (clearing information not defined)",
		"Invalid account type (unknown)",
		"Invalid capacity (unknown)",
		"No internal mid-point established",
		"Invalid amend (hidden to iceberg/fully visible)",
		"Invalid amend (iceberg/fully visible to hidden)",
		"Invalid capacity type",
		"Invalid price band",
		"Invalid Instrument with no closing price maintained.",
		"Invalid settlement date",
		"Invalid instrument without a delay model",
		"Invalid Instrument with invalid delay model",
		"Trade Reporting time over",
		"Invalid TrdSubType(829) for zero priced trade",
		"Executing Trader Group not specified",
		"Counterparty Trader Group not specified",
		"Order Expired due to Self Execution Prevention",
		"Failed maximum order value validation",
		"Invalid MES (> order size)",
		"Invalid MES (less than minimum size)",
		"Invalid MES (negative)",
		"Invalid MES (not a multiple of lot size)",
		"MES Should be greater than the Minimum Size of the Book",
		"Minimum Quantity Cannot be nagative",
		"Order Value Cannot exceed the maximum value",
		"Invalid Order Sub Type",
		"Non Persistent Orders cannot be TQ Uncross only",
		"Invalid Message Type for Lit Book",
		"Invalid Order Qualifier",
		"MES not allowed for persistent orders",
		"Invalid Execution Instruction for Bid/Offer Book",
		"Invalid Peg Price Type",
		"IOC/FOK orders not allowed on Primary Peg",
		"Invalid Execution Instruction for Instrument",
		"Invalid TIF (GFA) for ___Continuous Only___ Matching Instruction",
		"Invalid request. Order Cancellation or Amendment not allowed",
		"Invalid amend (cannot amend MES)",
		"Invalid Book Target Book in the Received Order",
		"Attached Instrument Is not Dark",
		"Cannot amend Account Type",
		"Cannot amend Capacity",
		"Invalid display quantity (> zero)",
		"Invalid Limit Price (must be greater than zero)",
		"GFA Orders Disabled for Instrument",
		"MTL Orders Not Allowed During Continuous Trading",
		"ExpireTime amendment not allowed for Order's TIF",
		"ExpireDate amendment not allowed for Order's TIF",
		"Day Orders are not permitted",
		"Trading Party Only Authorized to Submit Limit Orders",
		"Trading Party Only Authorized to Submit Market Orders",
		"Trading Party Only Authorized to One Limit Order",
		"Trading Party Not Authorized to Submit Named Orders",
		"Submitting Party Should Always Be an Executing Firm",
		"ClOrdID not Specified",
		"Maximum number of active Orders per book limit reached",
		"Cannot amend/cancel user name/trader group",
		"Invalid Contra Firm",
		"Cannot Amend Pre Trade Anonymity",
		"Maximum delay for off book trade submission exceeded ",
		"Invalid Capacity for delay publication",
		"Price point does not exist on contra side",
		"Cross Orders disabled for Instrument",
		"Pegged Orders are not allowed",
		"Pegged Orders are allowed for Normal Book only",
		"Admin Session In Progress",
		"Special Order Entry Not Allowed During This Session",
		"This Session Only Allows Management of Special Order",
		"Trading Party Not Authorized to Manage Special Order",
		"Current Date TrdType O&OK not allowed during this session",
		"Volume < 1 Round lot allowed only for TrdType OL&DL",
		"Invalid Firm(Contra-Firm & Executing firm is the same)",
		"Invalid Settlement Date:End date < Start date",
		"Invalid Settlement Date:Start/End date not specified",
		"Invalid Settlement Date:Start/End date not valid for TrdTyp",
		"Invalid Settlement Date:Start Date < Executed date",
		"SettlCurrency entered is not a valid ISO currency code",
		"Trade Cancellations not allowed (Post Close)",
		"Trade Sub Types are not supported as per the Setup",
		"Trade Cancellations are not allowed during Post Close",
		"Invalid Instrument/Trade Type/Capacity combination",
		"Invalid Tag specified in TCR",
		"Discrepancy in trade terms (Start/End date)",
		"Pre-Release of trades not allowed (Post Close)",
		"Settlement date not specified"};
	switch(code) {
	case tag_SUCCESS:
		return reject_code_strs[0];
	case reject_codes_enum::Native_Trading_Gateway__Invalid_User_ID_or_password_1:
		return reject_code_strs[1];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TrdSubType__829__4:
		return reject_code_strs[2];
	case reject_codes_enum::Post_Trade_Gateway__Not_Authorized_9:
		return reject_code_strs[3];
	case reject_codes_enum::Post_Trade_Gateway__Cannot_Match_Selection_Criteria_100:
		return reject_code_strs[4];
	case reject_codes_enum::Native_Trading_Gateway__Invalid_message_length_101:
		return reject_code_strs[5];
	case reject_codes_enum::Native_Trading_Gateway__Message_not_supported_102:
		return reject_code_strs[6];
	case reject_codes_enum::Native_Trading_Gateway__Invalid_message_version_103:
		return reject_code_strs[7];
	case reject_codes_enum::Native_Trading_Gateway__Login_request_being_processed_105:
		return reject_code_strs[8];
	case reject_codes_enum::Native_Trading_Gateway__Required_field_not_set_115:
		return reject_code_strs[9];
	case reject_codes_enum::Native_Trading_Gateway__Segment_not_set_201:
		return reject_code_strs[10];
	case reject_codes_enum::Native_Trading_Gateway__Invalid_request_type_203:
		return reject_code_strs[11];
	case reject_codes_enum::Post_Trade_Gateway__Request_Limit_for_Day_Reached_500:
		return reject_code_strs[12];
	case reject_codes_enum::MatchingEngine__Max_active_order_limit_reached_for_user_for_matching_thread_928:
		return reject_code_strs[13];
	case reject_codes_enum::Matching_Engine__Invalid_order_quantity__less_than_or_equal_to_zero__1000:
		return reject_code_strs[14];
	case reject_codes_enum::Matching_Engine__Invalid_order_quantity__less_than_minimum_quantity__1001:
		return reject_code_strs[15];
	case reject_codes_enum::Matching_Engine__Invalid_order_size__not_multiple_of_lot_size__1002:
		return reject_code_strs[16];
	case reject_codes_enum::Matching_Engine__Invalid_order_quantity__greater_than_maximum_quantity__1003:
		return reject_code_strs[17];
	case reject_codes_enum::Matching_Engine__Invalid_order_quantity_1004:
		return reject_code_strs[18];
	case reject_codes_enum::Matching_Engine__Invalid_order_size____maximum_order_value__1005:
		return reject_code_strs[19];
	case reject_codes_enum::Matching_Engine__Invalid_order_size____minimum_reserve_order_value__1007:
		return reject_code_strs[20];
	case reject_codes_enum::Matching_Engine__Invalid_order_size____minimum_reserve_order_size__1008:
		return reject_code_strs[21];
	case reject_codes_enum::Matching_Engine__Invalid_display_size____zero__1100:
		return reject_code_strs[22];
	case reject_codes_enum::Matching_Engine__Invalid_display_quantity__greater_than_order_quantity__1101:
		return reject_code_strs[23];
	case reject_codes_enum::Matching_Engine__Invalid_display_quantity__not_multiple_of_lot_size__1102:
		return reject_code_strs[24];
	case reject_codes_enum::Matching_Engine__Invalid_display_size_1103:
		return reject_code_strs[25];
	case reject_codes_enum::Matching_Engine__Iceberg_orders_disabled_for_instrument_1105:
		return reject_code_strs[26];
	case reject_codes_enum::Matching_Engine__Orders_with_hidden_sizes_not_permitted_for_order_book_1108:
		return reject_code_strs[27];
	case reject_codes_enum::Matching_Engine__Invalid_minimum_size____order_size__1150:
		return reject_code_strs[28];
	case reject_codes_enum::Matching_Engine__Invalid_minimum_size__not_multiple_of_lot_size__1151:
		return reject_code_strs[29];
	case reject_codes_enum::Matching_Engine__Minimum_fill_orders_disabled_for_instrument_1154:
		return reject_code_strs[30];
	case reject_codes_enum::Matching_Engine__Orders_with_minimum_size_not_permitted_for_order_book_1155:
		return reject_code_strs[31];
	case reject_codes_enum::Matching_Engine__Invalid_limit_price__not_multiple_of_tick__1201:
		return reject_code_strs[32];
	case reject_codes_enum::Matching_Engine__Invalid_limit_price__price_band_breached__1202:
		return reject_code_strs[33];
	case reject_codes_enum::Matching_Engine__Invalid_limit_price____maximum_price__1203:
		return reject_code_strs[34];
	case reject_codes_enum::Matching_Engine__Invalid_limit_price____minimum_price__1204:
		return reject_code_strs[35];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__unknown__1400:
		return reject_code_strs[36];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__market_or_stop_order_with_a_limit_price__1401:
		return reject_code_strs[37];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__market_or_limit_order_with_a_stop_price__1402:
		return reject_code_strs[38];
	case reject_codes_enum::Matching_Engine__Market_orders_disabled_for_instrument_1403:
		return reject_code_strs[39];
	case reject_codes_enum::Matching_Engine__Stop_orders_disabled_for_instrument_1404:
		return reject_code_strs[40];
	case reject_codes_enum::Matching_Engine__Market_to_limit_orders_disabled_for_instrument_1405:
		return reject_code_strs[41];
	case reject_codes_enum::Matching_Engine__Market_orders_are_not_permitted_during_an_auction_call_1408:
		return reject_code_strs[42];
	case reject_codes_enum::Matching_Engine__Invalid_type__only_limit_orders_permitted_for_order_book__1409:
		return reject_code_strs[43];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__unknown__1500:
		return reject_code_strs[44];
	case reject_codes_enum::Matching_Engine__Invalid_expire_time__elapsed__1501:
		return reject_code_strs[45];
	case reject_codes_enum::Matching_Engine__Invalid_expire_time__time_is_for_a_future_date__1502:
		return reject_code_strs[46];
	case reject_codes_enum::Matching_Engine__Invalid_expire_date__elapsed__1503:
		return reject_code_strs[47];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__IOC___FOK_not_permitted_during_auction_calls__1506:
		return reject_code_strs[48];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__OPG_not_permitted_outside_opening_auction_call__1507:
		return reject_code_strs[49];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__invalid_date_format__1508:
		return reject_code_strs[50];
	case reject_codes_enum::Matching_Engine__No_time_qualifier_specified_1509:
		return reject_code_strs[51];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__GFA_orders_not_supported__1511:
		return reject_code_strs[52];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__IOC___FOK_not_permitted_during_Pause_session__1515:
		return reject_code_strs[53];
	case reject_codes_enum::Matching_Engine__GTD_orders_disabled_for_instrument_1516:
		return reject_code_strs[54];
	case reject_codes_enum::Matching_Engine__GTT_orders_disabled_for_instrument_1517:
		return reject_code_strs[55];
	case reject_codes_enum::Matching_Engine__FOK_orders_disabled_for_instrument_1518:
		return reject_code_strs[56];
	case reject_codes_enum::Matching_Engine__GTC_orders_disabled_for_instrument_1519:
		return reject_code_strs[57];
	case reject_codes_enum::Matching_Engine__OPG_orders_disabled_for_instrument_1520:
		return reject_code_strs[58];
	case reject_codes_enum::Matching_Engine__ATC_orders_disabled_for_instrument_1521:
		return reject_code_strs[59];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__Not_permitted_for_order_book__1522:
		return reject_code_strs[60];
	case reject_codes_enum::Matching_Engine__CPX_orders_disabled_for_instrument_1526:
		return reject_code_strs[61];
	case reject_codes_enum::Matching_Engine__Maximum_parked_order_limit_reached_for_instrument_1527:
		return reject_code_strs[62];
	case reject_codes_enum::Matching_Engine__IOC_orders_disabled_for_instrument_1528:
		return reject_code_strs[63];
	case reject_codes_enum::Matching_Engine__Expired__end_of_day__1550:
		return reject_code_strs[64];
	case reject_codes_enum::Matching_Engine__Invalid_clearing_details__details_not_provided_or_invalid__1700:
		return reject_code_strs[65];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_submit_interest_for_instrument_1800:
		return reject_code_strs[66];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_submit_interest_for__Owner_ID__1801:
		return reject_code_strs[67];
	case reject_codes_enum::Matching_Engine___s_not_registered_to_submit_orders_for_instrument_1806:
		return reject_code_strs[68];
	case reject_codes_enum::Matching_Engine__Invalid_side_1900:
		return reject_code_strs[69];
	case reject_codes_enum::Matching_Engine__Invalid_order_status___d__1901:
		return reject_code_strs[70];
	case reject_codes_enum::Matching_Engine__Received_Prior_to_First_Trading_Date_of_instrument_1902:
		return reject_code_strs[71];
	case reject_codes_enum::Matching_Engine__Last_Trading_Date_of_instrument_elapsed_1903:
		return reject_code_strs[72];
	case reject_codes_enum::Matching_Engine__Invalid_order_capacity_1904:
		return reject_code_strs[73];
	case reject_codes_enum::Matching_Engine__Invalid_instrument_set_up__no_tick_structure__1905:
		return reject_code_strs[74];
	case reject_codes_enum::Matching_Engine__Short_sales_disabled_for_instrument_1906:
		return reject_code_strs[75];
	case reject_codes_enum::Matching_Engine__Named_orders_disabled_for_instrument_1907:
		return reject_code_strs[76];
	case reject_codes_enum::Matching_Engine__Only_named_orders_permitted_for_order_book_1908:
		return reject_code_strs[77];
	case reject_codes_enum::Matching_Engine__Invalid_side__Sell_short_sales_not_permitted_for_order_book__1915:
		return reject_code_strs[78];
	case reject_codes_enum::Matching_Engine__Maximum_active_order_limit_reached_for_matching_thread_1927:
		return reject_code_strs[79];
	case reject_codes_enum::Matching_Engine__Max_active_order_limit_reached_for_user_for_matching_thread_1928:
		return reject_code_strs[80];
	case reject_codes_enum::Matching_Engine__Order_not_found__too_late_to_cancel_or_unknown_order__2000:
		return reject_code_strs[81];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_mass_cancel_interest_2001:
		return reject_code_strs[82];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_mass_cancel_interest_for_firm_2002:
		return reject_code_strs[83];
	case reject_codes_enum::Matching_Engine__Unknown_user__submitting_Trader_ID__2003:
		return reject_code_strs[84];
	case reject_codes_enum::Matching_Engine__Unknown_instrument_2004:
		return reject_code_strs[85];
	case reject_codes_enum::Matching_Engine__Unknown_segment_2006:
		return reject_code_strs[86];
	case reject_codes_enum::Matching_Engine__Unknown_firm_2007:
		return reject_code_strs[87];
	case reject_codes_enum::Matching_Engine__Unknown_clearing_mnemonic_2008:
		return reject_code_strs[88];
	case reject_codes_enum::Matching_Engine__Unknown_user__target_Owner_ID__2009:
		return reject_code_strs[89];
	case reject_codes_enum::Matching_Engine__Unknown_user__target_Trader_ID__2010:
		return reject_code_strs[90];
	case reject_codes_enum::Matching_Engine__Invalid_mass_cancel_type_2011:
		return reject_code_strs[91];
	case reject_codes_enum::Matching_Engine__No_orders_for_instrument_underlying_2012:
		return reject_code_strs[92];
	case reject_codes_enum::Matching_Engine__Unknown_Node_ID_2014:
		return reject_code_strs[93];
	case reject_codes_enum::Matching_Engine__Other_2099:
		return reject_code_strs[94];
	case reject_codes_enum::Matching_Engine__Invalid_order_quantity__less_than_filled_quantity__3000:
		return reject_code_strs[95];
	case reject_codes_enum::Matching_Engine__Invalid_display_size____order_size__3100:
		return reject_code_strs[96];
	case reject_codes_enum::Matching_Engine__Conversion_of_fully_hidden_order_to_iceberg_order_prohibited_3101:
		return reject_code_strs[97];
	case reject_codes_enum::Matching_Engine__Conversion_of_fully_hidden_order_to_fully_visible_prohibited_3102:
		return reject_code_strs[98];
	case reject_codes_enum::Matching_Engine__Conversion_of_iceberg_order_to_fully_hidden_order_prohibited_3103:
		return reject_code_strs[99];
	case reject_codes_enum::Matching_Engine__Conversion_of_fully_visible_order_to_fully_hidden_prohibited_3104:
		return reject_code_strs[100];
	case reject_codes_enum::Matching_Engine__Invalid_owner__different_from_original_order__3700:
		return reject_code_strs[101];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_manage_interest_for_instrument_3800:
		return reject_code_strs[102];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_manage_interest_for__s_3801:
		return reject_code_strs[103];
	case reject_codes_enum::Matching_Engine__Invalid_side__different_from_original_order__3900:
		return reject_code_strs[104];
	case reject_codes_enum::Matching_Engine__Order_type_may_not_be_amended_3906:
		return reject_code_strs[105];
	case reject_codes_enum::Matching_Engine__User_not_registered_to_submit_quotes_for_instrument_4800:
		return reject_code_strs[106];
	case reject_codes_enum::Matching_Engine__Quotes_disabled_for_instrument_4908:
		return reject_code_strs[107];
	case reject_codes_enum::Matching_Engine__Mass_quotes_not_permitted_for_order_book_4909:
		return reject_code_strs[108];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_price______zero__7000:
		return reject_code_strs[109];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_size______zero__7001:
		return reject_code_strs[110];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_time_7002:
		return reject_code_strs[111];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_off_book_trade_type_7003:
		return reject_code_strs[112];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_Trade_ID_7004:
		return reject_code_strs[113];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_executing_firm_7005:
		return reject_code_strs[114];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_contra_firm_7006:
		return reject_code_strs[115];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_executing_user__Owner_ID__7007:
		return reject_code_strs[116];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_executing_user__Trader_ID__7008:
		return reject_code_strs[117];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_contra_user__Owner_ID__7009:
		return reject_code_strs[118];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_contra_user__Default_User__7010:
		return reject_code_strs[119];
	case reject_codes_enum::Post_Trade_Gateway__User_not_registered_to_submit_trade_reports_for_instrument_7011:
		return reject_code_strs[120];
	case reject_codes_enum::Post_Trade_Gateway__User_not_registered_to_submit_third_party_trade_reports_7012:
		return reject_code_strs[121];
	case reject_codes_enum::Post_Trade_Gateway__Executing_firm_not_registered_to_submit_trade_reports_7013:
		return reject_code_strs[122];
	case reject_codes_enum::Post_Trade_Gateway__Contra_firm_not_registered_to_submit_trade_reports_7014:
		return reject_code_strs[123];
	case reject_codes_enum::Post_Trade_Gateway__Not_registered_to_trade_report_for_executing_firm__Owner_ID__7015:
		return reject_code_strs[124];
	case reject_codes_enum::Post_Trade_Gateway__Not_registered_to_submit_trade_reports_for_contra_firm_7016:
		return reject_code_strs[125];
	case reject_codes_enum::Post_Trade_Gateway__User_not_authorised_to_pre_release_trade_7017:
		return reject_code_strs[126];
	case reject_codes_enum::Post_Trade_Gateway__User_not_authorised_to_cancel_trade_7018:
		return reject_code_strs[127];
	case reject_codes_enum::Post_Trade_Gateway__Trade_already_cancelled_7019:
		return reject_code_strs[128];
	case reject_codes_enum::Post_Trade_Gateway__Trade_already_published_7020:
		return reject_code_strs[129];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_reporting_model_7021:
		return reject_code_strs[130];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_executing_user__Default_User__7022:
		return reject_code_strs[131];
	case reject_codes_enum::Post_Trade_Gateway__Trade_exempt_from_publication_7023:
		return reject_code_strs[132];
	case reject_codes_enum::Post_Trade_Gateway__Executing_user__Owner_ID__not_from_executing_firm_7024:
		return reject_code_strs[133];
	case reject_codes_enum::Post_Trade_Gateway__Contra_user__Owner_ID__not_from_contra_firm_7025:
		return reject_code_strs[134];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_instrument_7027:
		return reject_code_strs[135];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_order_capacity_7028:
		return reject_code_strs[136];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_contra_firm_7029:
		return reject_code_strs[137];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_settlement_date_7030:
		return reject_code_strs[138];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__price__7031:
		return reject_code_strs[139];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__size__7032:
		return reject_code_strs[140];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__off_book_trade_type__7033:
		return reject_code_strs[141];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__execution_time__7034:
		return reject_code_strs[142];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__instrument__7035:
		return reject_code_strs[143];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__settlement_date__7036:
		return reject_code_strs[144];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_indicator__7037:
		return reject_code_strs[145];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_delay__7038:
		return reject_code_strs[146];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__publication_delay__7039:
		return reject_code_strs[147];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__contra_party__7040:
		return reject_code_strs[148];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__side__7041:
		return reject_code_strs[149];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__FirmTradeID__7042:
		return reject_code_strs[150];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_size____maximum_size__7043:
		return reject_code_strs[151];
	case reject_codes_enum::Post_Trade_Gateway__User_not_authorised_to_amend_trade_7045:
		return reject_code_strs[152];
	case reject_codes_enum::Post_Trade_Gateway__Trade_already_confirmed_7046:
		return reject_code_strs[153];
	case reject_codes_enum::Post_Trade_Gateway__Trade_confirmation_process_cancelled_7047:
		return reject_code_strs[154];
	case reject_codes_enum::Post_Trade_Gateway__Trade_time_not_specified_7048:
		return reject_code_strs[155];
	case reject_codes_enum::Post_Trade_Gateway__Pre_release_process_cancelled_7049:
		return reject_code_strs[156];
	case reject_codes_enum::Post_Trade_Gateway__Cancellation_process_terminated_7050:
		return reject_code_strs[157];
	case reject_codes_enum::Post_Trade_Gateway__Pre_release_process_initiated_7051:
		return reject_code_strs[158];
	case reject_codes_enum::Post_Trade_Gateway__Cancellation_process_initiated_7052:
		return reject_code_strs[159];
	case reject_codes_enum::Post_Trade_Gateway__Third_party_trade_reports_not_permitted_7053:
		return reject_code_strs[160];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_side_for_contra_firm_7054:
		return reject_code_strs[161];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_publication_indicator__trade_must_be_published__7055:
		return reject_code_strs[162];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_trade_size__must_be_an_integer__7056:
		return reject_code_strs[163];
	case reject_codes_enum::Post_Trade_Gateway__Request_does_not_include_the_trade_term_to_be_amended_7057:
		return reject_code_strs[164];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_side_for_executing_firm_7058:
		return reject_code_strs[165];
	case reject_codes_enum::Post_Trade_Gateway__Trade_is_not_confirmed_7059:
		return reject_code_strs[166];
	case reject_codes_enum::Post_Trade_Gateway__Request_already_accepted_declined_7060:
		return reject_code_strs[167];
	case reject_codes_enum::Post_Trade_Gateway__Duplicate_Firm_Trade_ID_7061:
		return reject_code_strs[168];
	case reject_codes_enum::Post_Trade_Gateway__Trade_reporting_disabled_for_instrument_7500:
		return reject_code_strs[169];
	case reject_codes_enum::Post_Trade_Gateway__Trade_reporting_disabled_for_instrument__no_order_book__7501:
		return reject_code_strs[170];
	case reject_codes_enum::Post_Trade_Gateway__Off_book_trade_reporting_prohibited__not_in_session__7502:
		return reject_code_strs[171];
	case reject_codes_enum::Post_Trade_Gateway__Instrument_halted_7503:
		return reject_code_strs[172];
	case reject_codes_enum::Post_Trade_Gateway__Instrument_suspended_7504:
		return reject_code_strs[173];
	case reject_codes_enum::Post_Trade_Gateway__Instrument_inactive_7505:
		return reject_code_strs[174];
	case reject_codes_enum::Post_Trade_Gateway__Instrument_in_Post_Close_Session_7506:
		return reject_code_strs[175];
	case reject_codes_enum::Post_Trade_Gateway__Received_Prior_to_First_Trading_Date_of_instrument_7507:
		return reject_code_strs[176];
	case reject_codes_enum::Post_Trade_Gateway__Last_Trading_Date_of_instrument_elapsed_7508:
		return reject_code_strs[177];
	case reject_codes_enum::Post_Trade_Gateway__Non_binary_trades_may_not_be_cancelled_7600:
		return reject_code_strs[178];
	case reject_codes_enum::Post_Trade_Gateway__Cancel_request_for_trade_already_submitted_7601:
		return reject_code_strs[179];
	case reject_codes_enum::Post_Trade_Gateway__Unknown_Trade_ID__for_instrument__7602:
		return reject_code_strs[180];
	case reject_codes_enum::Post_Trade_Gateway__TradeHandlingInstr_not_specified_7800:
		return reject_code_strs[181];
	case reject_codes_enum::Post_Trade_Gateway__FirmTradeID_not_specified_7801:
		return reject_code_strs[182];
	case reject_codes_enum::Post_Trade_Gateway__TradeReportType_not_specified_7802:
		return reject_code_strs[183];
	case reject_codes_enum::Post_Trade_Gateway__Off_book_trade_type_not_specified_7803:
		return reject_code_strs[184];
	case reject_codes_enum::Post_Trade_Gateway__Price_not_specified_7804:
		return reject_code_strs[185];
	case reject_codes_enum::Post_Trade_Gateway__Size_not_specified_7805:
		return reject_code_strs[186];
	case reject_codes_enum::Post_Trade_Gateway__Order_capacity_not_specified__executing_firm__7806:
		return reject_code_strs[187];
	case reject_codes_enum::Post_Trade_Gateway__Order_capacity_not_specified__contra_firm__7807:
		return reject_code_strs[188];
	case reject_codes_enum::Post_Trade_Gateway__Executing_firm_not_specified_7808:
		return reject_code_strs[189];
	case reject_codes_enum::Post_Trade_Gateway__Contra_firm_not_specified_7809:
		return reject_code_strs[190];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TradeHandlingInstr_7810:
		return reject_code_strs[191];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TradeReportType_7811:
		return reject_code_strs[192];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TradeReportTransType_7812:
		return reject_code_strs[193];
	case reject_codes_enum::Post_Trade_Gateway__TradeReportTransType_not_specified_7813:
		return reject_code_strs[194];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TradePublishIndicator_7814:
		return reject_code_strs[195];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_NoSides_7815:
		return reject_code_strs[196];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Side_7816:
		return reject_code_strs[197];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_NoPartyIDs_7817:
		return reject_code_strs[198];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_PartyIDSource_7818:
		return reject_code_strs[199];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_PartyRole_7819:
		return reject_code_strs[200];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_OrderCapacity_7820:
		return reject_code_strs[201];
	case reject_codes_enum::Post_Trade_Gateway__TradeID_not_specified_7821:
		return reject_code_strs[202];
	case reject_codes_enum::Post_Trade_Gateway__Instrument_not_specified_7822:
		return reject_code_strs[203];
	case reject_codes_enum::Post_Trade_Gateway__User_not_authorised_to_amend_trade_for_this_customer_7823:
		return reject_code_strs[204];
	case reject_codes_enum::Post_Trade_Gateway__PartyRole_not_specified_7824:
		return reject_code_strs[205];
	case reject_codes_enum::Post_Trade_Gateway__NoPartyIDs_not_specified_7825:
		return reject_code_strs[206];
	case reject_codes_enum::Post_Trade_Gateway__Trade_is_not_confirmed_7826:
		return reject_code_strs[207];
	case reject_codes_enum::Post_Trade_Gateway__Trade_registration_request_was_rejected_withdrawn_7827:
		return reject_code_strs[208];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__no_delay_table__7900:
		return reject_code_strs[209];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__no_trade_sub_type_table__7901:
		return reject_code_strs[210];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_system_set_up__no_trade_type_table__7902:
		return reject_code_strs[211];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__empty_trade_type_table__7903:
		return reject_code_strs[212];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__unsupported_reporting_model__7904:
		return reject_code_strs[213];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__delay_not_defined_in_delay_table__7905:
		return reject_code_strs[214];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__undefined_share_class__7906:
		return reject_code_strs[215];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__threshold_range_not_defined__7907:
		return reject_code_strs[216];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_set_up__invalid_trading_cycle__7908:
		return reject_code_strs[217];
	case reject_codes_enum::Matching_Engine__Duplicate_client_order_ID_8001:
		return reject_code_strs[218];
	case reject_codes_enum::Matching_Engine__Unknown_instrument_9000:
		return reject_code_strs[219];
	case reject_codes_enum::Matching_Engine__Unknown_order_book_9001:
		return reject_code_strs[220];
	case reject_codes_enum::Matching_Engine__Instrument_halted_9002:
		return reject_code_strs[221];
	case reject_codes_enum::Matching_Engine__Instrument_halted_or_suspended_9003:
		return reject_code_strs[222];
	case reject_codes_enum::Matching_Engine__Instrument_halted__last_trading_day_reached__9004:
		return reject_code_strs[223];
	case reject_codes_enum::Matching_Engine__Market_is_closed_9005:
		return reject_code_strs[224];
	case reject_codes_enum::Matching_Engine__Instrument_halted__market_suspended__9006:
		return reject_code_strs[225];
	case reject_codes_enum::Matching_Engine__Instrument_halted__invalid_trading_session__9007:
		return reject_code_strs[226];
	case reject_codes_enum::Matching_Engine__Session_is_closed_9008:
		return reject_code_strs[227];
	case reject_codes_enum::Matching_Engine__Instrument_halted__order_book_in_invalid_state__9009:
		return reject_code_strs[228];
	case reject_codes_enum::Matching_Engine__Instrument_in_Post_Close_session_9011:
		return reject_code_strs[229];
	case reject_codes_enum::Matching_Engine__Instrument_halted__invalid_set_up__9012:
		return reject_code_strs[230];
	case reject_codes_enum::Matching_Engine__Instrument_halted__invalid_order_book_set_up__9013:
		return reject_code_strs[231];
	case reject_codes_enum::Matching_Engine__Instrument_in_Pre_Trading_session_9014:
		return reject_code_strs[232];
	case reject_codes_enum::Matching_Engine__Instrument_in_Closing_Price_Publication_session_9015:
		return reject_code_strs[233];
	case reject_codes_enum::Matching_Engine__Unknown_user__Owner_ID__9100:
		return reject_code_strs[234];
	case reject_codes_enum::Matching_Engine__Unknown_user__Trader_ID__9101:
		return reject_code_strs[235];
	case reject_codes_enum::Matching_Engine__User_suspended_9102:
		return reject_code_strs[236];
	case reject_codes_enum::Matching_Engine__User_inactive_9103:
		return reject_code_strs[237];
	case reject_codes_enum::Matching_Engine__Invalid_user__not_attached_to_trading_firm__9104:
		return reject_code_strs[238];
	case reject_codes_enum::Matching_Engine__Firm_inactive_9105:
		return reject_code_strs[239];
	case reject_codes_enum::Matching_Engine__Invalid_trading_session__unknown__9200:
		return reject_code_strs[240];
	case reject_codes_enum::Matching_Engine__Invalid_new_order_message_9201:
		return reject_code_strs[241];
	case reject_codes_enum::Matching_Engine__Invalid_amend_order_message_9202:
		return reject_code_strs[242];
	case reject_codes_enum::Matching_Engine__Invalid_cancel_order_message_9203:
		return reject_code_strs[243];
	case reject_codes_enum::Native_Trading_Gateway__Required_field_missing_9900:
		return reject_code_strs[244];
	case reject_codes_enum::Native_Trading_Gateway__Invalid_value_in_field_9901:
		return reject_code_strs[245];
	case reject_codes_enum::Native_Trading_Gateway__Concurrent_login_limit_reached__9903:
		return reject_code_strs[246];
	case reject_codes_enum::Native_Trading_Gateway__Logons_not_allowed_at_this_time_9906:
		return reject_code_strs[247];
	case reject_codes_enum::Native_Trading_Gateway__Maximum_message_rate_exceeded_9990:
		return reject_code_strs[248];
	case reject_codes_enum::Native_Trading_Gateway__Matching_partition_suspended_9998:
		return reject_code_strs[249];
	case reject_codes_enum::Native_Trading_Gateway__System_suspended_9999:
		return reject_code_strs[250];
	case reject_codes_enum::Drop_Copy_Gateway__No_open_orders_for_specified_Party_ID_10000:
		return reject_code_strs[251];
	case reject_codes_enum::Drop_Copy_Gateway__Request_limit_for_day_reached_10001:
		return reject_code_strs[252];
	case reject_codes_enum::Drop_Copy_Gateway__Order_download_not_permitted_for_specified_Party_ID__10003:
		return reject_code_strs[253];
	case reject_codes_enum::Drop_Copy_Gateway__Not_authorised_to_request_an_open_order_download_10004:
		return reject_code_strs[254];
	case reject_codes_enum::Drop_Copy_Gateway__Open_order_download_not_permitted_at_this_time_10005:
		return reject_code_strs[255];
	case reject_codes_enum::Drop_Copy_Gateway__Unknown_Party_ID_10006:
		return reject_code_strs[256];
	case reject_codes_enum::Drop_Copy_Gateway__No_open_orders_for_specified_instrument__10008:
		return reject_code_strs[257];
	case reject_codes_enum::Drop_Copy_Gateway__Segment_not_specified_10009:
		return reject_code_strs[258];
	case reject_codes_enum::Drop_Copy_Gateway__Unknown_Segment_10010:
		return reject_code_strs[259];
	case reject_codes_enum::Drop_Copy_Gateway__No_open_orders_for_specified_segment_10011:
		return reject_code_strs[260];
	case reject_codes_enum::Matching_Engine__Invalid_reserve_value____minimum_reserve_order_value__111000:
		return reject_code_strs[261];
	case reject_codes_enum::Matching_Engine__Invalid_qty___max_order_qty__111001:
		return reject_code_strs[262];
	case reject_codes_enum::Matching_Engine__Invalid_display_size____minimum_disclosed_size__111100:
		return reject_code_strs[263];
	case reject_codes_enum::Matching_Engine__Invalid_display_size__pegged_orders_cannot_be_displayed__111101:
		return reject_code_strs[264];
	case reject_codes_enum::Matching_Engine__Invalid_display_size____order_size__111102:
		return reject_code_strs[265];
	case reject_codes_enum::Matching_Engine__Invalid_order__un_priced_order_with_hidden_quantity__111103:
		return reject_code_strs[266];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__named_orders_are_not_allowed__111400:
		return reject_code_strs[267];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__stop_stop_limit_orders_are_not_allowed__111401:
		return reject_code_strs[268];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__not_allowed_in_the_session__111402:
		return reject_code_strs[269];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__pegged_orders_cannot_be_stop_orders__111403:
		return reject_code_strs[270];
	case reject_codes_enum::Matching_Engine__Invalid_amend__cannot_amend_order_type__111404:
		return reject_code_strs[271];
	case reject_codes_enum::Matching_Engine__Invalid_order_type_for_user__Pegged_Order__111406:
		return reject_code_strs[272];
	case reject_codes_enum::Matching_Engine__Invalid_order_type__mkt_orders_not_allowed__111407:
		return reject_code_strs[273];
	case reject_codes_enum::Matching_Engine__Invalid_amend__cannot_amend_TIF__111500:
		return reject_code_strs[274];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__relevant_session_elapsed_not_found__111501:
		return reject_code_strs[275];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__not_allowed_for_the_session__111502:
		return reject_code_strs[276];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__maximum_order_duration_is_set__111503:
		return reject_code_strs[277];
	case reject_codes_enum::Matching_Engine__Invalid_expiry_date__maximum_order_duration_is_violated__111504:
		return reject_code_strs[278];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__not_permitted_for_pegged_orders__111506:
		return reject_code_strs[279];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__GFS_market_to_limit_orders_not_permitted__111507:
		return reject_code_strs[280];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__GFS_stop_orders_not_permitted__111508:
		return reject_code_strs[281];
	case reject_codes_enum::Matching_Engine__GFS_Orders_Disabled_for_Instrument_111509:
		return reject_code_strs[282];
	case reject_codes_enum::Matching_Engine__Invalid_session__cannot_enter_orders_quotes__111600:
		return reject_code_strs[283];
	case reject_codes_enum::Matching_Engine__Invalid_session__orders_are_not_allowed__111601:
		return reject_code_strs[284];
	case reject_codes_enum::Matching_Engine__Invalid_session__Only_aggressive_orders_are_allowed__111602:
		return reject_code_strs[285];
	case reject_codes_enum::Matching_Engine__Invalid_session__cannot_cancel_amend_orders_quotes__111800:
		return reject_code_strs[286];
	case reject_codes_enum::Matching_Engine__Invalid_clearing_set_up__clearing_information_not_defined__111801:
		return reject_code_strs[287];
	case reject_codes_enum::Matching_Engine__Invalid_account_type__unknown__111900:
		return reject_code_strs[288];
	case reject_codes_enum::Matching_Engine__Invalid_capacity__unknown__111901:
		return reject_code_strs[289];
	case reject_codes_enum::Matching_Engine__No_internal_mid_point_established_111902:
		return reject_code_strs[290];
	case reject_codes_enum::Matching_Engine__Invalid_amend__hidden_to_iceberg_fully_visible__113100:
		return reject_code_strs[291];
	case reject_codes_enum::Matching_Engine__Invalid_amend__iceberg_fully_visible_to_hidden__113101:
		return reject_code_strs[292];
	case reject_codes_enum::Post_Trade___Invalid_capacity_type_117001:
		return reject_code_strs[293];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_price_band_117002:
		return reject_code_strs[294];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Instrument_with_no_closing_price_maintained__117003:
		return reject_code_strs[295];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_settlement_date_117004:
		return reject_code_strs[296];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_instrument_without_a_delay_model_117005:
		return reject_code_strs[297];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Instrument_with_invalid_delay_model_117006:
		return reject_code_strs[298];
	case reject_codes_enum::Post_Trade_Gateway__Trade_Reporting_time_over_117007:
		return reject_code_strs[299];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_TrdSubType_829__for_zero_priced_trade_117008:
		return reject_code_strs[300];
	case reject_codes_enum::Post_Trade_Gateway__Executing_Trader_Group_not_specified_117027:
		return reject_code_strs[301];
	case reject_codes_enum::Post_Trade_Gateway__Counterparty_Trader_Group_not_specified_117028:
		return reject_code_strs[302];
	case reject_codes_enum::Matching_Engine__Order_Expired_due_to_Self_Execution_Prevention_119200:
		return reject_code_strs[303];
	case reject_codes_enum::Matching_Engine__Failed_maximum_order_value_validation_121001:
		return reject_code_strs[304];
	case reject_codes_enum::Matching_Engine__Invalid_MES____order_size__121002:
		return reject_code_strs[305];
	case reject_codes_enum::Matching_Engine__Invalid_MES__less_than_minimum_size__121003:
		return reject_code_strs[306];
	case reject_codes_enum::Matching_Engine__Invalid_MES__negative__121004:
		return reject_code_strs[307];
	case reject_codes_enum::Matching_Engine__Invalid_MES__not_a_multiple_of_lot_size__121005:
		return reject_code_strs[308];
	case reject_codes_enum::Matching_Engine__MES_Should_be_greater_than_the_Minimum_Size_of_the_Book_121006:
		return reject_code_strs[309];
	case reject_codes_enum::Matching_Engine__Minimum_Quantity_Cannot_be_nagative_121007:
		return reject_code_strs[310];
	case reject_codes_enum::Matching_Engine__Order_Value_Cannot_exceed_the_maximum_value_121008:
		return reject_code_strs[311];
	case reject_codes_enum::Matching_Engine__Invalid_Order_Sub_Type_121401:
		return reject_code_strs[312];
	case reject_codes_enum::Matching_Engine__Non_Persistent_Orders_cannot_be_TQ_Uncross_only_121402:
		return reject_code_strs[313];
	case reject_codes_enum::Matching_Engine__Invalid_Message_Type_for_Lit_Book_121403:
		return reject_code_strs[314];
	case reject_codes_enum::Matching_Engine__Invalid_Order_Qualifier_121500:
		return reject_code_strs[315];
	case reject_codes_enum::Matching_Engine__MES_not_allowed_for_persistent_orders_121902:
		return reject_code_strs[316];
	case reject_codes_enum::Matching_Engine__Invalid_Execution_Instruction_for_Bid_Offer_Book_121904:
		return reject_code_strs[317];
	case reject_codes_enum::Matching_Engine__Invalid_Peg_Price_Type_121905:
		return reject_code_strs[318];
	case reject_codes_enum::Matching_Engine__IOC_FOK_orders_not_allowed_on_Primary_Peg_121907:
		return reject_code_strs[319];
	case reject_codes_enum::Matching_Engine__Invalid_Execution_Instruction_for_Instrument_121908:
		return reject_code_strs[320];
	case reject_codes_enum::Matching_Engine__Invalid_TIF__GFA__for____Continuous_Only____Matching_Instruction_121914:
		return reject_code_strs[321];
	case reject_codes_enum::Matching_Engine__Invalid_request__Order_Cancellation_or_Amendment_not_allowed_121915:
		return reject_code_strs[322];
	case reject_codes_enum::Matching_Engine__Invalid_amend__cannot_amend_MES__123201:
		return reject_code_strs[323];
	case reject_codes_enum::Matching_Engine__Invalid_Book_Target_Book_in_the_Received_Order_129001:
		return reject_code_strs[324];
	case reject_codes_enum::Matching_Engine__Attached_Instrument_Is_not_Dark_129500:
		return reject_code_strs[325];
	case reject_codes_enum::Matching_Engine__Cannot_amend_Account_Type_129501:
		return reject_code_strs[326];
	case reject_codes_enum::Matching_Engine__Cannot_amend_Capacity_129502:
		return reject_code_strs[327];
	case reject_codes_enum::Matching_Engine__Invalid_display_quantity____zero__129503:
		return reject_code_strs[328];
	case reject_codes_enum::Matching_Engine__Invalid_Limit_Price__must_be_greater_than_zero__130154:
		return reject_code_strs[329];
	case reject_codes_enum::Matching_Engine__GFA_Orders_Disabled_for_Instrument_130252:
		return reject_code_strs[330];
	case reject_codes_enum::Matching_Engine__MTL_Orders_Not_Allowed_During_Continuous_Trading_130253:
		return reject_code_strs[331];
	case reject_codes_enum::Matching_Engine__ExpireTime_amendment_not_allowed_for_Order_s_TIF_130304:
		return reject_code_strs[332];
	case reject_codes_enum::Matching_Engine__ExpireDate_amendment_not_allowed_for_Order_s_TIF_130305:
		return reject_code_strs[333];
	case reject_codes_enum::Matching_Engine__Day_Orders_are_not_permitted_130306:
		return reject_code_strs[334];
	case reject_codes_enum::Matching_Engine__Trading_Party_Only_Authorized_to_Submit_Limit_Orders_130451:
		return reject_code_strs[335];
	case reject_codes_enum::Matching_Engine__Trading_Party_Only_Authorized_to_Submit_Market_Orders_130452:
		return reject_code_strs[336];
	case reject_codes_enum::Matching_Engine__Trading_Party_Only_Authorized_to_One_Limit_Order_130453:
		return reject_code_strs[337];
	case reject_codes_enum::Matching_Engine__Trading_Party_Not_Authorized_to_Submit_Named_Orders_130454:
		return reject_code_strs[338];
	case reject_codes_enum::Matching_Engine__Submitting_Party_Should_Always_Be_an_Executing_Firm_130518:
		return reject_code_strs[339];
	case reject_codes_enum::Matching_Engine__ClOrdID_not_Specified_130519:
		return reject_code_strs[340];
	case reject_codes_enum::Matching_Engine__Maximum_number_of_active_Orders_per_book_limit_reached_130520:
		return reject_code_strs[341];
	case reject_codes_enum::Matching_Engine__Cannot_amend_cancel_user_name_trader_group_130951:
		return reject_code_strs[342];
	case reject_codes_enum::Matching_Engine__Invalid_Contra_Firm_131004:
		return reject_code_strs[343];
	case reject_codes_enum::Matching_Engine__Cannot_Amend_Pre_Trade_Anonymity_131352:
		return reject_code_strs[344];
	case reject_codes_enum::Post_Trade_Gateway__Maximum_delay_for_off_book_trade_submission_exceeded__131655:
		return reject_code_strs[345];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Capacity_for_delay_publication_131657:
		return reject_code_strs[346];
	case reject_codes_enum::Matching_Engine__Price_point_does_not_exist_on_contra_side_140151:
		return reject_code_strs[347];
	case reject_codes_enum::Matching_Engine__Cross_Orders_disabled_for_Instrument_140251:
		return reject_code_strs[348];
	case reject_codes_enum::Matching_Engine__Pegged_Orders_are_not_allowed_140252:
		return reject_code_strs[349];
	case reject_codes_enum::Matching_Engine__Pegged_Orders_are_allowed_for_Normal_Book_only_140253:
		return reject_code_strs[350];
	case reject_codes_enum::Matching_Engine__Admin_Session_In_Progress_140351:
		return reject_code_strs[351];
	case reject_codes_enum::Matching_Engine__Special_Order_Entry_Not_Allowed_During_This_Session_140352:
		return reject_code_strs[352];
	case reject_codes_enum::Matching_Engine__This_Session_Only_Allows_Management_of_Special_Order_140353:
		return reject_code_strs[353];
	case reject_codes_enum::Matching_Engine__Trading_Party_Not_Authorized_to_Manage_Special_Order_140451:
		return reject_code_strs[354];
	case reject_codes_enum::Post_Trade_Gateway__Current_Date_TrdType_O_OK_not_allowed_during_this_session_141651:
		return reject_code_strs[355];
	case reject_codes_enum::Post_Trade_Gateway__Volume___1_Round_lot_allowed_only_for_TrdType_OL_DL_141652:
		return reject_code_strs[356];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Firm_Contra_Firm___Executing_firm_is_the_same__141653:
		return reject_code_strs[357];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Settlement_Date_End_date___Start_date_141654:
		return reject_code_strs[358];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Settlement_Date_Start_End_date_not_specified_141655:
		return reject_code_strs[359];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Settlement_Date_Start_End_date_not_valid_for_TrdTyp_141656:
		return reject_code_strs[360];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Settlement_Date_Start_Date___Executed_date_141657:
		return reject_code_strs[361];
	case reject_codes_enum::Post_Trade_Gateway__SettlCurrency_entered_is_not_a_valid_ISO_currency_code_141658:
		return reject_code_strs[362];
	case reject_codes_enum::Post_Trade_Gateway__Trade_Cancellations_not_allowed__Post_Close__141659:
		return reject_code_strs[363];
	case reject_codes_enum::Post_Trade_Gateway__Trade_Sub_Types_are_not_supported_as_per_the_Setup_141660:
		return reject_code_strs[364];
	case reject_codes_enum::Post_Trade_Gateway__Trade_Cancellations_are_not_allowed_during_Post_Close_141661:
		return reject_code_strs[365];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Instrument_Trade_Type_Capacity_combination_141662:
		return reject_code_strs[366];
	case reject_codes_enum::Post_Trade_Gateway__Invalid_Tag_specified_in_TCR_141663:
		return reject_code_strs[367];
	case reject_codes_enum::Post_Trade_Gateway__Discrepancy_in_trade_terms__Start_End_date__141664:
		return reject_code_strs[368];
	case reject_codes_enum::Post_Trade_Gateway__Pre_Release_of_trades_not_allowed__Post_Close__141665:
		return reject_code_strs[369];
	case reject_codes_enum::Post_Trade_Gateway__Settlement_date_not_specified_141666:
		return reject_code_strs[370];
	default:
		DEBUG_ASSERT(!"Unknown reject code.");
		return "Unknown reject code.";
	};
}
}}}}}
#endif
