/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t= libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t= libjmmcg::msm::hash::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	using transition_table= typename msm_base_t::template rows<
		typename row_t::template row<
			src_msg_details_t::NewOrder_t::static_type,
			typename base_t::template convert_then_send_ref_data<typename src_msg_details_t::NewOrder_t, typename dest_msg_details_t::NewOrder_t>,
			dest_msg_details_t::NewOrder_t::static_type>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelRequest_t::static_type,
			typename base_t::template convert_then_send_ref_data<typename src_msg_details_t::OrderCancelRequest_t, typename dest_msg_details_t::OrderCancelRequest_t>,
			dest_msg_details_t::OrderCancelRequest_t::static_type>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelReplace_t::static_type,
			typename base_t::template convert_then_send_ref_data<typename src_msg_details_t::OrderCancelReplace_t, typename dest_msg_details_t::OrderCancelReplaceRequest_t>,
			dest_msg_details_t::OrderCancelReplaceRequest_t::static_type>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename src_msg_details_t::Reject_t, true, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::Reject_t::static_type> >;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline bool
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& dest_skt) {
	DEBUG_ASSERT(!ref_data_.empty());
	auto const& hdr= reinterpret_cast<typename src_msg_details_t::Header_t const&>(buff);
	const auto last_state= msm.process(hdr.type(), buff, exchg_skt, dest_skt);
	return last_state == dest_msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::string
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << "Source message details: ";
	SrcMsgDetails::to_stream(ss);
	ss << "\n\tDestination message details: ";
	DestMsgDetails::to_stream(ss);
	return ss.str();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t= libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t= libjmmcg::msm::hash::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	/**
		From section \todo 5.1: "TODO" of [1]: the response to an ExecutionReport is a various messages.

		\param	msg	The message that was received, that shall be processed.
		\param client_skt	The socket to which any responses should be written.
	*/
	class ExecutionReportResponse {
	public:
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		static inline constexpr const typename dest_msg_details_t::MsgTypes_t exit_values= static_cast<typename dest_msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename dest_msg_details_t::MsgTypes_t>::type>(dest_msg_details_t::ExecutionReport_t::static_type)
			| static_cast<typename std::underlying_type<typename dest_msg_details_t::MsgTypes_t>::type>(dest_msg_details_t::OrderRejected_t::static_type));

		explicit constexpr ExecutionReportResponse(isin_mapping_data_const_reference rd) noexcept(true)
			: ref_data_(rd) {
			DEBUG_ASSERT(!ref_data_.empty());
		}
		constexpr ExecutionReportResponse(ExecutionReportResponse const& nor) noexcept(true)
			: ref_data_(nor.ref_data_) {
			DEBUG_ASSERT(!ref_data_.empty());
		}

		template<auto state, auto>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const noexcept(false) {
			DEBUG_ASSERT(!ref_data_.empty());
			auto const& msg= reinterpret_cast<typename src_msg_details_t::ExecutionReport_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			switch(msg.orderStatus()) {
			case common::OrderStatus::New:
			case common::OrderStatus::Partiallyfilled:
			case common::OrderStatus::Filled:
			case common::OrderStatus::Cancelled:
			case common::OrderStatus::Expired:
			case common::OrderStatus::Suspended: {
				const typename dest_msg_details_t::ExecutionReport_t reply(msg, ref_data_);
				client_skt.write(reply);
				return dest_msg_details_t::ExecutionReport_t::static_type;
			}
			case common::OrderStatus::Rejected:
			default: {
				const typename dest_msg_details_t::OrderRejected_t reply(msg);
				client_skt.write(reply);
				return dest_msg_details_t::OrderRejected_t::static_type;
			}
			}
		}

		template<auto state, auto next>
		REALLY_FORCE_INLINE end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		isin_mapping_data_const_reference ref_data_;
	};
	struct LogonReplyResponse {
		using end_states= typename dest_msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename src_msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		explicit constexpr LogonReplyResponse(isin_mapping_data_const_reference) noexcept(true) {}
		constexpr LogonReplyResponse(LogonReplyResponse const&) noexcept(true)= default;

		/**
			From section 5.3: "Terminating a connection" of [1]: the response to a Logout is to stop processing.
		*/
		template<auto state, auto next>
		end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t&) const noexcept(false) {
			auto const& msg= reinterpret_cast<typename src_msg_details_t::LogonReply_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			switch(msg.rejectCode()) {
			case src_msg_details_t::LogonReply_t::RejectCode_t::tag_SUCCESS:
				// TODO need to set as logged on.
				break;
			default:
				BOOST_THROW_EXCEPTION(libjmmcg::throw_api_exception<std::runtime_error>(fmt::format("Unrecognised LogonResponse: wrong message-type decoded as state or wrong reject_code (not tag_SUCESS). reject_code={}, reject_code (as string)='{}', tag_SUCCESS={}, tag_SUCCESS (as string)='{}', state={}, message type={}", static_cast<typename src_msg_details_t::LogonReply_t::RejectCodes_t>(msg.rejectCode()), libjmmcg::tostring(msg.rejectCode()), static_cast<typename src_msg_details_t::LogonReply_t::RejectCodes_t>(src_msg_details_t::LogonReply_t::RejectCode_t::tag_SUCCESS), libjmmcg::tostring(src_msg_details_t::LogonReply_t::RejectCode_t::tag_SUCCESS), libjmmcg::tostring(state), libjmmcg::tostring(msg.type())), static_cast<end_states (LogonReplyResponse::*)(typename src_msg_details_t::msg_buffer_t const&, socket_t&, socket_t&) const>(&LogonReplyResponse::process<state, next>)));
			}
			return next;
		}

		template<auto state, auto next>
		end_states
		process(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}
	};
	using transition_table= typename msm_base_t::template rows<
		/**
			From section 5.2.2: "Heartbeats" of [1]: the response to a server Heartbeat is a Heartbeat.
		*/
		typename row_t::template row<
			src_msg_details_t::ServerHeartbeat_t::static_type,
			typename base_t::template just_send_to_exchg<typename src_msg_details_t::ClientHeartbeat_t>,
			dest_msg_details_t::ClientHeartbeat_t::static_type>,
		/**
			From section \todo 5.1: "TODO" of [1]: the response to an Reject is a client Reject.

			\param	msg	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
		*/
		typename row_t::template row<
			src_msg_details_t::Reject_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::Reject_t, typename dest_msg_details_t::OrderRejected_t>,
			dest_msg_details_t::OrderRejected_t::static_type>,
		/**
			From section 5.3: "Terminating a connection" of [1]: the response to a Logout is to stop processing.
		*/
		typename row_t::template row<
			src_msg_details_t::Logout_t::static_type,
			typename base_t::template no_op<typename dest_msg_details_t::MsgTypes_t>,	 // TODO
			dest_msg_details_t::MsgTypes_t::Exit>,
		typename row_t::template row<
			src_msg_details_t::ExecutionReport_t::static_type,
			ExecutionReportResponse,
			ExecutionReportResponse::exit_values>,
		/**
			From section \todo 5.1: "TODO" of [1]: the response to an OrderCancelReject is a client OrderCancelReject.
		*/
		typename row_t::template row<
			src_msg_details_t::OrderCancelReject_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::OrderCancelReject_t, typename dest_msg_details_t::CancelRejected_t>,
			dest_msg_details_t::CancelRejected_t::static_type>,
		typename row_t::template row<
			src_msg_details_t::LogonReply_t::static_type,
			LogonReplyResponse,
			dest_msg_details_t::LogonRequest_t::static_type>,
		/**
			From section \todo 5.1: "TODO" of [1]: the response to an OrderCancelReject is a client OrderCancelReject.

			\param	msg	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
		*/
		typename row_t::template row<
			src_msg_details_t::BusinessReject_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::BusinessReject_t, typename dest_msg_details_t::BusinessMessageReject_t>,
			dest_msg_details_t::BusinessMessageReject_t::static_type>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename dest_msg_details_t::Reject_t, true, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::Reject_t::static_type> >;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline bool
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) {
	DEBUG_ASSERT(!ref_data_.empty());
	auto const& hdr= reinterpret_cast<typename src_msg_details_t::Header_t const&>(buff);
	const auto last_state= msm.process(hdr.type(), buff, exchg_skt, client_cxn);
	return last_state == dest_msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::string
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream os;
	os << "Source message details: ";
	SrcMsgDetails::to_stream(os);
	os << "\n\tDestination message details: ";
	DestMsgDetails::to_stream(os);
	return os.str();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

template<class SrcMsgDetails, class SktT>
struct simulator_responses<SrcMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t= libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t= libjmmcg::msm::hash::row_types<typename msg_details_t::MsgTypes_t, typename msg_details_t::MsgTypes_t>;
	struct LogonRequestResponse {
		using end_states= typename msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		explicit constexpr LogonRequestResponse(simulator_responses const&) noexcept(true) {}
		constexpr LogonRequestResponse(LogonRequestResponse const&) noexcept(true)= default;

		/**
			From section 5.1: "Establishing a connection" of [1]: the response to a Logon is a LogonReply.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const noexcept(false) {
			auto const& msg= reinterpret_cast<typename msg_details_t::LogonRequest_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			typename msg_details_t::LogonReply_t reply;
			if(std::strcmp(msg.userName.begin(), simulator_responses::username.begin()) == 0) {
				if(std::strcmp(msg.password.begin(), simulator_responses::password.begin()) == 0) {
					reply.rejectCode(msg_details_t::LogonReply_t::RejectCode_t::tag_SUCCESS);
					client_skt.write(reply);
				} else {
					reply.rejectCode(msg_details_t::LogonReply_t::invalid_logon_details);
					client_skt.write(reply);
				}
			} else {
				reply.rejectCode(msg_details_t::LogonReply_t::unknown_user);
				client_skt.write(reply);
			}
			return next;
		}

		template<auto state, auto next>
		constexpr end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}
	};
	struct LogoutRequestResponse {
		using end_states= typename msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		explicit constexpr LogoutRequestResponse(simulator_responses const&) noexcept(true) {}
		constexpr LogoutRequestResponse(LogoutRequestResponse const&) noexcept(true)= default;

		/**
			From section 5.3: "Terminating a connection" of [1]: the response to a LogoutRequest is a Logout.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t& client_skt) const noexcept(false) {
			client_skt.write(typename msg_details_t::Logout_t(simulator_responses::logout_reason));
			std::this_thread::sleep_for(simulator_responses::logout_timeout);
			return next;
		}

		template<auto state, auto next>
		constexpr end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}
	};
	class OrderCancelRequestResponse {
	public:
		using end_states= typename msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values= static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::ExecutionReport_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderCancelReject_t::static_type));

		explicit OrderCancelRequestResponse(simulator_responses& sr) noexcept(true)
			: sim_resp(sr) {
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
		}
		constexpr OrderCancelRequestResponse(OrderCancelRequestResponse const& nor) noexcept(true)
			: sim_resp(nor.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a OrderCancelRequest.

			\return The next state for the state machine.
		*/
		template<auto state, auto>
		end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const noexcept(false) {
			auto const& msg= reinterpret_cast<typename msg_details_t::OrderCancelRequest_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			DEBUG_ASSERT(!std::string(msg.originalClientOrderID().data()).empty());
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
			auto const cancelled_order= sim_resp.order_book.find(msg.originalClientOrderID());
			if(cancelled_order != sim_resp.order_book.end()) {
				typename msg_details_t::ExecutionReport_t reply(
					sim_resp.sequenceNumber,
					msg.originalClientOrderID(),
					msg_details_t::AppID::Partition1,
					msg_details_t::ExecType::Cancelled,
					cancelled_order->second.limitPrice(),
					cancelled_order->second.instrumentID(),
					cancelled_order->second.side());
				reply.orderStatus(msg_details_t::OrderStatus::Cancelled);
				reply.executedQty(0);
				reply.leavesQty(cancelled_order->second.orderQty());
				sim_resp.order_book.erase(msg.originalClientOrderID());
				client_skt.write(reply);
				return msg_details_t::ExecutionReport_t::static_type;
			} else {
				const typename msg_details_t::OrderCancelReject_t reply(sim_resp.sequenceNumber, msg.originalClientOrderID());
				client_skt.write(reply);
				return msg_details_t::OrderCancelReject_t::static_type;
			}
		}

		template<auto state, auto next>
		constexpr end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses& sim_resp;
	};
	class OrderCancelReplaceRequestResponse {
	public:
		using end_states= typename msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values= static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::ExecutionReport_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderCancelReject_t::static_type));

		explicit OrderCancelReplaceRequestResponse(simulator_responses& sr) noexcept(true)
			: sim_resp(sr) {
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
		}
		constexpr OrderCancelReplaceRequestResponse(OrderCancelReplaceRequestResponse const& nor) noexcept(true)
			: sim_resp(nor.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a OrderCancelReplaceRequest.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto>
		end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const noexcept(false) {
			auto const& msg= reinterpret_cast<typename msg_details_t::OrderCancelReplaceRequest_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
			auto const modified_order= sim_resp.order_book.find(msg.originalClientOrderID());
			if(modified_order != sim_resp.order_book.end()) {
				modified_order->second.limitPrice(msg.limitPrice());
				modified_order->second.orderQty(msg.orderQty());
				modified_order->second.tif(msg.tif());
				modified_order->second.side(msg.side());
				typename msg_details_t::ExecutionReport_t reply(
					sim_resp.sequenceNumber,
					msg.originalClientOrderID(),
					msg_details_t::AppID::Partition1,
					msg_details_t::ExecType::Replaced,
					modified_order->second.limitPrice(),
					modified_order->second.instrumentID(),
					modified_order->second.side());
				reply.orderStatus(msg_details_t::OrderStatus::Cancelled);
				reply.executedQty(0);
				reply.leavesQty(modified_order->second.orderQty());
				client_skt.write(reply);
				return msg_details_t::ExecutionReport_t::static_type;
			} else {
				const typename msg_details_t::OrderCancelReject_t reply(sim_resp.sequenceNumber, msg.originalClientOrderID());
				client_skt.write(reply);
				return msg_details_t::OrderCancelReject_t::static_type;
			}
		}

		template<auto state, auto next>
		constexpr end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses& sim_resp;
	};
	class NewOrderResponse {
	public:
		using end_states= typename msg_details_t::MsgTypes_t;
		using arguments_types= std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, socket_t&>,
				std::tuple<typename msg_details_t::msg_buffer_t const&, socket_t&, exchanges::common::socket_type::client_cxn_ptr_t&> > >;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values= static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::ExecutionReport_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::BusinessReject_t::static_type));

		explicit NewOrderResponse(simulator_responses& sr) noexcept(true)
			: sim_resp(sr) {
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
		}
		constexpr NewOrderResponse(NewOrderResponse const& nor) noexcept(true)
			: sim_resp(nor.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a NewOrder.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t&, socket_t& client_skt) const noexcept(false) {
			auto const& msg= reinterpret_cast<typename msg_details_t::NewOrder_t const&>(buff);
			DEBUG_ASSERT(msg.type() == state);
			DEBUG_ASSERT(dynamic_cast<simulator_responses*>(&sim_resp));
			if(msg.instrumentID() == simulator_responses::instrumentID) {
				typename msg_details_t::ExecutionReport_t reply(
					sim_resp.sequenceNumber,
					msg.clientOrderID(),
					msg_details_t::AppID::Partition1,
					msg_details_t::ExecType::New,
					simulator_responses::scaled_price,
					msg.instrumentID(),
					msg.side());
				if(msg.orderQty() <= simulator_responses::quantity_limit) {
					if(msg.orderType() == msg_details_t::OrderType::Market
						|| (msg.side() == msg_details_t::Side::Buy && msg.limitPrice() < simulator_responses::scaled_price)
						|| (msg.side() == msg_details_t::Side::Sell && msg.limitPrice() > simulator_responses::scaled_price)) {
						reply.orderStatus(msg_details_t::OrderStatus::Filled);
						reply.executedQty(msg.orderQty());
						reply.leavesQty(0);
						reply.executedPrice(msg.limitPrice());
						client_skt.write(reply);
					} else {
						sim_resp.order_book.emplace(msg.clientOrderID(), msg);
						DEBUG_ASSERT(!sim_resp.order_book.empty());
						DEBUG_ASSERT(sim_resp.order_book.find(msg.clientOrderID()) != sim_resp.order_book.end());
						reply.orderStatus(msg_details_t::OrderStatus::New);
						reply.executedQty(0);
						reply.leavesQty(msg.orderQty());
						reply.executedPrice(0);
						client_skt.write(reply);
					}
				} else {
					auto new_item= sim_resp.order_book.emplace(msg.clientOrderID(), msg);
					new_item.first->second.orderQty(msg.orderQty() - simulator_responses::quantity_limit);
					reply.orderStatus(msg_details_t::OrderStatus::Partiallyfilled);
					reply.executedQty(simulator_responses::quantity_limit);
					reply.leavesQty(msg.orderQty() - simulator_responses::quantity_limit);
					DEBUG_ASSERT(!sim_resp.order_book.empty());
					DEBUG_ASSERT(sim_resp.order_book.find(msg.clientOrderID()) != sim_resp.order_book.end());
					client_skt.write(reply);
				}
				return msg_details_t::ExecutionReport_t::static_type;
			} else {
				typename msg_details_t::BusinessReject_t reply(
					sim_resp.sequenceNumber,
					msg.clientOrderID());
				reply.rejectCode(msg_details_t::BusinessReject_t::unknown_instrument);
				client_skt.write(reply);
				return msg_details_t::BusinessReject_t::static_type;
			}
		}

		template<auto state, auto next>
		constexpr end_states
		process(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses& sim_resp;
	};
	using transition_table= typename msm_base_t::template rows<
		typename row_t::template row<
			msg_details_t::LogonRequest_t::static_type,
			LogonRequestResponse,
			msg_details_t::LogonReply_t::static_type>,
		/**
			From section 5.2.2: "Heartbeats" of [1]: the response to a Heartbeat is nothing.
		*/
		typename row_t::template row<
			msg_details_t::ClientHeartbeat_t::static_type,
			typename base_t::template no_op<typename msg_details_t::MsgTypes_t>,
			msg_details_t::ServerHeartbeat_t::static_type>,
		typename row_t::template row<
			msg_details_t::LogoutRequest_t::static_type,
			LogoutRequestResponse,
			msg_details_t::MsgTypes_t::Exit>,
		typename row_t::template row<
			msg_details_t::OrderCancelRequest_t::static_type,
			OrderCancelRequestResponse,
			OrderCancelRequestResponse::exit_values>,
		typename row_t::template row<
			msg_details_t::OrderCancelReplaceRequest_t::static_type,
			OrderCancelReplaceRequestResponse,
			OrderCancelReplaceRequestResponse::exit_values>,
		typename row_t::template row<
			msg_details_t::NewOrder_t::static_type,
			NewOrderResponse,
			NewOrderResponse::exit_values>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename msg_details_t::Reject_t, true, typename msg_details_t::MsgTypes_t>,
			msg_details_t::Reject_t::static_type> >;
};

template<class SrcMsgDetails, class SktT>
inline bool
simulator_responses<SrcMsgDetails, SktT>::process_msg(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt) {
	++(this->sequenceNumber);
	auto const& hdr= reinterpret_cast<typename msg_details_t::Header_t const&>(buff);
	const auto last_state= msm.process(hdr.type(), buff, exchg_skt, client_skt);
	return last_state == msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class SktT>
inline std::string
simulator_responses<SrcMsgDetails, SktT>::to_string() const noexcept(false) {
	return fmt::format(
		"{}\n\tValid instrument ID={}\n\tInvalid instrument ID={}\n\tLogout reason: '{}'\n\tLogout timeout=",
		base_t::to_string(),
		instrumentID,
		invalidInstrumentID,
		std::string_view(logout_reason.begin(), std::find(logout_reason.begin(), logout_reason.end(), '\0')),
		logout_timeout);
}

template<class SrcMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, simulator_responses<SrcMsgDetails, SktT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}}}}}
