/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

template<class LogonT> inline
connectivity_policy<LogonT>::connectivity_policy(gateways_t const &gws, logon_args_t const &logon, logoff_args_t const &logoff) noexcept(true)
: gateways(gws), logon_args(logon), logoff_args(logoff) {
}

template<class LogonT>
template<class ConnectFn> inline void
connectivity_policy<LogonT>::operator()(ConnectFn const &cnx) const noexcept(false) {
	if (connection_failed(cnx, gateways.primary_gateway)) {
		std::exception_ptr cnx_failed(connection_failed(cnx, gateways.secondary_gateway));
		if (cnx_failed) {
			std::rethrow_exception(cnx_failed);
		}
	}
}

template<class LogonT>
template<class ConnectFn> inline std::exception_ptr
connectivity_policy<LogonT>::connection_failed(ConnectFn const &cnx, endpoint_t const &endpoint) noexcept(true) {
	std::exception_ptr cnx_failed{};
	unsigned i=0;
	for (; i<max_attempts; ++i) {
		try {
			cnx(boost::asio::ip::tcp::endpoint(endpoint.first, endpoint.second));
			break;
		} catch (...) {
			cnx_failed=std::current_exception();
			std::this_thread::sleep_for(min_timeout);
		}
	}
	return (i<max_attempts) ? std::exception_ptr() : cnx_failed;
}

template<class MsgT>
template<class ClientCxn>
inline
server_hb_t<MsgT>::server_hb_t(ClientCxn &cxn, typename base_t::report_error_fn_t &report_error)
: base_t(cxn, report_error, []() {return typename base_t::hb_t{};}) {
}

} } } } }
