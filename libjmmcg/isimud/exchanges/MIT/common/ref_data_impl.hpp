/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

constexpr inline
ref_data::security_id_key::security_id_key(element_type const &isin) noexcept(true)
: isin_(isin) {
}

inline
ref_data::security_id_key::security_id_key(ref_data::MF_Record const &mfr) noexcept(true)
: isin_(mfr.isin) {
}

inline bool
ref_data::security_id_key::operator==(security_id_key const &sik) const noexcept(true) {
	return isin_==sik.isin_;
}

inline bool
ref_data::security_id_key::operator<(security_id_key const &sik) const noexcept(true) {
	return isin_<sik.isin_;
}

inline std::string
ref_data::security_id_key::to_string() const noexcept(false) {
	std::ostringstream os;
	os
		<<"ISIN="<<isin_.to_string();
	return os.str();
}

inline
ref_data::ref_data(std::istream &is) noexcept(false)
: cont_(
	[&is]() {
		libjmmcg::line_iterator line_it(is), eof;
		std::vector<std::pair<security_id_key, exchanges::MIT::common::SecurityID_t>> key_to_mapped;
		while (line_it!=eof) {
			libjmmcg::csv_iterator semicolon_it(*line_it, ';');
			std::string sid=*semicolon_it;
			DEBUG_ASSERT(!sid.empty());
			std::string isin=*std::next(semicolon_it, isin_field);
			DEBUG_ASSERT(!isin.empty());
			const MF_Record mfr{
				boost::lexical_cast<common::SecurityID_t>(sid),
				boost::lexical_cast<security_id_key::element_type>(isin),
			};
			key_to_mapped.emplace_back(security_id_key(mfr), mfr.instrument);
			++line_it;
		}
		return container_type(key_to_mapped.begin(), key_to_mapped.end());
	}()
) {
}

inline bool
ref_data::empty() const noexcept(true) {
	return cont_.empty();
}

inline ref_data::size_type
ref_data::size() const noexcept(true) {
	return cont_.size();
}

inline auto const &
ref_data::at(auto const &k) const noexcept(false) {
	return cont_.at(k);
}

inline std::string
ref_data::to_string() const noexcept(false) {
	std::ostringstream os;
	os<<cont_;
	return os.str();
}

inline std::ostream &
operator<<(std::ostream &os, ref_data::security_id_key const &sik) noexcept(false) {
	os<<sik.to_string();
	return os;
}

inline std::ostream &
operator<<(std::ostream &os, isin_mapping_data_const_reference rd) noexcept(false) {
	os<<rd.to_string();
	return os;
}

} } } } }
