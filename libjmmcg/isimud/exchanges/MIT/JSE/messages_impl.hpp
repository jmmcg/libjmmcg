/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace JSE {

inline constexpr TransmissionComplete::TransmissionComplete(const common::Status::element_type s) noexcept(true)
	: Header_t(static_cast<TransmissionComplete const*>(nullptr)), status(s) {}

inline constexpr Logon::Logon(const logon_args_t::UserName_t& UN, const logon_args_t::Password_t& P, const logon_args_t::Password_t& NP) noexcept(true)
	: Header_t(static_cast<Logon const*>(nullptr)), userName(UN), password(P), newPassword(NP) {
}

inline constexpr Logon::Logon(logon_args_t const& a) noexcept(true)
	: Logon(a.username, a.password, a.new_password) {
}

inline LogonResponse::LogonResponse() noexcept(true)
	: Header_t(static_cast<LogonResponse const*>(nullptr)) {}

struct LogonResponse::respond {
	template<class ReplyMsg, class Op>
	void
	operator()(ReplyMsg const& msg, Op const& o) const noexcept(false) {
		switch(msg.rejectCode()) {
		case ReplyMsg::logon_success:
			o.operator()();
			break;
		default: {
			std::ostringstream os;
			os << "Failed to logon. Reject code='" << msg.rejectCode() << "'";
			BOOST_THROW_EXCEPTION(std::runtime_error(os.str()));
		}
		}
	}
};

inline constexpr NewOrder::NewOrder(common::SeqNum_t, common::ClientOrderID_t const& clID, common::OrderType::element_type const oT, common::TIF::element_type const t, common::Side::element_type const s, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t p) noexcept(true)
	: Header_t(static_cast<NewOrder const*>(nullptr)),
	  clientOrderID_(clID),
	  instrumentID_(instID),
	  orderType_(oT),
	  tif_(t),
	  side_(s),
	  orderQty_(ordQty),
	  limitPrice_(p) {
}

inline OrderCancelReplaceRequest::OrderCancelReplaceRequest(common::ClientOrderID_t const& clID, common::ClientOrderID_t const& origclID, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t const p, common::TIF::element_type t, common::Side::element_type s) noexcept(true)
	: Header_t(static_cast<OrderCancelReplaceRequest const*>(nullptr)),
	  instrumentID_(instID),
	  tif_(t),
	  side_(s),
	  orderQty_(ordQty),
	  limitPrice_(p) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	libjmmcg::memcpy_opt(origclID, originalClientOrderID_);
}

inline constexpr NewOrderCross::NewOrderCross(common::ClientOrderID_t const& origclID, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t const p, common::TIF::element_type t) noexcept(true)
	: Header_t(static_cast<NewOrderCross const*>(nullptr)),
	  buySideClientOrderID(origclID),
	  instrumentID_(instID),
	  tif_(t),
	  limitPrice_(p),
	  orderQty_(ordQty) {
}

inline ExecutionReport::ExecutionReport() noexcept(true)
	: Header_t(static_cast<ExecutionReport const*>(nullptr)) {
}

inline constexpr ExecutionReport::ExecutionReport(std::int32_t seqNum, common::ClientOrderID_t const& clID, common::AppID::element_type aID, common::ExecType::element_type eT, common::Price_t const price, common::SecurityID_t instID, common::Side::element_type s) noexcept(true)
	: Header_t(static_cast<ExecutionReport const*>(nullptr)),
	  partitionID(aID),
	  sequenceNumber(seqNum),
	  clientOrderID_(clID),
	  execType_(eT),
	  executedPrice_(price),
	  instrumentID_(instID),
	  side_(s) {
}

}}}}}
