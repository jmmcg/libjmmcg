#ifndef ISIMUD_EXCHANGES_MIT_JSE_MESSAGES_HPP
#define ISIMUD_EXCHANGES_MIT_JSE_MESSAGES_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "mit_jse_config.h"
#include "reject_codes.hpp"

#include "../common/messages.hpp"
#include "../common/ref_data.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

/**
	From <a href="https://www.jse.co.za/content/JSETechnologyDocumentItems/Volume%2001%20-%20Native%20Trading%20Gateway%20v3.03.pdf">"JSE Specification Document Volume 01 - Native Trading Gateway", Version 3.01, 26 April 2016</a>.
*/
namespace JSE {

using CompID_t= std::array<char, 6>;
using ExecutionID_t= std::array<char, 21>;
using ExpireDateTime_t= std::array<char, 17>;
using Password_t= std::array<char, 10>;
using PasswordExpiry_t= std::int32_t;
using TraderMnemonic_t= std::array<char, 17>;
using Segment_t= std::array<char, 6>;

struct logon_args_t {
	using UserName_t= JSE::CompID_t;
	using Password_t= JSE::Password_t;

	const UserName_t username{{}};
	const Password_t password{{}};
	const Password_t new_password{{}};
};

/**
	Section: "6.4.8 Transmission Complete"
*/
struct [[gnu::packed]] TransmissionComplete : public common::Header {
	using Header_t= common::Header;
	static inline common::MsgType::element_type constexpr const static_type= common::MsgType::MissedMessageReport;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};

	const common::Status::element_type status;

	explicit constexpr TransmissionComplete(const common::Status::element_type s) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall TransmissionComplete(SrcMsg const& msg) noexcept(true)= delete;
};

/**
	Section: "6.4.1 Logon"
*/
struct [[gnu::packed]] Logon : public common::Header {
	using Header_t= common::Header;
	using logon_args_t= JSE::logon_args_t;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::LogonRequest;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};

	const logon_args_t::UserName_t userName;
	const logon_args_t::Password_t password;
	const logon_args_t::Password_t newPassword;
	static inline constexpr const std::uint8_t messageVersion= 1;

	constexpr Logon(const logon_args_t::UserName_t& UN, const logon_args_t::Password_t& P, const logon_args_t::Password_t& NP) noexcept(true);
	explicit constexpr Logon(logon_args_t const& a) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall Logon(SrcMsg const& msg) noexcept(true)= delete;
};

/**
	Section: "6.4.2 Logon Response"
*/
struct [[gnu::packed]] LogonResponse : public common::Header {
	using Header_t= common::Header;
	using RejectCode_t= mit_jse::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::LogonReply;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};
	/// Allow a client connected to the exchange to process the LogonReponse message.
	struct respond;

	static inline constexpr const RejectCode_t logon_success= mit_jse::reject_codes_enum::tag_SUCCESS;
	static inline constexpr const RejectCode_t invalid_logon_details= mit_jse::reject_codes_enum::Native_Trading_Gateway_1_Invalid_User_ID_or_password;
	static inline constexpr const RejectCode_t unknown_user= mit_jse::reject_codes_enum::Native_Trading_Gateway_1_Invalid_User_ID_or_password;

	RejectCode_t rejectCode_;
	PasswordExpiry_t passwordExpiry;

	LogonResponse() noexcept(true);

	RejectCode_t rejectCode() const noexcept(true) {
		return rejectCode_;
	}
	void rejectCode(RejectCode_t const& rc) noexcept(true) {
		rejectCode_= rc;
	}
};

/**
	Section: "6.5.1 New Order"
*/
struct [[gnu::packed]] NewOrder : public common::Header {
	using Header_t= common::Header;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::NewOrder;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};
	using order_qty_t= std::int32_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	common::ClientOrderID_t clientOrderID_;
	common::SecurityID_t instrumentID_;
	TraderMnemonic_t traderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};
	common::Account_t account{"\0\0\0\0\0\0\0\0\0"};
	common::OrderType::element_type orderType_;
	common::TIF::element_type tif_;
	ExpireDateTime_t expireDateTime{};
	common::Side::element_type side_;
	order_qty_t orderQty_;
	order_qty_t displayQty{};
	order_qty_t minimumQty{};
	common::Price_t limitPrice_;
	common::Price_t stoppedPrice= 0;
	common::Capacity::element_type capacity= common::Capacity::Principal;
	const common::AutoCancel::element_type cancelOnDisconnect= common::AutoCancel::Cancel;
	const common::OrderBook::element_type orderBook= common::OrderBook::Regular;
	common::ExecutionInstruction::element_type executionInstruction= common::ExecutionInstruction::DoNotExcludeHiddenOrders;
	common::OrderSubType::element_type orderSubType= common::OrderSubType::Order;

	constexpr __stdcall NewOrder(common::SeqNum_t, common::ClientOrderID_t const& clID, common::OrderType::element_type const oT, common::TIF::element_type const t, common::Side::element_type const s, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t p) noexcept(true);

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall NewOrder(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);

	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}

	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}

	common::OrderType::element_type orderType() const noexcept(true) {
		return orderType_;
	}
	void orderType(common::OrderType::element_type i) noexcept(true) {
		orderType_= i;
	}

	common::Side::element_type side() const noexcept(true) {
		return side_;
	}
	void side(common::Side::element_type i) noexcept(true) {
		side_= i;
	}

	common::ClientOrderID_t const& clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(common::ClientOrderID_t const& clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	common::Price_t limitPrice() const noexcept(true) {
		return limitPrice_;
	}
	void limitPrice(common::Price_t p) noexcept(true) {
		limitPrice_= p;
	}

	common::TIF::element_type tif() const noexcept(true) {
		return tif_;
	}
	void tif(common::TIF::element_type t) noexcept(true) {
		tif_= t;
	}
};

/**
	Section: "6.5.2 Order Cancel Request"
*/
struct [[gnu::packed]] OrderCancelRequestSpecific1 {
	common::SecurityID_t instrumentID_;
	TraderMnemonic_t traderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};

	explicit constexpr OrderCancelRequestSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};

/**
	Section: "6.5.3 Order Mass Cancel Request"
*/
struct [[gnu::packed]] OrderMassCancelRequestSpecific1 {
	common::SecurityID_t instrumentID_;
	Segment_t segment;

	explicit constexpr OrderMassCancelRequestSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID), segment{} {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};
struct [[gnu::packed]] OrderMassCancelRequestSpecific2 {
	common::TargetBook::element_type targetBook= common::TargetBook::DarkMidpointOrderBook;
};
/**
	Section: "6.5.4 Order Cancel/Replace Request"
*/
struct [[gnu::packed]] OrderCancelReplaceRequest : public common::Header {
	using Header_t= common::Header;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::OrderCancelReplaceRequest;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};
	using order_qty_t= std::int32_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	common::ClientOrderID_t clientOrderID_;
	common::ClientOrderID_t originalClientOrderID_;
	common::OrderID_t orderID{};
	common::SecurityID_t instrumentID_;
	TraderMnemonic_t traderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};
	common::Account_t account{"\0\0\0\0\0\0\0\0\0"};
	common::OrderType::element_type orderType_{};
	common::TIF::element_type tif_;
	ExpireDateTime_t expireDateTime{};
	common::Side::element_type side_;
	order_qty_t orderQty_;
	order_qty_t displayQty{};
	order_qty_t minimumQty{};
	common::Price_t limitPrice_;
	common::Price_t stoppedPrice= 0;
	const common::OrderBook::element_type orderBook= common::OrderBook::Regular;

	OrderCancelReplaceRequest(common::ClientOrderID_t const& clID, common::ClientOrderID_t const& origclID, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t const p, common::TIF::element_type t, common::Side::element_type s) noexcept(true);

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall OrderCancelReplaceRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);

	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}

	common::ClientOrderID_t const& clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(common::ClientOrderID_t const& clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	common::ClientOrderID_t const& originalClientOrderID() const noexcept(true) {
		return originalClientOrderID_;
	}
	void originalClientOrderID(common::ClientOrderID_t const& clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, originalClientOrderID_);
	}

	common::Price_t limitPrice() const noexcept(true) {
		return limitPrice_;
	}
	void limitPrice(common::Price_t p) noexcept(true) {
		limitPrice_= p;
	}

	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}

	common::Side::element_type side() const noexcept(true) {
		return side_;
	}
	void side(common::Side::element_type i) noexcept(true) {
		side_= i;
	}

	common::TIF::element_type tif() const noexcept(true) {
		return tif_;
	}
	void tif(common::TIF::element_type t) noexcept(true) {
		tif_= t;
	}
};

/**
	Section: "6.5.5 New Order Cross"
*/
struct [[gnu::packed]] NewOrderCross : public common::Header {
	using Header_t= common::Header;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::NewOrderCrossMessage;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};
	using order_qty_t= std::int32_t;

	common::CrossID_t crossID{};
	const common::CrossType::element_type crossType= common::CrossType::InternalCross;
	common::ClientOrderID_t buySideClientOrderID;
	common::Capacity::element_type buySideCapacity{};
	TraderMnemonic_t buySideTraderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};
	common::Account_t buySideccount{"\0\0\0\0\0\0\0\0\0"};
	common::ClientOrderID_t sellSideClientOrderID{};
	common::Capacity::element_type sellSideCapacity{};
	TraderMnemonic_t sellSideTraderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};
	common::Account_t sellSideAccount{"\0\0\0\0\0\0\0\0\0"};
	common::SecurityID_t instrumentID_;
	common::OrderType::element_type orderType_{};
	common::TIF::element_type tif_;
	common::Price_t limitPrice_;
	order_qty_t orderQty_;

	constexpr NewOrderCross(common::ClientOrderID_t const& origclID, common::SecurityID_t instID, order_qty_t ordQty, common::Price_t const p, common::TIF::element_type t) noexcept(true);

	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}

	common::Price_t limitPrice() const noexcept(true) {
		return limitPrice_;
	}
	void limitPrice(common::Price_t p) noexcept(true) {
		limitPrice_= p;
	}

	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}

	common::TIF::element_type tif() const noexcept(true) {
		return tif_;
	}
	void tif(common::TIF::element_type t) noexcept(true) {
		tif_= t;
	}
};

/**
	Section: "6.6.1 Execution Report"
*/
struct [[gnu::packed]] ExecutionReport : public common::Header {
	using Header_t= common::Header;

	static inline constexpr const common::MsgType::element_type static_type= common::MsgType::ExecutionReport;
	enum : std::size_t {
		header_t_size= sizeof(Header_t)
	};
	using RejectCode_t= mit_jse::reject_codes_enum;
	using order_qty_t= std::int32_t;

	common::AppID::element_type partitionID;
	std::int32_t sequenceNumber;
	ExecutionID_t executionID{};
	common::ClientOrderID_t clientOrderID_;
	common::OrderID_t orderID{};
	common::ExecType::element_type execType_;
	common::OrderStatus::element_type orderStatus_{};
	RejectCode_t orderRejectCode_{};
	common::Price_t executedPrice_;
	order_qty_t executedQty_{};
	order_qty_t leavesQty_{};
	common::Container::element_type container_{};
	common::SecurityID_t instrumentID_;
	common::Side::element_type side_;
	TraderMnemonic_t traderMnemonic{"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"};
	common::Account_t account{"\0\0\0\0\0\0\0\0\0"};
	common::IsMarketOpsRequest::element_type isMarketOpsRequest{};
	common::TransactTime_t transactTime{};
	const common::OrderBook::element_type orderBook= common::OrderBook::Regular;
	common::ExecInstruction::element_type execInstuction_{};
	common::CrossID_t crossID_{};
	common::CrossType::element_type crossType_{};

	ExecutionReport() noexcept(true);
	constexpr ExecutionReport(std::int32_t seqNum, common::ClientOrderID_t const& clID, common::AppID::element_type aID, common::ExecType::element_type eT, common::Price_t const price, common::SecurityID_t instID, common::Side::element_type s) noexcept(true);

	common::ExecType::element_type execType() const noexcept(true) {
		return execType_;
	}
	void execType(common::ExecType::element_type e) noexcept(true) {
		execType_= e;
	}

	common::ClientOrderID_t const& clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(common::ClientOrderID_t const& clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}

	common::Price_t executedPrice() const noexcept(true) {
		return executedPrice_;
	}
	void executedPrice(common::Price_t p) noexcept(true) {
		executedPrice_= p;
	}

	common::OrderStatus::element_type orderStatus() const noexcept(true) {
		return orderStatus_;
	}
	void orderStatus(common::OrderStatus::element_type os) noexcept(true) {
		orderStatus_= os;
	}

	int32_t executedQty() const noexcept(true) {
		return executedQty_;
	}
	void executedQty(std::int32_t eq) noexcept(true) {
		executedQty_= eq;
	}

	int32_t leavesQty() const noexcept(true) {
		return leavesQty_;
	}
	void leavesQty(std::int32_t eq) noexcept(true) {
		leavesQty_= eq;
	}

	common::Side::element_type side() const noexcept(true) {
		return side_;
	}
	void side(common::Side::element_type s) noexcept(true) {
		side_= s;
	}

	RejectCode_t orderRejectCode() const noexcept(true) {
		return orderRejectCode_;
	}
	void orderRejectCode(RejectCode_t r) noexcept(true) {
		orderRejectCode_= r;
	}
};

/**
	Section: "6.6.2 Order Cancel Reject"
*/
struct [[gnu::packed]] OrderCancelRejectSpecific {
	using RejectCode_t= mit_jse::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;

	common::TransactTime_t transactTime;
	RejectCode_t cancelRejectReason_;
	const common::OrderBook::element_type orderBook= common::OrderBook::Regular;
};

/**
	Section: "6.6.3 Order Mass Cancel Report"
*/
struct [[gnu::packed]] OrderMassCancelReportSpecific {
	using RejectCode_t= mit_jse::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;

	common::TransactTime_t transactTime;
	const common::OrderBook::element_type orderBook= common::OrderBook::Regular;
};

/**
	Section: "6.9.2 Business Reject"
*/
struct [[gnu::packed]] BusinessRejectSpecific {
	using RejectCode_t= mit_jse::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	static inline constexpr const RejectCode_t unknown_instrument= mit_jse::reject_codes_enum::MatchingEngine_9000_Unknown_instrument;
};

struct [[gnu::packed]] OrderCancelRequest : public common::OrderCancelRequest<OrderCancelRequestSpecific1> {
	using base_t= common::OrderCancelRequest<OrderCancelRequestSpecific1>;
	using base_t::base_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	explicit __stdcall OrderCancelRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct MsgTypes {
	static inline constexpr const exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE;

	using ref_data= common::ref_data;
	template<class Op>
	static ref_data create_ref_data(Op&& op) noexcept(true) {
		auto&& ref_data_src= op();
		return ref_data{ref_data_src};
	}
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	using MsgType_t= common::MsgType_t;
	using MsgTypes_t= common::MsgType::element_type;
	using UserName_t= JSE::logon_args_t::UserName_t;
	using Password_t= JSE::logon_args_t::Password_t;
	using SecurityID_t= common::SecurityID_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= NewOrder::order_qty_t;
	using ClientOrderID_t= common::ClientOrderID_t;
	using OrderType= common::OrderType::element_type;
	using Side= common::Side::element_type;
	using TIF= common::TIF::element_type;
	using ExecType= common::ExecType::element_type;
	using AppID= common::AppID::element_type;
	using OrderStatus= common::OrderStatus::element_type;
	using logon_args_t= JSE::logon_args_t;

	using Header_t= common::Header;
	using LogonRequest_t= JSE::Logon;
	using LogonReply_t= JSE::LogonResponse;
	using LogoutRequest_t= common::LogoutRequest;
	using Logout_t= LogoutRequest_t;
	using ClientHeartbeat_t= common::Heartbeat;
	using ServerHeartbeat_t= common::Heartbeat;
	using Reject_t= common::Reject<mit_jse::reject_codes_enum, static_cast<mit_jse::reject_codes_enum>(102)>;
	using MissedMessageRequest_t= common::MissedMessageRequest;
	using MissedMessageRequestAck_t= common::MissedMessageRequestAck;
	using MissedMessageReport_t= common::MissedMessageReport;
	using TransmissionComplete_t= JSE::TransmissionComplete;
	using SystemStatus_t= common::SystemStatus;
	using NewOrder_t= JSE::NewOrder;
	using OrderCancelRequest_t= JSE::OrderCancelRequest;
	using OrderMassCancelRequest_t= common::OrderMassCancelRequest<OrderMassCancelRequestSpecific1, OrderMassCancelRequestSpecific2>;
	using OrderCancelReplaceRequest_t= JSE::OrderCancelReplaceRequest;
	using NewOrderCross_t= JSE::NewOrderCross;
	using ExecutionReport_t= JSE::ExecutionReport;
	using OrderCancelReject_t= common::OrderCancelReject<OrderCancelRejectSpecific>;
	using OrderMassCancelReport_t= common::OrderMassCancelReport<OrderMassCancelReportSpecific>;
	using BusinessReject_t= common::BusinessReject<BusinessRejectSpecific>;

	using client_to_exchange_messages_t= boost::mpl::vector<
		NewOrder_t,
		OrderCancelRequest_t,
		OrderMassCancelRequest_t,
		OrderCancelReplaceRequest_t,
		NewOrderCross_t,
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		MissedMessageRequest_t>;

	using exchange_to_client_messages_t= boost::mpl::vector<
		TransmissionComplete_t,
		ExecutionReport_t,
		OrderCancelReject_t,
		OrderMassCancelReport_t,
		BusinessReject_t,
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		MissedMessageReport_t,
		MissedMessageRequestAck_t,
		Reject_t,
		SystemStatus_t>;

	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= LogonRequest_t::header_t_size
	};
	BOOST_MPL_ASSERT_RELATION(max_msg_size, >=, header_t_size);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const Price_t implied_decimal_places= common::implied_decimal_places;

	template<class ConnPolT, class SktT, class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return common::make_ctor_args<MsgTypes, ConnPolT, SktT, ThrdT>(vm);
	}

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test MIT JSE size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 30);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 12);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), ==, 24);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Reject_t), ==, 59);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequest_t), ==, 9);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequestAck_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TransmissionComplete_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::SystemStatus_t), ==, 6);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrder_t), ==, 108);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelRequest_t), ==, 88);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelRequest_t), ==, 37);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReplaceRequest_t), ==, 136);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrderCross_t), ==, 139);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ExecutionReport_t), ==, 149);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReject_t), ==, 54);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelReport_t), ==, 43);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::BusinessReject_t), ==, 54);

}

}
}}}}

#include "messages_impl.hpp"

#endif
