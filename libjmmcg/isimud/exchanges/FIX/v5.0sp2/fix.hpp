#ifndef ISIMUD_EXCHANGES_FIX_v5_0_sp5_fix_hpp
#define ISIMUD_EXCHANGES_FIX_v5_0_sp5_fix_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

#include "../common/connectivity_policy.hpp"

#include "../../common/connection.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

using connection_t= exchanges::common::connection<MsgTypes, common::connectivity_policy<MsgTypes::logon_args_t>>;

}}}}}

extern template class FIX_FIX_V5_0SP2_EXPORT libisimud::exchanges::common::connection<libisimud::exchanges::FIX::v5_0sp2::MsgTypes, libisimud::exchanges::FIX::common::connectivity_policy<libisimud::exchanges::FIX::v5_0sp2::MsgTypes::logon_args_t>>;

#endif
