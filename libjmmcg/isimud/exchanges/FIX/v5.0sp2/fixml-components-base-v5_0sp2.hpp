#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_COMPONENTS_BASE_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_COMPONENTS_BASE_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: 2020-03-11T03:21:37.364Z

#include "fixml-fields-fast-v5_0sp2.hpp"
#include "fixml-fields-impl-5-0-SP2.hpp"

#define ISIMUD_FIXML_COMPONENTS_BASE_HDR_GENERATED_DATE "2020-03-11T03:21:37.364Z"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

struct BaseHeaderElements {

	const auto BodyLengthTag=libjmmcg::enum_tags::mpl::to_array<BodyLength>::value;
	const auto MsgTypeTag=libjmmcg::enum_tags::mpl::to_array<MsgType>::value;
	MsgType_t MsgType_;

};

struct HopGrpElements {

};

struct BatchHeaderElements {

};

struct BatchElements {

};

struct CommissionDataElements {

};

struct DiscretionInstructionsElements {

};

struct FinancingDetailsElements {

};

struct InstrumentElements {

};

struct InstrumentExtensionElements {

};

struct InstrumentLegElements {

};

struct LegBenchmarkCurveDataElements {

};

struct LegStipulationsElements {

};

struct NestedPartiesElements {

};

struct OrderQtyDataElements {

};

struct PartiesElements {

};

struct PegInstructionsElements {

};

struct PositionAmountDataElements {

};

struct PositionQtyElements {

};

struct SettlInstructionsDataElements {

};

struct SettlPartiesElements {

};

struct SpreadOrBenchmarkCurveDataElements {

};

struct StipulationsElements {

};

struct TrdRegTimestampsElements {

};

struct UnderlyingInstrumentElements {

};

struct YieldDataElements {

};

struct UnderlyingStipulationsElements {

};

struct NestedParties2Elements {

};

struct NestedParties3Elements {

};

struct ClrInstGrpElements {

};

struct CompIDReqGrpElements {

};

struct CompIDStatGrpElements {

};

struct ContAmtGrpElements {

};

struct ContraGrpElements {

};

struct ExecAllocGrpElements {

};

struct InstrmtGrpElements {

};

struct InstrmtLegExecGrpElements {

};

struct InstrmtLegGrpElements {

};

struct InstrmtLegIOIGrpElements {

};

struct InstrmtLegSecListGrpElements {

};

struct InstrmtMDReqGrpElements {

};

struct LegOrdGrpElements {

};

struct LegPreAllocGrpElements {

};

struct LinesOfTextGrpElements {

};

struct MiscFeesGrpElements {

};

struct OrdAllocGrpElements {

};

struct PreAllocGrpElements {

};

struct PreAllocMlegGrpElements {

};

struct RoutingGrpElements {

};

struct TrdgSesGrpElements {

};

struct UndInstrmtGrpElements {

};

struct EvntGrpElements {

};

struct SecAltIDGrpElements {

};

struct LegSecAltIDGrpElements {

};

struct UndSecAltIDGrpElements {

};

struct AttrbGrpElements {

};

struct DlvyInstGrpElements {

};

struct SettlPtysSubGrpElements {

};

struct PtysSubGrpElements {

};

struct NstdPtysSubGrpElements {

};

struct NstdPtys2SubGrpElements {

};

struct NstdPtys3SubGrpElements {

};

struct StrategyParametersGrpElements {

};

struct InstrumentPartiesElements {

};

struct InstrumentPtysSubGrpElements {

};

struct UndlyInstrumentPartiesElements {

};

struct UndlyInstrumentPtysSubGrpElements {

};

struct DisplayInstructionElements {

};

struct TriggeringInstructionElements {

};

struct RootPartiesElements {

};

struct RootSubPartiesElements {

};

struct TrdSessLstGrpElements {

};

struct MsgTypeGrpElements {

};

struct SecurityTradingRulesElements {

};

struct SettlDetailsElements {

};

struct TickRulesElements {

};

struct StrikeRulesElements {

};

struct MaturityRulesElements {

};

struct SecondaryPriceLimitsElements {

};

struct PriceLimitsElements {

};

struct MarketDataFeedTypesElements {

};

struct LotTypeRulesElements {

};

struct MatchRulesElements {

};

struct ExecInstRulesElements {

};

struct TimeInForceRulesElements {

};

struct OrdTypeRulesElements {

};

struct TradingSessionRulesElements {

};

struct TradingSessionRulesGrpElements {

};

struct BaseTradingRulesElements {

};

struct MarketSegmentGrpElements {

};

struct DerivativeInstrumentPartySubIDsGrpElements {

};

struct DerivativeInstrumentPartiesElements {

};

struct DerivativeInstrumentAttributeElements {

};

struct NestedInstrumentAttributeElements {

};

struct DerivativeInstrumentElements {

};

struct DerivativeSecurityAltIDGrpElements {

};

struct DerivativeEventsGrpElements {

};

struct DerivativeSecurityDefinitionElements {

};

struct RelSymDerivSecUpdGrpElements {

};

struct ApplicationSequenceControlElements {

};

struct NstdPtys4SubGrpElements {

};

struct NestedParties4Elements {

};

struct RateSourceElements {

};

struct TargetPartiesElements {

};

struct ComplexEventsElements {

};

struct ComplexEventDatesElements {

};

struct ComplexEventTimesElements {

};

struct MatchingInstructionsElements {

};

struct LimitAmtsElements {

};

struct MarginAmountElements {

};

struct RelatedInstrumentGrpElements {

};

struct PartyRelationshipGrpElements {

};

struct PartyDetailGrpElements {

};

struct PartyDetailAltIDGrpElements {

};

struct PartyDetailAltSubGrpElements {

};

struct InstrumentScopeElements {

};

struct InstrumentScopeSecurityAltIDGrpElements {

};

struct RelatedPartyDetailGrpElements {

};

struct RelatedPartyDetailSubGrpElements {

};

struct RelatedPartyDetailAltIDGrpElements {

};

struct RelatedPartyDetailAltSubGrpElements {

};

struct InstrumentScopeGrpElements {

};

struct RequestingPartyGrpElements {

};

struct RequestingPartySubGrpElements {

};

struct PartyDetailSubGrpElements {

};

struct SecurityClassificationGrpElements {

};

struct ThrottleParamsGrpElements {

};

struct ThrottleMsgTypeGrpElements {

};

struct ThrottleResponseElements {

};

struct CollateralAmountGrpElements {

};

struct MarketSegmentScopeGrpElements {

};

struct TargetMarketSegmentGrpElements {

};

struct AffectedMarketSegmentGrpElements {

};

struct NotAffectedMarketSegmentGrpElements {

};

struct DisclosureInstructionGrpElements {

};

struct TradeAllocAmtGrpElements {

};

struct TradePriceConditionGrpElements {

};

struct TradeQtyGrpElements {

};

struct TradePositionQtyElements {

};

struct RelatedTradeGrpElements {

};

struct RelatedPositionGrpElements {

};

struct ValueChecksGrpElements {

};

struct AdditionalTermBondRefGrpElements {

};

struct AdditionalTermGrpElements {

};

struct AllocRegulatoryTradeIDGrpElements {

};

struct CashSettlTermGrpElements {

};

struct FinancingContractualDefinitionGrpElements {

};

struct FinancingContractualMatrixGrpElements {

};

struct FinancingTermSupplementGrpElements {

};

struct LegEvntGrpElements {

};

struct LegPaymentScheduleGrpElements {

};

struct LegPaymentScheduleRateSourceGrpElements {

};

struct LegPaymentStreamElements {

};

struct LegPaymentStreamFixedRateElements {

};

struct LegPaymentStreamFloatingRateElements {

};

struct LegPaymentStreamNonDeliverableFixingDateGrpElements {

};

struct LegPaymentStreamNonDeliverableSettlTermsElements {

};

struct LegPaymentStreamPaymentDatesElements {

};

struct LegPaymentStreamResetDatesElements {

};

struct LegPaymentStubGrpElements {

};

struct LegProvisionCashSettlPaymentDatesElements {

};

struct LegProvisionCashSettlPaymentFixedDateGrpElements {

};

struct LegProvisionCashSettlValueDatesElements {

};

struct LegProvisionOptionExerciseFixedDateGrpElements {

};

struct LegProvisionOptionExerciseDatesElements {

};

struct LegProvisionOptionExpirationDateElements {

};

struct LegProvisionOptionRelevantUnderlyingDateElements {

};

struct LegProvisionGrpElements {

};

struct LegProvisionPartiesElements {

};

struct LegProvisionPtysSubGrpElements {

};

struct LegSecondaryAssetGrpElements {

};

struct LegSettlRateDisruptionFallbackGrpElements {

};

struct LegStreamCalculationPeriodDatesElements {

};

struct LegStreamEffectiveDateElements {

};

struct LegStreamGrpElements {

};

struct LegStreamTerminationDateElements {

};

struct PaymentGrpElements {

};

struct PaymentScheduleGrpElements {

};

struct PaymentScheduleRateSourceGrpElements {

};

struct PaymentSettlGrpElements {

};

struct PaymentSettlPartiesElements {

};

struct PaymentSettlPtysSubGrpElements {

};

struct PaymentStreamElements {

};

struct PaymentStreamFixedRateElements {

};

struct PaymentStreamFloatingRateElements {

};

struct PaymentStreamNonDeliverableFixingDateGrpElements {

};

struct PaymentStreamNonDeliverableSettlTermsElements {

};

struct PaymentStreamPaymentDatesElements {

};

struct PaymentStreamResetDatesElements {

};

struct PaymentStubGrpElements {

};

struct PhysicalSettlTermGrpElements {

};

struct PhysicalSettlDeliverableObligationGrpElements {

};

struct ProtectionTermGrpElements {

};

struct ProtectionTermEventGrpElements {

};

struct ProtectionTermEventQualifierGrpElements {

};

struct ProtectionTermObligationGrpElements {

};

struct ProvisionCashSettlPaymentDatesElements {

};

struct ProvisionCashSettlPaymentFixedDateGrpElements {

};

struct ProvisionCashSettlValueDatesElements {

};

struct ProvisionOptionExerciseFixedDateGrpElements {

};

struct ProvisionOptionExerciseDatesElements {

};

struct ProvisionOptionExpirationDateElements {

};

struct ProvisionOptionRelevantUnderlyingDateElements {

};

struct ProvisionGrpElements {

};

struct ProvisionPartiesElements {

};

struct ProvisionPtysSubGrpElements {

};

struct RegulatoryTradeIDGrpElements {

};

struct SecondaryAssetGrpElements {

};

struct SettlRateDisruptionFallbackGrpElements {

};

struct SideRegulatoryTradeIDGrpElements {

};

struct StreamCalculationPeriodDatesElements {

};

struct StreamEffectiveDateElements {

};

struct StreamGrpElements {

};

struct StreamTerminationDateElements {

};

struct UnderlyingComplexEventsElements {

};

struct UnderlyingComplexEventDatesElements {

};

struct UnderlyingComplexEventTimesElements {

};

struct UnderlyingEvntGrpElements {

};

struct UnderlyingPaymentScheduleGrpElements {

};

struct UnderlyingPaymentScheduleRateSourceGrpElements {

};

struct UnderlyingPaymentStreamElements {

};

struct UnderlyingPaymentStreamFixedRateElements {

};

struct UnderlyingPaymentStreamFloatingRateElements {

};

struct UnderlyingPaymentStreamNonDeliverableFixingDateGrpElements {

};

struct UnderlyingPaymentStreamNonDeliverableSettlTermsElements {

};

struct UnderlyingPaymentStreamPaymentDatesElements {

};

struct UnderlyingPaymentStreamResetDatesElements {

};

struct UnderlyingPaymentStubGrpElements {

};

struct UnderlyingSecondaryAssetGrpElements {

};

struct UnderlyingSettlRateDisruptionFallbackGrpElements {

};

struct UnderlyingStreamCalculationPeriodDatesElements {

};

struct UnderlyingStreamEffectiveDateElements {

};

struct UnderlyingStreamGrpElements {

};

struct UnderlyingStreamTerminationDateElements {

};

struct CashSettlDealerGrpElements {

};

struct BusinessCenterGrpElements {

};

struct DateAdjustmentElements {

};

struct LegBusinessCenterGrpElements {

};

struct LegDateAdjustmentElements {

};

struct LegPaymentScheduleFixingDateBusinessCenterGrpElements {

};

struct LegPaymentScheduleInterimExchangeDateBusinessCenterGrpElements {

};

struct LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements {

};

struct LegPaymentStreamPaymentDateBusinessCenterGrpElements {

};

struct LegPaymentStreamResetDateBusinessCenterGrpElements {

};

struct LegPaymentStreamInitialFixingDateBusinessCenterGrpElements {

};

struct LegPaymentStreamFixingDateBusinessCenterGrpElements {

};

struct LegProvisionCashSettlPaymentDateBusinessCenterGrpElements {

};

struct LegProvisionCashSettlValueDateBusinessCenterGrpElements {

};

struct LegProvisionOptionExerciseBusinessCenterGrpElements {

};

struct LegProvisionOptionExpirationDateBusinessCenterGrpElements {

};

struct LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements {

};

struct LegProvisionDateBusinessCenterGrpElements {

};

struct LegStreamCalculationPeriodBusinessCenterGrpElements {

};

struct LegStreamFirstPeriodStartDateBusinessCenterGrpElements {

};

struct LegStreamEffectiveDateBusinessCenterGrpElements {

};

struct LegStreamTerminationDateBusinessCenterGrpElements {

};

struct PaymentBusinessCenterGrpElements {

};

struct PaymentScheduleFixingDateBusinessCenterGrpElements {

};

struct PaymentScheduleInterimExchangeDateBusinessCenterGrpElements {

};

struct PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements {

};

struct PaymentStreamPaymentDateBusinessCenterGrpElements {

};

struct PaymentStreamResetDateBusinessCenterGrpElements {

};

struct PaymentStreamInitialFixingDateBusinessCenterGrpElements {

};

struct PaymentStreamFixingDateBusinessCenterGrpElements {

};

struct ProtectionTermEventNewsSourceGrpElements {

};

struct ProvisionCashSettlPaymentDateBusinessCenterGrpElements {

};

struct ProvisionCashSettlValueDateBusinessCenterGrpElements {

};

struct ProvisionOptionExerciseBusinessCenterGrpElements {

};

struct ProvisionOptionExpirationDateBusinessCenterGrpElements {

};

struct ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements {

};

struct ProvisionDateBusinessCenterGrpElements {

};

struct StreamCalculationPeriodBusinessCenterGrpElements {

};

struct StreamFirstPeriodStartDateBusinessCenterGrpElements {

};

struct StreamEffectiveBusinessCenterGrpElements {

};

struct StreamTerminationDateBusinessCenterGrpElements {

};

struct UnderlyingBusinessCenterGrpElements {

};

struct UnderlyingDateAdjustmentElements {

};

struct UnderlyingPaymentScheduleFixingDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamPaymentDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamResetDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamFixingDateBusinessCenterGrpElements {

};

struct UnderlyingStreamCalculationPeriodBusinessCenterGrpElements {

};

struct UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpElements {

};

struct UnderlyingStreamEffectiveDateBusinessCenterGrpElements {

};

struct UnderlyingStreamTerminationDateBusinessCenterGrpElements {

};

struct LegPaymentStreamNonDeliverableSettlRateSourceElements {

};

struct LegSettlRateFallbackRateSourceElements {

};

struct PaymentStreamNonDeliverableSettlRateSourceElements {

};

struct SettlRateFallbackRateSourceElements {

};

struct UnderlyingPaymentStreamNonDeliverableSettlRateSourceElements {

};

struct UnderlyingSettlRateFallbackRateSourceElements {

};

struct ProvisionCashSettlQuoteSourceElements {

};

struct LegProvisionCashSettlQuoteSourceElements {

};

struct AttachmentGrpElements {

};

struct AttachmentKeywordGrpElements {

};

struct AssetAttributeGrpElements {

};

struct ComplexEventAveragingObservationGrpElements {

};

struct ComplexEventCreditEventGrpElements {

};

struct ComplexEventCreditEventQualifierGrpElements {

};

struct ComplexEventPeriodDateGrpElements {

};

struct ComplexEventPeriodGrpElements {

};

struct ComplexEventRateSourceGrpElements {

};

struct ComplexEventDateBusinessCenterGrpElements {

};

struct ComplexEventRelativeDateElements {

};

struct ComplexEventCreditEventSourceGrpElements {

};

struct ComplexEventScheduleGrpElements {

};

struct DeliveryScheduleGrpElements {

};

struct DeliveryScheduleSettlDayGrpElements {

};

struct DeliveryScheduleSettlTimeGrpElements {

};

struct DeliveryStreamElements {

};

struct DeliveryStreamCycleGrpElements {

};

struct DeliveryStreamCommoditySourceGrpElements {

};

struct MarketDisruptionElements {

};

struct MarketDisruptionEventGrpElements {

};

struct MarketDisruptionFallbackGrpElements {

};

struct MarketDisruptionFallbackReferencePriceGrpElements {

};

struct OptionExerciseElements {

};

struct OptionExerciseBusinessCenterGrpElements {

};

struct OptionExerciseDatesElements {

};

struct OptionExerciseDateGrpElements {

};

struct OptionExerciseExpirationDateBusinessCenterGrpElements {

};

struct OptionExerciseExpirationElements {

};

struct OptionExerciseExpirationDateGrpElements {

};

struct PaymentScheduleFixingDayGrpElements {

};

struct PaymentStreamPricingBusinessCenterGrpElements {

};

struct PaymentStreamPaymentDateGrpElements {

};

struct PaymentStreamPricingDateGrpElements {

};

struct PaymentStreamPricingDayGrpElements {

};

struct PricingDateBusinessCenterGrpElements {

};

struct PricingDateTimeElements {

};

struct StreamAssetAttributeGrpElements {

};

struct StreamCalculationPeriodDateGrpElements {

};

struct StreamCommoditySettlBusinessCenterGrpElements {

};

struct StreamCommodityElements {

};

struct StreamCommodityAltIDGrpElements {

};

struct StreamCommodityDataSourceGrpElements {

};

struct StreamCommoditySettlDayGrpElements {

};

struct StreamCommoditySettlTimeGrpElements {

};

struct StreamCommoditySettlPeriodGrpElements {

};

struct MandatoryClearingJurisdictionGrpElements {

};

struct LegAdditionalTermBondRefGrpElements {

};

struct LegAdditionalTermGrpElements {

};

struct LegAssetAttributeGrpElements {

};

struct LegCashSettlDealerGrpElements {

};

struct LegCashSettlTermGrpElements {

};

struct LegComplexEventAveragingObservationGrpElements {

};

struct LegComplexEventCreditEventGrpElements {

};

struct LegComplexEventCreditEventQualifierGrpElements {

};

struct LegComplexEventPeriodDateGrpElements {

};

struct LegComplexEventPeriodGrpElements {

};

struct LegComplexEventRateSourceGrpElements {

};

struct LegComplexEventDateBusinessCenterGrpElements {

};

struct LegComplexEventRelativeDateElements {

};

struct LegComplexEventCreditEventSourceGrpElements {

};

struct LegComplexEventsElements {

};

struct LegComplexEventDatesElements {

};

struct LegComplexEventTimesElements {

};

struct LegComplexEventScheduleGrpElements {

};

struct LegDeliveryScheduleGrpElements {

};

struct LegDeliveryScheduleSettlDayGrpElements {

};

struct LegDeliveryScheduleSettlTimeGrpElements {

};

struct LegDeliveryStreamElements {

};

struct LegStreamAssetAttributeGrpElements {

};

struct LegDeliveryStreamCycleGrpElements {

};

struct LegDeliveryStreamCommoditySourceGrpElements {

};

struct LegInstrumentPartiesElements {

};

struct LegInstrumentPtysSubGrpElements {

};

struct LegMarketDisruptionElements {

};

struct LegMarketDisruptionEventGrpElements {

};

struct LegMarketDisruptionFallbackGrpElements {

};

struct LegMarketDisruptionFallbackReferencePriceGrpElements {

};

struct LegOptionExerciseElements {

};

struct LegOptionExerciseBusinessCenterGrpElements {

};

struct LegOptionExerciseDatesElements {

};

struct LegOptionExerciseDateGrpElements {

};

struct LegOptionExerciseExpirationDateBusinessCenterGrpElements {

};

struct LegOptionExerciseExpirationElements {

};

struct LegOptionExerciseExpirationDateGrpElements {

};

struct LegPaymentScheduleFixingDayGrpElements {

};

struct LegPaymentStreamPricingBusinessCenterGrpElements {

};

struct LegPaymentStreamPaymentDateGrpElements {

};

struct LegPaymentStreamPricingDateGrpElements {

};

struct LegPaymentStreamPricingDayGrpElements {

};

struct LegPhysicalSettlTermGrpElements {

};

struct LegPhysicalSettlDeliverableObligationGrpElements {

};

struct LegPricingDateBusinessCenterGrpElements {

};

struct LegPricingDateTimeElements {

};

struct LegProtectionTermEventNewsSourceGrpElements {

};

struct LegProtectionTermGrpElements {

};

struct LegProtectionTermEventGrpElements {

};

struct LegProtectionTermEventQualifierGrpElements {

};

struct LegProtectionTermObligationGrpElements {

};

struct LegStreamCalculationPeriodDateGrpElements {

};

struct LegStreamCommoditySettlBusinessCenterGrpElements {

};

struct LegStreamCommodityElements {

};

struct LegStreamCommodityAltIDGrpElements {

};

struct LegStreamCommodityDataSourceGrpElements {

};

struct LegStreamCommoditySettlDayGrpElements {

};

struct LegStreamCommoditySettlTimeGrpElements {

};

struct LegStreamCommoditySettlPeriodGrpElements {

};

struct UnderlyingAssetAttributeGrpElements {

};

struct UnderlyingComplexEventAveragingObservationGrpElements {

};

struct UnderlyingComplexEventCreditEventGrpElements {

};

struct UnderlyingComplexEventCreditEventQualifierGrpElements {

};

struct UnderlyingComplexEventPeriodDateGrpElements {

};

struct UnderlyingComplexEventPeriodGrpElements {

};

struct UnderlyingComplexEventRateSourceGrpElements {

};

struct UnderlyingComplexEventDateBusinessCenterGrpElements {

};

struct UnderlyingComplexEventRelativeDateElements {

};

struct UnderlyingComplexEventCreditEventSourceGrpElements {

};

struct UnderlyingComplexEventScheduleGrpElements {

};

struct UnderlyingDeliveryScheduleGrpElements {

};

struct UnderlyingDeliveryScheduleSettlDayGrpElements {

};

struct UnderlyingDeliveryScheduleSettlTimeGrpElements {

};

struct UnderlyingDeliveryStreamElements {

};

struct UnderlyingStreamAssetAttributeGrpElements {

};

struct UnderlyingDeliveryStreamCycleGrpElements {

};

struct UnderlyingDeliveryStreamCommoditySourceGrpElements {

};

struct UnderlyingOptionExerciseElements {

};

struct UnderlyingOptionExerciseBusinessCenterGrpElements {

};

struct UnderlyingOptionExerciseDatesElements {

};

struct UnderlyingOptionExerciseDateGrpElements {

};

struct UnderlyingOptionExerciseExpirationDateBusinessCenterGrpElements {

};

struct UnderlyingOptionExerciseExpirationElements {

};

struct UnderlyingOptionExerciseExpirationDateGrpElements {

};

struct UnderlyingMarketDisruptionElements {

};

struct UnderlyingMarketDisruptionEventGrpElements {

};

struct UnderlyingMarketDisruptionFallbackGrpElements {

};

struct UnderlyingMarketDisruptionFallbackReferencePriceGrpElements {

};

struct UnderlyingPaymentScheduleFixingDayGrpElements {

};

struct UnderlyingPaymentStreamPricingBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamPaymentDateGrpElements {

};

struct UnderlyingPaymentStreamPricingDateGrpElements {

};

struct UnderlyingPaymentStreamPricingDayGrpElements {

};

struct UnderlyingPricingDateBusinessCenterGrpElements {

};

struct UnderlyingPricingDateTimeElements {

};

struct UnderlyingStreamCalculationPeriodDateGrpElements {

};

struct UnderlyingStreamCommoditySettlBusinessCenterGrpElements {

};

struct UnderlyingStreamCommodityElements {

};

struct UnderlyingStreamCommodityAltIDGrpElements {

};

struct UnderlyingStreamCommodityDataSourceGrpElements {

};

struct UnderlyingStreamCommoditySettlDayGrpElements {

};

struct UnderlyingStreamCommoditySettlTimeGrpElements {

};

struct UnderlyingStreamCommoditySettlPeriodGrpElements {

};

struct UnderlyingAdditionalTermBondRefGrpElements {

};

struct UnderlyingAdditionalTermGrpElements {

};

struct UnderlyingCashSettlDealerGrpElements {

};

struct UnderlyingCashSettlTermGrpElements {

};

struct UnderlyingPhysicalSettlTermGrpElements {

};

struct UnderlyingPhysicalSettlDeliverableObligationGrpElements {

};

struct UnderlyingProtectionTermGrpElements {

};

struct UnderlyingProtectionTermEventGrpElements {

};

struct UnderlyingProtectionTermEventQualifierGrpElements {

};

struct UnderlyingProtectionTermObligationGrpElements {

};

struct UnderlyingProtectionTermEventNewsSourceGrpElements {

};

struct UnderlyingProvisionCashSettlPaymentDatesElements {

};

struct UnderlyingProvisionCashSettlPaymentFixedDateGrpElements {

};

struct UnderlyingProvisionCashSettlQuoteSourceElements {

};

struct UnderlyingProvisionCashSettlValueDatesElements {

};

struct UnderlyingProvisionOptionExerciseFixedDateGrpElements {

};

struct UnderlyingProvisionOptionExerciseDatesElements {

};

struct UnderlyingProvisionOptionExpirationDateElements {

};

struct UnderlyingProvisionOptionRelevantUnderlyingDateElements {

};

struct UnderlyingProvisionGrpElements {

};

struct UnderlyingProvisionPartiesElements {

};

struct UnderlyingProvisionPtysSubGrpElements {

};

struct UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpElements {

};

struct UnderlyingProvisionCashSettlValueDateBusinessCenterGrpElements {

};

struct UnderlyingProvisionOptionExerciseBusinessCenterGrpElements {

};

struct UnderlyingProvisionOptionExpirationDateBusinessCenterGrpElements {

};

struct UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements {

};

struct UnderlyingProvisionDateBusinessCenterGrpElements {

};

struct TargetPtysSubGrpElements {

};

struct LegFinancingDetailsElements {

};

struct LegFinancingContractualDefinitionsGrpElements {

};

struct LegFinancingTermSupplementGrpElements {

};

struct LegFinancingContractualMatrixGrpElements {

};

struct RelativeValueGrpElements {

};

struct AuctionTypeRuleGrpElements {

};

struct FlexProductEligibilityGrpElements {

};

struct PriceRangeRuleGrpElements {

};

struct QuoteSizeRuleGrpElements {

};

struct RelatedMarketSegmentGrpElements {

};

struct ClearingPriceParametersGrpElements {

};

struct MiscFeesSubGrpElements {

};

struct CommissionDataGrpElements {

};

struct AllocCommissionDataGrpElements {

};

struct CashSettlDateElements {

};

struct CashSettlDateBusinessCenterGrpElements {

};

struct DividendAccrualFloatingRateElements {

};

struct DividendAccrualPaymentDateBusinessCenterGrpElements {

};

struct DividendAccrualPaymentDateElements {

};

struct DividendConditionsElements {

};

struct DividendFXTriggerDateElements {

};

struct DividendFXTriggerDateBusinessCenterGrpElements {

};

struct DividendPeriodGrpElements {

};

struct DividendPeriodBusinessCenterGrpElements {

};

struct ExtraordinaryEventGrpElements {

};

struct LegCashSettlDateElements {

};

struct LegCashSettlDateBusinessCenterGrpElements {

};

struct LegDividendAccrualPaymentDateBusinessCenterGrpElements {

};

struct LegDividendAccrualFloatingRateElements {

};

struct LegDividendAccrualPaymentDateElements {

};

struct LegDividendConditionsElements {

};

struct LegDividendFXTriggerDateElements {

};

struct LegDividendFXTriggerDateBusinessCenterGrpElements {

};

struct LegDividendPeriodGrpElements {

};

struct LegDividendPeriodBusinessCenterGrpElements {

};

struct LegExtraordinaryEventGrpElements {

};

struct LegOptionExerciseMakeWholeProvisionElements {

};

struct LegPaymentStreamCompoundingDateGrpElements {

};

struct LegPaymentStreamCompoundingDatesElements {

};

struct LegPaymentStreamCompoundingDatesBusinessCenterGrpElements {

};

struct LegPaymentStreamCompoundingEndDateElements {

};

struct LegPaymentStreamCompoundingFloatingRateElements {

};

struct LegPaymentStreamCompoundingStartDateElements {

};

struct LegPaymentStreamFormulaImageElements {

};

struct LegPaymentStreamFinalPricePaymentDateElements {

};

struct LegPaymentStreamFixingDateGrpElements {

};

struct LegPaymentStreamFormulaElements {

};

struct LegPaymentStubEndDateElements {

};

struct LegPaymentStubEndDateBusinessCenterGrpElements {

};

struct LegPaymentStubStartDateElements {

};

struct LegPaymentStubStartDateBusinessCenterGrpElements {

};

struct LegReturnRateDateGrpElements {

};

struct LegReturnRateFXConversionGrpElements {

};

struct LegReturnRateGrpElements {

};

struct LegReturnRateInformationSourceGrpElements {

};

struct LegReturnRatePriceGrpElements {

};

struct LegReturnRateValuationDateBusinessCenterGrpElements {

};

struct LegReturnRateValuationDateGrpElements {

};

struct LegSettlMethodElectionDateElements {

};

struct LegSettlMethodElectionDateBusinessCenterGrpElements {

};

struct OptionExerciseMakeWholeProvisionElements {

};

struct PaymentStreamCompoundingDateGrpElements {

};

struct PaymentStreamCompoundingDatesElements {

};

struct PaymentStreamCompoundingDatesBusinessCenterGrpElements {

};

struct PaymentStreamCompoundingEndDateElements {

};

struct PaymentStreamCompoundingFloatingRateElements {

};

struct PaymentStreamCompoundingStartDateElements {

};

struct PaymentStreamFormulaImageElements {

};

struct PaymentStreamFinalPricePaymentDateElements {

};

struct PaymentStreamFixingDateGrpElements {

};

struct PaymentStreamFormulaElements {

};

struct PaymentStubEndDateElements {

};

struct PaymentStubEndDateBusinessCenterGrpElements {

};

struct PaymentStubStartDateElements {

};

struct PaymentStubStartDateBusinessCenterGrpElements {

};

struct ReturnRateDateGrpElements {

};

struct ReturnRateFXConversionGrpElements {

};

struct ReturnRateGrpElements {

};

struct ReturnRateInformationSourceGrpElements {

};

struct ReturnRatePriceGrpElements {

};

struct ReturnRateValuationDateBusinessCenterGrpElements {

};

struct ReturnRateValuationDateGrpElements {

};

struct SettlMethodElectionDateBusinessCenterGrpElements {

};

struct SettlMethodElectionDateElements {

};

struct UnderlyingCashSettlDateBusinessCenterGrpElements {

};

struct UnderlyingCashSettlDateElements {

};

struct UnderlyingDividendAccrualPaymentDateBusinessCenterGrpElements {

};

struct UnderlyingDividendAccrualFloatingRateElements {

};

struct UnderlyingDividendAccrualPaymentDateElements {

};

struct UnderlyingDividendConditionsElements {

};

struct UnderlyingDividendFXTriggerDateElements {

};

struct UnderlyingDividendFXTriggerDateBusinessCenterGrpElements {

};

struct UnderlyingDividendPaymentGrpElements {

};

struct UnderlyingDividendPayoutElements {

};

struct UnderlyingDividendPeriodGrpElements {

};

struct UnderlyingDividendPeriodBusinessCenterGrpElements {

};

struct UnderlyingExtraordinaryEventGrpElements {

};

struct UnderlyingOptionExerciseMakeWholeProvisionElements {

};

struct UnderlyingPaymentStreamCompoundingDateGrpElements {

};

struct UnderlyingPaymentStreamCompoundingDatesElements {

};

struct UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpElements {

};

struct UnderlyingPaymentStreamCompoundingEndDateElements {

};

struct UnderlyingPaymentStreamCompoundingFloatingRateElements {

};

struct UnderlyingPaymentStreamCompoundingStartDateElements {

};

struct UnderlyingPaymentStreamFormulaImageElements {

};

struct UnderlyingPaymentStreamFinalPricePaymentDateElements {

};

struct UnderlyingPaymentStreamFixingDateGrpElements {

};

struct UnderlyingPaymentStreamFormulaElements {

};

struct UnderlyingPaymentStubEndDateElements {

};

struct UnderlyingPaymentStubEndDateBusinessCenterGrpElements {

};

struct UnderlyingPaymentStubStartDateElements {

};

struct UnderlyingPaymentStubStartDateBusinessCenterGrpElements {

};

struct UnderlyingRateSpreadScheduleElements {

};

struct UnderlyingRateSpreadStepGrpElements {

};

struct UnderlyingReturnRateDateGrpElements {

};

struct UnderlyingReturnRateFXConversionGrpElements {

};

struct UnderlyingReturnRateGrpElements {

};

struct UnderlyingReturnRateInformationSourceGrpElements {

};

struct UnderlyingReturnRatePriceGrpElements {

};

struct UnderlyingReturnRateValuationDateBusinessCenterGrpElements {

};

struct UnderlyingReturnRateValuationDateGrpElements {

};

struct UnderlyingSettlMethodElectionDateBusinessCenterGrpElements {

};

struct UnderlyingSettlMethodElectionDateElements {

};

struct TrdRegPublicationGrpElements {

};

struct OrderAttributeGrpElements {

};

struct SideCollateralAmountGrpElements {

};

struct QuoteAttributeGrpElements {

};

struct PriceQualifierGrpElements {

};

struct IndexRollMonthGrpElements {

};

struct ReferenceDataDateGrpElements {

};

struct FloatingRateIndexElements {

};

struct AveragePriceDetailElements {

};

struct MatchExceptionGrpElements {

};

struct MatchingDataPointGrpElements {

};

struct OrderAggregationGrpElements {

};

struct ExecutionAggregationGrpElements {

};

struct BaseHeaderAttributes {

};

struct HopGrpAttributes {

};

struct MessageHeaderAttributes {

};

struct BatchHeaderAttributes {

};

struct BatchAttributes {

};

struct FixmlAttributes {

};

struct CommissionDataAttributes {

};

struct DiscretionInstructionsAttributes {

};

struct FinancingDetailsAttributes {

};

struct InstrumentAttributes {

};

struct InstrumentExtensionAttributes {

};

struct InstrumentLegAttributes {

};

struct LegBenchmarkCurveDataAttributes {

};

struct LegStipulationsAttributes {

};

struct NestedPartiesAttributes {

};

struct OrderQtyDataAttributes {

};

struct PartiesAttributes {

};

struct PegInstructionsAttributes {

};

struct PositionAmountDataAttributes {

};

struct PositionQtyAttributes {

};

struct SettlInstructionsDataAttributes {

};

struct SettlPartiesAttributes {

};

struct SpreadOrBenchmarkCurveDataAttributes {

};

struct StipulationsAttributes {

};

struct TrdRegTimestampsAttributes {

};

struct UnderlyingInstrumentAttributes {

};

struct YieldDataAttributes {

};

struct UnderlyingStipulationsAttributes {

};

struct NestedParties2Attributes {

};

struct NestedParties3Attributes {

};

struct ClrInstGrpAttributes {

};

struct CompIDReqGrpAttributes {

};

struct CompIDStatGrpAttributes {
	RefCompID_t RefCompID;
		StatusValue_t StatValu;
	
};

struct ContAmtGrpAttributes {

};

struct ContraGrpAttributes {

};

struct ExecAllocGrpAttributes {

};

struct InstrmtGrpAttributes {

};

struct InstrmtLegExecGrpAttributes {

};

struct InstrmtLegGrpAttributes {

};

struct InstrmtLegIOIGrpAttributes {

};

struct InstrmtLegSecListGrpAttributes {

};

struct InstrmtMDReqGrpAttributes {

};

struct LegOrdGrpAttributes {

};

struct LegPreAllocGrpAttributes {

};

struct LinesOfTextGrpAttributes {
	Text_t Txt;
	
};

struct MiscFeesGrpAttributes {

};

struct OrdAllocGrpAttributes {

};

struct PreAllocGrpAttributes {

};

struct PreAllocMlegGrpAttributes {

};

struct RoutingGrpAttributes {

};

struct TrdgSesGrpAttributes {

};

struct UndInstrmtGrpAttributes {

};

struct EvntGrpAttributes {

};

struct SecAltIDGrpAttributes {

};

struct LegSecAltIDGrpAttributes {

};

struct UndSecAltIDGrpAttributes {

};

struct AttrbGrpAttributes {

};

struct DlvyInstGrpAttributes {

};

struct SettlPtysSubGrpAttributes {

};

struct PtysSubGrpAttributes {

};

struct NstdPtysSubGrpAttributes {

};

struct NstdPtys2SubGrpAttributes {

};

struct NstdPtys3SubGrpAttributes {

};

struct StrategyParametersGrpAttributes {

};

struct InstrumentPartiesAttributes {

};

struct InstrumentPtysSubGrpAttributes {

};

struct UndlyInstrumentPartiesAttributes {

};

struct UndlyInstrumentPtysSubGrpAttributes {

};

struct DisplayInstructionAttributes {

};

struct TriggeringInstructionAttributes {

};

struct RootPartiesAttributes {

};

struct RootSubPartiesAttributes {

};

struct TrdSessLstGrpAttributes {
	TradingSessionID_t SesID;
		TradSesStatus_t Stat;
	
};

struct MsgTypeGrpAttributes {

};

struct SecurityTradingRulesAttributes {

};

struct SettlDetailsAttributes {

};

struct SecurityXMLAttributes {

};

struct TickRulesAttributes {

};

struct StrikeRulesAttributes {

};

struct MaturityRulesAttributes {

};

struct SecondaryPriceLimitsAttributes {

};

struct PriceLimitsAttributes {

};

struct MarketDataFeedTypesAttributes {

};

struct LotTypeRulesAttributes {

};

struct MatchRulesAttributes {

};

struct ExecInstRulesAttributes {

};

struct TimeInForceRulesAttributes {

};

struct OrdTypeRulesAttributes {

};

struct TradingSessionRulesAttributes {

};

struct TradingSessionRulesGrpAttributes {

};

struct BaseTradingRulesAttributes {

};

struct MarketSegmentGrpAttributes {

};

struct DerivativeInstrumentPartySubIDsGrpAttributes {

};

struct DerivativeInstrumentPartiesAttributes {

};

struct DerivativeInstrumentAttributeAttributes {

};

struct NestedInstrumentAttributeAttributes {

};

struct DerivativeInstrumentAttributes {

};

struct DerivativeSecurityAltIDGrpAttributes {

};

struct DerivativeEventsGrpAttributes {

};

struct DerivativeSecurityDefinitionAttributes {

};

struct RelSymDerivSecUpdGrpAttributes {

};

struct DerivativeSecurityXMLAttributes {

};

struct ApplicationSequenceControlAttributes {

};

struct NstdPtys4SubGrpAttributes {

};

struct NestedParties4Attributes {

};

struct RateSourceAttributes {

};

struct TargetPartiesAttributes {

};

struct ComplexEventsAttributes {

};

struct ComplexEventDatesAttributes {

};

struct ComplexEventTimesAttributes {

};

struct MatchingInstructionsAttributes {

};

struct LimitAmtsAttributes {

};

struct MarginAmountAttributes {

};

struct RelatedInstrumentGrpAttributes {

};

struct PartyRelationshipGrpAttributes {

};

struct PartyDetailGrpAttributes {

};

struct PartyDetailAltIDGrpAttributes {

};

struct PartyDetailAltSubGrpAttributes {

};

struct InstrumentScopeAttributes {

};

struct InstrumentScopeSecurityAltIDGrpAttributes {

};

struct RelatedPartyDetailGrpAttributes {

};

struct RelatedPartyDetailSubGrpAttributes {

};

struct RelatedPartyDetailAltIDGrpAttributes {

};

struct RelatedPartyDetailAltSubGrpAttributes {

};

struct InstrumentScopeGrpAttributes {

};

struct RequestingPartyGrpAttributes {

};

struct RequestingPartySubGrpAttributes {

};

struct PartyDetailSubGrpAttributes {

};

struct SecurityClassificationGrpAttributes {

};

struct ThrottleParamsGrpAttributes {

};

struct ThrottleMsgTypeGrpAttributes {

};

struct ThrottleResponseAttributes {

};

struct CollateralAmountGrpAttributes {

};

struct MarketSegmentScopeGrpAttributes {

};

struct TargetMarketSegmentGrpAttributes {

};

struct AffectedMarketSegmentGrpAttributes {

};

struct NotAffectedMarketSegmentGrpAttributes {

};

struct DisclosureInstructionGrpAttributes {

};

struct TradeAllocAmtGrpAttributes {

};

struct TradePriceConditionGrpAttributes {

};

struct TradeQtyGrpAttributes {

};

struct TradePositionQtyAttributes {

};

struct RelatedTradeGrpAttributes {

};

struct RelatedPositionGrpAttributes {

};

struct ValueChecksGrpAttributes {

};

struct LegSecurityXMLAttributes {

};

struct UnderlyingSecurityXMLAttributes {

};

struct AdditionalTermBondRefGrpAttributes {

};

struct AdditionalTermGrpAttributes {

};

struct AllocRegulatoryTradeIDGrpAttributes {

};

struct CashSettlTermGrpAttributes {

};

struct FinancingContractualDefinitionGrpAttributes {

};

struct FinancingContractualMatrixGrpAttributes {

};

struct FinancingTermSupplementGrpAttributes {

};

struct LegEvntGrpAttributes {

};

struct LegPaymentScheduleGrpAttributes {

};

struct LegPaymentScheduleRateSourceGrpAttributes {

};

struct LegPaymentStreamAttributes {

};

struct LegPaymentStreamFixedRateAttributes {

};

struct LegPaymentStreamFloatingRateAttributes {

};

struct LegPaymentStreamNonDeliverableFixingDateGrpAttributes {

};

struct LegPaymentStreamNonDeliverableSettlTermsAttributes {

};

struct LegPaymentStreamPaymentDatesAttributes {

};

struct LegPaymentStreamResetDatesAttributes {

};

struct LegPaymentStubGrpAttributes {

};

struct LegProvisionCashSettlPaymentDatesAttributes {

};

struct LegProvisionCashSettlPaymentFixedDateGrpAttributes {

};

struct LegProvisionCashSettlValueDatesAttributes {

};

struct LegProvisionOptionExerciseFixedDateGrpAttributes {

};

struct LegProvisionOptionExerciseDatesAttributes {

};

struct LegProvisionOptionExpirationDateAttributes {

};

struct LegProvisionOptionRelevantUnderlyingDateAttributes {

};

struct LegProvisionGrpAttributes {

};

struct LegProvisionPartiesAttributes {

};

struct LegProvisionPtysSubGrpAttributes {

};

struct LegSecondaryAssetGrpAttributes {

};

struct LegSettlRateDisruptionFallbackGrpAttributes {

};

struct LegStreamCalculationPeriodDatesAttributes {

};

struct LegStreamEffectiveDateAttributes {

};

struct LegStreamGrpAttributes {

};

struct LegStreamTerminationDateAttributes {

};

struct PaymentGrpAttributes {

};

struct PaymentScheduleGrpAttributes {

};

struct PaymentScheduleRateSourceGrpAttributes {

};

struct PaymentSettlGrpAttributes {

};

struct PaymentSettlPartiesAttributes {

};

struct PaymentSettlPtysSubGrpAttributes {

};

struct PaymentStreamAttributes {

};

struct PaymentStreamFixedRateAttributes {

};

struct PaymentStreamFloatingRateAttributes {

};

struct PaymentStreamNonDeliverableFixingDateGrpAttributes {

};

struct PaymentStreamNonDeliverableSettlTermsAttributes {

};

struct PaymentStreamPaymentDatesAttributes {

};

struct PaymentStreamResetDatesAttributes {

};

struct PaymentStubGrpAttributes {

};

struct PhysicalSettlTermGrpAttributes {

};

struct PhysicalSettlDeliverableObligationGrpAttributes {

};

struct ProtectionTermGrpAttributes {

};

struct ProtectionTermEventGrpAttributes {

};

struct ProtectionTermEventQualifierGrpAttributes {

};

struct ProtectionTermObligationGrpAttributes {

};

struct ProvisionCashSettlPaymentDatesAttributes {

};

struct ProvisionCashSettlPaymentFixedDateGrpAttributes {

};

struct ProvisionCashSettlValueDatesAttributes {

};

struct ProvisionOptionExerciseFixedDateGrpAttributes {

};

struct ProvisionOptionExerciseDatesAttributes {

};

struct ProvisionOptionExpirationDateAttributes {

};

struct ProvisionOptionRelevantUnderlyingDateAttributes {

};

struct ProvisionGrpAttributes {

};

struct ProvisionPartiesAttributes {

};

struct ProvisionPtysSubGrpAttributes {

};

struct RegulatoryTradeIDGrpAttributes {

};

struct SecondaryAssetGrpAttributes {

};

struct SettlRateDisruptionFallbackGrpAttributes {

};

struct SideRegulatoryTradeIDGrpAttributes {

};

struct StreamCalculationPeriodDatesAttributes {

};

struct StreamEffectiveDateAttributes {

};

struct StreamGrpAttributes {

};

struct StreamTerminationDateAttributes {

};

struct UnderlyingComplexEventsAttributes {

};

struct UnderlyingComplexEventDatesAttributes {

};

struct UnderlyingComplexEventTimesAttributes {

};

struct UnderlyingEvntGrpAttributes {

};

struct UnderlyingPaymentScheduleGrpAttributes {

};

struct UnderlyingPaymentScheduleRateSourceGrpAttributes {

};

struct UnderlyingPaymentStreamAttributes {

};

struct UnderlyingPaymentStreamFixedRateAttributes {

};

struct UnderlyingPaymentStreamFloatingRateAttributes {

};

struct UnderlyingPaymentStreamNonDeliverableFixingDateGrpAttributes {

};

struct UnderlyingPaymentStreamNonDeliverableSettlTermsAttributes {

};

struct UnderlyingPaymentStreamPaymentDatesAttributes {

};

struct UnderlyingPaymentStreamResetDatesAttributes {

};

struct UnderlyingPaymentStubGrpAttributes {

};

struct UnderlyingSecondaryAssetGrpAttributes {

};

struct UnderlyingSettlRateDisruptionFallbackGrpAttributes {

};

struct UnderlyingStreamCalculationPeriodDatesAttributes {

};

struct UnderlyingStreamEffectiveDateAttributes {

};

struct UnderlyingStreamGrpAttributes {

};

struct UnderlyingStreamTerminationDateAttributes {

};

struct CashSettlDealerGrpAttributes {

};

struct BusinessCenterGrpAttributes {

};

struct DateAdjustmentAttributes {

};

struct LegBusinessCenterGrpAttributes {

};

struct LegDateAdjustmentAttributes {

};

struct LegPaymentScheduleFixingDateBusinessCenterGrpAttributes {

};

struct LegPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes {

};

struct LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes {

};

struct LegPaymentStreamPaymentDateBusinessCenterGrpAttributes {

};

struct LegPaymentStreamResetDateBusinessCenterGrpAttributes {

};

struct LegPaymentStreamInitialFixingDateBusinessCenterGrpAttributes {

};

struct LegPaymentStreamFixingDateBusinessCenterGrpAttributes {

};

struct LegProvisionCashSettlPaymentDateBusinessCenterGrpAttributes {

};

struct LegProvisionCashSettlValueDateBusinessCenterGrpAttributes {

};

struct LegProvisionOptionExerciseBusinessCenterGrpAttributes {

};

struct LegProvisionOptionExpirationDateBusinessCenterGrpAttributes {

};

struct LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes {

};

struct LegProvisionDateBusinessCenterGrpAttributes {

};

struct LegStreamCalculationPeriodBusinessCenterGrpAttributes {

};

struct LegStreamFirstPeriodStartDateBusinessCenterGrpAttributes {

};

struct LegStreamEffectiveDateBusinessCenterGrpAttributes {

};

struct LegStreamTerminationDateBusinessCenterGrpAttributes {

};

struct PaymentBusinessCenterGrpAttributes {

};

struct PaymentScheduleFixingDateBusinessCenterGrpAttributes {

};

struct PaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes {

};

struct PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes {

};

struct PaymentStreamPaymentDateBusinessCenterGrpAttributes {

};

struct PaymentStreamResetDateBusinessCenterGrpAttributes {

};

struct PaymentStreamInitialFixingDateBusinessCenterGrpAttributes {

};

struct PaymentStreamFixingDateBusinessCenterGrpAttributes {

};

struct ProtectionTermEventNewsSourceGrpAttributes {

};

struct ProvisionCashSettlPaymentDateBusinessCenterGrpAttributes {

};

struct ProvisionCashSettlValueDateBusinessCenterGrpAttributes {

};

struct ProvisionOptionExerciseBusinessCenterGrpAttributes {

};

struct ProvisionOptionExpirationDateBusinessCenterGrpAttributes {

};

struct ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes {

};

struct ProvisionDateBusinessCenterGrpAttributes {

};

struct StreamCalculationPeriodBusinessCenterGrpAttributes {

};

struct StreamFirstPeriodStartDateBusinessCenterGrpAttributes {

};

struct StreamEffectiveBusinessCenterGrpAttributes {

};

struct StreamTerminationDateBusinessCenterGrpAttributes {

};

struct UnderlyingBusinessCenterGrpAttributes {

};

struct UnderlyingDateAdjustmentAttributes {

};

struct UnderlyingPaymentScheduleFixingDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamPaymentDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamResetDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamFixingDateBusinessCenterGrpAttributes {

};

struct UnderlyingStreamCalculationPeriodBusinessCenterGrpAttributes {

};

struct UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpAttributes {

};

struct UnderlyingStreamEffectiveDateBusinessCenterGrpAttributes {

};

struct UnderlyingStreamTerminationDateBusinessCenterGrpAttributes {

};

struct LegPaymentStreamNonDeliverableSettlRateSourceAttributes {

};

struct LegSettlRateFallbackRateSourceAttributes {

};

struct PaymentStreamNonDeliverableSettlRateSourceAttributes {

};

struct SettlRateFallbackRateSourceAttributes {

};

struct UnderlyingPaymentStreamNonDeliverableSettlRateSourceAttributes {

};

struct UnderlyingSettlRateFallbackRateSourceAttributes {

};

struct ProvisionCashSettlQuoteSourceAttributes {

};

struct LegProvisionCashSettlQuoteSourceAttributes {

};

struct AttachmentGrpAttributes {

};

struct AttachmentKeywordGrpAttributes {

};

struct AssetAttributeGrpAttributes {

};

struct ComplexEventAveragingObservationGrpAttributes {

};

struct ComplexEventCreditEventGrpAttributes {

};

struct ComplexEventCreditEventQualifierGrpAttributes {

};

struct ComplexEventPeriodDateGrpAttributes {

};

struct ComplexEventPeriodGrpAttributes {

};

struct ComplexEventRateSourceGrpAttributes {

};

struct ComplexEventDateBusinessCenterGrpAttributes {

};

struct ComplexEventRelativeDateAttributes {

};

struct ComplexEventCreditEventSourceGrpAttributes {

};

struct ComplexEventScheduleGrpAttributes {

};

struct DeliveryScheduleGrpAttributes {

};

struct DeliveryScheduleSettlDayGrpAttributes {

};

struct DeliveryScheduleSettlTimeGrpAttributes {

};

struct DeliveryStreamAttributes {

};

struct DeliveryStreamCycleGrpAttributes {

};

struct DeliveryStreamCommoditySourceGrpAttributes {

};

struct MarketDisruptionAttributes {

};

struct MarketDisruptionEventGrpAttributes {

};

struct MarketDisruptionFallbackGrpAttributes {

};

struct MarketDisruptionFallbackReferencePriceGrpAttributes {

};

struct OptionExerciseAttributes {

};

struct OptionExerciseBusinessCenterGrpAttributes {

};

struct OptionExerciseDatesAttributes {

};

struct OptionExerciseDateGrpAttributes {

};

struct OptionExerciseExpirationDateBusinessCenterGrpAttributes {

};

struct OptionExerciseExpirationAttributes {

};

struct OptionExerciseExpirationDateGrpAttributes {

};

struct PaymentScheduleFixingDayGrpAttributes {

};

struct PaymentStreamPricingBusinessCenterGrpAttributes {

};

struct PaymentStreamPaymentDateGrpAttributes {

};

struct PaymentStreamPricingDateGrpAttributes {

};

struct PaymentStreamPricingDayGrpAttributes {

};

struct PricingDateBusinessCenterGrpAttributes {

};

struct PricingDateTimeAttributes {

};

struct StreamAssetAttributeGrpAttributes {

};

struct StreamCalculationPeriodDateGrpAttributes {

};

struct StreamCommoditySettlBusinessCenterGrpAttributes {

};

struct StreamCommodityAttributes {

};

struct StreamCommodityAltIDGrpAttributes {

};

struct StreamCommodityDataSourceGrpAttributes {

};

struct StreamCommoditySettlDayGrpAttributes {

};

struct StreamCommoditySettlTimeGrpAttributes {

};

struct StreamCommoditySettlPeriodGrpAttributes {

};

struct MandatoryClearingJurisdictionGrpAttributes {

};

struct LegAdditionalTermBondRefGrpAttributes {

};

struct LegAdditionalTermGrpAttributes {

};

struct LegAssetAttributeGrpAttributes {

};

struct LegCashSettlDealerGrpAttributes {

};

struct LegCashSettlTermGrpAttributes {

};

struct LegComplexEventAveragingObservationGrpAttributes {

};

struct LegComplexEventCreditEventGrpAttributes {

};

struct LegComplexEventCreditEventQualifierGrpAttributes {

};

struct LegComplexEventPeriodDateGrpAttributes {

};

struct LegComplexEventPeriodGrpAttributes {

};

struct LegComplexEventRateSourceGrpAttributes {

};

struct LegComplexEventDateBusinessCenterGrpAttributes {

};

struct LegComplexEventRelativeDateAttributes {

};

struct LegComplexEventCreditEventSourceGrpAttributes {

};

struct LegComplexEventsAttributes {

};

struct LegComplexEventDatesAttributes {

};

struct LegComplexEventTimesAttributes {

};

struct LegComplexEventScheduleGrpAttributes {

};

struct LegDeliveryScheduleGrpAttributes {

};

struct LegDeliveryScheduleSettlDayGrpAttributes {

};

struct LegDeliveryScheduleSettlTimeGrpAttributes {

};

struct LegDeliveryStreamAttributes {

};

struct LegStreamAssetAttributeGrpAttributes {

};

struct LegDeliveryStreamCycleGrpAttributes {

};

struct LegDeliveryStreamCommoditySourceGrpAttributes {

};

struct LegInstrumentPartiesAttributes {

};

struct LegInstrumentPtysSubGrpAttributes {

};

struct LegMarketDisruptionAttributes {

};

struct LegMarketDisruptionEventGrpAttributes {

};

struct LegMarketDisruptionFallbackGrpAttributes {

};

struct LegMarketDisruptionFallbackReferencePriceGrpAttributes {

};

struct LegOptionExerciseAttributes {

};

struct LegOptionExerciseBusinessCenterGrpAttributes {

};

struct LegOptionExerciseDatesAttributes {

};

struct LegOptionExerciseDateGrpAttributes {

};

struct LegOptionExerciseExpirationDateBusinessCenterGrpAttributes {

};

struct LegOptionExerciseExpirationAttributes {

};

struct LegOptionExerciseExpirationDateGrpAttributes {

};

struct LegPaymentScheduleFixingDayGrpAttributes {

};

struct LegPaymentStreamPricingBusinessCenterGrpAttributes {

};

struct LegPaymentStreamPaymentDateGrpAttributes {

};

struct LegPaymentStreamPricingDateGrpAttributes {

};

struct LegPaymentStreamPricingDayGrpAttributes {

};

struct LegPhysicalSettlTermGrpAttributes {

};

struct LegPhysicalSettlDeliverableObligationGrpAttributes {

};

struct LegPricingDateBusinessCenterGrpAttributes {

};

struct LegPricingDateTimeAttributes {

};

struct LegProtectionTermEventNewsSourceGrpAttributes {

};

struct LegProtectionTermGrpAttributes {

};

struct LegProtectionTermEventGrpAttributes {

};

struct LegProtectionTermEventQualifierGrpAttributes {

};

struct LegProtectionTermObligationGrpAttributes {

};

struct LegStreamCalculationPeriodDateGrpAttributes {

};

struct LegStreamCommoditySettlBusinessCenterGrpAttributes {

};

struct LegStreamCommodityAttributes {

};

struct LegStreamCommodityAltIDGrpAttributes {

};

struct LegStreamCommodityDataSourceGrpAttributes {

};

struct LegStreamCommoditySettlDayGrpAttributes {

};

struct LegStreamCommoditySettlTimeGrpAttributes {

};

struct LegStreamCommoditySettlPeriodGrpAttributes {

};

struct UnderlyingAssetAttributeGrpAttributes {

};

struct UnderlyingComplexEventAveragingObservationGrpAttributes {

};

struct UnderlyingComplexEventCreditEventGrpAttributes {

};

struct UnderlyingComplexEventCreditEventQualifierGrpAttributes {

};

struct UnderlyingComplexEventPeriodDateGrpAttributes {

};

struct UnderlyingComplexEventPeriodGrpAttributes {

};

struct UnderlyingComplexEventRateSourceGrpAttributes {

};

struct UnderlyingComplexEventDateBusinessCenterGrpAttributes {

};

struct UnderlyingComplexEventRelativeDateAttributes {

};

struct UnderlyingComplexEventCreditEventSourceGrpAttributes {

};

struct UnderlyingComplexEventScheduleGrpAttributes {

};

struct UnderlyingDeliveryScheduleGrpAttributes {

};

struct UnderlyingDeliveryScheduleSettlDayGrpAttributes {

};

struct UnderlyingDeliveryScheduleSettlTimeGrpAttributes {

};

struct UnderlyingDeliveryStreamAttributes {

};

struct UnderlyingStreamAssetAttributeGrpAttributes {

};

struct UnderlyingDeliveryStreamCycleGrpAttributes {

};

struct UnderlyingDeliveryStreamCommoditySourceGrpAttributes {

};

struct UnderlyingOptionExerciseAttributes {

};

struct UnderlyingOptionExerciseBusinessCenterGrpAttributes {

};

struct UnderlyingOptionExerciseDatesAttributes {

};

struct UnderlyingOptionExerciseDateGrpAttributes {

};

struct UnderlyingOptionExerciseExpirationDateBusinessCenterGrpAttributes {

};

struct UnderlyingOptionExerciseExpirationAttributes {

};

struct UnderlyingOptionExerciseExpirationDateGrpAttributes {

};

struct UnderlyingMarketDisruptionAttributes {

};

struct UnderlyingMarketDisruptionEventGrpAttributes {

};

struct UnderlyingMarketDisruptionFallbackGrpAttributes {

};

struct UnderlyingMarketDisruptionFallbackReferencePriceGrpAttributes {

};

struct UnderlyingPaymentScheduleFixingDayGrpAttributes {

};

struct UnderlyingPaymentStreamPricingBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamPaymentDateGrpAttributes {

};

struct UnderlyingPaymentStreamPricingDateGrpAttributes {

};

struct UnderlyingPaymentStreamPricingDayGrpAttributes {

};

struct UnderlyingPricingDateBusinessCenterGrpAttributes {

};

struct UnderlyingPricingDateTimeAttributes {

};

struct UnderlyingStreamCalculationPeriodDateGrpAttributes {

};

struct UnderlyingStreamCommoditySettlBusinessCenterGrpAttributes {

};

struct UnderlyingStreamCommodityAttributes {

};

struct UnderlyingStreamCommodityAltIDGrpAttributes {

};

struct UnderlyingStreamCommodityDataSourceGrpAttributes {

};

struct UnderlyingStreamCommoditySettlDayGrpAttributes {

};

struct UnderlyingStreamCommoditySettlTimeGrpAttributes {

};

struct UnderlyingStreamCommoditySettlPeriodGrpAttributes {

};

struct UnderlyingAdditionalTermBondRefGrpAttributes {

};

struct UnderlyingAdditionalTermGrpAttributes {

};

struct UnderlyingCashSettlDealerGrpAttributes {

};

struct UnderlyingCashSettlTermGrpAttributes {

};

struct UnderlyingPhysicalSettlTermGrpAttributes {

};

struct UnderlyingPhysicalSettlDeliverableObligationGrpAttributes {

};

struct UnderlyingProtectionTermGrpAttributes {

};

struct UnderlyingProtectionTermEventGrpAttributes {

};

struct UnderlyingProtectionTermEventQualifierGrpAttributes {

};

struct UnderlyingProtectionTermObligationGrpAttributes {

};

struct UnderlyingProtectionTermEventNewsSourceGrpAttributes {

};

struct UnderlyingProvisionCashSettlPaymentDatesAttributes {

};

struct UnderlyingProvisionCashSettlPaymentFixedDateGrpAttributes {

};

struct UnderlyingProvisionCashSettlQuoteSourceAttributes {

};

struct UnderlyingProvisionCashSettlValueDatesAttributes {

};

struct UnderlyingProvisionOptionExerciseFixedDateGrpAttributes {

};

struct UnderlyingProvisionOptionExerciseDatesAttributes {

};

struct UnderlyingProvisionOptionExpirationDateAttributes {

};

struct UnderlyingProvisionOptionRelevantUnderlyingDateAttributes {

};

struct UnderlyingProvisionGrpAttributes {

};

struct UnderlyingProvisionPartiesAttributes {

};

struct UnderlyingProvisionPtysSubGrpAttributes {

};

struct UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpAttributes {

};

struct UnderlyingProvisionCashSettlValueDateBusinessCenterGrpAttributes {

};

struct UnderlyingProvisionOptionExerciseBusinessCenterGrpAttributes {

};

struct UnderlyingProvisionOptionExpirationDateBusinessCenterGrpAttributes {

};

struct UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes {

};

struct UnderlyingProvisionDateBusinessCenterGrpAttributes {

};

struct TargetPtysSubGrpAttributes {

};

struct LegFinancingDetailsAttributes {

};

struct LegFinancingContractualDefinitionsGrpAttributes {

};

struct LegFinancingTermSupplementGrpAttributes {

};

struct LegFinancingContractualMatrixGrpAttributes {

};

struct RelativeValueGrpAttributes {

};

struct AuctionTypeRuleGrpAttributes {

};

struct FlexProductEligibilityGrpAttributes {

};

struct PriceRangeRuleGrpAttributes {

};

struct QuoteSizeRuleGrpAttributes {

};

struct RelatedMarketSegmentGrpAttributes {

};

struct ClearingPriceParametersGrpAttributes {

};

struct MiscFeesSubGrpAttributes {

};

struct CommissionDataGrpAttributes {

};

struct AllocCommissionDataGrpAttributes {

};

struct CashSettlDateAttributes {

};

struct CashSettlDateBusinessCenterGrpAttributes {

};

struct DividendAccrualFloatingRateAttributes {

};

struct DividendAccrualPaymentDateBusinessCenterGrpAttributes {

};

struct DividendAccrualPaymentDateAttributes {

};

struct DividendConditionsAttributes {

};

struct DividendFXTriggerDateAttributes {

};

struct DividendFXTriggerDateBusinessCenterGrpAttributes {

};

struct DividendPeriodGrpAttributes {

};

struct DividendPeriodBusinessCenterGrpAttributes {

};

struct ExtraordinaryEventGrpAttributes {

};

struct LegCashSettlDateAttributes {

};

struct LegCashSettlDateBusinessCenterGrpAttributes {

};

struct LegDividendAccrualPaymentDateBusinessCenterGrpAttributes {

};

struct LegDividendAccrualFloatingRateAttributes {

};

struct LegDividendAccrualPaymentDateAttributes {

};

struct LegDividendConditionsAttributes {

};

struct LegDividendFXTriggerDateAttributes {

};

struct LegDividendFXTriggerDateBusinessCenterGrpAttributes {

};

struct LegDividendPeriodGrpAttributes {

};

struct LegDividendPeriodBusinessCenterGrpAttributes {

};

struct LegExtraordinaryEventGrpAttributes {

};

struct LegOptionExerciseMakeWholeProvisionAttributes {

};

struct LegPaymentStreamCompoundingDateGrpAttributes {

};

struct LegPaymentStreamCompoundingDatesAttributes {

};

struct LegPaymentStreamCompoundingDatesBusinessCenterGrpAttributes {

};

struct LegPaymentStreamCompoundingEndDateAttributes {

};

struct LegPaymentStreamCompoundingFloatingRateAttributes {

};

struct LegPaymentStreamCompoundingStartDateAttributes {

};

struct LegPaymentStreamFormulaImageAttributes {

};

struct LegPaymentStreamFinalPricePaymentDateAttributes {

};

struct LegPaymentStreamFixingDateGrpAttributes {

};

struct LegPaymentStreamFormulaAttributes {

};

struct LegPaymentStreamFormulaMathGrpAttributes {

};

struct LegPaymentStubEndDateAttributes {

};

struct LegPaymentStubEndDateBusinessCenterGrpAttributes {

};

struct LegPaymentStubStartDateAttributes {

};

struct LegPaymentStubStartDateBusinessCenterGrpAttributes {

};

struct LegReturnRateDateGrpAttributes {

};

struct LegReturnRateFXConversionGrpAttributes {

};

struct LegReturnRateGrpAttributes {

};

struct LegReturnRateInformationSourceGrpAttributes {

};

struct LegReturnRatePriceGrpAttributes {

};

struct LegReturnRateValuationDateBusinessCenterGrpAttributes {

};

struct LegReturnRateValuationDateGrpAttributes {

};

struct LegSettlMethodElectionDateAttributes {

};

struct LegSettlMethodElectionDateBusinessCenterGrpAttributes {

};

struct OptionExerciseMakeWholeProvisionAttributes {

};

struct PaymentStreamCompoundingDateGrpAttributes {

};

struct PaymentStreamCompoundingDatesAttributes {

};

struct PaymentStreamCompoundingDatesBusinessCenterGrpAttributes {

};

struct PaymentStreamCompoundingEndDateAttributes {

};

struct PaymentStreamCompoundingFloatingRateAttributes {

};

struct PaymentStreamCompoundingStartDateAttributes {

};

struct PaymentStreamFormulaImageAttributes {

};

struct PaymentStreamFinalPricePaymentDateAttributes {

};

struct PaymentStreamFixingDateGrpAttributes {

};

struct PaymentStreamFormulaMathGrpAttributes {

};

struct PaymentStreamFormulaAttributes {

};

struct PaymentStubEndDateAttributes {

};

struct PaymentStubEndDateBusinessCenterGrpAttributes {

};

struct PaymentStubStartDateAttributes {

};

struct PaymentStubStartDateBusinessCenterGrpAttributes {

};

struct ReturnRateDateGrpAttributes {

};

struct ReturnRateFXConversionGrpAttributes {

};

struct ReturnRateGrpAttributes {

};

struct ReturnRateInformationSourceGrpAttributes {

};

struct ReturnRatePriceGrpAttributes {

};

struct ReturnRateValuationDateBusinessCenterGrpAttributes {

};

struct ReturnRateValuationDateGrpAttributes {

};

struct SettlMethodElectionDateBusinessCenterGrpAttributes {

};

struct SettlMethodElectionDateAttributes {

};

struct UnderlyingCashSettlDateBusinessCenterGrpAttributes {

};

struct UnderlyingCashSettlDateAttributes {

};

struct UnderlyingDividendAccrualPaymentDateBusinessCenterGrpAttributes {

};

struct UnderlyingDividendAccrualFloatingRateAttributes {

};

struct UnderlyingDividendAccrualPaymentDateAttributes {

};

struct UnderlyingDividendConditionsAttributes {

};

struct UnderlyingDividendFXTriggerDateAttributes {

};

struct UnderlyingDividendFXTriggerDateBusinessCenterGrpAttributes {

};

struct UnderlyingDividendPaymentGrpAttributes {

};

struct UnderlyingDividendPayoutAttributes {

};

struct UnderlyingDividendPeriodGrpAttributes {

};

struct UnderlyingDividendPeriodBusinessCenterGrpAttributes {

};

struct UnderlyingExtraordinaryEventGrpAttributes {

};

struct UnderlyingOptionExerciseMakeWholeProvisionAttributes {

};

struct UnderlyingPaymentStreamCompoundingDateGrpAttributes {

};

struct UnderlyingPaymentStreamCompoundingDatesAttributes {

};

struct UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStreamCompoundingEndDateAttributes {

};

struct UnderlyingPaymentStreamCompoundingFloatingRateAttributes {

};

struct UnderlyingPaymentStreamCompoundingStartDateAttributes {

};

struct UnderlyingPaymentStreamFormulaImageAttributes {

};

struct UnderlyingPaymentStreamFinalPricePaymentDateAttributes {

};

struct UnderlyingPaymentStreamFixingDateGrpAttributes {

};

struct UnderlyingPaymentStreamFormulaAttributes {

};

struct UnderlyingPaymentStreamFormulaMathGrpAttributes {

};

struct UnderlyingPaymentStubEndDateAttributes {

};

struct UnderlyingPaymentStubEndDateBusinessCenterGrpAttributes {

};

struct UnderlyingPaymentStubStartDateAttributes {

};

struct UnderlyingPaymentStubStartDateBusinessCenterGrpAttributes {

};

struct UnderlyingRateSpreadScheduleAttributes {

};

struct UnderlyingRateSpreadStepGrpAttributes {

};

struct UnderlyingReturnRateDateGrpAttributes {

};

struct UnderlyingReturnRateFXConversionGrpAttributes {

};

struct UnderlyingReturnRateGrpAttributes {

};

struct UnderlyingReturnRateInformationSourceGrpAttributes {

};

struct UnderlyingReturnRatePriceGrpAttributes {

};

struct UnderlyingReturnRateValuationDateBusinessCenterGrpAttributes {

};

struct UnderlyingReturnRateValuationDateGrpAttributes {

};

struct UnderlyingSettlMethodElectionDateBusinessCenterGrpAttributes {

};

struct UnderlyingSettlMethodElectionDateAttributes {

};

struct TrdRegPublicationGrpAttributes {

};

struct OrderAttributeGrpAttributes {

};

struct SideCollateralAmountGrpAttributes {

};

struct QuoteAttributeGrpAttributes {

};

struct PriceQualifierGrpAttributes {

};

struct IndexRollMonthGrpAttributes {

};

struct ReferenceDataDateGrpAttributes {

};

struct FloatingRateIndexAttributes {

};

struct AveragePriceDetailAttributes {

};

struct MatchExceptionGrpAttributes {

};

struct MatchingDataPointGrpAttributes {

};

struct OrderAggregationGrpAttributes {

};

struct ExecutionAggregationGrpAttributes {

};

struct BaseHeader_t {
	BaseHeaderElements BaseHeaderElements_;
			BaseHeaderAttributes BaseHeaderAttributes_;

};

struct HopGrp_Block_t {
	HopGrpElements HopGrpElements_;
			HopGrpAttributes HopGrpAttributes_;

};

struct MessageHeader_t: public BaseHeader_t {
	MessageHeaderAttributes MessageHeaderAttributes_;

};

struct BatchHeader_t: public BaseHeader_t {
	BatchHeaderElements BatchHeaderElements_;
			BatchHeaderAttributes BatchHeaderAttributes_;

};

struct Abstract_message_t {
	MessageHeader_t Hdr;
		
};

struct Batch_t {
	BatchElements BatchElements_;
			BatchAttributes BatchAttributes_;

};

struct CommissionData_Block_t {
	CommissionDataElements CommissionDataElements_;
			CommissionDataAttributes CommissionDataAttributes_;

};

struct DiscretionInstructions_Block_t {
	DiscretionInstructionsElements DiscretionInstructionsElements_;
			DiscretionInstructionsAttributes DiscretionInstructionsAttributes_;

};

struct FinancingDetails_Block_t {
	FinancingDetailsElements FinancingDetailsElements_;
			FinancingDetailsAttributes FinancingDetailsAttributes_;

};

struct Instrument_Block_t {
	InstrumentElements InstrumentElements_;
			InstrumentAttributes InstrumentAttributes_;

};

struct InstrumentExtension_Block_t {
	InstrumentExtensionElements InstrumentExtensionElements_;
			InstrumentExtensionAttributes InstrumentExtensionAttributes_;

};

struct InstrumentLeg_Block_t {
	InstrumentLegElements InstrumentLegElements_;
			InstrumentLegAttributes InstrumentLegAttributes_;

};

struct LegBenchmarkCurveData_Block_t {
	LegBenchmarkCurveDataElements LegBenchmarkCurveDataElements_;
			LegBenchmarkCurveDataAttributes LegBenchmarkCurveDataAttributes_;

};

struct LegStipulations_Block_t {
	LegStipulationsElements LegStipulationsElements_;
			LegStipulationsAttributes LegStipulationsAttributes_;

};

struct NestedParties_Block_t {
	NestedPartiesElements NestedPartiesElements_;
			NestedPartiesAttributes NestedPartiesAttributes_;

};

struct OrderQtyData_Block_t {
	OrderQtyDataElements OrderQtyDataElements_;
			OrderQtyDataAttributes OrderQtyDataAttributes_;

};

struct Parties_Block_t {
	PartiesElements PartiesElements_;
			PartiesAttributes PartiesAttributes_;

};

struct PegInstructions_Block_t {
	PegInstructionsElements PegInstructionsElements_;
			PegInstructionsAttributes PegInstructionsAttributes_;

};

struct PositionAmountData_Block_t {
	PositionAmountDataElements PositionAmountDataElements_;
			PositionAmountDataAttributes PositionAmountDataAttributes_;

};

struct PositionQty_Block_t {
	PositionQtyElements PositionQtyElements_;
			PositionQtyAttributes PositionQtyAttributes_;

};

struct SettlInstructionsData_Block_t {
	SettlInstructionsDataElements SettlInstructionsDataElements_;
			SettlInstructionsDataAttributes SettlInstructionsDataAttributes_;

};

struct SettlParties_Block_t {
	SettlPartiesElements SettlPartiesElements_;
			SettlPartiesAttributes SettlPartiesAttributes_;

};

struct SpreadOrBenchmarkCurveData_Block_t {
	SpreadOrBenchmarkCurveDataElements SpreadOrBenchmarkCurveDataElements_;
			SpreadOrBenchmarkCurveDataAttributes SpreadOrBenchmarkCurveDataAttributes_;

};

struct Stipulations_Block_t {
	StipulationsElements StipulationsElements_;
			StipulationsAttributes StipulationsAttributes_;

};

struct TrdRegTimestamps_Block_t {
	TrdRegTimestampsElements TrdRegTimestampsElements_;
			TrdRegTimestampsAttributes TrdRegTimestampsAttributes_;

};

struct UnderlyingInstrument_Block_t {
	UnderlyingInstrumentElements UnderlyingInstrumentElements_;
			UnderlyingInstrumentAttributes UnderlyingInstrumentAttributes_;

};

struct YieldData_Block_t {
	YieldDataElements YieldDataElements_;
			YieldDataAttributes YieldDataAttributes_;

};

struct UnderlyingStipulations_Block_t {
	UnderlyingStipulationsElements UnderlyingStipulationsElements_;
			UnderlyingStipulationsAttributes UnderlyingStipulationsAttributes_;

};

struct NestedParties2_Block_t {
	NestedParties2Elements NestedParties2Elements_;
			NestedParties2Attributes NestedParties2Attributes_;

};

struct NestedParties3_Block_t {
	NestedParties3Elements NestedParties3Elements_;
			NestedParties3Attributes NestedParties3Attributes_;

};

struct ClrInstGrp_Block_t {
	ClrInstGrpElements ClrInstGrpElements_;
			ClrInstGrpAttributes ClrInstGrpAttributes_;

};

struct CompIDReqGrp_Block_t {
	CompIDReqGrpElements CompIDReqGrpElements_;
			CompIDReqGrpAttributes CompIDReqGrpAttributes_;

};

struct CompIDStatGrp_Block_t {
	CompIDStatGrpElements CompIDStatGrpElements_;
			CompIDStatGrpAttributes CompIDStatGrpAttributes_;

};

struct ContAmtGrp_Block_t {
	ContAmtGrpElements ContAmtGrpElements_;
			ContAmtGrpAttributes ContAmtGrpAttributes_;

};

struct ContraGrp_Block_t {
	ContraGrpElements ContraGrpElements_;
			ContraGrpAttributes ContraGrpAttributes_;

};

struct ExecAllocGrp_Block_t {
	ExecAllocGrpElements ExecAllocGrpElements_;
			ExecAllocGrpAttributes ExecAllocGrpAttributes_;

};

struct InstrmtGrp_Block_t {
	InstrmtGrpElements InstrmtGrpElements_;
			InstrmtGrpAttributes InstrmtGrpAttributes_;

};

struct InstrmtLegExecGrp_Block_t {
	InstrmtLegExecGrpElements InstrmtLegExecGrpElements_;
			InstrmtLegExecGrpAttributes InstrmtLegExecGrpAttributes_;

};

struct InstrmtLegGrp_Block_t {
	InstrmtLegGrpElements InstrmtLegGrpElements_;
			InstrmtLegGrpAttributes InstrmtLegGrpAttributes_;

};

struct InstrmtLegIOIGrp_Block_t {
	InstrmtLegIOIGrpElements InstrmtLegIOIGrpElements_;
			InstrmtLegIOIGrpAttributes InstrmtLegIOIGrpAttributes_;

};

struct InstrmtLegSecListGrp_Block_t {
	InstrmtLegSecListGrpElements InstrmtLegSecListGrpElements_;
			InstrmtLegSecListGrpAttributes InstrmtLegSecListGrpAttributes_;

};

struct InstrmtMDReqGrp_Block_t {
	InstrmtMDReqGrpElements InstrmtMDReqGrpElements_;
			InstrmtMDReqGrpAttributes InstrmtMDReqGrpAttributes_;

};

struct LegOrdGrp_Block_t {
	LegOrdGrpElements LegOrdGrpElements_;
			LegOrdGrpAttributes LegOrdGrpAttributes_;

};

struct LegPreAllocGrp_Block_t {
	LegPreAllocGrpElements LegPreAllocGrpElements_;
			LegPreAllocGrpAttributes LegPreAllocGrpAttributes_;

};

struct LinesOfTextGrp_Block_t {
	LinesOfTextGrpElements LinesOfTextGrpElements_;
			LinesOfTextGrpAttributes LinesOfTextGrpAttributes_;

};

struct MiscFeesGrp_Block_t {
	MiscFeesGrpElements MiscFeesGrpElements_;
			MiscFeesGrpAttributes MiscFeesGrpAttributes_;

};

struct OrdAllocGrp_Block_t {
	OrdAllocGrpElements OrdAllocGrpElements_;
			OrdAllocGrpAttributes OrdAllocGrpAttributes_;

};

struct PreAllocGrp_Block_t {
	PreAllocGrpElements PreAllocGrpElements_;
			PreAllocGrpAttributes PreAllocGrpAttributes_;

};

struct PreAllocMlegGrp_Block_t {
	PreAllocMlegGrpElements PreAllocMlegGrpElements_;
			PreAllocMlegGrpAttributes PreAllocMlegGrpAttributes_;

};

struct RoutingGrp_Block_t {
	RoutingGrpElements RoutingGrpElements_;
			RoutingGrpAttributes RoutingGrpAttributes_;

};

struct TrdgSesGrp_Block_t {
	TrdgSesGrpElements TrdgSesGrpElements_;
			TrdgSesGrpAttributes TrdgSesGrpAttributes_;

};

struct UndInstrmtGrp_Block_t {
	UndInstrmtGrpElements UndInstrmtGrpElements_;
			UndInstrmtGrpAttributes UndInstrmtGrpAttributes_;

};

struct EvntGrp_Block_t {
	EvntGrpElements EvntGrpElements_;
			EvntGrpAttributes EvntGrpAttributes_;

};

struct SecAltIDGrp_Block_t {
	SecAltIDGrpElements SecAltIDGrpElements_;
			SecAltIDGrpAttributes SecAltIDGrpAttributes_;

};

struct LegSecAltIDGrp_Block_t {
	LegSecAltIDGrpElements LegSecAltIDGrpElements_;
			LegSecAltIDGrpAttributes LegSecAltIDGrpAttributes_;

};

struct UndSecAltIDGrp_Block_t {
	UndSecAltIDGrpElements UndSecAltIDGrpElements_;
			UndSecAltIDGrpAttributes UndSecAltIDGrpAttributes_;

};

struct AttrbGrp_Block_t {
	AttrbGrpElements AttrbGrpElements_;
			AttrbGrpAttributes AttrbGrpAttributes_;

};

struct DlvyInstGrp_Block_t {
	DlvyInstGrpElements DlvyInstGrpElements_;
			DlvyInstGrpAttributes DlvyInstGrpAttributes_;

};

struct SettlPtysSubGrp_Block_t {
	SettlPtysSubGrpElements SettlPtysSubGrpElements_;
			SettlPtysSubGrpAttributes SettlPtysSubGrpAttributes_;

};

struct PtysSubGrp_Block_t {
	PtysSubGrpElements PtysSubGrpElements_;
			PtysSubGrpAttributes PtysSubGrpAttributes_;

};

struct NstdPtysSubGrp_Block_t {
	NstdPtysSubGrpElements NstdPtysSubGrpElements_;
			NstdPtysSubGrpAttributes NstdPtysSubGrpAttributes_;

};

struct NstdPtys2SubGrp_Block_t {
	NstdPtys2SubGrpElements NstdPtys2SubGrpElements_;
			NstdPtys2SubGrpAttributes NstdPtys2SubGrpAttributes_;

};

struct NstdPtys3SubGrp_Block_t {
	NstdPtys3SubGrpElements NstdPtys3SubGrpElements_;
			NstdPtys3SubGrpAttributes NstdPtys3SubGrpAttributes_;

};

struct StrategyParametersGrp_Block_t {
	StrategyParametersGrpElements StrategyParametersGrpElements_;
			StrategyParametersGrpAttributes StrategyParametersGrpAttributes_;

};

struct InstrumentParties_Block_t {
	InstrumentPartiesElements InstrumentPartiesElements_;
			InstrumentPartiesAttributes InstrumentPartiesAttributes_;

};

struct InstrumentPtysSubGrp_Block_t {
	InstrumentPtysSubGrpElements InstrumentPtysSubGrpElements_;
			InstrumentPtysSubGrpAttributes InstrumentPtysSubGrpAttributes_;

};

struct UndlyInstrumentParties_Block_t {
	UndlyInstrumentPartiesElements UndlyInstrumentPartiesElements_;
			UndlyInstrumentPartiesAttributes UndlyInstrumentPartiesAttributes_;

};

struct UndlyInstrumentPtysSubGrp_Block_t {
	UndlyInstrumentPtysSubGrpElements UndlyInstrumentPtysSubGrpElements_;
			UndlyInstrumentPtysSubGrpAttributes UndlyInstrumentPtysSubGrpAttributes_;

};

struct DisplayInstruction_Block_t {
	DisplayInstructionElements DisplayInstructionElements_;
			DisplayInstructionAttributes DisplayInstructionAttributes_;

};

struct TriggeringInstruction_Block_t {
	TriggeringInstructionElements TriggeringInstructionElements_;
			TriggeringInstructionAttributes TriggeringInstructionAttributes_;

};

struct RootParties_Block_t {
	RootPartiesElements RootPartiesElements_;
			RootPartiesAttributes RootPartiesAttributes_;

};

struct RootSubParties_Block_t {
	RootSubPartiesElements RootSubPartiesElements_;
			RootSubPartiesAttributes RootSubPartiesAttributes_;

};

struct TrdSessLstGrp_Block_t {
	TrdSessLstGrpElements TrdSessLstGrpElements_;
			TrdSessLstGrpAttributes TrdSessLstGrpAttributes_;

};

struct MsgTypeGrp_Block_t {
	MsgTypeGrpElements MsgTypeGrpElements_;
			MsgTypeGrpAttributes MsgTypeGrpAttributes_;

};

struct SecurityTradingRules_Block_t {
	SecurityTradingRulesElements SecurityTradingRulesElements_;
			SecurityTradingRulesAttributes SecurityTradingRulesAttributes_;

};

struct SettlDetails_Block_t {
	SettlDetailsElements SettlDetailsElements_;
			SettlDetailsAttributes SettlDetailsAttributes_;

};

struct SecurityXML_Block_t {
	SecurityXMLAttributes SecurityXMLAttributes_;

};

struct TickRules_Block_t {
	TickRulesElements TickRulesElements_;
			TickRulesAttributes TickRulesAttributes_;

};

struct StrikeRules_Block_t {
	StrikeRulesElements StrikeRulesElements_;
			StrikeRulesAttributes StrikeRulesAttributes_;

};

struct MaturityRules_Block_t {
	MaturityRulesElements MaturityRulesElements_;
			MaturityRulesAttributes MaturityRulesAttributes_;

};

struct SecondaryPriceLimits_Block_t {
	SecondaryPriceLimitsElements SecondaryPriceLimitsElements_;
			SecondaryPriceLimitsAttributes SecondaryPriceLimitsAttributes_;

};

struct PriceLimits_Block_t {
	PriceLimitsElements PriceLimitsElements_;
			PriceLimitsAttributes PriceLimitsAttributes_;

};

struct MarketDataFeedTypes_Block_t {
	MarketDataFeedTypesElements MarketDataFeedTypesElements_;
			MarketDataFeedTypesAttributes MarketDataFeedTypesAttributes_;

};

struct LotTypeRules_Block_t {
	LotTypeRulesElements LotTypeRulesElements_;
			LotTypeRulesAttributes LotTypeRulesAttributes_;

};

struct MatchRules_Block_t {
	MatchRulesElements MatchRulesElements_;
			MatchRulesAttributes MatchRulesAttributes_;

};

struct ExecInstRules_Block_t {
	ExecInstRulesElements ExecInstRulesElements_;
			ExecInstRulesAttributes ExecInstRulesAttributes_;

};

struct TimeInForceRules_Block_t {
	TimeInForceRulesElements TimeInForceRulesElements_;
			TimeInForceRulesAttributes TimeInForceRulesAttributes_;

};

struct OrdTypeRules_Block_t {
	OrdTypeRulesElements OrdTypeRulesElements_;
			OrdTypeRulesAttributes OrdTypeRulesAttributes_;

};

struct TradingSessionRules_Block_t {
	TradingSessionRulesElements TradingSessionRulesElements_;
			TradingSessionRulesAttributes TradingSessionRulesAttributes_;

};

struct TradingSessionRulesGrp_Block_t {
	TradingSessionRulesGrpElements TradingSessionRulesGrpElements_;
			TradingSessionRulesGrpAttributes TradingSessionRulesGrpAttributes_;

};

struct BaseTradingRules_Block_t {
	BaseTradingRulesElements BaseTradingRulesElements_;
			BaseTradingRulesAttributes BaseTradingRulesAttributes_;

};

struct MarketSegmentGrp_Block_t {
	MarketSegmentGrpElements MarketSegmentGrpElements_;
			MarketSegmentGrpAttributes MarketSegmentGrpAttributes_;

};

struct DerivativeInstrumentPartySubIDsGrp_Block_t {
	DerivativeInstrumentPartySubIDsGrpElements DerivativeInstrumentPartySubIDsGrpElements_;
			DerivativeInstrumentPartySubIDsGrpAttributes DerivativeInstrumentPartySubIDsGrpAttributes_;

};

struct DerivativeInstrumentParties_Block_t {
	DerivativeInstrumentPartiesElements DerivativeInstrumentPartiesElements_;
			DerivativeInstrumentPartiesAttributes DerivativeInstrumentPartiesAttributes_;

};

struct DerivativeInstrumentAttribute_Block_t {
	DerivativeInstrumentAttributeElements DerivativeInstrumentAttributeElements_;
			DerivativeInstrumentAttributeAttributes DerivativeInstrumentAttributeAttributes_;

};

struct NestedInstrumentAttribute_Block_t {
	NestedInstrumentAttributeElements NestedInstrumentAttributeElements_;
			NestedInstrumentAttributeAttributes NestedInstrumentAttributeAttributes_;

};

struct DerivativeInstrument_Block_t {
	DerivativeInstrumentElements DerivativeInstrumentElements_;
			DerivativeInstrumentAttributes DerivativeInstrumentAttributes_;

};

struct DerivativeSecurityAltIDGrp_Block_t {
	DerivativeSecurityAltIDGrpElements DerivativeSecurityAltIDGrpElements_;
			DerivativeSecurityAltIDGrpAttributes DerivativeSecurityAltIDGrpAttributes_;

};

struct DerivativeEventsGrp_Block_t {
	DerivativeEventsGrpElements DerivativeEventsGrpElements_;
			DerivativeEventsGrpAttributes DerivativeEventsGrpAttributes_;

};

struct DerivativeSecurityDefinition_Block_t {
	DerivativeSecurityDefinitionElements DerivativeSecurityDefinitionElements_;
			DerivativeSecurityDefinitionAttributes DerivativeSecurityDefinitionAttributes_;

};

struct RelSymDerivSecUpdGrp_Block_t {
	RelSymDerivSecUpdGrpElements RelSymDerivSecUpdGrpElements_;
			RelSymDerivSecUpdGrpAttributes RelSymDerivSecUpdGrpAttributes_;

};

struct DerivativeSecurityXML_Block_t {
	DerivativeSecurityXMLAttributes DerivativeSecurityXMLAttributes_;

};

struct ApplicationSequenceControl_Block_t {
	ApplicationSequenceControlElements ApplicationSequenceControlElements_;
			ApplicationSequenceControlAttributes ApplicationSequenceControlAttributes_;

};

struct NstdPtys4SubGrp_Block_t {
	NstdPtys4SubGrpElements NstdPtys4SubGrpElements_;
			NstdPtys4SubGrpAttributes NstdPtys4SubGrpAttributes_;

};

struct NestedParties4_Block_t {
	NestedParties4Elements NestedParties4Elements_;
			NestedParties4Attributes NestedParties4Attributes_;

};

struct RateSource_Block_t {
	RateSourceElements RateSourceElements_;
			RateSourceAttributes RateSourceAttributes_;

};

struct TargetParties_Block_t {
	TargetPartiesElements TargetPartiesElements_;
			TargetPartiesAttributes TargetPartiesAttributes_;

};

struct ComplexEvents_Block_t {
	ComplexEventsElements ComplexEventsElements_;
			ComplexEventsAttributes ComplexEventsAttributes_;

};

struct ComplexEventDates_Block_t {
	ComplexEventDatesElements ComplexEventDatesElements_;
			ComplexEventDatesAttributes ComplexEventDatesAttributes_;

};

struct ComplexEventTimes_Block_t {
	ComplexEventTimesElements ComplexEventTimesElements_;
			ComplexEventTimesAttributes ComplexEventTimesAttributes_;

};

struct MatchingInstructions_Block_t {
	MatchingInstructionsElements MatchingInstructionsElements_;
			MatchingInstructionsAttributes MatchingInstructionsAttributes_;

};

struct LimitAmts_Block_t {
	LimitAmtsElements LimitAmtsElements_;
			LimitAmtsAttributes LimitAmtsAttributes_;

};

struct MarginAmount_Block_t {
	MarginAmountElements MarginAmountElements_;
			MarginAmountAttributes MarginAmountAttributes_;

};

struct RelatedInstrumentGrp_Block_t {
	RelatedInstrumentGrpElements RelatedInstrumentGrpElements_;
			RelatedInstrumentGrpAttributes RelatedInstrumentGrpAttributes_;

};

struct PartyRelationshipGrp_Block_t {
	PartyRelationshipGrpElements PartyRelationshipGrpElements_;
			PartyRelationshipGrpAttributes PartyRelationshipGrpAttributes_;

};

struct PartyDetailGrp_Block_t {
	PartyDetailGrpElements PartyDetailGrpElements_;
			PartyDetailGrpAttributes PartyDetailGrpAttributes_;

};

struct PartyDetailAltIDGrp_Block_t {
	PartyDetailAltIDGrpElements PartyDetailAltIDGrpElements_;
			PartyDetailAltIDGrpAttributes PartyDetailAltIDGrpAttributes_;

};

struct PartyDetailAltSubGrp_Block_t {
	PartyDetailAltSubGrpElements PartyDetailAltSubGrpElements_;
			PartyDetailAltSubGrpAttributes PartyDetailAltSubGrpAttributes_;

};

struct InstrumentScope_Block_t {
	InstrumentScopeElements InstrumentScopeElements_;
			InstrumentScopeAttributes InstrumentScopeAttributes_;

};

struct InstrumentScopeSecurityAltIDGrp_Block_t {
	InstrumentScopeSecurityAltIDGrpElements InstrumentScopeSecurityAltIDGrpElements_;
			InstrumentScopeSecurityAltIDGrpAttributes InstrumentScopeSecurityAltIDGrpAttributes_;

};

struct RelatedPartyDetailGrp_Block_t {
	RelatedPartyDetailGrpElements RelatedPartyDetailGrpElements_;
			RelatedPartyDetailGrpAttributes RelatedPartyDetailGrpAttributes_;

};

struct RelatedPartyDetailSubGrp_Block_t {
	RelatedPartyDetailSubGrpElements RelatedPartyDetailSubGrpElements_;
			RelatedPartyDetailSubGrpAttributes RelatedPartyDetailSubGrpAttributes_;

};

struct RelatedPartyDetailAltIDGrp_Block_t {
	RelatedPartyDetailAltIDGrpElements RelatedPartyDetailAltIDGrpElements_;
			RelatedPartyDetailAltIDGrpAttributes RelatedPartyDetailAltIDGrpAttributes_;

};

struct RelatedPartyDetailAltSubGrp_Block_t {
	RelatedPartyDetailAltSubGrpElements RelatedPartyDetailAltSubGrpElements_;
			RelatedPartyDetailAltSubGrpAttributes RelatedPartyDetailAltSubGrpAttributes_;

};

struct InstrumentScopeGrp_Block_t {
	InstrumentScopeGrpElements InstrumentScopeGrpElements_;
			InstrumentScopeGrpAttributes InstrumentScopeGrpAttributes_;

};

struct RequestingPartyGrp_Block_t {
	RequestingPartyGrpElements RequestingPartyGrpElements_;
			RequestingPartyGrpAttributes RequestingPartyGrpAttributes_;

};

struct RequestingPartySubGrp_Block_t {
	RequestingPartySubGrpElements RequestingPartySubGrpElements_;
			RequestingPartySubGrpAttributes RequestingPartySubGrpAttributes_;

};

struct PartyDetailSubGrp_Block_t {
	PartyDetailSubGrpElements PartyDetailSubGrpElements_;
			PartyDetailSubGrpAttributes PartyDetailSubGrpAttributes_;

};

struct SecurityClassificationGrp_Block_t {
	SecurityClassificationGrpElements SecurityClassificationGrpElements_;
			SecurityClassificationGrpAttributes SecurityClassificationGrpAttributes_;

};

struct ThrottleParamsGrp_Block_t {
	ThrottleParamsGrpElements ThrottleParamsGrpElements_;
			ThrottleParamsGrpAttributes ThrottleParamsGrpAttributes_;

};

struct ThrottleMsgTypeGrp_Block_t {
	ThrottleMsgTypeGrpElements ThrottleMsgTypeGrpElements_;
			ThrottleMsgTypeGrpAttributes ThrottleMsgTypeGrpAttributes_;

};

struct ThrottleResponse_Block_t {
	ThrottleResponseElements ThrottleResponseElements_;
			ThrottleResponseAttributes ThrottleResponseAttributes_;

};

struct CollateralAmountGrp_Block_t {
	CollateralAmountGrpElements CollateralAmountGrpElements_;
			CollateralAmountGrpAttributes CollateralAmountGrpAttributes_;

};

struct MarketSegmentScopeGrp_Block_t {
	MarketSegmentScopeGrpElements MarketSegmentScopeGrpElements_;
			MarketSegmentScopeGrpAttributes MarketSegmentScopeGrpAttributes_;

};

struct TargetMarketSegmentGrp_Block_t {
	TargetMarketSegmentGrpElements TargetMarketSegmentGrpElements_;
			TargetMarketSegmentGrpAttributes TargetMarketSegmentGrpAttributes_;

};

struct AffectedMarketSegmentGrp_Block_t {
	AffectedMarketSegmentGrpElements AffectedMarketSegmentGrpElements_;
			AffectedMarketSegmentGrpAttributes AffectedMarketSegmentGrpAttributes_;

};

struct NotAffectedMarketSegmentGrp_Block_t {
	NotAffectedMarketSegmentGrpElements NotAffectedMarketSegmentGrpElements_;
			NotAffectedMarketSegmentGrpAttributes NotAffectedMarketSegmentGrpAttributes_;

};

struct DisclosureInstructionGrp_Block_t {
	DisclosureInstructionGrpElements DisclosureInstructionGrpElements_;
			DisclosureInstructionGrpAttributes DisclosureInstructionGrpAttributes_;

};

struct TradeAllocAmtGrp_Block_t {
	TradeAllocAmtGrpElements TradeAllocAmtGrpElements_;
			TradeAllocAmtGrpAttributes TradeAllocAmtGrpAttributes_;

};

struct TradePriceConditionGrp_Block_t {
	TradePriceConditionGrpElements TradePriceConditionGrpElements_;
			TradePriceConditionGrpAttributes TradePriceConditionGrpAttributes_;

};

struct TradeQtyGrp_Block_t {
	TradeQtyGrpElements TradeQtyGrpElements_;
			TradeQtyGrpAttributes TradeQtyGrpAttributes_;

};

struct TradePositionQty_Block_t {
	TradePositionQtyElements TradePositionQtyElements_;
			TradePositionQtyAttributes TradePositionQtyAttributes_;

};

struct RelatedTradeGrp_Block_t {
	RelatedTradeGrpElements RelatedTradeGrpElements_;
			RelatedTradeGrpAttributes RelatedTradeGrpAttributes_;

};

struct RelatedPositionGrp_Block_t {
	RelatedPositionGrpElements RelatedPositionGrpElements_;
			RelatedPositionGrpAttributes RelatedPositionGrpAttributes_;

};

struct ValueChecksGrp_Block_t {
	ValueChecksGrpElements ValueChecksGrpElements_;
			ValueChecksGrpAttributes ValueChecksGrpAttributes_;

};

struct LegSecurityXML_Block_t {
	LegSecurityXMLAttributes LegSecurityXMLAttributes_;

};

struct UnderlyingSecurityXML_Block_t {
	UnderlyingSecurityXMLAttributes UnderlyingSecurityXMLAttributes_;

};

struct AdditionalTermBondRefGrp_Block_t {
	AdditionalTermBondRefGrpElements AdditionalTermBondRefGrpElements_;
			AdditionalTermBondRefGrpAttributes AdditionalTermBondRefGrpAttributes_;

};

struct AdditionalTermGrp_Block_t {
	AdditionalTermGrpElements AdditionalTermGrpElements_;
			AdditionalTermGrpAttributes AdditionalTermGrpAttributes_;

};

struct AllocRegulatoryTradeIDGrp_Block_t {
	AllocRegulatoryTradeIDGrpElements AllocRegulatoryTradeIDGrpElements_;
			AllocRegulatoryTradeIDGrpAttributes AllocRegulatoryTradeIDGrpAttributes_;

};

struct CashSettlTermGrp_Block_t {
	CashSettlTermGrpElements CashSettlTermGrpElements_;
			CashSettlTermGrpAttributes CashSettlTermGrpAttributes_;

};

struct FinancingContractualDefinitionGrp_Block_t {
	FinancingContractualDefinitionGrpElements FinancingContractualDefinitionGrpElements_;
			FinancingContractualDefinitionGrpAttributes FinancingContractualDefinitionGrpAttributes_;

};

struct FinancingContractualMatrixGrp_Block_t {
	FinancingContractualMatrixGrpElements FinancingContractualMatrixGrpElements_;
			FinancingContractualMatrixGrpAttributes FinancingContractualMatrixGrpAttributes_;

};

struct FinancingTermSupplementGrp_Block_t {
	FinancingTermSupplementGrpElements FinancingTermSupplementGrpElements_;
			FinancingTermSupplementGrpAttributes FinancingTermSupplementGrpAttributes_;

};

struct LegEvntGrp_Block_t {
	LegEvntGrpElements LegEvntGrpElements_;
			LegEvntGrpAttributes LegEvntGrpAttributes_;

};

struct LegPaymentScheduleGrp_Block_t {
	LegPaymentScheduleGrpElements LegPaymentScheduleGrpElements_;
			LegPaymentScheduleGrpAttributes LegPaymentScheduleGrpAttributes_;

};

struct LegPaymentScheduleRateSourceGrp_Block_t {
	LegPaymentScheduleRateSourceGrpElements LegPaymentScheduleRateSourceGrpElements_;
			LegPaymentScheduleRateSourceGrpAttributes LegPaymentScheduleRateSourceGrpAttributes_;

};

struct LegPaymentStream_Block_t {
	LegPaymentStreamElements LegPaymentStreamElements_;
			LegPaymentStreamAttributes LegPaymentStreamAttributes_;

};

struct LegPaymentStreamFixedRate_Block_t {
	LegPaymentStreamFixedRateElements LegPaymentStreamFixedRateElements_;
			LegPaymentStreamFixedRateAttributes LegPaymentStreamFixedRateAttributes_;

};

struct LegPaymentStreamFloatingRate_Block_t {
	LegPaymentStreamFloatingRateElements LegPaymentStreamFloatingRateElements_;
			LegPaymentStreamFloatingRateAttributes LegPaymentStreamFloatingRateAttributes_;

};

struct LegPaymentStreamNonDeliverableFixingDateGrp_Block_t {
	LegPaymentStreamNonDeliverableFixingDateGrpElements LegPaymentStreamNonDeliverableFixingDateGrpElements_;
			LegPaymentStreamNonDeliverableFixingDateGrpAttributes LegPaymentStreamNonDeliverableFixingDateGrpAttributes_;

};

struct LegPaymentStreamNonDeliverableSettlTerms_Block_t {
	LegPaymentStreamNonDeliverableSettlTermsElements LegPaymentStreamNonDeliverableSettlTermsElements_;
			LegPaymentStreamNonDeliverableSettlTermsAttributes LegPaymentStreamNonDeliverableSettlTermsAttributes_;

};

struct LegPaymentStreamPaymentDates_Block_t {
	LegPaymentStreamPaymentDatesElements LegPaymentStreamPaymentDatesElements_;
			LegPaymentStreamPaymentDatesAttributes LegPaymentStreamPaymentDatesAttributes_;

};

struct LegPaymentStreamResetDates_Block_t {
	LegPaymentStreamResetDatesElements LegPaymentStreamResetDatesElements_;
			LegPaymentStreamResetDatesAttributes LegPaymentStreamResetDatesAttributes_;

};

struct LegPaymentStubGrp_Block_t {
	LegPaymentStubGrpElements LegPaymentStubGrpElements_;
			LegPaymentStubGrpAttributes LegPaymentStubGrpAttributes_;

};

struct LegProvisionCashSettlPaymentDates_Block_t {
	LegProvisionCashSettlPaymentDatesElements LegProvisionCashSettlPaymentDatesElements_;
			LegProvisionCashSettlPaymentDatesAttributes LegProvisionCashSettlPaymentDatesAttributes_;

};

struct LegProvisionCashSettlPaymentFixedDateGrp_Block_t {
	LegProvisionCashSettlPaymentFixedDateGrpElements LegProvisionCashSettlPaymentFixedDateGrpElements_;
			LegProvisionCashSettlPaymentFixedDateGrpAttributes LegProvisionCashSettlPaymentFixedDateGrpAttributes_;

};

struct LegProvisionCashSettlValueDates_Block_t {
	LegProvisionCashSettlValueDatesElements LegProvisionCashSettlValueDatesElements_;
			LegProvisionCashSettlValueDatesAttributes LegProvisionCashSettlValueDatesAttributes_;

};

struct LegProvisionOptionExerciseFixedDateGrp_Block_t {
	LegProvisionOptionExerciseFixedDateGrpElements LegProvisionOptionExerciseFixedDateGrpElements_;
			LegProvisionOptionExerciseFixedDateGrpAttributes LegProvisionOptionExerciseFixedDateGrpAttributes_;

};

struct LegProvisionOptionExerciseDates_Block_t {
	LegProvisionOptionExerciseDatesElements LegProvisionOptionExerciseDatesElements_;
			LegProvisionOptionExerciseDatesAttributes LegProvisionOptionExerciseDatesAttributes_;

};

struct LegProvisionOptionExpirationDate_Block_t {
	LegProvisionOptionExpirationDateElements LegProvisionOptionExpirationDateElements_;
			LegProvisionOptionExpirationDateAttributes LegProvisionOptionExpirationDateAttributes_;

};

struct LegProvisionOptionRelevantUnderlyingDate_Block_t {
	LegProvisionOptionRelevantUnderlyingDateElements LegProvisionOptionRelevantUnderlyingDateElements_;
			LegProvisionOptionRelevantUnderlyingDateAttributes LegProvisionOptionRelevantUnderlyingDateAttributes_;

};

struct LegProvisionGrp_Block_t {
	LegProvisionGrpElements LegProvisionGrpElements_;
			LegProvisionGrpAttributes LegProvisionGrpAttributes_;

};

struct LegProvisionParties_Block_t {
	LegProvisionPartiesElements LegProvisionPartiesElements_;
			LegProvisionPartiesAttributes LegProvisionPartiesAttributes_;

};

struct LegProvisionPtysSubGrp_Block_t {
	LegProvisionPtysSubGrpElements LegProvisionPtysSubGrpElements_;
			LegProvisionPtysSubGrpAttributes LegProvisionPtysSubGrpAttributes_;

};

struct LegSecondaryAssetGrp_Block_t {
	LegSecondaryAssetGrpElements LegSecondaryAssetGrpElements_;
			LegSecondaryAssetGrpAttributes LegSecondaryAssetGrpAttributes_;

};

struct LegSettlRateDisruptionFallbackGrp_Block_t {
	LegSettlRateDisruptionFallbackGrpElements LegSettlRateDisruptionFallbackGrpElements_;
			LegSettlRateDisruptionFallbackGrpAttributes LegSettlRateDisruptionFallbackGrpAttributes_;

};

struct LegStreamCalculationPeriodDates_Block_t {
	LegStreamCalculationPeriodDatesElements LegStreamCalculationPeriodDatesElements_;
			LegStreamCalculationPeriodDatesAttributes LegStreamCalculationPeriodDatesAttributes_;

};

struct LegStreamEffectiveDate_Block_t {
	LegStreamEffectiveDateElements LegStreamEffectiveDateElements_;
			LegStreamEffectiveDateAttributes LegStreamEffectiveDateAttributes_;

};

struct LegStreamGrp_Block_t {
	LegStreamGrpElements LegStreamGrpElements_;
			LegStreamGrpAttributes LegStreamGrpAttributes_;

};

struct LegStreamTerminationDate_Block_t {
	LegStreamTerminationDateElements LegStreamTerminationDateElements_;
			LegStreamTerminationDateAttributes LegStreamTerminationDateAttributes_;

};

struct PaymentGrp_Block_t {
	PaymentGrpElements PaymentGrpElements_;
			PaymentGrpAttributes PaymentGrpAttributes_;

};

struct PaymentScheduleGrp_Block_t {
	PaymentScheduleGrpElements PaymentScheduleGrpElements_;
			PaymentScheduleGrpAttributes PaymentScheduleGrpAttributes_;

};

struct PaymentScheduleRateSourceGrp_Block_t {
	PaymentScheduleRateSourceGrpElements PaymentScheduleRateSourceGrpElements_;
			PaymentScheduleRateSourceGrpAttributes PaymentScheduleRateSourceGrpAttributes_;

};

struct PaymentSettlGrp_Block_t {
	PaymentSettlGrpElements PaymentSettlGrpElements_;
			PaymentSettlGrpAttributes PaymentSettlGrpAttributes_;

};

struct PaymentSettlParties_Block_t {
	PaymentSettlPartiesElements PaymentSettlPartiesElements_;
			PaymentSettlPartiesAttributes PaymentSettlPartiesAttributes_;

};

struct PaymentSettlPtysSubGrp_Block_t {
	PaymentSettlPtysSubGrpElements PaymentSettlPtysSubGrpElements_;
			PaymentSettlPtysSubGrpAttributes PaymentSettlPtysSubGrpAttributes_;

};

struct PaymentStream_Block_t {
	PaymentStreamElements PaymentStreamElements_;
			PaymentStreamAttributes PaymentStreamAttributes_;

};

struct PaymentStreamFixedRate_Block_t {
	PaymentStreamFixedRateElements PaymentStreamFixedRateElements_;
			PaymentStreamFixedRateAttributes PaymentStreamFixedRateAttributes_;

};

struct PaymentStreamFloatingRate_Block_t {
	PaymentStreamFloatingRateElements PaymentStreamFloatingRateElements_;
			PaymentStreamFloatingRateAttributes PaymentStreamFloatingRateAttributes_;

};

struct PaymentStreamNonDeliverableFixingDateGrp_Block_t {
	PaymentStreamNonDeliverableFixingDateGrpElements PaymentStreamNonDeliverableFixingDateGrpElements_;
			PaymentStreamNonDeliverableFixingDateGrpAttributes PaymentStreamNonDeliverableFixingDateGrpAttributes_;

};

struct PaymentStreamNonDeliverableSettlTerms_Block_t {
	PaymentStreamNonDeliverableSettlTermsElements PaymentStreamNonDeliverableSettlTermsElements_;
			PaymentStreamNonDeliverableSettlTermsAttributes PaymentStreamNonDeliverableSettlTermsAttributes_;

};

struct PaymentStreamPaymentDates_Block_t {
	PaymentStreamPaymentDatesElements PaymentStreamPaymentDatesElements_;
			PaymentStreamPaymentDatesAttributes PaymentStreamPaymentDatesAttributes_;

};

struct PaymentStreamResetDates_Block_t {
	PaymentStreamResetDatesElements PaymentStreamResetDatesElements_;
			PaymentStreamResetDatesAttributes PaymentStreamResetDatesAttributes_;

};

struct PaymentStubGrp_Block_t {
	PaymentStubGrpElements PaymentStubGrpElements_;
			PaymentStubGrpAttributes PaymentStubGrpAttributes_;

};

struct PhysicalSettlTermGrp_Block_t {
	PhysicalSettlTermGrpElements PhysicalSettlTermGrpElements_;
			PhysicalSettlTermGrpAttributes PhysicalSettlTermGrpAttributes_;

};

struct PhysicalSettlDeliverableObligationGrp_Block_t {
	PhysicalSettlDeliverableObligationGrpElements PhysicalSettlDeliverableObligationGrpElements_;
			PhysicalSettlDeliverableObligationGrpAttributes PhysicalSettlDeliverableObligationGrpAttributes_;

};

struct ProtectionTermGrp_Block_t {
	ProtectionTermGrpElements ProtectionTermGrpElements_;
			ProtectionTermGrpAttributes ProtectionTermGrpAttributes_;

};

struct ProtectionTermEventGrp_Block_t {
	ProtectionTermEventGrpElements ProtectionTermEventGrpElements_;
			ProtectionTermEventGrpAttributes ProtectionTermEventGrpAttributes_;

};

struct ProtectionTermEventQualifierGrp_Block_t {
	ProtectionTermEventQualifierGrpElements ProtectionTermEventQualifierGrpElements_;
			ProtectionTermEventQualifierGrpAttributes ProtectionTermEventQualifierGrpAttributes_;

};

struct ProtectionTermObligationGrp_Block_t {
	ProtectionTermObligationGrpElements ProtectionTermObligationGrpElements_;
			ProtectionTermObligationGrpAttributes ProtectionTermObligationGrpAttributes_;

};

struct ProvisionCashSettlPaymentDates_Block_t {
	ProvisionCashSettlPaymentDatesElements ProvisionCashSettlPaymentDatesElements_;
			ProvisionCashSettlPaymentDatesAttributes ProvisionCashSettlPaymentDatesAttributes_;

};

struct ProvisionCashSettlPaymentFixedDateGrp_Block_t {
	ProvisionCashSettlPaymentFixedDateGrpElements ProvisionCashSettlPaymentFixedDateGrpElements_;
			ProvisionCashSettlPaymentFixedDateGrpAttributes ProvisionCashSettlPaymentFixedDateGrpAttributes_;

};

struct ProvisionCashSettlValueDates_Block_t {
	ProvisionCashSettlValueDatesElements ProvisionCashSettlValueDatesElements_;
			ProvisionCashSettlValueDatesAttributes ProvisionCashSettlValueDatesAttributes_;

};

struct ProvisionOptionExerciseFixedDateGrp_Block_t {
	ProvisionOptionExerciseFixedDateGrpElements ProvisionOptionExerciseFixedDateGrpElements_;
			ProvisionOptionExerciseFixedDateGrpAttributes ProvisionOptionExerciseFixedDateGrpAttributes_;

};

struct ProvisionOptionExerciseDates_Block_t {
	ProvisionOptionExerciseDatesElements ProvisionOptionExerciseDatesElements_;
			ProvisionOptionExerciseDatesAttributes ProvisionOptionExerciseDatesAttributes_;

};

struct ProvisionOptionExpirationDate_Block_t {
	ProvisionOptionExpirationDateElements ProvisionOptionExpirationDateElements_;
			ProvisionOptionExpirationDateAttributes ProvisionOptionExpirationDateAttributes_;

};

struct ProvisionOptionRelevantUnderlyingDate_Block_t {
	ProvisionOptionRelevantUnderlyingDateElements ProvisionOptionRelevantUnderlyingDateElements_;
			ProvisionOptionRelevantUnderlyingDateAttributes ProvisionOptionRelevantUnderlyingDateAttributes_;

};

struct ProvisionGrp_Block_t {
	ProvisionGrpElements ProvisionGrpElements_;
			ProvisionGrpAttributes ProvisionGrpAttributes_;

};

struct ProvisionParties_Block_t {
	ProvisionPartiesElements ProvisionPartiesElements_;
			ProvisionPartiesAttributes ProvisionPartiesAttributes_;

};

struct ProvisionPtysSubGrp_Block_t {
	ProvisionPtysSubGrpElements ProvisionPtysSubGrpElements_;
			ProvisionPtysSubGrpAttributes ProvisionPtysSubGrpAttributes_;

};

struct RegulatoryTradeIDGrp_Block_t {
	RegulatoryTradeIDGrpElements RegulatoryTradeIDGrpElements_;
			RegulatoryTradeIDGrpAttributes RegulatoryTradeIDGrpAttributes_;

};

struct SecondaryAssetGrp_Block_t {
	SecondaryAssetGrpElements SecondaryAssetGrpElements_;
			SecondaryAssetGrpAttributes SecondaryAssetGrpAttributes_;

};

struct SettlRateDisruptionFallbackGrp_Block_t {
	SettlRateDisruptionFallbackGrpElements SettlRateDisruptionFallbackGrpElements_;
			SettlRateDisruptionFallbackGrpAttributes SettlRateDisruptionFallbackGrpAttributes_;

};

struct SideRegulatoryTradeIDGrp_Block_t {
	SideRegulatoryTradeIDGrpElements SideRegulatoryTradeIDGrpElements_;
			SideRegulatoryTradeIDGrpAttributes SideRegulatoryTradeIDGrpAttributes_;

};

struct StreamCalculationPeriodDates_Block_t {
	StreamCalculationPeriodDatesElements StreamCalculationPeriodDatesElements_;
			StreamCalculationPeriodDatesAttributes StreamCalculationPeriodDatesAttributes_;

};

struct StreamEffectiveDate_Block_t {
	StreamEffectiveDateElements StreamEffectiveDateElements_;
			StreamEffectiveDateAttributes StreamEffectiveDateAttributes_;

};

struct StreamGrp_Block_t {
	StreamGrpElements StreamGrpElements_;
			StreamGrpAttributes StreamGrpAttributes_;

};

struct StreamTerminationDate_Block_t {
	StreamTerminationDateElements StreamTerminationDateElements_;
			StreamTerminationDateAttributes StreamTerminationDateAttributes_;

};

struct UnderlyingComplexEvents_Block_t {
	UnderlyingComplexEventsElements UnderlyingComplexEventsElements_;
			UnderlyingComplexEventsAttributes UnderlyingComplexEventsAttributes_;

};

struct UnderlyingComplexEventDates_Block_t {
	UnderlyingComplexEventDatesElements UnderlyingComplexEventDatesElements_;
			UnderlyingComplexEventDatesAttributes UnderlyingComplexEventDatesAttributes_;

};

struct UnderlyingComplexEventTimes_Block_t {
	UnderlyingComplexEventTimesElements UnderlyingComplexEventTimesElements_;
			UnderlyingComplexEventTimesAttributes UnderlyingComplexEventTimesAttributes_;

};

struct UnderlyingEvntGrp_Block_t {
	UnderlyingEvntGrpElements UnderlyingEvntGrpElements_;
			UnderlyingEvntGrpAttributes UnderlyingEvntGrpAttributes_;

};

struct UnderlyingPaymentScheduleGrp_Block_t {
	UnderlyingPaymentScheduleGrpElements UnderlyingPaymentScheduleGrpElements_;
			UnderlyingPaymentScheduleGrpAttributes UnderlyingPaymentScheduleGrpAttributes_;

};

struct UnderlyingPaymentScheduleRateSourceGrp_Block_t {
	UnderlyingPaymentScheduleRateSourceGrpElements UnderlyingPaymentScheduleRateSourceGrpElements_;
			UnderlyingPaymentScheduleRateSourceGrpAttributes UnderlyingPaymentScheduleRateSourceGrpAttributes_;

};

struct UnderlyingPaymentStream_Block_t {
	UnderlyingPaymentStreamElements UnderlyingPaymentStreamElements_;
			UnderlyingPaymentStreamAttributes UnderlyingPaymentStreamAttributes_;

};

struct UnderlyingPaymentStreamFixedRate_Block_t {
	UnderlyingPaymentStreamFixedRateElements UnderlyingPaymentStreamFixedRateElements_;
			UnderlyingPaymentStreamFixedRateAttributes UnderlyingPaymentStreamFixedRateAttributes_;

};

struct UnderlyingPaymentStreamFloatingRate_Block_t {
	UnderlyingPaymentStreamFloatingRateElements UnderlyingPaymentStreamFloatingRateElements_;
			UnderlyingPaymentStreamFloatingRateAttributes UnderlyingPaymentStreamFloatingRateAttributes_;

};

struct UnderlyingPaymentStreamNonDeliverableFixingDateGrp_Block_t {
	UnderlyingPaymentStreamNonDeliverableFixingDateGrpElements UnderlyingPaymentStreamNonDeliverableFixingDateGrpElements_;
			UnderlyingPaymentStreamNonDeliverableFixingDateGrpAttributes UnderlyingPaymentStreamNonDeliverableFixingDateGrpAttributes_;

};

struct UnderlyingPaymentStreamNonDeliverableSettlTerms_Block_t {
	UnderlyingPaymentStreamNonDeliverableSettlTermsElements UnderlyingPaymentStreamNonDeliverableSettlTermsElements_;
			UnderlyingPaymentStreamNonDeliverableSettlTermsAttributes UnderlyingPaymentStreamNonDeliverableSettlTermsAttributes_;

};

struct UnderlyingPaymentStreamPaymentDates_Block_t {
	UnderlyingPaymentStreamPaymentDatesElements UnderlyingPaymentStreamPaymentDatesElements_;
			UnderlyingPaymentStreamPaymentDatesAttributes UnderlyingPaymentStreamPaymentDatesAttributes_;

};

struct UnderlyingPaymentStreamResetDates_Block_t {
	UnderlyingPaymentStreamResetDatesElements UnderlyingPaymentStreamResetDatesElements_;
			UnderlyingPaymentStreamResetDatesAttributes UnderlyingPaymentStreamResetDatesAttributes_;

};

struct UnderlyingPaymentStubGrp_Block_t {
	UnderlyingPaymentStubGrpElements UnderlyingPaymentStubGrpElements_;
			UnderlyingPaymentStubGrpAttributes UnderlyingPaymentStubGrpAttributes_;

};

struct UnderlyingSecondaryAssetGrp_Block_t {
	UnderlyingSecondaryAssetGrpElements UnderlyingSecondaryAssetGrpElements_;
			UnderlyingSecondaryAssetGrpAttributes UnderlyingSecondaryAssetGrpAttributes_;

};

struct UnderlyingSettlRateDisruptionFallbackGrp_Block_t {
	UnderlyingSettlRateDisruptionFallbackGrpElements UnderlyingSettlRateDisruptionFallbackGrpElements_;
			UnderlyingSettlRateDisruptionFallbackGrpAttributes UnderlyingSettlRateDisruptionFallbackGrpAttributes_;

};

struct UnderlyingStreamCalculationPeriodDates_Block_t {
	UnderlyingStreamCalculationPeriodDatesElements UnderlyingStreamCalculationPeriodDatesElements_;
			UnderlyingStreamCalculationPeriodDatesAttributes UnderlyingStreamCalculationPeriodDatesAttributes_;

};

struct UnderlyingStreamEffectiveDate_Block_t {
	UnderlyingStreamEffectiveDateElements UnderlyingStreamEffectiveDateElements_;
			UnderlyingStreamEffectiveDateAttributes UnderlyingStreamEffectiveDateAttributes_;

};

struct UnderlyingStreamGrp_Block_t {
	UnderlyingStreamGrpElements UnderlyingStreamGrpElements_;
			UnderlyingStreamGrpAttributes UnderlyingStreamGrpAttributes_;

};

struct UnderlyingStreamTerminationDate_Block_t {
	UnderlyingStreamTerminationDateElements UnderlyingStreamTerminationDateElements_;
			UnderlyingStreamTerminationDateAttributes UnderlyingStreamTerminationDateAttributes_;

};

struct CashSettlDealerGrp_Block_t {
	CashSettlDealerGrpElements CashSettlDealerGrpElements_;
			CashSettlDealerGrpAttributes CashSettlDealerGrpAttributes_;

};

struct BusinessCenterGrp_Block_t {
	BusinessCenterGrpElements BusinessCenterGrpElements_;
			BusinessCenterGrpAttributes BusinessCenterGrpAttributes_;

};

struct DateAdjustment_Block_t {
	DateAdjustmentElements DateAdjustmentElements_;
			DateAdjustmentAttributes DateAdjustmentAttributes_;

};

struct LegBusinessCenterGrp_Block_t {
	LegBusinessCenterGrpElements LegBusinessCenterGrpElements_;
			LegBusinessCenterGrpAttributes LegBusinessCenterGrpAttributes_;

};

struct LegDateAdjustment_Block_t {
	LegDateAdjustmentElements LegDateAdjustmentElements_;
			LegDateAdjustmentAttributes LegDateAdjustmentAttributes_;

};

struct LegPaymentScheduleFixingDateBusinessCenterGrp_Block_t {
	LegPaymentScheduleFixingDateBusinessCenterGrpElements LegPaymentScheduleFixingDateBusinessCenterGrpElements_;
			LegPaymentScheduleFixingDateBusinessCenterGrpAttributes LegPaymentScheduleFixingDateBusinessCenterGrpAttributes_;

};

struct LegPaymentScheduleInterimExchangeDateBusinessCenterGrp_Block_t {
	LegPaymentScheduleInterimExchangeDateBusinessCenterGrpElements LegPaymentScheduleInterimExchangeDateBusinessCenterGrpElements_;
			LegPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes LegPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrp_Block_t {
	LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements_;
			LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes LegPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamPaymentDateBusinessCenterGrp_Block_t {
	LegPaymentStreamPaymentDateBusinessCenterGrpElements LegPaymentStreamPaymentDateBusinessCenterGrpElements_;
			LegPaymentStreamPaymentDateBusinessCenterGrpAttributes LegPaymentStreamPaymentDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamResetDateBusinessCenterGrp_Block_t {
	LegPaymentStreamResetDateBusinessCenterGrpElements LegPaymentStreamResetDateBusinessCenterGrpElements_;
			LegPaymentStreamResetDateBusinessCenterGrpAttributes LegPaymentStreamResetDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamInitialFixingDateBusinessCenterGrp_Block_t {
	LegPaymentStreamInitialFixingDateBusinessCenterGrpElements LegPaymentStreamInitialFixingDateBusinessCenterGrpElements_;
			LegPaymentStreamInitialFixingDateBusinessCenterGrpAttributes LegPaymentStreamInitialFixingDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamFixingDateBusinessCenterGrp_Block_t {
	LegPaymentStreamFixingDateBusinessCenterGrpElements LegPaymentStreamFixingDateBusinessCenterGrpElements_;
			LegPaymentStreamFixingDateBusinessCenterGrpAttributes LegPaymentStreamFixingDateBusinessCenterGrpAttributes_;

};

struct LegProvisionCashSettlPaymentDateBusinessCenterGrp_Block_t {
	LegProvisionCashSettlPaymentDateBusinessCenterGrpElements LegProvisionCashSettlPaymentDateBusinessCenterGrpElements_;
			LegProvisionCashSettlPaymentDateBusinessCenterGrpAttributes LegProvisionCashSettlPaymentDateBusinessCenterGrpAttributes_;

};

struct LegProvisionCashSettlValueDateBusinessCenterGrp_Block_t {
	LegProvisionCashSettlValueDateBusinessCenterGrpElements LegProvisionCashSettlValueDateBusinessCenterGrpElements_;
			LegProvisionCashSettlValueDateBusinessCenterGrpAttributes LegProvisionCashSettlValueDateBusinessCenterGrpAttributes_;

};

struct LegProvisionOptionExerciseBusinessCenterGrp_Block_t {
	LegProvisionOptionExerciseBusinessCenterGrpElements LegProvisionOptionExerciseBusinessCenterGrpElements_;
			LegProvisionOptionExerciseBusinessCenterGrpAttributes LegProvisionOptionExerciseBusinessCenterGrpAttributes_;

};

struct LegProvisionOptionExpirationDateBusinessCenterGrp_Block_t {
	LegProvisionOptionExpirationDateBusinessCenterGrpElements LegProvisionOptionExpirationDateBusinessCenterGrpElements_;
			LegProvisionOptionExpirationDateBusinessCenterGrpAttributes LegProvisionOptionExpirationDateBusinessCenterGrpAttributes_;

};

struct LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrp_Block_t {
	LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements_;
			LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes LegProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes_;

};

struct LegProvisionDateBusinessCenterGrp_Block_t {
	LegProvisionDateBusinessCenterGrpElements LegProvisionDateBusinessCenterGrpElements_;
			LegProvisionDateBusinessCenterGrpAttributes LegProvisionDateBusinessCenterGrpAttributes_;

};

struct LegStreamCalculationPeriodBusinessCenterGrp_Block_t {
	LegStreamCalculationPeriodBusinessCenterGrpElements LegStreamCalculationPeriodBusinessCenterGrpElements_;
			LegStreamCalculationPeriodBusinessCenterGrpAttributes LegStreamCalculationPeriodBusinessCenterGrpAttributes_;

};

struct LegStreamFirstPeriodStartDateBusinessCenterGrp_Block_t {
	LegStreamFirstPeriodStartDateBusinessCenterGrpElements LegStreamFirstPeriodStartDateBusinessCenterGrpElements_;
			LegStreamFirstPeriodStartDateBusinessCenterGrpAttributes LegStreamFirstPeriodStartDateBusinessCenterGrpAttributes_;

};

struct LegStreamEffectiveDateBusinessCenterGrp_Block_t {
	LegStreamEffectiveDateBusinessCenterGrpElements LegStreamEffectiveDateBusinessCenterGrpElements_;
			LegStreamEffectiveDateBusinessCenterGrpAttributes LegStreamEffectiveDateBusinessCenterGrpAttributes_;

};

struct LegStreamTerminationDateBusinessCenterGrp_Block_t {
	LegStreamTerminationDateBusinessCenterGrpElements LegStreamTerminationDateBusinessCenterGrpElements_;
			LegStreamTerminationDateBusinessCenterGrpAttributes LegStreamTerminationDateBusinessCenterGrpAttributes_;

};

struct PaymentBusinessCenterGrp_Block_t {
	PaymentBusinessCenterGrpElements PaymentBusinessCenterGrpElements_;
			PaymentBusinessCenterGrpAttributes PaymentBusinessCenterGrpAttributes_;

};

struct PaymentScheduleFixingDateBusinessCenterGrp_Block_t {
	PaymentScheduleFixingDateBusinessCenterGrpElements PaymentScheduleFixingDateBusinessCenterGrpElements_;
			PaymentScheduleFixingDateBusinessCenterGrpAttributes PaymentScheduleFixingDateBusinessCenterGrpAttributes_;

};

struct PaymentScheduleInterimExchangeDateBusinessCenterGrp_Block_t {
	PaymentScheduleInterimExchangeDateBusinessCenterGrpElements PaymentScheduleInterimExchangeDateBusinessCenterGrpElements_;
			PaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes PaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes_;

};

struct PaymentStreamNonDeliverableFixingDatesBusinessCenterGrp_Block_t {
	PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements_;
			PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes PaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes_;

};

struct PaymentStreamPaymentDateBusinessCenterGrp_Block_t {
	PaymentStreamPaymentDateBusinessCenterGrpElements PaymentStreamPaymentDateBusinessCenterGrpElements_;
			PaymentStreamPaymentDateBusinessCenterGrpAttributes PaymentStreamPaymentDateBusinessCenterGrpAttributes_;

};

struct PaymentStreamResetDateBusinessCenterGrp_Block_t {
	PaymentStreamResetDateBusinessCenterGrpElements PaymentStreamResetDateBusinessCenterGrpElements_;
			PaymentStreamResetDateBusinessCenterGrpAttributes PaymentStreamResetDateBusinessCenterGrpAttributes_;

};

struct PaymentStreamInitialFixingDateBusinessCenterGrp_Block_t {
	PaymentStreamInitialFixingDateBusinessCenterGrpElements PaymentStreamInitialFixingDateBusinessCenterGrpElements_;
			PaymentStreamInitialFixingDateBusinessCenterGrpAttributes PaymentStreamInitialFixingDateBusinessCenterGrpAttributes_;

};

struct PaymentStreamFixingDateBusinessCenterGrp_Block_t {
	PaymentStreamFixingDateBusinessCenterGrpElements PaymentStreamFixingDateBusinessCenterGrpElements_;
			PaymentStreamFixingDateBusinessCenterGrpAttributes PaymentStreamFixingDateBusinessCenterGrpAttributes_;

};

struct ProtectionTermEventNewsSourceGrp_Block_t {
	ProtectionTermEventNewsSourceGrpElements ProtectionTermEventNewsSourceGrpElements_;
			ProtectionTermEventNewsSourceGrpAttributes ProtectionTermEventNewsSourceGrpAttributes_;

};

struct ProvisionCashSettlPaymentDateBusinessCenterGrp_Block_t {
	ProvisionCashSettlPaymentDateBusinessCenterGrpElements ProvisionCashSettlPaymentDateBusinessCenterGrpElements_;
			ProvisionCashSettlPaymentDateBusinessCenterGrpAttributes ProvisionCashSettlPaymentDateBusinessCenterGrpAttributes_;

};

struct ProvisionCashSettlValueDateBusinessCenterGrp_Block_t {
	ProvisionCashSettlValueDateBusinessCenterGrpElements ProvisionCashSettlValueDateBusinessCenterGrpElements_;
			ProvisionCashSettlValueDateBusinessCenterGrpAttributes ProvisionCashSettlValueDateBusinessCenterGrpAttributes_;

};

struct ProvisionOptionExerciseBusinessCenterGrp_Block_t {
	ProvisionOptionExerciseBusinessCenterGrpElements ProvisionOptionExerciseBusinessCenterGrpElements_;
			ProvisionOptionExerciseBusinessCenterGrpAttributes ProvisionOptionExerciseBusinessCenterGrpAttributes_;

};

struct ProvisionOptionExpirationDateBusinessCenterGrp_Block_t {
	ProvisionOptionExpirationDateBusinessCenterGrpElements ProvisionOptionExpirationDateBusinessCenterGrpElements_;
			ProvisionOptionExpirationDateBusinessCenterGrpAttributes ProvisionOptionExpirationDateBusinessCenterGrpAttributes_;

};

struct ProvisionOptionRelevantUnderlyingDateBusinessCenterGrp_Block_t {
	ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements_;
			ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes ProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes_;

};

struct ProvisionDateBusinessCenterGrp_Block_t {
	ProvisionDateBusinessCenterGrpElements ProvisionDateBusinessCenterGrpElements_;
			ProvisionDateBusinessCenterGrpAttributes ProvisionDateBusinessCenterGrpAttributes_;

};

struct StreamCalculationPeriodBusinessCenterGrp_Block_t {
	StreamCalculationPeriodBusinessCenterGrpElements StreamCalculationPeriodBusinessCenterGrpElements_;
			StreamCalculationPeriodBusinessCenterGrpAttributes StreamCalculationPeriodBusinessCenterGrpAttributes_;

};

struct StreamFirstPeriodStartDateBusinessCenterGrp_Block_t {
	StreamFirstPeriodStartDateBusinessCenterGrpElements StreamFirstPeriodStartDateBusinessCenterGrpElements_;
			StreamFirstPeriodStartDateBusinessCenterGrpAttributes StreamFirstPeriodStartDateBusinessCenterGrpAttributes_;

};

struct StreamEffectiveBusinessCenterGrp_Block_t {
	StreamEffectiveBusinessCenterGrpElements StreamEffectiveBusinessCenterGrpElements_;
			StreamEffectiveBusinessCenterGrpAttributes StreamEffectiveBusinessCenterGrpAttributes_;

};

struct StreamTerminationDateBusinessCenterGrp_Block_t {
	StreamTerminationDateBusinessCenterGrpElements StreamTerminationDateBusinessCenterGrpElements_;
			StreamTerminationDateBusinessCenterGrpAttributes StreamTerminationDateBusinessCenterGrpAttributes_;

};

struct UnderlyingBusinessCenterGrp_Block_t {
	UnderlyingBusinessCenterGrpElements UnderlyingBusinessCenterGrpElements_;
			UnderlyingBusinessCenterGrpAttributes UnderlyingBusinessCenterGrpAttributes_;

};

struct UnderlyingDateAdjustment_Block_t {
	UnderlyingDateAdjustmentElements UnderlyingDateAdjustmentElements_;
			UnderlyingDateAdjustmentAttributes UnderlyingDateAdjustmentAttributes_;

};

struct UnderlyingPaymentScheduleFixingDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentScheduleFixingDateBusinessCenterGrpElements UnderlyingPaymentScheduleFixingDateBusinessCenterGrpElements_;
			UnderlyingPaymentScheduleFixingDateBusinessCenterGrpAttributes UnderlyingPaymentScheduleFixingDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpElements UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpElements_;
			UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes UnderlyingPaymentScheduleInterimExchangeDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpElements_;
			UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamPaymentDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamPaymentDateBusinessCenterGrpElements UnderlyingPaymentStreamPaymentDateBusinessCenterGrpElements_;
			UnderlyingPaymentStreamPaymentDateBusinessCenterGrpAttributes UnderlyingPaymentStreamPaymentDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamResetDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamResetDateBusinessCenterGrpElements UnderlyingPaymentStreamResetDateBusinessCenterGrpElements_;
			UnderlyingPaymentStreamResetDateBusinessCenterGrpAttributes UnderlyingPaymentStreamResetDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpElements UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpElements_;
			UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpAttributes UnderlyingPaymentStreamInitialFixingDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamFixingDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamFixingDateBusinessCenterGrpElements UnderlyingPaymentStreamFixingDateBusinessCenterGrpElements_;
			UnderlyingPaymentStreamFixingDateBusinessCenterGrpAttributes UnderlyingPaymentStreamFixingDateBusinessCenterGrpAttributes_;

};

struct UnderlyingStreamCalculationPeriodBusinessCenterGrp_Block_t {
	UnderlyingStreamCalculationPeriodBusinessCenterGrpElements UnderlyingStreamCalculationPeriodBusinessCenterGrpElements_;
			UnderlyingStreamCalculationPeriodBusinessCenterGrpAttributes UnderlyingStreamCalculationPeriodBusinessCenterGrpAttributes_;

};

struct UnderlyingStreamFirstPeriodStartDateBusinessCenterGrp_Block_t {
	UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpElements UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpElements_;
			UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpAttributes UnderlyingStreamFirstPeriodStartDateBusinessCenterGrpAttributes_;

};

struct UnderlyingStreamEffectiveDateBusinessCenterGrp_Block_t {
	UnderlyingStreamEffectiveDateBusinessCenterGrpElements UnderlyingStreamEffectiveDateBusinessCenterGrpElements_;
			UnderlyingStreamEffectiveDateBusinessCenterGrpAttributes UnderlyingStreamEffectiveDateBusinessCenterGrpAttributes_;

};

struct UnderlyingStreamTerminationDateBusinessCenterGrp_Block_t {
	UnderlyingStreamTerminationDateBusinessCenterGrpElements UnderlyingStreamTerminationDateBusinessCenterGrpElements_;
			UnderlyingStreamTerminationDateBusinessCenterGrpAttributes UnderlyingStreamTerminationDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamNonDeliverableSettlRateSource_Block_t {
	LegPaymentStreamNonDeliverableSettlRateSourceElements LegPaymentStreamNonDeliverableSettlRateSourceElements_;
			LegPaymentStreamNonDeliverableSettlRateSourceAttributes LegPaymentStreamNonDeliverableSettlRateSourceAttributes_;

};

struct LegSettlRateFallbackRateSource_Block_t {
	LegSettlRateFallbackRateSourceElements LegSettlRateFallbackRateSourceElements_;
			LegSettlRateFallbackRateSourceAttributes LegSettlRateFallbackRateSourceAttributes_;

};

struct PaymentStreamNonDeliverableSettlRateSource_Block_t {
	PaymentStreamNonDeliverableSettlRateSourceElements PaymentStreamNonDeliverableSettlRateSourceElements_;
			PaymentStreamNonDeliverableSettlRateSourceAttributes PaymentStreamNonDeliverableSettlRateSourceAttributes_;

};

struct SettlRateFallbackRateSource_Block_t {
	SettlRateFallbackRateSourceElements SettlRateFallbackRateSourceElements_;
			SettlRateFallbackRateSourceAttributes SettlRateFallbackRateSourceAttributes_;

};

struct UnderlyingPaymentStreamNonDeliverableSettlRateSource_Block_t {
	UnderlyingPaymentStreamNonDeliverableSettlRateSourceElements UnderlyingPaymentStreamNonDeliverableSettlRateSourceElements_;
			UnderlyingPaymentStreamNonDeliverableSettlRateSourceAttributes UnderlyingPaymentStreamNonDeliverableSettlRateSourceAttributes_;

};

struct UnderlyingSettlRateFallbackRateSource_Block_t {
	UnderlyingSettlRateFallbackRateSourceElements UnderlyingSettlRateFallbackRateSourceElements_;
			UnderlyingSettlRateFallbackRateSourceAttributes UnderlyingSettlRateFallbackRateSourceAttributes_;

};

struct ProvisionCashSettlQuoteSource_Block_t {
	ProvisionCashSettlQuoteSourceElements ProvisionCashSettlQuoteSourceElements_;
			ProvisionCashSettlQuoteSourceAttributes ProvisionCashSettlQuoteSourceAttributes_;

};

struct LegProvisionCashSettlQuoteSource_Block_t {
	LegProvisionCashSettlQuoteSourceElements LegProvisionCashSettlQuoteSourceElements_;
			LegProvisionCashSettlQuoteSourceAttributes LegProvisionCashSettlQuoteSourceAttributes_;

};

struct AttachmentGrp_Block_t {
	AttachmentGrpElements AttachmentGrpElements_;
			AttachmentGrpAttributes AttachmentGrpAttributes_;

};

struct AttachmentKeywordGrp_Block_t {
	AttachmentKeywordGrpElements AttachmentKeywordGrpElements_;
			AttachmentKeywordGrpAttributes AttachmentKeywordGrpAttributes_;

};

struct AssetAttributeGrp_Block_t {
	AssetAttributeGrpElements AssetAttributeGrpElements_;
			AssetAttributeGrpAttributes AssetAttributeGrpAttributes_;

};

struct ComplexEventAveragingObservationGrp_Block_t {
	ComplexEventAveragingObservationGrpElements ComplexEventAveragingObservationGrpElements_;
			ComplexEventAveragingObservationGrpAttributes ComplexEventAveragingObservationGrpAttributes_;

};

struct ComplexEventCreditEventGrp_Block_t {
	ComplexEventCreditEventGrpElements ComplexEventCreditEventGrpElements_;
			ComplexEventCreditEventGrpAttributes ComplexEventCreditEventGrpAttributes_;

};

struct ComplexEventCreditEventQualifierGrp_Block_t {
	ComplexEventCreditEventQualifierGrpElements ComplexEventCreditEventQualifierGrpElements_;
			ComplexEventCreditEventQualifierGrpAttributes ComplexEventCreditEventQualifierGrpAttributes_;

};

struct ComplexEventPeriodDateGrp_Block_t {
	ComplexEventPeriodDateGrpElements ComplexEventPeriodDateGrpElements_;
			ComplexEventPeriodDateGrpAttributes ComplexEventPeriodDateGrpAttributes_;

};

struct ComplexEventPeriodGrp_Block_t {
	ComplexEventPeriodGrpElements ComplexEventPeriodGrpElements_;
			ComplexEventPeriodGrpAttributes ComplexEventPeriodGrpAttributes_;

};

struct ComplexEventRateSourceGrp_Block_t {
	ComplexEventRateSourceGrpElements ComplexEventRateSourceGrpElements_;
			ComplexEventRateSourceGrpAttributes ComplexEventRateSourceGrpAttributes_;

};

struct ComplexEventDateBusinessCenterGrp_Block_t {
	ComplexEventDateBusinessCenterGrpElements ComplexEventDateBusinessCenterGrpElements_;
			ComplexEventDateBusinessCenterGrpAttributes ComplexEventDateBusinessCenterGrpAttributes_;

};

struct ComplexEventRelativeDate_Block_t {
	ComplexEventRelativeDateElements ComplexEventRelativeDateElements_;
			ComplexEventRelativeDateAttributes ComplexEventRelativeDateAttributes_;

};

struct ComplexEventCreditEventSourceGrp_Block_t {
	ComplexEventCreditEventSourceGrpElements ComplexEventCreditEventSourceGrpElements_;
			ComplexEventCreditEventSourceGrpAttributes ComplexEventCreditEventSourceGrpAttributes_;

};

struct ComplexEventScheduleGrp_Block_t {
	ComplexEventScheduleGrpElements ComplexEventScheduleGrpElements_;
			ComplexEventScheduleGrpAttributes ComplexEventScheduleGrpAttributes_;

};

struct DeliveryScheduleGrp_Block_t {
	DeliveryScheduleGrpElements DeliveryScheduleGrpElements_;
			DeliveryScheduleGrpAttributes DeliveryScheduleGrpAttributes_;

};

struct DeliveryScheduleSettlDayGrp_Block_t {
	DeliveryScheduleSettlDayGrpElements DeliveryScheduleSettlDayGrpElements_;
			DeliveryScheduleSettlDayGrpAttributes DeliveryScheduleSettlDayGrpAttributes_;

};

struct DeliveryScheduleSettlTimeGrp_Block_t {
	DeliveryScheduleSettlTimeGrpElements DeliveryScheduleSettlTimeGrpElements_;
			DeliveryScheduleSettlTimeGrpAttributes DeliveryScheduleSettlTimeGrpAttributes_;

};

struct DeliveryStream_Block_t {
	DeliveryStreamElements DeliveryStreamElements_;
			DeliveryStreamAttributes DeliveryStreamAttributes_;

};

struct DeliveryStreamCycleGrp_Block_t {
	DeliveryStreamCycleGrpElements DeliveryStreamCycleGrpElements_;
			DeliveryStreamCycleGrpAttributes DeliveryStreamCycleGrpAttributes_;

};

struct DeliveryStreamCommoditySourceGrp_Block_t {
	DeliveryStreamCommoditySourceGrpElements DeliveryStreamCommoditySourceGrpElements_;
			DeliveryStreamCommoditySourceGrpAttributes DeliveryStreamCommoditySourceGrpAttributes_;

};

struct MarketDisruption_Block_t {
	MarketDisruptionElements MarketDisruptionElements_;
			MarketDisruptionAttributes MarketDisruptionAttributes_;

};

struct MarketDisruptionEventGrp_Block_t {
	MarketDisruptionEventGrpElements MarketDisruptionEventGrpElements_;
			MarketDisruptionEventGrpAttributes MarketDisruptionEventGrpAttributes_;

};

struct MarketDisruptionFallbackGrp_Block_t {
	MarketDisruptionFallbackGrpElements MarketDisruptionFallbackGrpElements_;
			MarketDisruptionFallbackGrpAttributes MarketDisruptionFallbackGrpAttributes_;

};

struct MarketDisruptionFallbackReferencePriceGrp_Block_t {
	MarketDisruptionFallbackReferencePriceGrpElements MarketDisruptionFallbackReferencePriceGrpElements_;
			MarketDisruptionFallbackReferencePriceGrpAttributes MarketDisruptionFallbackReferencePriceGrpAttributes_;

};

struct OptionExercise_Block_t {
	OptionExerciseElements OptionExerciseElements_;
			OptionExerciseAttributes OptionExerciseAttributes_;

};

struct OptionExerciseBusinessCenterGrp_Block_t {
	OptionExerciseBusinessCenterGrpElements OptionExerciseBusinessCenterGrpElements_;
			OptionExerciseBusinessCenterGrpAttributes OptionExerciseBusinessCenterGrpAttributes_;

};

struct OptionExerciseDates_Block_t {
	OptionExerciseDatesElements OptionExerciseDatesElements_;
			OptionExerciseDatesAttributes OptionExerciseDatesAttributes_;

};

struct OptionExerciseDateGrp_Block_t {
	OptionExerciseDateGrpElements OptionExerciseDateGrpElements_;
			OptionExerciseDateGrpAttributes OptionExerciseDateGrpAttributes_;

};

struct OptionExerciseExpirationDateBusinessCenterGrp_Block_t {
	OptionExerciseExpirationDateBusinessCenterGrpElements OptionExerciseExpirationDateBusinessCenterGrpElements_;
			OptionExerciseExpirationDateBusinessCenterGrpAttributes OptionExerciseExpirationDateBusinessCenterGrpAttributes_;

};

struct OptionExerciseExpiration_Block_t {
	OptionExerciseExpirationElements OptionExerciseExpirationElements_;
			OptionExerciseExpirationAttributes OptionExerciseExpirationAttributes_;

};

struct OptionExerciseExpirationDateGrp_Block_t {
	OptionExerciseExpirationDateGrpElements OptionExerciseExpirationDateGrpElements_;
			OptionExerciseExpirationDateGrpAttributes OptionExerciseExpirationDateGrpAttributes_;

};

struct PaymentScheduleFixingDayGrp_Block_t {
	PaymentScheduleFixingDayGrpElements PaymentScheduleFixingDayGrpElements_;
			PaymentScheduleFixingDayGrpAttributes PaymentScheduleFixingDayGrpAttributes_;

};

struct PaymentStreamPricingBusinessCenterGrp_Block_t {
	PaymentStreamPricingBusinessCenterGrpElements PaymentStreamPricingBusinessCenterGrpElements_;
			PaymentStreamPricingBusinessCenterGrpAttributes PaymentStreamPricingBusinessCenterGrpAttributes_;

};

struct PaymentStreamPaymentDateGrp_Block_t {
	PaymentStreamPaymentDateGrpElements PaymentStreamPaymentDateGrpElements_;
			PaymentStreamPaymentDateGrpAttributes PaymentStreamPaymentDateGrpAttributes_;

};

struct PaymentStreamPricingDateGrp_Block_t {
	PaymentStreamPricingDateGrpElements PaymentStreamPricingDateGrpElements_;
			PaymentStreamPricingDateGrpAttributes PaymentStreamPricingDateGrpAttributes_;

};

struct PaymentStreamPricingDayGrp_Block_t {
	PaymentStreamPricingDayGrpElements PaymentStreamPricingDayGrpElements_;
			PaymentStreamPricingDayGrpAttributes PaymentStreamPricingDayGrpAttributes_;

};

struct PricingDateBusinessCenterGrp_Block_t {
	PricingDateBusinessCenterGrpElements PricingDateBusinessCenterGrpElements_;
			PricingDateBusinessCenterGrpAttributes PricingDateBusinessCenterGrpAttributes_;

};

struct PricingDateTime_Block_t {
	PricingDateTimeElements PricingDateTimeElements_;
			PricingDateTimeAttributes PricingDateTimeAttributes_;

};

struct StreamAssetAttributeGrp_Block_t {
	StreamAssetAttributeGrpElements StreamAssetAttributeGrpElements_;
			StreamAssetAttributeGrpAttributes StreamAssetAttributeGrpAttributes_;

};

struct StreamCalculationPeriodDateGrp_Block_t {
	StreamCalculationPeriodDateGrpElements StreamCalculationPeriodDateGrpElements_;
			StreamCalculationPeriodDateGrpAttributes StreamCalculationPeriodDateGrpAttributes_;

};

struct StreamCommoditySettlBusinessCenterGrp_Block_t {
	StreamCommoditySettlBusinessCenterGrpElements StreamCommoditySettlBusinessCenterGrpElements_;
			StreamCommoditySettlBusinessCenterGrpAttributes StreamCommoditySettlBusinessCenterGrpAttributes_;

};

struct StreamCommodity_Block_t {
	StreamCommodityElements StreamCommodityElements_;
			StreamCommodityAttributes StreamCommodityAttributes_;

};

struct StreamCommodityAltIDGrp_Block_t {
	StreamCommodityAltIDGrpElements StreamCommodityAltIDGrpElements_;
			StreamCommodityAltIDGrpAttributes StreamCommodityAltIDGrpAttributes_;

};

struct StreamCommodityDataSourceGrp_Block_t {
	StreamCommodityDataSourceGrpElements StreamCommodityDataSourceGrpElements_;
			StreamCommodityDataSourceGrpAttributes StreamCommodityDataSourceGrpAttributes_;

};

struct StreamCommoditySettlDayGrp_Block_t {
	StreamCommoditySettlDayGrpElements StreamCommoditySettlDayGrpElements_;
			StreamCommoditySettlDayGrpAttributes StreamCommoditySettlDayGrpAttributes_;

};

struct StreamCommoditySettlTimeGrp_Block_t {
	StreamCommoditySettlTimeGrpElements StreamCommoditySettlTimeGrpElements_;
			StreamCommoditySettlTimeGrpAttributes StreamCommoditySettlTimeGrpAttributes_;

};

struct StreamCommoditySettlPeriodGrp_Block_t {
	StreamCommoditySettlPeriodGrpElements StreamCommoditySettlPeriodGrpElements_;
			StreamCommoditySettlPeriodGrpAttributes StreamCommoditySettlPeriodGrpAttributes_;

};

struct MandatoryClearingJurisdictionGrp_Block_t {
	MandatoryClearingJurisdictionGrpElements MandatoryClearingJurisdictionGrpElements_;
			MandatoryClearingJurisdictionGrpAttributes MandatoryClearingJurisdictionGrpAttributes_;

};

struct LegAdditionalTermBondRefGrp_Block_t {
	LegAdditionalTermBondRefGrpElements LegAdditionalTermBondRefGrpElements_;
			LegAdditionalTermBondRefGrpAttributes LegAdditionalTermBondRefGrpAttributes_;

};

struct LegAdditionalTermGrp_Block_t {
	LegAdditionalTermGrpElements LegAdditionalTermGrpElements_;
			LegAdditionalTermGrpAttributes LegAdditionalTermGrpAttributes_;

};

struct LegAssetAttributeGrp_Block_t {
	LegAssetAttributeGrpElements LegAssetAttributeGrpElements_;
			LegAssetAttributeGrpAttributes LegAssetAttributeGrpAttributes_;

};

struct LegCashSettlDealerGrp_Block_t {
	LegCashSettlDealerGrpElements LegCashSettlDealerGrpElements_;
			LegCashSettlDealerGrpAttributes LegCashSettlDealerGrpAttributes_;

};

struct LegCashSettlTermGrp_Block_t {
	LegCashSettlTermGrpElements LegCashSettlTermGrpElements_;
			LegCashSettlTermGrpAttributes LegCashSettlTermGrpAttributes_;

};

struct LegComplexEventAveragingObservationGrp_Block_t {
	LegComplexEventAveragingObservationGrpElements LegComplexEventAveragingObservationGrpElements_;
			LegComplexEventAveragingObservationGrpAttributes LegComplexEventAveragingObservationGrpAttributes_;

};

struct LegComplexEventCreditEventGrp_Block_t {
	LegComplexEventCreditEventGrpElements LegComplexEventCreditEventGrpElements_;
			LegComplexEventCreditEventGrpAttributes LegComplexEventCreditEventGrpAttributes_;

};

struct LegComplexEventCreditEventQualifierGrp_Block_t {
	LegComplexEventCreditEventQualifierGrpElements LegComplexEventCreditEventQualifierGrpElements_;
			LegComplexEventCreditEventQualifierGrpAttributes LegComplexEventCreditEventQualifierGrpAttributes_;

};

struct LegComplexEventPeriodDateGrp_Block_t {
	LegComplexEventPeriodDateGrpElements LegComplexEventPeriodDateGrpElements_;
			LegComplexEventPeriodDateGrpAttributes LegComplexEventPeriodDateGrpAttributes_;

};

struct LegComplexEventPeriodGrp_Block_t {
	LegComplexEventPeriodGrpElements LegComplexEventPeriodGrpElements_;
			LegComplexEventPeriodGrpAttributes LegComplexEventPeriodGrpAttributes_;

};

struct LegComplexEventRateSourceGrp_Block_t {
	LegComplexEventRateSourceGrpElements LegComplexEventRateSourceGrpElements_;
			LegComplexEventRateSourceGrpAttributes LegComplexEventRateSourceGrpAttributes_;

};

struct LegComplexEventDateBusinessCenterGrp_Block_t {
	LegComplexEventDateBusinessCenterGrpElements LegComplexEventDateBusinessCenterGrpElements_;
			LegComplexEventDateBusinessCenterGrpAttributes LegComplexEventDateBusinessCenterGrpAttributes_;

};

struct LegComplexEventRelativeDate_Block_t {
	LegComplexEventRelativeDateElements LegComplexEventRelativeDateElements_;
			LegComplexEventRelativeDateAttributes LegComplexEventRelativeDateAttributes_;

};

struct LegComplexEventCreditEventSourceGrp_Block_t {
	LegComplexEventCreditEventSourceGrpElements LegComplexEventCreditEventSourceGrpElements_;
			LegComplexEventCreditEventSourceGrpAttributes LegComplexEventCreditEventSourceGrpAttributes_;

};

struct LegComplexEvents_Block_t {
	LegComplexEventsElements LegComplexEventsElements_;
			LegComplexEventsAttributes LegComplexEventsAttributes_;

};

struct LegComplexEventDates_Block_t {
	LegComplexEventDatesElements LegComplexEventDatesElements_;
			LegComplexEventDatesAttributes LegComplexEventDatesAttributes_;

};

struct LegComplexEventTimes_Block_t {
	LegComplexEventTimesElements LegComplexEventTimesElements_;
			LegComplexEventTimesAttributes LegComplexEventTimesAttributes_;

};

struct LegComplexEventScheduleGrp_Block_t {
	LegComplexEventScheduleGrpElements LegComplexEventScheduleGrpElements_;
			LegComplexEventScheduleGrpAttributes LegComplexEventScheduleGrpAttributes_;

};

struct LegDeliveryScheduleGrp_Block_t {
	LegDeliveryScheduleGrpElements LegDeliveryScheduleGrpElements_;
			LegDeliveryScheduleGrpAttributes LegDeliveryScheduleGrpAttributes_;

};

struct LegDeliveryScheduleSettlDayGrp_Block_t {
	LegDeliveryScheduleSettlDayGrpElements LegDeliveryScheduleSettlDayGrpElements_;
			LegDeliveryScheduleSettlDayGrpAttributes LegDeliveryScheduleSettlDayGrpAttributes_;

};

struct LegDeliveryScheduleSettlTimeGrp_Block_t {
	LegDeliveryScheduleSettlTimeGrpElements LegDeliveryScheduleSettlTimeGrpElements_;
			LegDeliveryScheduleSettlTimeGrpAttributes LegDeliveryScheduleSettlTimeGrpAttributes_;

};

struct LegDeliveryStream_Block_t {
	LegDeliveryStreamElements LegDeliveryStreamElements_;
			LegDeliveryStreamAttributes LegDeliveryStreamAttributes_;

};

struct LegStreamAssetAttributeGrp_Block_t {
	LegStreamAssetAttributeGrpElements LegStreamAssetAttributeGrpElements_;
			LegStreamAssetAttributeGrpAttributes LegStreamAssetAttributeGrpAttributes_;

};

struct LegDeliveryStreamCycleGrp_Block_t {
	LegDeliveryStreamCycleGrpElements LegDeliveryStreamCycleGrpElements_;
			LegDeliveryStreamCycleGrpAttributes LegDeliveryStreamCycleGrpAttributes_;

};

struct LegDeliveryStreamCommoditySourceGrp_Block_t {
	LegDeliveryStreamCommoditySourceGrpElements LegDeliveryStreamCommoditySourceGrpElements_;
			LegDeliveryStreamCommoditySourceGrpAttributes LegDeliveryStreamCommoditySourceGrpAttributes_;

};

struct LegInstrumentParties_Block_t {
	LegInstrumentPartiesElements LegInstrumentPartiesElements_;
			LegInstrumentPartiesAttributes LegInstrumentPartiesAttributes_;

};

struct LegInstrumentPtysSubGrp_Block_t {
	LegInstrumentPtysSubGrpElements LegInstrumentPtysSubGrpElements_;
			LegInstrumentPtysSubGrpAttributes LegInstrumentPtysSubGrpAttributes_;

};

struct LegMarketDisruption_Block_t {
	LegMarketDisruptionElements LegMarketDisruptionElements_;
			LegMarketDisruptionAttributes LegMarketDisruptionAttributes_;

};

struct LegMarketDisruptionEventGrp_Block_t {
	LegMarketDisruptionEventGrpElements LegMarketDisruptionEventGrpElements_;
			LegMarketDisruptionEventGrpAttributes LegMarketDisruptionEventGrpAttributes_;

};

struct LegMarketDisruptionFallbackGrp_Block_t {
	LegMarketDisruptionFallbackGrpElements LegMarketDisruptionFallbackGrpElements_;
			LegMarketDisruptionFallbackGrpAttributes LegMarketDisruptionFallbackGrpAttributes_;

};

struct LegMarketDisruptionFallbackReferencePriceGrp_Block_t {
	LegMarketDisruptionFallbackReferencePriceGrpElements LegMarketDisruptionFallbackReferencePriceGrpElements_;
			LegMarketDisruptionFallbackReferencePriceGrpAttributes LegMarketDisruptionFallbackReferencePriceGrpAttributes_;

};

struct LegOptionExercise_Block_t {
	LegOptionExerciseElements LegOptionExerciseElements_;
			LegOptionExerciseAttributes LegOptionExerciseAttributes_;

};

struct LegOptionExerciseBusinessCenterGrp_Block_t {
	LegOptionExerciseBusinessCenterGrpElements LegOptionExerciseBusinessCenterGrpElements_;
			LegOptionExerciseBusinessCenterGrpAttributes LegOptionExerciseBusinessCenterGrpAttributes_;

};

struct LegOptionExerciseDates_Block_t {
	LegOptionExerciseDatesElements LegOptionExerciseDatesElements_;
			LegOptionExerciseDatesAttributes LegOptionExerciseDatesAttributes_;

};

struct LegOptionExerciseDateGrp_Block_t {
	LegOptionExerciseDateGrpElements LegOptionExerciseDateGrpElements_;
			LegOptionExerciseDateGrpAttributes LegOptionExerciseDateGrpAttributes_;

};

struct LegOptionExerciseExpirationDateBusinessCenterGrp_Block_t {
	LegOptionExerciseExpirationDateBusinessCenterGrpElements LegOptionExerciseExpirationDateBusinessCenterGrpElements_;
			LegOptionExerciseExpirationDateBusinessCenterGrpAttributes LegOptionExerciseExpirationDateBusinessCenterGrpAttributes_;

};

struct LegOptionExerciseExpiration_Block_t {
	LegOptionExerciseExpirationElements LegOptionExerciseExpirationElements_;
			LegOptionExerciseExpirationAttributes LegOptionExerciseExpirationAttributes_;

};

struct LegOptionExerciseExpirationDateGrp_Block_t {
	LegOptionExerciseExpirationDateGrpElements LegOptionExerciseExpirationDateGrpElements_;
			LegOptionExerciseExpirationDateGrpAttributes LegOptionExerciseExpirationDateGrpAttributes_;

};

struct LegPaymentScheduleFixingDayGrp_Block_t {
	LegPaymentScheduleFixingDayGrpElements LegPaymentScheduleFixingDayGrpElements_;
			LegPaymentScheduleFixingDayGrpAttributes LegPaymentScheduleFixingDayGrpAttributes_;

};

struct LegPaymentStreamPricingBusinessCenterGrp_Block_t {
	LegPaymentStreamPricingBusinessCenterGrpElements LegPaymentStreamPricingBusinessCenterGrpElements_;
			LegPaymentStreamPricingBusinessCenterGrpAttributes LegPaymentStreamPricingBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamPaymentDateGrp_Block_t {
	LegPaymentStreamPaymentDateGrpElements LegPaymentStreamPaymentDateGrpElements_;
			LegPaymentStreamPaymentDateGrpAttributes LegPaymentStreamPaymentDateGrpAttributes_;

};

struct LegPaymentStreamPricingDateGrp_Block_t {
	LegPaymentStreamPricingDateGrpElements LegPaymentStreamPricingDateGrpElements_;
			LegPaymentStreamPricingDateGrpAttributes LegPaymentStreamPricingDateGrpAttributes_;

};

struct LegPaymentStreamPricingDayGrp_Block_t {
	LegPaymentStreamPricingDayGrpElements LegPaymentStreamPricingDayGrpElements_;
			LegPaymentStreamPricingDayGrpAttributes LegPaymentStreamPricingDayGrpAttributes_;

};

struct LegPhysicalSettlTermGrp_Block_t {
	LegPhysicalSettlTermGrpElements LegPhysicalSettlTermGrpElements_;
			LegPhysicalSettlTermGrpAttributes LegPhysicalSettlTermGrpAttributes_;

};

struct LegPhysicalSettlDeliverableObligationGrp_Block_t {
	LegPhysicalSettlDeliverableObligationGrpElements LegPhysicalSettlDeliverableObligationGrpElements_;
			LegPhysicalSettlDeliverableObligationGrpAttributes LegPhysicalSettlDeliverableObligationGrpAttributes_;

};

struct LegPricingDateBusinessCenterGrp_Block_t {
	LegPricingDateBusinessCenterGrpElements LegPricingDateBusinessCenterGrpElements_;
			LegPricingDateBusinessCenterGrpAttributes LegPricingDateBusinessCenterGrpAttributes_;

};

struct LegPricingDateTime_Block_t {
	LegPricingDateTimeElements LegPricingDateTimeElements_;
			LegPricingDateTimeAttributes LegPricingDateTimeAttributes_;

};

struct LegProtectionTermEventNewsSourceGrp_Block_t {
	LegProtectionTermEventNewsSourceGrpElements LegProtectionTermEventNewsSourceGrpElements_;
			LegProtectionTermEventNewsSourceGrpAttributes LegProtectionTermEventNewsSourceGrpAttributes_;

};

struct LegProtectionTermGrp_Block_t {
	LegProtectionTermGrpElements LegProtectionTermGrpElements_;
			LegProtectionTermGrpAttributes LegProtectionTermGrpAttributes_;

};

struct LegProtectionTermEventGrp_Block_t {
	LegProtectionTermEventGrpElements LegProtectionTermEventGrpElements_;
			LegProtectionTermEventGrpAttributes LegProtectionTermEventGrpAttributes_;

};

struct LegProtectionTermEventQualifierGrp_Block_t {
	LegProtectionTermEventQualifierGrpElements LegProtectionTermEventQualifierGrpElements_;
			LegProtectionTermEventQualifierGrpAttributes LegProtectionTermEventQualifierGrpAttributes_;

};

struct LegProtectionTermObligationGrp_Block_t {
	LegProtectionTermObligationGrpElements LegProtectionTermObligationGrpElements_;
			LegProtectionTermObligationGrpAttributes LegProtectionTermObligationGrpAttributes_;

};

struct LegStreamCalculationPeriodDateGrp_Block_t {
	LegStreamCalculationPeriodDateGrpElements LegStreamCalculationPeriodDateGrpElements_;
			LegStreamCalculationPeriodDateGrpAttributes LegStreamCalculationPeriodDateGrpAttributes_;

};

struct LegStreamCommoditySettlBusinessCenterGrp_Block_t {
	LegStreamCommoditySettlBusinessCenterGrpElements LegStreamCommoditySettlBusinessCenterGrpElements_;
			LegStreamCommoditySettlBusinessCenterGrpAttributes LegStreamCommoditySettlBusinessCenterGrpAttributes_;

};

struct LegStreamCommodity_Block_t {
	LegStreamCommodityElements LegStreamCommodityElements_;
			LegStreamCommodityAttributes LegStreamCommodityAttributes_;

};

struct LegStreamCommodityAltIDGrp_Block_t {
	LegStreamCommodityAltIDGrpElements LegStreamCommodityAltIDGrpElements_;
			LegStreamCommodityAltIDGrpAttributes LegStreamCommodityAltIDGrpAttributes_;

};

struct LegStreamCommodityDataSourceGrp_Block_t {
	LegStreamCommodityDataSourceGrpElements LegStreamCommodityDataSourceGrpElements_;
			LegStreamCommodityDataSourceGrpAttributes LegStreamCommodityDataSourceGrpAttributes_;

};

struct LegStreamCommoditySettlDayGrp_Block_t {
	LegStreamCommoditySettlDayGrpElements LegStreamCommoditySettlDayGrpElements_;
			LegStreamCommoditySettlDayGrpAttributes LegStreamCommoditySettlDayGrpAttributes_;

};

struct LegStreamCommoditySettlTimeGrp_Block_t {
	LegStreamCommoditySettlTimeGrpElements LegStreamCommoditySettlTimeGrpElements_;
			LegStreamCommoditySettlTimeGrpAttributes LegStreamCommoditySettlTimeGrpAttributes_;

};

struct LegStreamCommoditySettlPeriodGrp_Block_t {
	LegStreamCommoditySettlPeriodGrpElements LegStreamCommoditySettlPeriodGrpElements_;
			LegStreamCommoditySettlPeriodGrpAttributes LegStreamCommoditySettlPeriodGrpAttributes_;

};

struct UnderlyingAssetAttributeGrp_Block_t {
	UnderlyingAssetAttributeGrpElements UnderlyingAssetAttributeGrpElements_;
			UnderlyingAssetAttributeGrpAttributes UnderlyingAssetAttributeGrpAttributes_;

};

struct UnderlyingComplexEventAveragingObservationGrp_Block_t {
	UnderlyingComplexEventAveragingObservationGrpElements UnderlyingComplexEventAveragingObservationGrpElements_;
			UnderlyingComplexEventAveragingObservationGrpAttributes UnderlyingComplexEventAveragingObservationGrpAttributes_;

};

struct UnderlyingComplexEventCreditEventGrp_Block_t {
	UnderlyingComplexEventCreditEventGrpElements UnderlyingComplexEventCreditEventGrpElements_;
			UnderlyingComplexEventCreditEventGrpAttributes UnderlyingComplexEventCreditEventGrpAttributes_;

};

struct UnderlyingComplexEventCreditEventQualifierGrp_Block_t {
	UnderlyingComplexEventCreditEventQualifierGrpElements UnderlyingComplexEventCreditEventQualifierGrpElements_;
			UnderlyingComplexEventCreditEventQualifierGrpAttributes UnderlyingComplexEventCreditEventQualifierGrpAttributes_;

};

struct UnderlyingComplexEventPeriodDateGrp_Block_t {
	UnderlyingComplexEventPeriodDateGrpElements UnderlyingComplexEventPeriodDateGrpElements_;
			UnderlyingComplexEventPeriodDateGrpAttributes UnderlyingComplexEventPeriodDateGrpAttributes_;

};

struct UnderlyingComplexEventPeriodGrp_Block_t {
	UnderlyingComplexEventPeriodGrpElements UnderlyingComplexEventPeriodGrpElements_;
			UnderlyingComplexEventPeriodGrpAttributes UnderlyingComplexEventPeriodGrpAttributes_;

};

struct UnderlyingComplexEventRateSourceGrp_Block_t {
	UnderlyingComplexEventRateSourceGrpElements UnderlyingComplexEventRateSourceGrpElements_;
			UnderlyingComplexEventRateSourceGrpAttributes UnderlyingComplexEventRateSourceGrpAttributes_;

};

struct UnderlyingComplexEventDateBusinessCenterGrp_Block_t {
	UnderlyingComplexEventDateBusinessCenterGrpElements UnderlyingComplexEventDateBusinessCenterGrpElements_;
			UnderlyingComplexEventDateBusinessCenterGrpAttributes UnderlyingComplexEventDateBusinessCenterGrpAttributes_;

};

struct UnderlyingComplexEventRelativeDate_Block_t {
	UnderlyingComplexEventRelativeDateElements UnderlyingComplexEventRelativeDateElements_;
			UnderlyingComplexEventRelativeDateAttributes UnderlyingComplexEventRelativeDateAttributes_;

};

struct UnderlyingComplexEventCreditEventSourceGrp_Block_t {
	UnderlyingComplexEventCreditEventSourceGrpElements UnderlyingComplexEventCreditEventSourceGrpElements_;
			UnderlyingComplexEventCreditEventSourceGrpAttributes UnderlyingComplexEventCreditEventSourceGrpAttributes_;

};

struct UnderlyingComplexEventScheduleGrp_Block_t {
	UnderlyingComplexEventScheduleGrpElements UnderlyingComplexEventScheduleGrpElements_;
			UnderlyingComplexEventScheduleGrpAttributes UnderlyingComplexEventScheduleGrpAttributes_;

};

struct UnderlyingDeliveryScheduleGrp_Block_t {
	UnderlyingDeliveryScheduleGrpElements UnderlyingDeliveryScheduleGrpElements_;
			UnderlyingDeliveryScheduleGrpAttributes UnderlyingDeliveryScheduleGrpAttributes_;

};

struct UnderlyingDeliveryScheduleSettlDayGrp_Block_t {
	UnderlyingDeliveryScheduleSettlDayGrpElements UnderlyingDeliveryScheduleSettlDayGrpElements_;
			UnderlyingDeliveryScheduleSettlDayGrpAttributes UnderlyingDeliveryScheduleSettlDayGrpAttributes_;

};

struct UnderlyingDeliveryScheduleSettlTimeGrp_Block_t {
	UnderlyingDeliveryScheduleSettlTimeGrpElements UnderlyingDeliveryScheduleSettlTimeGrpElements_;
			UnderlyingDeliveryScheduleSettlTimeGrpAttributes UnderlyingDeliveryScheduleSettlTimeGrpAttributes_;

};

struct UnderlyingDeliveryStream_Block_t {
	UnderlyingDeliveryStreamElements UnderlyingDeliveryStreamElements_;
			UnderlyingDeliveryStreamAttributes UnderlyingDeliveryStreamAttributes_;

};

struct UnderlyingStreamAssetAttributeGrp_Block_t {
	UnderlyingStreamAssetAttributeGrpElements UnderlyingStreamAssetAttributeGrpElements_;
			UnderlyingStreamAssetAttributeGrpAttributes UnderlyingStreamAssetAttributeGrpAttributes_;

};

struct UnderlyingDeliveryStreamCycleGrp_Block_t {
	UnderlyingDeliveryStreamCycleGrpElements UnderlyingDeliveryStreamCycleGrpElements_;
			UnderlyingDeliveryStreamCycleGrpAttributes UnderlyingDeliveryStreamCycleGrpAttributes_;

};

struct UnderlyingDeliveryStreamCommoditySourceGrp_Block_t {
	UnderlyingDeliveryStreamCommoditySourceGrpElements UnderlyingDeliveryStreamCommoditySourceGrpElements_;
			UnderlyingDeliveryStreamCommoditySourceGrpAttributes UnderlyingDeliveryStreamCommoditySourceGrpAttributes_;

};

struct UnderlyingOptionExercise_Block_t {
	UnderlyingOptionExerciseElements UnderlyingOptionExerciseElements_;
			UnderlyingOptionExerciseAttributes UnderlyingOptionExerciseAttributes_;

};

struct UnderlyingOptionExerciseBusinessCenterGrp_Block_t {
	UnderlyingOptionExerciseBusinessCenterGrpElements UnderlyingOptionExerciseBusinessCenterGrpElements_;
			UnderlyingOptionExerciseBusinessCenterGrpAttributes UnderlyingOptionExerciseBusinessCenterGrpAttributes_;

};

struct UnderlyingOptionExerciseDates_Block_t {
	UnderlyingOptionExerciseDatesElements UnderlyingOptionExerciseDatesElements_;
			UnderlyingOptionExerciseDatesAttributes UnderlyingOptionExerciseDatesAttributes_;

};

struct UnderlyingOptionExerciseDateGrp_Block_t {
	UnderlyingOptionExerciseDateGrpElements UnderlyingOptionExerciseDateGrpElements_;
			UnderlyingOptionExerciseDateGrpAttributes UnderlyingOptionExerciseDateGrpAttributes_;

};

struct UnderlyingOptionExerciseExpirationDateBusinessCenterGrp_Block_t {
	UnderlyingOptionExerciseExpirationDateBusinessCenterGrpElements UnderlyingOptionExerciseExpirationDateBusinessCenterGrpElements_;
			UnderlyingOptionExerciseExpirationDateBusinessCenterGrpAttributes UnderlyingOptionExerciseExpirationDateBusinessCenterGrpAttributes_;

};

struct UnderlyingOptionExerciseExpiration_Block_t {
	UnderlyingOptionExerciseExpirationElements UnderlyingOptionExerciseExpirationElements_;
			UnderlyingOptionExerciseExpirationAttributes UnderlyingOptionExerciseExpirationAttributes_;

};

struct UnderlyingOptionExerciseExpirationDateGrp_Block_t {
	UnderlyingOptionExerciseExpirationDateGrpElements UnderlyingOptionExerciseExpirationDateGrpElements_;
			UnderlyingOptionExerciseExpirationDateGrpAttributes UnderlyingOptionExerciseExpirationDateGrpAttributes_;

};

struct UnderlyingMarketDisruption_Block_t {
	UnderlyingMarketDisruptionElements UnderlyingMarketDisruptionElements_;
			UnderlyingMarketDisruptionAttributes UnderlyingMarketDisruptionAttributes_;

};

struct UnderlyingMarketDisruptionEventGrp_Block_t {
	UnderlyingMarketDisruptionEventGrpElements UnderlyingMarketDisruptionEventGrpElements_;
			UnderlyingMarketDisruptionEventGrpAttributes UnderlyingMarketDisruptionEventGrpAttributes_;

};

struct UnderlyingMarketDisruptionFallbackGrp_Block_t {
	UnderlyingMarketDisruptionFallbackGrpElements UnderlyingMarketDisruptionFallbackGrpElements_;
			UnderlyingMarketDisruptionFallbackGrpAttributes UnderlyingMarketDisruptionFallbackGrpAttributes_;

};

struct UnderlyingMarketDisruptionFallbackReferencePriceGrp_Block_t {
	UnderlyingMarketDisruptionFallbackReferencePriceGrpElements UnderlyingMarketDisruptionFallbackReferencePriceGrpElements_;
			UnderlyingMarketDisruptionFallbackReferencePriceGrpAttributes UnderlyingMarketDisruptionFallbackReferencePriceGrpAttributes_;

};

struct UnderlyingPaymentScheduleFixingDayGrp_Block_t {
	UnderlyingPaymentScheduleFixingDayGrpElements UnderlyingPaymentScheduleFixingDayGrpElements_;
			UnderlyingPaymentScheduleFixingDayGrpAttributes UnderlyingPaymentScheduleFixingDayGrpAttributes_;

};

struct UnderlyingPaymentStreamPricingBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamPricingBusinessCenterGrpElements UnderlyingPaymentStreamPricingBusinessCenterGrpElements_;
			UnderlyingPaymentStreamPricingBusinessCenterGrpAttributes UnderlyingPaymentStreamPricingBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamPaymentDateGrp_Block_t {
	UnderlyingPaymentStreamPaymentDateGrpElements UnderlyingPaymentStreamPaymentDateGrpElements_;
			UnderlyingPaymentStreamPaymentDateGrpAttributes UnderlyingPaymentStreamPaymentDateGrpAttributes_;

};

struct UnderlyingPaymentStreamPricingDateGrp_Block_t {
	UnderlyingPaymentStreamPricingDateGrpElements UnderlyingPaymentStreamPricingDateGrpElements_;
			UnderlyingPaymentStreamPricingDateGrpAttributes UnderlyingPaymentStreamPricingDateGrpAttributes_;

};

struct UnderlyingPaymentStreamPricingDayGrp_Block_t {
	UnderlyingPaymentStreamPricingDayGrpElements UnderlyingPaymentStreamPricingDayGrpElements_;
			UnderlyingPaymentStreamPricingDayGrpAttributes UnderlyingPaymentStreamPricingDayGrpAttributes_;

};

struct UnderlyingPricingDateBusinessCenterGrp_Block_t {
	UnderlyingPricingDateBusinessCenterGrpElements UnderlyingPricingDateBusinessCenterGrpElements_;
			UnderlyingPricingDateBusinessCenterGrpAttributes UnderlyingPricingDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPricingDateTime_Block_t {
	UnderlyingPricingDateTimeElements UnderlyingPricingDateTimeElements_;
			UnderlyingPricingDateTimeAttributes UnderlyingPricingDateTimeAttributes_;

};

struct UnderlyingStreamCalculationPeriodDateGrp_Block_t {
	UnderlyingStreamCalculationPeriodDateGrpElements UnderlyingStreamCalculationPeriodDateGrpElements_;
			UnderlyingStreamCalculationPeriodDateGrpAttributes UnderlyingStreamCalculationPeriodDateGrpAttributes_;

};

struct UnderlyingStreamCommoditySettlBusinessCenterGrp_Block_t {
	UnderlyingStreamCommoditySettlBusinessCenterGrpElements UnderlyingStreamCommoditySettlBusinessCenterGrpElements_;
			UnderlyingStreamCommoditySettlBusinessCenterGrpAttributes UnderlyingStreamCommoditySettlBusinessCenterGrpAttributes_;

};

struct UnderlyingStreamCommodity_Block_t {
	UnderlyingStreamCommodityElements UnderlyingStreamCommodityElements_;
			UnderlyingStreamCommodityAttributes UnderlyingStreamCommodityAttributes_;

};

struct UnderlyingStreamCommodityAltIDGrp_Block_t {
	UnderlyingStreamCommodityAltIDGrpElements UnderlyingStreamCommodityAltIDGrpElements_;
			UnderlyingStreamCommodityAltIDGrpAttributes UnderlyingStreamCommodityAltIDGrpAttributes_;

};

struct UnderlyingStreamCommodityDataSourceGrp_Block_t {
	UnderlyingStreamCommodityDataSourceGrpElements UnderlyingStreamCommodityDataSourceGrpElements_;
			UnderlyingStreamCommodityDataSourceGrpAttributes UnderlyingStreamCommodityDataSourceGrpAttributes_;

};

struct UnderlyingStreamCommoditySettlDayGrp_Block_t {
	UnderlyingStreamCommoditySettlDayGrpElements UnderlyingStreamCommoditySettlDayGrpElements_;
			UnderlyingStreamCommoditySettlDayGrpAttributes UnderlyingStreamCommoditySettlDayGrpAttributes_;

};

struct UnderlyingStreamCommoditySettlTimeGrp_Block_t {
	UnderlyingStreamCommoditySettlTimeGrpElements UnderlyingStreamCommoditySettlTimeGrpElements_;
			UnderlyingStreamCommoditySettlTimeGrpAttributes UnderlyingStreamCommoditySettlTimeGrpAttributes_;

};

struct UnderlyingStreamCommoditySettlPeriodGrp_Block_t {
	UnderlyingStreamCommoditySettlPeriodGrpElements UnderlyingStreamCommoditySettlPeriodGrpElements_;
			UnderlyingStreamCommoditySettlPeriodGrpAttributes UnderlyingStreamCommoditySettlPeriodGrpAttributes_;

};

struct UnderlyingAdditionalTermBondRefGrp_Block_t {
	UnderlyingAdditionalTermBondRefGrpElements UnderlyingAdditionalTermBondRefGrpElements_;
			UnderlyingAdditionalTermBondRefGrpAttributes UnderlyingAdditionalTermBondRefGrpAttributes_;

};

struct UnderlyingAdditionalTermGrp_Block_t {
	UnderlyingAdditionalTermGrpElements UnderlyingAdditionalTermGrpElements_;
			UnderlyingAdditionalTermGrpAttributes UnderlyingAdditionalTermGrpAttributes_;

};

struct UnderlyingCashSettlDealerGrp_Block_t {
	UnderlyingCashSettlDealerGrpElements UnderlyingCashSettlDealerGrpElements_;
			UnderlyingCashSettlDealerGrpAttributes UnderlyingCashSettlDealerGrpAttributes_;

};

struct UnderlyingCashSettlTermGrp_Block_t {
	UnderlyingCashSettlTermGrpElements UnderlyingCashSettlTermGrpElements_;
			UnderlyingCashSettlTermGrpAttributes UnderlyingCashSettlTermGrpAttributes_;

};

struct UnderlyingPhysicalSettlTermGrp_Block_t {
	UnderlyingPhysicalSettlTermGrpElements UnderlyingPhysicalSettlTermGrpElements_;
			UnderlyingPhysicalSettlTermGrpAttributes UnderlyingPhysicalSettlTermGrpAttributes_;

};

struct UnderlyingPhysicalSettlDeliverableObligationGrp_Block_t {
	UnderlyingPhysicalSettlDeliverableObligationGrpElements UnderlyingPhysicalSettlDeliverableObligationGrpElements_;
			UnderlyingPhysicalSettlDeliverableObligationGrpAttributes UnderlyingPhysicalSettlDeliverableObligationGrpAttributes_;

};

struct UnderlyingProtectionTermGrp_Block_t {
	UnderlyingProtectionTermGrpElements UnderlyingProtectionTermGrpElements_;
			UnderlyingProtectionTermGrpAttributes UnderlyingProtectionTermGrpAttributes_;

};

struct UnderlyingProtectionTermEventGrp_Block_t {
	UnderlyingProtectionTermEventGrpElements UnderlyingProtectionTermEventGrpElements_;
			UnderlyingProtectionTermEventGrpAttributes UnderlyingProtectionTermEventGrpAttributes_;

};

struct UnderlyingProtectionTermEventQualifierGrp_Block_t {
	UnderlyingProtectionTermEventQualifierGrpElements UnderlyingProtectionTermEventQualifierGrpElements_;
			UnderlyingProtectionTermEventQualifierGrpAttributes UnderlyingProtectionTermEventQualifierGrpAttributes_;

};

struct UnderlyingProtectionTermObligationGrp_Block_t {
	UnderlyingProtectionTermObligationGrpElements UnderlyingProtectionTermObligationGrpElements_;
			UnderlyingProtectionTermObligationGrpAttributes UnderlyingProtectionTermObligationGrpAttributes_;

};

struct UnderlyingProtectionTermEventNewsSourceGrp_Block_t {
	UnderlyingProtectionTermEventNewsSourceGrpElements UnderlyingProtectionTermEventNewsSourceGrpElements_;
			UnderlyingProtectionTermEventNewsSourceGrpAttributes UnderlyingProtectionTermEventNewsSourceGrpAttributes_;

};

struct UnderlyingProvisionCashSettlPaymentDates_Block_t {
	UnderlyingProvisionCashSettlPaymentDatesElements UnderlyingProvisionCashSettlPaymentDatesElements_;
			UnderlyingProvisionCashSettlPaymentDatesAttributes UnderlyingProvisionCashSettlPaymentDatesAttributes_;

};

struct UnderlyingProvisionCashSettlPaymentFixedDateGrp_Block_t {
	UnderlyingProvisionCashSettlPaymentFixedDateGrpElements UnderlyingProvisionCashSettlPaymentFixedDateGrpElements_;
			UnderlyingProvisionCashSettlPaymentFixedDateGrpAttributes UnderlyingProvisionCashSettlPaymentFixedDateGrpAttributes_;

};

struct UnderlyingProvisionCashSettlQuoteSource_Block_t {
	UnderlyingProvisionCashSettlQuoteSourceElements UnderlyingProvisionCashSettlQuoteSourceElements_;
			UnderlyingProvisionCashSettlQuoteSourceAttributes UnderlyingProvisionCashSettlQuoteSourceAttributes_;

};

struct UnderlyingProvisionCashSettlValueDates_Block_t {
	UnderlyingProvisionCashSettlValueDatesElements UnderlyingProvisionCashSettlValueDatesElements_;
			UnderlyingProvisionCashSettlValueDatesAttributes UnderlyingProvisionCashSettlValueDatesAttributes_;

};

struct UnderlyingProvisionOptionExerciseFixedDateGrp_Block_t {
	UnderlyingProvisionOptionExerciseFixedDateGrpElements UnderlyingProvisionOptionExerciseFixedDateGrpElements_;
			UnderlyingProvisionOptionExerciseFixedDateGrpAttributes UnderlyingProvisionOptionExerciseFixedDateGrpAttributes_;

};

struct UnderlyingProvisionOptionExerciseDates_Block_t {
	UnderlyingProvisionOptionExerciseDatesElements UnderlyingProvisionOptionExerciseDatesElements_;
			UnderlyingProvisionOptionExerciseDatesAttributes UnderlyingProvisionOptionExerciseDatesAttributes_;

};

struct UnderlyingProvisionOptionExpirationDate_Block_t {
	UnderlyingProvisionOptionExpirationDateElements UnderlyingProvisionOptionExpirationDateElements_;
			UnderlyingProvisionOptionExpirationDateAttributes UnderlyingProvisionOptionExpirationDateAttributes_;

};

struct UnderlyingProvisionOptionRelevantUnderlyingDate_Block_t {
	UnderlyingProvisionOptionRelevantUnderlyingDateElements UnderlyingProvisionOptionRelevantUnderlyingDateElements_;
			UnderlyingProvisionOptionRelevantUnderlyingDateAttributes UnderlyingProvisionOptionRelevantUnderlyingDateAttributes_;

};

struct UnderlyingProvisionGrp_Block_t {
	UnderlyingProvisionGrpElements UnderlyingProvisionGrpElements_;
			UnderlyingProvisionGrpAttributes UnderlyingProvisionGrpAttributes_;

};

struct UnderlyingProvisionParties_Block_t {
	UnderlyingProvisionPartiesElements UnderlyingProvisionPartiesElements_;
			UnderlyingProvisionPartiesAttributes UnderlyingProvisionPartiesAttributes_;

};

struct UnderlyingProvisionPtysSubGrp_Block_t {
	UnderlyingProvisionPtysSubGrpElements UnderlyingProvisionPtysSubGrpElements_;
			UnderlyingProvisionPtysSubGrpAttributes UnderlyingProvisionPtysSubGrpAttributes_;

};

struct UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrp_Block_t {
	UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpElements UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpElements_;
			UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpAttributes UnderlyingProvisionCashSettlPaymentDateBusinessCenterGrpAttributes_;

};

struct UnderlyingProvisionCashSettlValueDateBusinessCenterGrp_Block_t {
	UnderlyingProvisionCashSettlValueDateBusinessCenterGrpElements UnderlyingProvisionCashSettlValueDateBusinessCenterGrpElements_;
			UnderlyingProvisionCashSettlValueDateBusinessCenterGrpAttributes UnderlyingProvisionCashSettlValueDateBusinessCenterGrpAttributes_;

};

struct UnderlyingProvisionOptionExerciseBusinessCenterGrp_Block_t {
	UnderlyingProvisionOptionExerciseBusinessCenterGrpElements UnderlyingProvisionOptionExerciseBusinessCenterGrpElements_;
			UnderlyingProvisionOptionExerciseBusinessCenterGrpAttributes UnderlyingProvisionOptionExerciseBusinessCenterGrpAttributes_;

};

struct UnderlyingProvisionOptionExpirationDateBusinessCenterGrp_Block_t {
	UnderlyingProvisionOptionExpirationDateBusinessCenterGrpElements UnderlyingProvisionOptionExpirationDateBusinessCenterGrpElements_;
			UnderlyingProvisionOptionExpirationDateBusinessCenterGrpAttributes UnderlyingProvisionOptionExpirationDateBusinessCenterGrpAttributes_;

};

struct UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrp_Block_t {
	UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpElements_;
			UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes UnderlyingProvisionOptionRelevantUnderlyingDateBusinessCenterGrpAttributes_;

};

struct UnderlyingProvisionDateBusinessCenterGrp_Block_t {
	UnderlyingProvisionDateBusinessCenterGrpElements UnderlyingProvisionDateBusinessCenterGrpElements_;
			UnderlyingProvisionDateBusinessCenterGrpAttributes UnderlyingProvisionDateBusinessCenterGrpAttributes_;

};

struct TargetPtysSubGrp_Block_t {
	TargetPtysSubGrpElements TargetPtysSubGrpElements_;
			TargetPtysSubGrpAttributes TargetPtysSubGrpAttributes_;

};

struct LegFinancingDetails_Block_t {
	LegFinancingDetailsElements LegFinancingDetailsElements_;
			LegFinancingDetailsAttributes LegFinancingDetailsAttributes_;

};

struct LegFinancingContractualDefinitionsGrp_Block_t {
	LegFinancingContractualDefinitionsGrpElements LegFinancingContractualDefinitionsGrpElements_;
			LegFinancingContractualDefinitionsGrpAttributes LegFinancingContractualDefinitionsGrpAttributes_;

};

struct LegFinancingTermSupplementGrp_Block_t {
	LegFinancingTermSupplementGrpElements LegFinancingTermSupplementGrpElements_;
			LegFinancingTermSupplementGrpAttributes LegFinancingTermSupplementGrpAttributes_;

};

struct LegFinancingContractualMatrixGrp_Block_t {
	LegFinancingContractualMatrixGrpElements LegFinancingContractualMatrixGrpElements_;
			LegFinancingContractualMatrixGrpAttributes LegFinancingContractualMatrixGrpAttributes_;

};

struct RelativeValueGrp_Block_t {
	RelativeValueGrpElements RelativeValueGrpElements_;
			RelativeValueGrpAttributes RelativeValueGrpAttributes_;

};

struct AuctionTypeRuleGrp_Block_t {
	AuctionTypeRuleGrpElements AuctionTypeRuleGrpElements_;
			AuctionTypeRuleGrpAttributes AuctionTypeRuleGrpAttributes_;

};

struct FlexProductEligibilityGrp_Block_t {
	FlexProductEligibilityGrpElements FlexProductEligibilityGrpElements_;
			FlexProductEligibilityGrpAttributes FlexProductEligibilityGrpAttributes_;

};

struct PriceRangeRuleGrp_Block_t {
	PriceRangeRuleGrpElements PriceRangeRuleGrpElements_;
			PriceRangeRuleGrpAttributes PriceRangeRuleGrpAttributes_;

};

struct QuoteSizeRuleGrp_Block_t {
	QuoteSizeRuleGrpElements QuoteSizeRuleGrpElements_;
			QuoteSizeRuleGrpAttributes QuoteSizeRuleGrpAttributes_;

};

struct RelatedMarketSegmentGrp_Block_t {
	RelatedMarketSegmentGrpElements RelatedMarketSegmentGrpElements_;
			RelatedMarketSegmentGrpAttributes RelatedMarketSegmentGrpAttributes_;

};

struct ClearingPriceParametersGrp_Block_t {
	ClearingPriceParametersGrpElements ClearingPriceParametersGrpElements_;
			ClearingPriceParametersGrpAttributes ClearingPriceParametersGrpAttributes_;

};

struct MiscFeesSubGrp_Block_t {
	MiscFeesSubGrpElements MiscFeesSubGrpElements_;
			MiscFeesSubGrpAttributes MiscFeesSubGrpAttributes_;

};

struct CommissionDataGrp_Block_t {
	CommissionDataGrpElements CommissionDataGrpElements_;
			CommissionDataGrpAttributes CommissionDataGrpAttributes_;

};

struct AllocCommissionDataGrp_Block_t {
	AllocCommissionDataGrpElements AllocCommissionDataGrpElements_;
			AllocCommissionDataGrpAttributes AllocCommissionDataGrpAttributes_;

};

struct CashSettlDate_Block_t {
	CashSettlDateElements CashSettlDateElements_;
			CashSettlDateAttributes CashSettlDateAttributes_;

};

struct CashSettlDateBusinessCenterGrp_Block_t {
	CashSettlDateBusinessCenterGrpElements CashSettlDateBusinessCenterGrpElements_;
			CashSettlDateBusinessCenterGrpAttributes CashSettlDateBusinessCenterGrpAttributes_;

};

struct DividendAccrualFloatingRate_Block_t {
	DividendAccrualFloatingRateElements DividendAccrualFloatingRateElements_;
			DividendAccrualFloatingRateAttributes DividendAccrualFloatingRateAttributes_;

};

struct DividendAccrualPaymentDateBusinessCenterGrp_Block_t {
	DividendAccrualPaymentDateBusinessCenterGrpElements DividendAccrualPaymentDateBusinessCenterGrpElements_;
			DividendAccrualPaymentDateBusinessCenterGrpAttributes DividendAccrualPaymentDateBusinessCenterGrpAttributes_;

};

struct DividendAccrualPaymentDate_Block_t {
	DividendAccrualPaymentDateElements DividendAccrualPaymentDateElements_;
			DividendAccrualPaymentDateAttributes DividendAccrualPaymentDateAttributes_;

};

struct DividendConditions_Block_t {
	DividendConditionsElements DividendConditionsElements_;
			DividendConditionsAttributes DividendConditionsAttributes_;

};

struct DividendFXTriggerDate_Block_t {
	DividendFXTriggerDateElements DividendFXTriggerDateElements_;
			DividendFXTriggerDateAttributes DividendFXTriggerDateAttributes_;

};

struct DividendFXTriggerDateBusinessCenterGrp_Block_t {
	DividendFXTriggerDateBusinessCenterGrpElements DividendFXTriggerDateBusinessCenterGrpElements_;
			DividendFXTriggerDateBusinessCenterGrpAttributes DividendFXTriggerDateBusinessCenterGrpAttributes_;

};

struct DividendPeriodGrp_Block_t {
	DividendPeriodGrpElements DividendPeriodGrpElements_;
			DividendPeriodGrpAttributes DividendPeriodGrpAttributes_;

};

struct DividendPeriodBusinessCenterGrp_Block_t {
	DividendPeriodBusinessCenterGrpElements DividendPeriodBusinessCenterGrpElements_;
			DividendPeriodBusinessCenterGrpAttributes DividendPeriodBusinessCenterGrpAttributes_;

};

struct ExtraordinaryEventGrp_Block_t {
	ExtraordinaryEventGrpElements ExtraordinaryEventGrpElements_;
			ExtraordinaryEventGrpAttributes ExtraordinaryEventGrpAttributes_;

};

struct LegCashSettlDate_Block_t {
	LegCashSettlDateElements LegCashSettlDateElements_;
			LegCashSettlDateAttributes LegCashSettlDateAttributes_;

};

struct LegCashSettlDateBusinessCenterGrp_Block_t {
	LegCashSettlDateBusinessCenterGrpElements LegCashSettlDateBusinessCenterGrpElements_;
			LegCashSettlDateBusinessCenterGrpAttributes LegCashSettlDateBusinessCenterGrpAttributes_;

};

struct LegDividendAccrualPaymentDateBusinessCenterGrp_Block_t {
	LegDividendAccrualPaymentDateBusinessCenterGrpElements LegDividendAccrualPaymentDateBusinessCenterGrpElements_;
			LegDividendAccrualPaymentDateBusinessCenterGrpAttributes LegDividendAccrualPaymentDateBusinessCenterGrpAttributes_;

};

struct LegDividendAccrualFloatingRate_Block_t {
	LegDividendAccrualFloatingRateElements LegDividendAccrualFloatingRateElements_;
			LegDividendAccrualFloatingRateAttributes LegDividendAccrualFloatingRateAttributes_;

};

struct LegDividendAccrualPaymentDate_Block_t {
	LegDividendAccrualPaymentDateElements LegDividendAccrualPaymentDateElements_;
			LegDividendAccrualPaymentDateAttributes LegDividendAccrualPaymentDateAttributes_;

};

struct LegDividendConditions_Block_t {
	LegDividendConditionsElements LegDividendConditionsElements_;
			LegDividendConditionsAttributes LegDividendConditionsAttributes_;

};

struct LegDividendFXTriggerDate_Block_t {
	LegDividendFXTriggerDateElements LegDividendFXTriggerDateElements_;
			LegDividendFXTriggerDateAttributes LegDividendFXTriggerDateAttributes_;

};

struct LegDividendFXTriggerDateBusinessCenterGrp_Block_t {
	LegDividendFXTriggerDateBusinessCenterGrpElements LegDividendFXTriggerDateBusinessCenterGrpElements_;
			LegDividendFXTriggerDateBusinessCenterGrpAttributes LegDividendFXTriggerDateBusinessCenterGrpAttributes_;

};

struct LegDividendPeriodGrp_Block_t {
	LegDividendPeriodGrpElements LegDividendPeriodGrpElements_;
			LegDividendPeriodGrpAttributes LegDividendPeriodGrpAttributes_;

};

struct LegDividendPeriodBusinessCenterGrp_Block_t {
	LegDividendPeriodBusinessCenterGrpElements LegDividendPeriodBusinessCenterGrpElements_;
			LegDividendPeriodBusinessCenterGrpAttributes LegDividendPeriodBusinessCenterGrpAttributes_;

};

struct LegExtraordinaryEventGrp_Block_t {
	LegExtraordinaryEventGrpElements LegExtraordinaryEventGrpElements_;
			LegExtraordinaryEventGrpAttributes LegExtraordinaryEventGrpAttributes_;

};

struct LegOptionExerciseMakeWholeProvision_Block_t {
	LegOptionExerciseMakeWholeProvisionElements LegOptionExerciseMakeWholeProvisionElements_;
			LegOptionExerciseMakeWholeProvisionAttributes LegOptionExerciseMakeWholeProvisionAttributes_;

};

struct LegPaymentStreamCompoundingDateGrp_Block_t {
	LegPaymentStreamCompoundingDateGrpElements LegPaymentStreamCompoundingDateGrpElements_;
			LegPaymentStreamCompoundingDateGrpAttributes LegPaymentStreamCompoundingDateGrpAttributes_;

};

struct LegPaymentStreamCompoundingDates_Block_t {
	LegPaymentStreamCompoundingDatesElements LegPaymentStreamCompoundingDatesElements_;
			LegPaymentStreamCompoundingDatesAttributes LegPaymentStreamCompoundingDatesAttributes_;

};

struct LegPaymentStreamCompoundingDatesBusinessCenterGrp_Block_t {
	LegPaymentStreamCompoundingDatesBusinessCenterGrpElements LegPaymentStreamCompoundingDatesBusinessCenterGrpElements_;
			LegPaymentStreamCompoundingDatesBusinessCenterGrpAttributes LegPaymentStreamCompoundingDatesBusinessCenterGrpAttributes_;

};

struct LegPaymentStreamCompoundingEndDate_Block_t {
	LegPaymentStreamCompoundingEndDateElements LegPaymentStreamCompoundingEndDateElements_;
			LegPaymentStreamCompoundingEndDateAttributes LegPaymentStreamCompoundingEndDateAttributes_;

};

struct LegPaymentStreamCompoundingFloatingRate_Block_t {
	LegPaymentStreamCompoundingFloatingRateElements LegPaymentStreamCompoundingFloatingRateElements_;
			LegPaymentStreamCompoundingFloatingRateAttributes LegPaymentStreamCompoundingFloatingRateAttributes_;

};

struct LegPaymentStreamCompoundingStartDate_Block_t {
	LegPaymentStreamCompoundingStartDateElements LegPaymentStreamCompoundingStartDateElements_;
			LegPaymentStreamCompoundingStartDateAttributes LegPaymentStreamCompoundingStartDateAttributes_;

};

struct LegPaymentStreamFormulaImage_Block_t {
	LegPaymentStreamFormulaImageElements LegPaymentStreamFormulaImageElements_;
			LegPaymentStreamFormulaImageAttributes LegPaymentStreamFormulaImageAttributes_;

};

struct LegPaymentStreamFinalPricePaymentDate_Block_t {
	LegPaymentStreamFinalPricePaymentDateElements LegPaymentStreamFinalPricePaymentDateElements_;
			LegPaymentStreamFinalPricePaymentDateAttributes LegPaymentStreamFinalPricePaymentDateAttributes_;

};

struct LegPaymentStreamFixingDateGrp_Block_t {
	LegPaymentStreamFixingDateGrpElements LegPaymentStreamFixingDateGrpElements_;
			LegPaymentStreamFixingDateGrpAttributes LegPaymentStreamFixingDateGrpAttributes_;

};

struct LegPaymentStreamFormula_Block_t {
	LegPaymentStreamFormulaElements LegPaymentStreamFormulaElements_;
			LegPaymentStreamFormulaAttributes LegPaymentStreamFormulaAttributes_;

};

struct LegPaymentStreamFormulaMathGrp_Block_t {
	LegPaymentStreamFormulaMathGrpAttributes LegPaymentStreamFormulaMathGrpAttributes_;

};

struct LegPaymentStubEndDate_Block_t {
	LegPaymentStubEndDateElements LegPaymentStubEndDateElements_;
			LegPaymentStubEndDateAttributes LegPaymentStubEndDateAttributes_;

};

struct LegPaymentStubEndDateBusinessCenterGrp_Block_t {
	LegPaymentStubEndDateBusinessCenterGrpElements LegPaymentStubEndDateBusinessCenterGrpElements_;
			LegPaymentStubEndDateBusinessCenterGrpAttributes LegPaymentStubEndDateBusinessCenterGrpAttributes_;

};

struct LegPaymentStubStartDate_Block_t {
	LegPaymentStubStartDateElements LegPaymentStubStartDateElements_;
			LegPaymentStubStartDateAttributes LegPaymentStubStartDateAttributes_;

};

struct LegPaymentStubStartDateBusinessCenterGrp_Block_t {
	LegPaymentStubStartDateBusinessCenterGrpElements LegPaymentStubStartDateBusinessCenterGrpElements_;
			LegPaymentStubStartDateBusinessCenterGrpAttributes LegPaymentStubStartDateBusinessCenterGrpAttributes_;

};

struct LegReturnRateDateGrp_Block_t {
	LegReturnRateDateGrpElements LegReturnRateDateGrpElements_;
			LegReturnRateDateGrpAttributes LegReturnRateDateGrpAttributes_;

};

struct LegReturnRateFXConversionGrp_Block_t {
	LegReturnRateFXConversionGrpElements LegReturnRateFXConversionGrpElements_;
			LegReturnRateFXConversionGrpAttributes LegReturnRateFXConversionGrpAttributes_;

};

struct LegReturnRateGrp_Block_t {
	LegReturnRateGrpElements LegReturnRateGrpElements_;
			LegReturnRateGrpAttributes LegReturnRateGrpAttributes_;

};

struct LegReturnRateInformationSourceGrp_Block_t {
	LegReturnRateInformationSourceGrpElements LegReturnRateInformationSourceGrpElements_;
			LegReturnRateInformationSourceGrpAttributes LegReturnRateInformationSourceGrpAttributes_;

};

struct LegReturnRatePriceGrp_Block_t {
	LegReturnRatePriceGrpElements LegReturnRatePriceGrpElements_;
			LegReturnRatePriceGrpAttributes LegReturnRatePriceGrpAttributes_;

};

struct LegReturnRateValuationDateBusinessCenterGrp_Block_t {
	LegReturnRateValuationDateBusinessCenterGrpElements LegReturnRateValuationDateBusinessCenterGrpElements_;
			LegReturnRateValuationDateBusinessCenterGrpAttributes LegReturnRateValuationDateBusinessCenterGrpAttributes_;

};

struct LegReturnRateValuationDateGrp_Block_t {
	LegReturnRateValuationDateGrpElements LegReturnRateValuationDateGrpElements_;
			LegReturnRateValuationDateGrpAttributes LegReturnRateValuationDateGrpAttributes_;

};

struct LegSettlMethodElectionDate_Block_t {
	LegSettlMethodElectionDateElements LegSettlMethodElectionDateElements_;
			LegSettlMethodElectionDateAttributes LegSettlMethodElectionDateAttributes_;

};

struct LegSettlMethodElectionDateBusinessCenterGrp_Block_t {
	LegSettlMethodElectionDateBusinessCenterGrpElements LegSettlMethodElectionDateBusinessCenterGrpElements_;
			LegSettlMethodElectionDateBusinessCenterGrpAttributes LegSettlMethodElectionDateBusinessCenterGrpAttributes_;

};

struct OptionExerciseMakeWholeProvision_Block_t {
	OptionExerciseMakeWholeProvisionElements OptionExerciseMakeWholeProvisionElements_;
			OptionExerciseMakeWholeProvisionAttributes OptionExerciseMakeWholeProvisionAttributes_;

};

struct PaymentStreamCompoundingDateGrp_Block_t {
	PaymentStreamCompoundingDateGrpElements PaymentStreamCompoundingDateGrpElements_;
			PaymentStreamCompoundingDateGrpAttributes PaymentStreamCompoundingDateGrpAttributes_;

};

struct PaymentStreamCompoundingDates_Block_t {
	PaymentStreamCompoundingDatesElements PaymentStreamCompoundingDatesElements_;
			PaymentStreamCompoundingDatesAttributes PaymentStreamCompoundingDatesAttributes_;

};

struct PaymentStreamCompoundingDatesBusinessCenterGrp_Block_t {
	PaymentStreamCompoundingDatesBusinessCenterGrpElements PaymentStreamCompoundingDatesBusinessCenterGrpElements_;
			PaymentStreamCompoundingDatesBusinessCenterGrpAttributes PaymentStreamCompoundingDatesBusinessCenterGrpAttributes_;

};

struct PaymentStreamCompoundingEndDate_Block_t {
	PaymentStreamCompoundingEndDateElements PaymentStreamCompoundingEndDateElements_;
			PaymentStreamCompoundingEndDateAttributes PaymentStreamCompoundingEndDateAttributes_;

};

struct PaymentStreamCompoundingFloatingRate_Block_t {
	PaymentStreamCompoundingFloatingRateElements PaymentStreamCompoundingFloatingRateElements_;
			PaymentStreamCompoundingFloatingRateAttributes PaymentStreamCompoundingFloatingRateAttributes_;

};

struct PaymentStreamCompoundingStartDate_Block_t {
	PaymentStreamCompoundingStartDateElements PaymentStreamCompoundingStartDateElements_;
			PaymentStreamCompoundingStartDateAttributes PaymentStreamCompoundingStartDateAttributes_;

};

struct PaymentStreamFormulaImage_Block_t {
	PaymentStreamFormulaImageElements PaymentStreamFormulaImageElements_;
			PaymentStreamFormulaImageAttributes PaymentStreamFormulaImageAttributes_;

};

struct PaymentStreamFinalPricePaymentDate_Block_t {
	PaymentStreamFinalPricePaymentDateElements PaymentStreamFinalPricePaymentDateElements_;
			PaymentStreamFinalPricePaymentDateAttributes PaymentStreamFinalPricePaymentDateAttributes_;

};

struct PaymentStreamFixingDateGrp_Block_t {
	PaymentStreamFixingDateGrpElements PaymentStreamFixingDateGrpElements_;
			PaymentStreamFixingDateGrpAttributes PaymentStreamFixingDateGrpAttributes_;

};

struct PaymentStreamFormulaMathGrp_Block_t {
	PaymentStreamFormulaMathGrpAttributes PaymentStreamFormulaMathGrpAttributes_;

};

struct PaymentStreamFormula_Block_t {
	PaymentStreamFormulaElements PaymentStreamFormulaElements_;
			PaymentStreamFormulaAttributes PaymentStreamFormulaAttributes_;

};

struct PaymentStubEndDate_Block_t {
	PaymentStubEndDateElements PaymentStubEndDateElements_;
			PaymentStubEndDateAttributes PaymentStubEndDateAttributes_;

};

struct PaymentStubEndDateBusinessCenterGrp_Block_t {
	PaymentStubEndDateBusinessCenterGrpElements PaymentStubEndDateBusinessCenterGrpElements_;
			PaymentStubEndDateBusinessCenterGrpAttributes PaymentStubEndDateBusinessCenterGrpAttributes_;

};

struct PaymentStubStartDate_Block_t {
	PaymentStubStartDateElements PaymentStubStartDateElements_;
			PaymentStubStartDateAttributes PaymentStubStartDateAttributes_;

};

struct PaymentStubStartDateBusinessCenterGrp_Block_t {
	PaymentStubStartDateBusinessCenterGrpElements PaymentStubStartDateBusinessCenterGrpElements_;
			PaymentStubStartDateBusinessCenterGrpAttributes PaymentStubStartDateBusinessCenterGrpAttributes_;

};

struct ReturnRateDateGrp_Block_t {
	ReturnRateDateGrpElements ReturnRateDateGrpElements_;
			ReturnRateDateGrpAttributes ReturnRateDateGrpAttributes_;

};

struct ReturnRateFXConversionGrp_Block_t {
	ReturnRateFXConversionGrpElements ReturnRateFXConversionGrpElements_;
			ReturnRateFXConversionGrpAttributes ReturnRateFXConversionGrpAttributes_;

};

struct ReturnRateGrp_Block_t {
	ReturnRateGrpElements ReturnRateGrpElements_;
			ReturnRateGrpAttributes ReturnRateGrpAttributes_;

};

struct ReturnRateInformationSourceGrp_Block_t {
	ReturnRateInformationSourceGrpElements ReturnRateInformationSourceGrpElements_;
			ReturnRateInformationSourceGrpAttributes ReturnRateInformationSourceGrpAttributes_;

};

struct ReturnRatePriceGrp_Block_t {
	ReturnRatePriceGrpElements ReturnRatePriceGrpElements_;
			ReturnRatePriceGrpAttributes ReturnRatePriceGrpAttributes_;

};

struct ReturnRateValuationDateBusinessCenterGrp_Block_t {
	ReturnRateValuationDateBusinessCenterGrpElements ReturnRateValuationDateBusinessCenterGrpElements_;
			ReturnRateValuationDateBusinessCenterGrpAttributes ReturnRateValuationDateBusinessCenterGrpAttributes_;

};

struct ReturnRateValuationDateGrp_Block_t {
	ReturnRateValuationDateGrpElements ReturnRateValuationDateGrpElements_;
			ReturnRateValuationDateGrpAttributes ReturnRateValuationDateGrpAttributes_;

};

struct SettlMethodElectionDateBusinessCenterGrp_Block_t {
	SettlMethodElectionDateBusinessCenterGrpElements SettlMethodElectionDateBusinessCenterGrpElements_;
			SettlMethodElectionDateBusinessCenterGrpAttributes SettlMethodElectionDateBusinessCenterGrpAttributes_;

};

struct SettlMethodElectionDate_Block_t {
	SettlMethodElectionDateElements SettlMethodElectionDateElements_;
			SettlMethodElectionDateAttributes SettlMethodElectionDateAttributes_;

};

struct UnderlyingCashSettlDateBusinessCenterGrp_Block_t {
	UnderlyingCashSettlDateBusinessCenterGrpElements UnderlyingCashSettlDateBusinessCenterGrpElements_;
			UnderlyingCashSettlDateBusinessCenterGrpAttributes UnderlyingCashSettlDateBusinessCenterGrpAttributes_;

};

struct UnderlyingCashSettlDate_Block_t {
	UnderlyingCashSettlDateElements UnderlyingCashSettlDateElements_;
			UnderlyingCashSettlDateAttributes UnderlyingCashSettlDateAttributes_;

};

struct UnderlyingDividendAccrualPaymentDateBusinessCenterGrp_Block_t {
	UnderlyingDividendAccrualPaymentDateBusinessCenterGrpElements UnderlyingDividendAccrualPaymentDateBusinessCenterGrpElements_;
			UnderlyingDividendAccrualPaymentDateBusinessCenterGrpAttributes UnderlyingDividendAccrualPaymentDateBusinessCenterGrpAttributes_;

};

struct UnderlyingDividendAccrualFloatingRate_Block_t {
	UnderlyingDividendAccrualFloatingRateElements UnderlyingDividendAccrualFloatingRateElements_;
			UnderlyingDividendAccrualFloatingRateAttributes UnderlyingDividendAccrualFloatingRateAttributes_;

};

struct UnderlyingDividendAccrualPaymentDate_Block_t {
	UnderlyingDividendAccrualPaymentDateElements UnderlyingDividendAccrualPaymentDateElements_;
			UnderlyingDividendAccrualPaymentDateAttributes UnderlyingDividendAccrualPaymentDateAttributes_;

};

struct UnderlyingDividendConditions_Block_t {
	UnderlyingDividendConditionsElements UnderlyingDividendConditionsElements_;
			UnderlyingDividendConditionsAttributes UnderlyingDividendConditionsAttributes_;

};

struct UnderlyingDividendFXTriggerDate_Block_t {
	UnderlyingDividendFXTriggerDateElements UnderlyingDividendFXTriggerDateElements_;
			UnderlyingDividendFXTriggerDateAttributes UnderlyingDividendFXTriggerDateAttributes_;

};

struct UnderlyingDividendFXTriggerDateBusinessCenterGrp_Block_t {
	UnderlyingDividendFXTriggerDateBusinessCenterGrpElements UnderlyingDividendFXTriggerDateBusinessCenterGrpElements_;
			UnderlyingDividendFXTriggerDateBusinessCenterGrpAttributes UnderlyingDividendFXTriggerDateBusinessCenterGrpAttributes_;

};

struct UnderlyingDividendPaymentGrp_Block_t {
	UnderlyingDividendPaymentGrpElements UnderlyingDividendPaymentGrpElements_;
			UnderlyingDividendPaymentGrpAttributes UnderlyingDividendPaymentGrpAttributes_;

};

struct UnderlyingDividendPayout_Block_t {
	UnderlyingDividendPayoutElements UnderlyingDividendPayoutElements_;
			UnderlyingDividendPayoutAttributes UnderlyingDividendPayoutAttributes_;

};

struct UnderlyingDividendPeriodGrp_Block_t {
	UnderlyingDividendPeriodGrpElements UnderlyingDividendPeriodGrpElements_;
			UnderlyingDividendPeriodGrpAttributes UnderlyingDividendPeriodGrpAttributes_;

};

struct UnderlyingDividendPeriodBusinessCenterGrp_Block_t {
	UnderlyingDividendPeriodBusinessCenterGrpElements UnderlyingDividendPeriodBusinessCenterGrpElements_;
			UnderlyingDividendPeriodBusinessCenterGrpAttributes UnderlyingDividendPeriodBusinessCenterGrpAttributes_;

};

struct UnderlyingExtraordinaryEventGrp_Block_t {
	UnderlyingExtraordinaryEventGrpElements UnderlyingExtraordinaryEventGrpElements_;
			UnderlyingExtraordinaryEventGrpAttributes UnderlyingExtraordinaryEventGrpAttributes_;

};

struct UnderlyingOptionExerciseMakeWholeProvision_Block_t {
	UnderlyingOptionExerciseMakeWholeProvisionElements UnderlyingOptionExerciseMakeWholeProvisionElements_;
			UnderlyingOptionExerciseMakeWholeProvisionAttributes UnderlyingOptionExerciseMakeWholeProvisionAttributes_;

};

struct UnderlyingPaymentStreamCompoundingDateGrp_Block_t {
	UnderlyingPaymentStreamCompoundingDateGrpElements UnderlyingPaymentStreamCompoundingDateGrpElements_;
			UnderlyingPaymentStreamCompoundingDateGrpAttributes UnderlyingPaymentStreamCompoundingDateGrpAttributes_;

};

struct UnderlyingPaymentStreamCompoundingDates_Block_t {
	UnderlyingPaymentStreamCompoundingDatesElements UnderlyingPaymentStreamCompoundingDatesElements_;
			UnderlyingPaymentStreamCompoundingDatesAttributes UnderlyingPaymentStreamCompoundingDatesAttributes_;

};

struct UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrp_Block_t {
	UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpElements UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpElements_;
			UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpAttributes UnderlyingPaymentStreamCompoundingDatesBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStreamCompoundingEndDate_Block_t {
	UnderlyingPaymentStreamCompoundingEndDateElements UnderlyingPaymentStreamCompoundingEndDateElements_;
			UnderlyingPaymentStreamCompoundingEndDateAttributes UnderlyingPaymentStreamCompoundingEndDateAttributes_;

};

struct UnderlyingPaymentStreamCompoundingFloatingRate_Block_t {
	UnderlyingPaymentStreamCompoundingFloatingRateElements UnderlyingPaymentStreamCompoundingFloatingRateElements_;
			UnderlyingPaymentStreamCompoundingFloatingRateAttributes UnderlyingPaymentStreamCompoundingFloatingRateAttributes_;

};

struct UnderlyingPaymentStreamCompoundingStartDate_Block_t {
	UnderlyingPaymentStreamCompoundingStartDateElements UnderlyingPaymentStreamCompoundingStartDateElements_;
			UnderlyingPaymentStreamCompoundingStartDateAttributes UnderlyingPaymentStreamCompoundingStartDateAttributes_;

};

struct UnderlyingPaymentStreamFormulaImage_Block_t {
	UnderlyingPaymentStreamFormulaImageElements UnderlyingPaymentStreamFormulaImageElements_;
			UnderlyingPaymentStreamFormulaImageAttributes UnderlyingPaymentStreamFormulaImageAttributes_;

};

struct UnderlyingPaymentStreamFinalPricePaymentDate_Block_t {
	UnderlyingPaymentStreamFinalPricePaymentDateElements UnderlyingPaymentStreamFinalPricePaymentDateElements_;
			UnderlyingPaymentStreamFinalPricePaymentDateAttributes UnderlyingPaymentStreamFinalPricePaymentDateAttributes_;

};

struct UnderlyingPaymentStreamFixingDateGrp_Block_t {
	UnderlyingPaymentStreamFixingDateGrpElements UnderlyingPaymentStreamFixingDateGrpElements_;
			UnderlyingPaymentStreamFixingDateGrpAttributes UnderlyingPaymentStreamFixingDateGrpAttributes_;

};

struct UnderlyingPaymentStreamFormula_Block_t {
	UnderlyingPaymentStreamFormulaElements UnderlyingPaymentStreamFormulaElements_;
			UnderlyingPaymentStreamFormulaAttributes UnderlyingPaymentStreamFormulaAttributes_;

};

struct UnderlyingPaymentStreamFormulaMathGrp_Block_t {
	UnderlyingPaymentStreamFormulaMathGrpAttributes UnderlyingPaymentStreamFormulaMathGrpAttributes_;

};

struct UnderlyingPaymentStubEndDate_Block_t {
	UnderlyingPaymentStubEndDateElements UnderlyingPaymentStubEndDateElements_;
			UnderlyingPaymentStubEndDateAttributes UnderlyingPaymentStubEndDateAttributes_;

};

struct UnderlyingPaymentStubEndDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStubEndDateBusinessCenterGrpElements UnderlyingPaymentStubEndDateBusinessCenterGrpElements_;
			UnderlyingPaymentStubEndDateBusinessCenterGrpAttributes UnderlyingPaymentStubEndDateBusinessCenterGrpAttributes_;

};

struct UnderlyingPaymentStubStartDate_Block_t {
	UnderlyingPaymentStubStartDateElements UnderlyingPaymentStubStartDateElements_;
			UnderlyingPaymentStubStartDateAttributes UnderlyingPaymentStubStartDateAttributes_;

};

struct UnderlyingPaymentStubStartDateBusinessCenterGrp_Block_t {
	UnderlyingPaymentStubStartDateBusinessCenterGrpElements UnderlyingPaymentStubStartDateBusinessCenterGrpElements_;
			UnderlyingPaymentStubStartDateBusinessCenterGrpAttributes UnderlyingPaymentStubStartDateBusinessCenterGrpAttributes_;

};

struct UnderlyingRateSpreadSchedule_Block_t {
	UnderlyingRateSpreadScheduleElements UnderlyingRateSpreadScheduleElements_;
			UnderlyingRateSpreadScheduleAttributes UnderlyingRateSpreadScheduleAttributes_;

};

struct UnderlyingRateSpreadStepGrp_Block_t {
	UnderlyingRateSpreadStepGrpElements UnderlyingRateSpreadStepGrpElements_;
			UnderlyingRateSpreadStepGrpAttributes UnderlyingRateSpreadStepGrpAttributes_;

};

struct UnderlyingReturnRateDateGrp_Block_t {
	UnderlyingReturnRateDateGrpElements UnderlyingReturnRateDateGrpElements_;
			UnderlyingReturnRateDateGrpAttributes UnderlyingReturnRateDateGrpAttributes_;

};

struct UnderlyingReturnRateFXConversionGrp_Block_t {
	UnderlyingReturnRateFXConversionGrpElements UnderlyingReturnRateFXConversionGrpElements_;
			UnderlyingReturnRateFXConversionGrpAttributes UnderlyingReturnRateFXConversionGrpAttributes_;

};

struct UnderlyingReturnRateGrp_Block_t {
	UnderlyingReturnRateGrpElements UnderlyingReturnRateGrpElements_;
			UnderlyingReturnRateGrpAttributes UnderlyingReturnRateGrpAttributes_;

};

struct UnderlyingReturnRateInformationSourceGrp_Block_t {
	UnderlyingReturnRateInformationSourceGrpElements UnderlyingReturnRateInformationSourceGrpElements_;
			UnderlyingReturnRateInformationSourceGrpAttributes UnderlyingReturnRateInformationSourceGrpAttributes_;

};

struct UnderlyingReturnRatePriceGrp_Block_t {
	UnderlyingReturnRatePriceGrpElements UnderlyingReturnRatePriceGrpElements_;
			UnderlyingReturnRatePriceGrpAttributes UnderlyingReturnRatePriceGrpAttributes_;

};

struct UnderlyingReturnRateValuationDateBusinessCenterGrp_Block_t {
	UnderlyingReturnRateValuationDateBusinessCenterGrpElements UnderlyingReturnRateValuationDateBusinessCenterGrpElements_;
			UnderlyingReturnRateValuationDateBusinessCenterGrpAttributes UnderlyingReturnRateValuationDateBusinessCenterGrpAttributes_;

};

struct UnderlyingReturnRateValuationDateGrp_Block_t {
	UnderlyingReturnRateValuationDateGrpElements UnderlyingReturnRateValuationDateGrpElements_;
			UnderlyingReturnRateValuationDateGrpAttributes UnderlyingReturnRateValuationDateGrpAttributes_;

};

struct UnderlyingSettlMethodElectionDateBusinessCenterGrp_Block_t {
	UnderlyingSettlMethodElectionDateBusinessCenterGrpElements UnderlyingSettlMethodElectionDateBusinessCenterGrpElements_;
			UnderlyingSettlMethodElectionDateBusinessCenterGrpAttributes UnderlyingSettlMethodElectionDateBusinessCenterGrpAttributes_;

};

struct UnderlyingSettlMethodElectionDate_Block_t {
	UnderlyingSettlMethodElectionDateElements UnderlyingSettlMethodElectionDateElements_;
			UnderlyingSettlMethodElectionDateAttributes UnderlyingSettlMethodElectionDateAttributes_;

};

struct TrdRegPublicationGrp_Block_t {
	TrdRegPublicationGrpElements TrdRegPublicationGrpElements_;
			TrdRegPublicationGrpAttributes TrdRegPublicationGrpAttributes_;

};

struct OrderAttributeGrp_Block_t {
	OrderAttributeGrpElements OrderAttributeGrpElements_;
			OrderAttributeGrpAttributes OrderAttributeGrpAttributes_;

};

struct SideCollateralAmountGrp_Block_t {
	SideCollateralAmountGrpElements SideCollateralAmountGrpElements_;
			SideCollateralAmountGrpAttributes SideCollateralAmountGrpAttributes_;

};

struct QuoteAttributeGrp_Block_t {
	QuoteAttributeGrpElements QuoteAttributeGrpElements_;
			QuoteAttributeGrpAttributes QuoteAttributeGrpAttributes_;

};

struct PriceQualifierGrp_Block_t {
	PriceQualifierGrpElements PriceQualifierGrpElements_;
			PriceQualifierGrpAttributes PriceQualifierGrpAttributes_;

};

struct IndexRollMonthGrp_Block_t {
	IndexRollMonthGrpElements IndexRollMonthGrpElements_;
			IndexRollMonthGrpAttributes IndexRollMonthGrpAttributes_;

};

struct ReferenceDataDateGrp_Block_t {
	ReferenceDataDateGrpElements ReferenceDataDateGrpElements_;
			ReferenceDataDateGrpAttributes ReferenceDataDateGrpAttributes_;

};

struct FloatingRateIndex_Block_t {
	FloatingRateIndexElements FloatingRateIndexElements_;
			FloatingRateIndexAttributes FloatingRateIndexAttributes_;

};

struct AveragePriceDetail_Block_t {
	AveragePriceDetailElements AveragePriceDetailElements_;
			AveragePriceDetailAttributes AveragePriceDetailAttributes_;

};

struct MatchExceptionGrp_Block_t {
	MatchExceptionGrpElements MatchExceptionGrpElements_;
			MatchExceptionGrpAttributes MatchExceptionGrpAttributes_;

};

struct MatchingDataPointGrp_Block_t {
	MatchingDataPointGrpElements MatchingDataPointGrpElements_;
			MatchingDataPointGrpAttributes MatchingDataPointGrpAttributes_;

};

struct OrderAggregationGrp_Block_t {
	OrderAggregationGrpElements OrderAggregationGrpElements_;
			OrderAggregationGrpAttributes OrderAggregationGrpAttributes_;

};

struct ExecutionAggregationGrp_Block_t {
	ExecutionAggregationGrpElements ExecutionAggregationGrpElements_;
			ExecutionAggregationGrpAttributes ExecutionAggregationGrpAttributes_;

};

using Message=Abstract_message_t;

namespace FIXML_COMPONENTS_BASE {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_FIXML_COMPONENTS_BASE_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif
