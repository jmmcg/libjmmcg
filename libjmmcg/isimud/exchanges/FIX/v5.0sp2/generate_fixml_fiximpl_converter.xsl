<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output omit-xml-declaration="yes" />
<xsl:param name="fixml_base_hdr" required="yes" as="xs:string"/>
<xsl:param name="fix_version" required="yes" as="xs:string"/>
<xsl:include href="generate_bits.xsl"/>
<xsl:template name="make_using_stmts">
	<xsl:param name="FIX_type"/>
	<xsl:choose>
		<xsl:when test="xs:restriction">
			<xsl:value-of select="xs:restriction/@base"/>
		</xsl:when>
		<xsl:when test="xs:union">
			<xsl:value-of select="tokenize(xs:union/@memberTypes, ' ')[1]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>FIXME</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="/">
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso20022.org/10383/iso-10383-market-identifier-code"/>
-->
#include "<xsl:value-of select="$fixml_base_hdr"/>"

#define ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "<xsl:value-of select="current-dateTime()"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace <xsl:value-of select="$fix_version"/> {

<xsl:for-each select="/xs:schema/xs:simpleType">
using <xsl:value-of select="@name"/>=<xsl:call-template name="make_using_stmts">
		<xsl:with-param name="FIX_type" select="."/>
	</xsl:call-template>;
</xsl:for-each>
namespace <xsl:call-template name="base_filename"/> {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
