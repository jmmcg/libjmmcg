<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
	<!ENTITY tab "<xsl:text>&#x9;</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fm="http://www.fixprotocol.org/FIXML-5-0-SP2/METADATA">
<xsl:output omit-xml-declaration="yes" />
<xsl:param name="fixml_impl_hdr" required="yes" as="xs:string"/>
<xsl:param name="fields_fast_hdr" required="yes" as="xs:string"/>
<xsl:param name="fix_version" required="yes" as="xs:string"/>
<xsl:include href="generate_bits.xsl"/>
<xsl:template name="write-type-elements">
<xsl:param name="elements" required="yes"/>
<xsl:choose>
	<xsl:when test="$elements/xs:sequence/xs:element">
		<xsl:for-each select="$elements/xs:sequence/xs:element">
&tab;<xsl:value-of select="@type"/><xsl:text> </xsl:text><xsl:value-of select="@name"/>;
		</xsl:for-each>
	</xsl:when>
	<xsl:otherwise>
		<xsl:for-each select="$elements/xs:sequence/xs:group">
&tab;<xsl:value-of select="@ref"/><xsl:text> </xsl:text><xsl:value-of select="@ref"/>_;
		</xsl:for-each>
	</xsl:otherwise>
</xsl:choose>
<xsl:if test="$elements/xs:attributeGroup/@ref">
&tab;<xsl:value-of select="$elements/xs:attributeGroup/@ref"/><xsl:text> </xsl:text><xsl:value-of select="$elements/xs:attributeGroup/@ref"/>_;
</xsl:if>
</xsl:template>
<xsl:template name="write-attrGroup-elements">
	<xsl:param name="group" required="yes"/>
struct <xsl:value-of select="@name"/> <xsl:if test="xs:extension/@base">: public <xsl:value-of select="xs:extension/@base"/>  </xsl:if> {
<xsl:if test="@name='BaseHeaderElements'">
	const auto BodyLengthTag=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>BodyLength<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	const auto MsgTypeTag=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text>MsgType<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	MsgType_t MsgType_;
</xsl:if>
	<xsl:for-each select="$group/xs:attribute[@use='required']">
&tab;<xsl:value-of select="@type"/><xsl:text> </xsl:text><xsl:value-of select="@name"/>;
	</xsl:for-each>
};
</xsl:template>
<xsl:template match="/">
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso20022.org/10383/iso-10383-market-identifier-code"/>
-->
#include "<xsl:value-of select="$fields_fast_hdr"/>"
#include "<xsl:value-of select="$fixml_impl_hdr"/>"

#define ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "<xsl:value-of select="current-dateTime()"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace <xsl:value-of select="$fix_version"/> {
<xsl:for-each select="/xs:schema/xs:group">
<xsl:call-template name="write-attrGroup-elements">
<xsl:with-param name="group" select="."/>
</xsl:call-template>
</xsl:for-each>
<xsl:for-each select="/xs:schema/xs:attributeGroup">
<xsl:call-template name="write-attrGroup-elements">
<xsl:with-param name="group" select="."/>
</xsl:call-template>
</xsl:for-each>
<xsl:for-each select="/xs:schema/xs:complexType">
struct <xsl:value-of select="@name"/> <xsl:if test="xs:complexContent/xs:extension/@base">: public <xsl:value-of select="xs:complexContent/xs:extension/@base"/></xsl:if> {
<xsl:choose>
	<xsl:when test="xs:complexContent/xs:extension">
		<xsl:call-template name="write-type-elements">
		<xsl:with-param name="elements" select="xs:complexContent/xs:extension"/>
		</xsl:call-template>
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="write-type-elements">
		<xsl:with-param name="elements" select="."/>
		</xsl:call-template>
	</xsl:otherwise>
</xsl:choose>
};
</xsl:for-each>
<xsl:for-each select="/xs:schema/xs:element[@type]">
using <xsl:value-of select="@name"/>=<xsl:value-of select="@type"/>;
</xsl:for-each>
namespace <xsl:call-template name="base_filename"/> {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
