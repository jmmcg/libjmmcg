/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_client.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <chrono>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

fix_client::fix_client(boost::asio::ip::address const& addr, unsigned short port_num, libjmmcg::socket::socket_priority priority, std::size_t incoming_cpu, libjmmcg::latency_timestamps_itf& ts) noexcept(false)
	: base_t(),
	  client(
		  exchanges::FIX::v5_0sp2::connection_t::conn_pol_t(
			  typename exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::gateways_t(
				  std::make_pair(addr, port_num))),
		  priority,
		  incoming_cpu),
	  timestamps(ts) {
	DEBUG_ASSERT(as_fix_msg_type.is_valid());
}

std::pair<fix_client::timed_results_t, bool>
fix_client::in_order_tx_rx_stats(unsigned long num_loops, unsigned short loops_for_conv, double perc_conv_estimate) noexcept(false) {
	const std::pair<timed_results_t, bool> timed_results(libjmmcg::compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[this, &num_loops]() {
			if(!signal_status()) {
				const auto t1= std::chrono::high_resolution_clock::now();
				in_order_tx_rx(num_loops);
				const auto t2= std::chrono::high_resolution_clock::now();
				DEBUG_ASSERT(receive_fix_msg.is_valid());
				return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / static_cast<double>(num_loops));
			} else {
				return timed_results_t::value_type();
			}
		}));
	return timed_results;
}

std::ostream&
operator<<(std::ostream& os, fix_client const& fc) noexcept(false) {
	os
		<< static_cast<fix_client::base_t const&>(fc)
		<< ", FIX message to send: " << fc.fix_buffer
		<< ", expected response: " << boost::core::demangle(typeid(fc.receive_fix_msg).name())
		<< ", client connection details: " << fc.client;
	return os;
}

}}}}}
