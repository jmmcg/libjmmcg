#ifndef ISIMUD_EXCHANGES_FIX_5_0sp2_types_hpp
#define ISIMUD_EXCHANGES_FIX_5_0sp2_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../common/types.hpp"

#include "common/iso_3166_country_codes.hpp"
#include "common/iso_4217_currency_codes.hpp"

#include "fixml-fields-fast-v5_0sp2.hpp"
#include "fixml-fields-impl-5-0-SP2.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX {

namespace common {

using ExecType= v5_0sp2::ExecType_t;
using FieldsFast= v5_0sp2::FieldsFast;
using MsgType_t= v5_0sp2::MsgType_enum_t_ul_t;
using MsgType= v5_0sp2::MsgType_t;
using OrdStatus= v5_0sp2::OrdStatus_t;
using Price_t= v5_0sp2::Price_t;
using Quantity_t= v5_0sp2::Quantity_t;
using SecurityIDSource= v5_0sp2::SecurityIDSource_t;
using Side= v5_0sp2::Side_t;
using TIF= v5_0sp2::TimeInForce_t;
using OrderType= v5_0sp2::OrdType_t;
using RejectCode_t= v5_0sp2::RejectCode_t;
using SeqNum_t= v5_0sp2::SeqNum;
using ExDestinationIDSource= v5_0sp2::ExDestinationIDSource_t;
using ExDestination_t= v5_0sp2::ExDestination_t;
using v5_0sp2::shortest_field_length;

}

namespace v5_0sp2 {

#define JMMCG_FIX_v5_0sp2_MSG_VER "8=FIX.5.0"
inline constexpr const char MsgVersion[]= JMMCG_FIX_v5_0sp2_MSG_VER;

namespace test {

BOOST_MPL_ASSERT_RELATION((libjmmcg::enum_tags::mpl::to_array<FieldsFast::BodyLength>::size), ==, sizeof(JMMCG_FIX_MSG_BODY_LENGTH_TAG) - 1);
BOOST_MPL_ASSERT_RELATION((libjmmcg::enum_tags::mpl::to_array<FieldsFast::MsgType>::size), >=, sizeof(JMMCG_FIX_MSG_TYPE_TAG) - 1);

}

}

}}}}

#endif
