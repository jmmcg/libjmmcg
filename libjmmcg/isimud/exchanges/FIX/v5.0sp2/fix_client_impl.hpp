/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

inline void
fix_client::send() noexcept(false) {
	client.send(as_fix_msg_type);
}

inline bool
fix_client::receive() noexcept(false) {
	return client.receive(receive_fix_msg);
}

inline void
fix_client::in_order_tx_rx(unsigned long num_messages) noexcept(false) {
	for(unsigned long i= 0; i < num_messages; ++i) {
		const libjmmcg::latency_timestamps::period t(timestamps);
		send();
		if(receive()) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to send the {}-th message of {}.", i, num_messages), &fix_client::in_order_tx_rx));
		}
	}
}

}}}}}
