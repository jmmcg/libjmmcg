<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->
<!DOCTYPE xsl:stylesheet [
        <!ENTITY nl "<xsl:text>
</xsl:text>">
        ]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:include href="generate_bits.xsl"/>
    <xsl:param name="country_codes_hdr" required="yes" as="xs:string"/>
    <xsl:param name="currency_codes_hdr" required="yes" as="xs:string"/>
    <xsl:param name="mic_codes_hdr" required="yes" as="xs:string"/>
    <xsl:param name="fix_version" required="yes" as="xs:string"/>
    <xsl:template name="to_cpp_builtin_type">
        <xsl:param name="FIX_name"/>
        <xsl:param name="FIX_type"/>
        <xsl:choose>
            <xsl:when test="$FIX_name='char'">
                <xsl:text>char</xsl:text>
            </xsl:when>
            <xsl:when test="$FIX_name='Boolean'">
                <xsl:text>private_::boolean_type</xsl:text>
            </xsl:when>
            <xsl:when test="$FIX_name='Country'">
                <xsl:text>exchanges::common::ctry_codes::alpha_2::ISO_3166_Country_Codes</xsl:text>
            </xsl:when>
            <xsl:when test="$FIX_name='Currency'">
                <xsl:text>exchanges::common::ccy_codes::ISO_4217_Currency_Codes</xsl:text>
            </xsl:when>
            <xsl:when test="$FIX_name='Exchange'">
                <xsl:text>exchanges::common::mic_codes::ISO_10383_MIC_Codes</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$FIX_type='xs:nonNegativeInteger'">
                        <xsl:text>unsigned long</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:positiveInteger'">
                        <xsl:text>unsigned long</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:integer'">
                        <xsl:text>long</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:decimal'">
                        <xsl:text>double</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:dateTime'">
                        <xsl:text>std::string</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:time'">
                        <xsl:text>std::string</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:date'">
                        <xsl:text>std::array</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text>char, 8</xsl:text><xsl:text
                            disable-output-escaping="yes">&gt;</xsl:text>
                    </xsl:when>
                    <xsl:when test="$FIX_type='xs:string'">
                        <xsl:text>std::string</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>FIXME</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="/">
        #ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP
        #define ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP

        /******************************************************************************
        ** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
        **
        ** This library is free software; you can redistribute it and/or
        ** modify it under the terms of the GNU Lesser General Public
        ** License as published by the Free Software Foundation; either
        ** version 2.1 of the License, or (at your option) any later version.
        **
        ** This library is distributed in the hope that it will be useful,
        ** but WITHOUT ANY WARRANTY; without even the implied warranty of
        ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
        ** Lesser General Public License for more details.
        **
        ** You should have received a copy of the GNU Lesser General Public
        ** License along with this library; if not, write to the Free Software
        ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
        */

        // Auto-generated header file.
        // DO NOT EDIT. IT WILL BE OVERWRITTEN.
        // Generated:
        <xsl:value-of select="current-dateTime()"/>
        <!--
            Developed using:
            - <a href="http://www.freeformatter.com/xsl-transformer.html"/>
            - <a href="https://xslttest.appspot.com"/>
            - <a href="https://www.iso20022.org/10383/iso-10383-market-identifier-code"/>
        -->
        #include "../../common/<xsl:value-of select="$country_codes_hdr"/>"
        #include "../../common/<xsl:value-of select="$currency_codes_hdr"/>"
        #include "../../common/<xsl:value-of select="$mic_codes_hdr"/>"

        #include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>array<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        #include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

        #define ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "<xsl:value-of
            select="current-dateTime()"/>"

        namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace
        <xsl:value-of select="$fix_version"/> {

        namespace private_ {

        enum boolean_type : char {
        boolean_true='Y',
        boolean_false='N'
        };

        }

        <xsl:for-each select="/xs:schema/xs:simpleType">
            <xsl:if test="@name ne 'char'">
                ///
                <xsl:value-of select="tokenize(xs:annotation/xs:documentation, '\n')[normalize-space()][1]"/>
                /**
                <xsl:value-of select="xs:annotation/xs:documentation"/>
                */
                using <xsl:value-of select="@name"/>=
                <xsl:call-template name="to_cpp_builtin_type">
                    <xsl:with-param name="FIX_name" select="@name"/>
                    <xsl:with-param name="FIX_type" select="xs:restriction/@base"/>
                </xsl:call-template>
                ;
            </xsl:if>
        </xsl:for-each>

        namespace
        <xsl:call-template name="base_filename"/>
        {

        inline std::string
        to_string() noexcept(false) {
        return std::string("Built with FIX data-types generated on: '" ISIMUD_<xsl:call-template name="base_filename"/>
        _HDR_GENERATED_DATE "'.");
        }

        }

        } } } } }

        #endif&nl;
    </xsl:template>
</xsl:stylesheet>
