#ifndef ISIMUD_EXCHANGES_FIX_5_0sp2_MESSAGES_HPP
#define ISIMUD_EXCHANGES_FIX_5_0sp2_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_v5_0sp2_config.h"
#include "types.hpp"

#include "../common/messages.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iosfwd>

/**
	FIX documentation:
	-# <a href="https://www.fixtrading.org/standards/fix-5-0-sp-2/">"FIX 5.0 Specification Service Pack 2"</a>..
	-# <a href="https://github.com/jamesdbrock/hffix/commit/d2539d5602672e596c0bb34e1fe642918df7b14f">Usge of the XML specification to generate code</a>.

	Note that Wireshark has a built-in FIX protocol: see the "Analyze/Enabled Protocols..." dialog in Wireshark. See <a href="https://www.wireshark.org/docs/dfref/f/fix.html"/> also.
*/
namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

struct VersionSpecific {
	static inline constexpr const common::underlying_fix_data_buffer::value_type fix_template_to_msg_type[]= JMMCG_FIX_v5_0sp2_MSG_VER JMMCG_FIX_MSG_BODY_LENGTH_TAG JMMCG_FIX_MSG_BODY_LENGTH_NULL JMMCG_FIX_MSG_TYPE_TAG;
	enum : std::size_t {
		fix_template_body_length_offset= sizeof(JMMCG_FIX_v5_0sp2_MSG_VER) - 1 + libjmmcg::enum_tags::mpl::to_array<common::FieldsFast::BodyLength>::size,
		fix_template_msg_type_offset= sizeof(fix_template_to_msg_type) - 1
	};
	static inline constexpr const char MsgVer[]= JMMCG_FIX_v5_0sp2_MSG_VER;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static constexpr bool is_valid(Msg const& m) noexcept(true);
};

struct LogonSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Logon>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct LogoutRequestSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Logout>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct HeartbeatSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Heartbeat>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	[[gnu::pure]] static bool is_valid(Msg const& m) noexcept(true);
};

struct ServerHeartbeat final : common::Message<HeartbeatSpecific> {
	using base_t= common::Message<HeartbeatSpecific>;

	ServerHeartbeat()
		: base_t() {
		pointer data= this->set_header<common::MsgType::Heartbeat>();
		this->finalise_msg(data);
	}
};

struct NewOrderSingleSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::NewOrderSingle>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderCancelRequestSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::OrderCancelRequest>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderCancelReplaceSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::OrderCancelReplaceRequest>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct ExecutionReportSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct LogonReplySpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Logon>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct LogoutSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Logout>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct CancelOrderSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::OrderCancelRequest>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct BusinessMessageRejectSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::BusinessMessageReject>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct ModifyOrderSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::OrderCancelReplaceRequest>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct TradeCaptureReportSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::TradeCaptureReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct TradeCaptureReportRequestSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::TradeCaptureReportRequest>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderAcknowledgementSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionAck>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderRejectedSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Reject>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderModifiedSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderRestatedSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct UserModifyRejectedSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Reject>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderCancelledSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct CancelRejectedSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::OrderCancelReject>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct OrderExecutionSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::ExecutionReport>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct TradeCaptureReportAckSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::TradeCaptureReportAck>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct TradeCaptureReportRequestAckSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::TradeCaptureReportRequestAck>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

struct RejectSpecific final : public VersionSpecific {
	using msg_type= libjmmcg::enum_tags::mpl::to_array<common::MsgType::Reject>;

	/// Verify that the referenced FIX message is valid.
	/**
		\param	m	A wrapper for the FIX message,
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	template<class Msg>
	static bool is_valid(Msg const& m) noexcept(true);
};

/**
	I expect that the FIX message will be constructible using accessors, but also via placement-new or a reference to the data-block. I also expect that a FIX message can be constructed from BATS or MIT messages. Also BATS & MIT messages will be constructible from FIX messages.
*/
struct MsgTypes {
	struct ref_data {};
	using isin_mapping_data_const_reference= ref_data const&;
	using FieldsFast= common::FieldsFast;

	using MsgType_t= common::MsgType_t;

	using MsgTypes_t= common::MsgType;

	using UserName_t= common::UserName_t;
	using Password_t= common::Password_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= common::Quantity_t;
	using Side= common::Side;
	using ClientOrderID_t= common::ClientOrderID_t;
	using SecurityID_t= common::SecurityID_t;
	using ExecType= common::ExecType;
	using TIF= common::TIF;
	using OrderType= common::OrderType;
	struct logon_args_t {};

	using LogonRequest_t= common::Message<LogonSpecific>;
	using Header_t= LogonRequest_t::Header_t;
	using LogoutRequest_t= common::Message<LogoutRequestSpecific>;
	using ClientHeartbeat_t= common::Message<HeartbeatSpecific>;
	using ServerHeartbeat_t= v5_0sp2::ServerHeartbeat;
	using NewOrderSingle_t= common::Message<NewOrderSingleSpecific>;
	using OrderCancelRequest_t= common::Message<OrderCancelRequestSpecific>;
	using OrderCancelReplace_t= common::Message<OrderCancelReplaceSpecific>;
	using ExecutionReport_t= common::Message<ExecutionReportSpecific>;
	using LogonReply_t= common::Message<LogonReplySpecific>;
	using Logout_t= common::Message<LogoutSpecific>;
	using CancelOrder_t= common::Message<CancelOrderSpecific>;
	using ModifyOrder_t= common::Message<ModifyOrderSpecific>;
	using TradeCaptureReport_t= common::Message<TradeCaptureReportSpecific>;
	using TradeCaptureReportRequest_t= common::Message<TradeCaptureReportRequestSpecific>;
	using OrderAcknowledgement_t= common::Message<OrderAcknowledgementSpecific>;
	using OrderRejected_t= common::Message<OrderRejectedSpecific>;
	using BusinessMessageReject_t= common::Message<BusinessMessageRejectSpecific>;
	using OrderModified_t= common::Message<OrderModifiedSpecific>;
	using OrderRestated_t= common::Message<OrderRestatedSpecific>;
	using UserModifyRejected_t= common::Message<UserModifyRejectedSpecific>;
	using OrderCancelled_t= common::Message<OrderCancelledSpecific>;
	using CancelRejected_t= common::Message<CancelRejectedSpecific>;
	using OrderExecution_t= common::Message<OrderExecutionSpecific>;
	using TradeCaptureReportAck_t= common::Message<TradeCaptureReportAckSpecific>;
	using TradeCaptureReportRequestAck_t= common::Message<TradeCaptureReportRequestAckSpecific>;
	using Reject_t= common::Message<RejectSpecific>;

	using BusinessReject_t= BusinessMessageReject_t;
	using NewOrder_t= NewOrderSingle_t;
	using OrderCancelReject_t= CancelRejected_t;
	using OrderCancelReplaceRequest_t= ModifyOrder_t;

	using client_to_exchange_messages_t= boost::mpl::vector<
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		NewOrderSingle_t,
		OrderCancelRequest_t,
		OrderCancelReplace_t,
		ModifyOrder_t,
		TradeCaptureReport_t,
		TradeCaptureReportRequest_t>;
	using exchange_to_client_messages_t= boost::mpl::vector<
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		OrderAcknowledgement_t,
		ExecutionReport_t,
		OrderRejected_t,
		BusinessMessageReject_t,
		OrderModified_t,
		OrderRestated_t,
		UserModifyRejected_t,
		OrderCancelled_t,
		CancelRejected_t,
		OrderExecution_t,
		TradeCaptureReportAck_t,
		TradeCaptureReportRequestAck_t,
		Reject_t>;
	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= Header_t::header_t_size
	};
	BOOST_MPL_ASSERT_RELATION(max_msg_size, >=, header_t_size);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using underlying_fix_data_buffer= common::underlying_fix_data_buffer;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const std::uint64_t implied_decimal_places= common::implied_decimal_places;

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test FIX v5.0sp2 size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrderSingle_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelRequest_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReplace_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ExecutionReport_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ModifyOrder_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TradeCaptureReport_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TradeCaptureReportRequest_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Logout_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderAcknowledgement_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderRejected_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::BusinessMessageReject_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderModified_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderRestated_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::UserModifyRejected_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelled_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::CancelRejected_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderExecution_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TradeCaptureReportAck_t), ==, 576);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TradeCaptureReportRequestAck_t), ==, 576);

}

}}}}}

#include "messages_impl.hpp"

#endif
