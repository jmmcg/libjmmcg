#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_DATATYPES_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_DATATYPES_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: 2020-03-11T03:21:26.04Z

#include "../../common/iso_3166_country_codes.hpp"
#include "../../common/iso_4217_currency_codes.hpp"
#include "../../common/iso_10383_mic_codes.hpp"

#include <array>
#include <string>

#define ISIMUD_FIXML_DATATYPES_HDR_GENERATED_DATE "2020-03-11T03:21:26.04Z"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

namespace private_ {

	enum boolean_type : char {
		boolean_true='Y',
		boolean_false='N'
	};

}


/// int field representing the length in bytes. Value must be positive.
/**
	int field representing the length in bytes. Value must be positive.
*/
using Length=unsigned long;
	
/// int field representing a field's tag number when using FIX "Tag=Value" syntax. Value must be positive and may not contain leading zeros.
/**
	int field representing a field's tag number when using FIX "Tag=Value" syntax. Value must be positive and may not contain leading zeros.
*/
using TagNum=unsigned long;
	
/// int field representing a message sequence number. Value must be positive.
/**
	int field representing a message sequence number. Value must be positive.
*/
using SeqNum=unsigned long;
	
/// float field capable of storing either a whole number (no decimal places) of "shares" (securities denominated in whole units) or a decimal value containing decimal places for non-share quantity asset classes (securities denominated in fractional units).
/**
	float field capable of storing either a whole number (no decimal places) of "shares" (securities denominated in whole units) or a decimal value containing decimal places for non-share quantity asset classes (securities denominated in fractional units).
*/
using Qty=double;
	
/// float field representing a price. Note the number of decimal places may vary. For certain asset classes prices may be negative values. For example, prices for options strategies can be negative under certain market conditions. Refer to Volume 7: FIX Usage by Product for asset classes that support negative price values.
/**
	float field representing a price. Note the number of decimal places may vary. For certain asset classes prices may be negative values. For example, prices for options strategies can be negative under certain market conditions. Refer to Volume 7: FIX Usage by Product for asset classes that support negative price values.
*/
using Price=double;
	
/// float field representing a price offset, which can be mathematically added to a "Price". Note the number of decimal places may vary and some fields such as LastForwardPoints may be negative.
/**
	float field representing a price offset, which can be mathematically added to a "Price". Note the number of decimal places may vary and some fields such as LastForwardPoints may be negative.
*/
using PriceOffset=double;
	
/// float field typically representing a Price times a Qty
/**
	float field typically representing a Price times a Qty
*/
using Amt=double;
	
/// float field representing a percentage (e.g. 0.05 represents 5% and 0.9525 represents 95.25%). Note the number of decimal places may vary.
/**
	float field representing a percentage (e.g. 0.05 represents 5% and 0.9525 represents 95.25%). Note the number of decimal places may vary.
*/
using Percentage=double;
	
/// char field containing one of two values:
/**
	char field containing one of two values:
'Y' = True/Yes
'N' = False/No
*/
using Boolean=private_::boolean_type;
	
/// string field containing one or more space delimited single character values (e.g. |18=2 A F| ).
/**
	string field containing one or more space delimited single character values (e.g. |18=2 A F| ).
*/
using MultipleCharValue=std::string;
	
/// string field containing one or more space delimited multiple character values (e.g. |277=AV AN A| ).
/**
	string field containing one or more space delimited multiple character values (e.g. |277=AV AN A| ).
*/
using MultipleStringValue=std::string;
	
/// string field representing a country using ISO 3166 Country code (2 character) values (see Appendix 6-B).
/**
	string field representing a country using ISO 3166 Country code (2 character) values (see Appendix 6-B).
*/
using Country= exchanges::common::ctry_codes::alpha_2::ISO_3166_Country_Codes;

/// string field representing a currency type using ISO 4217 Currency code (3 character) values (see Appendix 6-A).
/**
	string field representing a currency type using ISO 4217 Currency code (3 character) values (see Appendix 6-A).
*/
using Currency= exchanges::common::ccy_codes::ISO_4217_Currency_Codes;

/// string field representing a market or exchange using ISO 10383 Market Identifier Code (MIC) values (see"Appendix 6-C).
/**
	string field representing a market or exchange using ISO 10383 Market Identifier Code (MIC) values (see"Appendix 6-C).
*/
using Exchange= exchanges::common::mic_codes::ISO_10383_MIC_Codes;

/// string field representing month of a year. An optional day of the month can be appended or an optional week code.
/**
	string field representing month of a year. An optional day of the month can be appended or an optional week code.
Valid formats:
YYYYMM
YYYYMMDD
YYYYMMWW
Valid values:
YYYY = 0000-9999; MM = 01-12; DD = 01-31; WW = w1, w2, w3, w4, w5.
*/
using MonthYear=std::string;
	
/// string field representing time/date combination represented in UTC (Universal Time Coordinated, also known as "GMT") in either YYYYMMDD-HH:MM:SS (whole seconds) or YYYYMMDD-HH:MM:SS.sss* format, colons, dash, and period required.
/**
	string field representing time/date combination represented in UTC (Universal Time Coordinated, also known as "GMT") in either YYYYMMDD-HH:MM:SS (whole seconds) or YYYYMMDD-HH:MM:SS.sss* format, colons, dash, and period required.

Valid values:
YYYY = 0000-9999, MM = 01-12, DD = 01-31, HH = 00-23, MM = 00-59, SS = 00-60 (60 only if UTC leap second), sss* fractions of seconds.
The fractions of seconds may be empty when no fractions of seconds are conveyed (in such a case the period is not conveyed), it may include 3 digits to convey milliseconds, 6 digits to convey microseconds, 9 digits to convey nanoseconds, 12 digits to convey picoseconds; Other number of digits may be used with bilateral agreement.

Leap Seconds: Note that UTC includes corrections for leap seconds, which are inserted to account for slowing of the rotation of the earth. Leap second insertion is declared by the International Earth Rotation Service (IERS) and has, since 1972, only occurred on the night of Dec. 31 or Jun 30. The IERS considers March 31 and September 30 as secondary dates for leap second insertion, but has never utilized these dates. During a leap second insertion, a UTCTimestamp field may read "19981231-23:59:59", "19981231-23:59:60", "19990101-00:00:00". (see http://tycho.usno.navy.mil/leapsec.html)
*/
using UTCTimestamp=std::string;
	
/// string field representing time-only represented in UTC (Universal Time Coordinated, also known as "GMT") in either HH:MM:SS (whole seconds) or HH:MM:SS.sss* (milliseconds) format, colons, and period required. This special-purpose field is paired with UTCDateOnly to form a proper UTCTimestamp for bandwidth-sensitive messages.
/**
	string field representing time-only represented in UTC (Universal Time Coordinated, also known as "GMT") in either HH:MM:SS (whole seconds) or HH:MM:SS.sss* (milliseconds) format, colons, and period required. This special-purpose field is paired with UTCDateOnly to form a proper UTCTimestamp for bandwidth-sensitive messages.
Valid values:
HH = 00-23, MM = 00-59, SS = 00-60 (60 only if UTC leap second), sss* fractions of seconds. The fractions of seconds may be empty when no fractions of seconds are conveyed (in such a case the period is not conveyed), it may include 3 digits to convey milliseconds, 6 digits to convey microseconds, 9 digits to convey nanoseconds, 12 digits to convey picoseconds; Other number of digits may be used with bilateral agreement.
*/
using UTCTimeOnly=std::string;
	
/// string field representing Date represented in UTC (Universal Time Coordinated, also known as "GMT") in YYYYMMDD format. This special-purpose field is paired with UTCTimeOnly to form a proper UTCTimestamp for bandwidth-sensitive messages.
/**
	string field representing Date represented in UTC (Universal Time Coordinated, also known as "GMT") in YYYYMMDD format. This special-purpose field is paired with UTCTimeOnly to form a proper UTCTimestamp for bandwidth-sensitive messages.
Valid values:
YYYY = 0000-9999, MM = 01-12, DD = 01-31.
*/
using UTCDateOnly=std::array<char, 8>;
	
/// string field representing a Date of Local Market (as opposed to UTC) in YYYYMMDD format. This is the "normal" date field used by the FIX Protocol.
/**
	string field representing a Date of Local Market (as opposed to UTC) in YYYYMMDD format. This is the "normal" date field used by the FIX Protocol.

Valid values:
YYYY = 0000-9999, MM = 01-12, DD = 01-31

*/
using LocalMktDate=std::array<char, 8>;
	
/// used to allow the expression of FX standard tenors in addition to the base valid enumerations defined for the field that uses this pattern data type. This pattern data type is defined as follows:
/**
	used to allow the expression of FX standard tenors in addition to the base valid enumerations defined for the field that uses this pattern data type. This pattern data type is defined as follows:
Dx = tenor expression for "days", e.g. "D5", where "x" is any integer &gt; 0
Mx = tenor expression for "months", e.g. "M3", where "x" is any integer &gt; 0
Wx = tenor expression for "weeks", e.g. "W13", where "x" is any integer &gt; 0
Yx = tenor expression for "years", e.g. "Y1", where "x" is any integer &gt; 0
*/
using Tenor=std::string;
	
/// Values "100" and above are reserved for bilaterally agreed upon user defined enumerations.
/**
	Values "100" and above are reserved for bilaterally agreed upon user defined enumerations.
*/
using Reserved100Plus=long;
	
/// Values "1000" and above are reserved for bilaterally agreed upon user defined enumerations.
/**
	Values "1000" and above are reserved for bilaterally agreed upon user defined enumerations.
*/
using Reserved1000Plus=long;
	
/// Values "4000" and above are reserved for bilaterally agreed upon user defined enumerations.
/**
	Values "4000" and above are reserved for bilaterally agreed upon user defined enumerations.
*/
using Reserved4000Plus=long;
	
/// Contains an XML document raw data with no format or content restrictions. XMLData fields are always immediately preceded by a length field. The length field should specify the number of bytes of the value of the data field (up to but not including the terminating SOH).
/**
	Contains an XML document raw data with no format or content restrictions. XMLData fields are always immediately preceded by a length field. The length field should specify the number of bytes of the value of the data field (up to but not including the terminating SOH).
*/
using XMLData=std::string;
	

namespace FIXML_DATATYPES {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_FIXML_DATATYPES_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif
