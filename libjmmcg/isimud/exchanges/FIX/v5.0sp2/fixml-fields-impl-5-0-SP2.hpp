
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_FIELDS_IMPL_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_FIELDS_IMPL_HPP

/******************************************************************************
**
**	$Header$
**
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: 2020-03-11T03:21:40.074Z

#include "fixml-fields-base-5-0-SP2.hpp"

#define ISIMUD_FIXML_FIELDS_IMPL_HDR_GENERATED_DATE "2020-03-11T03:21:40.074Z"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {


using AdvSide_t=AdvSide_enum_t;

using AdvTransType_t=AdvTransType_enum_t;

using CommType_t=CommType_enum_t;

using ExecInst_t=ExecInst_enum_t;

using HandlInst_t=HandlInst_enum_t;

using SecurityIDSource_t=SecurityIDSource_enum_t;

using IOIQltyInd_t=IOIQltyInd_enum_t;

using IOIQty_t=IOIQty_enum_t;

using IOITransType_t=IOITransType_enum_t;

using LastCapacity_t=LastCapacity_enum_t;

using MsgType_t=MsgType_enum_t;

using OrdStatus_t=OrdStatus_enum_t;

using OrdType_t=OrdType_enum_t;

using PossDupFlag_t=PossDupFlag_enum_t;

using RejectCode_t=RejectCode_enum_t;

using Side_t=Side_enum_t;

using TimeInForce_t=TimeInForce_enum_t;

using Urgency_t=Urgency_enum_t;

using SettlType_t=SettlType_enum_t;

using SymbolSfx_t=SymbolSfx_enum_t;

using AllocTransType_t=AllocTransType_enum_t;

using PositionEffect_t=PositionEffect_enum_t;

using ProcessCode_t=ProcessCode_enum_t;

using AllocStatus_t=AllocStatus_enum_t;

using AllocRejCode_t=AllocRejCode_enum_t;

using EmailType_t=EmailType_enum_t;

using PossResend_t=PossResend_enum_t;

using CxlRejReason_t=CxlRejReason_enum_t;

using OrdRejReason_t=OrdRejReason_enum_t;

using IOIQualifier_t=IOIQualifier_enum_t;

using ReportToExch_t=ReportToExch_enum_t;

using LocateReqd_t=LocateReqd_enum_t;

using ForexReq_t=ForexReq_enum_t;

using DKReason_t=DKReason_enum_t;

using IOINaturalFlag_t=IOINaturalFlag_enum_t;

using MiscFeeType_t=MiscFeeType_enum_t;

using ExecType_t=ExecType_enum_t;

using SettlCurrFxRateCalc_t=SettlCurrFxRateCalc_enum_t;

using SettlInstMode_t=SettlInstMode_enum_t;

using SettlInstTransType_t=SettlInstTransType_enum_t;

using SettlInstSource_t=SettlInstSource_enum_t;

using SecurityType_t=SecurityType_enum_t;

using StandInstDbType_t=StandInstDbType_enum_t;

using SettlDeliveryType_t=SettlDeliveryType_enum_t;

using AllocLinkType_t=AllocLinkType_enum_t;

using PutOrCall_t=PutOrCall_enum_t;

using CoveredOrUncovered_t=CoveredOrUncovered_enum_t;

using NotifyBrokerOfCredit_t=NotifyBrokerOfCredit_enum_t;

using AllocHandlInst_t=AllocHandlInst_enum_t;

using RoutingType_t=RoutingType_enum_t;

using BenchmarkCurveName_t=BenchmarkCurveName_enum_t;

using StipulationType_t=StipulationType_enum_t;

using YieldType_t=YieldType_enum_t;

using TradedFlatSwitch_t=TradedFlatSwitch_enum_t;

using SubscriptionRequestType_t=SubscriptionRequestType_enum_t;

using MDUpdateType_t=MDUpdateType_enum_t;

using AggregatedBook_t=AggregatedBook_enum_t;

using MDEntryType_t=MDEntryType_enum_t;

using TickDirection_t=TickDirection_enum_t;

using QuoteCondition_t=QuoteCondition_enum_t;

using TradeCondition_t=TradeCondition_enum_t;

using MDUpdateAction_t=MDUpdateAction_enum_t;

using MDReqRejReason_t=MDReqRejReason_enum_t;

using DeleteReason_t=DeleteReason_enum_t;

using OpenCloseSettlFlag_t=OpenCloseSettlFlag_enum_t;

using FinancialStatus_t=FinancialStatus_enum_t;

using CorporateAction_t=CorporateAction_enum_t;

using QuoteStatus_t=QuoteStatus_enum_t;

using QuoteCancelType_t=QuoteCancelType_enum_t;

using QuoteRejectReason_t=QuoteRejectReason_enum_t;

using QuoteResponseLevel_t=QuoteResponseLevel_enum_t;

using QuoteRequestType_t=QuoteRequestType_enum_t;

using UnderlyingSecurityIDSource_t=SecurityIDSource_enum_t;

using UnderlyingSecurityType_t=SecurityType_enum_t;

using UnderlyingSymbolSfx_t=SymbolSfx_enum_t;

using UnderlyingPutOrCall_t=PutOrCall_enum_t;

using SecurityRequestType_t=SecurityRequestType_enum_t;

using SecurityResponseType_t=SecurityResponseType_enum_t;

using UnsolicitedIndicator_t=UnsolicitedIndicator_enum_t;

using SecurityTradingStatus_t=SecurityTradingStatus_enum_t;

using HaltReason_t=HaltReason_enum_t;

using InViewOfCommon_t=InViewOfCommon_enum_t;

using DueToRelated_t=DueToRelated_enum_t;

using Adjustment_t=Adjustment_enum_t;

using TradingSessionID_t=TradingSessionID_enum_t;

using TradSesMethod_t=TradSesMethod_enum_t;

using TradSesMode_t=TradSesMode_enum_t;

using TradSesStatus_t=TradSesStatus_enum_t;

using QuoteEntryRejectReason_t=QuoteRejectReason_enum_t;

using RefMsgType_t=MsgType_enum_t;

using BidRequestTransType_t=BidRequestTransType_enum_t;

using SolicitedFlag_t=SolicitedFlag_enum_t;

using ExecRestatementReason_t=ExecRestatementReason_enum_t;

using BusinessRejectReason_t=BusinessRejectReason_enum_t;

using DiscretionInst_t=DiscretionInst_enum_t;

using BidType_t=BidType_enum_t;

using BidDescriptorType_t=BidDescriptorType_enum_t;

using SideValueInd_t=SideValueInd_enum_t;

using LiquidityIndType_t=LiquidityIndType_enum_t;

using ExchangeForPhysical_t=ExchangeForPhysical_enum_t;

using ProgRptReqs_t=ProgRptReqs_enum_t;

using IncTaxInd_t=IncTaxInd_enum_t;

using BidTradeType_t=BidTradeType_enum_t;

using BasisPxType_t=BasisPxType_enum_t;

using PriceType_t=PriceType_enum_t;

using GTBookingInst_t=GTBookingInst_enum_t;

using ListStatusType_t=ListStatusType_enum_t;

using NetGrossInd_t=NetGrossInd_enum_t;

using ListOrderStatus_t=ListOrderStatus_enum_t;

using ListExecInstType_t=ListExecInstType_enum_t;

using CxlRejResponseTo_t=CxlRejResponseTo_enum_t;

using MultiLegReportingType_t=MultiLegReportingType_enum_t;

using PartyIDSource_t=PartyIDSource_enum_t;

using PartyRole_t=PartyRole_enum_t;

using SecurityAltIDSource_t=SecurityIDSource_enum_t;

using UnderlyingSecurityAltIDSource_t=SecurityIDSource_enum_t;

using Product_t=Product_enum_t;

using UnderlyingProduct_t=Product_enum_t;

using RoundingDirection_t=RoundingDirection_enum_t;

using DistribPaymentMethod_t=DistribPaymentMethod_enum_t;

using CancellationRights_t=CancellationRights_enum_t;

using MoneyLaunderingStatus_t=MoneyLaunderingStatus_enum_t;

using ExecPriceType_t=ExecPriceType_enum_t;

using TradeReportTransType_t=TradeReportTransType_enum_t;

using PaymentMethod_t=PaymentMethod_enum_t;

using TaxAdvantageType_t=TaxAdvantageType_enum_t;

using FundRenewWaiv_t=FundRenewWaiv_enum_t;

using RegistStatus_t=RegistStatus_enum_t;

using RegistRejReasonCode_t=RegistRejReasonCode_enum_t;

using RegistTransType_t=RegistTransType_enum_t;

using OwnershipType_t=OwnershipType_enum_t;

using ContAmtType_t=ContAmtType_enum_t;

using OwnerType_t=OwnerType_enum_t;

using NestedPartyIDSource_t=PartyIDSource_enum_t;

using OrderCapacity_t=OrderCapacity_enum_t;

using OrderRestrictions_t=OrderRestrictions_enum_t;

using MassCancelRequestType_t=MassCancelRequestType_enum_t;

using MassCancelResponse_t=MassCancelResponse_enum_t;

using MassCancelRejectReason_t=MassCancelRejectReason_enum_t;

using QuoteType_t=QuoteType_enum_t;

using NestedPartyRole_t=PartyRole_enum_t;

using CashMargin_t=CashMargin_enum_t;

using Scope_t=Scope_enum_t;

using MDImplicitDelete_t=MDImplicitDelete_enum_t;

using CrossType_t=CrossType_enum_t;

using CrossPrioritization_t=CrossPrioritization_enum_t;

using SecurityListRequestType_t=SecurityListRequestType_enum_t;

using SecurityRequestResult_t=SecurityRequestResult_enum_t;

using MultiLegRptTypeReq_t=MultiLegRptTypeReq_enum_t;

using LegPositionEffect_t=PositionEffect_enum_t;

using LegCoveredOrUncovered_t=CoveredOrUncovered_enum_t;

using TradSesStatusRejReason_t=TradSesStatusRejReason_enum_t;

using TradeRequestType_t=TradeRequestType_enum_t;

using PreviouslyReported_t=PreviouslyReported_enum_t;

using MatchStatus_t=MatchStatus_enum_t;

using MatchType_t=MatchType_enum_t;

using OddLot_t=OddLot_enum_t;

using ClearingInstruction_t=ClearingInstruction_enum_t;

using AccountType_t=AccountType_enum_t;

using CustOrderCapacity_t=CustOrderCapacity_enum_t;

using MassStatusReqType_t=MassStatusReqType_enum_t;

using LegSettlType_t=SettlType_enum_t;

using DayBookingInst_t=DayBookingInst_enum_t;

using BookingUnit_t=BookingUnit_enum_t;

using PreallocMethod_t=PreallocMethod_enum_t;

using LegSymbolSfx_t=SymbolSfx_enum_t;

using LegSecurityIDSource_t=SecurityIDSource_enum_t;

using LegSecurityAltIDSource_t=SecurityIDSource_enum_t;

using LegProduct_t=Product_enum_t;

using LegSecurityType_t=SecurityType_enum_t;

using LegSide_t=Side_enum_t;

using TradingSessionSubID_t=TradingSessionSubID_enum_t;

using AllocType_t=AllocType_enum_t;

using ClearingFeeIndicator_t=ClearingFeeIndicator_enum_t;

using WorkingIndicator_t=WorkingIndicator_enum_t;

using PriorityIndicator_t=PriorityIndicator_enum_t;

using LegalConfirm_t=LegalConfirm_enum_t;

using QuoteRequestRejectReason_t=QuoteRequestRejectReason_enum_t;

using AcctIDSource_t=AcctIDSource_enum_t;

using AllocAcctIDSource_t=AcctIDSource_enum_t;

using BenchmarkPriceType_t=PriceType_enum_t;

using ConfirmStatus_t=ConfirmStatus_enum_t;

using ConfirmTransType_t=ConfirmTransType_enum_t;

using DeliveryForm_t=DeliveryForm_enum_t;

using LegBenchmarkCurveName_t=BenchmarkCurveName_enum_t;

using LegBenchmarkPriceType_t=PriceType_enum_t;

using LegIOIQty_t=IOIQty_enum_t;

using LegPriceType_t=PriceType_enum_t;

using LegStipulationType_t=StipulationType_enum_t;

using LegSwapType_t=LegSwapType_enum_t;

using QuotePriceType_t=QuotePriceType_enum_t;

using QuoteRespType_t=QuoteRespType_enum_t;

using QuoteQualifier_t=IOIQualifier_enum_t;

using YieldRedemptionPriceType_t=PriceType_enum_t;

using PosType_t=PosType_enum_t;

using PosQtyStatus_t=PosQtyStatus_enum_t;

using PosAmtType_t=PosAmtType_enum_t;

using PosTransType_t=PosTransType_enum_t;

using PosMaintAction_t=PosMaintAction_enum_t;

using SettlSessID_t=SettlSessID_enum_t;

using AdjustmentType_t=AdjustmentType_enum_t;

using PosMaintStatus_t=PosMaintStatus_enum_t;

using PosMaintResult_t=PosMaintResult_enum_t;

using PosReqType_t=PosReqType_enum_t;

using ResponseTransportType_t=ResponseTransportType_enum_t;

using PosReqResult_t=PosReqResult_enum_t;

using PosReqStatus_t=PosReqStatus_enum_t;

using SettlPriceType_t=SettlPriceType_enum_t;

using UnderlyingSettlPriceType_t=SettlPriceType_enum_t;

using AssignmentMethod_t=AssignmentMethod_enum_t;

using ExerciseMethod_t=ExerciseMethod_enum_t;

using TradeRequestResult_t=TradeRequestResult_enum_t;

using TradeRequestStatus_t=TradeRequestStatus_enum_t;

using TradeReportRejectReason_t=TradeReportRejectReason_enum_t;

using SideMultiLegReportingType_t=SideMultiLegReportingType_enum_t;

using Nested2PartyIDSource_t=PartyIDSource_enum_t;

using Nested2PartyRole_t=PartyRole_enum_t;

using BenchmarkSecurityIDSource_t=SecurityIDSource_enum_t;

using TrdRegTimestampType_t=TrdRegTimestampType_enum_t;

using ConfirmType_t=ConfirmType_enum_t;

using ConfirmRejReason_t=ConfirmRejReason_enum_t;

using BookingType_t=BookingType_enum_t;

using IndividualAllocRejCode_t=AllocRejCode_enum_t;

using AllocSettlInstType_t=AllocSettlInstType_enum_t;

using SettlPartyIDSource_t=PartyIDSource_enum_t;

using SettlPartyRole_t=PartyRole_enum_t;

using SettlPartySubIDType_t=PartySubIDType_enum_t;

using DlvyInstType_t=DlvyInstType_enum_t;

using TerminationType_t=TerminationType_enum_t;

using SettlInstReqRejCode_t=SettlInstReqRejCode_enum_t;

using AllocReportType_t=AllocReportType_enum_t;

using AllocCancReplaceReason_t=AllocCancReplaceReason_enum_t;

using AllocAccountType_t=AllocAccountType_enum_t;

using PartySubIDType_t=PartySubIDType_enum_t;

using NestedPartySubIDType_t=PartySubIDType_enum_t;

using Nested2PartySubIDType_t=PartySubIDType_enum_t;

using AllocIntermedReqType_t=AllocIntermedReqType_enum_t;

using ApplQueueResolution_t=ApplQueueResolution_enum_t;

using ApplQueueAction_t=ApplQueueAction_enum_t;

using AvgPxIndicator_t=AvgPxIndicator_enum_t;

using TradeAllocIndicator_t=TradeAllocIndicator_enum_t;

using ExpirationCycle_t=ExpirationCycle_enum_t;

using TrdType_t=TrdType_enum_t;

using TrdSubType_t=TrdSubType_enum_t;

using PegMoveType_t=PegMoveType_enum_t;

using PegOffsetType_t=PegOffsetType_enum_t;

using PegLimitType_t=PegLimitType_enum_t;

using PegRoundDirection_t=PegRoundDirection_enum_t;

using PegScope_t=PegScope_enum_t;

using DiscretionMoveType_t=DiscretionMoveType_enum_t;

using DiscretionOffsetType_t=DiscretionOffsetType_enum_t;

using DiscretionLimitType_t=DiscretionLimitType_enum_t;

using DiscretionRoundDirection_t=DiscretionRoundDirection_enum_t;

using DiscretionScope_t=DiscretionScope_enum_t;

using TargetStrategy_t=TargetStrategy_enum_t;

using LastLiquidityInd_t=LastLiquidityInd_enum_t;

using PublishTrdIndicator_t=PublishTrdIndicator_enum_t;

using ShortSaleReason_t=ShortSaleReason_enum_t;

using QtyType_t=QtyType_enum_t;

using SecondaryTrdType_t=TrdType_enum_t;

using TradeReportType_t=TradeReportType_enum_t;

using AllocNoOrdersType_t=AllocNoOrdersType_enum_t;

using EventType_t=EventType_enum_t;

using InstrAttribType_t=InstrAttribType_enum_t;

using CPProgram_t=CPProgram_enum_t;

using UnderlyingCPProgram_t=CPProgram_enum_t;

using UnderlyingStipType_t=StipulationType_enum_t;

using MiscFeeBasis_t=MiscFeeBasis_enum_t;

using LastFragment_t=LastFragment_enum_t;

using CollAsgnReason_t=CollAsgnReason_enum_t;

using CollInquiryQualifier_t=CollInquiryQualifier_enum_t;

using CollAsgnTransType_t=CollAsgnTransType_enum_t;

using CollAsgnRespType_t=CollAsgnRespType_enum_t;

using CollAsgnRejectReason_t=CollAsgnRejectReason_enum_t;

using CollStatus_t=CollStatus_enum_t;

using LastRptRequested_t=LastRptRequested_enum_t;

using DeliveryType_t=DeliveryType_enum_t;

using UserRequestType_t=UserRequestType_enum_t;

using UserStatus_t=UserStatus_enum_t;

using StatusValue_t=StatusValue_enum_t;

using NetworkRequestType_t=NetworkRequestType_enum_t;

using NetworkStatusResponseType_t=NetworkStatusResponseType_enum_t;

using TrdRptStatus_t=TrdRptStatus_enum_t;

using AffirmStatus_t=AffirmStatus_enum_t;

using CollAction_t=CollAction_enum_t;

using CollInquiryStatus_t=CollInquiryStatus_enum_t;

using CollInquiryResult_t=CollInquiryResult_enum_t;

using Nested3PartyIDSource_t=PartyIDSource_enum_t;

using Nested3PartyRole_t=PartyRole_enum_t;

using Nested3PartySubIDType_t=PartySubIDType_enum_t;

using StrategyParameterType_t=StrategyParameterType_enum_t;

using SecurityStatus_t=SecurityStatus_enum_t;

using UnderlyingCashType_t=UnderlyingCashType_enum_t;

using UnderlyingSettlementType_t=UnderlyingSettlementType_enum_t;

using SecurityUpdateAction_t=SecurityUpdateAction_enum_t;

using ExpirationQtyType_t=ExpirationQtyType_enum_t;

using IndividualAllocType_t=IndividualAllocType_enum_t;

using UnitOfMeasure_t=UnitOfMeasure_enum_t;

using TimeUnit_t=TimeUnit_enum_t;

using UnderlyingUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingTimeUnit_t=TimeUnit_enum_t;

using LegTimeUnit_t=TimeUnit_enum_t;

using AllocMethod_t=AllocMethod_enum_t;

using SideTrdSubTyp_t=TrdSubType_enum_t;

using SideTrdRegTimestampType_t=TrdRegTimestampType_enum_t;

using AsOfIndicator_t=AsOfIndicator_enum_t;

using MDBookType_t=MDBookType_enum_t;

using MDOriginType_t=MDOriginType_enum_t;

using CustOrderHandlingInst_t=CustOrderHandlingInst_enum_t;

using OrderHandlingInstSource_t=OrderHandlingInstSource_enum_t;

using DeskType_t=DeskType_enum_t;

using DeskTypeSource_t=DeskTypeSource_enum_t;

using DeskOrderHandlingInst_t=CustOrderHandlingInst_enum_t;

using ExecAckStatus_t=ExecAckStatus_enum_t;

using UnderlyingSettlMethod_t=SettlMethod_enum_t;

using CollApplType_t=CollApplType_enum_t;

using UnderlyingFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using AllocPositionEffect_t=AllocPositionEffect_enum_t;

using DealingCapacity_t=DealingCapacity_enum_t;

using InstrmtAssignmentMethod_t=InstrmtAssignmentMethod_enum_t;

using InstrumentPartyIDSource_t=PartyIDSource_enum_t;

using InstrumentPartyRole_t=PartyRole_enum_t;

using InstrumentPartySubIDType_t=PartySubIDType_enum_t;

using AggressorIndicator_t=AggressorIndicator_enum_t;

using UnderlyingInstrumentPartyIDSource_t=PartyIDSource_enum_t;

using UnderlyingInstrumentPartyRole_t=PartyRole_enum_t;

using UnderlyingInstrumentPartySubIDType_t=PartySubIDType_enum_t;

using MDQuoteType_t=MDQuoteType_enum_t;

using RefOrderIDSource_t=RefOrderIDSource_enum_t;

using DisplayWhen_t=DisplayWhen_enum_t;

using DisplayMethod_t=DisplayMethod_enum_t;

using PriceProtectionScope_t=PriceProtectionScope_enum_t;

using LotType_t=LotType_enum_t;

using PegPriceType_t=PegPriceType_enum_t;

using PegSecurityIDSource_t=SecurityIDSource_enum_t;

using TriggerType_t=TriggerType_enum_t;

using TriggerAction_t=TriggerAction_enum_t;

using TriggerSecurityIDSource_t=SecurityIDSource_enum_t;

using TriggerPriceType_t=TriggerPriceType_enum_t;

using TriggerPriceTypeScope_t=TriggerPriceTypeScope_enum_t;

using TriggerPriceDirection_t=TriggerPriceDirection_enum_t;

using TriggerOrderType_t=TriggerOrderType_enum_t;

using OrderCategory_t=OrderCategory_enum_t;

using RootPartyIDSource_t=PartyIDSource_enum_t;

using RootPartyRole_t=PartyRole_enum_t;

using RootPartySubIDType_t=PartySubIDType_enum_t;

using TradeHandlingInstr_t=TradeHandlingInstr_enum_t;

using OrigTradeHandlingInstr_t=TradeHandlingInstr_enum_t;

using ApplVerID_t=ApplVerID_enum_t;

using RefApplVerID_t=ApplVerID_enum_t;

using ExDestinationIDSource_t=ExDestinationIDSource_enum_t;

using DefaultApplVerID_t=ApplVerID_enum_t;

using ImpliedMarketIndicator_t=ImpliedMarketIndicator_enum_t;

using SettlObligMode_t=SettlObligMode_enum_t;

using SettlObligTransType_t=SettlObligTransType_enum_t;

using SettlObligSource_t=SettlObligSource_enum_t;

using QuoteEntryStatus_t=QuoteEntryStatus_enum_t;

using PrivateQuote_t=PrivateQuote_enum_t;

using RespondentType_t=RespondentType_enum_t;

using SecurityTradingEvent_t=SecurityTradingEvent_enum_t;

using StatsType_t=StatsType_enum_t;

using MDSecSizeType_t=MDSecSizeType_enum_t;

using PriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using SettlMethod_t=SettlMethod_enum_t;

using ExerciseStyle_t=ExerciseStyle_enum_t;

using PriceQuoteMethod_t=PriceQuoteMethod_enum_t;

using ValuationMethod_t=ValuationMethod_enum_t;

using ListMethod_t=ListMethod_enum_t;

using TickRuleType_t=TickRuleType_enum_t;

using NestedInstrAttribType_t=InstrAttribType_enum_t;

using DerivativeSymbolSfx_t=SymbolSfx_enum_t;

using DerivativeSecurityIDSource_t=SecurityIDSource_enum_t;

using DerivativeSecurityAltIDSource_t=SecurityIDSource_enum_t;

using CommUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DerivativeProduct_t=Product_enum_t;

using DerivativeSecurityType_t=SecurityType_enum_t;

using DerivativeInstrmtAssignmentMethod_t=InstrmtAssignmentMethod_enum_t;

using DerivativeSecurityStatus_t=SecurityStatus_enum_t;

using DerivativeUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DerivativeTimeUnit_t=TimeUnit_enum_t;

using DerivativeEventType_t=EventType_enum_t;

using DerivativeInstrumentPartyIDSource_t=PartyIDSource_enum_t;

using DerivativeInstrumentPartyRole_t=PartyRole_enum_t;

using DerivativeInstrumentPartySubIDType_t=PartySubIDType_enum_t;

using DerivativeExerciseStyle_t=ExerciseStyle_enum_t;

using MaturityMonthYearIncrementUnits_t=MaturityMonthYearIncrementUnits_enum_t;

using MaturityMonthYearFormat_t=MaturityMonthYearFormat_enum_t;

using StrikeExerciseStyle_t=ExerciseStyle_enum_t;

using SecondaryPriceLimitType_t=PriceLimitType_enum_t;

using PriceLimitType_t=PriceLimitType_enum_t;

using ExecInstValue_t=ExecInst_enum_t;

using DerivativeInstrAttribType_t=InstrAttribType_enum_t;

using DerivativePriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DerivativeSettlMethod_t=SettlMethod_enum_t;

using DerivativePriceQuoteMethod_t=PriceQuoteMethod_enum_t;

using DerivativeValuationMethod_t=ValuationMethod_enum_t;

using DerivativeListMethod_t=ListMethod_enum_t;

using DerivativePutOrCall_t=PutOrCall_enum_t;

using ListUpdateAction_t=ListUpdateAction_enum_t;

using TradSesUpdateAction_t=SecurityUpdateAction_enum_t;

using ApplReqType_t=ApplReqType_enum_t;

using ApplResponseType_t=ApplResponseType_enum_t;

using ApplResponseError_t=ApplResponseError_enum_t;

using LegPutOrCall_t=PutOrCall_enum_t;

using TradSesEvent_t=TradSesEvent_enum_t;

using MassActionType_t=MassActionType_enum_t;

using MassActionScope_t=MassActionScope_enum_t;

using MassActionResponse_t=MassActionResponse_enum_t;

using MassActionRejectReason_t=MassActionRejectReason_enum_t;

using MultilegModel_t=MultilegModel_enum_t;

using MultilegPriceMethod_t=MultilegPriceMethod_enum_t;

using LegExecInst_t=ExecInst_enum_t;

using ContingencyType_t=ContingencyType_enum_t;

using ListRejectReason_t=ListRejectReason_enum_t;

using TrdRepPartyRole_t=PartyRole_enum_t;

using TradePublishIndicator_t=TradePublishIndicator_enum_t;

using MarketUpdateAction_t=SecurityUpdateAction_enum_t;

using SessionStatus_t=SessionStatus_enum_t;

using Nested4PartySubIDType_t=PartySubIDType_enum_t;

using Nested4PartyIDSource_t=PartyIDSource_enum_t;

using Nested4PartyRole_t=PartyRole_enum_t;

using UnderlyingExerciseStyle_t=ExerciseStyle_enum_t;

using LegExerciseStyle_t=ExerciseStyle_enum_t;

using LegPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using ApplReportType_t=ApplReportType_enum_t;

using OrderDelayUnit_t=OrderDelayUnit_enum_t;

using VenueType_t=VenueType_enum_t;

using RefOrdIDReason_t=RefOrdIDReason_enum_t;

using OrigCustOrderCapacity_t=OrigCustOrderCapacity_enum_t;

using ModelType_t=ModelType_enum_t;

using ContractMultiplierUnit_t=ContractMultiplierUnit_enum_t;

using LegContractMultiplierUnit_t=ContractMultiplierUnit_enum_t;

using UnderlyingContractMultiplierUnit_t=ContractMultiplierUnit_enum_t;

using DerivativeContractMultiplierUnit_t=ContractMultiplierUnit_enum_t;

using FlowScheduleType_t=FlowScheduleType_enum_t;

using LegFlowScheduleType_t=FlowScheduleType_enum_t;

using UnderlyingFlowScheduleType_t=FlowScheduleType_enum_t;

using DerivativeFlowScheduleType_t=FlowScheduleType_enum_t;

using FillLiquidityInd_t=LastLiquidityInd_enum_t;

using SideLiquidityInd_t=LastLiquidityInd_enum_t;

using RateSource_t=RateSource_enum_t;

using RateSourceType_t=RateSourceType_enum_t;

using RestructuringType_t=RestructuringType_enum_t;

using Seniority_t=Seniority_enum_t;

using UnderlyingRestructuringType_t=RestructuringType_enum_t;

using UnderlyingSeniority_t=Seniority_enum_t;

using TargetPartyIDSource_t=PartyIDSource_enum_t;

using TargetPartyRole_t=PartyRole_enum_t;

using SecurityListType_t=SecurityListType_enum_t;

using SecurityListTypeSource_t=SecurityListTypeSource_enum_t;

using NewsCategory_t=NewsCategory_enum_t;

using NewsRefType_t=NewsRefType_enum_t;

using StrikePriceDeterminationMethod_t=StrikePriceDeterminationMethod_enum_t;

using StrikePriceBoundaryMethod_t=StrikePriceBoundaryMethod_enum_t;

using UnderlyingPriceDeterminationMethod_t=UnderlyingPriceDeterminationMethod_enum_t;

using OptPayoutType_t=OptPayoutType_enum_t;

using ComplexEventType_t=ComplexEventType_enum_t;

using ComplexEventPriceBoundaryMethod_t=ComplexEventPriceBoundaryMethod_enum_t;

using ComplexEventPriceTimeType_t=ComplexEventPriceTimeType_enum_t;

using ComplexEventCondition_t=ComplexEventCondition_enum_t;

using StreamAsgnReqType_t=StreamAsgnReqType_enum_t;

using StreamAsgnRejReason_t=StreamAsgnRejReason_enum_t;

using StreamAsgnAckType_t=StreamAsgnAckType_enum_t;

using RequestedPartyRole_t=PartyRole_enum_t;

using RequestResult_t=RequestResult_enum_t;

using PartyRelationship_t=PartyRelationship_enum_t;

using PartyDetailAltIDSource_t=PartyIDSource_enum_t;

using PartyDetailAltSubIDType_t=PartySubIDType_enum_t;

using TrdAckStatus_t=TrdAckStatus_enum_t;

using RiskLimitType_t=RiskLimitType_enum_t;

using InstrumentScopeOperator_t=InstrumentScopeOperator_enum_t;

using InstrumentScopeSecurityIDSource_t=SecurityIDSource_enum_t;

using InstrumentScopeProduct_t=Product_enum_t;

using InstrumentScopeSecurityType_t=SecurityType_enum_t;

using InstrumentScopePutOrCall_t=PutOrCall_enum_t;

using InstrumentScopeSettlType_t=SettlType_enum_t;

using RelatedPartyDetailIDSource_t=PartyIDSource_enum_t;

using RelatedPartyDetailRole_t=PartyRole_enum_t;

using RelatedPartyDetailSubIDType_t=PartySubIDType_enum_t;

using RelatedPartyDetailAltIDSource_t=PartyIDSource_enum_t;

using RelatedPartyDetailAltSubIDType_t=PartySubIDType_enum_t;

using SwapSubClass_t=SwapSubClass_enum_t;

using SecurityClassificationReason_t=SecurityClassificationReason_enum_t;

using PosAmtReason_t=PosAmtReason_enum_t;

using LegPosAmtType_t=PosAmtType_enum_t;

using LegPosAmtReason_t=PosAmtReason_enum_t;

using LegQtyType_t=QtyType_enum_t;

using SideClearingTradePriceType_t=SideClearingTradePriceType_enum_t;

using SecurityRejectReason_t=SecurityRejectReason_enum_t;

using ThrottleStatus_t=ThrottleStatus_enum_t;

using ThrottleAction_t=ThrottleAction_enum_t;

using ThrottleType_t=ThrottleType_enum_t;

using ThrottleTimeUnit_t=OrderDelayUnit_enum_t;

using StreamAsgnType_t=StreamAsgnType_enum_t;

using ThrottleMsgType_t=MsgType_enum_t;

using MatchInst_t=MatchInst_enum_t;

using TriggerScope_t=TriggerScope_enum_t;

using LimitAmtType_t=LimitAmtType_enum_t;

using MarginReqmtInqQualifier_t=MarginReqmtInqQualifier_enum_t;

using MarginReqmtRptType_t=MarginReqmtRptType_enum_t;

using MarginReqmtInqStatus_t=CollInquiryStatus_enum_t;

using MarginReqmtInqResult_t=MarginReqmtInqResult_enum_t;

using MarginAmtType_t=MarginAmtType_enum_t;

using RelatedInstrumentType_t=RelatedInstrumentType_enum_t;

using RelatedSecurityIDSource_t=SecurityIDSource_enum_t;

using MarketMakerActivity_t=MarketMakerActivity_enum_t;

using RequestingPartyIDSource_t=PartyIDSource_enum_t;

using RequestingPartyRole_t=PartyRole_enum_t;

using RequestingPartySubIDType_t=PartySubIDType_enum_t;

using PartyDetailStatus_t=PartyDetailStatus_enum_t;

using PartyDetailRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using RelatedPartyDetailRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using SecurityMassTradingStatus_t=SecurityTradingStatus_enum_t;

using SecurityMassTradingEvent_t=SecurityTradingEvent_enum_t;

using MassHaltReason_t=HaltReason_enum_t;

using MDSecurityTradingStatus_t=SecurityTradingStatus_enum_t;

using MDHaltReason_t=HaltReason_enum_t;

using ThrottleInst_t=ThrottleInst_enum_t;

using ThrottleCountIndicator_t=ThrottleCountIndicator_enum_t;

using ShortSaleRestriction_t=ShortSaleRestriction_enum_t;

using ShortSaleExemptionReason_t=ShortSaleExemptionReason_enum_t;

using LegShortSaleExemptionReason_t=ShortSaleExemptionReason_enum_t;

using SideShortSaleExemptionReason_t=ShortSaleExemptionReason_enum_t;

using PartyDetailIDSource_t=PartyIDSource_enum_t;

using PartyDetailRole_t=PartyRole_enum_t;

using PartyDetailSubIDType_t=PartySubIDType_enum_t;

using StrikeUnitOfMeasure_t=UnitOfMeasure_enum_t;

using OrderOrigination_t=OrderOrigination_enum_t;

using AllocationRollupInstruction_t=AllocationRollupInstruction_enum_t;

using AllocReversalStatus_t=AllocReversalStatus_enum_t;

using ObligationType_t=ObligationType_enum_t;

using TradePriceNegotiationMethod_t=TradePriceNegotiationMethod_enum_t;

using UpfrontPriceType_t=UpfrontPriceType_enum_t;

using RiskLimitRequestType_t=RiskLimitRequestType_enum_t;

using RiskLimitRequestResult_t=RiskLimitRequestResult_enum_t;

using RiskLimitRequestStatus_t=PartyDetailRequestStatus_enum_t;

using RiskLimitStatus_t=PartyDetailDefinitionStatus_enum_t;

using RiskLimitResult_t=RiskLimitRequestResult_enum_t;

using RiskLimitAction_t=RiskLimitAction_enum_t;

using RiskWarningLevelAction_t=RiskLimitAction_enum_t;

using EntitlementType_t=EntitlementType_enum_t;

using EntitlementAttribDatatype_t=EntitlementAttribDatatype_enum_t;

using TradSesControl_t=TradSesControl_enum_t;

using TradeVolType_t=TradeVolType_enum_t;

using OrderEventType_t=OrderEventType_enum_t;

using OrderEventReason_t=OrderEventReason_enum_t;

using OrderEventLiquidityIndicator_t=LastLiquidityInd_enum_t;

using AuctionType_t=AuctionType_enum_t;

using AuctionInstruction_t=AuctionInstruction_enum_t;

using LockType_t=LockType_enum_t;

using ReleaseInstruction_t=ReleaseInstruction_enum_t;

using DisclosureType_t=DisclosureType_enum_t;

using DisclosureInstruction_t=DisclosureInstruction_enum_t;

using TradingCapacity_t=TradingCapacity_enum_t;

using ClearingAccountType_t=ClearingAccountType_enum_t;

using LegClearingAccountType_t=ClearingAccountType_enum_t;

using TargetPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using RelatedPriceSource_t=RelatedPriceSource_enum_t;

using MinQtyMethod_t=MinQtyMethod_enum_t;

using Triggered_t=Triggered_enum_t;

using EventTimeUnit_t=EventTimeUnit_enum_t;

using ClearedIndicator_t=ClearedIndicator_enum_t;

using ContractRefPosType_t=ContractRefPosType_enum_t;

using PositionCapacity_t=PositionCapacity_enum_t;

using PosQtyUnitOfMeasure_t=UnitOfMeasure_enum_t;

using TradePriceCondition_t=TradePriceCondition_enum_t;

using TradeAllocStatus_t=TradeAllocStatus_enum_t;

using TradeQtyType_t=TradeQtyType_enum_t;

using TradeAllocAmtType_t=PosAmtType_enum_t;

using TradeAllocGroupInstruction_t=TradeAllocGroupInstruction_enum_t;

using OffsetInstruction_t=OffsetInstruction_enum_t;

using TradeAllocAmtReason_t=PosAmtReason_enum_t;

using SideAvgPxIndicator_t=SideAvgPxIndicator_enum_t;

using RelatedTradeIDSource_t=RelatedTradeIDSource_enum_t;

using RelatedPositionIDSource_t=RelatedPositionIDSource_enum_t;

using QuoteAckStatus_t=QuoteAckStatus_enum_t;

using ValueCheckType_t=ValueCheckType_enum_t;

using ValueCheckAction_t=ValueCheckAction_enum_t;

using PartyDetailRequestResult_t=PartyDetailRequestResult_enum_t;

using PartyDetailRequestStatus_t=PartyDetailRequestStatus_enum_t;

using PartyDetailDefinitionStatus_t=PartyDetailDefinitionStatus_enum_t;

using PartyDetailDefinitionResult_t=PartyDetailRequestResult_enum_t;

using EntitlementRequestResult_t=EntitlementRequestResult_enum_t;

using EntitlementRequestStatus_t=PartyDetailRequestStatus_enum_t;

using EntitlementStatus_t=EntitlementStatus_enum_t;

using EntitlementResult_t=EntitlementRequestResult_enum_t;

using SettlPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using TradeMatchAckStatus_t=TradeMatchAckStatus_enum_t;

using TradeMatchRejectReason_t=TradeMatchRejectReason_enum_t;

using SideVenueType_t=VenueType_enum_t;

using RegulatoryTradeIDEvent_t=RegulatoryTradeIDEvent_enum_t;

using RegulatoryTradeIDType_t=RegulatoryTradeIDType_enum_t;

using AllocRegulatoryTradeIDEvent_t=RegulatoryTradeIDEvent_enum_t;

using AllocRegulatoryTradeIDType_t=RegulatoryTradeIDType_enum_t;

using ExposureDurationUnit_t=OrderDelayUnit_enum_t;

using PriceMovementType_t=PriceMovementType_enum_t;

using ClearingIntention_t=ClearingIntention_enum_t;

using TradeClearingInstruction_t=ClearingInstruction_enum_t;

using ConfirmationMethod_t=ConfirmationMethod_enum_t;

using VerificationMethod_t=VerificationMethod_enum_t;

using ClearingRequirementException_t=ClearingRequirementException_enum_t;

using IRSDirection_t=IRSDirection_enum_t;

using RegulatoryReportType_t=RegulatoryReportType_enum_t;

using TradeCollateralization_t=TradeCollateralization_enum_t;

using TradeContinuation_t=TradeContinuation_enum_t;

using AssetClass_t=AssetClass_enum_t;

using AssetSubClass_t=AssetSubClass_enum_t;

using SwapClass_t=SwapClass_enum_t;

using CouponType_t=CouponType_enum_t;

using CouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using CouponDayCount_t=CouponDayCount_enum_t;

using ConvertibleBondEquityIDSource_t=SecurityIDSource_enum_t;

using LienSeniority_t=LienSeniority_enum_t;

using LoanFacility_t=LoanFacility_enum_t;

using ReferenceEntityType_t=ReferenceEntityType_enum_t;

using SideRegulatoryTradeIDEvent_t=RegulatoryTradeIDEvent_enum_t;

using SideRegulatoryTradeIDType_t=RegulatoryTradeIDType_enum_t;

using SecondaryAssetClass_t=AssetClass_enum_t;

using SecondaryAssetSubClass_t=AssetSubClass_enum_t;

using BlockTrdAllocIndicator_t=BlockTrdAllocIndicator_enum_t;

using UnderlyingEventType_t=EventType_enum_t;

using UnderlyingCouponType_t=CouponType_enum_t;

using UnderlyingCouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingCouponDayCount_t=CouponDayCount_enum_t;

using UnderlyingObligationIDSource_t=SecurityIDSource_enum_t;

using UnderlyingEquityIDSource_t=SecurityIDSource_enum_t;

using UnderlyingLienSeniority_t=LienSeniority_enum_t;

using UnderlyingLoanFacility_t=LoanFacility_enum_t;

using UnderlyingReferenceEntityType_t=ReferenceEntityType_enum_t;

using UnderlyingAssignmentMethod_t=InstrmtAssignmentMethod_enum_t;

using UnderlyingSecurityStatus_t=SecurityStatus_enum_t;

using UnderlyingObligationType_t=UnderlyingObligationType_enum_t;

using UnderlyingAssetClass_t=AssetClass_enum_t;

using UnderlyingAssetSubClass_t=AssetSubClass_enum_t;

using UnderlyingSwapClass_t=SwapClass_enum_t;

using UnderlyingStrikePriceDeterminationMethod_t=StrikePriceDeterminationMethod_enum_t;

using UnderlyingStrikePriceBoundaryMethod_t=StrikePriceBoundaryMethod_enum_t;

using UnderlyingOptPayoutType_t=OptPayoutType_enum_t;

using UnderlyingPriceQuoteMethod_t=PriceQuoteMethod_enum_t;

using UnderlyingValuationMethod_t=ValuationMethod_enum_t;

using UnderlyingListMethod_t=ListMethod_enum_t;

using UnderlyingShortSaleRestriction_t=ShortSaleRestriction_enum_t;

using UnderlyingComplexEventType_t=ComplexEventType_enum_t;

using UnderlyingComplexEventPriceBoundaryMethod_t=ComplexEventPriceBoundaryMethod_enum_t;

using UnderlyingComplexEventPriceTimeType_t=ComplexEventPriceTimeType_enum_t;

using UnderlyingComplexEventCondition_t=ComplexEventCondition_enum_t;

using LegEventType_t=EventType_enum_t;

using LegEventTimeUnit_t=EventTimeUnit_enum_t;

using LegAssetClass_t=AssetClass_enum_t;

using LegAssetSubClass_t=AssetSubClass_enum_t;

using LegSwapClass_t=SwapClass_enum_t;

using LegSecondaryAssetClass_t=AssetClass_enum_t;

using LegSecondaryAssetSubClass_t=AssetSubClass_enum_t;

using UnderlyingSecondaryAssetClass_t=AssetClass_enum_t;

using UnderlyingSecondaryAssetSubClass_t=AssetSubClass_enum_t;

using MarginAmtFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using CollateralFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using PayCollectFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using PositionFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using AttachmentEncodingType_t=AttachmentEncodingType_enum_t;

using NegotiationMethod_t=NegotiationMethod_enum_t;

using ComplexOptPayoutPaySide_t=PaymentPaySide_enum_t;

using ComplexOptPayoutReceiveSide_t=PaymentPaySide_enum_t;

using ComplexOptPayoutTime_t=ComplexOptPayoutTime_enum_t;

using ComplexEventQuoteBasis_t=ComplexEventQuoteBasis_enum_t;

using ComplexEventCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using ComplexEventCreditEventNotifyingParty_t=ComplexEventCreditEventNotifyingParty_enum_t;

using StrategyType_t=StrategyType_enum_t;

using SettlDisruptionProvision_t=SettlDisruptionProvision_enum_t;

using InstrumentRoundingDirection_t=RoundingDirection_enum_t;

using LegInstrmtAssignmentMethod_t=InstrmtAssignmentMethod_enum_t;

using LegSecurityStatus_t=SecurityStatus_enum_t;

using LegRestructuringType_t=RestructuringType_enum_t;

using LegSeniority_t=Seniority_enum_t;

using LegObligationType_t=ObligationType_enum_t;

using LegSwapSubClass_t=SwapSubClass_enum_t;

using LegCouponType_t=CouponType_enum_t;

using LegCouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegCouponDayCount_t=CouponDayCount_enum_t;

using LegConvertibleBondEquityIDSource_t=SecurityIDSource_enum_t;

using LegLienSeniority_t=LienSeniority_enum_t;

using LegLoanFacility_t=LoanFacility_enum_t;

using LegReferenceEntityType_t=ReferenceEntityType_enum_t;

using LegStrikeUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegStrikePriceDeterminationMethod_t=StrikePriceDeterminationMethod_enum_t;

using LegStrikePriceBoundaryMethod_t=StrikePriceBoundaryMethod_enum_t;

using LegUnderlyingPriceDeterminationMethod_t=UnderlyingPriceDeterminationMethod_enum_t;

using LegSettlMethod_t=SettlMethod_enum_t;

using LegOptPayoutType_t=OptPayoutType_enum_t;

using LegPriceQuoteMethod_t=PriceQuoteMethod_enum_t;

using LegValuationMethod_t=ValuationMethod_enum_t;

using LegListMethod_t=ListMethod_enum_t;

using LegCPProgram_t=CPProgram_enum_t;

using LegShortSaleRestriction_t=ShortSaleRestriction_enum_t;

using AssetGroup_t=AssetGroup_enum_t;

using LegStrategyType_t=StrategyType_enum_t;

using LegSettlDisruptionProvision_t=SettlDisruptionProvision_enum_t;

using LegInstrumentRoundingDirection_t=RoundingDirection_enum_t;

using LegComplexEventType_t=ComplexEventType_enum_t;

using LegComplexOptPayoutPaySide_t=PaymentPaySide_enum_t;

using LegComplexOptPayoutReceiveSide_t=PaymentPaySide_enum_t;

using LegComplexOptPayoutTime_t=ComplexOptPayoutTime_enum_t;

using LegComplexEventPriceBoundaryMethod_t=ComplexEventPriceBoundaryMethod_enum_t;

using LegComplexEventPriceTimeType_t=ComplexEventPriceTimeType_enum_t;

using LegComplexEventCondition_t=ComplexEventCondition_enum_t;

using LegComplexEventQuoteBasis_t=ComplexEventQuoteBasis_enum_t;

using LegComplexEventCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using LegComplexEventCreditEventNotifyingParty_t=ComplexEventCreditEventNotifyingParty_enum_t;

using LegInstrumentPartyIDSource_t=PartyIDSource_enum_t;

using LegInstrumentPartyRole_t=PartyRole_enum_t;

using LegInstrumentPartySubIDType_t=PartySubIDType_enum_t;

using UnderlyingComplexOptPayoutPaySide_t=PaymentPaySide_enum_t;

using UnderlyingComplexOptPayoutReceiveSide_t=PaymentPaySide_enum_t;

using UnderlyingComplexOptPayoutTime_t=ComplexOptPayoutTime_enum_t;

using UnderlyingComplexEventQuoteBasis_t=ComplexEventQuoteBasis_enum_t;

using UnderlyingComplexEventCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using UnderlyingComplexEventCreditEventNotifyingParty_t=ComplexEventCreditEventNotifyingParty_enum_t;

using UnderlyingSwapSubClass_t=SwapSubClass_enum_t;

using UnderlyingStrikeUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingStrategyType_t=StrategyType_enum_t;

using UnderlyingSettlDisruptionProvision_t=SettlDisruptionProvision_enum_t;

using UnderlyingInstrumentRoundingDirection_t=RoundingDirection_enum_t;

using RiskLimitReportStatus_t=RiskLimitReportStatus_enum_t;

using RiskLimitReportRejectReason_t=RiskLimitReportRejectReason_enum_t;

using RiskLimitCheckTransType_t=RiskLimitCheckTransType_enum_t;

using RiskLimitCheckType_t=RiskLimitCheckType_enum_t;

using RiskLimitCheckRequestType_t=RiskLimitCheckRequestType_enum_t;

using RiskLimitCheckRequestStatus_t=RiskLimitCheckRequestStatus_enum_t;

using RiskLimitCheckRequestResult_t=RiskLimitCheckRequestResult_enum_t;

using PartyActionType_t=PartyActionType_enum_t;

using PartyActionResponse_t=PartyActionResponse_enum_t;

using PartyActionRejectReason_t=PartyActionRejectReason_enum_t;

using RefRiskLimitCheckIDType_t=RefRiskLimitCheckIDType_enum_t;

using RiskLimitVelocityUnit_t=TimeUnit_enum_t;

using RequestingPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using RiskLimitCheckModelType_t=RiskLimitCheckModelType_enum_t;

using RiskLimitCheckStatus_t=RiskLimitCheckStatus_enum_t;

using SideRiskLimitCheckStatus_t=RiskLimitCheckStatus_enum_t;

using RegulatoryTransactionType_t=RegulatoryTransactionType_enum_t;

using LegAssetGroup_t=AssetGroup_enum_t;

using PartyRiskLimitStatus_t=PartyRiskLimitStatus_enum_t;

using RemunerationIndicator_t=RemunerationIndicator_enum_t;

using PosReportAction_t=PosMaintAction_enum_t;

using SettlPriceFxRateCalc_t=SettlCurrFxRateCalc_enum_t;

using TaxonomyType_t=TaxonomyType_enum_t;

using PartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using DerivativeInstrumentPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using InstrumentPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using LegInstrumentPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using LegProvisionPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using Nested2PartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using Nested3PartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using Nested4PartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using NestedPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using ProvisionPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using RequestedPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using TradeContingency_t=TradeContingency_enum_t;

using RootPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using SettlPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using UnderlyingInstrumentPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using AllocRefRiskLimitCheckIDType_t=RefRiskLimitCheckIDType_enum_t;

using LimitRole_t=PartyRole_enum_t;

using RegulatoryTradeIDScope_t=RegulatoryTradeIDScope_enum_t;

using SideRegulatoryTradeIDScope_t=RegulatoryTradeIDScope_enum_t;

using AllocRegulatoryTradeIDScope_t=RegulatoryTradeIDScope_enum_t;

using EntitlementSubType_t=EntitlementSubType_enum_t;

using QuoteModelType_t=QuoteModelType_enum_t;

using ExecMethod_t=ExecMethod_enum_t;

using RelatedToSecurityIDSource_t=SecurityIDSource_enum_t;

using MassOrderRequestStatus_t=MassOrderRequestStatus_enum_t;

using MassOrderRequestResult_t=MassOrderRequestResult_enum_t;

using OrderResponseLevel_t=OrderResponseLevel_enum_t;

using OrderEntryAction_t=OrderEntryAction_enum_t;

using ExecTypeReason_t=ExecTypeReason_enum_t;

using TargetPartySubIDType_t=PartySubIDType_enum_t;

using TransferTransType_t=TransferTransType_enum_t;

using TransferType_t=TransferType_enum_t;

using TransferScope_t=TransferScope_enum_t;

using TransferStatus_t=TransferStatus_enum_t;

using TransferRejectReason_t=TransferRejectReason_enum_t;

using TransferReportType_t=TransferReportType_enum_t;

using AggressorSide_t=Side_enum_t;

using MDStatisticType_t=MDStatisticType_enum_t;

using MDStatisticScope_t=MDStatisticScope_enum_t;

using MDStatisticSubScope_t=MDStatisticSubScope_enum_t;

using MDStatisticScopeType_t=MDStatisticScopeType_enum_t;

using MDStatisticFrequencyUnit_t=OrderDelayUnit_enum_t;

using MDStatisticDelayUnit_t=OrderDelayUnit_enum_t;

using MDStatisticIntervalType_t=MDStatisticIntervalType_enum_t;

using MDStatisticIntervalTypeUnit_t=TimeUnit_enum_t;

using MDStatisticIntervalUnit_t=OrderDelayUnit_enum_t;

using MDStatisticRatioType_t=MDStatisticRatioType_enum_t;

using MDStatisticRequestResult_t=MDStatisticRequestResult_enum_t;

using MDStatisticStatus_t=MDStatisticStatus_enum_t;

using MDStatisticValueType_t=MDStatisticValueType_enum_t;

using MDStatisticValueUnit_t=OrderDelayUnit_enum_t;

using AllocRiskLimitCheckStatus_t=RiskLimitCheckStatus_enum_t;

using CollRptRejectReason_t=CollRptRejectReason_enum_t;

using CollRptStatus_t=CollRptStatus_enum_t;

using UnderlyingAssetGroup_t=AssetGroup_enum_t;

using LegDeliveryType_t=DeliveryType_enum_t;

using LegTerminationType_t=TerminationType_enum_t;

using CrossedIndicator_t=CrossedIndicator_enum_t;

using TradeReportingIndicator_t=TradeReportingIndicator_enum_t;

using RelativeValueType_t=RelativeValueType_enum_t;

using RelativeValueSide_t=RelativeValueSide_enum_t;

using MDReportEvent_t=MDReportEvent_enum_t;

using MarketSegmentStatus_t=MarketSegmentStatus_enum_t;

using MarketSegmentType_t=MarketSegmentType_enum_t;

using MarketSegmentSubType_t=MarketSegmentSubType_enum_t;

using MarketSegmentRelationship_t=MarketSegmentRelationship_enum_t;

using QuoteSideIndicator_t=QuoteSideIndicator_enum_t;

using MarketDepthTimeIntervalUnit_t=OrderDelayUnit_enum_t;

using MDRecoveryTimeIntervalUnit_t=OrderDelayUnit_enum_t;

using CustomerPriority_t=CustomerPriority_enum_t;

using SettlSubMethod_t=SettlSubMethod_enum_t;

using BusinessDayType_t=BusinessDayConvention_enum_t;

using CalculationMethod_t=CalculationMethod_enum_t;

using OrderAttributeType_t=OrderAttributeType_enum_t;

using ComplexEventPVFinalPriceElectionFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using StrikeIndexQuote_t=StrikeIndexQuote_enum_t;

using ExtraordinaryEventAdjustmentMethod_t=ExtraordinaryEventAdjustmentMethod_enum_t;

using LegStrikeIndexQuote_t=StrikeIndexQuote_enum_t;

using LegExtraordinaryEventAdjustmentMethod_t=ExtraordinaryEventAdjustmentMethod_enum_t;

using LegComplexEventPVFinalPriceElectionFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using UnderlyingComplexEventPVFinalPriceElectionFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using UnderlyingNotionalAdjustments_t=UnderlyingNotionalAdjustments_enum_t;

using UnderlyingFutureIDSource_t=SecurityIDSource_enum_t;

using UnderlyingStrikeIndexQuote_t=StrikeIndexQuote_enum_t;

using UnderlyingExtraordinaryEventAdjustmentMethod_t=ExtraordinaryEventAdjustmentMethod_enum_t;

using CollateralAmountType_t=CollateralAmountType_enum_t;

using CommissionAmountType_t=CommissionAmountType_enum_t;

using CommissionBasis_t=CommType_enum_t;

using CommissionUnitOfMeasure_t=UnitOfMeasure_enum_t;

using AllocCommissionAmountType_t=CommissionAmountType_enum_t;

using AllocCommissionBasis_t=CommType_enum_t;

using AllocCommissionUnitOfMeasure_t=UnitOfMeasure_enum_t;

using AlgorithmicTradeIndicator_t=AlgorithmicTradeIndicator_enum_t;

using TrdRegPublicationType_t=TrdRegPublicationType_enum_t;

using TrdRegPublicationReason_t=TrdRegPublicationReason_enum_t;

using SideTradeReportingIndicator_t=TradeReportingIndicator_enum_t;

using MassActionReason_t=MassActionReason_enum_t;

using NotAffectedReason_t=NotAffectedReason_enum_t;

using OrderOwnershipIndicator_t=OrderOwnershipIndicator_enum_t;

using InTheMoneyCondition_t=InTheMoneyCondition_enum_t;

using LegInTheMoneyCondition_t=InTheMoneyCondition_enum_t;

using UnderlyingInTheMoneyCondition_t=InTheMoneyCondition_enum_t;

using DerivativeInTheMoneyCondition_t=InTheMoneyCondition_enum_t;

using SideCollateralAmountType_t=CollateralAmountType_enum_t;

using SideCollateralFXRateCalc_t=UnderlyingFXRateCalc_enum_t;

using ExDestinationType_t=ExDestinationType_enum_t;

using MarketCondition_t=MarketCondition_enum_t;

using QuoteAttributeType_t=QuoteAttributeType_enum_t;

using PriceQualifier_t=PriceQualifier_enum_t;

using MDValueTier_t=MDValueTier_enum_t;

using MiscFeeQualifier_t=MiscFeeQualifier_enum_t;

using UnderlyingIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using CommissionAmountSubType_t=CommissionAmountSubType_enum_t;

using AllocCommissionAmountSubType_t=CommissionAmountSubType_enum_t;

using FloatingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using FloatingRateIndexIDSource_t=SecurityIDSource_enum_t;

using CommodityFinalPriceType_t=CommodityFinalPriceType_enum_t;

using ReferenceDataDateType_t=ReferenceDataDateType_enum_t;

using ReturnTrigger_t=ReturnTrigger_enum_t;

using LegReturnTrigger_t=ReturnTrigger_enum_t;

using UnderlyingReturnTrigger_t=ReturnTrigger_enum_t;

using AveragePriceType_t=AveragePriceType_enum_t;

using AllocGroupStatus_t=AllocGroupStatus_enum_t;

using AllocRequestStatus_t=AllocRequestStatus_enum_t;

using AllocAvgPxIndicator_t=AvgPxIndicator_enum_t;

using MatchExceptionType_t=MatchExceptionType_enum_t;

using MatchExceptionElementType_t=MatchExceptionElementType_enum_t;

using MatchExceptionToleranceValueType_t=MatchExceptionToleranceValueType_enum_t;

using MatchingDataPointIndicator_t=MatchingDataPointIndicator_enum_t;

using MatchingDataPointType_t=MatchExceptionElementType_enum_t;

using TradeAggregationTransType_t=TradeAggregationTransType_enum_t;

using TradeAggregationRequestStatus_t=TradeAggregationRequestStatus_enum_t;

using TradeAggregationRejectReason_t=TradeAggregationRejectReason_enum_t;

using OffshoreIndicator_t=OffshoreIndicator_enum_t;

using AdditionalTermBondSecurityIDSource_t=SecurityIDSource_enum_t;

using AdditionalTermBondSeniority_t=Seniority_enum_t;

using AdditionalTermBondCouponType_t=CouponType_enum_t;

using AdditionalTermBondCouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using AdditionalTermBondDayCount_t=CouponDayCount_enum_t;

using CashSettlQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using CashSettlValuationMethod_t=CashSettlValuationMethod_enum_t;

using StreamType_t=StreamType_enum_t;

using StreamPaySide_t=PaymentPaySide_enum_t;

using StreamReceiveSide_t=PaymentPaySide_enum_t;

using UnderlyingStreamEffectiveDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamEffectiveDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingStreamEffectiveDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using StreamTerminationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamTerminationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using StreamTerminationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using StreamCalculationPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamFirstPeriodStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamCalculationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using StreamCalculationRollConvention_t=DateRollConvention_enum_t;

using LegPaymentStreamNonDeliverableSettlRateSource_t=RateSource_enum_t;

using SettlRatePostponementCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using ProvisionType_t=ProvisionType_enum_t;

using ProvisionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionDateTenorUnit_t=ProvisionDateTenorUnit_enum_t;

using ProvisionCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using ProvisionOptionSinglePartyBuyerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using ProvisionOptionSinglePartySellerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using ProvisionOptionExerciseStyle_t=ExerciseStyle_enum_t;

using ProvisionCashSettlMethod_t=ProvisionCashSettlMethod_enum_t;

using ProvisionCashSettlQuoteType_t=ProvisionCashSettlQuoteType_enum_t;

using ProvisionCashSettlQuoteSource_t=PaymentStreamRateIndexSource_enum_t;

using ProvisionCashSettlValueDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionCashSettlValueDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ProvisionCashSettlValueDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionOptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionOptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using ProvisionOptionExerciseFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using ProvisionOptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ProvisionOptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionOptionExerciseFixedDateType_t=ProvisionOptionExerciseFixedDateType_enum_t;

using ProvisionOptionExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionOptionExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ProvisionOptionExpirationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionOptionRelevantUnderlyingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionOptionRelevantUnderlyingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ProvisionOptionRelevantUnderlyingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionCashSettlPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ProvisionCashSettlPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ProvisionCashSettlPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionCashSettlPaymentDateType_t=ProvisionCashSettlPaymentDateType_enum_t;

using ProvisionPartyIDSource_t=PartyIDSource_enum_t;

using ProvisionPartyRole_t=PartyRole_enum_t;

using ProvisionPartySubIDType_t=PartySubIDType_enum_t;

using ProtectionTermEventUnit_t=ProtectionTermEventUnit_enum_t;

using ProtectionTermEventDayType_t=PaymentStreamInflationLagDayType_enum_t;

using ProtectionTermEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using PaymentType_t=PaymentType_enum_t;

using PaymentPaySide_t=PaymentPaySide_enum_t;

using PaymentReceiveSide_t=PaymentPaySide_enum_t;

using PaymentBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentSettlStyle_t=PaymentSettlStyle_enum_t;

using PaymentSettlPartyIDSource_t=PartyIDSource_enum_t;

using PaymentSettlPartyRole_t=PartyRole_enum_t;

using PaymentSettlPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using PaymentSettlPartySubIDType_t=PartySubIDType_enum_t;

using LegStreamType_t=StreamType_enum_t;

using LegStreamPaySide_t=PaymentPaySide_enum_t;

using LegStreamReceiveSide_t=PaymentPaySide_enum_t;

using LegStreamEffectiveDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamEffectiveDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegStreamEffectiveDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegStreamTerminationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamTerminationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegStreamTerminationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegStreamCalculationPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamFirstPeriodStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamCalculationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegStreamCalculationRollConvention_t=DateRollConvention_enum_t;

using LegPaymentStreamType_t=PaymentStreamType_enum_t;

using LegPaymentStreamDayCount_t=CouponDayCount_enum_t;

using LegPaymentStreamDiscountType_t=PaymentStreamDiscountType_enum_t;

using LegPaymentStreamDiscountRateDayCount_t=CouponDayCount_enum_t;

using LegPaymentStreamCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using LegPaymentStreamPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamPaymentFrequencyUnit_t=PaymentStreamPaymentFrequencyUnit_enum_t;

using LegPaymentStreamPaymentRollConvention_t=DateRollConvention_enum_t;

using LegPaymentStreamPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamResetDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamResetFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegPaymentStreamResetWeeklyRollConvention_t=PaymentStreamResetWeeklyRollConvention_enum_t;

using LegPaymentStreamInitialFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamInitialFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamInitialFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamRateCutoffDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamRateCutoffDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamRateIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using LegPaymentStreamRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegPaymentStreamRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegPaymentStreamRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegPaymentStreamCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStreamCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStreamFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStreamFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStreamFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using LegPaymentStreamAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using LegPaymentStreamNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using LegPaymentStreamInflationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using LegPaymentStreamInflationLagDayType_t=PaymentStreamInflationLagDayType_enum_t;

using LegPaymentStreamInflationInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using LegPaymentStreamInflationIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using LegPaymentStreamFRADiscounting_t=PaymentStreamFRADiscounting_enum_t;

using LegPaymentStreamNonDeliverableFixingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamNonDeliverableFixingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamNonDeliverableFixingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegSettlRateFallbackRateSource_t=RateSource_enum_t;

using LegNonDeliverableFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using PaymentStreamNonDeliverableSettlRateSource_t=RateSource_enum_t;

using SettlRateFallbackRateSource_t=RateSource_enum_t;

using LegPaymentScheduleType_t=PaymentScheduleType_enum_t;

using LegPaymentScheduleStubType_t=PaymentStubType_enum_t;

using LegPaymentSchedulePaySide_t=PaymentPaySide_enum_t;

using LegPaymentScheduleReceiveSide_t=PaymentPaySide_enum_t;

using LegPaymentScheduleRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegPaymentScheduleRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegPaymentScheduleStepFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegPaymentScheduleStepRelativeTo_t=PaymentScheduleStepRelativeTo_enum_t;

using LegPaymentScheduleFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentScheduleFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentScheduleFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentScheduleInterimExchangeDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentScheduleInterimExchangeDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentScheduleInterimExchangeDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentScheduleRateSource_t=RateSource_enum_t;

using LegPaymentScheduleRateSourceType_t=RateSourceType_enum_t;

using LegPaymentStubType_t=PaymentStubType_enum_t;

using LegPaymentStubLength_t=PaymentStubLength_enum_t;

using LegPaymentStubIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using LegPaymentStubIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegPaymentStubIndexRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegPaymentStubIndexRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegPaymentStubIndexCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStubIndexCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStubIndexFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStubIndexFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStubIndex2Source_t=PaymentStreamRateIndexSource_enum_t;

using LegPaymentStubIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegPaymentStubIndex2RateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegPaymentStubIndex2RateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegProvisionType_t=ProvisionType_enum_t;

using LegProvisionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionDateTenorUnit_t=ProvisionDateTenorUnit_enum_t;

using LegProvisionCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using LegProvisionOptionSinglePartyBuyerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using LegProvisionOptionSinglePartySellerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using LegProvisionOptionExerciseStyle_t=ExerciseStyle_enum_t;

using LegProvisionCashSettlMethod_t=ProvisionCashSettlMethod_enum_t;

using LegProvisionCashSettlQuoteType_t=ProvisionCashSettlQuoteType_enum_t;

using LegProvisionCashSettlQuoteSource_t=PaymentStreamRateIndexSource_enum_t;

using LegProvisionCashSettlPaymentDateType_t=ProvisionCashSettlPaymentDateType_enum_t;

using LegProvisionOptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionOptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using LegProvisionOptionExerciseFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegProvisionOptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegProvisionOptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionOptionExerciseFixedDateType_t=ProvisionOptionExerciseFixedDateType_enum_t;

using LegProvisionOptionExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionOptionExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegProvisionOptionExpirationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionOptionRelevantUnderlyingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionOptionRelevantUnderlyingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegProvisionOptionRelevantUnderlyingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionCashSettlPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionCashSettlPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegProvisionCashSettlPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionCashSettlValueDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProvisionCashSettlValueDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegProvisionCashSettlValueDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionPartyIDSource_t=PartyIDSource_enum_t;

using LegProvisionPartyRole_t=PartyRole_enum_t;

using LegProvisionPartySubIDType_t=PartySubIDType_enum_t;

using UnderlyingStreamType_t=StreamType_enum_t;

using UnderlyingStreamPaySide_t=PaymentPaySide_enum_t;

using UnderlyingStreamReceiveSide_t=PaymentPaySide_enum_t;

using UnderlyingStreamTerminationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamTerminationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingStreamTerminationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingStreamCalculationPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamFirstPeriodStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamCalculationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingStreamCalculationRollConvention_t=DateRollConvention_enum_t;

using UnderlyingPaymentStreamType_t=PaymentStreamType_enum_t;

using UnderlyingPaymentStreamDayCount_t=CouponDayCount_enum_t;

using UnderlyingPaymentStreamDiscountType_t=PaymentStreamDiscountType_enum_t;

using UnderlyingPaymentStreamDiscountRateDayCount_t=CouponDayCount_enum_t;

using UnderlyingPaymentStreamCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using UnderlyingPaymentStreamPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamPaymentFrequencyUnit_t=PaymentStreamPaymentFrequencyUnit_enum_t;

using UnderlyingPaymentStreamPaymentRollConvention_t=DateRollConvention_enum_t;

using UnderlyingPaymentStreamPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamResetDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamResetFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingPaymentStreamResetWeeklyRollConvention_t=PaymentStreamResetWeeklyRollConvention_enum_t;

using UnderlyingPaymentStreamInitialFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamInitialFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamInitialFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamRateCutoffDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamRateCutoffDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamRateIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using UnderlyingPaymentStreamRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingPaymentStreamRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingPaymentStreamRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingPaymentStreamCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStreamCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStreamFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStreamFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStreamFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using UnderlyingPaymentStreamAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using UnderlyingPaymentStreamNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using UnderlyingPaymentStreamInflationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using UnderlyingPaymentStreamInflationLagDayType_t=PaymentStreamInflationLagDayType_enum_t;

using UnderlyingPaymentStreamInflationInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using UnderlyingPaymentStreamInflationIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using UnderlyingPaymentStreamFRADiscounting_t=PaymentStreamFRADiscounting_enum_t;

using UnderlyingPaymentStreamNonDeliverableFixingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamNonDeliverableFixingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamNonDeliverableFixingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingNonDeliverableFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using UnderlyingPaymentStreamNonDeliverableSettlRateSource_t=RateSource_enum_t;

using UnderlyingSettlRatePostponementCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using UnderlyingPaymentScheduleType_t=PaymentScheduleType_enum_t;

using UnderlyingPaymentScheduleStubType_t=PaymentStubType_enum_t;

using UnderlyingPaymentSchedulePaySide_t=PaymentPaySide_enum_t;

using UnderlyingPaymentScheduleReceiveSide_t=PaymentPaySide_enum_t;

using UnderlyingPaymentScheduleRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingPaymentScheduleRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingPaymentScheduleStepFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingPaymentScheduleStepRelativeTo_t=PaymentScheduleStepRelativeTo_enum_t;

using UnderlyingPaymentScheduleFixingDateBusinessDayCnvtn_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentScheduleFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentScheduleFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentScheduleInterimExchangeDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentScheduleInterimExchangeDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentScheduleInterimExchangeDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentScheduleRateSource_t=RateSource_enum_t;

using UnderlyingPaymentScheduleRateSourceType_t=RateSourceType_enum_t;

using UnderlyingPaymentStubType_t=PaymentStubType_enum_t;

using UnderlyingPaymentStubLength_t=PaymentStubLength_enum_t;

using UnderlyingPaymentStubIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using UnderlyingPaymentStubIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingPaymentStubIndexRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingPaymentStubIndexRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingPaymentStubIndexCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStubIndexCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStubIndexFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStubIndexFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStubIndex2Source_t=PaymentStreamRateIndexSource_enum_t;

using UnderlyingPaymentStubIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingPaymentStubIndex2RateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingPaymentStubIndex2RateTreatment_t=PaymentStreamRateTreatment_enum_t;

using PaymentStreamType_t=PaymentStreamType_enum_t;

using PaymentStreamDayCount_t=CouponDayCount_enum_t;

using PaymentStreamDiscountType_t=PaymentStreamDiscountType_enum_t;

using PaymentStreamDiscountRateDayCount_t=CouponDayCount_enum_t;

using PaymentStreamCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using PaymentStreamPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamPaymentFrequencyUnit_t=PaymentStreamPaymentFrequencyUnit_enum_t;

using PaymentStreamPaymentRollConvention_t=DateRollConvention_enum_t;

using PaymentStreamPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamResetDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamResetFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using PaymentStreamResetWeeklyRollConvention_t=PaymentStreamResetWeeklyRollConvention_enum_t;

using PaymentStreamInitialFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamInitialFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamInitialFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamRateCutoffDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamRateCutoffDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamRateIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using PaymentStreamRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using PaymentStreamRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using PaymentStreamRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using PaymentStreamCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStreamCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStreamFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStreamFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStreamFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using PaymentStreamAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using PaymentStreamNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using PaymentStreamInflationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using PaymentStreamInflationLagDayType_t=PaymentStreamInflationLagDayType_enum_t;

using PaymentStreamInflationInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using PaymentStreamInflationIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using PaymentStreamFRADiscounting_t=PaymentStreamFRADiscounting_enum_t;

using PaymentStreamNonDeliverableFixingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamNonDeliverableFixingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamNonDeliverableFixingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using NonDeliverableFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using PaymentScheduleType_t=PaymentScheduleType_enum_t;

using PaymentScheduleStubType_t=PaymentStubType_enum_t;

using PaymentSchedulePaySide_t=PaymentPaySide_enum_t;

using PaymentScheduleReceiveSide_t=PaymentPaySide_enum_t;

using PaymentScheduleRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using PaymentScheduleRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using PaymentScheduleStepFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using PaymentScheduleStepRelativeTo_t=PaymentScheduleStepRelativeTo_enum_t;

using PaymentScheduleFixingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentScheduleFixingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentScheduleFixingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentScheduleInterimExchangeDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentScheduleInterimExchangeDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentScheduleInterimExchangeDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentScheduleRateSource_t=RateSource_enum_t;

using PaymentScheduleRateSourceType_t=RateSourceType_enum_t;

using PaymentStubType_t=PaymentStubType_enum_t;

using PaymentStubLength_t=PaymentStubLength_enum_t;

using PaymentStubIndexSource_t=PaymentStreamRateIndexSource_enum_t;

using PaymentStubIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using PaymentStubIndexRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using PaymentStubIndexRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using PaymentStubIndexCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStubIndexCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStubIndexFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStubIndexFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStubIndex2Source_t=PaymentStreamRateIndexSource_enum_t;

using PaymentStubIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using PaymentStubIndex2RateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using PaymentStubIndex2RateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingSettlRateFallbackRateSource_t=RateSource_enum_t;

using LegSettlRatePostponementCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using StreamEffectiveDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamEffectiveDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using StreamEffectiveDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionPartyRoleQualifier_t=PartyDetailRoleQualifier_enum_t;

using PaymentPriceType_t=PriceType_enum_t;

using PaymentStreamPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using BusinessDayConvention_t=BusinessDayConvention_enum_t;

using DateRollConvention_t=DateRollConvention_enum_t;

using LegBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegDateRollConvention_t=DateRollConvention_enum_t;

using UnderlyingBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingDateRollConvention_t=DateRollConvention_enum_t;

using PaymentSubType_t=PaymentSubType_enum_t;

using ComplexEventCreditEventUnit_t=ProtectionTermEventUnit_enum_t;

using ComplexEventCreditEventDayType_t=ComplexEventDateOffsetDayType_enum_t;

using ComplexEventCreditEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using ComplexEventPeriodType_t=ComplexEventPeriodType_enum_t;

using ComplexEventRateSource_t=RateSource_enum_t;

using ComplexEventRateSourceType_t=RateSourceType_enum_t;

using ComplexEventDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ComplexEventDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using ComplexEventDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ComplexEventScheduleFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using ComplexEventScheduleRollConvention_t=DateRollConvention_enum_t;

using DeliveryScheduleType_t=DeliveryScheduleType_enum_t;

using DeliveryScheduleNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DeliveryScheduleNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using DeliveryScheduleToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DeliveryScheduleToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using DeliveryScheduleSettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using DeliveryScheduleSettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using DeliveryScheduleSettlDay_t=DeliveryScheduleSettlDay_enum_t;

using DeliveryScheduleSettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using DeliveryStreamType_t=DeliveryStreamType_enum_t;

using DeliveryStreamDeliveryRestriction_t=DeliveryStreamDeliveryRestriction_enum_t;

using DeliveryStreamDeliveryContingentPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using DeliveryStreamTitleTransferCondition_t=DeliveryStreamTitleTransferCondition_enum_t;

using DeliveryStreamToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using DeliveryStreamToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using DeliveryStreamToleranceOptionSide_t=DeliveryStreamToleranceOptionSide_enum_t;

using DeliveryStreamElectingPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using MarketDisruptionProvision_t=MarketDisruptionProvision_enum_t;

using MarketDisruptionFallbackProvision_t=MarketDisruptionFallbackProvision_enum_t;

using MarketDisruptionFallbackUnderlierType_t=MarketDisruptionFallbackUnderlierType_enum_t;

using MarketDisruptionFallbackUnderlierSecurityIDSource_t=SecurityIDSource_enum_t;

using ExerciseConfirmationMethod_t=ExerciseConfirmationMethod_enum_t;

using OptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using OptionExerciseEarliestDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using OptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using OptionExerciseFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using OptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using OptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using OptionExerciseDateType_t=OptionExerciseDateType_enum_t;

using OptionExerciseExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using OptionExerciseExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using OptionExerciseExpirationFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using OptionExerciseExpirationRollConvention_t=DateRollConvention_enum_t;

using OptionExerciseExpirationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using OptionExerciseExpirationDateType_t=OptionExerciseDateType_enum_t;

using PaymentUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentDateOffsetDayType_t=PaymentDateOffsetDayType_enum_t;

using PaymentForwardStartType_t=PaymentForwardStartType_enum_t;

using PaymentScheduleFixingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using PaymentScheduleRateUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentScheduleRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using PaymentScheduleSettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentScheduleStepUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentScheduleFixingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using PaymentScheduleFixingLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using PaymentScheduleFixingFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamFixedAmountUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentStreamRateIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using PaymentStreamRateIndexUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentStreamSettlLevel_t=PaymentStreamSettlLevel_enum_t;

using PaymentStreamReferenceLevelUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentStreamRateSpreadUnitOfMeasure_t=UnitOfMeasure_enum_t;

using PaymentStreamRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using PaymentStreamCalculationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using PaymentStreamFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamPricingDayType_t=ComplexEventDateOffsetDayType_enum_t;

using PaymentStreamPricingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using PaymentStreamPricingBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamPaymentDateType_t=OptionExerciseDateType_enum_t;

using PaymentStreamPricingDateType_t=OptionExerciseDateType_enum_t;

using PaymentStreamPricingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using PricingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamCalculationPeriodDateType_t=OptionExerciseDateType_enum_t;

using StreamCalculationCorrectionUnit_t=ProtectionTermEventUnit_enum_t;

using StreamCommoditySecurityIDSource_t=SecurityIDSource_enum_t;

using StreamCommodityUnitOfMeasure_t=UnitOfMeasure_enum_t;

using StreamCommodityNearbySettlDayUnit_t=StreamCommodityNearbySettlDayUnit_enum_t;

using StreamCommoditySettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamCommoditySettlDateRollUnit_t=StreamCommoditySettlDateRollUnit_enum_t;

using StreamCommoditySettlDayType_t=ComplexEventDateOffsetDayType_enum_t;

using StreamCommodityDataSourceIDType_t=StreamCommodityDataSourceIDType_enum_t;

using StreamCommoditySettlDay_t=DeliveryScheduleSettlDay_enum_t;

using StreamCommoditySettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using StreamCommoditySettlPeriodNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using StreamCommoditySettlPeriodFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using StreamCommoditySettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using StreamCommoditySettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using StreamNotionalFrequencyUnit_t=TimeUnit_enum_t;

using StreamNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using StreamNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using StreamTotalNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegAdditionalTermBondSecurityIDSource_t=SecurityIDSource_enum_t;

using LegAdditionalTermBondSeniority_t=Seniority_enum_t;

using LegAdditionalTermBondCouponType_t=CouponType_enum_t;

using LegAdditionalTermBondCouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegAdditionalTermBondDayCount_t=CouponDayCount_enum_t;

using LegCashSettlQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using LegCashSettlValuationMethod_t=CashSettlValuationMethod_enum_t;

using LegComplexEventCreditEventUnit_t=ProtectionTermEventUnit_enum_t;

using LegComplexEventCreditEventDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegComplexEventCreditEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using LegComplexEventPeriodType_t=ComplexEventPeriodType_enum_t;

using LegComplexEventRateSource_t=RateSource_enum_t;

using LegComplexEventRateSourceType_t=RateSourceType_enum_t;

using LegComplexEventDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegComplexEventDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegComplexEventDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegComplexEventScheduleFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using LegComplexEventScheduleRollConvention_t=DateRollConvention_enum_t;

using LegDeliveryScheduleType_t=DeliveryScheduleType_enum_t;

using LegDeliveryScheduleNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegDeliveryScheduleNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using LegDeliveryScheduleToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegDeliveryScheduleToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using LegDeliveryScheduleSettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using LegDeliveryScheduleSettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using LegDeliveryScheduleSettlDay_t=DeliveryScheduleSettlDay_enum_t;

using LegDeliveryScheduleSettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using LegDeliveryStreamType_t=DeliveryStreamType_enum_t;

using LegDeliveryStreamDeliveryRestriction_t=DeliveryStreamDeliveryRestriction_enum_t;

using LegDeliveryStreamDeliveryContingentPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using LegDeliveryStreamTitleTransferCondition_t=DeliveryStreamTitleTransferCondition_enum_t;

using LegDeliveryStreamToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegDeliveryStreamToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using LegDeliveryStreamToleranceOptionSide_t=DeliveryStreamToleranceOptionSide_enum_t;

using LegDeliveryStreamElectingPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using LegMarketDisruptionProvision_t=MarketDisruptionProvision_enum_t;

using LegMarketDisruptionFallbackProvision_t=MarketDisruptionFallbackProvision_enum_t;

using LegMarketDisruptionFallbackUnderlierType_t=MarketDisruptionFallbackUnderlierType_enum_t;

using LegMarketDisruptionFallbackUnderlierSecurityIDSource_t=SecurityIDSource_enum_t;

using LegExerciseConfirmationMethod_t=ExerciseConfirmationMethod_enum_t;

using LegOptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegOptionExerciseEarliestDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegOptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using LegOptionExerciseFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using LegOptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegOptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegOptionExerciseDateType_t=OptionExerciseDateType_enum_t;

using LegOptionExerciseExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegOptionExerciseExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegOptionExerciseExpirationFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using LegOptionExerciseExpirationRollConvention_t=DateRollConvention_enum_t;

using LegOptionExerciseExpirationDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegOptionExerciseExpirationDateType_t=OptionExerciseDateType_enum_t;

using LegPaymentScheduleFixingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using LegPaymentScheduleRateUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentScheduleRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using LegPaymentScheduleSettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentScheduleStepUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentScheduleFixingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using LegPaymentScheduleFixingLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using LegPaymentScheduleFixingFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamFixedAmountUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentStreamRateIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegPaymentStreamRateIndexUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentStreamSettlLevel_t=PaymentStreamSettlLevel_enum_t;

using LegPaymentStreamReferenceLevelUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentStreamRateSpreadUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegPaymentStreamRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using LegPaymentStreamCalculationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using LegPaymentStreamFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamPricingDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegPaymentStreamPricingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using LegPaymentStreamPricingBusinessDayConvention_t=BusinessDayConvention_enum_t;

using StreamCommoditySettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using LegPaymentStreamPaymentDateType_t=OptionExerciseDateType_enum_t;

using LegPaymentStreamPricingDateType_t=OptionExerciseDateType_enum_t;

using LegPaymentStreamPricingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using LegPricingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegProtectionTermEventUnit_t=ProtectionTermEventUnit_enum_t;

using LegProtectionTermEventDayType_t=PaymentStreamInflationLagDayType_enum_t;

using LegProtectionTermEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using LegStreamCalculationPeriodDateType_t=OptionExerciseDateType_enum_t;

using LegStreamCalculationCorrectionUnit_t=ProtectionTermEventUnit_enum_t;

using LegStreamCommoditySecurityIDSource_t=SecurityIDSource_enum_t;

using LegStreamCommodityUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegStreamCommodityNearbySettlDayUnit_t=StreamCommodityNearbySettlDayUnit_enum_t;

using LegStreamCommoditySettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamCommoditySettlDateRollUnit_t=StreamCommoditySettlDateRollUnit_enum_t;

using LegStreamCommoditySettlDayType_t=ComplexEventDateOffsetDayType_enum_t;

using LegStreamCommodityDataSourceIDType_t=StreamCommodityDataSourceIDType_enum_t;

using LegStreamCommoditySettlDay_t=DeliveryScheduleSettlDay_enum_t;

using LegStreamCommoditySettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using LegStreamCommoditySettlPeriodNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegStreamCommoditySettlPeriodFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using LegStreamCommoditySettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegStreamCommoditySettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using UnderlyingAdditionalTermBondSecurityIDSource_t=SecurityIDSource_enum_t;

using LegStreamNotionalFrequencyUnit_t=TimeUnit_enum_t;

using LegStreamNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using LegStreamNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using LegStreamTotalNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingComplexEventCreditEventUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingComplexEventCreditEventDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingComplexEventCreditEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using UnderlyingComplexEventPeriodType_t=ComplexEventPeriodType_enum_t;

using UnderlyingComplexEventRateSource_t=RateSource_enum_t;

using UnderlyingComplexEventRateSourceType_t=RateSourceType_enum_t;

using UnderlyingComplexEventDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingComplexEventDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingComplexEventDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingComplexEventScheduleFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingComplexEventScheduleRollConvention_t=DateRollConvention_enum_t;

using UnderlyingDeliveryScheduleType_t=DeliveryScheduleType_enum_t;

using UnderlyingDeliveryScheduleNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingDeliveryScheduleNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using UnderlyingDeliveryScheduleToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingDeliveryScheduleToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using UnderlyingDeliveryScheduleSettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using UnderlyingDeliveryScheduleSettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using UnderlyingDeliveryScheduleSettlDay_t=DeliveryScheduleSettlDay_enum_t;

using UnderlyingDeliveryScheduleSettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using UnderlyingDeliveryStreamType_t=DeliveryStreamType_enum_t;

using UnderlyingDeliveryStreamDeliveryRestriction_t=DeliveryStreamDeliveryRestriction_enum_t;

using UnderlyingDeliveryStreamDeliveryContingentPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using UnderlyingDeliveryStreamTitleTransferCondition_t=DeliveryStreamTitleTransferCondition_enum_t;

using UnderlyingDeliveryStreamToleranceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingDeliveryStreamToleranceType_t=DeliveryScheduleToleranceType_enum_t;

using UnderlyingDeliveryStreamToleranceOptionSide_t=DeliveryStreamToleranceOptionSide_enum_t;

using UnderlyingDeliveryStreamElectingPartySide_t=DeliveryStreamElectingPartySide_enum_t;

using UnderlyingExerciseConfirmationMethod_t=ExerciseConfirmationMethod_enum_t;

using UnderlyingOptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingOptionExerciseEarliestDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingOptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using UnderlyingOptionExerciseFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingOptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingOptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingOptionExerciseDateType_t=OptionExerciseDateType_enum_t;

using UnderlyingOptionExerciseExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingOptionExerciseExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingOptionExerciseExpirationFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingOptionExerciseExpirationRollConvention_t=DateRollConvention_enum_t;

using UnderlyingOptionExerciseExpirationDateOffsetDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingOptionExerciseExpirationDateType_t=OptionExerciseDateType_enum_t;

using UnderlyingMarketDisruptionProvision_t=MarketDisruptionProvision_enum_t;

using UnderlyingMarketDisruptionFallbackProvision_t=MarketDisruptionFallbackProvision_enum_t;

using UnderlyingMarketDisruptionFallbackUnderlierType_t=MarketDisruptionFallbackUnderlierType_enum_t;

using UnderlyingMarketDisruptionFallbackUnderlierSecurityIDSource_t=SecurityIDSource_enum_t;

using UnderlyingPaymentScheduleFixingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using UnderlyingPaymentScheduleRateUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentScheduleRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using UnderlyingPaymentScheduleSettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentScheduleStepUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentScheduleFixingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using UnderlyingPaymentScheduleFixingLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using UnderlyingPaymentScheduleFixingFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamFixedAmountUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentStreamRateIndex2CurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingPaymentStreamRateIndexUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentStreamSettlLevel_t=PaymentStreamSettlLevel_enum_t;

using UnderlyingPaymentStreamReferenceLevelUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentStreamRateSpreadUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingPaymentStreamRateSpreadType_t=PaymentStreamRateSpreadType_enum_t;

using UnderlyingPaymentStreamCalculationLagUnit_t=PaymentStreamInflationLagUnit_enum_t;

using UnderlyingPaymentStreamFirstObservationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamPricingDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamPricingDayDistribution_t=PaymentStreamPricingDayDistribution_enum_t;

using UnderlyingPaymentStreamPricingBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegStreamCommoditySettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using UnderlyingStreamCommoditySettlTimeType_t=DeliveryScheduleSettlTimeType_enum_t;

using UnderlyingPaymentStreamPaymentDateType_t=OptionExerciseDateType_enum_t;

using UnderlyingPaymentStreamPricingDateType_t=OptionExerciseDateType_enum_t;

using UnderlyingPaymentStreamPricingDayOfWeek_t=PaymentStreamPricingDayOfWeek_enum_t;

using UnderlyingPricingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamCalculationPeriodDateType_t=OptionExerciseDateType_enum_t;

using UnderlyingStreamCalculationCorrectionUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingStreamCommoditySecurityIDSource_t=SecurityIDSource_enum_t;

using UnderlyingStreamCommodityUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingStreamCommodityNearbySettlDayUnit_t=StreamCommodityNearbySettlDayUnit_enum_t;

using UnderlyingStreamCommoditySettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingStreamCommoditySettlDateRollUnit_t=StreamCommoditySettlDateRollUnit_enum_t;

using UnderlyingStreamCommoditySettlDayType_t=ComplexEventDateOffsetDayType_enum_t;

using UnderlyingStreamCommodityDataSourceIDType_t=StreamCommodityDataSourceIDType_enum_t;

using UnderlyingStreamCommoditySettlDay_t=DeliveryScheduleSettlDay_enum_t;

using UnderlyingStreamCommoditySettlFlowType_t=DeliveryScheduleSettlFlowType_enum_t;

using UnderlyingStreamCommoditySettlPeriodNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingStreamCommoditySettlPeriodFrequencyUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingStreamCommoditySettlPeriodPriceUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingStreamCommoditySettlHolidaysProcessingInstruction_t=DeliveryScheduleSettlHolidaysProcessingInstruction_enum_t;

using UnderlyingStreamNotionalFrequencyUnit_t=TimeUnit_enum_t;

using UnderlyingStreamNotionalCommodityFrequency_t=StreamNotionalCommodityFrequency_enum_t;

using UnderlyingStreamNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingStreamTotalNotionalUnitOfMeasure_t=UnitOfMeasure_enum_t;

using UnderlyingAdditionalTermBondSeniority_t=Seniority_enum_t;

using UnderlyingAdditionalTermBondCouponType_t=CouponType_enum_t;

using UnderlyingAdditionalTermBondCouponFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingAdditionalTermBondDayCount_t=CouponDayCount_enum_t;

using UnderlyingCashSettlQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using UnderlyingCashSettlValuationMethod_t=CashSettlValuationMethod_enum_t;

using UnderlyingProtectionTermEventUnit_t=ProtectionTermEventUnit_enum_t;

using UnderlyingProtectionTermEventDayType_t=PaymentStreamInflationLagDayType_enum_t;

using UnderlyingProtectionTermEventQualifier_t=ProtectionTermEventQualifier_enum_t;

using UnderlyingProvisionCashSettlPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionCashSettlPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingProvisionCashSettlPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionCashSettlPaymentDateType_t=ProvisionCashSettlPaymentDateType_enum_t;

using UnderlyingProvisionCashSettlQuoteSource_t=PaymentStreamRateIndexSource_enum_t;

using UnderlyingProvisionCashSettlValueDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionCashSettlValueDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingProvisionCashSettlValueDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionOptionExerciseFixedDateType_t=ProvisionOptionExerciseFixedDateType_enum_t;

using UnderlyingProvisionOptionExerciseBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionOptionExerciseEarliestDateOffsetUnit_t=ProvisionOptionExerciseEarliestDateOffsetUnit_enum_t;

using UnderlyingProvisionOptionExerciseFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingProvisionOptionExerciseStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingProvisionOptionExerciseStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionOptionExpirationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionOptionExpirationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingProvisionOptionExpirationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionOptionRelevantUnderlyingDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionOptionRelevantUnderlyingDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingProvisionOptionRelevantUnderlyingDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionType_t=ProvisionType_enum_t;

using UnderlyingProvisionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingProvisionDateTenorUnit_t=ProvisionDateTenorUnit_enum_t;

using UnderlyingProvisionCalculationAgent_t=ProvisionCalculationAgent_enum_t;

using UnderlyingProvisionOptionSinglePartyBuyerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using UnderlyingProvisionOptionSinglePartySellerSide_t=ProvisionOptionSinglePartyBuyerSide_enum_t;

using UnderlyingProvisionOptionExerciseStyle_t=ExerciseStyle_enum_t;

using UnderlyingProvisionCashSettlMethod_t=ProvisionCashSettlMethod_enum_t;

using UnderlyingProvisionCashSettlQuoteType_t=ProvisionCashSettlQuoteType_enum_t;

using UnderlyingProvisionPartyIDSource_t=PartyIDSource_enum_t;

using UnderlyingProvisionPartyRole_t=PartyRole_enum_t;

using UnderlyingProvisionPartySubIDType_t=PartySubIDType_enum_t;

using DeliveryStreamDeliveryPointSource_t=DeliveryStreamDeliveryPointSource_enum_t;

using LegDeliveryStreamDeliveryPointSource_t=DeliveryStreamDeliveryPointSource_enum_t;

using UnderlyingDeliveryStreamDeliveryPointSource_t=DeliveryStreamDeliveryPointSource_enum_t;

using CashSettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using CashSettlDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using CashSettlDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using CashSettlPriceDefault_t=CashSettlPriceDefault_enum_t;

using DividendFloatingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using DividendFloatingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using DividendFloatingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using DividendCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using DividendCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using DividendFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using DividendFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using DividendFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using DividendAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using DividendNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using DividendAccrualPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using DividendAccrualPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using DividendAccrualPaymeentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using DividendEntitlementEvent_t=DividendEntitlementEvent_enum_t;

using DividendAmountType_t=DividendAmountType_enum_t;

using ExtraordinaryDividendPartySide_t=PaymentStreamCapRateBuySide_enum_t;

using ExtraordinaryDividendAmountType_t=DividendAmountType_enum_t;

using DividendCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using NonCashDividendTreatment_t=NonCashDividendTreatment_enum_t;

using DividendComposition_t=DividendComposition_enum_t;

using DividendFXTriggerDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using DividendFXTriggerDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using DividendFXTriggerDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using DividendPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using DividendPeriodValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using DividendPeriodValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using DividendPeriodPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using DividendPeriodPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegCashSettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegCashSettlDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegCashSettlDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegCashSettlPriceDefault_t=CashSettlPriceDefault_enum_t;

using LegDividendFloatingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegDividendFloatingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegDividendFloatingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegDividendCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using LegDividendCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using LegDividendFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegDividendFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegDividendFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using LegDividendAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using LegDividendNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using LegDividendAccrualPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegDividendAccrualPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegDividendAccrualPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegDividendEntitlementEvent_t=DividendEntitlementEvent_enum_t;

using LegDividendAmountType_t=DividendAmountType_enum_t;

using LegExtraordinaryDividendPartySide_t=PaymentStreamCapRateBuySide_enum_t;

using LegExtraordinaryDividendAmountType_t=DividendAmountType_enum_t;

using LegDividendCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using LegNonCashDividendTreatment_t=NonCashDividendTreatment_enum_t;

using LegDividendComposition_t=DividendComposition_enum_t;

using LegDividendFXTriggerDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegDividendFXTriggerDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegDividendFXTriggerDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegDividendPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegDividendPeriodValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegDividendPeriodValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegDividendPeriodPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegDividendPeriodPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegSettlMethodElectingPartySide_t=PaymentPaySide_enum_t;

using LegMakeWholeBenchmarkQuote_t=StrikeIndexQuote_enum_t;

using LegMakeWholeInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using LegPaymentStreamInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using LegPaymentStreamInterpolationPeriod_t=PaymentStreamInterpolationPeriod_enum_t;

using LegPaymentStreamCompoundingDateType_t=NonDeliverableFixingDateType_enum_t;

using LegPaymentStreamCompoundingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStreamCompoundingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamCompoundingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamCompoundingFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegPaymentStreamCompoundingRollConvention_t=DateRollConvention_enum_t;

using LegPaymentStreamCompoundingEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamCompoundingEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamCompoundingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using LegPaymentStreamCompoundingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using LegPaymentStreamCompoundingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using LegPaymentStreamCompoundingCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStreamCompoundingCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using LegPaymentStreamCompoundingFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStreamCompoundingFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using LegPaymentStreamCompoundingFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using LegPaymentStreamCompoundingAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using LegPaymentStreamCompoundingNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using LegPaymentStreamCompoundingStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamCompoundingStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamFinalPricePaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStreamFinalPricePaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using LegPaymentStreamFirstObservationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStreamLinkStrikePriceType_t=PaymentStreamLinkStrikePriceType_enum_t;

using LegPaymentStreamRealizedVarianceMethod_t=PaymentStreamRealizedVarianceMethod_enum_t;

using LegPaymentStubEndDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStubEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStubEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegPaymentStubStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegPaymentStubStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegPaymentStubStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegProvisionBreakFeeElection_t=ProvisionBreakFeeElection_enum_t;

using LegReturnRateDateMode_t=ReturnRateDateMode_enum_t;

using LegReturnRateValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegReturnRateValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegReturnRateValuationStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegReturnRateValuationStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegReturnRateValuationEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegReturnRateValuationEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegReturnRateValuationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using LegReturnRateValuationFrequencyRollConvention_t=DateRollConvention_enum_t;

using LegReturnRateValuationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegReturnRateFXRateCalc_t=SettlCurrFxRateCalc_enum_t;

using LegReturnRatePriceSequence_t=ReturnRatePriceSequence_enum_t;

using LegReturnRateCommissionBasis_t=CommType_enum_t;

using LegReturnRateQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using LegReturnRateQuoteTimeType_t=ReturnRateQuoteTimeType_enum_t;

using LegReturnRateValuationTimeType_t=ReturnRateQuoteTimeType_enum_t;

using LegReturnRateValuationPriceOption_t=ReturnRateValuationPriceOption_enum_t;

using LegReturnRateFinalPriceFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using LegReturnRateInformationSource_t=RateSource_enum_t;

using LegReturnRatePriceBasis_t=ReturnRatePriceBasis_enum_t;

using LegReturnRatePriceType_t=ReturnRatePriceType_enum_t;

using LegReturnRateValuationDateType_t=NonDeliverableFixingDateType_enum_t;

using LegSettlMethodElectionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using LegSettlMethodElectionDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using LegSettlMethodElectionDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using LegStreamNotionalAdjustments_t=StreamNotionalAdjustments_enum_t;

using SettlMethodElectingPartySide_t=PaymentPaySide_enum_t;

using MakeWholeBenchmarkQuote_t=StrikeIndexQuote_enum_t;

using MakeWholeInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using PaymentStreamInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using PaymentStreamInterpolationPeriod_t=PaymentStreamInterpolationPeriod_enum_t;

using PaymentStreamCompoundingDateType_t=NonDeliverableFixingDateType_enum_t;

using PaymentStreamCompoundingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStreamCompoundingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamCompoundingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamCompoundingFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using PaymentStreamCompoundingRollConvention_t=DateRollConvention_enum_t;

using PaymentStreamCompoundingEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamCompoundingEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamCompoundingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using PaymentStreamCompoundingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using PaymentStreamCompoundingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using PaymentStreamCompoundingCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStreamCompoundingCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using PaymentStreamCompoundingFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStreamCompoundingFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using PaymentStreamCompoundingFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using PaymentStreamCompoundingAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using PaymentStreamCompoundingNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using PaymentStreamCompoundingStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamCompoundingStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamFinalPricePaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStreamFinalPricePaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using PaymentStreamFirstObservationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStreamLinkStrikePriceType_t=PaymentStreamLinkStrikePriceType_enum_t;

using PaymentStreamRealizedVarianceMethod_t=PaymentStreamRealizedVarianceMethod_enum_t;

using PaymentStubEndDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStubEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStubEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using PaymentStubStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using PaymentStubStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using PaymentStubStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ProvisionBreakFeeElection_t=ProvisionBreakFeeElection_enum_t;

using ReturnRateDateMode_t=ReturnRateDateMode_enum_t;

using ReturnRateValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ReturnRateValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ReturnRateValuationStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ReturnRateValuationStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ReturnRateValuationEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using ReturnRateValuationEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using ReturnRateValuationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using ReturnRateValuationFrequencyRollConvention_t=DateRollConvention_enum_t;

using ReturnRateValuationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using ReturnRateFXRateCalc_t=SettlCurrFxRateCalc_enum_t;

using ReturnRatePriceSequence_t=ReturnRatePriceSequence_enum_t;

using ReturnRateCommissionBasis_t=CommType_enum_t;

using ReturnRateQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using ReturnRateQuoteTimeType_t=ReturnRateQuoteTimeType_enum_t;

using ReturnRateValuationTimeType_t=ReturnRateQuoteTimeType_enum_t;

using ReturnRateValuationPriceOption_t=ReturnRateValuationPriceOption_enum_t;

using ReturnRateFinalPriceFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using ReturnRateInformationSource_t=RateSource_enum_t;

using ReturnRatePriceBasis_t=ReturnRatePriceBasis_enum_t;

using ReturnRatePriceType_t=ReturnRatePriceType_enum_t;

using ReturnRateValuationDateType_t=NonDeliverableFixingDateType_enum_t;

using SettlMethodElectionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using SettlMethodElectionDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using SettlMethodElectionDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using StreamNotionalAdjustments_t=StreamNotionalAdjustments_enum_t;

using UnderlyingCashSettlDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingCashSettlDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingCashSettlDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingCashSettlPriceDefault_t=CashSettlPriceDefault_enum_t;

using UnderlyingDividendFloatingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingDividendFloatingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingDividendFloatingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingDividendCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingDividendCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingDividendFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingDividendFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingDividendFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using UnderlyingDividendAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using UnderlyingDividendNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using UnderlyingDividendAccrualPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingDividendAccrualPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingDividendAccrualPaymentDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingDividendEntitlementEvent_t=DividendEntitlementEvent_enum_t;

using UnderlyingDividendAmountType_t=DividendAmountType_enum_t;

using UnderlyingExtraordinaryDividendPartySide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingExtraordinaryDividendAmountType_t=DividendAmountType_enum_t;

using UnderlyingDividendCompoundingMethod_t=PaymentStreamCompoundingMethod_enum_t;

using UnderlyingNonCashDividendTreatment_t=NonCashDividendTreatment_enum_t;

using UnderlyingDividendComposition_t=DividendComposition_enum_t;

using UnderlyingDividendFXTriggerDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingDividendFXTriggerDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingDividendFXTriggerDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingDividendPeriodBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingDividendPeriodValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingDividendPeriodValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingDividendPeriodPaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingDividendPeriodPaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingSettlMethodElectingPartySide_t=PaymentPaySide_enum_t;

using UnderlyingMakeWholeBenchmarkQuote_t=StrikeIndexQuote_enum_t;

using UnderlyingMakeWholeInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using UnderlyingPaymentStreamInterpolationMethod_t=PaymentStreamInflationInterpolationMethod_enum_t;

using UnderlyingPaymentStreamInterpolationPeriod_t=PaymentStreamInterpolationPeriod_enum_t;

using UnderlyingPaymentStreamCompoundingDateType_t=NonDeliverableFixingDateType_enum_t;

using UnderlyingPaymentStreamCompoundingDatesBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStreamCompoundingDatesOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamCompoundingDatesOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamCompoundingFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingPaymentStreamCompoundingRollConvention_t=DateRollConvention_enum_t;

using UnderlyingPaymentStreamCompoundingEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamCompoundingEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamCompoundingRateIndexCurveUnit_t=PaymentStreamRateIndexCurveUnit_enum_t;

using UnderlyingPaymentStreamCompoundingRateSpreadPositionType_t=PaymentStreamRateSpreadPositionType_enum_t;

using UnderlyingPaymentStreamCompoundingRateTreatment_t=PaymentStreamRateTreatment_enum_t;

using UnderlyingPaymentStreamCompoundingCapRateBuySide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStreamCompoundingCapRateSellSide_t=PaymentStreamCapRateBuySide_enum_t;

using UnderlyingPaymentStreamCompoundingFloorRateBuySide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStreamCompoundingFloorRateSellSide_t=PaymentStreamFloorRateBuySide_enum_t;

using UnderlyingPaymentStreamCompoundingFinalRateRoundingDirection_t=RoundingDirection_enum_t;

using UnderlyingPaymentStreamCompoundingAveragingMethod_t=PaymentStreamAveragingMethod_enum_t;

using UnderlyingPaymentStreamCompoundingNegativeRateTreatment_t=PaymentStreamNegativeRateTreatment_enum_t;

using UnderlyingPaymentStreamCompoundingStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamCompoundingStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamFinalPricePaymentDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStreamFinalPricePaymentDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamFixingDateType_t=NonDeliverableFixingDateType_enum_t;

using UnderlyingPaymentStreamFirstObservationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStreamLinkStrikePriceType_t=PaymentStreamLinkStrikePriceType_enum_t;

using UnderlyingPaymentStreamRealizedVarianceMethod_t=PaymentStreamRealizedVarianceMethod_enum_t;

using UnderlyingPaymentStubEndDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStubEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStubEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingPaymentStubStartDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingPaymentStubStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingPaymentStubStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingProvisionBreakFeeElection_t=ProvisionBreakFeeElection_enum_t;

using UnderlyingReturnRateDateMode_t=ReturnRateDateMode_enum_t;

using UnderlyingReturnRateValuationDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingReturnRateValuationDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingReturnRateValuationStartDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingReturnRateValuationStartDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingReturnRateValuationEndDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingReturnRateValuationEndDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingReturnRateValuationFrequencyUnit_t=CouponFrequencyUnit_enum_t;

using UnderlyingReturnRateValuationFrequencyRollConvention_t=DateRollConvention_enum_t;

using UnderlyingReturnRateValuationDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingReturnRateFXRateCalc_t=SettlCurrFxRateCalc_enum_t;

using UnderlyingReturnRatePriceSequence_t=ReturnRatePriceSequence_enum_t;

using UnderlyingReturnRateCommissionBasis_t=CommType_enum_t;

using UnderlyingReturnRateQuoteMethod_t=CashSettlQuoteMethod_enum_t;

using UnderlyingReturnRateQuoteTimeType_t=ReturnRateQuoteTimeType_enum_t;

using UnderlyingReturnRateValuationTimeType_t=ReturnRateQuoteTimeType_enum_t;

using UnderlyingReturnRateValuationPriceOption_t=ReturnRateValuationPriceOption_enum_t;

using UnderlyingReturnRateFinalPriceFallback_t=ComplexEventPVFinalPriceElectionFallback_enum_t;

using UnderlyingReturnRateInformationSource_t=RateSource_enum_t;

using UnderlyingReturnRatePriceBasis_t=ReturnRatePriceBasis_enum_t;

using UnderlyingReturnRatePriceType_t=ReturnRatePriceType_enum_t;

using UnderlyingReturnRateValuationDateType_t=NonDeliverableFixingDateType_enum_t;

using UnderlyingSettlMethodElectionDateBusinessDayConvention_t=BusinessDayConvention_enum_t;

using UnderlyingSettlMethodElectionDateOffsetUnit_t=PaymentStreamPaymentDateOffsetUnit_enum_t;

using UnderlyingSettlMethodElectionDateOffsetDayType_t=PaymentStreamPaymentDateOffsetDayType_enum_t;

using UnderlyingStreamNotionalAdjustments_t=StreamNotionalAdjustments_enum_t;

using LegPaymentStreamRateIndexIDSource_t=SecurityIDSource_enum_t;

using PaymentStreamRateIndexIDSource_t=SecurityIDSource_enum_t;

using UnderlyingPaymentStreamRateIndexIDSource_t=SecurityIDSource_enum_t;

using BatchProcessMode_t=BatchProcessMode_enum_t;

namespace FIXML_FIELDS_IMPL {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_FIXML_FIELDS_IMPL_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif
