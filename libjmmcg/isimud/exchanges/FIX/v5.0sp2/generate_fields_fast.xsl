?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nl "<xsl:text>
</xsl:text>">
	<!ENTITY tab "<xsl:text>&#x9;</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fm="http://www.fixprotocol.org/FIXML-5-0-SP2/METADATA">
<xsl:output omit-xml-declaration="yes" />
<xsl:param name="fname_hdr" required="yes" as="xs:string"/>
<xsl:param name="enum_identifier" required="yes" as="xs:string"/>
<xsl:param name="fix_version" required="yes" as="xs:string"/>
<xsl:include href="generate_bits.xsl"/>
<xsl:template name="make_mpl_enum">
	<xsl:param name="enum_name" required="yes"/>
	<xsl:param name="enum_tags" required="yes"/>
	<xsl:variable name="max_attr_len" select="max($enum_tags/xs:simpleType/xs:annotation/xs:appinfo/fm:Xref/@Tag/string-length(.))"/>
	<xsl:choose>
		<xsl:when test="$max_attr_len lt 7">
enum class <xsl:value-of select="$enum_name"/> : <xsl:call-template name="select_builtin">
				<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
			</xsl:call-template> {&nl;
	BodyLength=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>&apos;\001&apos;, &apos;9&apos;, &apos;=&apos;<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text>::value,</xsl:text>&nl;
	CheckSum=libjmmcg::enum_tags::mpl::to_tag<xsl:text disable-output-escaping="yes">&lt;</xsl:text>&apos;\001&apos;, &apos;1&apos;, &apos;0&apos;, &apos;=&apos;<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text>::value,</xsl:text>&nl;
			<xsl:for-each select="$enum_tags/xs:simpleType/xs:annotation/xs:appinfo/fm:Xref">
				&tab;<xsl:value-of select="@name"/><xsl:text>=libjmmcg::enum_tags::mpl::to_tag</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>&apos;\001&apos;, <xsl:call-template name="string_to_quoted_chars">
					<xsl:with-param name="text" select="@Tag"/>
				</xsl:call-template>, &apos;=&apos;<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value,&nl;
			</xsl:for-each>
	MatchAll=std::numeric_limits<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:call-template name="select_builtin">
		<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
	</xsl:call-template>
	<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::max()-1,	///<xsl:text disable-output-escaping="yes">&lt;</xsl:text> For the meta-state machine to allow a catch-all rule to reject anything unhandled.
	Exit=std::numeric_limits<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:call-template name="select_builtin">
		<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
	</xsl:call-template>
	<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::max()	///<xsl:text disable-output-escaping="yes">&lt;</xsl:text> For the meta-state machine: the exit state to exit the msm.
};
		</xsl:when>
		<xsl:otherwise>
			<!--
				Too many characters in the field tag. Logic totally broken. Game over. Sorry.
			-->
			FIXME
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="/">
<xsl:variable name="header_guard1" select="upper-case(replace(tokenize($fname_hdr, '/')[last()], '[-\./]', '_'))"/>
<xsl:variable name="header_guard" select="substring($header_guard1, 1, string-length($header_guard1)-12)"/>
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:value-of select="$header_guard"/>_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:value-of select="$header_guard"/>_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso20022.org/10383/iso-10383-market-identifier-code"/>
-->
#include "core/enum_as_char_array.hpp"

#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>cstdint<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>iostream<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>limits<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>type_traits<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

#define ISIMUD_<xsl:value-of select="$header_guard"/>_HDR_GENERATED_DATE "<xsl:value-of select="current-dateTime()"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace <xsl:value-of select="$fix_version"/> {
<xsl:call-template name="make_mpl_enum">
	<xsl:with-param name="enum_name" select="$enum_identifier"/>
	<xsl:with-param name="enum_tags" select="/xs:schema"/>
</xsl:call-template>
const std::size_t shortest_field_length=<xsl:value-of select="3+min(/xs:schema/xs:simpleType/xs:annotation/xs:appinfo/fm:Xref/@Tag/string-length(.))"/>;
<xsl:call-template name="make_mpl_enum_operators">
	<xsl:with-param name="enum_name" select="$enum_identifier"/>
</xsl:call-template>
namespace <xsl:value-of select="$header_guard"/> {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_<xsl:value-of select="$header_guard"/>_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
