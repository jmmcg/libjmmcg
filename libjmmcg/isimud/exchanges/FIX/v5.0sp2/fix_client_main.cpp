/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_client.hpp"

#include "core/logging.hpp"

#include <boost/asio.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <iostream>

using namespace libisimud;
using namespace libjmmcg;

int
main(int argc, char const* const* argv) {
	try {
		using thread_t= libjmmcg::ppd::jthread<ppd::platform_api, ppd::heavyweight_threading>;
		using fix_client_t= exchanges::FIX::v5_0sp2::fix_client;

		boost::program_options::options_description general(
			application::make_program_options(
				std::string{
					"A simple client that repeatedly sends a fixed FIX-message to a link listening at a IPADDR:PORT combination. The executable name indicates the message version implemented. For details regarding the properties of the client see the documentation that came with the distribution.\n\n"}
				+ application::wireshark_info + "\n\n" + application::make_help_message("consultant@isimud.ltd.uk")));
		boost::program_options::options_description client("FIX-translator options.");
		client.add_options()("address", boost::program_options::value<boost::asio::ip::address>()->default_value(boost::asio::ip::address_v4::loopback()), "IP address (in v4 format; thus the network interface to which it is bound) to which the messages should be sent.")("port", boost::program_options::value<unsigned short>()->required(), "The port to which the messages should be sent.")("processor", boost::program_options::value<unsigned short>()->required()->default_value(exchanges::common::thread_traits::client_to_exchange_thread.core), "The core to which the client should be bound.");
		boost::program_options::options_description performance("Performance-monitoring options.");
		performance.add_options()("num_loops", boost::program_options::value<unsigned long>()->required()->default_value(20000), "The number of times the fix message should be sent (and therefore received) in each batch.")("loops_for_conv", boost::program_options::value<unsigned short>()->required()->default_value(500), "The maximum number of times the batch should be run.")("perc_conv_estimate", boost::program_options::value<double>()->required()->default_value(2.0), "The maximum value of mean average-deviation to which the repeated runs of the batches should converge.");
		performance.add_options()(
			"num_timings",
			boost::program_options::value<unsigned long>()->required()->default_value(0UL),
			"The number (positive-integer) of latency timings to reserve. This value must be greater than the total number of messages to be processed, otherwise the behaviour is undefined. Upon exit, these values will be written in CSV-format to a file named 'argv[0]+\"-%%%%%.PID.csv\"' (where each '%' is a random hexadecimal digit and PID is the current process id). This file shall be created in the current directory. If the value is zero, then the file shall not be created, no values will be stored, unlimited messages may be processed.");
		boost::program_options::options_description logging("Logging options.");
		logging.add_options()("log_level", boost::program_options::value<libjmmcg::log::level_severity_type>()->default_value(libjmmcg::log::level_severity_type::trace, std::string(boost::log::trivial::to_string(libjmmcg::log::level_severity_type::trace)) + ", <" + libjmmcg::log::all_severities_to_string() + ">"), "Sets the level from which to log. Only log messages equal-to or higher will be logged. Default is to log everything.");
		boost::program_options::options_description all("All options.");
		boost::program_options::options_description locale("Locale options.");
		locale.add_options()("locale", boost::program_options::value<std::string>()->default_value(std::string(ISIMUD_LOCALE)), "Sets the locale for the server. This locale should have been installed by the 'isimud/scripts/postinst.in' when the program was installed.");
		all.add(locale);
		all.add(general).add(client).add(performance).add(logging);
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(auto const exit_code= application::check_basic_options(vm, all, " Built with FIX: " ISIMUD_FIX_EXCHANGE_VERSION, std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		} else {
			boost::program_options::notify(vm);
			thread_t::thread_traits::set_backtrace_on_signal();
			libjmmcg::log::set_global_level(vm["log_level"].as<libjmmcg::log::level_severity_type>());
			{
				[[maybe_unused]] auto const locale_err= std::setlocale(LC_ALL, vm["locale"].as<std::string>().c_str());
				DEBUG_ASSERT(locale_err != nullptr);
			}
			const std::size_t num_timestamps= vm["num_timings"].as<std::size_t>();
			latency_timestamps timestamps(num_timestamps);
			{
				fix_client_t sender(
					vm["address"].as<boost::asio::ip::address>(),
					vm["port"].as<unsigned short>(),
					socket::socket_priority::high,
					vm["processor"].as<unsigned short>(),
					timestamps);
				LIBJMMCG_LOG_TRIVIAL(info) << "Current process ID: " << std::dec << boost::interprocess::ipcdetail::get_current_process_id();
				LIBJMMCG_LOG_TRIVIAL(info) << sender;
				std::pair<fix_client_t::timed_results_t, bool> results;
				thread_t collect_stats(
					[&sender, &vm, &results]() {
						results= sender.in_order_tx_rx_stats(
							vm["num_loops"].as<unsigned long>(),
							vm["loops_for_conv"].as<unsigned short>(),
							vm["perc_conv_estimate"].as<double>());
					});
				thread_t::thread_traits::set_kernel_affinity(
					thread_t::thread_traits::get_current_thread(),
					thread_t::thread_traits::api_params_type::processor_mask_type(0));
				thread_t::thread_traits::set_kernel_priority(
					thread_t::thread_traits::get_current_thread(),
					thread_t::thread_traits::api_params_type::priority_type::idle);
				std::optional<lock_all_proc_mem> locker;
				try {
					locker.emplace(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
				} catch(...) {
					LIBJMMCG_LOG_TRIVIAL(warning) << "Unable to lock the memory of the process. If page-faulting occurs, it might affect performance. Continuing regardless... Details: " << boost::current_exception_diagnostic_information();
				}
				sender.exit_requested().wait(false);
				while(collect_stats.is_running()) {
					thread_t::thread_traits::sleep(30);
				}
				thread_t::thread_traits::set_kernel_priority(
					thread_t::thread_traits::api_threading_traits::get_current_thread(),
					thread_t::thread_traits::api_params_type::priority_type::normal);
				LIBJMMCG_LOG_TRIVIAL(info) << "Exchange round-trip in-order time (microseconds)=" << results.first << (results.second ? " converged." : " NOT converged.");
				LIBJMMCG_LOG_TRIVIAL(info) << sender;
				thread_t::thread_traits::raise(SIGQUIT);
			}
			timestamps.write_to_csv_file(std::clog, argv[0]);
			return exit_codes::codes::exit_success;
		}
	} catch(std::exception const& ex) {
		LIBJMMCG_LOG_TRIVIAL(error) << "STL-derived exception. Details: " << boost::diagnostic_information(ex);
		return exit_codes::codes::exit_stl_exception;
	} catch(...) {
		LIBJMMCG_LOG_TRIVIAL(error) << "Unknown exception. Details: " << boost::current_exception_diagnostic_information();
		return exit_codes::codes::exit_unknown_exception;
	}
}
