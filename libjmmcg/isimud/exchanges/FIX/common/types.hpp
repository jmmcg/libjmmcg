#ifndef ISIMUD_EXCHANGES_FIX_common_types_hpp
#define ISIMUD_EXCHANGES_FIX_common_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/isimud_config.h"

#include "core/enum_as_char_array.hpp"
#include "core/int128_compatibility.hpp"

#include <array>
#include <iostream>
#include <iterator>
#include <limits>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

using element_type= char;
enum : std::size_t {
	max_size_of_fix_message= sizeof(libjmmcg::uint128_t) * 32	///< The largest size of FIX message that the library can accommodate including all components, e.g. type, size & checksum fields. Note that this must be in units of 512 bits, due to the use of AVX2 operations, if available.
};
/// An underlying data-buffer into which the fix message may be created.
/**
	Ideally this should be suitably aligned, e.g. 64-it aligned. For optimal performance it would actually be the underlying memory of the network card or IP stack.
*/
using underlying_fix_data_buffer= std::array<element_type, max_size_of_fix_message>;
using pointer= element_type*;
using const_pointer= element_type const*;
/// The type of the internal data-buffer.
/**
	FIX messages are basically an array of chars. so we need a type that is compatible with this char-array that is received from a socket.
*/
using data_block_t= pointer;

using SecurityID_t= std::array<char, 20>;
using ClientOrderID_t= std::array<char, 20>;
using Reason_t= std::array<char, 20>;
using Password_t= std::array<char, 25>;
using UserName_t= std::array<char, 25>;
using Price_str_t= std::array<char, 25>;

inline constexpr const char Separator= '\001';
inline constexpr const unsigned short CheckSumLength= 3;

/// The checksum type used by FIX.
using checksum_t= std::array<char, CheckSumLength + 1>;

inline constexpr const std::uint64_t implied_decimal_places= 1;

/// When creating a FIX message it will always have this many chars. All this variable-sized stuff is a crock.
#define JMMCG_FIX_MSG_BODY_LENGTH_NULL "000"

#define JMMCG_FIX_MSG_BODY_LENGTH_TAG "\0019="
#define JMMCG_FIX_MSG_TYPE_TAG "\00135="

template<std::size_t Sz>
inline std::ostream&
to_stream(std::ostream& o, common::element_type const (&val)[Sz]) {
	const_pointer null_at= std::find(val, std::next(val, Sz), element_type{'\0'});
	o << std::string_view{val, null_at};
	return o;
}

}}}}}

#endif
