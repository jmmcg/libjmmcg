#ifndef ISIMUD_EXCHANGES_FIX_common_MESSAGES_HPP
#define ISIMUD_EXCHANGES_FIX_common_MESSAGES_HPP
/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "conversions.hpp"

#include "core/memops.hpp"
#include "core/non_allocatable.hpp"
#include "core/ttypes.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/mpl/assert.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <cstring>
#include <numeric>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

struct logon_args_t {
};

struct logoff_args_t {
};

template<
	class MsgVer	///< The version of FIX protocol to support.
	>
class [[gnu::packed]] Header {
public:
	using msg_version_t= MsgVer;
	using element_type= common::element_type;
	using pointer= element_type*;
	using const_pointer= element_type const*;
	using size_type= std::size_t;
	using MsgTypes_t= common::MsgType;
	using SeqNum_t= common::SeqNum_t;

	enum : bool {
		has_static_size= false	 ///< The message is statically-sized, not dynamically, so sizeof(the derived message-type) is the amount to copy, i.e. length() returns sizeof(the derived message-type).
	};

	enum : std::size_t {
		header_t_size= msg_version_t::fix_template_body_length_offset + sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) + 1
	};

	/// Create a FIX message from an array of chars read from a socket or elsewhere.
	/**
		In debug builds the validity of the FIX message is checked.

		\param	b	The array of chars that must comprise a valid FIX message.
	*/
	constexpr __stdcall Header() noexcept(true);

	/// Determine the total size of the referenced message.
	/**
		The referenced message must be valid.

		\return	The size of the whole the message, including the version, size (itself) and checksum fields.

		\see size()
	*/
	constexpr size_type length() const noexcept(true);

	/// Determine if the specified tag is in the referenced message.
	/**
		\param field	The field to be found. Note that the range must contain the field otherwise the behaviour shall be undefined.
		\return	True if the specified tag was found in the referenced message.
	*/
	template<FieldsFast field>
	constexpr std::string_view __fastcall find() noexcept(true);
	/// Determine if the specified tag is in the referenced message.
	/**
		\param field	The field to be found. Note that the range must contain the field otherwise the behaviour shall be undefined.
		\return	True if the specified tag was found in the referenced message.
	*/
	template<FieldsFast field>
	constexpr std::string_view __fastcall find() const noexcept(true);

	constexpr std::string_view __fastcall find_ExDestination() const noexcept(true);
	constexpr ExDestination_t __fastcall find_MIC() const noexcept(true);

	[[gnu::pure]] MsgTypes_t type() const noexcept(true);

	/// Verify that the referenced FIX message is valid.
	/**
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	constexpr bool is_valid() const noexcept(true);

	/// Generate the checksum for the referenced FIX message.
	/**
		\param	body_len	The length of the body of the referenced FIX message.
		\return The computed check sum.
	*/
	checksum_t
	generate_checksum(size_type body_len) const noexcept(true);

	/// Verify that the checksum of the referenced FIX message is valid.
	/**
		\param	body_len	The length of the body of the referenced FIX message.
		\param	start_of_checksum_value	A pointer to that start of the checksum value embedded in the referenced FIX message.
		\return	True is the checksum of the referenced FIX message is valid, false otherwise.
	*/
	constexpr bool is_checksum_valid(size_type body_len, const_pointer start_of_checksum_value) const noexcept(true);

protected:
	static_assert(max_size_of_fix_message > 0, "Buffer must be non-zero.");
	ALIGN_TO_L1_CACHE element_type begin_string[sizeof(msg_version_t::MsgVer) - 1];
	libjmmcg::enum_tags::mpl::to_array<FieldsFast::BodyLength>::element_type_no_null body_length_tag;
	element_type body_length_value[sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1];
	libjmmcg::enum_tags::mpl::to_array<FieldsFast::MsgType>::element_type_no_null msg_type_tag;
	underlying_fix_data_buffer data_;

	template<MsgTypes_t MsgType>
	constexpr underlying_fix_data_buffer::iterator
	set_header() noexcept(true);
	template<
		MsgTypes_t MsgType>
	constexpr underlying_fix_data_buffer::iterator
	set_sequence_num_int(SeqNum_t seq_num) noexcept(true);
	template<
		MsgTypes_t MsgType,
		class SrcMsg,
		class Ret= typename std::enable_if<std::is_class<SrcMsg>::value, underlying_fix_data_buffer::iterator>::type>
	constexpr Ret
	set_sequence_num(SrcMsg const& msg) noexcept(true);
};

/// The basic, underlying type used to implement FIX message reading.
/**
 *	Wraps a FIX message contained in an array of chars that has been written into from a socket or may be created via various constructor overloads.
 * FIXimate at <a href="https://fiximate.fixtrading.org"/> is particularly useful.
 */
template<
	class MsgVer	///< The version of FIX protocol to support.
	>
class [[gnu::packed]] Message : public Header<MsgVer> {
public:
	using Header_t= Header<MsgVer>;
	using msg_version_t= typename Header_t::msg_version_t;
	using element_type= typename Header_t::element_type;
	using pointer= typename Header_t::pointer;
	using const_pointer= typename Header_t::const_pointer;
	using size_type= typename Header_t::size_type;
	using RejectCode_t= common::RejectCode_t;
	using SeqNum_t= typename Header_t::SeqNum_t;
	using MsgTypes_t= typename Header_t::MsgTypes_t;

	static inline constexpr const MsgTypes_t static_type= msg_version_t::msg_type::tag;
	static inline constexpr const RejectCode_t unknown_msg= RejectCode_t::Invalid_MsgType;

	explicit constexpr __stdcall Message() noexcept(true);
	explicit constexpr __stdcall Message(logon_args_t const&) noexcept(true);
	explicit constexpr __stdcall Message(logoff_args_t const&) noexcept(true);
	/// Construct a FIX Reject message.
	template<
		std::size_t N,
		typename std::enable_if<(N > 1), std::size_t>::type Sz= N - 1	 ///< One less as I do not want to copy the terminal '\0' at the end of the C-style string literal.
		>
	constexpr Message(SeqNum_t ref_seq_num, RejectCode_t rc, char const (&msg)[N]) noexcept(true);
	/// Create a NewOrderSIngle.
	__stdcall Message(SeqNum_t seqNum, ClientOrderID_t const& clID, OrderType const oT, TIF const t, Side const s, SecurityID_t instID, Quantity_t ordQty, Price_t p, ExDestination_t mic) noexcept(true);
	/// Create an ExecutionReport.
	__stdcall Message(SeqNum_t seqNum, ClientOrderID_t const& clID, ExecType eT, Price_t const price, SecurityID_t instID, Side s, Quantity_t execdQty, Quantity_t leavesQty, ExDestination_t mic) noexcept(true);
	/// Create a message from the source message, into the underlying buffer.
	/**
		In debug builds the validity of the FIX message is checked.

		If an error is generated, then this function will need to be specialised for the particular SrcMsg.

		\param	msg	The source message from which the target message should be created.
	*/
	template<
		class SrcMsg	///< The type of message from which this FIX message should be created. E.g. a MIT-type, BATSBOE, etc.
		>
	__stdcall explicit Message(SrcMsg const& msg)= delete;
	/// Create a message from the source message, into the underlying buffer.
	/**
		In debug builds the validity of the FIX message is checked.

		If an error is generated, then this function will need to be specialised for the particular SrcMsg.

		\param	msg	The source message from which the target message should be created.
		\param	ref_data	The reference data to map from exchange-based instrument identifiers to client-based symbology.
	*/
	template<
		class SrcMsg,	 ///< The type of message from which this FIX message should be created. E.g. a MIT-type, BATSBOE, etc.
		class RefData>
	__stdcall Message(SrcMsg const& msg, RefData const& ref_data)= delete;

	/*
		\todo The idea is that the fields that we wish to comprise the FIX message from are passed as a set of template parameters, so that the FIX message can be largely constructed at compile-time, thus being highly efficient.
	*/
	template<
		class RefData,
		class... Fields>
	Message(RefData const& ref_data, Fields... fields);

	/// Determine the size of the referenced message.
	/**
		The referenced message must be valid.

		\return	The size of the body of the message, excluding the version, size (itself) and checksum fields.

		\see length()
	*/
	constexpr size_type __fastcall size() const noexcept(true);

	/// Determine if the specified tag is in the referenced message.
	/**
		\param field	The field to be found. Note that the range must contain the field otherwise the behaviour shall be undefined.
		\return	True if the specified tag was found in the referenced message.
	*/
	template<FieldsFast field>
	constexpr bool __fastcall search() const noexcept(true);

	/// Verify that the referenced FIX message is valid.
	/**
		\return	True is the referenced FIX message is valid, false otherwise.
	*/
	constexpr bool is_valid() const noexcept(true);
	constexpr bool has_ISIN() const noexcept(true);
	constexpr bool has_ExDest() const noexcept(true);

	std::string to_string() const noexcept(false);

	constexpr ClientOrderID_t clientOrderID() const noexcept(true);
	constexpr Side side() const noexcept(true);
	constexpr Price_t limitPrice() const noexcept(true);
	constexpr Quantity_t orderQty() const noexcept(true);
	constexpr void orderQty(Quantity_t q) noexcept(true);
	constexpr SecurityID_t instrumentID() const noexcept(true);

	template<class OutIter>
	static OutIter write_ClOrdID(ClientOrderID_t const& clID, OutIter data) noexcept(true);
	template<class OutIter>
	static OutIter write_TransactTime(OutIter data) noexcept(true);
	template<class OutIter>
	static OutIter write_ExDest(ExDestination_t mic, OutIter data) noexcept(true);

	template<FieldsFast field>
	static constexpr underlying_fix_data_buffer::iterator
	add_field_tag(underlying_fix_data_buffer::iterator data) noexcept(true);

protected:
	constexpr void finalise_msg(underlying_fix_data_buffer::iterator) noexcept(true);

private:
	static constexpr underlying_fix_data_buffer::iterator
	add_field_tag(FieldsFast field, underlying_fix_data_buffer::iterator data) noexcept(true);
};

template<class MsgVer>
std::ostream&
operator<<(std::ostream& os, Message<MsgVer> const& m);

}}}}}

#include "messages_impl.hpp"

#endif
