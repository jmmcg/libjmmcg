#ifndef ISIMUD_EXCHANGES_FIX_common_CONVERSIONS_HPP
#define ISIMUD_EXCHANGES_FIX_common_CONVERSIONS_HPP
/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * \file This should be included *after* "libjmmcg/isimud/exchanges/FIX/v5.0sp2/types.hpp", othereise it misses important declarations, e.g. Price_t.
 */

#include "core/memops.hpp"
#include "core/ttypes.hpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wold-style-cast"

#include <libassert/assert.hpp>

#pragma GCC diagnostic pop

#include <algorithm>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

/**
	Assumes the symbol is an ISIN.
*/
inline std::size_t
convert(SecurityID_t const& i, pointer buff, std::size_t sz) {
	DEBUG_ASSERT(i.max_size() <= sz);
	const std::size_t sz_to_copy= std::min(i.max_size(), sz);
	std::copy_n(i.begin(), sz_to_copy, buff);
	return sz_to_copy;
}

inline std::size_t
convert(std::int32_t a, pointer buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::uint32_t a, pointer buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::int64_t a, pointer buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::uint64_t a, pointer buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(FIX::common::Price_t const& a, pointer buff, std::size_t sz) {
	const double c= static_cast<double>(a) / implied_decimal_places;
	return libjmmcg::tostring(c, buff, sz);
}

template<class Ret>
[[gnu::pure]] Ret
convert(std::string_view const&)
	= delete;

template<>
inline std::int32_t
convert<std::int32_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<std::int32_t>(libjmmcg::fromstring<long>(a.begin(), a.size()));
}

template<>
inline std::uint32_t
convert<std::uint32_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<std::uint32_t>(libjmmcg::fromstring<long>(a.begin(), a.size()));
}

template<>
inline std::uint64_t
convert<std::uint64_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return static_cast<std::uint64_t>(libjmmcg::fromstring<long>(a.begin(), a.size()));
}

template<>
inline std::string
convert<std::string>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return {a.begin(), a.end()};
}

template<>
inline Quantity_t
convert<Quantity_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	return libjmmcg::fromstring<Quantity_t>(a.begin(), a.size());
}

template<>
inline SecurityID_t
convert<SecurityID_t>(std::string_view const& a) {
	DEBUG_ASSERT(!a.empty());
	SecurityID_t tmp{};
	DEBUG_ASSERT(a.size() <= tmp.size());
	std::memcpy(tmp.begin(), a.begin(), a.size());
	return tmp;
}

}}}}}

#endif
