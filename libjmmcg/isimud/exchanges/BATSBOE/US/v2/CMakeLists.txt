## Copyright (c) 2015 by J.M.McGuiness, isimud@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA* 02111-1307 USA

set(JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION "v2_3_6" CACHE STRING "The version of the BATSBOE exchange that is supported.")

set(JMMCG_${PROJECT_NAME}_SpecificationURL "http://cdn.cboe.com/resources/membership/Cboe_US_Equities_BOE_Specification.pdf")
get_filename_component(JMMCG_Copy${PROJECT_NAME}SpecificationName ${JMMCG_${PROJECT_NAME}_SpecificationURL} NAME)
add_custom_target(Copy_${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_Specifications
	COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../docs/exchanges/${PROJECT_NAME}/${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}
	COMMAND ${WGET_EXECUTABLE} ${JMMCG_MAKE_WGET_VERY_ROBUST} --quiet --output-document=${CMAKE_CURRENT_SOURCE_DIR}/../../../../../docs/exchanges/${PROJECT_NAME}/${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}/${JMMCG_Copy${PROJECT_NAME}SpecificationName} ${JMMCG_${PROJECT_NAME}_SpecificationURL}
)

configure_file(config.h.in ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}_v2_config.h)

set(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_TARGET_SOURCES
	batsboe.cpp
	batsboe.hpp
	messages.cpp
	messages.hpp
	messages_impl.hpp
)
add_library_with_options(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} "${${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_TARGET_SOURCES}")
add_link_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} jmmcg)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} jmmcg)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} Copy_${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_Specifications)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} ConvertISOSpecsToCXX)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} FindPerfectHashMaskForBATSBOEUSSimMsgs)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} FindPerfectHashMaskForBATSBOEExchgMsgs)
add_dependency_target_details(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} FindPerfectHashMaskForFIXMsgs)
target_compile_definitions(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_objlib
	PUBLIC
		LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_FIX_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_MSGS}"
)
target_include_directories(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_objlib
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<BUILD_INTERFACE:${ConvertISOCurrencySpecToCXX_INCLUDE_DIR}>
		$<BUILD_INTERFACE:${ConvertISOCurrencySpecToCXX_INCLUDE_DIR}/..>
)
target_compile_definitions(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}
	PUBLIC
		LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_FIX_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_MSGS}"
)
target_compile_definitions(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_archive
	PUBLIC
		LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_FIX_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_MSGS}"
)

set(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim_TARGET_SOURCES
	batsboe_sim.cpp
	batsboe_sim.hpp
)
add_dynamic_library_with_options(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim "${${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim_TARGET_SOURCES}")
add_dependencies(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim
	ConvertISOSpecsToCXX
	FindPerfectHashMaskForBATSBOEUSSimMsgs
	FindPerfectHashMaskForBATSBOEExchgMsgs
	FindPerfectHashMaskForFIXMsgs
)
target_compile_definitions(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim
	PUBLIC
		LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_FIX_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS}"
		LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_HDR="${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_US_SIM_MSGS}"
)
target_include_directories(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim
	PUBLIC
		$<BUILD_INTERFACE:${ConvertISOCurrencySpecToCXX_INCLUDE_DIR}/..>
)
target_link_libraries(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim
	PUBLIC
		jmmcg
)

set(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_simulator_TARGET_SOURCES
	batsboe_sim_main.cpp
	batsboe_sim.hpp
)
add_executable_with_options(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_simulator "${${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_simulator_TARGET_SOURCES}")
target_include_directories(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_simulator
	PRIVATE
		${ConvertISOCurrencySpecToCXX_INCLUDE_DIR}/..
		${ConvertISOMICSpecToCXX_INCLUDE_DIR}/..
)
target_link_libraries(${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_simulator
	${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}
	${PROJECT_NAME}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION}_sim
)

install(
	EXPORT ${PROJECT_NAME}Targets
	NAMESPACE isimud::
	DESTINATION share/cmake/Modules COMPONENT development
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)

install(
	FILES
		${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}_v2_config.h
	DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}
	COMPONENT development
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)
