#ifndef ISIMUD_EXCHANGES_BATSBOE_US_v1_MESSAGES_HPP
#define ISIMUD_EXCHANGES_BATSBOE_US_v1_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "batsboe_us_v1_config.h"

#include "../types.hpp"

#include "../../common/messages.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace US {

/**
	From <a href="https://cdn.batstrading.com/resources/membership/BATS_BZX_Exchange_US_Equities_BOE_Specification.pdf">"BZX Exchange US Equities BOE Specification Version 1.8.6"</a>.

	\todo
*/
namespace v1 {

struct MsgTypes {
	static inline constexpr const exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS;

	using ref_data= common::SeqNum_t;
	template<class Op>
	static constexpr ref_data create_ref_data(Op) noexcept(true) {
		return ref_data{};
	}
	using isin_mapping_data_const_reference= ref_data const&;

	using MsgType_t= common::MsgType_t;
	using MsgTypes_t= US::MsgType::element_type;
	using UserName_t= common::UserName_t;
	using Password_t= common::Password_t;
	using Symbol_t= common::Symbol_t;
	using SecurityID_t= common::SecurityID_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= common::Quantity_t;
	using ClientOrderID_t= common::ClientOrderID_t;
	using OrderType= common::OrdType::element_type;
	using Side= common::Side::element_type;
	using TIF= common::TIF::element_type;
	using OrderRejectReason= common::OrderRejectReason::element_type;
	using LoginResponseStatus= common::LoginResponseStatus::element_type;
	using logon_args_t= common::logon_args_t;

	using LogonRequest_t= common::LogonRequest<MsgTypes_t>;
	using Header_t= LogonRequest_t::Header_t;
	using LogoutRequest_t= common::LogoutRequest<MsgTypes_t>;
	using ClientHeartbeat_t= common::ClientHeartbeat<MsgTypes_t>;
	using LogonReply_t= common::LogonReply<MsgTypes_t>;
	using Logout_t= common::Logout<MsgTypes_t>;
	using ServerHeartbeat_t= common::ServerHeartbeat<MsgTypes_t>;
	using ReplayComplete_t= common::ReplayComplete<MsgTypes_t>;
	using NewOrder_t= common::NewOrder<MsgTypes_t>;
	using CancelOrder_t= common::CancelOrder<MsgTypes_t>;
	using ModifyOrder_t= common::ModifyOrder<MsgTypes_t>;
	using OrderAcknowledgement_t= common::OrderAcknowledgement<MsgTypes_t>;
	using OrderRejected_t= common::OrderRejected<MsgTypes_t>;
	using OrderModified_t= common::OrderModified<MsgTypes_t>;
	using OrderRestated_t= common::OrderRestated<MsgTypes_t>;
	using UserModifyRejected_t= common::UserModifyRejected<MsgTypes_t>;
	using OrderCancelled_t= common::OrderCancelled<MsgTypes_t>;
	using CancelRejected_t= common::CancelRejected<MsgTypes_t>;
	using OrderExecution_t= common::OrderExecution<MsgTypes_t>;
	using TradeCancelOrCorrect_t= common::TradeCancelOrCorrect<MsgTypes_t>;

	using ExecutionReport_t= OrderExecution_t;
	using Heartbeat_t= ClientHeartbeat_t;
	using OrderCancelRequest_t= CancelOrder_t;
	using OrderCancelReject_t= CancelRejected_t;
	using OrderCancelReplaceRequest_t= ModifyOrder_t;

	using client_to_exchange_messages_t= boost::mpl::vector<
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		NewOrder_t,
		CancelOrder_t,
		ModifyOrder_t>;

	using exchange_to_client_messages_t= boost::mpl::vector<
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		ReplayComplete_t,
		OrderAcknowledgement_t,
		OrderRejected_t,
		OrderModified_t,
		OrderRestated_t,
		UserModifyRejected_t,
		OrderCancelled_t,
		CancelRejected_t,
		OrderExecution_t,
		TradeCancelOrCorrect_t>;

	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1> > >::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= sizeof(typename LogonRequest_t::Header_t)
	};
	BOOST_MPL_ASSERT_RELATION(max_msg_size, >=, header_t_size);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const Price_t implied_decimal_places= common::implied_decimal_places;

	template<class ConnPolT, class SktT, class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return common::make_ctor_args<MsgTypes, ConnPolT, SktT, ThrdT>(vm);
	}

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test BATSBOE US v1 size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 261);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), ==, 10);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 10);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 11);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Logout_t), ==, 76);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 10);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ReplayComplete_t), ==, 10);
// TODO BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrder_t), ==, 157);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::CancelOrder_t), ==, 36);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ModifyOrder_t), ==, 73);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderAcknowledgement_t), ==, 245);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderRejected_t), ==, 213);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderModified_t), ==, 205);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderRestated_t), ==, 216);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::UserModifyRejected_t), ==, 108);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelled_t), ==, 191);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::CancelRejected_t), ==, 100);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderExecution_t), ==, 215);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::TradeCancelOrCorrect_t), ==, 170);

}

}
}}}}}

#include "messages_impl.hpp"

#endif
