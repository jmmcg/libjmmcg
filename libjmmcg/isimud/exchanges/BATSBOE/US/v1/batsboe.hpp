#ifndef ISIMUD_EXCHANGES_BATSBOE_US_v1_batsboe_hpp
#define ISIMUD_EXCHANGES_BATSBOE_US_v1_batsboe_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

#include "../../common/connectivity_policy.hpp"
#include "../../common/processing_rules.hpp"

#include "../../../common/connection.hpp"
#include "../../../common/exchange_connection.hpp"

#include "core/socket_server.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace US { namespace v1 {

using connection_t= exchanges::common::connection<MsgTypes, common::connectivity_policy<MsgTypes::logon_args_t>>;

template<class ExchgCxns>
using client_cxn_t= libjmmcg::socket::svr<
	libjmmcg::socket::server_manager::forwarding<ExchgCxns, connection_t::socket_t>>;
template<class ClientMsgTypes>
using exchange_cxn_t= exchanges::common::connection_processor<
	common::client_to_exchange_transformations<ClientMsgTypes, MsgTypes, connection_t::socket_t>,
	common::exchange_to_client_transformations<MsgTypes, ClientMsgTypes, connection_t::socket_t>,
	common::connectivity_policy<MsgTypes::logon_args_t>>;

template<class ClientMsgTypes>
using link_t= exchanges::common::exchange_connection<
	client_cxn_t,
	exchange_cxn_t<ClientMsgTypes>>;

}}}}}}

extern template class libisimud::exchanges::common::connection<libisimud::exchanges::BATSBOE::US::v1::MsgTypes, libisimud::exchanges::BATSBOE::common::connectivity_policy<libisimud::exchanges::BATSBOE::US::v1::MsgTypes::logon_args_t>>;

#endif
