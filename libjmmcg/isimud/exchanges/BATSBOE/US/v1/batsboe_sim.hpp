#ifndef ISIMUD_EXCHANGES_BATSBOE_US_v1_batsboe_sim_hpp
#define ISIMUD_EXCHANGES_BATSBOE_US_v1_batsboe_sim_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

#include "../../common/connectivity_policy.hpp"
#include "../../common/processing_rules.hpp"

#include "../../../common/socket_type.hpp"

#include "core/socket_server.hpp"
#include "core/socket_server_manager.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace US { namespace v1 {

using simulator_t= libjmmcg::socket::svr<
	libjmmcg::socket::server_manager::loopback<
		common::simulator_responses<MsgTypes, exchanges::common::socket_type::socket_t>,
		common::server_hb_t<typename MsgTypes::ServerHeartbeat_t>,
		exchanges::common::socket_type::socket_t> >;

}}}}}}
/* TODO - llinker errors...
lass libjmmcg::socket::svr<
	libjmmcg::socket::server_manager::loopback<
		libisimud::exchanges::BATSBOE::common::simulator_responses<libisimud::exchanges::BATSBOE::US::v1::MsgTypes, libisimud::exchanges::common::socket_type::socket_t>,
		libisimud::exchanges::BATSBOE::common::server_hb_t<libisimud::exchanges::BATSBOE::US::v1::MsgTypes::ServerHeartbeat_t>,
		libisimud::exchanges::common::socket_type::socket_t> >;
*/
#endif
