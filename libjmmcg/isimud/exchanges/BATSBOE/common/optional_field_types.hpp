#ifndef ISIMUD_EXCHANGES_BATSBOE_common_optional_field_types_hpp
#define ISIMUD_EXCHANGES_BATSBOE_common_optional_field_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#ifdef BOOST_MPL_LIMIT_MAP_SIZE
#	undef BOOST_MPL_LIMIT_MAP_SIZE
#endif
#define BOOST_MPL_LIMIT_MAP_SIZE 60

#include "core/bitfield_map.hpp"

#include <boost/mpl/map.hpp>

#include <array>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common { namespace optional {

namespace LogonTypes {

struct [[gnu::packed]] Units {
	std::uint8_t unitNumber;
	std::uint8_t unitSequence[4];
};

namespace OrderAcknowledgement {

enum class return_bitfield1 : std::uint8_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7)
};

enum class return_bitfield2 : std::uint8_t {
	Symbol= (0x1UL << 0),
	SymbolSfx= (0x1UL << 1),
	Currency= (0x1UL << 2),
	IDSource= (0x1UL << 3),
	SecurityID= (0x1UL << 4),
	SecurityExchange= (0x1UL << 5),
	Capacity= (0x1UL << 6),
	CrossFlag= (0x1UL << 7)
};

enum class return_bitfield3 : std::uint8_t {
	Account= (0x1UL << 0),
	ClearingFirm= (0x1UL << 1),
	ClearingAccount= (0x1UL << 2),
	DisplayIndicator= (0x1UL << 3),
	MaxFloor= (0x1UL << 4),
	DiscretionAmount= (0x1UL << 5),
	OrderQuantity= (0x1UL << 6),
	PreventParticipantMatch= (0x1UL << 7)
};

enum class return_bitfield5 : std::uint8_t {
	OrigClOrdID= (0x1UL << 0),
	LeavesQty= (0x1UL << 1),
	LastShares= (0x1UL << 2),
	LastPx= (0x1UL << 3),
	DisplayPrice= (0x1UL << 4),
	WorkingPrice= (0x1UL << 5),
	BaseLiquidityIndicator= (0x1UL << 6),
	ExpireTime= (0x1UL << 7)
};

enum class return_bitfield6 : std::uint8_t {
	SecondaryOrderId= (0x1UL << 0),
	CCP= (0x1UL << 1),
	Reserved1= (0x1UL << 2),
	AttributedQuote= (0x1UL << 3),
	Reserved2= (0x1UL << 4),
	Reserved3= (0x1UL << 5),
	Reserved4= (0x1UL << 6),
	Reserved5= (0x1UL << 7)
};

enum class return_bitfield7 : std::uint8_t {
	SubLiquidityIndicator= (0x1UL << 0),
	Reserved1= (0x1UL << 1),
	Reserved2= (0x1UL << 2),
	Reserved3= (0x1UL << 3),
	Reserved4= (0x1UL << 4),
	Reserved5= (0x1UL << 5),
	Reserved6= (0x1UL << 6),
	Reserved7= (0x1UL << 7)
};

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield5 bitfield5;
	return_bitfield6 bitfield6;
	return_bitfield7 bitfield7;
	const std::uint8_t reserved= 0;
};

}

namespace OrderRejected {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	const std::uint8_t reserved_bitfield3= 0;
	const std::uint8_t reserved_bitfield4= 0;
	const std::uint8_t reserved_bitfield5= 0;
	const std::uint8_t reserved_bitfield6= 0;
	const std::uint8_t reserved= 0;
};

}

namespace OrderModified {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield5= OrderAcknowledgement::return_bitfield5;
using return_bitfield6= OrderAcknowledgement::return_bitfield6;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	const std::uint8_t reserved_bitfield1= 0;
	return_bitfield3 bitfield3;
	const std::uint8_t reserved_bitfield3= 0;
	return_bitfield5 bitfield5;
	return_bitfield6 bitfield6;
	const std::uint8_t reserved_bitfield6= 0;
	const std::uint8_t reserved= 0;
};

}

namespace OrderRestated {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;

enum class return_bitfield4 : std::uint8_t {
	Reserved1= (0x1UL << 0),
	Reserved2= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	CorrectedSize= (0x1UL << 5),
	PartyID= (0x1UL << 6),
	AccessFee= (0x1UL << 7)
};

using return_bitfield5= OrderAcknowledgement::return_bitfield5;
using return_bitfield6= OrderAcknowledgement::return_bitfield6;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	return_bitfield5 bitfield5;
	return_bitfield6 bitfield6;
	const std::uint8_t reserved_bitfield6= 0;
	const std::uint8_t reserved= 0;
};

}

namespace UserModifyRejected {

struct [[gnu::packed]] bitfields {
	const std::uint8_t reserved_bitfield[7]= {0, 0, 0, 0, 0, 0, 0};
	const std::uint8_t reserved= 0;
};

}

namespace OrderCancelled {

enum class return_bitfield1 : std::uint8_t {
	Side= (0x1UL << 0),
	Reserved1= (0x1UL << 1),
	Reserved2= (0x1UL << 2),
	Reserved3= (0x1UL << 3),
	Reserved4= (0x1UL << 4),
	Reserved5= (0x1UL << 5),
	Reserved6= (0x1UL << 6),
	Reserved7= (0x1UL << 7)
};

enum class return_bitfield2 : std::uint8_t {
	Symbol= (0x1UL << 0),
	Reserved1= (0x1UL << 1),
	Reserved2= (0x1UL << 2),
	Reserved3= (0x1UL << 3),
	Reserved4= (0x1UL << 4),
	Reserved5= (0x1UL << 5),
	Reserved6= (0x1UL << 6),
	Reserved7= (0x1UL << 7)
};

using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield4= OrderRestated::return_bitfield4;
using return_bitfield5= OrderAcknowledgement::return_bitfield5;
using return_bitfield6= OrderAcknowledgement::return_bitfield6;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	return_bitfield5 bitfield5;
	return_bitfield6 bitfield6;
	const std::uint8_t reserved= 0;
};

}

namespace OrderExecution {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield6= OrderAcknowledgement::return_bitfield6;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	const std::uint8_t reserved_bitfield3= 0;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield6 bitfield6;
	const std::uint8_t reserved_bitfield6= 0;
	const std::uint8_t reserved= 0;
};

}

namespace TradeCancelCorrect {

using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield4= OrderRestated::return_bitfield4;

struct [[gnu::packed]] bitfields {
	const std::uint8_t reserved_bitfield0= 0;
	return_bitfield2 bitfield2;
	const std::uint8_t reserved_bitfield2= 0;
	return_bitfield4 bitfield4;
	const std::uint8_t reserved_bitfield4= 0;
	const std::uint8_t reserved_bitfield5= 0;
	const std::uint8_t reserved_bitfield6= 0;
	const std::uint8_t reserved= 0;
};

}

namespace Reserved {

using bitfields= UserModifyRejected::bitfields;

}

namespace TradeCaptureReportAck {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;

enum class return_bitfield4 : std::uint8_t {
	Reserved1= (0x1UL << 0),
	Reserved2= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	CorrectedSize= (0x1UL << 5),
	PartyID= (0x1UL << 6),
	Reserved8= (0x1UL << 7)
};

enum class return_bitfield6 : std::uint8_t {
	Reserved1= (0x1UL << 0),
	CCP= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	Reserved6= (0x1UL << 5),
	Reserved7= (0x1UL << 6),
	PartyRole= (0x1UL << 7)
};

enum class return_bitfield7 : std::uint8_t {
	Reserved1= (0x1UL << 0),
	TradeReportTypeReturn= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	Reserved6= (0x1UL << 5),
	LargeSize= (0x1UL << 6),
	Reserved8= (0x1UL << 7)
};

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield6 bitfield6;
	return_bitfield7 bitfield7;
	const std::uint8_t reserved= 0;
};

}

namespace TradeCaptureReportRej {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield4= TradeCaptureReportAck::return_bitfield4;
using return_bitfield6= TradeCaptureReportAck::return_bitfield6;
using return_bitfield7= TradeCaptureReportAck::return_bitfield7;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield6 bitfield6;
	return_bitfield7 bitfield7;
	const std::uint8_t reserved= 0;
};

}

namespace TradeCaptureConfirm {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield4= TradeCaptureReportAck::return_bitfield4;
using return_bitfield6= TradeCaptureReportAck::return_bitfield6;

enum class return_bitfield7 : std::uint8_t {
	Reserved1= (0x1UL << 0),
	TradeReportTypeReturn= (0x1UL << 1),
	TradePublishIndReturn= (0x1UL << 2),
	Text= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	Reserved6= (0x1UL << 5),
	LargeSize= (0x1UL << 6),
	Reserved8= (0x1UL << 7)
};

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield6 bitfield6;
	return_bitfield7 bitfield7;
	const std::uint8_t reserved= 0;
};

}

namespace TradeCaptureDecline {

using return_bitfield1= OrderAcknowledgement::return_bitfield1;
using return_bitfield2= OrderAcknowledgement::return_bitfield2;
using return_bitfield3= OrderAcknowledgement::return_bitfield3;
using return_bitfield4= TradeCaptureReportAck::return_bitfield4;
using return_bitfield6= TradeCaptureReportAck::return_bitfield6;
using return_bitfield7= TradeCaptureReportAck::return_bitfield7;

struct [[gnu::packed]] bitfields {
	return_bitfield1 bitfield1;
	return_bitfield2 bitfield2;
	return_bitfield3 bitfield3;
	return_bitfield4 bitfield4;
	const std::uint8_t reserved_bitfield4= 0;
	return_bitfield6 bitfield6;
	return_bitfield7 bitfield7;
	const std::uint8_t reserved= 0;
};

}

}

namespace NewOrder {

enum class bitfields_t : std::uint64_t {
	ClearingFirm= (0x1UL << 0),
	ClearingAccount= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxFloor= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	Reserved1= (0x1UL << 9),
	Currency= (0x1UL << 10),
	IDSource= (0x1UL << 11),
	SecurityID= (0x1UL << 12),
	SecurityExchange= (0x1UL << 13),
	Capacity= (0x1UL << 14),
	RoutingInst= (0x1UL << 15),
	Account= (0x1UL << 16),
	DisplayIndicator= (0x1UL << 17),
	MaxRemovePct= (0x1UL << 18),
	Reserved2= (0x1UL << 19),
	PegDifference= (0x1UL << 20),
	PreventParticipantMatch= (0x1UL << 21),
	LocateReqd= (0x1UL < 22),
	ExpireTime= (0x1UL << 23),
	Reserved3= (0x1UL << 24),
	Reserved4= (0x1UL << 25),
	Reserved5= (0x1UL << 26),
	Reserved6= (0x1UL << 27),
	Reserved7= (0x1UL << 28),
	Reserved8= (0x1UL << 29),
	Reserved9= (0x1UL << 30),
	Reserved10= (0x1UL << 31),
	CrossFlag= (0x1UL << 32),
	AttributedQuote= (0x1UL << 33),
	BookingType= (0x1UL << 34),
	last_field= (0x1UL << 34)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
			common::Currency_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
			common::IDSource::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
			common::SecurityID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
			common::SecurityExchange_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
			common::Capacity::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::RoutingInst>::type,
			common::RoutingInst_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			std::int64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LocateReqd>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExpireTime>::type,
			common::DateTime_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved9>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AttributedQuote>::type,
			common::AttributedQuote::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::BookingType>::type,
			common::BookingType::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace CancelOrder {

enum class bitfields_t : std::uint16_t {
	ClearingFirm= (0x1UL << 0),
	last_field= (0x1UL << 0)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace ModifyOrder {

enum class bitfields_t : std::uint16_t {
	ClearingFirm= (0x1UL << 0),
	Reserved1= (0x1UL << 1),
	OrderQty= (0x1UL << 2),
	Price= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	CancelOrigOnReject= (0x1UL << 5),
	ExecInst= (0x1UL << 6),
	Side= (0x1UL << 7),
	last_field= (0x1UL << 7)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQty>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CancelOrigOnReject>::type,
			common::CancelOrigOnReject::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderAcknowledgement {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	SymbolSfx= (0x1UL << 9),
	Currency= (0x1UL << 10),
	IDSource= (0x1UL << 11),
	SecurityID= (0x1UL << 12),
	SecurityExchange= (0x1UL << 13),
	Capacity= (0x1UL << 14),
	CrossFlag= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	Reserved3= (0x1UL << 24),
	Reserved4= (0x1UL << 25),
	Reserved5= (0x1UL << 26),
	Reserved6= (0x1UL << 27),
	Reserved7= (0x1UL << 28),
	Reserved8= (0x1UL << 29),
	Reserved9= (0x1UL << 30),
	Reserved10= (0x1UL << 31),
	OrigClOrdID= (0x1UL << 32),
	LeavesQty= (0x1UL << 33),
	LastShares= (0x1UL << 34),
	LastPx= (0x1UL << 35),
	DisplayPrice= (0x1UL << 36),
	WorkingPrice= (0x1UL << 37),
	BaseLiquidityIndicator= (0x1UL << 38),
	ExpireTime= (0x1UL << 39),
	SecondaryOrderId= (0x1UL << 40),
	CCP= (0x1UL << 41),
	Reserved11= (0x1UL << 42),
	AttributedQuote= (0x1UL << 43),
	Reserved12= (0x1UL << 44),
	Reserved13= (0x1UL << 45),
	Reserved14= (0x1UL << 46),
	Reserved15= (0x1UL << 47),
	SubLiquidityIndicator= (0x1UL << 48),
	last_field= (0x1UL << 48)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			common::SPrice_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SymbolSfx>::type,
			common::SymbolSfx_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
			common::Currency_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
			common::IDSource::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
			common::SecurityID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
			common::SecurityExchange_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
			common::Capacity::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved9>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrigClOrdID>::type,
			common::ClientOrderID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LeavesQty>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastShares>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastPx>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::WorkingPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::BaseLiquidityIndicator>::type,
			common::BaseLiquidityIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExpireTime>::type,
			common::DateTime_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecondaryOrderId>::type,
			uint64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
			common::CCP::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AttributedQuote>::type,
			common::AttributedQuote::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SubLiquidityIndicator>::type,
			common::SubLiquidityIndicator> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderRejected {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	SymbolSfx= (0x1UL << 9),
	Currency= (0x1UL << 10),
	IDSource= (0x1UL << 11),
	SecurityID= (0x1UL << 12),
	SecurityExchange= (0x1UL << 13),
	Capacity= (0x1UL << 14),
	CrossFlag= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	last_field= (0x1UL << 23)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			common::SPrice_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SymbolSfx>::type,
			common::SymbolSfx_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
			common::Currency_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
			common::IDSource::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
			common::SecurityID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
			common::SecurityExchange_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
			common::Capacity::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderModified {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7),
	Reserved1= (0x1UL << 8),
	Reserved2= (0x1UL << 9),
	Reserved3= (0x1UL << 10),
	Reserved4= (0x1UL << 11),
	Reserved5= (0x1UL << 12),
	Reserved6= (0x1UL << 13),
	Reserved7= (0x1UL << 14),
	Reserved8= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	Reserved10= (0x1UL << 24),
	Reserved11= (0x1UL << 25),
	Reserved12= (0x1UL << 26),
	Reserved13= (0x1UL << 27),
	Reserved14= (0x1UL << 28),
	Reserved15= (0x1UL << 29),
	Reserved16= (0x1UL << 30),
	Reserved17= (0x1UL << 31),
	OrigClOrdID= (0x1UL << 32),
	LeavesQty= (0x1UL << 33),
	LastShares= (0x1UL << 34),
	LastPx= (0x1UL << 35),
	DisplayPrice= (0x1UL << 36),
	WorkingPrice= (0x1UL << 37),
	BaseLiquidityIndicator= (0x1UL << 38),
	ExpireTime= (0x1UL << 39),
	SecondaryOrderId= (0x1UL << 40),
	CCP= (0x1UL << 41),
	Reserved18= (0x1UL << 42),
	AttributedQuote= (0x1UL << 43),
	last_field= (0x1UL << 43)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			common::SPrice_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved17>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrigClOrdID>::type,
			common::ClientOrderID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LeavesQty>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastShares>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastPx>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::WorkingPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::BaseLiquidityIndicator>::type,
			common::BaseLiquidityIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExpireTime>::type,
			common::DateTime_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecondaryOrderId>::type,
			uint64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
			common::CCP::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AttributedQuote>::type,
			common::AttributedQuote::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderRestated {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	Reserved2= (0x1UL << 9),
	Reserved3= (0x1UL << 10),
	Reserved4= (0x1UL << 11),
	Reserved5= (0x1UL << 12),
	Reserved6= (0x1UL << 13),
	Reserved7= (0x1UL << 14),
	Reserved8= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	Reserved10= (0x1UL << 24),
	Reserved11= (0x1UL << 25),
	Reserved12= (0x1UL << 26),
	Reserved13= (0x1UL << 27),
	Reserved14= (0x1UL << 28),
	CorrectedSize= (0x1UL << 29),
	PartyID= (0x1UL << 30),
	AccessFee= (0x1UL << 31),
	OrigClOrdID= (0x1UL << 32),
	LeavesQty= (0x1UL << 33),
	LastShares= (0x1UL << 34),
	LastPx= (0x1UL << 35),
	DisplayPrice= (0x1UL << 36),
	WorkingPrice= (0x1UL << 37),
	BaseLiquidityIndicator= (0x1UL << 38),
	ExpireTime= (0x1UL << 39),
	SecondaryOrderId= (0x1UL << 40),
	CCP= (0x1UL << 41),
	Reserved15= (0x1UL << 42),
	AttributedQuote= (0x1UL << 43),
	last_field= (0x1UL << 43)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			common::SPrice_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CorrectedSize>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PartyID>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AccessFee>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrigClOrdID>::type,
			common::ClientOrderID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LeavesQty>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastShares>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastPx>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::WorkingPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::BaseLiquidityIndicator>::type,
			common::BaseLiquidityIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExpireTime>::type,
			common::DateTime_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecondaryOrderId>::type,
			uint64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
			common::CCP::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AttributedQuote>::type,
			common::AttributedQuote::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace UserModifyRejected {

enum class bitfields_t : std::uint64_t {
	last_field= (0x1UL << 0)
};
struct bitfields_to_type_map {
	using bitfields_tags_type= bitfields_t;

	enum : std::size_t {
		mapped_types_size= 1
	};

	static constexpr std::size_t size() noexcept(true) {
		return sizeof(bitfields_to_type_map);
	}
};

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderCancelled {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	Reserved2= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	Reserved6= (0x1UL << 5),
	Reserved7= (0x1UL << 6),
	Reserved8= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	Reserved10= (0x1UL << 9),
	Reserved11= (0x1UL << 10),
	Reserved12= (0x1UL << 11),
	Reserved13= (0x1UL << 12),
	Reserved14= (0x1UL << 13),
	Reserved15= (0x1UL << 14),
	Reserved16= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	Reserved18= (0x1UL << 24),
	Reserved19= (0x1UL << 25),
	Reserved20= (0x1UL << 26),
	Reserved21= (0x1UL << 27),
	Reserved22= (0x1UL << 28),
	CorrectedSize= (0x1UL << 29),
	PartyID= (0x1UL << 30),
	AccessFee= (0x1UL << 31),
	OrigClOrdID= (0x1UL << 32),
	LeavesQty= (0x1UL << 33),
	LastShares= (0x1UL << 34),
	LastPx= (0x1UL << 35),
	DisplayPrice= (0x1UL << 36),
	WorkingPrice= (0x1UL << 37),
	BaseLiquidityIndicator= (0x1UL << 38),
	ExpireTime= (0x1UL << 39),
	SecondaryOrderId= (0x1UL << 40),
	CCP= (0x1UL << 41),
	Reserved23= (0x1UL << 42),
	AttributedQuote= (0x1UL << 43),
	last_field= (0x1UL << 43)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved19>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved20>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved21>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved22>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CorrectedSize>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PartyID>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AccessFee>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrigClOrdID>::type,
			common::ClientOrderID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LeavesQty>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastShares>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::LastPx>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::WorkingPrice>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::BaseLiquidityIndicator>::type,
			common::BaseLiquidityIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExpireTime>::type,
			common::DateTime_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecondaryOrderId>::type,
			uint64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
			common::CCP::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved23>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AttributedQuote>::type,
			common::AttributedQuote::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace CancelRejected {

enum class bitfields_t : std::uint64_t {
	last_field= (0x1UL << 0)
};
struct bitfields_to_type_map {
	using bitfields_tags_type= bitfields_t;

	enum : std::size_t {
		mapped_types_size= 1
	};

	static constexpr std::size_t size() noexcept(true) {
		return sizeof(bitfields_to_type_map);
	}
};

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace OrderExecution {

enum class bitfields_t : std::uint64_t {
	Side= (0x1UL << 0),
	PegDifference= (0x1UL << 1),
	Price= (0x1UL << 2),
	ExecInst= (0x1UL << 3),
	OrdType= (0x1UL << 4),
	TimeInForce= (0x1UL << 5),
	MinQty= (0x1UL << 6),
	MaxRemovePct= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	SymbolSfx= (0x1UL << 9),
	Currency= (0x1UL << 10),
	IDSource= (0x1UL << 11),
	SecurityID= (0x1UL << 12),
	SecurityExchange= (0x1UL << 13),
	Capacity= (0x1UL << 14),
	CrossFlag= (0x1UL << 15),
	Account= (0x1UL << 16),
	ClearingFirm= (0x1UL << 17),
	ClearingAccount= (0x1UL << 18),
	DisplayIndicator= (0x1UL << 19),
	MaxFloor= (0x1UL << 20),
	DiscretionAmount= (0x1UL << 21),
	OrderQuantity= (0x1UL << 22),
	PreventParticipantMatch= (0x1UL << 23),
	Reserved3= (0x1UL << 24),
	Reserved4= (0x1UL << 25),
	Reserved5= (0x1UL << 26),
	Reserved6= (0x1UL << 27),
	Reserved7= (0x1UL << 28),
	Reserved8= (0x1UL << 29),
	Reserved9= (0x1UL << 30),
	Reserved10= (0x1UL << 31),
	Reserved11= (0x1UL << 32),
	Reserved12= (0x1UL << 33),
	Reserved13= (0x1UL << 34),
	Reserved14= (0x1UL << 35),
	Reserved15= (0x1UL << 36),
	Reserved16= (0x1UL << 37),
	Reserved17= (0x1UL << 38),
	Reserved18= (0x1UL << 39),
	SecondaryOrderId= (0x1UL << 40),
	CCP= (0x1UL << 41),
	last_field= (0x1UL << 41)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Side>::type,
			common::Side::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
			common::SPrice_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Price>::type,
			common::Price_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
			common::ExecInst::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
			common::OrdType::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
			common::TIF::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
			std::uint8_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SymbolSfx>::type,
			common::SymbolSfx_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
			common::Currency_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
			common::IDSource::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
			common::SecurityID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
			common::SecurityExchange_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
			common::Capacity::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Account>::type,
			common::Account_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
			common::ClearingFirm_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
			common::ClearingAccount_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
			common::DisplayIndicator::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
			std::uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::DiscretionAmount>::type,
			std::uint16_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
			common::Quantity_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
			common::PreventParticipantMatch_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved9>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved17>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecondaryOrderId>::type,
			uint64_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
			common::CCP::element_type> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

namespace TradeCancelOrCorrect {

enum class bitfields_t : std::uint64_t {
	Reserved1= (0x1UL << 0),
	Reserved2= (0x1UL << 1),
	Reserved3= (0x1UL << 2),
	Reserved4= (0x1UL << 3),
	Reserved5= (0x1UL << 4),
	Reserved6= (0x1UL << 5),
	Reserved7= (0x1UL << 6),
	Reserved8= (0x1UL << 7),
	Symbol= (0x1UL << 8),
	SymbolSfx= (0x1UL << 9),
	Currency= (0x1UL << 10),
	IDSource= (0x1UL << 11),
	SecurityID= (0x1UL << 12),
	SecurityExchange= (0x1UL << 13),
	Capacity= (0x1UL << 14),
	CrossFlag= (0x1UL << 15),
	Reserved10= (0x1UL << 16),
	Reserved11= (0x1UL << 17),
	Reserved12= (0x1UL << 18),
	Reserved13= (0x1UL << 19),
	Reserved14= (0x1UL << 20),
	Reserved15= (0x1UL << 21),
	Reserved16= (0x1UL << 22),
	Reserved17= (0x1UL << 23),
	Reserved18= (0x1UL << 24),
	Reserved19= (0x1UL << 25),
	Reserved20= (0x1UL << 26),
	Reserved21= (0x1UL << 27),
	Reserved22= (0x1UL << 28),
	CorrectedSize= (0x1UL << 29),
	PartyID= (0x1UL << 30),
	AccessFee= (0x1UL << 31),
	last_field= (0x1UL << 31)
};

using bitfields_to_type_map= libjmmcg::bitfield_map<
	boost::mpl::map<
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
			common::Symbol_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SymbolSfx>::type,
			common::SymbolSfx_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
			common::Currency_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
			common::IDSource::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
			common::SecurityID_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
			common::SecurityExchange_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
			common::Capacity::element_type>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved17>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved19>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved20>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved21>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::Reserved22>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::CorrectedSize>::type,
			uint32_t>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::PartyID>::type,
			libjmmcg::a_zero_sized_class>,
		boost::mpl::pair<
			std::integral_constant<bitfields_t, bitfields_t::AccessFee>::type,
			libjmmcg::a_zero_sized_class> > >;

BOOST_MPL_ASSERT_RELATION(static_cast<std::size_t>(libjmmcg::mpl::bit_position<static_cast<std::underlying_type<bitfields_t>::type>(bitfields_t::last_field)>::value), ==, static_cast<std::size_t>(bitfields_to_type_map::mapped_types_size));

}

}}}}}}

#endif
