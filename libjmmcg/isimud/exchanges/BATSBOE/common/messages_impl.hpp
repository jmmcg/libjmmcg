/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

template<class ExchgMsgDetailsT, class ConnPolT, class SktT, class ThrdT>
inline auto
make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
	using conn_pol_t= ConnPolT;
	using exchg_msg_details_t= ExchgMsgDetailsT;

	std::string_view const MIC_code(libjmmcg::enum_tags::mpl::to_array<exchg_msg_details_t::MIC_code>::value.begin(), libjmmcg::enum_tags::mpl::to_array<exchg_msg_details_t::MIC_code>::value.end());
	const std::string sess_sub_id(vm[fmt::format("{}_session_sub_id", MIC_code).c_str()].as<std::string>());
	common::SessionSubID_t sess_sub_id_conv;
	std::memcpy(sess_sub_id_conv.data(), sess_sub_id.data(), std::min(sess_sub_id_conv.size(), sess_sub_id.size()));
	const std::string user(vm[fmt::format("{}_username", MIC_code).c_str()].as<std::string>());
	typename exchg_msg_details_t::UserName_t user_conv{};
	std::memcpy(user_conv.data(), user.data(), std::min(user_conv.size(), user.size()));
	const std::string pass(vm[fmt::format("{}_password", MIC_code).c_str()].as<std::string>());
	typename exchg_msg_details_t::Password_t pass_conv{};
	std::memcpy(pass_conv.data(), pass.data(), std::min(pass_conv.size(), pass.size()));
	// TODO Must be shared, as the link, also the processing rules require a reference to this object.
	auto seq_num(
		std::make_shared<typename exchg_msg_details_t::SeqNum_t>(
			vm[fmt::format("{}_sequence_number", MIC_code).c_str()].as<unsigned>()));

	return std::make_tuple(
		conn_pol_t(
			typename conn_pol_t::gateways_t(
				std::make_pair(boost::asio::ip::address_v4::from_string(vm[fmt::format("{}_primary_gateway_address", MIC_code).c_str()].as<std::string>()), vm[fmt::format("{}_gateway_port", MIC_code).c_str()].as<unsigned short>())),
			typename conn_pol_t::logon_args_t{
				vm[fmt::format("{}_sequence_number", MIC_code).c_str()].as<unsigned>(),
				sess_sub_id_conv,
				user_conv,
				pass_conv,
				vm[fmt::format("{}_no_unspec_replay", MIC_code).c_str()].as<bool>()},
			common::logoff_args_t{
				*seq_num}),
		SktT::socket_priority::high,
		ThrdT::exchange_to_client_thread.core,
		seq_num);
}

template<auto Msg>
inline Header<Msg>::Header(std::size_t l) noexcept(true)
	: length_(static_cast<std::uint16_t>(l - sizeof(std::uint16_t))), sequenceNumber() {
	DEBUG_ASSERT(l < std::numeric_limits<std::uint16_t>::max());
	DEBUG_ASSERT((length_ + sizeof(std::uint16_t)) >= sizeof(Header));
}

template<auto Msg>
inline Header<Msg>::Header(std::size_t l, SeqNum_t seqNum) noexcept(true)
	: length_(static_cast<std::uint16_t>(l - sizeof(std::uint16_t))), sequenceNumber(seqNum) {
	DEBUG_ASSERT(l < std::numeric_limits<std::uint16_t>::max());
	DEBUG_ASSERT((length_ + sizeof(std::uint16_t)) >= sizeof(Header));
}

template<auto Msg>
inline std::uint16_t
Header<Msg>::length() const noexcept(true) {
	const std::uint16_t l= static_cast<std::uint16_t>(length_ + sizeof(std::uint16_t));
	DEBUG_ASSERT(l >= sizeof(Header));
	return l;
}

template<class MsgT>
inline LogonRequest<MsgT>::LogonRequest(SeqNum_t seqNum, const SessionSubID_t ssID, const UserName_t& UN, const Password_t& P, const bool noUnspec) noexcept(true)
	: Header_t(sizeof(LogonRequest), seqNum), sessionSubID(ssID), userName(UN), password(P), noUnspecifiedUnitReplay(noUnspec) {
}

template<class MsgT>
inline LogonRequest<MsgT>::LogonRequest(logon_args_t const& a) noexcept(true)
	: LogonRequest(a.sequenceNumber, a.sessionSubID, a.username, a.password, a.noUnspec) {
}

template<class MsgT>
inline LogoutRequest<MsgT>::LogoutRequest(SeqNum_t seqNum) noexcept(true)
	: Header_t(sizeof(LogoutRequest), seqNum) {
}

template<class MsgT>
inline LogoutRequest<MsgT>::LogoutRequest(logoff_args_t const& a) noexcept(true)
	: LogoutRequest(a.sequenceNumber) {
}

template<class MsgT>
inline ClientHeartbeat<MsgT>::ClientHeartbeat(SeqNum_t seqNum) noexcept(true)
	: Header_t(sizeof(ClientHeartbeat), seqNum) {
}

template<class MsgT>
inline LogonReply<MsgT>::LogonReply() noexcept(true)
	: Header_t(sizeof(LogonReply)) {
}

template<class MsgT>
inline LogonReply<MsgT>::LogonReply(SeqNum_t seqNum) noexcept(true)
	: Header_t(sizeof(LogonReply), seqNum) {
}

template<class MsgT>
struct LogonReply<MsgT>::respond {
	template<class ReplyMsg, class Op>
	void
	operator()(ReplyMsg const& msg, Op const& o) const noexcept(false) {
		switch(msg.rejectCode()) {
		case ReplyMsg::logon_success:
			o.operator()();
			break;
		default: {
			std::ostringstream os;
			os << "Failed to logon. Reject code='" << msg.rejectCode() << "'";
			BOOST_THROW_EXCEPTION(std::runtime_error(os.str()));
		}
		}
	}
};

template<class MsgT>
inline Logout<MsgT>::Logout() noexcept(true)
	: Header_t(sizeof(Logout)) {
}

template<class MsgT>
inline Logout<MsgT>::Logout(SeqNum_t seqNum, LogoutReason::element_type lr) noexcept(true)
	: Header_t(sizeof(Logout), seqNum), logoutReason(lr) {
}

template<class MsgT>
inline ServerHeartbeat<MsgT>::ServerHeartbeat(SeqNum_t seqNum) noexcept(true)
	: Header_t(sizeof(ServerHeartbeat), seqNum) {
}

template<class MsgT>
inline ReplayComplete<MsgT>::ReplayComplete(SeqNum_t seqNum) noexcept(true)
	: Header_t(sizeof(ReplayComplete), seqNum) {
}

template<class MsgT>
inline NewOrder<MsgT>::NewOrder(SeqNum_t seqNum, ClientOrderID_t const& clID, OrdType::element_type const oT, TIF::element_type const t, Side::element_type const s, Symbol_t const& symb, SecurityID_t const& instID, common::Quantity_t ordQty, Price_t p) noexcept(true)
	: Header_t(sizeof(NewOrder) - sizeof(bitfields_to_type_map), seqNum), side_(s), orderQty_(ordQty) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	bitfields.push_back<bitfields_tags_type::Price>(p);
	bitfields.push_back<bitfields_tags_type::OrdType>(oT);
	bitfields.push_back<bitfields_tags_type::TimeInForce>(t);
	bitfields.push_back<bitfields_tags_type::Symbol>(symb);
	bitfields.push_back<bitfields_tags_type::IDSource>(IDSource::ISIN);
	bitfields.push_back<bitfields_tags_type::SecurityID>(instID);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrdType::element_type const&
NewOrder<MsgT>::orderType() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::OrdType>();
}

template<class MsgT>
inline void
NewOrder<MsgT>::orderType(OrdType::element_type const& i) noexcept(true) {
	bitfields.at<bitfields_tags_type::OrdType>()= i;
}

template<class MsgT>
inline SecurityID_t const&
NewOrder<MsgT>::instrumentID() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::SecurityID>();
}

template<class MsgT>
inline void
NewOrder<MsgT>::instrumentID(SecurityID_t const& i) noexcept(true) {
	libjmmcg::memcpy_opt(i, bitfields.at<bitfields_tags_type::SecurityID>());
}

template<class MsgT>
inline Price_t
NewOrder<MsgT>::limitPrice() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Price>();
}

template<class MsgT>
inline void
NewOrder<MsgT>::limitPrice(Price_t p) noexcept(true) {
	bitfields.at<bitfields_tags_type::Price>()= p;
}

template<class MsgT>
inline TIF::element_type
NewOrder<MsgT>::tif() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::TimeInForce>();
}

template<class MsgT>
inline void
NewOrder<MsgT>::tif(TIF::element_type t) noexcept(true) {
	bitfields.at<bitfields_tags_type::TimeInForce>()= t;
}

template<class MsgT>
inline Symbol_t const&
NewOrder<MsgT>::symbol() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Symbol>();
}

template<class MsgT>
inline void
NewOrder<MsgT>::symbol(Symbol_t const& i) noexcept(true) {
	libjmmcg::memcpy_opt(i, bitfields.at<bitfields_tags_type::Symbol>());
}

template<class MsgT>
inline CancelOrder<MsgT>::CancelOrder(SeqNum_t seqNum, ClientOrderID_t const& origclID) noexcept(true)
	: Header_t(sizeof(CancelOrder) - sizeof(bitfields_to_type_map), seqNum) {
	libjmmcg::memcpy_opt(origclID, originalClientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline ModifyOrder<MsgT>::ModifyOrder(SeqNum_t seqNum, ClientOrderID_t const& clID, common::Quantity_t ordQty, Price_t p, Side::element_type s) noexcept(true)
	: Header_t(sizeof(ModifyOrder) - sizeof(bitfields_to_type_map), seqNum) {
	libjmmcg::memcpy_opt(clID, originalClientOrderID_);
	bitfields.push_back<bitfields_tags_type::OrderQty>(ordQty);
	bitfields.push_back<bitfields_tags_type::Price>(p);
	bitfields.push_back<bitfields_tags_type::Side>(s);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline Price_t
ModifyOrder<MsgT>::limitPrice() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Price>();
}

template<class MsgT>
inline void
ModifyOrder<MsgT>::limitPrice(Price_t p) noexcept(true) {
	bitfields.at<bitfields_tags_type::Price>()= p;
}

template<class MsgT>
inline common::Quantity_t
ModifyOrder<MsgT>::orderQty() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::OrderQty>();
}

template<class MsgT>
inline void
ModifyOrder<MsgT>::orderQty(common::Quantity_t i) noexcept(true) {
	bitfields.at<bitfields_tags_type::OrderQty>()= i;
}

template<class MsgT>
inline Side::element_type
ModifyOrder<MsgT>::side() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Side>();
}

template<class MsgT>
inline void
ModifyOrder<MsgT>::side(Side::element_type i) noexcept(true) {
	bitfields.at<bitfields_tags_type::Side>()= i;
}

template<class MsgT>
inline OrderAcknowledgement<MsgT>::OrderAcknowledgement(SeqNum_t seqNum, ClientOrderID_t const& clID) noexcept(true)
	: Header_t(sizeof(OrderAcknowledgement) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderRejected<MsgT>::OrderRejected() noexcept(true)
	: Header_t(sizeof(OrderRejected) - sizeof(bitfields_to_type_map)), transactionTime(), orderRejectReason() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderRejected<MsgT>::OrderRejected(SeqNum_t seqNum, ClientOrderID_t const& clID, OrderRejectReason::element_type orr) noexcept(true)
	: Header_t(sizeof(OrderRejected) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()), orderRejectReason(orr) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderModified<MsgT>::OrderModified() noexcept(true)
	: Header_t(sizeof(OrderModified) - sizeof(bitfields_to_type_map)), transactionTime() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderModified<MsgT>::OrderModified(SeqNum_t seqNum, ClientOrderID_t const& clID, Price_t p, Side::element_type s, common::Quantity_t ordQty) noexcept(true)
	: Header_t(sizeof(OrderModified) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	bitfields.push_back<bitfields_tags_type::Side>(s);
	bitfields.push_back<bitfields_tags_type::Price>(p);
	bitfields.push_back<bitfields_tags_type::OrderQuantity>(ordQty);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline Price_t
OrderModified<MsgT>::limitPrice() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Price>();
}

template<class MsgT>
inline void
OrderModified<MsgT>::limitPrice(Price_t p) noexcept(true) {
	bitfields.at<bitfields_tags_type::Price>()= p;
}

template<class MsgT>
inline common::Quantity_t
OrderModified<MsgT>::orderQty() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::OrderQuantity>();
}

template<class MsgT>
inline void
OrderModified<MsgT>::orderQty(common::Quantity_t i) noexcept(true) {
	bitfields.at<bitfields_tags_type::OrderQuantity>()= i;
}

template<class MsgT>
inline Side::element_type
OrderModified<MsgT>::side() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Side>();
}

template<class MsgT>
inline void
OrderModified<MsgT>::side(Side::element_type i) noexcept(true) {
	bitfields.at<bitfields_tags_type::Side>()= i;
}

template<class MsgT>
inline OrderRestated<MsgT>::OrderRestated(SeqNum_t seqNum, ClientOrderID_t const& clID) noexcept(true)
	: Header_t(sizeof(OrderRestated) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline UserModifyRejected<MsgT>::UserModifyRejected() noexcept(true)
	: Header_t(sizeof(UserModifyRejected) - sizeof(bitfields_to_type_map)), transactionTime(), modifyRejectReason() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline UserModifyRejected<MsgT>::UserModifyRejected(SeqNum_t seqNum, ClientOrderID_t const& clID, OrderRejectReason::element_type orr) noexcept(true)
	: Header_t(sizeof(UserModifyRejected) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()), modifyRejectReason(orr) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderCancelled<MsgT>::OrderCancelled() noexcept(true)
	: Header_t(sizeof(OrderCancelled) - sizeof(bitfields_to_type_map)), transactionTime(), cancelReason() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderCancelled<MsgT>::OrderCancelled(SeqNum_t seqNum, ClientOrderID_t const& clID, OrderRejectReason::element_type orr, Symbol_t const& i, Price_t p, Side::element_type s, common::Quantity_t ls, common::Quantity_t ordQty) noexcept(true)
	: Header_t(sizeof(OrderCancelled) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()), cancelReason(orr) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	bitfields.push_back<bitfields_tags_type::Side>(s);
	bitfields.push_back<bitfields_tags_type::Symbol>(i);
	bitfields.push_back<bitfields_tags_type::OrderQuantity>(ordQty);
	bitfields.push_back<bitfields_tags_type::LeavesQty>(ordQty);
	bitfields.push_back<bitfields_tags_type::LastShares>(ls);
	bitfields.push_back<bitfields_tags_type::LastPx>(p);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline Side::element_type
OrderCancelled<MsgT>::side() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Side>();
}

template<class MsgT>
inline common::Quantity_t
OrderCancelled<MsgT>::orderQty() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::OrderQuantity>();
}

template<class MsgT>
inline common::Quantity_t
OrderCancelled<MsgT>::leavesQty() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::LeavesQty>();
}

template<class MsgT>
inline common::Quantity_t
OrderCancelled<MsgT>::lastShares() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::LastShares>();
}

template<class MsgT>
inline Price_t
OrderCancelled<MsgT>::lastPrice() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::LastPx>();
}

template<class MsgT>
inline Symbol_t const&
OrderCancelled<MsgT>::symbol() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Symbol>();
}

template<class MsgT>
inline void
OrderCancelled<MsgT>::symbol(Symbol_t const& i) noexcept(true) {
	libjmmcg::memcpy_opt(i, bitfields.at<bitfields_tags_type::Symbol>());
}

template<class MsgT>
inline CancelRejected<MsgT>::CancelRejected() noexcept(true)
	: Header_t(sizeof(CancelRejected) - sizeof(bitfields_to_type_map)), transactionTime(), cancelRejectReason() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline CancelRejected<MsgT>::CancelRejected(SeqNum_t seqNum, ClientOrderID_t const& clID, OrderRejectReason::element_type crr) noexcept(true)
	: Header_t(sizeof(CancelRejected) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()), cancelRejectReason(crr) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderExecution<MsgT>::OrderExecution() noexcept(true)
	: Header_t(sizeof(OrderExecution) - sizeof(bitfields_to_type_map)), transactionTime() {
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline OrderExecution<MsgT>::OrderExecution(SeqNum_t seqNum, ClientOrderID_t const& clID, Price_t const price, SecurityID_t const& instID, Side::element_type s) noexcept(true)
	: Header_t(sizeof(OrderExecution) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()), lastPx(price) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	bitfields.push_back<bitfields_tags_type::Side>(s);
	bitfields.push_back<bitfields_tags_type::IDSource>(IDSource::ISIN);
	bitfields.push_back<bitfields_tags_type::SecurityID>(instID);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

template<class MsgT>
inline SecurityID_t const&
OrderExecution<MsgT>::instrumentID() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::SecurityID>();
}

template<class MsgT>
inline void
OrderExecution<MsgT>::instrumentID(SecurityID_t const& i) noexcept(true) {
	libjmmcg::memcpy_opt(i, bitfields.at<bitfields_tags_type::SecurityID>());
}

template<class MsgT>
inline Side::element_type
OrderExecution<MsgT>::side() const noexcept(true) {
	return bitfields.at<bitfields_tags_type::Side>();
}

template<class MsgT>
inline void
OrderExecution<MsgT>::side(Side::element_type s) noexcept(true) {
	bitfields.at<bitfields_tags_type::Side>()= s;
}

template<class MsgT>
inline TradeCancelOrCorrect<MsgT>::TradeCancelOrCorrect(SeqNum_t seqNum, ClientOrderID_t const& clID) noexcept(true)
	: Header_t(sizeof(TradeCancelOrCorrect) - sizeof(bitfields_to_type_map), seqNum), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	this->length_= static_cast<std::uint16_t>(this->length_ + static_cast<std::uint16_t>(bitfields.size()));
}

}}}}}
