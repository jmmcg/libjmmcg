#ifndef ISIMUD_EXCHANGES_BATSBOE_common_connectivity_policy_hpp
#define ISIMUD_EXCHANGES_BATSBOE_common_connectivity_policy_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"
#include "../../common/server_heartbeats.hpp"

#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <atomic>
#include <chrono>
#include <thread>
#include <exception>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

/// An implementation of the connectivity policy for the BATSBOE protocol.
/**
	From section 2.2 "Login, Replay & Sequencing" of the EU & US specifications.
*/
template<class LogonT>
class connectivity_policy {
public:
	using logon_args_t=LogonT;
	using endpoint_t=std::pair<boost::asio::ip::address, unsigned short>;
	struct gateways_t {
		endpoint_t primary_gateway;
		
		explicit gateways_t(endpoint_t const &primary) noexcept(true)
		: primary_gateway(primary) {
		}
		gateways_t(gateways_t const &gws) noexcept(true)
		: primary_gateway(gws.primary_gateway) {
		}
	};
	enum : unsigned {
		max_attempts=3
	};
	static inline constexpr const std::chrono::seconds min_timeout{5};
	const gateways_t gateways;
	const logon_args_t logon_args;
	const logoff_args_t logoff_args;

	explicit connectivity_policy(gateways_t const &gws, logon_args_t const &logon, logoff_args_t const &logoff) noexcept(true);

	template<class ConnectFn>
	void operator()(ConnectFn const &cnx) const noexcept(false);

private:
	template<class ConnectFn> static
	std::exception_ptr connection_failed(ConnectFn const &cnx, endpoint_t const &endpoint) noexcept(true);
};

/// Section 2.4 "Heartbeats" of [1]. Generate heartbeats from the containing simulator.
/**
	The simulator generates heartbeats to which the client responds.
*/
template<class MsgT>
class server_hb_t : public exchanges::common::server_hb_t<MsgT, 5, 1> {
public:
	using base_t=exchanges::common::server_hb_t<MsgT, 5, 1>;

	template<class ClientCxn>
	server_hb_t(ClientCxn &cxn, typename base_t::report_error_fn_t &report_error);
};

} } } } }

#include "connectivity_policy_impl.hpp"

#endif
