#ifndef ISIMUD_EXCHANGES_BATSBOE_common_types_hpp
#define ISIMUD_EXCHANGES_BATSBOE_common_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/iso_4217_currency_codes.hpp"

#include "core/yet_another_enum_wrapper.hpp"

#include <limits>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

using DateTime_t= std::uint64_t;
using MsgType_t= std::uint8_t;
using Price_t= std::uint64_t;
using Quantity_t= std::uint32_t;
using SeqNum_t= std::uint32_t;
using SPrice_t= std::int64_t;

using Account_t= std::array<char, 16>;
using ClearingAccount_t= std::array<char, 4>;
using ClearingFirm_t= std::array<char, 4>;
using ClientOrderID_t= std::array<char, 20>;
using ContraBroker_t= std::array<char, 4>;
using Currency_t= exchanges::common::ccy_codes::currency_t;
using LogoutReasonText_t= std::array<char, 60>;
using Password_t= std::array<char, 10>;
using PreventParticipantMatch_t= std::array<char, 3>;
using RoutingInst_t= std::array<char, 4>;
using SecurityExchange_t= std::array<char, 4>;
using SecurityID_t= std::array<char, 16>;
using SessionSubID_t= std::array<char, 4>;
using Symbol_t= std::array<char, 8>;
using SymbolSfx_t= std::array<char, 8>;
using Text_t= std::array<char, 60>;
using TradeReportID_t= std::array<char, 20>;
using TradeReportRefID_t= std::array<char, 20>;
using UserName_t= std::array<char, 4>;

/// The number of implied decimal-places for MIT use.
/**
	From Section 10 "List of Optional Fields".
*/
inline constexpr const Price_t implied_decimal_places= 10000;

/**
 * \TODO replace with LIBJMMCG_MAKE_ENUM_TAG_VALUES
 *
 * \see LIBJMMCG_MAKE_ENUM_TAG_VALUES
 */
LIBJMMCG_MAKE_ENUM_TAG_VALUES(MsgType,
	MsgType_t,
	(LogoutRequest, 0x02),
	(ClientHeartbeat, 0x03),
	(Logout, 0x08),
	(ServerHeartbeat, 0x09),
	(ReplayComplete, 0x13),
	(NewOrder, 0x04),
	(CancelOrder, 0x05),
	(ModifyOrder, 0x06),
	(OrderAcknowledgement, 0x0A),
	(OrderRejected, 0x0B),
	(OrderModified, 0x0C),
	(OrderRestated, 0x0D),
	(UserModifyRejected, 0x0E),
	(OrderCancelled, 0x0F),
	(CancelRejected, 0x10),
	(OrderExecution, 0x11),
	(TradeCancelOrCorrect, 0x12),
	(MatchAll, std::numeric_limits<MsgType_t>::max() - 1),	///< For the meta-state machine to allow a catch-all rule to reject anything unhandled.
	(Exit, std::numeric_limits<MsgType_t>::max())	///< For the meta-state machine: the exit state to exit the msm.
);

LIBJMMCG_MAKE_ENUM_TAG_VALUES(LoginResponseStatus,
	char,
	(LoginAccepted, 'A'),
	(NotAuthorized, 'N'),
	(SessionDisabled, 'D'),
	(SessionInUse, 'B'),
	(InvalidSession, 'S'),
	(SequenceAheadInLoginMessage, 'Q'),
	(InvalidUnitGivenInLoginMessage, 'I'),
	(InvalidReturnBitfieldInLoginMessage, 'F'),
	(InvalidLoginRequestMessageStructure, 'M'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(LogoutReason,
	char,
	(UserRequested, 'U'),
	(EndofDay, 'E'),
	(Administrative, 'A'),
	(ProtocolViolation, '!'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(Side,
	char,
	(Buy, '1'),
	(Sell, '2'),
	(Sell_short, '5'),
	(Sell_short_exempt, '6'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(ExecInst,
	char,
	(NoSpecialHandling, '\0'),
	(MarketPeg, 'P'),
	(PrimaryPeg, 'R'),
	(Midpoint, 'M'),
	(AlternateMidpoint, 'L'),
	(ExternalDarkOnly, 'u'),
	(ExternalDarkLit, 'v'),
	(ExternalLitOnly, 'w'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(OrdType,
	char,
	(Market, '1'),
	(Limit, '2'),
	(Pegged, 'P'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TIF,
	char,
	(Day, '0'),
	(GTC, '1'),	  ///< GTC (allowed, but treated as Day).
	(ATO, '2'),	  ///< At The Open (reserved for future use).
	(IOC, '3'),	  ///< IOC (Portion not filled immediately is cancelled. Market orders are implicitly IOC.).
	(GTD, '6'),	  ///< GTD (expires at earlier of specified ExpireTime or end of day).
	(ATC, '7')	 ///< At The Close (reserved for future use).
);

LIBJMMCG_MAKE_ENUM_TAG_VALUES(IDSource,
	char,
	(ISIN, '4'),
	(RIC, '5'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(Capacity,
	char,
	(Agency, 'A'),
	(Principal, 'P'),
	(Riskless, 'R'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(DisplayIndicator,
	char,
	(DisplayedOrder, 'X'),
	(Invisible, 'I'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(BookingType,
	char,
	(RegularBooking, '0'),
	(CFD, '1')	 ///< Contract for difference.
);

LIBJMMCG_MAKE_ENUM_TAG_VALUES(CancelOrigOnReject,
	char,
	(LeaveOriginal, 'N'),
	(CancelOriginal, 'Y'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(PreviouslyReported,
	char,
	(TradeWillPrint, 'N'),
	(TradeWillNotPrint, 'Y'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TransactionCategory,
	char,
	(RegularTrade, 'P'),	  ///< Aka Plain-Vanilla Trade.
	(SpecialPrice, 'F'),	  ///< Aka Trade with Conditions.
	(DarkTrade, 'D'),
	(TechnicalTrade, 'T'),
	(GiveUpGiveinTtrade, 'G'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TradeReportTransType,
	std::uint8_t,
	(New, 0),
	(Cancel, 1),
	(Replace, 2),
	(Release, 3));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(VenueType,
	char,
	(OffBook, 'X'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TradingSessionSubId,
	std::uint8_t,
	(ScheduledOpeningAuction, 2),
	(ScheduledClosingAuction, 4),
	(ScheduledIntradayAuction, 6),
	(UnscheduledAuction, 9),
	(ContinuousTrading, 3),
	(PostTrading, 5),
	(OutOfMainSessionTrading, 10));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(MatchType,
	std::uint8_t,
	(OffExchange, 1),
	(OnExchange, 3),
	(SystematicInternalizer, 9));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(SecondaryTrdType,
	std::uint8_t,
	(BenchmarkTrade, 58));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TradePriceCondition,
	std::uint8_t,
	(CumDividend, 0),
	(ExDividend, 2));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(TradePublishIndicator,
	std::uint8_t,
	(PublishTrade, 1),
	(DeferredPublication, 2));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(ExecutionMethod,
	char,
	(Automated, 'A'),
	(Manual, 'M'),
	(Unspecified, 'U'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(BaseLiquidityIndicator,
	char,
	(AddedLiquidity, 'A'),
	(RemovedLiquidity, 'R'),
	(RoutedAnotherMarket, 'X'),
	(AuctionTrade, 'C'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(SubLiquidityIndicator,
	char,
	(DarkPoolExecution, 'D'),
	(AddedRPILiquidity, 'E'),
	(RemovedLiquidity, 'T'),	///< From the BATS Dark Pool by IOC order.
	(AddedHiddenLiquidityPriceImproved, 'I'),
	(AddedHiddenLiquidity, 'H'),
	(ExecutionFromOrderJoinedNBBO, 'J'),
	(AddedLiquidityHiddenPortionIceberg, 'K'),	///< Trade added Liquidity from the hidden (reserve) portion of an iceberg order.
	(ExecutionFromOrderSetNBBO, 'S'),
	(AddedVisibleLiquidityPriceImproved, 'V'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(CCP,
	char,
	(EuropeanMultilateralClearingFacility, 'E'),
	(LCHClearnet, 'L'),
	(SIXXXClear, 'X'),
	(EuroCCP, 'C'),
	(NoneClearingSuppressed, 'N'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(OrderRejectReason,
	char,
	(Admin, 'A'),
	(CapacityUndefined, 'C'),
	(DuplicateClOrdID, 'D'),
	(Halted, 'H'),
	(IncorrectDataCenter, 'I'),
	(TooLateToCancel, 'J'),
	(OrderRateThresholdExceeded, 'K'),
	(PriceExceedsCrossRange, 'L'),
	(MaxSizeExceeded, 'M'),	  ///< OrderQty would cause LeavesQty to exceed allowable size.
	(NoLiquidityToExecuteAgainst, 'N'),
	(ClOrdIDNotMatchKnownOrder, 'O'),
	(CantModifyOrderInPendingFill, 'P'),
	(AwaitingFirstTrade, 'Q'),
	(RoutingUnavailable, 'R'),
	(RoutingOrderWouldTradeOnAwayDestination, 'T'),
	(UserRequested, 'U'),
	(WouldWash, 'V'),
	(AddLiquidityOnlyOrderWouldRemove, 'W'),
	(OrderExpired, 'X'),
	(SymbolNotSupported, 'Y'),
	(UnforeseenReason, 'Z'),
	(BrokerOption, 'b'),
	(LargeinScale, 'l'),
	(ReserveReload, 'r'),
	(MarketAccessRiskLimitExceeded, 'm'),
	(MaxOpenOrdersCountExceeded, 'o'),
	(CrossedMarket, 'x'),
	(OrderReceivedByBATSDuringReplay, 'y'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(RestatementReason,
	char,
	(RepricingofPegOrder, 'P'),
	(Reroute, 'R'),
	(LockedInCross, 'X'),
	(Wash, 'W'),
	(Reload, 'L'),
	(LiquidityUpdated, 'Q'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(Reason,
	char,
	(Admin, 'A'),
	(DuplicateTradeReportID, 'D'),
	(MarketAccessRiskLimitExceeded, 'm'),
	(SymbolNotSupported, 'Y'),
	(UnforseenReason, 'Z'));

LIBJMMCG_MAKE_ENUM_TAG_VALUES(AttributedQuote,
	char,
	(DoNotAttributeFirmMPID, 'N'),
	(AttributeFirmMPID, 'Y'));

}}}}}

#endif
