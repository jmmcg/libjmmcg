/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <functional>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t=libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t=libjmmcg::msm::hash::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	using transition_table=typename msm_base_t::template rows<
		typename row_t::template row<
			src_msg_details_t::NewOrder_t::static_type,
			typename base_t::template convert_then_send_seq_num<typename src_msg_details_t::NewOrder_t, typename dest_msg_details_t::NewOrder_t>,
			dest_msg_details_t::NewOrder_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelRequest_t::static_type,
			typename base_t::template convert_then_send_seq_num<typename src_msg_details_t::OrderCancelRequest_t, typename dest_msg_details_t::OrderCancelRequest_t>,
			dest_msg_details_t::OrderCancelRequest_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelReplace_t::static_type,
			typename base_t::template convert_then_send_seq_num<typename src_msg_details_t::OrderCancelReplace_t, typename dest_msg_details_t::OrderCancelReplaceRequest_t>,
			dest_msg_details_t::OrderCancelReplaceRequest_t::static_type
		>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename src_msg_details_t::Reject_t, true, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::OrderRejected_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline bool
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::process_msg(typename src_msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, socket_t &client_skt) {
	++sequenceNumber;
	typename src_msg_details_t::Header_t const &hdr=reinterpret_cast<typename src_msg_details_t::Header_t const &>(buff);
	const auto last_state=msm.process(hdr.type(), buff, exchg_skt, client_skt);
	return last_state==dest_msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::string
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<<"\n\tCurrent sequence number="<<sequenceNumber
		<<"Source message details: ";
	SrcMsgDetails::to_stream(ss);
	ss<<"\n\tDestination message details: ";
	DestMsgDetails::to_stream(ss);
	return ss.str();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t=libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t=libjmmcg::msm::hash::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	using transition_table=typename msm_base_t::template rows<
		typename row_t::template row<
			src_msg_details_t::OrderRejected_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::OrderRejected_t, typename dest_msg_details_t::OrderRejected_t>,
			dest_msg_details_t::OrderRejected_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelReject_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::OrderCancelReject_t, typename dest_msg_details_t::CancelRejected_t>,
			dest_msg_details_t::CancelRejected_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::UserModifyRejected_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::UserModifyRejected_t, typename dest_msg_details_t::BusinessMessageReject_t>,
			dest_msg_details_t::BusinessMessageReject_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::OrderCancelled_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::OrderCancelled_t, typename dest_msg_details_t::ExecutionReport_t>,
			dest_msg_details_t::ExecutionReport_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::ExecutionReport_t::static_type,
			typename base_t::template convert_then_send<typename src_msg_details_t::ExecutionReport_t, typename dest_msg_details_t::ExecutionReport_t>,
			dest_msg_details_t::ExecutionReport_t::static_type
		>,
		/**
			From section 2.4: "Heartbeats" of [1]: the response to a server Heartbeat is a Heartbeat.

			\param	msg	The message that was received, that shall be processed.
			\param exchg_skt	The socket to which any responses should be written.
			\return False to continue processing messages, true otherwise.
		*/
		typename row_t::template row<
			src_msg_details_t::ServerHeartbeat_t::static_type,
			typename base_t::template convert_then_send_seq_num<typename src_msg_details_t::ClientHeartbeat_t, typename src_msg_details_t::ClientHeartbeat_t, false>,
			dest_msg_details_t::ClientHeartbeat_t::static_type
		>,
		/**
			From section 2.5: "Logging Out" of [1]: the response to a Logout is to stop processing.

			\param	msg	The message that was received, that shall be processed.
			\param client_skt	The socket to which any responses should be written.
			\return False to continue processing messages, true otherwise.
		*/
		typename row_t::template row<
			src_msg_details_t::Logout_t::static_type,
			typename base_t::template no_op<typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::MsgTypes_t::Exit
		>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename dest_msg_details_t::Reject_t, true, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::Reject_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline bool
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::process_msg(typename src_msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) {
	++sequenceNumber;
	typename src_msg_details_t::Header_t const &hdr=reinterpret_cast<typename src_msg_details_t::Header_t const &>(buff);
	const auto last_state=msm.process(hdr.type(), buff, exchg_skt, client_cxn);
	return last_state==dest_msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::string
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream os;
	os<<"Source message details: ";
	SrcMsgDetails::to_stream(os);
	os<<"\n\tDestination message details: ";
	DestMsgDetails::to_stream(os);
	return os.str();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

template<class SrcMsgDetails, class SktT>
struct simulator_responses<SrcMsgDetails, SktT>::state_machine_t : libjmmcg::msm::hash::state_transition_table<state_machine_t> {
	using msm_base_t=libjmmcg::msm::hash::state_transition_table<state_machine_t>;
	using row_t=libjmmcg::msm::hash::row_types<typename msg_details_t::MsgTypes_t, typename msg_details_t::MsgTypes_t>;
	class LogonRequestResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		explicit LogonRequestResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr LogonRequestResponse(LogonRequestResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			From section 5.1: "Establishing a connection" of [1]: the response to a LogonRequest is a LogonReply.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &, socket_t &client_skt) const noexcept(false) {
			typename msg_details_t::LogonRequest_t const &msg=reinterpret_cast<typename msg_details_t::LogonRequest_t const &>(buff);
			DEBUG_ASSERT(msg.type()==state);
			typename msg_details_t::LogonReply_t reply(sim_resp.sequenceNumber);
			if (std::strcmp(msg.userName.begin(), sim_resp.username.begin())==0) {
				if (std::strcmp(msg.password.begin(), sim_resp.password.begin())==0) {
					reply.rejectCode(msg_details_t::LoginResponseStatus::LoginAccepted);
					client_skt.write(reply);
				} else {
					reply.rejectCode(msg_details_t::LoginResponseStatus::NotAuthorized);
					client_skt.write(reply);
				}
			} else {
				reply.rejectCode(msg_details_t::LoginResponseStatus::NotAuthorized);
				client_skt.write(reply);
			}
			return next;
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	class LogoutRequestResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		explicit LogoutRequestResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr LogoutRequestResponse(LogoutRequestResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			From section 5.3: "Terminating a connection" of [1]: the response to a Logout is a Logout.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &client_skt) const noexcept(false) {
			const typename msg_details_t::Logout_t reply(sim_resp.sequenceNumber, LogoutReason::UserRequested);
			client_skt.write(reply);
			return next;
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	class OrderCancelRequestResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values=static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderCancelled_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderCancelReject_t::static_type)
		);

		explicit OrderCancelRequestResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr OrderCancelRequestResponse(OrderCancelRequestResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a OrderCancelRequest.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &, socket_t &client_skt) const noexcept(false) {
			typename msg_details_t::OrderCancelRequest_t const &msg=reinterpret_cast<typename msg_details_t::OrderCancelRequest_t const &>(buff);
			DEBUG_ASSERT(msg.type()==state);
			DEBUG_ASSERT(!std::string(msg.originalClientOrderID().data()).empty());
			auto cancelled_order=sim_resp.order_book.find(msg.originalClientOrderID());
			if (cancelled_order!=sim_resp.order_book.end()) {
				const typename msg_details_t::OrderCancelled_t reply(
					sim_resp.sequenceNumber,
					msg.originalClientOrderID(),
					msg_details_t::OrderRejectReason::UserRequested,
					cancelled_order->second.symbol(),
					cancelled_order->second.limitPrice(),
					cancelled_order->second.side(),
					0,
					cancelled_order->second.orderQty()
				);
				sim_resp.order_book.erase(msg.originalClientOrderID());
				client_skt.write(reply);
				return msg_details_t::OrderCancelled_t::static_type;
			} else {
				const typename msg_details_t::OrderCancelReject_t reply(sim_resp.sequenceNumber, msg.originalClientOrderID(), msg_details_t::OrderRejectReason::ClOrdIDNotMatchKnownOrder);
				client_skt.write(reply);
				return msg_details_t::OrderCancelReject_t::static_type;
			}
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	class OrderCancelReplaceRequestResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values=static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderModified_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::UserModifyRejected_t::static_type)
		);

		explicit OrderCancelReplaceRequestResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr OrderCancelReplaceRequestResponse(OrderCancelReplaceRequestResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a OrderCancelReplaceRequest.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &, socket_t &client_skt) const noexcept(false) {
			typename msg_details_t::OrderCancelReplaceRequest_t const &msg=reinterpret_cast<typename msg_details_t::OrderCancelReplaceRequest_t const &>(buff);
			DEBUG_ASSERT(msg.type()==state);
			auto modified_order=sim_resp.order_book.find(msg.originalClientOrderID());
			if (modified_order!=sim_resp.order_book.end()) {
				modified_order->second.limitPrice(msg.limitPrice());
				modified_order->second.orderQty(msg.orderQty());
				modified_order->second.side(msg.side());
				const typename msg_details_t::OrderModified_t reply(
					sim_resp.sequenceNumber,
					msg.originalClientOrderID(),
					modified_order->second.limitPrice(),
					modified_order->second.side(),
					modified_order->second.orderQty()
				);
				client_skt.write(reply);
				return msg_details_t::OrderModified_t::static_type;
			} else {
				const typename msg_details_t::UserModifyRejected_t reply(sim_resp.sequenceNumber, msg.originalClientOrderID(), OrderRejectReason::ClOrdIDNotMatchKnownOrder);
				client_skt.write(reply);
				return msg_details_t::UserModifyRejected_t::static_type;
			}
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	class NewOrderResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		static inline constexpr const typename msg_details_t::MsgTypes_t exit_values=static_cast<typename msg_details_t::MsgTypes_t>(
			static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::ExecutionReport_t::static_type)
			| static_cast<typename std::underlying_type<typename msg_details_t::MsgTypes_t>::type>(msg_details_t::OrderRejected_t::static_type)
		);

		explicit NewOrderResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr NewOrderResponse(NewOrderResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			From section 9.1 "Order handling" of [1]: the response to a NewOrder.

			\return The next state from the declaration of the table.
		*/
		template<auto state, auto>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &, socket_t &client_skt) const noexcept(false) {
			typename msg_details_t::NewOrder_t const &msg=reinterpret_cast<typename msg_details_t::NewOrder_t const &>(buff);
			DEBUG_ASSERT(msg.type()==state);

			if (msg.instrumentID()==sim_resp.instrumentID) {
				typename msg_details_t::ExecutionReport_t reply(
					sim_resp.sequenceNumber,
					msg.clientOrderID(),
					sim_resp.scaled_price,
					msg.instrumentID(),
					msg.side()
				);
				if (msg.orderQty()<=sim_resp.quantity_limit) {
					if (msg.orderType()==msg_details_t::OrderType::Market
						|| (msg.side()==msg_details_t::Side::Buy && msg.limitPrice()<sim_resp.scaled_price)
						|| (msg.side()==msg_details_t::Side::Sell && msg.limitPrice()>sim_resp.scaled_price)) {
						reply.executedQty(msg.orderQty());
						reply.leavesQty(0);
						reply.executedPrice(msg.limitPrice());
						client_skt.write(reply);
					} else {
						sim_resp.order_book.emplace(msg.clientOrderID(), msg);
						DEBUG_ASSERT(!sim_resp.order_book.empty());
						DEBUG_ASSERT(sim_resp.order_book.find(msg.clientOrderID())!=sim_resp.order_book.end());
					}
				} else {
					auto new_item=sim_resp.order_book.emplace(msg.clientOrderID(), msg);
					new_item.first->second.orderQty(msg.orderQty()-sim_resp.quantity_limit);
					reply.executedQty(sim_resp.quantity_limit);
					reply.leavesQty(msg.orderQty()-sim_resp.quantity_limit);
					DEBUG_ASSERT(!sim_resp.order_book.empty());
					DEBUG_ASSERT(sim_resp.order_book.find(msg.clientOrderID())!=sim_resp.order_book.end());
					client_skt.write(reply);
				}
				return msg_details_t::ExecutionReport_t::static_type;
			} else {
				const typename msg_details_t::OrderRejected_t reply(sim_resp.sequenceNumber, msg.clientOrderID(), msg_details_t::OrderRejectReason::SymbolNotSupported);
				client_skt.write(reply);
				return msg_details_t::OrderRejected_t::static_type;
			}
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	class RejectUnhandledResponse {
	public:
		using end_states=typename msg_details_t::MsgTypes_t;
		using arguments_types=std::pair<
			end_states,
			std::tuple<
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &>,
				std::tuple<typename msg_details_t::msg_buffer_t const &, socket_t &, exchanges::common::socket_type::client_cxn_ptr_t &>
			>
		>;

		explicit RejectUnhandledResponse(simulator_responses &sr) noexcept(true)
		: sim_resp(sr) {}
		constexpr RejectUnhandledResponse(RejectUnhandledResponse const &v) noexcept(true)
		: sim_resp(v.sim_resp) {}

		/**
			\param client_skt	The socket to which any responses should be written.
			\return The next state from the declaration of the table.
		*/
		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &, socket_t &, socket_t &client_skt) const {
			const typename msg_details_t::OrderRejected_t response(sim_resp.sequenceNumber, typename msg_details_t::ClientOrderID_t(), msg_details_t::OrderRejected_t::unknown_msg);
			client_skt.write(response);
			return next;
		}

		template<auto state, auto next>
		end_states
		process(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t &client_cxn) const noexcept(false) {
			return process<state, next>(buff, exchg_skt, client_cxn->socket());
		}

	private:
		simulator_responses &sim_resp;
	};
	using transition_table=typename msm_base_t::template rows<
		typename row_t::template row<
			msg_details_t::LogonRequest_t::static_type,
			LogonRequestResponse,
			msg_details_t::LogonReply_t::static_type
		>,
		/**
			From section 2.4: "Heartbeats" of [1]: the response to a Heartbeat is nothing.
		*/
		typename row_t::template row<
			msg_details_t::ClientHeartbeat_t::static_type,
			typename base_t::template no_op<typename msg_details_t::MsgTypes_t>,
			msg_details_t::ServerHeartbeat_t::static_type
		>,
		typename row_t::template row<
			msg_details_t::LogoutRequest_t::static_type,
			LogoutRequestResponse,
			msg_details_t::MsgTypes_t::Exit
		>,
		typename row_t::template row<
			msg_details_t::OrderCancelRequest_t::static_type,
			OrderCancelRequestResponse,
			OrderCancelRequestResponse::exit_values
		>,
		typename row_t::template row<
			msg_details_t::OrderCancelReplaceRequest_t::static_type,
			OrderCancelReplaceRequestResponse,
			OrderCancelReplaceRequestResponse::exit_values
		>,
		typename row_t::template row<
			msg_details_t::NewOrder_t::static_type,
			NewOrderResponse,
			NewOrderResponse::exit_values
		>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			msg_details_t::MsgTypes_t::MatchAll,
			RejectUnhandledResponse,
			msg_details_t::OrderRejected_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class SktT>
inline bool
simulator_responses<SrcMsgDetails, SktT>::process_msg(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, socket_t &client_skt) {
	++(this->sequenceNumber);
	typename msg_details_t::Header_t const &hdr=reinterpret_cast<typename msg_details_t::Header_t const &>(buff);
	const auto last_state=msm.process(hdr.type(), buff, exchg_skt, client_skt);
	return last_state==msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class SktT> inline std::string
simulator_responses<SrcMsgDetails, SktT>::to_string() const noexcept(false) {
	std::ostringstream os;
	os
		<<base_t::to_string()
		<<"\n\tValid instrument ID="<<instrumentID
		<<"\n\tInvalid symbol="<<invalidSymbol
		<<"\n\tInvalid instrument ID="<<invalidInstrumentID;
		return os.str();
}

template<class SrcMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, simulator_responses<SrcMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

} } } } }
