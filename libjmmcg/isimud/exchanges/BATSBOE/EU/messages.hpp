#ifndef ISIMUD_EXCHANGES_BATSBOE_EU_MESSAGES_HPP
#define ISIMUD_EXCHANGES_BATSBOE_EU_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_HDR

#include "types.hpp"

#include "optional_field_types.hpp"

#include "../common/messages.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE {

/**
	From <a href="http://cdn.batstrading.com/resources/participant_resources/BATS_Europe_Binary_Order_Entry_Specification.pdf>"BATS Chi-X Europe Binary Order Entry Specification", Version 1.44, 27 November, 2014</a>
*/
namespace EU {

/**
	Section: "4.1.4 Trade Capture Report"
*/
struct [[gnu::packed]] TradeCaptureReport : public common::Header<MsgType::TradeCaptureReport> {
	using Header_t= common::Header<MsgType::TradeCaptureReport>;
	using bitfields_to_type_map= optional::TradeCaptureReport::bitfields_to_type_map;
	using bitfields_tags_type= bitfields_to_type_map::bitfields_tags_type;

	explicit TradeCaptureReport(common::SeqNum_t seqNumber) noexcept(true);

private:
	common::TradeReportID_t tradeReportID;
	common::Quantity_t lastShares;
	common::Price_t lastPx;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.10 Trade Capture Report Ack"
*/
struct [[gnu::packed]] TradeCaptureReportAck : public common::Header<MsgType::TradeCaptureReportAck> {
	using Header_t= common::Header<MsgType::TradeCaptureReportAck>;
	using bitfields_to_type_map= optional::TradeCaptureReportAck::bitfields_to_type_map;
	using bitfields_tags_type= bitfields_to_type_map::bitfields_tags_type;

	const common::DateTime_t transactionTime;

	explicit TradeCaptureReportAck(common::SeqNum_t seqNumber) noexcept(true);

private:
	common::TradeReportID_t tradeReportID;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.11 Trade Capture Report Reject"
*/
struct [[gnu::packed]] TradeCaptureReportReject : public common::Header<MsgType::TradeCaptureReportReject> {
	using Header_t= common::Header<MsgType::TradeCaptureReportReject>;
	using bitfields_to_type_map= optional::TradeCaptureReportAck::bitfields_to_type_map;
	using bitfields_tags_type= bitfields_to_type_map::bitfields_tags_type;

	const common::DateTime_t transactionTime;

	explicit TradeCaptureReportReject(common::SeqNum_t seqNumber) noexcept(true);

private:
	common::TradeReportID_t tradeReportID;
	common::Reason::element_type reason;
	common::Text_t text;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.12 Trade Capture Confirm"
*/
struct [[gnu::packed]] TradeCaptureConfirm : public common::Header<MsgType::TradeCaptureConfirm> {
	using Header_t= common::Header<MsgType::TradeCaptureConfirm>;
	using bitfields_to_type_map= optional::TradeCaptureConfirm::bitfields_to_type_map;
	using bitfields_tags_type= bitfields_to_type_map::bitfields_tags_type;

	const common::DateTime_t transactionTime;

	explicit TradeCaptureConfirm(common::SeqNum_t seqNumber) noexcept(true);

private:
	common::TradeReportID_t tradeReportID;
	common::TradeReportRefID_t tradeReportRefID;
	uint64_t tradeID;
	common::Quantity_t lastShares;
	common::Price_t lastPx;
	common::ContraBroker_t contraBroker;
	common::Reason::element_type reason;
	common::Text_t text;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.13 Trade Capture Decline"
*/
struct [[gnu::packed]] TradeCaptureDecline : public common::Header<MsgType::TradeCaptureDecline> {
	using Header_t= common::Header<MsgType::TradeCaptureDecline>;
	using bitfields_to_type_map= optional::TradeCaptureConfirm::bitfields_to_type_map;
	using bitfields_tags_type= bitfields_to_type_map::bitfields_tags_type;

	const common::DateTime_t transactionTime;

	explicit TradeCaptureDecline(common::SeqNum_t seqNumber) noexcept(true);

private:
	common::TradeReportID_t tradeReportID;
	common::TradeReportRefID_t tradeReportRefID;
	uint64_t tradeID;
	common::Quantity_t lastShares;
	common::Price_t lastPx;
	common::ContraBroker_t contraBroker;
	common::Reason::element_type reason;
	common::Text_t text;
	bitfields_to_type_map bitfields;
};

}
}}}}

#include "messages_impl.hpp"

#endif
