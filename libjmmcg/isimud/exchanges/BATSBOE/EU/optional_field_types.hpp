#ifndef ISIMUD_EXCHANGES_BATSBOE_EU_optional_field_types_hpp
#define ISIMUD_EXCHANGES_BATSBOE_EU_optional_field_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../common/optional_field_types.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace EU { namespace optional {

namespace TradeCaptureReport {

	enum class bitfields_t : uint32_t {
		Symbol=(0x1UL<<0),
		Reserved1=(0x1UL<<1),
		Currency=(0x1UL<<2),
		IDSource=(0x1UL<<3),
		SecurityID=(0x1UL<<4),
		SecurityExchange=(0x1UL<<5),
		ExecInst=(0x1UL<<6),
		PreviouslyReported=(0x1UL<<7),
		Capacity=(0x1UL<<8),
		Account=(0x1UL<<9),
		TransactionCategory=(0x1UL<<10),
		TradeTime=(0x1UL<<11),
		PartyRole=(0x1UL<<12),
		TradeReportTransType=(0x1UL<<13),
		TradeID=(0x1UL<<14),
		VenueType=(0x1UL<<15),
		TradingSessionSubId=(0x1UL<<16),
		MatchType=(0x1UL<<17),
		TrdSubType=(0x1UL<<18),
		SecondaryTrdType=(0x1UL<<19),
		TradePriceCondition=(0x1UL<<20),
		TradePublishIndicator=(0x1UL<<21),
		LargeSize=(0x1UL<<22),
		ExecutionMethod=(0x1UL<<23)
	};

	using bitfields_to_type_map=libjmmcg::bitfield_map<
		boost::mpl::map<
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
				common::Symbol_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
				common::Currency_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
				common::IDSource
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
				common::SecurityID_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
				common::SecurityExchange_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
				common::ExecInst
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PreviouslyReported>::type,
				common::PreviouslyReported
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
				common::Capacity
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Account>::type,
				common::Account_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TransactionCategory>::type,
				common::TransactionCategory
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradeTime>::type,
				common::DateTime_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PartyRole>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradeReportTransType>::type,
				common::TradeReportTransType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradeID>::type,
				uint64_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::VenueType>::type,
				common::VenueType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradingSessionSubId>::type,
				common::TradingSessionSubId
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MatchType>::type,
				common::MatchType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TrdSubType>::type,
				std::uint8_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecondaryTrdType>::type,
				common::SecondaryTrdType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradePriceCondition>::type,
				common::TradePriceCondition
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradePublishIndicator>::type,
				common::TradePublishIndicator
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::LargeSize>::type,
				uint64_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ExecutionMethod>::type,
				common::ExecutionMethod
			>
		>,
		3
	>;

}

namespace TradeCaptureReportAck {

	enum class bitfields_t : uint64_t {
		Side=(0x1UL<<0),
		PegDifference=(0x1UL<<1),
		Price=(0x1UL<<2),
		ExecInst=(0x1UL<<3),
		OrdType=(0x1UL<<4),
		TimeInForce=(0x1UL<<5),
		MinQty=(0x1UL<<6),
		MaxRemovePct=(0x1UL<<7),
		Symbol=(0x1UL<<8),
		Reserved1=(0x1UL<<9),
		Currency=(0x1UL<<10),
		IDSource=(0x1UL<<11),
		SecurityID=(0x1UL<<12),
		SecurityExchange=(0x1UL<<13),
		Capacity=(0x1UL<<14),
		CrossFlag=(0x1UL<<15),
		Account=(0x1UL<<16),
		ClearingFirm=(0x1UL<<17),
		ClearingAccount=(0x1UL<<18),
		DisplayIndicator=(0x1UL<<19),
		MaxFloor=(0x1UL<<20),
		Reserved2=(0x1UL<<21),
		OrderQuantity=(0x1UL<<22),
		PreventParticipantMatch=(0x1UL<<23),
		Reserved3=(0x1UL<<24),
		Reserved4=(0x1UL<<25),
		Reserved5=(0x1UL<<26),
		Reserved6=(0x1UL<<27),
		Reserved7=(0x1UL<<28),
		CorrectedSize=(0x1UL<<29),
		PartyID=(0x1UL<<30),
		Reserved8=(0x1UL<<31),
		Reserved9=(0x1UL<<32),
		Reserved10=(0x1UL<<33),
		Reserved11=(0x1UL<<34),
		Reserved12=(0x1UL<<35),
		Reserved13=(0x1UL<<36),
		Reserved14=(0x1UL<<37),
		Reserved15=(0x1UL<<38),
		Reserved16=(0x1UL<<39),
		Reserved17=(0x1UL<<40),
		CCP=(0x1UL<<41),
		Reserved18=(0x1UL<<42),
		Reserved19=(0x1UL<<43),
		Reserved20=(0x1UL<<44),
		Reserved21=(0x1UL<<45),
		Reserved22=(0x1UL<<46),
		PartyRole=(0x1UL<<47),
		Reserved23=(0x1UL<<48),
		TradeReportTypeReturn=(0x1UL<<49),
		Reserved24=(0x1UL<<50),
		Reserved25=(0x1UL<<51),
		Reserved26=(0x1UL<<52),
		Reserved27=(0x1UL<<53),
		LargeSize=(0x1UL<<54)
	};

	using bitfields_to_type_map=libjmmcg::bitfield_map<
		boost::mpl::map<
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Side>::type,
				common::Side
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
				common::SPrice_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Price>::type,
				common::Price_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
				common::ExecInst
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
				common::OrdType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
				common::TIF
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
				std::uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
				std::uint8_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
				common::Symbol_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
				common::Currency_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
				common::IDSource
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
				common::SecurityID_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
				common::SecurityExchange_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
				common::Capacity
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Account>::type,
				common::Account_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
				common::ClearingFirm_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
				common::ClearingAccount_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
				common::DisplayIndicator
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
				common::PreventParticipantMatch_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CorrectedSize>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PartyID>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved9>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved17>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
				common::CCP
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved19>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved20>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved21>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved22>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PartyRole>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved23>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradeReportTypeReturn>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved24>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved25>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved26>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved27>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::LargeSize>::type,
				uint64_t
			>
		>
	>;

}

namespace TradeCaptureConfirm {

	enum class bitfields_t : uint64_t {
		Side=(0x1UL<<0),
		PegDifference=(0x1UL<<1),
		Price=(0x1UL<<2),
		ExecInst=(0x1UL<<3),
		OrdType=(0x1UL<<4),
		TimeInForce=(0x1UL<<5),
		MinQty=(0x1UL<<6),
		MaxRemovePct=(0x1UL<<7),
		Symbol=(0x1UL<<8),
		Reserved1=(0x1UL<<9),
		Currency=(0x1UL<<10),
		IDSource=(0x1UL<<11),
		SecurityID=(0x1UL<<12),
		SecurityExchange=(0x1UL<<13),
		Capacity=(0x1UL<<14),
		CrossFlag=(0x1UL<<15),
		Account=(0x1UL<<16),
		ClearingFirm=(0x1UL<<17),
		ClearingAccount=(0x1UL<<18),
		DisplayIndicator=(0x1UL<<19),
		MaxFloor=(0x1UL<<20),
		Reserved2=(0x1UL<<21),
		OrderQuantity=(0x1UL<<22),
		PreventParticipantMatch=(0x1UL<<23),
		Reserved3=(0x1UL<<24),
		Reserved4=(0x1UL<<25),
		Reserved5=(0x1UL<<26),
		Reserved6=(0x1UL<<27),
		Reserved7=(0x1UL<<28),
		CorrectedSize=(0x1UL<<29),
		PartyID=(0x1UL<<30),
		Reserved8=(0x1UL<<31),
		Reserved9=(0x1UL<<32),
		Reserved10=(0x1UL<<33),
		Reserved11=(0x1UL<<34),
		Reserved12=(0x1UL<<35),
		Reserved13=(0x1UL<<36),
		Reserved14=(0x1UL<<37),
		Reserved15=(0x1UL<<38),
		Reserved16=(0x1UL<<39),
		Reserved17=(0x1UL<<40),
		CCP=(0x1UL<<41),
		Reserved18=(0x1UL<<42),
		Reserved19=(0x1UL<<43),
		Reserved20=(0x1UL<<44),
		Reserved21=(0x1UL<<45),
		Reserved22=(0x1UL<<46),
		PartyRole=(0x1UL<<47),
		Reserved23=(0x1UL<<48),
		TradeReportTypeReturn=(0x1UL<<49),
		TradePublishIndReturn=(0x1UL<<50),
		Text=(0x1UL<<51),
		Reserved24=(0x1UL<<52),
		Reserved25=(0x1UL<<53),
		LargeSize=(0x1UL<<54)
	};

	using bitfields_to_type_map=libjmmcg::bitfield_map<
		boost::mpl::map<
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Side>::type,
				common::Side
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PegDifference>::type,
				common::SPrice_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Price>::type,
				common::Price_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ExecInst>::type,
				common::ExecInst
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::OrdType>::type,
				common::OrdType
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TimeInForce>::type,
				common::TIF
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MinQty>::type,
				std::uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MaxRemovePct>::type,
				std::uint8_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Symbol>::type,
				common::Symbol_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved1>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Currency>::type,
				common::Currency_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::IDSource>::type,
				common::IDSource
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityID>::type,
				common::SecurityID_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::SecurityExchange>::type,
				common::SecurityExchange_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Capacity>::type,
				common::Capacity
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CrossFlag>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Account>::type,
				common::Account_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ClearingFirm>::type,
				common::ClearingFirm_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::ClearingAccount>::type,
				common::ClearingAccount_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::DisplayIndicator>::type,
				common::DisplayIndicator
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::MaxFloor>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved2>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::OrderQuantity>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PreventParticipantMatch>::type,
				common::PreventParticipantMatch_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved3>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved4>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved5>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved6>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved7>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CorrectedSize>::type,
				uint32_t
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PartyID>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved8>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved9>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved10>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved11>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved12>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved13>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved14>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved15>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved16>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved17>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::CCP>::type,
				common::CCP
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved18>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved19>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved20>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved21>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved22>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::PartyRole>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved23>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradeReportTypeReturn>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::TradePublishIndReturn>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Text>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved24>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::Reserved25>::type,
				libjmmcg::a_zero_sized_class
			>,
			boost::mpl::pair<
				std::integral_constant<bitfields_t, bitfields_t::LargeSize>::type,
				uint64_t
			>
		>
	>;

}

} } } } } }

#endif
