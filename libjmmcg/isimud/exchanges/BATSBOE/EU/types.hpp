#ifndef ISIMUD_EXCHANGES_BATSBOE_EU_types_hpp
#define ISIMUD_EXCHANGES_BATSBOE_EU_types_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../common/types.hpp"

#include "core/yet_another_enum_wrapper.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace EU {

LIBJMMCG_MAKE_ENUM_TAG_VALUES(MsgType,
	common::MsgType_t,
	(LogoutRequest, static_cast<common::MsgType_t>(common::MsgType::LogoutRequest)),
	(ClientHeartbeat, static_cast<common::MsgType_t>(common::MsgType::ClientHeartbeat)),
	(Logout, static_cast<common::MsgType_t>(common::MsgType::Logout)),
	(ServerHeartbeat, static_cast<common::MsgType_t>(common::MsgType::ServerHeartbeat)),
	(ReplayComplete, static_cast<common::MsgType_t>(common::MsgType::ReplayComplete)),
	(NewOrder, static_cast<common::MsgType_t>(common::MsgType::NewOrder)),
	(CancelOrder, static_cast<common::MsgType_t>(common::MsgType::CancelOrder)),
	(ModifyOrder, static_cast<common::MsgType_t>(common::MsgType::ModifyOrder)),
	(OrderAcknowledgement, static_cast<common::MsgType_t>(common::MsgType::OrderAcknowledgement)),
	(OrderRejected, static_cast<common::MsgType_t>(common::MsgType::OrderRejected)),
	(OrderModified, static_cast<common::MsgType_t>(common::MsgType::OrderModified)),
	(OrderRestated, static_cast<common::MsgType_t>(common::MsgType::OrderRestated)),
	(UserModifyRejected, static_cast<common::MsgType_t>(common::MsgType::UserModifyRejected)),
	(OrderCancelled, static_cast<common::MsgType_t>(common::MsgType::OrderCancelled)),
	(CancelRejected, static_cast<common::MsgType_t>(common::MsgType::CancelRejected)),
	(OrderExecution, static_cast<common::MsgType_t>(common::MsgType::OrderExecution)),
	(TradeCancelOrCorrect, static_cast<common::MsgType_t>(common::MsgType::TradeCancelOrCorrect)),
	(LogonRequest, 0x1E),
	(LogonReply, 0x1F),
	(TradeCaptureReport, 0x17),
	(TradeCaptureReportAck, 0x18),
	(TradeCaptureReportReject, 0x19),
	(TradeCaptureConfirm, 0x1A),
	(TradeCaptureDecline, 0x1B),
	(MatchAll, static_cast<common::MsgType::underlying_type_t>(common::MsgType::MatchAll)),
	(Exit, static_cast<common::MsgType::underlying_type_t>(common::MsgType::Exit)));

}}}}}

#endif
