#ifndef LIBJMMCG_ISIMUD_EXCHANGES_LINKS_SMART_ORDER_ROUTER_HPP
#define LIBJMMCG_ISIMUD_EXCHANGES_LINKS_SMART_ORDER_ROUTER_HPP

/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/isin.hpp"

#include <map>

namespace isimud { namespace ISIMUD_VER_NAMESPACE {

	/// An idea for implementing a very simple smart order router.
	class smart_order_router {
	public:
		struct available_to_trade {
			using price_t=int;	///< Probably needs to be a variant over the various price_types of the links if there is no common_type.
			using volume_t=int;	///< Probably needs to be a variant over the various volume_types of the links if there is no common_type.

			bool operator<(available_to_trade const &) const noexcept(true);
		};
		using link_t=int;	///< Base class of all the links.
		using link_ptr_t=link_t*;
		/**
			A collection that contains the ISINs that are tradeable on multiple exchanges ordered by ISIN->{price, volume}->link. So that as market data comes in the prices & available volumes are updated.

			May want to use a sorted vector instead....
		 */
		using container_type=std::map<
			exchanges::ISIN_t,	///< These are only ISINs that are traded on multiple venues.
			/**
			 * May want to use a std::heap instead, as only need the best available, not all in order.
			 */
			std::map<
				available_to_trade,
				link_ptr_t
			>
		>;

		/// The market-data thread receives updates for the subscribed ISINs and calls this function to update the internal container.
		/**
		 * Re-insert the ISIN into the map with the updated details.
		 */
		void market_data_update(ISIN_t const &, available_to_trade::price_t const &, available_to_trade::volume_t const &);

		/// Call to place an order via the SOR.
		/**
		* Places the trade on the venue that has the best price & most volume. Could split the trade between venues.
		*/
		void place_an_order(...) noexcept(false);

	private:
		/**
			TODO We'll need some kind of funky locking for this, probably multi-level....
		 */
		container_type multi_venue_instruments;
	};

} }

#endif

