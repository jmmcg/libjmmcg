#ifndef ISIMUD_EXCHANGES_links_fix_to_links_hpp
#define ISIMUD_EXCHANGES_links_fix_to_links_hpp

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_MICS_HDR

#include "../BATSBOE/EU/v1/batsboe.hpp"
#include "../BATSBOE/US/v1/batsboe.hpp"
#include "../MIT/BIT/bit.hpp"
#include "../MIT/JSE/jse.hpp"
#include "../MIT/LSE/lse.hpp"
#include "../MIT/OSLO/oslo.hpp"
#include "../MIT/TRQ/trq.hpp"

#include "fix_to_batsboe_program_options.hpp"
#include "fix_to_general_program_options.hpp"
#include "fix_to_mit_program_options.hpp"
#include "fix_to_link_main.hpp"

#include "../conversions/fix_to_batsboe_eu_conversions.hpp"
#include "../conversions/fix_to_batsboe_us_conversions.hpp"
#include "../conversions/fix_to_mit_conversions.hpp"
#include "../conversions/batsboe_eu_to_fix_conversions.hpp"
#include "../conversions/batsboe_us_to_fix_conversions.hpp"
#include "../conversions/mit_to_fix_conversions.hpp"

#include <fstream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

class fix_to_links : public fix_to_link {
public:
	using base_t=fix_to_link;
	template<class ExchgCxns>
	using client_cxn_t=libjmmcg::socket::svr<
		libjmmcg::socket::server_manager::forwarding<ExchgCxns, FIX::v5_0sp2::connection_t::socket_t>
	>;
	using links_t=common::exchange_connection<
		client_cxn_t,
		common::connection_processor<
			MIT::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, MIT::BIT::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			MIT::common::exchange_to_client_transformations<MIT::BIT::MsgTypes, FIX::v5_0sp2::MsgTypes, MIT::BIT::connection_t::socket_t>,
			MIT::common::connectivity_policy<MIT::BIT::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			MIT::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, MIT::JSE::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			MIT::common::exchange_to_client_transformations<MIT::JSE::MsgTypes, FIX::v5_0sp2::MsgTypes, MIT::JSE::connection_t::socket_t>,
			MIT::common::connectivity_policy<MIT::JSE::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			MIT::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, MIT::LSE::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			MIT::common::exchange_to_client_transformations<MIT::LSE::MsgTypes, FIX::v5_0sp2::MsgTypes, MIT::LSE::connection_t::socket_t>,
			MIT::common::connectivity_policy<MIT::LSE::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			MIT::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, MIT::OSLO::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			MIT::common::exchange_to_client_transformations<MIT::OSLO::MsgTypes, FIX::v5_0sp2::MsgTypes, MIT::OSLO::connection_t::socket_t>,
			MIT::common::connectivity_policy<MIT::OSLO::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			MIT::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, MIT::TRQ::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			MIT::common::exchange_to_client_transformations<MIT::TRQ::MsgTypes, FIX::v5_0sp2::MsgTypes, MIT::TRQ::connection_t::socket_t>,
			MIT::common::connectivity_policy<MIT::TRQ::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			BATSBOE::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, BATSBOE::EU::v1::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			BATSBOE::common::exchange_to_client_transformations<BATSBOE::EU::v1::MsgTypes, FIX::v5_0sp2::MsgTypes, BATSBOE::EU::v1::connection_t::socket_t>,
			BATSBOE::common::connectivity_policy<BATSBOE::EU::v1::MsgTypes::logon_args_t>
		>,
		common::connection_processor<
			BATSBOE::common::client_to_exchange_transformations<FIX::v5_0sp2::MsgTypes, BATSBOE::US::v1::MsgTypes, FIX::v5_0sp2::connection_t::socket_t>,
			BATSBOE::common::exchange_to_client_transformations<BATSBOE::US::v1::MsgTypes, FIX::v5_0sp2::MsgTypes, BATSBOE::US::v1::connection_t::socket_t>,
			BATSBOE::common::connectivity_policy<BATSBOE::US::v1::MsgTypes::logon_args_t>
		>
	>;
	template<class... ExchgLinks>
	struct get_links_ctor_args;
	template<class... ExchgLinks>
	struct get_all_ctor_args;
	
	static int main(int argc, char const * const *argv) noexcept(true);
};

} } }

#include "fix_to_links_main_impl.hpp"

#endif
