#ifndef ISIMUD_EXCHANGES_links_fix_to_mit_main_hpp
#define ISIMUD_EXCHANGES_links_fix_to_mit_main_hpp

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_to_general_program_options.hpp"
#include "fix_to_mit_program_options.hpp"
#include "fix_to_link_main.hpp"

#include <fstream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

template<class Link, class CxnT>
struct fix_to_mit_link : public fix_to_link {
	using base_t=fix_to_link;
	using link_t=Link;
	using connection_t=CxnT;

	static int main(int argc, char const * const *argv) noexcept(true);
};

} } }

#include "fix_to_mit_main_impl.hpp"

#endif
