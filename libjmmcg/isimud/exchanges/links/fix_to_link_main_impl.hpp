/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

template<class Link, class CxnT>
inline auto
fix_to_link::make_client_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
	using conn_pol_t= typename CxnT::conn_pol_t;
	using link_t= Link;

	return typename link_t::client_link_t::ctor_args{
		boost::asio::ip::address_v4::from_string(vm["client_address"].as<std::string>()),
		vm["client_port"].as<unsigned short>(),
		(conn_pol_t::max_attempts * conn_pol_t::min_timeout),
		link_t::socket_t::socket_priority::high,
		exchanges::common::thread_traits::client_to_exchange_thread.core,
		exchanges::common::thread_traits::client_to_exchange_thread.priority};
}

template<class Link, class Operation>
inline int
fix_to_link::main(int argc, char const* const* argv, boost::program_options::options_description&& all, Operation&& op) noexcept(true) {
	try {
		using namespace libjmmcg;
		using link_t= Link;

		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(auto const exit_code= application::check_basic_options(vm, all, " Built with FIX: " ISIMUD_FIX_EXCHANGE_VERSION ", MICs generated on: '" ISIMUD_ISO_10383_MIC_XML_GENERATED_DATE "', currency-codes created on: '" ISIMUD_ISO_4217_CURRENCY_CODES_XML_GENERATED_DATE "' and country-codes generated on: '" ISIMUD_ISO_3166_COUNTRY_CODES_XML_GENERATED_DATE "'", std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		} else {
			boost::program_options::notify(vm);
			common::thread_traits::api_threading_traits::set_backtrace_on_signal();
			libjmmcg::log::set_global_level(vm["log_level"].as<libjmmcg::log::level_severity_type>());
			{
				[[maybe_unused]] auto const locale_err= std::setlocale(LC_ALL, vm["locale"].as<std::string>().c_str());
				DEBUG_ASSERT(locale_err != nullptr);
			}
			auto const ctor_args= op(vm);
			const std::size_t num_timestamps= vm["num_timings"].as<std::size_t>();
			latency_timestamps timestamps(num_timestamps);
			std::atomic<std::size_t> number_of_reported_errors{};
			{
				typename link_t::exchg_links_t::report_error_fn_t exchg_report_error= [&number_of_reported_errors] NEVER_INLINE(std::string exchg_link_details, typename link_t::exchg_links_t::client_connection_t client_cxn_details, std::exception_ptr eptr) {
					auto const number_of_reported_errors_tmp= ++number_of_reported_errors;
					try {
						LIBJMMCG_LOG_TRIVIAL(error) << application::error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception information: " << eptr;
					} catch(...) {
						LIBJMMCG_LOG_TRIVIAL(error) << application::error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception: " << boost::current_exception_diagnostic_information();
					}
				};
				typename link_t::client_link_t::report_error_fn_t client_report_error= [&number_of_reported_errors] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
					auto const number_of_reported_errors_tmp= ++number_of_reported_errors;
					try {
						LIBJMMCG_LOG_TRIVIAL(error) << application::error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr;
					} catch(...) {
						LIBJMMCG_LOG_TRIVIAL(error) << application::error_message_prefix << "[" << number_of_reported_errors_tmp << "]: Failed whilst running the server. Server details: " << svr_details << ", exception: " << boost::current_exception_diagnostic_information();
					}
				};
				link_t link(
					std::get<0>(ctor_args),
					exchg_report_error,
					client_report_error,
					common::thread_traits::exchange_to_client_thread.core,
					common::thread_traits::exchange_to_client_thread.priority,
					timestamps,
					common::thread_traits::api_threading_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
					std::get<1>(ctor_args));
				std::optional<lock_all_proc_mem> locker;
				try {
					locker.emplace(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
				} catch(...) {
					LIBJMMCG_LOG_TRIVIAL(warning) << "Unable to lock the memory of the process. If page-faulting occurs, it might affect performance. Continuing regardless... Details: " << boost::current_exception_diagnostic_information();
				}
				LIBJMMCG_LOG_TRIVIAL(info) << "Current process ID: " << std::dec << boost::interprocess::ipcdetail::get_current_process_id();
				LIBJMMCG_LOG_TRIVIAL(info) << link;
				common::thread_traits::api_threading_traits::set_kernel_affinity(
					common::thread_traits::api_threading_traits::get_current_thread(),
					common::thread_traits::api_threading_traits::api_params_type::processor_mask_type(common::thread_traits::main_thread.core));
				common::thread_traits::api_threading_traits::set_kernel_priority(
					common::thread_traits::api_threading_traits::get_current_thread(),
					common::thread_traits::main_thread.priority);
				link.exit_requested().wait(false);
				common::thread_traits::api_threading_traits::set_kernel_priority(
					common::thread_traits::api_threading_traits::get_current_thread(),
					common::thread_traits::api_threading_traits::api_params_type::priority_type::normal);
				LIBJMMCG_LOG_TRIVIAL(info) << link;
			}
			timestamps.write_to_csv_file(std::clog, argv[0]);
			return number_of_reported_errors.load() ? exit_codes::codes::exit_could_not_forward_message : exit_codes::codes::exit_success;
		}
	} catch(std::exception const& ex) {
		LIBJMMCG_LOG_TRIVIAL(error) << "STL-derived exception. Details: " << boost::diagnostic_information(ex);
		return libjmmcg::exit_codes::codes::exit_stl_exception;
	} catch(...) {
		LIBJMMCG_LOG_TRIVIAL(error) << "Unknown exception. Details: " << boost::current_exception_diagnostic_information();
		return libjmmcg::exit_codes::codes::exit_unknown_exception;
	}
}

}}}
