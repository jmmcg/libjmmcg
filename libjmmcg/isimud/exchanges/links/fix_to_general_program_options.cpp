/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_to_general_program_options.hpp"

#include "../common/thread_traits.hpp"

#include "core/logging.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

boost::program_options::options_description
create_program_options() noexcept(false) {
	using namespace libjmmcg;

	std::ostringstream msg;
	msg << "A FIX-to-exchange translator that listens to an IPADDR:PORT combination for a FIX client and connects to the specified exchange IPADDR:PORTs. The executable name indicates the message translation performed & versions. Compiled to run on NUMA node: " << common::thread_traits::numa_index << ".\n\nAny failures to send a message from the connected exchange(s) to any connected client(s) are output to std::cerr (prefixed by '" << application::error_message_prefix << "[n]:' where 'n' is n-th error reported) and a return-code of 'exit_could_not_forward_message' is used. For further details regarding the properties of the translator see the documentation that came with the distribution.\n\n"
		 << application::wireshark_info << "\n\n" + application::make_help_message("consultant@isimud.ltd.uk");
	boost::program_options::options_description general(application::make_program_options(msg.str()));
	boost::program_options::options_description performance("Performance-monitoring options");
	performance.add_options()(
		"num_timings",
		boost::program_options::value<unsigned long>()->required()->default_value(0UL),
		"The number (positive-integer) of latency timings to reserve. This value must be greater than the total number of messages to be processed, otherwise the behaviour is undefined. Upon exit, these values will be written in CSV-format to a file named 'argv[0]+\"-%%%%%.PID.csv\"' (where each '%' is a random hexadecimal digit and PID is the current process id). This file shall be created in the current directory. If the value is zero, then the file shall not be created, no values will be stored, unlimited messages may be processed.");
	boost::program_options::options_description client("FIX-client options");
	client.add_options()(
		"client_address",
		boost::program_options::value<std::string>()->required()->default_value(boost::asio::ip::address_v4::loopback().to_string()),
		"IP address (in v4 format; thus the network interface to which it is bound) to which the translator should listen.")(
		"client_port",
		boost::program_options::value<unsigned short>()->required(),
		"An unused port to which the translator should listen.");
	boost::program_options::options_description locale("Locale options.");
	locale.add_options()("locale", boost::program_options::value<std::string>()->default_value(std::string(ISIMUD_LOCALE)), "Sets the locale for the server. This locale should have been installed by the 'isimud/scripts/postinst.in' when the program was installed.");
	boost::program_options::options_description logging("Logging options.");
	logging.add_options()("log_level", boost::program_options::value<boost::log::trivial::severity_level>()->default_value(boost::log::trivial::trace, std::string(boost::log::trivial::to_string(boost::log::trivial::trace)) + ", <" + libjmmcg::log::all_severities_to_string() + ">"), "Sets the level from which to log. Only log messages equal-to or higher will be logged. Default is to log everything.");
	boost::program_options::options_description all("All options.");
	all.add(general).add(client).add(performance).add(locale).add(logging);
	return all;
}

}}}
