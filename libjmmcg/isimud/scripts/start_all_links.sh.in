#!/bin/bash

# Copyright (c) 2017 by J.M.McGuiness, isimud@hussar.me.uk
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#

if [ $# -eq 0 ]
  then
    echo "Usage: " $0 " [OPTION]"
    echo "  Start ${PROJECT_NAME} with the supplied options.".
    echo "  Mandatory arguments to long options are mandatory for short options too."
    echo "    --config=[CONFIG_FILE] {mandatory] Where CONFIG_FILE is the directory-name or directory-path to the directory containing the configruation files.".
    exit 1
fi

# From: https://linux.die.net/man/8/ld-linux
export LD_BIND_NOW=yes

LOG_DIR=/var/log/${PROJECT_NAME}/${VERSION_NUMBER}

if [ ! -d "$LOG_DIR" ]; then
  # Just in case it got accidentally deleted after installation... See the 'postinst.in' script.
  mkdir -p $LOG_DIR
  CURRENT_LOG_DIR=/var/log/${PROJECT_NAME}/current
  rm "$CURRENT_LOG_DIR"
  ln -sf "$LOG_DIR" "$CURRENT_LOG_DIR"
  chown -R ${LIBJMMCG_LOG_DIR_USER}:${LIBJMMCG_LOG_DIR_GROUP} /var/log/${PROJECT_NAME}
  chmod -R g+rw /var/log/${PROJECT_NAME}
fi

LOG_FILE_FILEPATH_PORTION="$(echo $1 | cut -d"/" -f2- | tr / .)"
LOG_FILE_STEM=$LOG_DIR/nohup."$LOG_FILE_FILEPATH_PORTION"
LOG_FILE="$LOG_FILE_STEM"."$BASHPID".out

echo "Logging initially written to '$LOG_FILE'."

# This value should be the same as that used in isimud::exchanges::common::numa_index, otherwise the code will run on the wrong processor.
#  (One can determine which node the code has been compiled for by running the executable with the "--help" option.)
NUMA_INDEX=1

OPENONLOAD_INSTALLED=$(which onload)
OPENONLOAD_EXE=
if [ ! -z "$OPENONLOAD_INSTALLED" ];
then
	OPENONLOAD_EXE="$OPENONLOAD_INSTALLED"
	echo "Using OpenOnload to run..." >> "$LOG_FILE"
	echo "\tCommand: '$OPENONLOAD_EXE'" >> "$LOG_FILE"
fi
NUMACTL_INSTALLED=$(which numactl)
PROCESSORBIND_CMD=
if [ ! -z "$NUMACTL_INSTALLED" ];
then
	PROCESSORBIND_CMD="$NUMACTL_INSTALLED --preferred=$NUMA_INDEX --cpunodebind=$NUMA_INDEX"
	echo "Using numactl to run..." >> "$LOG_FILE"
	echo "\tCommand: '$PROCESSORBIND_CMD'" >> "$LOG_FILE"
fi

# Ensure that can we attempt to make a core file of unlimited size of the soft limit...
ulimit -S -c unlimited > /dev/null 2>&1
# (RLIMIT_MEMLOCK) Ensure that we can lock unlimited pages into memory.
ulimit -S -l unlimited > /dev/null 2>&1
# See the comment relating to "lock_mem_range" for more details.

start_link() {
	INSTALL_DIR=${CMAKE_INSTALL_PREFIX}/${CPACK_PACKAGE_FILE_NAME}/bin
	LINK_TO_START="$INSTALL_DIR"/$1
	shift
	echo "Starting '$LINK_TO_START'..." >> "$LOG_FILE"
	echo "Resultant command line: $PROCESSORBIND_CMD $OPENONLOAD_EXE $LINK_TO_START $@" >> "$LOG_FILE"
	nohup $PROCESSORBIND_CMD $OPENONLOAD_EXE "$LINK_TO_START" "$@" >> "$LOG_FILE" 2>&1 &
	SPAWNED_LINK_PID=$(ps -ef|grep "$LINK_TO_START $@"|grep -v "grep $LINK_TO_START $@"|cut --delim=' ' --fields=8-8)
	NEW_LOG_FILE="$LOG_FILE_STEM"."$SPAWNED_LINK_PID".out
	mv "$LOG_FILE" "$NEW_LOG_FILE"
	echo "Logging shall be written to '$NEW_LOG_FILE'."
 	return $?
}

# TODO Need to add command-line arguments...

start_link fix_to_bit_"${JMMCG_FIX_EXCHANGE_VERSION}"_"${JMMCG_BIT_EXCHANGE_VERSION}" "$@" &
start_link fix_to_jse_"${JMMCG_FIX_EXCHANGE_VERSION}"_"${JMMCG_JSE_EXCHANGE_VERSION}" "$@" &
start_link fix_to_lse_"${JMMCG_FIX_EXCHANGE_VERSION}"_"${JMMCG_LSE_EXCHANGE_VERSION}" "$@" &
start_link fix_to_oslo_"${JMMCG_FIX_EXCHANGE_VERSION}"_"${JMMCG_OSLO_EXCHANGE_VERSION}" "$@" &
start_link fix_to_trq_"${JMMCG_FIX_EXCHANGE_VERSION}"_"${JMMCG_TRQ_EXCHANGE_VERSION}" "$@" &

BATSBOE_ENABLED=${JMMCG_BUILD_BATSBOE}

if [ "$BATSBOE_ENABLED" = "ON" ]; then
	start_link fix_to_batsboe_eu_v1_${JMMCG_FIX_EXCHANGE_VERSION}_${JMMCG_BATSBOE_EU_v1_EXCHANGE_VERSION} "$@" &
	start_link fix_to_batsboe_eu_v2_${JMMCG_FIX_EXCHANGE_VERSION}_${JMMCG_BATSBOE_EU_v2_EXCHANGE_VERSION} "$@" &
	start_link fix_to_batsboe_us_v1_${JMMCG_FIX_EXCHANGE_VERSION}_${JMMCG_BATSBOE_US_v1_EXCHANGE_VERSION} "$@" &
	start_link fix_to_batsboe_us_v2_${JMMCG_FIX_EXCHANGE_VERSION}_${JMMCG_BATSBOE_US_v2_EXCHANGE_VERSION} "$@" &
fi

exit $?
