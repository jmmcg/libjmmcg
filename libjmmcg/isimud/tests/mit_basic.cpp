/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/MIT/BIT/messages.hpp"
#include "../exchanges/MIT/JSE/messages.hpp"
#include "../exchanges/MIT/LSE/messages.hpp"
#include "../exchanges/MIT/OSLO/messages.hpp"
#include "../exchanges/MIT/TRQ/messages.hpp"

using namespace libisimud;
const exchanges::MIT::common::SeqNum_t sequenceNumber(1);

typedef boost::mpl::list<
	exchanges::MIT::BIT::MsgTypes,
	exchanges::MIT::JSE::MsgTypes,
	exchanges::MIT::LSE::MsgTypes,
	exchanges::MIT::OSLO::MsgTypes,
	exchanges::MIT::TRQ::MsgTypes>
	msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_CASE_TEMPLATE(print_details, msg, msg_types) {
	std::stringstream ss;
	BOOST_CHECK_NO_THROW(msg::to_stream(ss));
	BOOST_CHECK(!ss.str().empty());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, msg, msg_types) {
	const typename msg::ClientOrderID_t origClientOrderID{"0000000000000000001"};
	const typename msg::ClientOrderID_t clientOrderID{"0000000000000000001"};
	const typename msg::SecurityID_t instID(133215);
	const typename msg::Price_t price(1);

	typename msg::LogonRequest_t logon(typename msg::LogonRequest_t::logon_args_t::UserName_t{"user"}, typename msg::LogonRequest_t::logon_args_t::Password_t{"fubar"}, typename msg::LogonRequest_t::logon_args_t::Password_t{"snafu"});
	BOOST_CHECK_EQUAL(logon.start_of_message, 2);
	BOOST_CHECK_EQUAL(logon.length(), sizeof(typename msg::LogonRequest_t));
	BOOST_CHECK_EQUAL(logon.type(), exchanges::MIT::common::MsgType::LogonRequest);
	BOOST_CHECK_EQUAL(logon.messageVersion, 1);
	BOOST_CHECK(logon.is_valid());
	typename msg::LogonReply_t logonReply;
	BOOST_CHECK_EQUAL(logonReply.start_of_message, 2);
	BOOST_CHECK_EQUAL(logonReply.length(), sizeof(typename msg::LogonReply_t));
	BOOST_CHECK_EQUAL(logonReply.type(), exchanges::MIT::common::MsgType::LogonReply);
	BOOST_CHECK(logonReply.is_valid());
	typename msg::LogoutRequest_t logout(exchanges::MIT::common::Reason_t{'U', 's', 'e', 'r', ' ', 'l', 'o', 'g', 'o', 'u', 't', ' ', 'r', 'e', 'c', 'e', 'i', 'v', 'e', 'd'});
	BOOST_CHECK_EQUAL(logout.start_of_message, 2);
	BOOST_CHECK_EQUAL(logout.length(), sizeof(typename msg::Logout_t));
	BOOST_CHECK_EQUAL(logout.type(), exchanges::MIT::common::MsgType::LogoutRequest);
	BOOST_CHECK(logout.is_valid());
	typename msg::ClientHeartbeat_t client_heartbeat;
	BOOST_CHECK_EQUAL(client_heartbeat.start_of_message, 2);
	BOOST_CHECK_EQUAL(client_heartbeat.length(), sizeof(typename msg::ClientHeartbeat_t));
	BOOST_CHECK_EQUAL(client_heartbeat.type(), exchanges::MIT::common::MsgType::Heartbeat);
	BOOST_CHECK(client_heartbeat.is_valid());
	typename msg::ServerHeartbeat_t server_heartbeat;
	BOOST_CHECK_EQUAL(server_heartbeat.start_of_message, 2);
	BOOST_CHECK_EQUAL(server_heartbeat.length(), sizeof(typename msg::ServerHeartbeat_t));
	BOOST_CHECK_EQUAL(server_heartbeat.type(), exchanges::MIT::common::MsgType::Heartbeat);
	BOOST_CHECK(server_heartbeat.is_valid());
	typename msg::MissedMessageRequest_t missedMessageRequest(exchanges::MIT::common::AppID::Partition1, 0);
	BOOST_CHECK_EQUAL(missedMessageRequest.start_of_message, 2);
	BOOST_CHECK_EQUAL(missedMessageRequest.length(), sizeof(typename msg::MissedMessageRequest_t));
	BOOST_CHECK_EQUAL(missedMessageRequest.type(), exchanges::MIT::common::MsgType::MissedMessageRequest);
	BOOST_CHECK(missedMessageRequest.is_valid());
	typename msg::MissedMessageRequestAck_t missedMessageRequestAck;
	BOOST_CHECK_EQUAL(missedMessageRequestAck.start_of_message, 2);
	BOOST_CHECK_EQUAL(missedMessageRequestAck.length(), sizeof(typename msg::MissedMessageRequestAck_t));
	BOOST_CHECK_EQUAL(missedMessageRequestAck.type(), exchanges::MIT::common::MsgType::MissedMessageRequestAck);
	BOOST_CHECK(missedMessageRequestAck.is_valid());
	typename msg::MissedMessageReport_t missedMessageReport;
	BOOST_CHECK_EQUAL(missedMessageReport.start_of_message, 2);
	BOOST_CHECK_EQUAL(missedMessageReport.length(), sizeof(typename msg::MissedMessageReport_t));
	BOOST_CHECK_EQUAL(missedMessageReport.type(), exchanges::MIT::common::MsgType::MissedMessageReport);
	BOOST_CHECK(missedMessageReport.is_valid());
	typename msg::Reject_t reject;
	BOOST_CHECK_EQUAL(reject.start_of_message, 2);
	BOOST_CHECK_EQUAL(reject.length(), sizeof(typename msg::Reject_t));
	BOOST_CHECK_EQUAL(reject.type(), exchanges::MIT::common::MsgType::Reject);
	BOOST_CHECK(reject.is_valid());
	typename msg::SystemStatus_t systemStatus;
	BOOST_CHECK_EQUAL(systemStatus.start_of_message, 2);
	BOOST_CHECK_EQUAL(systemStatus.length(), sizeof(typename msg::SystemStatus_t));
	BOOST_CHECK_EQUAL(systemStatus.type(), exchanges::MIT::common::MsgType::SystemStatus);
	BOOST_CHECK(systemStatus.is_valid());
	typename msg::NewOrder_t newOrder(sequenceNumber, clientOrderID, exchanges::MIT::common::OrderType::Market, exchanges::MIT::common::TIF::Day, exchanges::MIT::common::Side::Buy, instID, int32_t(), price);
	BOOST_CHECK_EQUAL(newOrder.start_of_message, 2);
	BOOST_CHECK_EQUAL(newOrder.length(), sizeof(typename msg::NewOrder_t));
	BOOST_CHECK_EQUAL(newOrder.type(), exchanges::MIT::common::MsgType::NewOrder);
	BOOST_CHECK(newOrder.is_valid());
	typename msg::OrderCancelReplaceRequest_t orderCancelReplaceRequest(clientOrderID, origClientOrderID, instID, int32_t(), price, exchanges::MIT::common::TIF::Day, exchanges::MIT::common::Side::Buy);
	BOOST_CHECK_EQUAL(orderCancelReplaceRequest.start_of_message, 2);
	BOOST_CHECK_EQUAL(orderCancelReplaceRequest.length(), sizeof(typename msg::OrderCancelReplaceRequest_t));
	BOOST_CHECK_EQUAL(orderCancelReplaceRequest.type(), exchanges::MIT::common::MsgType::OrderCancelReplaceRequest);
	BOOST_CHECK(orderCancelReplaceRequest.is_valid());
	typename msg::OrderCancelRequest_t orderCancelRequest(clientOrderID, clientOrderID, instID, exchanges::MIT::common::Side::Buy);
	BOOST_CHECK_EQUAL(orderCancelRequest.start_of_message, 2);
	BOOST_CHECK_EQUAL(orderCancelRequest.length(), sizeof(typename msg::OrderCancelRequest_t));
	BOOST_CHECK_EQUAL(orderCancelRequest.type(), exchanges::MIT::common::MsgType::OrderCancelRequest);
	BOOST_CHECK(orderCancelRequest.is_valid());
	typename msg::OrderMassCancelRequest_t orderMassCancelRequest(instID);
	BOOST_CHECK_EQUAL(orderMassCancelRequest.start_of_message, 2);
	BOOST_CHECK_EQUAL(orderMassCancelRequest.length(), sizeof(typename msg::OrderMassCancelRequest_t));
	BOOST_CHECK_EQUAL(orderMassCancelRequest.type(), exchanges::MIT::common::MsgType::OrderMassCancelRequest);
	BOOST_CHECK(orderMassCancelRequest.is_valid());
	typename msg::ExecutionReport_t executionReport;
	BOOST_CHECK_EQUAL(executionReport.start_of_message, 2);
	BOOST_CHECK_EQUAL(executionReport.length(), sizeof(typename msg::ExecutionReport_t));
	BOOST_CHECK_EQUAL(executionReport.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK(executionReport.is_valid());
	typename msg::OrderCancelReject_t orderCancelReject;
	BOOST_CHECK_EQUAL(orderCancelReject.start_of_message, 2);
	BOOST_CHECK_EQUAL(orderCancelReject.length(), sizeof(typename msg::OrderCancelReject_t));
	BOOST_CHECK_EQUAL(orderCancelReject.type(), exchanges::MIT::common::MsgType::OrderCancelReject);
	BOOST_CHECK(orderCancelReject.is_valid());
	typename msg::OrderMassCancelReport_t orderMassCancelReport;
	BOOST_CHECK_EQUAL(orderMassCancelReport.start_of_message, 2);
	BOOST_CHECK_EQUAL(orderMassCancelReport.length(), sizeof(typename msg::OrderMassCancelReport_t));
	BOOST_CHECK_EQUAL(orderMassCancelReport.type(), exchanges::MIT::common::MsgType::OrderMassCancelReport);
	BOOST_CHECK(orderMassCancelReport.is_valid());
	typename msg::BusinessReject_t businessReject;
	BOOST_CHECK_EQUAL(businessReject.start_of_message, 2);
	BOOST_CHECK_EQUAL(businessReject.length(), sizeof(typename msg::BusinessReject_t));
	BOOST_CHECK_EQUAL(businessReject.type(), exchanges::MIT::common::MsgType::BusinessMessageReject);
	BOOST_CHECK(businessReject.is_valid());
}

BOOST_AUTO_TEST_SUITE_END()
