/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "../exchanges/MIT/common/ref_data.hpp"

using namespace libisimud;

using ref_data_t= exchanges::MIT::common::ref_data;

BOOST_AUTO_TEST_SUITE(market_data)

BOOST_AUTO_TEST_CASE(ctor) {
	std::stringstream ss;
	const ref_data_t ref_data(ss);
	BOOST_CHECK(ref_data.empty());
	BOOST_CHECK(ref_data.empty());
	BOOST_CHECK(ref_data.empty());
}

BOOST_AUTO_TEST_CASE(one_entry) {
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss << ref_data_file;
	const ref_data_t ref_data(ss);
	BOOST_CHECK(!ref_data.empty());
	BOOST_CHECK_EQUAL(ref_data.size(), 1);
	const typename ref_data_t::security_id_key sik(exchanges::common::ISIN_t("GB00BH4HKS39"));
	BOOST_CHECK_NE(sik.isin_, exchanges::common::ISIN_t{});
	BOOST_CHECK_EQUAL(sik.isin_, sik.isin_);
	BOOST_CHECK_EQUAL(sik.isin_.hash(), 124731814251123);
	BOOST_CHECK_NO_THROW(ref_data.at(sik));
	BOOST_CHECK_EQUAL(ref_data.at(sik), 133215);
	BOOST_CHECK_NO_THROW(ref_data.at(133215));
	BOOST_CHECK_EQUAL(ref_data.at(133215), sik);
}

BOOST_AUTO_TEST_CASE(two_entries) {
	const std::string ref_data_file(
		"133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;\n"
		"2926;FTSE100;SET1;PT_T;TP_12;GB0000595859;;20000419;0;1;3000;32438040;1;;1;DE;ARM;ARM HLDGS.;0059585;3861344694;GBX;1;Y;0023;ARARM;ARM HOLDINGS PLC;0;;;7500;ORD 0.05P;;1;1;5;GB;;;FS10;4;;;;2;B;;;;;;");
	std::stringstream ss;
	ss << ref_data_file;
	const ref_data_t ref_data(ss);
	BOOST_CHECK(!ref_data.empty());
	BOOST_CHECK_EQUAL(ref_data.size(), 2);
	const typename ref_data_t::security_id_key sik1(exchanges::common::ISIN_t("GB00BH4HKS39"));
	BOOST_CHECK_EQUAL(sik1.isin_, sik1.isin_);
	BOOST_CHECK_NO_THROW(ref_data.at(sik1));
	BOOST_CHECK_EQUAL(ref_data.at(sik1), 133215);
	BOOST_CHECK_NO_THROW(ref_data.at(133215));
	BOOST_CHECK_EQUAL(ref_data.at(133215), sik1);
	const typename ref_data_t::security_id_key sik2(exchanges::common::ISIN_t("GB0000595859"));
	BOOST_CHECK_NO_THROW(ref_data.at(sik2));
	BOOST_CHECK_EQUAL(sik2.isin_, sik2.isin_);
	BOOST_CHECK_NE(sik1.isin_, sik2.isin_);
	BOOST_CHECK_NE(sik1.isin_.hash(), sik2.isin_.hash());
	BOOST_CHECK_LT(sik1.isin_, sik2.isin_);
	BOOST_CHECK_EQUAL(ref_data.at(sik2), 2926);
	BOOST_CHECK_NO_THROW(ref_data.at(2926));
	BOOST_CHECK_EQUAL(ref_data.at(2926), sik2);
}

BOOST_AUTO_TEST_SUITE_END()
