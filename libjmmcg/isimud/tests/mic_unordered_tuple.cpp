/******************************************************************************
** Copyright © 2021 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_MICS_HDR

#include "../exchanges/common/iso_10383_mic_codes.hpp"

#include "core/unordered_tuple.hpp"

using namespace libjmmcg;
using namespace libisimud;

struct base_t {
	[[nodiscard]] virtual exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept= 0;
	[[nodiscard]] virtual int process(int) const noexcept= 0;
};

struct bit_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA;

	const int i_;

	explicit bit_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i * i_;
	}
};

struct jse_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE;

	const unsigned i_;

	explicit jse_mock(unsigned i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i << i_;
	}
};

struct lse_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON;

	const int i_;

	explicit lse_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i % i_;
	}
};

struct oslo_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_BORS_XOSL;

	const int i_;

	explicit oslo_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i / i_;
	}
};

struct trq_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_TURQUOISE_TRQX;

	const int i_;

	explicit trq_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i + i_;
	}
};

struct batsboe_eu_v1_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_EUROPE___BXE_ORDER_BOOKS_BATE;

	const int i_;

	explicit batsboe_eu_v1_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i + i_;
	}
};

struct batsboe_us_v1_mock final : public base_t {
	static inline constexpr const auto MIC_code_= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS;

	const int i_;

	explicit batsboe_us_v1_mock(int i) noexcept
		: i_(i) {}

	[[nodiscard]] exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code() const noexcept override {
		return MIC_code_;
	}

	[[nodiscard]] int process(int i) const noexcept override {
		return i + i_;
	}
};

template<class T>
struct extract {
	static inline constexpr const auto value= T::MIC_code_;
};

BOOST_AUTO_TEST_SUITE(mic_hashing)

BOOST_AUTO_TEST_CASE(ctor) {
	BOOST_MPL_ASSERT_RELATION(sizeof(std::underlying_type_t<exchanges::common::mic_codes::ISO_10383_MIC_Codes>), <=, sizeof(unsigned));
	using collection_type= unordered_tuple<
		exchanges::common::mic_codes::ISO_10383_MIC_Codes,
		base_t,
		perfect_hash<
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_BORS_XOSL),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_TURQUOISE_TRQX),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_EUROPE___BXE_ORDER_BOOKS_BATE),
			static_cast<unsigned>(exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS)>,
		extract,
		bit_mock,
		jse_mock,
		lse_mock,
		oslo_mock,
		trq_mock,
		batsboe_eu_v1_mock,
		batsboe_us_v1_mock>;

	collection_type mocks{2, 3u, 4, 5, 6, 7, 8};
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_CONNECT_XOSC].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_CONNECT_XOSC);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_TURQUOISE_TRQX].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_TURQUOISE_TRQX);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_EUROPE___BXE_ORDER_BOOKS_BATE].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_EUROPE___BXE_ORDER_BOOKS_BATE);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS].MIC_code(), exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS);

	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA].process(10), 20);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_JOHANNESBURG_STOCK_EXCHANGE_XJSE].process(10), 80);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON].process(10), 2);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_OSLO_CONNECT_XOSC].process(10), 2);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_TURQUOISE_TRQX].process(10), 16);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_EUROPE___BXE_ORDER_BOOKS_BATE].process(10), 17);
	BOOST_CHECK_EQUAL(mocks[exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_CBOE_BZX_U_S__EQUITIES_EXCHANGE_BATS].process(10), 18);
}

BOOST_AUTO_TEST_SUITE_END()
