/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/BATSBOE/EU/v1/batsboe.hpp"
#include "../exchanges/BATSBOE/EU/v1/batsboe_sim.hpp"
#include "../exchanges/BATSBOE/EU/v2/batsboe.hpp"
#include "../exchanges/BATSBOE/EU/v2/batsboe_sim.hpp"

#include "../exchanges/FIX/v5.0sp2/fix_client.hpp"

#ifndef JMMCG_PERFORMANCE_TESTS
#	include "../exchanges/BATSBOE/US/v1/batsboe.hpp"
#	include "../exchanges/BATSBOE/US/v1/batsboe_sim.hpp"
#endif

#include "../exchanges/BATSBOE/US/v2/batsboe.hpp"
#include "../exchanges/BATSBOE/US/v2/batsboe_sim.hpp"

#include "../exchanges/conversions/batsboe_eu_to_fix_conversions.hpp"
#include "../exchanges/conversions/batsboe_us_to_fix_conversions.hpp"

#include "../exchanges/conversions/fix_to_batsboe_eu_conversions.hpp"
#include "../exchanges/conversions/fix_to_batsboe_us_conversions.hpp"

#include "core/ave_deviation_meter.hpp"
#include "core/jthread.hpp"
#include "core/latency_timestamps.hpp"
#include "core/stats_output.hpp"

using namespace libjmmcg;
using namespace libisimud;

using api_thread_traits= ppd::thread_params<ppd::platform_api>;
using timed_results_t= ave_deviation_meter<double>;

#ifdef JMMCG_PERFORMANCE_TESTS
using perf_latency_timestamps_type= latency_timestamps;
#else
using perf_latency_timestamps_type= no_latency_timestamps;
#endif

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short client_port= 12367u;
const unsigned short unused_primary_port= client_port + 1;
const unsigned short unused_secondary_port= unused_primary_port + 1;
const exchanges::BATSBOE::common::ClientOrderID_t clientOrderId1{"12345678901234test1"};
const auto sequenceNumber{std::make_shared<exchanges::BATSBOE::common::SeqNum_t>(1)};

using exchg_t_types= boost::mpl::list<
	std::pair<exchanges::BATSBOE::EU::v1::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::EU::v1::simulator_t>
#ifndef JMMCG_PERFORMANCE_TESTS
	,
	// TODO - not yet implemented	std::pair<exchanges::BATSBOE::EU::v2::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::EU::v2::simulator_t>,
	std::pair<exchanges::BATSBOE::US::v1::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::US::v1::simulator_t>
#endif
	// TODO - not yet implemented	std::pair<exchanges::BATSBOE::US::v2::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::US::v2::simulator_t>
	>;

template<class exchg_t>
struct only_sim {
	using link_t= typename exchg_t::first_type;
	using simulator_t= typename exchg_t::second_type;
	using conn_pol_t= typename link_t::exchg_links_t::exchg_link_t::conn_pol_t;
	using connection_t= exchanges::common::connection<
		typename simulator_t::msg_details_t,
		conn_pol_t>;

	const typename conn_pol_t::gateways_t gateways{
		std::make_pair(localhost, unused_primary_port)};
	const conn_pol_t conn_pol{
		gateways,
		exchanges::BATSBOE::common::logon_args_t{
			0,
			{"000"},
			simulator_t::proc_rules_t::username,
			simulator_t::proc_rules_t::password,
			false},
		exchanges::BATSBOE::common::logoff_args_t{
			0}};
	no_latency_timestamps ts{0};
	typename simulator_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	simulator_t svr{
		typename simulator_t::ctor_args{
			boost::asio::ip::address(),
			unused_primary_port,
			simulator_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		typename simulator_t::proc_rules_t(),
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}};
};

template<class exchg_t>
struct simulator_and_link : public only_sim<exchg_t> {
	using base_t= only_sim<exchg_t>;
	using link_t= typename base_t::link_t;
	using conn_pol_t= typename base_t::conn_pol_t;

	perf_latency_timestamps_type ts;
	typename link_t::exchg_links_t::report_error_fn_t exchg_report_error= [] NEVER_INLINE(std::string exchg_link_details, typename link_t::exchg_links_t::client_connection_t client_cxn_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", unknown exception.");
		}
	};
	typename link_t::client_link_t::report_error_fn_t client_report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	link_t link{
		typename link_t::client_link_t::ctor_args{
			boost::asio::ip::address(),
			client_port,
			(exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::max_attempts * exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::min_timeout),
			link_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::client_to_exchange_thread.core,
			exchanges::common::thread_traits::client_to_exchange_thread.priority},
		exchg_report_error,
		client_report_error,
		exchanges::common::thread_traits::exchange_to_client_thread.core,
		exchanges::common::thread_traits::exchange_to_client_thread.priority,
		ts,
		typename base_t::simulator_t::thread_t::thread_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
		std::tuple{
			this->conn_pol,
			link_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_to_client_thread.core,
			sequenceNumber}};

	explicit simulator_and_link(std::size_t num_ts)
		: ts(num_ts) {}
};

template<class exchg_t>
struct simulator_and_link_client_too : public simulator_and_link<exchg_t> {
	using base_t= simulator_and_link<exchg_t>;
	using base_t::base_t;

	perf_latency_timestamps_type client_ts;
	exchanges::FIX::v5_0sp2::fix_client client{
		localhost,
		client_port,
		exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
		exchanges::common::thread_traits::client_to_exchange_thread.core,
		client_ts};

	explicit simulator_and_link_client_too(std::size_t num_ts)
		: base_t(num_ts), client_ts(num_ts) {}
};

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("batsboe_exchanges_performance.csv"))

BOOST_AUTO_TEST_SUITE(simulator)

/**
	\test <a href="./examples/batsboe_exchanges_performance.svg">Graph</a> of performance results for the BATSBOE-based simulator-
			================================
	Results for 5000 repetitions.
	The timings include the full-round-trip: including time to create the BATSBOE order, send it, time in the simulator, time to receive, all of which includes the "wire-time".
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(reject, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 5000;
	const unsigned short loops_for_conv= 500;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 5.0;

	using fixture_t= only_sim<exchg_t>;

	const fixture_t f{};

	typename fixture_t::connection_t link(
		f.conn_pol,
		socket::socket_priority::high,
		exchanges::common::thread_traits::client_to_exchange_thread.core);

	auto send_and_receive= [&link]() {
		using msg_details_t= typename fixture_t::connection_t::msg_details_t;
		const typename msg_details_t::NewOrder_t msg(
			*sequenceNumber,
			clientOrderId1,
			msg_details_t::OrderType::Market,
			exchanges::BATSBOE::common::TIF::Day,
			msg_details_t::Side::Buy,
			exchg_t::second_type::proc_rules_t::invalidSymbol,
			exchg_t::second_type::proc_rules_t::invalidInstrumentID,
			exchg_t::second_type::proc_rules_t::quantity_limit - 1,
			exchg_t::second_type::proc_rules_t::price);
		link.send(msg);
		typename msg_details_t::OrderRejected_t reply;
		[[maybe_unused]] const auto ret= link.receive(reply);
	};

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&send_and_receive]() {
			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long i= 0; i < num_loops; ++i) {
				send_and_receive();
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / num_loops);
		}));
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tSimulator round-trip time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(exchange_gateways)

/**
	\test <a href="./examples/batsboe_exchanges_performance.svg">Graph</a> of performance results for the BATSBOE-based exchange-links, sending an order and receiving the reject one-at-a-time. The <a href="./examples/batsboe_exchanges_in_order_performance_latencies.svg">histogram</a> of the results, pinned but no numactl.
			================================
	Results for 5000 repetitions.
	The timings include the full-round-trip: including time to create the FIX order, convert it, send it, time in the simulator, time to receive, time to convert back to FIX, all of which includes the "wire-time".
	If numactl is to be used, the command "numactl --cpunodebind=1 --membind=1 ../../build/release/isimud/tests/test_batsboe_exchanges_performance" should be executed, Where NODE_INDEX should be the same value as that in thread_traits::numa_index.

	\see thread_traits::numa_index
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(order_rejected_at_a_time, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 5000;
	const unsigned short loops_for_conv= 500;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 5.0;

	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f(2 * 3 * num_loops * loops_for_conv);

	auto const& timed_results= f.client.in_order_tx_rx_stats(
		num_loops,
		loops_for_conv,
		perc_conv_estimate);
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tExchange round-trip in-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_CHECK(!timed_results.second);
	f.ts.write_to_named_csv_file(std::cout, "batsboe_exchanges_in_order_performance_latencies");
#endif
}

/**
	\test <a href="./examples/batsboe_exchanges_out_of_order_performance.svg">Graph</a> of performance results for the MIT-based exchange-links, sending the orders and receiving the rejects in parallel. The <a href="./examples/batsboe_exchanges_out_of_order_performance_latencies.svg">histogram</a> of the results, pinned but no numactl.
			================================
	Results for 20000 orders sent & rejects received.
	The timings include the full-round-trip: including time to create the FIX order, convert it, send it, time in the simulator, time to receive, time to convert back to FIX, all of which includes the "wire-time".
	If numactl is to be used, the command "numactl --cpunodebind=NODE_INDEX --membind=NODE_INDEX ../../build/release/isimud/tests/test_batsboe_exchanges_performance" should be executed, Where NODE_INDEX should be the same value as that in thread_traits::numa_index.

	\see thread_traits::numa_index
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(order_rejects_parallel, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 20000;
	const unsigned short loops_for_conv= 500;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 0.1;

	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f(2 * 3 * num_loops * loops_for_conv);

	auto send_and_receive= [&f]() {
		std::chrono::high_resolution_clock::time_point send_time{};
		std::chrono::high_resolution_clock::time_point receive_time{};

		auto send_batch_of_orders= [&f, &send_time]() {
			BOOST_CHECK(f.client.as_fix_msg_type.is_valid());
			// This farting around is just to throttle sending messages so that localhost is not flooded. (If it got flooded it would simply stop sending more messages and the test would break.)
			double accrued_delay_in_msec{};
			send_time= std::chrono::high_resolution_clock::now();
			for(unsigned long i= 0; i < num_loops; ++i) {
				f.client.send();
				accrued_delay_in_msec+= cpu_timer::pause_for_usec(400);
			}
			send_time= std::chrono::time_point_cast<std::chrono::high_resolution_clock::duration>(send_time + std::chrono::duration<double, std::micro>(accrued_delay_in_msec));
		};

		auto receive_batch_of_rejects= [&f, &receive_time]() {
			for(unsigned long i= 0; i < num_loops; ++i) {
				[[maybe_unused]] bool const ret= f.client.receive();
			}
			receive_time= std::chrono::high_resolution_clock::now();
			BOOST_CHECK(f.client.receive_fix_msg.is_valid());
		};

		{
			ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send(
				[send_batch_of_orders](std::atomic_flag& exit_requested) {
					send_batch_of_orders();
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
				});
			ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> receive(
				[receive_batch_of_rejects](std::atomic_flag& exit_requested) {
					receive_batch_of_rejects();
					exit_requested.test_and_set(std::memory_order_seq_cst);
					exit_requested.notify_all();
				});
		}
		return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(receive_time - send_time).count()) / num_loops);
	};

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		std::move(send_and_receive)));
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tExchange round-trip out-of-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
	f.ts.write_to_named_csv_file(std::cout, "batsboe_exchanges_out_of_order_performance_latencies");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
