/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/FIX/v5.0sp2/fix_client.hpp"
#include "../exchanges/MIT/BIT/bit.hpp"
#include "../exchanges/MIT/BIT/bit_sim.hpp"
#ifndef JMMCG_PERFORMANCE_TESTS
#	include "../exchanges/MIT/JSE/jse.hpp"
#	include "../exchanges/MIT/JSE/jse_sim.hpp"
#	include "../exchanges/MIT/LSE/lse.hpp"
#	include "../exchanges/MIT/LSE/lse_sim.hpp"
#	include "../exchanges/MIT/OSLO/oslo.hpp"
#	include "../exchanges/MIT/OSLO/oslo_sim.hpp"
#	include "../exchanges/MIT/TRQ/trq.hpp"
#	include "../exchanges/MIT/TRQ/trq_sim.hpp"
#endif
#include "../exchanges/conversions/fix_to_mit_conversions.hpp"
#include "../exchanges/conversions/mit_to_fix_conversions.hpp"

#include "core/ave_deviation_meter.hpp"
#include "core/jthread.hpp"
#include "core/latency_timestamps.hpp"
#include "core/stats_output.hpp"

#include <ratio>

using namespace libjmmcg;
using namespace libisimud;

using api_thread_traits= ppd::thread_params<ppd::platform_api>;
using timed_results_t= ave_deviation_meter<double>;

#ifdef JMMCG_PERFORMANCE_TESTS
using perf_latency_timestamps_type= latency_timestamps;
#else
using perf_latency_timestamps_type= no_latency_timestamps;
#endif

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short client_port= 12377u;
const boost::asio::ip::address primary_gw(boost::asio::ip::address_v4::loopback());
const unsigned short unused_primary_port= client_port + 1;
const boost::asio::ip::address secondary_gw(boost::asio::ip::address_v4::loopback());
const unsigned short unused_secondary_port= unused_primary_port + 1;
const exchanges::MIT::common::ClientOrderID_t clientOrderId1{"00000000000000test1"};
const exchanges::MIT::common::SeqNum_t sequenceNumber(1);

using exchg_t_types= boost::mpl::list<
	std::pair<exchanges::MIT::BIT::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::MIT::BIT::simulator_t>
#ifndef JMMCG_PERFORMANCE_TESTS
	,
	std::pair<exchanges::MIT::JSE::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::MIT::JSE::simulator_t>,
	std::pair<exchanges::MIT::LSE::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::MIT::LSE::simulator_t>,
	std::pair<exchanges::MIT::OSLO::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::MIT::OSLO::simulator_t>,
	std::pair<exchanges::MIT::TRQ::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::MIT::TRQ::simulator_t>
#endif
	>;

template<class exchg_t>
struct only_sim {
	using link_t= typename exchg_t::first_type;
	using simulator_t= typename exchg_t::second_type;
	using conn_pol_t= typename link_t::exchg_links_t::exchg_link_t::conn_pol_t;
	using connection_t= exchanges::common::connection<
		typename simulator_t::msg_details_t,
		conn_pol_t>;
	using ref_data_t= typename link_t::client_link_t::proc_rules_t::client_to_exchg_proc_rules_t::ref_data;

	static ref_data_t make_ref_data() noexcept(false) {
		const std::string ref_data_file(
			"133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;\n"
			"2926;FTSE100;SET1;PT_T;TP_12;GB0000595859;;20000419;0;1;3000;32438040;1;;1;DE;ARM;ARM HLDGS.;0059585;3861344694;GBX;1;Y;0023;ARARM;ARM HOLDINGS PLC;0;;;7500;ORD 0.05P;;1;1;5;GB;;;FS10;4;;;;2;B;;;;;;");
		std::stringstream ss;
		ss << ref_data_file;
		return ref_data_t(ss);
	}

	const typename conn_pol_t::gateways_t gateways{
		std::make_pair(primary_gw, unused_primary_port),
		std::make_pair(secondary_gw, unused_secondary_port)};
	const std::shared_ptr<ref_data_t> ref_data{std::make_shared<ref_data_t>(make_ref_data())};
	const conn_pol_t conn_pol{
		gateways,
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t{
			simulator_t::proc_rules_t::username,
			simulator_t::proc_rules_t::password,
			simulator_t::proc_rules_t::new_password},
		exchanges::MIT::common::logoff_args_t{
			simulator_t::proc_rules_t::logout_reason}};
	no_latency_timestamps ts{0};
	typename simulator_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	simulator_t svr{
		typename simulator_t::ctor_args{
			primary_gw,
			unused_primary_port,
			simulator_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		typename simulator_t::proc_rules_t(),
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}};
};

template<class exchg_t>
struct simulator_and_link : public only_sim<exchg_t> {
	using base_t= only_sim<exchg_t>;
	using link_t= typename base_t::link_t;
	using conn_pol_t= typename base_t::conn_pol_t;

	perf_latency_timestamps_type ts;
	typename link_t::exchg_links_t::report_error_fn_t exchg_report_error= [] NEVER_INLINE(std::string exchg_link_details, typename link_t::exchg_links_t::client_connection_t client_cxn_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", unknown exception.");
		}
	};
	typename link_t::client_link_t::report_error_fn_t client_report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	link_t link{
		typename link_t::client_link_t::ctor_args{
			localhost,
			client_port,
			(exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::max_attempts * exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::min_timeout),
			link_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::client_to_exchange_thread.core,
			exchanges::common::thread_traits::client_to_exchange_thread.priority,
		},
		exchg_report_error,
		client_report_error,
		exchanges::common::thread_traits::exchange_to_client_thread.core,
		exchanges::common::thread_traits::exchange_to_client_thread.priority,
		ts,
		typename base_t::simulator_t::thread_t::thread_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
		std::tuple{
			this->conn_pol,
			link_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::exchange_to_client_thread.core,
			this->ref_data}};

	explicit simulator_and_link(std::size_t num_ts)
		: ts(num_ts) {}
};

template<class exchg_t>
struct simulator_and_link_client_too : public simulator_and_link<exchg_t> {
	using base_t= simulator_and_link<exchg_t>;

	perf_latency_timestamps_type client_ts;
	exchanges::FIX::v5_0sp2::fix_client client{
		localhost,
		client_port,
		exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
		exchanges::common::thread_traits::client_to_exchange_thread.core,
		client_ts};

	explicit simulator_and_link_client_too(std::size_t num_ts)
		: base_t(num_ts), client_ts(num_ts) {}
};

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("mit_exchanges_performance.csv"))

BOOST_AUTO_TEST_SUITE(simulator)

/**
	\test <a href="./examples/mit_exchanges_performance.svg">Graph</a> of performance results for the MIT-based simulator-
			================================
	Results for 5000 repetitions.
	The timings include the full-round-trip: including time to create the MIT order, send it, time in the simulator, time to receive, all of which includes the "wire-time".
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(reject, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 10000;
	const unsigned short loops_for_conv= 1000;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 2.0;

	using fixture_t= only_sim<exchg_t>;

	const fixture_t f{};

	typename fixture_t::connection_t link(
		f.conn_pol,
		socket::socket_priority::high,
		exchanges::common::thread_traits::client_to_exchange_thread.core);

	auto send_and_receive= [&link]() {
		using msg_details_t= typename fixture_t::connection_t::msg_details_t;
		const typename msg_details_t::NewOrder_t msg(
			sequenceNumber,
			clientOrderId1,
			msg_details_t::OrderType::Market,
			exchanges::MIT::common::TIF::Day,
			msg_details_t::Side::Buy,
			exchg_t::second_type::proc_rules_t::invalidInstrumentID,
			exchg_t::second_type::proc_rules_t::quantity_limit - 1,
			exchg_t::second_type::proc_rules_t::price);
		link.send(msg);
		typename msg_details_t::BusinessReject_t reply;
		[[maybe_unused]] bool const ret= link.receive(reply);
	};

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&send_and_receive]() {
			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long i= 0; i < num_loops; ++i) {
				send_and_receive();
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / num_loops);
		}));
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tSimulator round-trip time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(exchange_gateways)

/**
	\test <a href="./examples/mit_exchanges_in_order_performance.svg">Graph</a> of performance results for the MIT-based exchange-links, sending an order and receiving the reject one-at-a-time. The histograms generated by using <a href="./examples/mit_exchanges_in_order_performance_latencies_GNU.svg">GNU</a> or <a href="./examples/mit_exchanges_in_order_performance_latencies_Clang.svg">Clang</a> to compile & link the code of the latest performance results, pinned and with numactl.
			================================
	Results for 20000 repetitions, repeated up to 500 times.
	The timings include the full-round-trip: including time to create the FIX order, convert it, send it, time in the simulator, time to receive, time to convert back to FIX, all of which includes the "wire-time".
	If numactl is to be used, the command "numactl --cpunodebind=NODE_INDEX --membind=NODE_INDEX ../../build/release/isimud/tests/test_mit_exchanges_performance" should be executed, Where NODE_INDEX should be the same value as that in thread_traits::numa_index.

	\see thread_traits::numa_index
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(order_rejected_at_a_time, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 20000;
	const unsigned short loops_for_conv= 2000;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 0.1;

	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f(2 * 3 * num_loops * loops_for_conv);

	auto const& timed_results= f.client.in_order_tx_rx_stats(
		num_loops,
		loops_for_conv,
		perc_conv_estimate);
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tExchange round-trip in-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
	f.ts.write_to_named_csv_file(std::cout, "mit_exchanges_in_order_performance_latencies");
#endif
}

/**
	\test <a href="./examples/mit_exchanges_out_of_order_performance.svg">Graph</a> of performance results for the MIT-based exchange-links, sending the orders and receiving the rejects in parallel. The histograms generated by using <a href="./examples/mit_exchanges_out_of_order_performance_latencies_GNU.svg">GNU</a> or <a href="./examples/mit_exchanges_out_of_order_performance_latencies_Clang.svg">Clang</a> to compile & link the code of the latest performance results, pinned and with numactl.
			================================
	Results for 20000 orders sent & rejects received, repeated up to 500 times.
	The timings include the full-round-trip: including time to create the FIX order, convert it, send it, time in the simulator, time to receive, time to convert back to FIX, all of which includes the "wire-time".
	If numactl is to be used, the command "numactl --cpunodebind=NODE_INDEX --membind=NODE_INDEX ../../build/release/isimud/tests/test_mit_exchanges_performance" should be executed, Where NODE_INDEX should be the same value as that in thread_traits::numa_index.

	\see thread_traits::numa_index
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(order_rejects_parallel, exchg_t, exchg_t_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 4000;	// TODO 5000 seems to lock-up.
	const unsigned short loops_for_conv= 2000;
#else
	const unsigned long num_loops= 1;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 0.1;

	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f(2 * 3 * num_loops * loops_for_conv);

	auto send_and_receive= [&f]() {
		std::chrono::high_resolution_clock::time_point send_time{};
		std::chrono::high_resolution_clock::time_point receive_time{};

		auto send_batch_of_orders= [&f, &send_time]() {
			f.client.as_fix_msg_type.is_valid();
			// This farting around is just to throttle sending messages so that localhost is not flooded. (If it got flooded it would simply stop sending more messages and the test would break.)
			double accrued_delay_in_msec{};
			send_time= std::chrono::high_resolution_clock::now();
			for(unsigned long i= 0; i < num_loops; ++i) {
				f.client.send();
				accrued_delay_in_msec+= cpu_timer::pause_for_usec(400);
			}
			send_time= std::chrono::time_point_cast<std::chrono::high_resolution_clock::duration>(send_time + std::chrono::duration<double, std::micro>(accrued_delay_in_msec));
		};

		auto receive_batch_of_rejects= [&f, &receive_time]() {
			for(unsigned long i= 0; i < num_loops; ++i) {
				[[maybe_unused]] bool const ret= f.client.receive();
			}
			receive_time= std::chrono::high_resolution_clock::now();
			f.client.receive_fix_msg.is_valid();
		};

		{
			ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send(
				[send_batch_of_orders]() {
					send_batch_of_orders();
				});
			ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> receive(
				[receive_batch_of_rejects]() {
					receive_batch_of_rejects();
				});
		}
		return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(receive_time - send_time).count()) / num_loops);
	};

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		std::move(send_and_receive)));
	std::cout << boost::core::demangle(typeid(typename exchg_t::first_type).name()) << "\n\tExchange round-trip out-of-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
	f.ts.write_to_named_csv_file(std::cout, "mit_exchanges_out_of_order_performance_latencies");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
