## Copyright (c) 2010 by J.M.McGuiness, isimud@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

PROJECT(tests CXX)

find_package(Boost 1.79 REQUIRED COMPONENTS date_time filesystem log unit_test_framework)
set_package_properties(Boost PROPERTIES
	DESCRIPTION "...one of the most highly regarded and expertly designed C++ library projects in the world."
	URL "https://www.boost.org/"
)
find_package(Gnuplot)
set_package_properties(Gnuplot PROPERTIES
	DESCRIPTION "Command-line driven interactive plotting program."
	URL "http://www.gnuplot.info/"
)

set(JMMCG_MAKE_HISTOGRAM_GNUPLOT ${CMAKE_CURRENT_SOURCE_DIR}/../../gnuplot/make_histogram.gnuplot CACHE PATH "The path to the gnuplot script for creating histograms.")

enable_testing()
include(CTest)

include_directories(
		${CMAKE_CURRENT_BINARY_DIR}/../exchanges
		${CMAKE_CURRENT_BINARY_DIR}/../exchanges/common
)

find_program(CTEST_MEMORYCHECK_COMMAND valgrind)
set(MEMORYCHECK_COMMAND_OPTIONS "--xml=yes --xml-file=valgrind.xml" CACHE STRING "Valgrind (memcheck) options, run with ctest -D ExperimentalMemCheck.")

set(TEST_PERFECT_HASH_MASK_FOR_MICS ${CMAKE_CURRENT_BINARY_DIR}/computed_minimum_perfect_hash_for_MICs.hpp)
add_custom_command(OUTPUT ${TEST_PERFECT_HASH_MASK_FOR_MICS}
	COMMAND $<TARGET_FILE:compute_mph_mask> ARGS --cpp_header=${TEST_PERFECT_HASH_MASK_FOR_MICS} --values 1297367361 1481265989 1481396046 1481593676 1414680920 1111577669 1111577683	# WARNING: these values are encoded in the source-file for the test too. Not changing then in synchrony is likely to cause compilation errors if they are incorrect! These are the values extracted from the MICs for BIT, JSE, LSE, OLSO, TRQ, BATSBOE_EU_v1 & BATSBOE_US_v1.
	DEPENDS compute_mph_mask
	COMMENT "Obtain the mask for the hash for the MICS: BIT, JSE, LSE, OLSO, TRQ, BATSBOE_EU_v1 & BATSBOE_US_v1: '${TEST_PERFECT_HASH_MASK_FOR_MICS}'."
)
add_custom_target(FindPerfectHashMaskForMICs
	DEPENDS ${TEST_PERFECT_HASH_MASK_FOR_MICS}
)

set(TEST_PERFECT_HASH_MASK_FOR_TEST_MSM ${CMAKE_CURRENT_BINARY_DIR}/computed_minimum_perfect_hash_for_Test_MSM.hpp)
add_custom_command(OUTPUT ${TEST_PERFECT_HASH_MASK_FOR_TEST_MSM}
	COMMAND $<TARGET_FILE:compute_mph_mask> ARGS --cpp_header=${TEST_PERFECT_HASH_MASK_FOR_TEST_MSM} --values 56 68	# WARNING: these values are encoded in the source-file for the test too. Not changing then in synchrony is likely to cause compilation errors if they are incorrect! These are the values extracted from the MICs for BIT, JSE, LSE, OLSO & TRQ.
	DEPENDS compute_mph_mask
	COMMENT "Obtain the mask for the hash for the messages for: BIT, JSE, LSE, OLSO & TRQ: '${TEST_PERFECT_HASH_MASK_FOR_TEST_MSM}'."
)
add_custom_target(FindPerfectHashMaskForTestMSM
	DEPENDS ${TEST_PERFECT_HASH_MASK_FOR_TEST_MSM}
)

ADD_EXECUTABLE(test_v_mic_unordered_tuple mic_unordered_tuple.cpp)
target_link_libraries(test_v_mic_unordered_tuple
	jmmcg
)
target_compile_definitions(test_v_mic_unordered_tuple
	PRIVATE
		LIBJMMCG_PERFECT_HASH_MASK_FOR_MICS_HDR="${TEST_PERFECT_HASH_MASK_FOR_MICS}"
)
add_dependencies(test_v_mic_unordered_tuple
	ConvertISOMICSpecToCXX
	FindPerfectHashMaskForMICs
)
add_test(
	NAME test_v_mic_unordered_tuple
	COMMAND test_v_mic_unordered_tuple
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_v_fix_basic fix_basic.cpp)
target_link_libraries(test_v_fix_basic
	fix_v5_0sp2
)
add_test(
	NAME test_v_fix_basic
	COMMAND test_v_fix_basic
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_fix_simulators fix_simulators.cpp)
target_link_libraries(test_fix_simulators
	fix_v5_0sp2
)
add_test(
	NAME test_fix_simulators
	COMMAND test_fix_simulators
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

if(${JMMCG_BUILD_BATSBOE})
	ADD_EXECUTABLE(test_v_batsboe_eu_basic batsboe_eu_basic.cpp)
	target_link_libraries(test_v_batsboe_eu_basic
		batsboe_eu_v1_44
		batsboe_eu_v2_0_46
		jmmcg
	)
	add_test(
		NAME test_v_batsboe_eu_basic
		COMMAND test_v_batsboe_eu_basic
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_batsboe_eu_simulators batsboe_eu_simulators.cpp)
	target_link_libraries(test_batsboe_eu_simulators
		batsboe_eu_v1_44
		batsboe_eu_v2_0_46
		jmmcg
	)
	add_test(
		NAME test_batsboe_eu_simulators
		COMMAND test_batsboe_eu_simulators
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_v_batsboe_us_basic batsboe_us_basic.cpp)
	target_link_libraries(test_v_batsboe_us_basic
		batsboe_us_v1_8_6
		batsboe_us_v2_3_6
		jmmcg
	)
	add_test(
		NAME test_v_batsboe_us_basic
		COMMAND test_v_batsboe_us_basic
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_batsboe_us_simulators batsboe_us_simulators.cpp)
	target_link_libraries(test_batsboe_us_simulators
		batsboe_us_v1_8_6
		batsboe_us_v2_3_6
		jmmcg
	)
	add_test(
		NAME test_batsboe_us_simulators
		COMMAND test_batsboe_us_simulators
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_v_fix_to_batsboe_eu fix_to_batsboe_eu.cpp)
	target_link_libraries(test_v_fix_to_batsboe_eu
		batsboe_eu_v1_44
		batsboe_eu_v2_0_46
		fix_v5_0sp2
	)
	add_test(
		NAME test_v_fix_to_batsboe_eu
		COMMAND test_v_fix_to_batsboe_eu
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_v_batsboe_eu_to_fix batsboe_eu_to_fix.cpp)
	target_link_libraries(test_v_batsboe_eu_to_fix
		batsboe_eu_v1_44
		batsboe_eu_v2_0_46
		fix_v5_0sp2
	)
	add_test(
		NAME test_v_batsboe_eu_to_fix
		COMMAND test_v_batsboe_eu_to_fix
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_v_fix_to_batsboe_us fix_to_batsboe_us.cpp)
	target_link_libraries(test_v_fix_to_batsboe_us
		batsboe_us_v1_8_6
		batsboe_us_v2_3_6
		fix_v5_0sp2
	)
	add_test(
		NAME test_v_fix_to_batsboe_us
		COMMAND test_v_fix_to_batsboe_us
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_v_batsboe_us_to_fix batsboe_us_to_fix.cpp)
	target_link_libraries(test_v_batsboe_us_to_fix
		batsboe_us_v1_8_6
		batsboe_us_v2_3_6
		fix_v5_0sp2
	)
	add_test(
		NAME test_v_batsboe_us_to_fix
		COMMAND test_v_batsboe_us_to_fix
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	ADD_EXECUTABLE(test_batsboe_exchanges batsboe_exchanges.cpp)
	target_compile_options(test_batsboe_exchanges
		PUBLIC
			$<$<CXX_COMPILER_ID:GNU>:-flarge-source-files>
	)
	target_link_libraries(test_batsboe_exchanges
		batsboe_eu_v1_44
		batsboe_us_v1_8_6
		batsboe_eu_v2_0_46
		batsboe_us_v2_3_6
		fix_v5_0sp2
	)
	add_test(
		NAME test_batsboe_exchanges
		COMMAND test_batsboe_exchanges
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set_tests_properties(test_batsboe_exchanges
		PROPERTIES TIMEOUT 600
	)
	ADD_EXECUTABLE(test_batsboe_exchanges_performance batsboe_exchanges_performance.cpp)
	set_target_properties(test_batsboe_exchanges_performance
		PROPERTIES
			INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
	)
	target_compile_options(test_batsboe_exchanges_performance
		PUBLIC
			$<$<CXX_COMPILER_ID:GNU>:-flarge-source-files>
	)
	if (JMMCG_PERFORMANCE_TESTS)
		target_link_libraries(test_batsboe_exchanges_performance
			batsboe_eu_v1_44_archive
			batsboe_us_v1_8_6_archive
			batsboe_eu_v2_0_46_archive
			batsboe_us_v2_3_6_archive
			fix_client_v5_0sp2_archive
			fix_v5_0sp2_archive
			Boost::date_time
		)
	else()
		target_link_libraries(test_batsboe_exchanges_performance
			batsboe_eu_v1_44
			batsboe_us_v1_8_6
			batsboe_eu_v2_0_46
			batsboe_us_v2_3_6
			fix_client_v5_0sp2
			fix_v5_0sp2
		)
	endif()
	add_test(
		NAME test_batsboe_exchanges_performance
		COMMAND test_batsboe_exchanges_performance
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set_tests_properties(test_batsboe_exchanges_performance
		PROPERTIES TIMEOUT 600
	)
	add_custom_command(OUTPUT batsboe_exchanges_performance.svg
		COMMAND ${GNUPLOT_EXECUTABLE} batsboe_exchanges_performance.gnuplot
		MAIN_DEPENDENCY batsboe_exchanges_performance.gnuplot
		DEPENDS batsboe_exchanges_performance.csv
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Generating performance graph in '${CMAKE_CURRENT_SOURCE_DIR}'."
	)
	add_custom_target(test_batsboe_exchanges_performance_graph ALL
		DEPENDS batsboe_exchanges_performance.svg
		SOURCES batsboe_exchanges_performance.csv batsboe_exchanges_performance.gnuplot
	)
	add_dependencies(test_batsboe_exchanges_performance_graph test_batsboe_exchanges_performance)
#	add_custom_command(OUTPUT batsboe_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
#		COMMAND ${GNUPLOT_EXECUTABLE} -e "datafile='batsboe_exchanges_in_order_performance_latencies.csv'" -e "graphfile='batsboe_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg'" -e "graph_title='Histogram version ${VERSION_NUMBER} of the partial in-order round-trip for the BATSBOE v1 EU exchange link using ${CMAKE_CXX_COMPILER_ID}.'" -e "catchall_bin=60" ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
#		VERBATIM
#		MAIN_DEPENDENCY ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
##		DEPENDS batsboe_exchanges_in_order_performance_latencies.csv
#		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
#		COMMENT "Generating in-order latency histogram in '${CMAKE_CURRENT_SOURCE_DIR}'."
#	)
#	add_custom_target(test_batsboe_exchanges_in_order_performance_histogram ALL
#		DEPENDS batsboe_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
#		SOURCES
##			batsboe_exchanges_in_order_performance_latencies.csv
#			${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
#	)
#	add_dependencies(test_batsboe_exchanges_in_order_performance_histogram test_batsboe_exchanges_performance)
endif()

ADD_EXECUTABLE(test_v_mit_reference_data mit_reference_data.cpp)
target_link_libraries(test_v_mit_reference_data
	jmmcg
)
add_test(
	NAME test_v_mit_reference_data
	COMMAND test_v_mit_reference_data
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_v_mit_basic mit_basic.cpp)
target_link_libraries(test_v_mit_basic
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
)
add_test(
	NAME test_v_mit_basic
	COMMAND test_v_mit_basic
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

set(TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS ${CMAKE_CURRENT_BINARY_DIR}/computed_minimum_perfect_hash_for_MIT_MSMs.hpp)
add_custom_command(OUTPUT ${TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS}
	COMMAND $<TARGET_FILE:compute_mph_mask> ARGS --cpp_header=${TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS} --values 68 70 71 65534	# WARNING: these values are encoded in the source-file for the test too. Not changing then in synchrony is likely to cause compilation errors if they are incorrect!
	DEPENDS compute_mph_mask
	COMMENT "Obtain the mask for the hash for MSMs using the MIT message-protocol: '${TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS}'."
)
add_custom_target(FindPerfectHashMaskForTestMITMSMs
	DEPENDS ${TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS}
)

ADD_EXECUTABLE(test_v_mit_msm mit_msm.cpp)
target_link_libraries(test_v_mit_msm
	Threads::Threads
)
target_compile_definitions(test_v_mit_msm
	PRIVATE
		LIBJMMCG_PERFECT_HASH_MASK_FOR_MIT_MSMS_HDR="${TEST_PERFECT_HASH_MASK_FOR_MIT_MSMS}"
)
add_dependencies(test_v_mit_msm
	numa_traits
	FindPerfectHashMaskForTestMITMSMs
)
target_link_libraries(test_v_mit_msm
	mit_bit_v6_4
#	mit_jse_v3_01
#	mit_lse_v11_6
#	mit_oslo_v4_1
#	mit_trq_v3_2
	fix_v5_0sp2
)
add_test(
	NAME test_v_mit_msm
	COMMAND test_v_mit_msm
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_v_fix_to_mit fix_to_mit.cpp)
target_link_libraries(test_v_fix_to_mit
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
	fix_v5_0sp2
)
add_test(
	NAME test_v_fix_to_mit
	COMMAND test_v_fix_to_mit
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_v_mit_to_fix mit_to_fix.cpp)
target_link_libraries(test_v_mit_to_fix
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
	fix_v5_0sp2
)
target_compile_definitions(test_v_mit_to_fix
	PRIVATE
		LIBJMMCG_PERFECT_HASH_MASK_FOR_TEST_MSM_HDR="${TEST_PERFECT_HASH_MASK_FOR_TEST_MSM}"
)
add_dependencies(test_v_mit_to_fix
	FindPerfectHashMaskForTestMSM
)
add_test(
	NAME test_v_mit_to_fix
	COMMAND test_v_mit_to_fix
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_mit_simulators mit_simulators.cpp)
target_link_libraries(test_mit_simulators
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
)
add_test(
	NAME test_mit_simulators
	COMMAND test_mit_simulators
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

ADD_EXECUTABLE(test_mit_exchanges mit_exchanges.cpp)
target_link_libraries(test_mit_exchanges
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
	fix_v5_0sp2
)
add_test(
	NAME test_mit_exchanges
	COMMAND test_mit_exchanges
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
set_tests_properties(test_mit_exchanges
	PROPERTIES TIMEOUT 600
)

ADD_EXECUTABLE(test_mit_exchanges_performance mit_exchanges_performance.cpp)
set_target_properties(test_mit_exchanges_performance
	PROPERTIES
		INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
)
target_compile_options(test_mit_exchanges_performance
	PUBLIC
		$<$<CXX_COMPILER_ID:GNU>:-flarge-source-files>
)
target_link_libraries(test_mit_exchanges_performance
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
	fix_client_v5_0sp2
	fix_v5_0sp2
)
add_test(
	NAME test_mit_exchanges_performance
	COMMAND test_mit_exchanges_performance
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
set_tests_properties(test_mit_exchanges_performance
	PROPERTIES TIMEOUT 600
)
add_custom_command(OUTPUT mit_exchanges_performance.svg
	COMMAND ${GNUPLOT_EXECUTABLE} mit_exchanges_performance.gnuplot
	MAIN_DEPENDENCY mit_exchanges_performance.gnuplot
	DEPENDS mit_exchanges_performance.csv
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	COMMENT "Generating performance graph in '${CMAKE_CURRENT_SOURCE_DIR}'."
)
add_custom_target(test_mit_exchanges_performance_graph ALL
	DEPENDS mit_exchanges_performance.svg
	SOURCES mit_exchanges_performance.csv mit_exchanges_performance.gnuplot
)
# TODO add_dependencies(test_mit_exchanges_performance_graph test_mit_exchanges_performance)
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/mit_exchanges_in_order_performance_latencies.csv)
	add_custom_command(OUTPUT mit_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
		COMMAND ${GNUPLOT_EXECUTABLE} -e "datafile='mit_exchanges_in_order_performance_latencies.csv'" -e "graphfile='mit_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg'" -e "graph_title='Histogram version ${VERSION_NUMBER} of the partial in-order round-trip for the MIT/BIT exchange link using ${CMAKE_CXX_COMPILER_ID}.'" -e "catchall_bin=25" ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
		VERBATIM
		MAIN_DEPENDENCY ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
#		DEPENDS mit_exchanges_in_order_performance_latencies.csv
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Generating in-order latency histogram in '${CMAKE_CURRENT_SOURCE_DIR}'."
	)
	add_custom_target(test_mit_exchanges_in_order_performance_histogram ALL
		DEPENDS mit_exchanges_in_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
		SOURCES
#			mit_exchanges_in_order_performance_latencies.csv
			${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
	)
#	add_dependencies(test_mit_exchanges_in_order_performance_histogram test_mit_exchanges_performance)
endif()
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/mit_exchanges_out_of_order_performance_latencies.csv)
	add_custom_command(OUTPUT mit_exchanges_out_of_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
		COMMAND ${GNUPLOT_EXECUTABLE} -e "datafile='mit_exchanges_out_of_order_performance_latencies.csv'" -e "graphfile='mit_exchanges_out_of_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg'" -e "graph_title='Histogram version ${VERSION_NUMBER} of the partial out-of-order round-trip for the MIT/BIT exchange link using ${CMAKE_CXX_COMPILER_ID}.'" -e "catchall_bin=40" ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
		VERBATIM
		MAIN_DEPENDENCY ${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
#		DEPENDS mit_exchanges_out_of_order_performance_latencies.csv
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Generating out-of-order latency histogram in '${CMAKE_CURRENT_SOURCE_DIR}'."
	)
	add_custom_target(test_mit_exchanges_out_of_order_performance_histogram ALL
		DEPENDS mit_exchanges_out_of_order_performance_latencies_${CMAKE_CXX_COMPILER_ID}.svg
		SOURCES
#			mit_exchanges_out_of_order_performance_latencies.csv
			${JMMCG_MAKE_HISTOGRAM_GNUPLOT}
	)
#	add_dependencies(test_mit_exchanges_out_of_order_performance_histogram test_mit_exchanges_performance)
endif()

set(TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS ${CMAKE_CURRENT_BINARY_DIR}/test_computed_minimum_perfect_hash_for_MICs.hpp)
add_custom_command(OUTPUT ${TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS}
	COMMAND $<TARGET_FILE:compute_mph_mask> ARGS --cpp_header=${TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS} --values 1297367361 1481396046	# WARNING: these values are encoded in the source-file for the test too. Not changing then in synchrony is likely to cause compilation errors if they are incorrect! These are the values extracted from the MICs for BIT & LSE.
	DEPENDS compute_mph_mask
	COMMENT "Obtain the mask for the hash for the MICS: BIT & LSE: '${TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS}'."
)
add_custom_target(FindTestLinksPerfectHashMaskForMICs
	DEPENDS ${TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS}
)

ADD_EXECUTABLE(test_multi_mit_exchanges multi_mit_exchanges.cpp)
target_link_libraries(test_multi_mit_exchanges
	mit_bit_v6_4
	mit_jse_v3_01
	mit_lse_v11_6
	mit_oslo_v4_1
	mit_trq_v3_2
	fix_v5_0sp2
)
target_compile_definitions(test_multi_mit_exchanges
	PRIVATE
		LIBJMMCG_TEST_PERFECT_HASH_MASK_FOR_MICS_HDR="${TEST_LINKS_PERFECT_HASH_MASK_FOR_MICS}"
)
add_dependencies(test_multi_mit_exchanges
	FindTestLinksPerfectHashMaskForMICs
)
add_test(
	NAME test_multi_mit_exchanges
	COMMAND test_multi_mit_exchanges
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
set_tests_properties(test_multi_mit_exchanges
	PROPERTIES TIMEOUT 600
)

if(${JMMCG_BUILD_BATSBOE})
	set(BATSBOE_LIBS
		batsboe_eu_v1_44
		batsboe_eu_v2_0_46
		batsboe_us_v1_8_6
		batsboe_us_v2_3_6
	)
else()
	set(BATSBOE_LIBS)
endif()

ADD_EXECUTABLE(test_v_order_book order_book.cpp)
target_link_libraries(test_v_order_book
	mit_bit_v6_4
# TODO Not bothering to get the rest of these working until I need it..,
# 	mit_jse_v3_01
#	mit_lse_v11_6
#	mit_oslo_v4_1
#	mit_trq_v3_2
	fix_v5_0sp2
#	${BATSBOE_LIBS}
)
add_test(
	NAME test_v_order_book
	COMMAND test_v_order_book
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

install(
	DIRECTORY .
	DESTINATION share/doc/libjmmcg/${VERSION_NUMBER}/html/${PROJECT_NAME}
	COMPONENT documentation
	DIRECTORY_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
	FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
	FILES_MATCHING PATTERN "*.svg"
)
