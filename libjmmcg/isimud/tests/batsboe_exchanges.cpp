/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/BATSBOE/EU/v1/batsboe.hpp"
#include "../exchanges/BATSBOE/EU/v1/batsboe_sim.hpp"
#include "../exchanges/BATSBOE/EU/v2/batsboe.hpp"
#include "../exchanges/BATSBOE/EU/v2/batsboe_sim.hpp"
#include "../exchanges/BATSBOE/US/v1/batsboe.hpp"
#include "../exchanges/BATSBOE/US/v1/batsboe_sim.hpp"
#include "../exchanges/BATSBOE/US/v2/batsboe.hpp"
#include "../exchanges/BATSBOE/US/v2/batsboe_sim.hpp"
#include "../exchanges/FIX/v5.0sp2/fix.hpp"
#include "../exchanges/conversions/batsboe_eu_to_fix_conversions.hpp"
#include "../exchanges/conversions/batsboe_us_to_fix_conversions.hpp"
#include "../exchanges/conversions/fix_to_batsboe_eu_conversions.hpp"
#include "../exchanges/conversions/fix_to_batsboe_us_conversions.hpp"

using namespace libjmmcg;
using namespace libisimud;

using api_thread_traits= ppd::thread_params<ppd::platform_api>;

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short client_port= 12377u;
const boost::asio::ip::address primary_gw(boost::asio::ip::address_v4::loopback());
const unsigned short unused_primary_port= client_port + 1;
const auto sequenceNumber{std::make_shared<exchanges::BATSBOE::common::SeqNum_t>(1)};

typedef boost::mpl::list<
	std::pair<exchanges::BATSBOE::EU::v1::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::EU::v1::simulator_t>,
	// TODO	std::pair<exchanges::BATSBOE::EU::v2::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::EU::v2::simulator_t>,
	std::pair<exchanges::BATSBOE::US::v1::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::US::v1::simulator_t>	 //,
	// TODO	std::pair<exchanges::BATSBOE::US::v2::link_t<exchanges::FIX::v5_0sp2::MsgTypes>, exchanges::BATSBOE::US::v2::simulator_t>
	>
	exchg_t_types;

template<class exchg_t>
struct only_sim {
	using link_t= typename exchg_t::first_type;
	using simulator_t= typename exchg_t::second_type;
	using conn_pol_t= typename link_t::exchg_links_t::exchg_link_t::conn_pol_t;
	using connection_t= exchanges::common::connection<
		typename simulator_t::msg_details_t,
		conn_pol_t>;

	const typename conn_pol_t::gateways_t gateways{
		std::make_pair(primary_gw, unused_primary_port)};
	const conn_pol_t conn_pol{
		gateways,
		exchanges::BATSBOE::common::logon_args_t{
			0,
			{"000"},
			simulator_t::proc_rules_t::username,
			simulator_t::proc_rules_t::password,
			false},
		exchanges::BATSBOE::common::logoff_args_t{
			*sequenceNumber}};
	no_latency_timestamps ts{0};
	typename simulator_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	simulator_t svr{
		typename simulator_t::ctor_args{
			primary_gw,
			unused_primary_port,
			simulator_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		typename simulator_t::proc_rules_t(),
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}};
};

template<class exchg_t>
struct simulator_and_link : public only_sim<exchg_t> {
	using base_t= only_sim<exchg_t>;
	using link_t= typename base_t::link_t;
	using conn_pol_t= typename base_t::conn_pol_t;

	no_latency_timestamps ts{0};
	typename link_t::exchg_links_t::report_error_fn_t exchg_report_error= [](std::string exchg_link_details, typename link_t::exchg_links_t::client_connection_t client_cxn_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", unknown exception.");
		}
	};
	typename link_t::client_link_t::report_error_fn_t client_report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	link_t link{
		typename link_t::client_link_t::ctor_args{
			localhost,
			client_port,
			(exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::max_attempts * exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::min_timeout),
			link_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::client_to_exchange_thread.core,
			exchanges::common::thread_traits::client_to_exchange_thread.priority},
		exchg_report_error,
		client_report_error,
		exchanges::common::thread_traits::exchange_to_client_thread.core,
		exchanges::common::thread_traits::exchange_to_client_thread.priority,
		ts,
		typename base_t::simulator_t::thread_t::thread_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
		std::tuple{
			this->conn_pol,
			link_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_to_client_thread.core,
			sequenceNumber}};
};

template<class exchg_t>
struct simulator_and_link_client_too : public simulator_and_link<exchg_t> {
	exchanges::FIX::v5_0sp2::connection_t client{
		exchanges::FIX::v5_0sp2::connection_t::conn_pol_t(
			typename exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::gateways_t(
				std::make_pair(localhost, client_port))),
		exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
		exchanges::common::thread_traits::client_to_exchange_thread.core};
};

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(client_initiated)

/**
	\test Verify Logon - no clients.
			==========================
	Verify that the link can log on to the exchange, with no clients.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(logon_no_clients, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link<exchg_t>;

	const fixture_t f;

	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Verify that gateway can re-Logon.
			=================================
	Verify that the gateway can re-log on to the exchange, without any clients.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(re_logon_no_clients, exchg_t, exchg_t_types) {
	using fixture_t= only_sim<exchg_t>;

	const fixture_t f;
	no_latency_timestamps ts{0};
	typename fixture_t::link_t::exchg_links_t::report_error_fn_t exchg_report_error= [](std::string exchg_link_details, typename fixture_t::link_t::exchg_links_t::client_connection_t client_cxn_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed to send a message to the client. Exchange details: " << exchg_link_details << ", client details: " << (client_cxn_details.get() ? client_cxn_details->to_string() : std::string{"No connected client."}) << ", unknown exception.");
		}
	};
	typename fixture_t::link_t::client_link_t::report_error_fn_t client_report_error= [](std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};

	{
		const typename fixture_t::link_t link{
			typename fixture_t::link_t::client_link_t::ctor_args{
				localhost,
				client_port,
				(exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::max_attempts * exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::min_timeout),
				fixture_t::link_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::client_to_exchange_thread.core,
				exchanges::common::thread_traits::client_to_exchange_thread.priority},
			exchg_report_error,
			client_report_error,
			exchanges::common::thread_traits::exchange_to_client_thread.core,
			exchanges::common::thread_traits::exchange_to_client_thread.priority,
			ts,
			typename fixture_t::simulator_t::thread_t::thread_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
			std::tuple{
				f.conn_pol,
				fixture_t::link_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_to_client_thread.core,
				sequenceNumber}};
		BOOST_CHECK(link.are_all_logged_on());
	}
	{
		const typename fixture_t::link_t link{
			typename fixture_t::link_t::client_link_t::ctor_args{
				localhost,
				client_port,
				(exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::max_attempts * exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::min_timeout),
				fixture_t::link_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::client_to_exchange_thread.core,
				exchanges::common::thread_traits::client_to_exchange_thread.priority},
			exchg_report_error,
			client_report_error,
			exchanges::common::thread_traits::exchange_to_client_thread.core,
			exchanges::common::thread_traits::exchange_to_client_thread.priority,
			ts,
			typename fixture_t::simulator_t::thread_t::thread_traits::thread_name_t{"link" LIBJMMCG_ENQUOTE(__LINE__)},
			std::tuple{
				f.conn_pol,
				fixture_t::link_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_to_client_thread.core,
				sequenceNumber}};
		BOOST_CHECK(link.are_all_logged_on());
	}
}

/**
	\test Verify Logon - with a client.
			=============================
	Verify that the link can log on to the exchange and remains logged on, with a client connected.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(logon_with_a_client, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link<exchg_t>;

	const fixture_t f;

	BOOST_CHECK(f.link.are_all_logged_on());
	{
		const exchanges::FIX::v5_0sp2::connection_t client(
			exchanges::FIX::v5_0sp2::connection_t::conn_pol_t(
				typename exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::gateways_t(
					std::make_pair(localhost, client_port))),
			exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core);
		BOOST_CHECK(f.link.are_all_logged_on());
	}
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Verify that clients can re-connect to logged-on gateway.
			========================================================
	Verify that the gateway can re-log on to the exchange, with a connected client.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(re_connect_client_with_logon, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link<exchg_t>;

	const fixture_t f;

	BOOST_CHECK(f.link.are_all_logged_on());
	{
		const exchanges::FIX::v5_0sp2::connection_t client(
			exchanges::FIX::v5_0sp2::connection_t::conn_pol_t(
				typename exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::gateways_t(
					std::make_pair(localhost, client_port))),
			exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core);
		BOOST_CHECK(f.link.are_all_logged_on());
	}
	BOOST_CHECK(f.link.are_all_logged_on());
	{
		const exchanges::FIX::v5_0sp2::connection_t client(
			exchanges::FIX::v5_0sp2::connection_t::conn_pol_t(
				typename exchanges::FIX::v5_0sp2::connection_t::conn_pol_t::gateways_t(
					std::make_pair(localhost, client_port))),
			exchanges::FIX::v5_0sp2::connection_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core);
		BOOST_CHECK(f.link.are_all_logged_on());
	}
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Verify Heartbeats - no clients.
			===============================
	Verify that Heartbeats occur, without any clients.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(heartbeats_no_clients, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link<exchg_t>;

	const fixture_t f;

	std::this_thread::sleep_for(std::chrono::seconds(fixture_t::simulator_t::svr_mgr_t::heartbeats_t::heartbeat_interval * (fixture_t::simulator_t::svr_mgr_t::heartbeats_t::max_missed_heartbeats + 1)));
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Verify Heartbeats - with client.
			================================
	Verify that Heartbeats occur, with a connected client.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(heartbeats_with_client, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link<exchg_t>;

	const fixture_t f;

	std::this_thread::sleep_for(std::chrono::seconds(fixture_t::simulator_t::svr_mgr_t::heartbeats_t::heartbeat_interval * (fixture_t::simulator_t::svr_mgr_t::heartbeats_t::max_missed_heartbeats + 1)));
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Response to an invalid NewOrder.
			================================
	Verify that the response to an invalid NewOrder is a BusinessReject.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(reject, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	BOOST_CHECK(f.link.are_all_logged_on());

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=153\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t::static_type);
	BOOST_REQUIRE_NO_THROW(f.client.send(fix_msg));
	BOOST_CHECK(f.link.are_all_logged_on());
	exchanges::FIX::v5_0sp2::MsgTypes::OrderRejected_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const ref_seq_num= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::RefSeqNum>();
	BOOST_CHECK_EQUAL(ref_seq_num, "2");
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Response to an invalid OrderCancelRequest.
			==========================================
	Verify that the response to an invalid OrderCancelRequest is a CancelRejected.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_reject, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	BOOST_CHECK(f.link.are_all_logged_on());

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=162\00135=F\00111=10\00141=10\00148=GB00BH4HKS39\00122=4\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=1\00144=43.000000\00159=0\0011133=G\001100=MTAA\00110=036\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t::static_type);
	BOOST_REQUIRE_NO_THROW(f.client.send(fix_msg));
	BOOST_CHECK(f.link.are_all_logged_on());
	exchanges::FIX::v5_0sp2::MsgTypes::BusinessMessageReject_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const client_order_id= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "2");
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Response to a valid OrderCancelRequest.
			=======================================
	Verify that the response to a valid OrderCancelRequest is a cancelled order ExecutionReport.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_accept, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer order_fix_buffer= {
		"8=FIX.5.0\0019=167\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB00BH4HKS39\00122=4\00154=1\00138=99\00140=2\00144=43.000000\00159=0\0011133=G\001100=MTAA\00110=117\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& order_fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*order_fix_buffer.begin());
	BOOST_CHECK_EQUAL(order_fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t::static_type);
	BOOST_CHECK(order_fix_msg.is_valid());
	BOOST_REQUIRE_NO_THROW(f.client.send(order_fix_msg));
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer cancel_fix_buffer= {
		"8=FIX.5.0\0019=157\00135=F\00111=10\00141=10\00148=GB00BH4HKS39\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=2\00144=43.000000\00159=0\0011133=G\001100=MTAA\00110=083\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const& cancel_fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const&>(*cancel_fix_buffer.begin());
	BOOST_CHECK(cancel_fix_msg.is_valid());
	BOOST_REQUIRE_NO_THROW(f.client.send(cancel_fix_msg));
	exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const client_order_id= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "3");
	auto const price= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(price, "43.000000");
	auto const side= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const orderQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "99");
	auto const leavesQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "99");
	BOOST_CHECK(f.link.are_all_logged_on());
}

/**
	\test Response to an invalid OrderCancelReplaceRequest.
			=================================================
	Verify that the response to an invalid OrderCancelReplaceRequest is a cancelled order CancelRejected.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(modify_reject, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=157\00135=G\00111=10\00141=10\00148=GB00BH4HKS39\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=1\00144=43.000000\00159=0\0011133=G\001100=MTAA\00110=102\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const&>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t::static_type);
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_REQUIRE_NO_THROW(f.client.send(fix_msg));
	exchanges::FIX::v5_0sp2::MsgTypes::BusinessMessageReject_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const client_order_id= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "2");
	BOOST_CHECK(f.link.are_all_logged_on());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(new_order)

BOOST_AUTO_TEST_SUITE(buy)

BOOST_AUTO_TEST_SUITE(day)

/**
	\test "Order handling" Test: Response to a BUY, DAY, MARKET NewOrder is a filled FIX message.
			=======================================================================================
	Verify that the response to a buy, day, market NewOrder is a filled FIX message.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(market_fill, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=167\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB00BH4HKS39\00122=4\00154=1\00138=99\00140=1\00144=41.000000\00159=0\0011133=G\001100=MTAA\00110=133\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t::static_type);
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_REQUIRE_NO_THROW(f.client.send(fix_msg));
	exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const client_order_id= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "2");
	auto const securityIDSource= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(securityIDSource, "4");
	auto const securityID= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(securityID, "GB00BH4HKS39");
	auto const execType= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ExecType>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::ExecType>(execType[0]), exchanges::FIX::common::ExecType::New);
	auto const price= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(price, "41.000000");
	auto const side= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const ordStatus= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::OrdStatus>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::OrdStatus>(ordStatus[0]), exchanges::FIX::common::OrdStatus::Filled);
	auto const orderQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "99");
	auto const leavesQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "0");
}

/**
	\test "Order handling" Test: Response to a BUY, DAY, MARKET NewOrder is a filled FIX message.
			=======================================================================================
	Verify that the response to a buy, day, market NewOrder is a filled FIX message.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(market_partial_fill, exchg_t, exchg_t_types) {
	using fixture_t= simulator_and_link_client_too<exchg_t>;

	fixture_t f;

	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=168\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB00BH4HKS39\00122=4\00154=1\00138=101\00140=1\00144=41.000000\00159=0\0011133=G\001100=MTAA\00110=166\001"};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(fix_msg.type(), exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t::static_type);
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_REQUIRE_NO_THROW(f.client.send(fix_msg));
	exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t receive_fix_msg;
	BOOST_CHECK(f.client.receive(receive_fix_msg));
	BOOST_CHECK(receive_fix_msg.is_valid());
	auto const client_order_id= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "2");
	auto const securityIDSource= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(securityIDSource, "4");
	auto const securityID= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(securityID, "GB00BH4HKS39");
	auto const execType= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::ExecType>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::ExecType>(execType[0]), exchanges::FIX::common::ExecType::New);
	auto const price= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(price, "42.000000");
	auto const side= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const ordStatus= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::OrdStatus>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::OrdStatus>(ordStatus[0]), exchanges::FIX::common::OrdStatus::Partially_filled);
	auto const orderQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "100");
	auto const leavesQty= receive_fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "1");
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
