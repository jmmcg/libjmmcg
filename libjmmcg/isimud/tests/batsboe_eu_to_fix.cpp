/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/conversions/batsboe_eu_to_fix_conversions.hpp"

using namespace libjmmcg;
using namespace libisimud;

const int32_t seqNum= 1;
const exchanges::BATSBOE::common::ClientOrderID_t clientOrderId1{"abcdefghijklm0test1"};
const exchanges::BATSBOE::common::Price_t price= 42 * exchanges::BATSBOE::common::implied_decimal_places;
const exchanges::BATSBOE::common::SecurityID_t instID{"GB00BH4HKS39"};
const exchanges::BATSBOE::common::Symbol_t symbol{"GB00BH4"};
const exchanges::BATSBOE::common::Side::element_type s= exchanges::BATSBOE::common::Side::Buy;

typedef boost::mpl::list<
	exchanges::BATSBOE::EU::v1::MsgTypes	//,
	// TODO	exchanges::BATSBOE::EU::v2::MsgTypes
	>
	msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderExecution, msg, msg_types) {
	typename msg::OrderExecution_t exchg_msg(seqNum, clientOrderId1, price, instID, s);
	exchg_msg.executedQty(0);
	exchg_msg.leavesQty(0);
	exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
	auto const fix_price= fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::BATSBOE::common::Price_t>(fromstring<double>(fix_price.begin(), fix_price.size())), price / exchanges::BATSBOE::common::implied_decimal_places);
	auto const sec_id_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(sec_id_type, "4");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(symbol, instID.begin());
	auto const orderQty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "0");
	auto const leavesQty= fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "0");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderRejected, msg, msg_types) {
	typename msg::OrderRejected_t exchg_msg(seqNum, clientOrderId1, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	exchanges::FIX::v5_0sp2::MsgTypes::OrderRejected_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CancelRejected, msg, msg_types) {
	typename msg::CancelRejected_t exchg_msg(seqNum, clientOrderId1, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	exchanges::FIX::v5_0sp2::MsgTypes::CancelRejected_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(UserModifyRejected, msg, msg_types) {
	typename msg::UserModifyRejected_t exchg_msg(seqNum, clientOrderId1, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	const exchanges::FIX::v5_0sp2::MsgTypes::BusinessMessageReject_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const ref_seq_num= fix_msg.find<exchanges::FIX::common::FieldsFast::RefSeqNum>();
	BOOST_CHECK_EQUAL(ref_seq_num, "1");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelled, msg, msg_types) {
	typename msg::OrderCancelled_t exchg_msg(seqNum, clientOrderId1, exchanges::BATSBOE::common::OrderRejectReason::Admin, symbol, price, s, 0, 0);
	exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
	auto const fix_price= fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::BATSBOE::common::Price_t>(fromstring<double>(fix_price.begin(), fix_price.size())), price / exchanges::BATSBOE::common::implied_decimal_places);
	auto const orderQty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "0");
	auto const leavesQty= fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "0");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const symbolf= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(symbolf, symbol.begin());
}

BOOST_AUTO_TEST_SUITE_END()
