/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/FIX/v5.0sp2/messages.hpp"

using namespace libisimud;

typedef boost::mpl::list<
	exchanges::FIX::v5_0sp2::MsgTypes>
	msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(admin)

BOOST_AUTO_TEST_CASE_TEMPLATE(print_details, msg, msg_types) {
	std::stringstream ss;
	BOOST_CHECK_NO_THROW(msg::to_stream(ss));
	BOOST_CHECK(!ss.str().empty());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logon, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=238\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogonRequest_t const&>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	BOOST_CHECK(fix_msg.is_valid());
	auto const usr= fix_msg.find<exchanges::FIX::common::FieldsFast::Username>();
	BOOST_CHECK_EQUAL(usr, "USER");
	auto const pass= fix_msg.find<exchanges::FIX::common::FieldsFast::Password>();
	BOOST_CHECK_EQUAL(pass, "PASSWORD");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(LogoutRequest, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=005\00135=5\00110=005\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogoutRequest_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(hearbeat, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=005\00135=0\00110=000\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(trade)

BOOST_AUTO_TEST_CASE_TEMPLATE(new_order_VOD, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB00BH4HKS39\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=000\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>();
	BOOST_CHECK_EQUAL(symbol, "EK");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const transact_time= fix_msg.find<exchanges::FIX::common::FieldsFast::TransactTime>();
	BOOST_CHECK_EQUAL(transact_time, "19980930-09:25:58");
	auto const order_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(order_qty, "10000");
	auto const ord_type= fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>();
	BOOST_CHECK_EQUAL(ord_type, "2");
	auto const sec_id_src_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(sec_id_src_type, "4");
	auto const sec_id_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(sec_id_type, "GB00BH4HKS39");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(new_order_ARM, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=153\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>();
	BOOST_CHECK_EQUAL(symbol, "EK");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const transact_time= fix_msg.find<exchanges::FIX::common::FieldsFast::TransactTime>();
	BOOST_CHECK_EQUAL(transact_time, "19980930-09:25:58");
	auto const order_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(order_qty, "10000");
	auto const ord_type= fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>();
	BOOST_CHECK_EQUAL(ord_type, "2");
	auto const sec_id_src_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(sec_id_src_type, "4");
	auto const sec_id_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(sec_id_type, "GB0000595859");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(order_cancel, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=073\00135=F\00111=10\00155=CUSTOME\00154=1\00160=19980930-09:25:58\00138=10000\0011133=G\001100=MTAA\00110=190\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>();
	BOOST_CHECK_EQUAL(symbol, "CUSTOME");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const transact_time= fix_msg.find<exchanges::FIX::common::FieldsFast::TransactTime>();
	BOOST_CHECK_EQUAL(transact_time, "19980930-09:25:58");
	auto const order_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(order_qty, "10000");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(order_cancel_replace, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=125\00135=G\00111=10\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=10000\00140=2\00122=4\00148=GB00BH4HKS39\00110=042\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const senderCompID= fix_msg.find<exchanges::FIX::common::FieldsFast::SenderCompID>();
	BOOST_CHECK_EQUAL(senderCompID, "VENDOR");
	auto const targetCompID= fix_msg.find<exchanges::FIX::common::FieldsFast::TargetCompID>();
	BOOST_CHECK_EQUAL(targetCompID, "BROKER");
	auto const msgSeqNum= fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(msgSeqNum, "10");
	auto const sendingTime= fix_msg.find<exchanges::FIX::common::FieldsFast::SendingTime>();
	BOOST_CHECK_EQUAL(sendingTime, "20000426-12:05:06");
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>();
	BOOST_CHECK_EQUAL(symbol, "EK");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const transact_time= fix_msg.find<exchanges::FIX::common::FieldsFast::TransactTime>();
	BOOST_CHECK_EQUAL(transact_time, "19980930-09:25:58");
	auto const order_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(order_qty, "10000");
	auto const ordType= fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>();
	BOOST_CHECK_EQUAL(ordType, "2");
	auto const sec_id_src_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(sec_id_src_type, "4");
	auto const sec_id_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(sec_id_type, "GB00BH4HKS39");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(order_reject, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=011\00135=3\00145=10\00110=008\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderRejected_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const ref_seq_num= fix_msg.find<exchanges::FIX::common::FieldsFast::RefSeqNum>();
	BOOST_CHECK_EQUAL(ref_seq_num, "10");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(execution_report, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=107\00135=8\00134=2\00111=10\0019=078\00135=8\00134=2\00111=10\00122=4\00148=GB00BH4HKS39\001150=0\00138=10000\001151=10000\00139=0\00144=42.000000\00154=1\00110=152\001"};
	auto const& fix_msg= reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t const&>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(fix_msg.length(), std::strlen(fix_buffer.begin()));
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, "10");
	auto const seqNum= fix_msg.find<exchanges::FIX::common::FieldsFast::MsgSeqNum>();
	BOOST_CHECK_EQUAL(seqNum, "2");
	auto const securityIDSource= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(securityIDSource, "4");
	auto const securityID= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(securityID, "GB00BH4HKS39");
	auto const execType= fix_msg.find<exchanges::FIX::common::FieldsFast::ExecType>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::ExecType>(execType[0]), exchanges::FIX::common::ExecType::New);
	auto const price= fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(price, "42.000000");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
	auto const order_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(order_qty, "10000");
	auto const leaves_qty= fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leaves_qty, "10000");
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
