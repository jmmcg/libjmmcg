/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/conversions/mit_to_fix_conversions.hpp"

#include "core/msm.hpp"

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_TEST_MSM_HDR

using namespace libjmmcg;
using namespace libisimud;

const int32_t seqNum= 1;
const exchanges::MIT::common::ClientOrderID_t clientOrderId1{"abcdefghijklm0test1"};
const exchanges::MIT::common::AppID::element_type aID= exchanges::MIT::common::AppID::Partition1;
const exchanges::MIT::common::ExecType::element_type eT= exchanges::MIT::common::ExecType::New;
const exchanges::MIT::common::Price_t price= 42 * exchanges::MIT::common::implied_decimal_places;
const exchanges::MIT::common::SecurityID_t instID= 133215;
const exchanges::MIT::common::Side::element_type s= exchanges::MIT::common::Side::Buy;

typedef boost::mpl::list<
	std::pair<exchanges::MIT::BIT::MsgTypes, exchanges::MIT::BIT::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::JSE::MsgTypes, exchanges::MIT::JSE::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::LSE::MsgTypes, exchanges::MIT::LSE::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::OSLO::MsgTypes, exchanges::MIT::OSLO::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::TRQ::MsgTypes, exchanges::MIT::TRQ::MsgTypes::ref_data>>
	msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_CASE_TEMPLATE(ExecutionReport, msg, msg_types) {
	typename msg::first_type::ExecutionReport_t exchg_msg(seqNum, clientOrderId1, aID, eT, price, instID, s);
	exchg_msg.orderStatus_= exchanges::MIT::common::OrderStatus::Rejected;
	exchg_msg.executedQty(0);
	exchg_msg.leavesQty(0);
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss << ref_data_file;
	const typename msg::second_type ref_data(ss);
	const exchanges::FIX::v5_0sp2::MsgTypes::ExecutionReport_t fix_msg(exchg_msg, ref_data);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
	auto const exec_type= fix_msg.find<exchanges::FIX::common::FieldsFast::ExecType>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::ExecType>(exec_type[0]), exchanges::FIX::common::convert(eT));
	auto const fix_price= fix_msg.find<exchanges::FIX::common::FieldsFast::Price>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::MIT::common::Price_t>(fromstring<double>(fix_price.begin(), fix_price.size())), price / exchanges::MIT::common::implied_decimal_places);
	auto const sec_id_type= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityIDSource>();
	BOOST_CHECK_EQUAL(sec_id_type, "4");
	auto const symbol= fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>();
	BOOST_CHECK_EQUAL(symbol, "GB00BH4HKS39");
	auto const ordStatus= fix_msg.find<exchanges::FIX::common::FieldsFast::OrdStatus>();
	BOOST_CHECK_EQUAL(static_cast<exchanges::FIX::common::OrdStatus>(ordStatus[0]), exchanges::FIX::common::OrdStatus::Rejected);
	auto const orderQty= fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>();
	BOOST_CHECK_EQUAL(orderQty, "0");
	auto const leavesQty= fix_msg.find<exchanges::FIX::common::FieldsFast::LeavesQty>();
	BOOST_CHECK_EQUAL(leavesQty, "0");
	auto const side= fix_msg.find<exchanges::FIX::common::FieldsFast::Side>();
	BOOST_CHECK_EQUAL(side, "1");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reject, msg, msg_types) {
	typename msg::first_type::Reject_t exchg_msg(clientOrderId1, static_cast<typename msg::first_type::Reject_t::RejectCode_t>(2004));
	const exchanges::FIX::v5_0sp2::MsgTypes::OrderRejected_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(order_reject, msg, msg_types) {
	typename msg::first_type::ExecutionReport_t exchg_msg(seqNum, clientOrderId1, aID, eT, price, instID, s);
	exchg_msg.orderRejectCode(static_cast<typename msg::first_type::Reject_t::RejectCode_t>(2004));
	exchg_msg.orderStatus(exchanges::MIT::common::OrderStatus::Rejected);
	const exchanges::FIX::v5_0sp2::MsgTypes::OrderRejected_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const ref_seq_num= fix_msg.find<exchanges::FIX::common::FieldsFast::RefSeqNum>();
	BOOST_CHECK_EQUAL(ref_seq_num, "1");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(BusinessReject, msg, msg_types) {
	typename msg::first_type::BusinessReject_t exchg_msg(seqNum, clientOrderId1);
	exchg_msg.rejectCode(static_cast<typename msg::first_type::BusinessReject_t::RejectCode_t>(9000));
	const exchanges::FIX::v5_0sp2::MsgTypes::BusinessMessageReject_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const ref_seq_num= fix_msg.find<exchanges::FIX::common::FieldsFast::RefSeqNum>();
	BOOST_CHECK_EQUAL(ref_seq_num, "1");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelReject, msg, msg_types) {
	typename msg::first_type::OrderCancelReject_t exchg_msg(seqNum, clientOrderId1);
	exchg_msg.cancelRejectReason(static_cast<typename msg::first_type::Reject_t::RejectCode_t>(2004));
	const exchanges::FIX::v5_0sp2::MsgTypes::CancelRejected_t fix_msg(exchg_msg);
	BOOST_CHECK(fix_msg.is_valid());
	auto const client_order_id= fix_msg.find<exchanges::FIX::common::FieldsFast::ClOrdID>();
	BOOST_CHECK_EQUAL(client_order_id, clientOrderId1.begin());
}

using dest_msg_details_t= exchanges::FIX::v5_0sp2::MsgTypes;

template<class SrcMsgT>
struct business_state_machine_t : msm::hash::state_transition_table<business_state_machine_t<SrcMsgT>> {
	using src_msg_details_t= SrcMsgT;
	using msm_base_t= msm::hash::state_transition_table<business_state_machine_t<SrcMsgT>>;
	using row_t= msm::hash::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;

	/**
	 *		From section \todo 5.1: "TODO" of [1]: the response to an ExecutionReport_t is an ExecutionReport_t.
	 *
	 *		\param	msg	The message that was received, that shall be processed.
	 *		\param client_skt	The socket to which any responses should be written.
	 */
	class ExecutionReportResponse {
	public:
		using arguments_types= std::pair<
			typename dest_msg_details_t::MsgTypes_t,
			std::tuple<
				std::tuple<typename src_msg_details_t::ExecutionReport_t const&, typename dest_msg_details_t::ExecutionReport_t&>>>;

		explicit constexpr ExecutionReportResponse(typename src_msg_details_t::ref_data const& rd) noexcept(true)
			: ref_data_(rd) {
			DEBUG_ASSERT(!ref_data_.empty());
		}

		constexpr ExecutionReportResponse(ExecutionReportResponse const& nor) noexcept(true)
			: ref_data_(nor.ref_data_) {
			DEBUG_ASSERT(!ref_data_.empty());
		}

		template<auto state, auto next>
		auto
		process(typename src_msg_details_t::ExecutionReport_t const& buff, typename dest_msg_details_t::ExecutionReport_t& reply) const noexcept(false) {
			typename src_msg_details_t::ExecutionReport_t const& msg= buff;
			BOOST_CHECK(msg.type() == state);
			BOOST_CHECK(!ref_data_.empty());
			reply= typename dest_msg_details_t::ExecutionReport_t(msg, ref_data_);
			return next;
		}

	private:
		typename src_msg_details_t::ref_data const& ref_data_;
	};

	using transition_table= typename msm_base_t::template rows<
		typename row_t::template row<
			src_msg_details_t::ExecutionReport_t::static_type,
			ExecutionReportResponse,
			dest_msg_details_t::ExecutionReport_t::static_type>,
		typename row_t::template row<
			src_msg_details_t::NewOrder_t::static_type,
			ExecutionReportResponse,
			dest_msg_details_t::NewOrder_t::static_type>>;
};

BOOST_AUTO_TEST_CASE_TEMPLATE(ExecutionReportUsingProcRules, msg, msg_types) {
	using src_msg_details_t= typename msg::first_type;
	using business_machine= msm::hash::machine<business_state_machine_t<src_msg_details_t>>;

	typename src_msg_details_t::ExecutionReport_t exchg_msg(seqNum, clientOrderId1, aID, eT, price, instID, s);
	exchg_msg.orderStatus_= exchanges::MIT::common::OrderStatus::Rejected;
	exchg_msg.executedQty(0);
	exchg_msg.leavesQty(0);
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss << ref_data_file;
	const typename msg::second_type ref_data(ss);
	BOOST_CHECK(!ref_data.empty());
	const business_machine business_msm(ref_data, ref_data);
	typename dest_msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK_EQUAL(business_msm.process(src_msg_details_t::ExecutionReport_t::static_type, exchg_msg, reply), dest_msg_details_t::ExecutionReport_t::static_type);
}

BOOST_AUTO_TEST_SUITE_END()
