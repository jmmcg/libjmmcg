/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/MIT/BIT/bit.hpp"
#include "../exchanges/MIT/BIT/bit_sim.hpp"
#include "../exchanges/MIT/JSE/jse.hpp"
#include "../exchanges/MIT/JSE/jse_sim.hpp"
#include "../exchanges/MIT/LSE/lse.hpp"
#include "../exchanges/MIT/LSE/lse_sim.hpp"
#include "../exchanges/MIT/OSLO/oslo.hpp"
#include "../exchanges/MIT/OSLO/oslo_sim.hpp"
#include "../exchanges/MIT/TRQ/trq.hpp"
#include "../exchanges/MIT/TRQ/trq_sim.hpp"

using namespace libjmmcg;
using namespace libisimud;

using api_thread_traits= ppd::thread_params<ppd::platform_api>;

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const boost::asio::ip::address primary_gw(boost::asio::ip::address_v4::loopback());
const unsigned short unused_primary_port= 12347u;
const boost::asio::ip::address secondary_gw(boost::asio::ip::address_v4::loopback());
const unsigned short unused_secondary_port= unused_primary_port + 1;
const exchanges::MIT::common::ClientOrderID_t clientOrderId1{"00000000000000test1"};

typedef boost::mpl::list<
	std::pair<exchanges::MIT::BIT::connection_t, exchanges::MIT::BIT::simulator_t>,
	std::pair<exchanges::MIT::JSE::connection_t, exchanges::MIT::JSE::simulator_t>,
	std::pair<exchanges::MIT::LSE::connection_t, exchanges::MIT::LSE::simulator_t>,
	std::pair<exchanges::MIT::OSLO::connection_t, exchanges::MIT::OSLO::simulator_t>,
	std::pair<exchanges::MIT::TRQ::connection_t, exchanges::MIT::TRQ::simulator_t> >
	exchg_t_types;

template<class exchg_t>
struct conn_args {
	using connection_t= typename exchg_t::first_type;
	using simulator_t= typename exchg_t::second_type;
	using conn_pol_t= typename connection_t::conn_pol_t;

	typename conn_pol_t::gateways_t gateways{
		std::make_pair(primary_gw, unused_primary_port),
		std::make_pair(secondary_gw, unused_secondary_port)};
	const conn_pol_t conn_pol{
		gateways,
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t{
			simulator_t::proc_rules_t::username,
			simulator_t::proc_rules_t::password,
			simulator_t::proc_rules_t::new_password},
		exchanges::MIT::common::logoff_args_t{
			simulator_t::proc_rules_t::logout_reason}};
};

template<class exchg_t>
struct conn_args_n_sim : public conn_args<exchg_t> {
	using base_t= conn_args<exchg_t>;
	using simulator_t= typename base_t::simulator_t;

	typename simulator_t::proc_rules_t proc_rules;
	no_latency_timestamps ts{0};
	typename simulator_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	simulator_t svr{
		typename simulator_t::ctor_args{
			primary_gw,
			unused_primary_port,
			simulator_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		proc_rules,
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}};
};

template<class exchg_t>
struct conn_args_sim_n_link : public conn_args_n_sim<exchg_t> {
	using base_t= conn_args_n_sim<exchg_t>;
	using connection_t= typename base_t::connection_t;
	using link_t= typename exchg_t::first_type;

	link_t link{
		this->conn_pol,
		link_t::skt_mgr_t::socket_t::socket_priority::low,
		exchanges::common::thread_traits::client_to_exchange_thread.core,
	};
};

template<class exchg_t>
struct conn_args_sim_n_link_logon_logoff : public conn_args_sim_n_link<exchg_t> {
	using base_t= conn_args_sim_n_link<exchg_t>;
	using connection_t= typename base_t::connection_t;
	using simulator_t= typename base_t::simulator_t;

	conn_args_sim_n_link_logon_logoff() {
		const typename connection_t::msg_details_t::LogonRequest_t msg(
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::UserName_t(this->conn_pol.logon_args.username),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(this->conn_pol.logon_args.password),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(this->conn_pol.logon_args.new_password));
		BOOST_REQUIRE_NO_THROW(this->link.send(msg));
		typename connection_t::msg_details_t::LogonReply_t reply;
		BOOST_CHECK(this->link.receive(reply));
		BOOST_CHECK_EQUAL(reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::LogonReply_t));
		BOOST_CHECK_EQUAL(reply.type(), connection_t::msg_details_t::LogonReply_t::static_type);
		BOOST_CHECK_EQUAL(reply.rejectCode(), connection_t::msg_details_t::LogonReply_t::logon_success);
	}

	~conn_args_sim_n_link_logon_logoff() {
		const typename connection_t::msg_details_t::LogoutRequest_t msg(exchanges::MIT::common::Reason_t{"fubar"});
		BOOST_REQUIRE_NO_THROW(this->link.send(msg));
		typename connection_t::msg_details_t::Logout_t reply;
		BOOST_CHECK(this->link.receive(reply));
		BOOST_CHECK_EQUAL(reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::Logout_t));
		BOOST_CHECK_EQUAL(reply.type(), connection_t::msg_details_t::Logout_t::static_type);
		BOOST_CHECK_EQUAL(reply.reason(), this->conn_pol.logoff_args.reason);
	}
};

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(links)

/**
	\test Section 4.4: "Connectivity Policy" of [1] Test: both gateways unavailable.
			==========================================================================
	Verify that more than 2*3*5 seconds passes if both gateways are unavailable.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(both_gateways_unavailable, exchg_t, exchg_t_types) {
	using fixture_t= conn_args<exchg_t>;
	fixture_t f;

	auto const& begin= std::chrono::system_clock::now();
	BOOST_CHECK_THROW(
		typename exchg_t::first_type link(
			f.conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core),
		std::exception);
	auto const& end= std::chrono::system_clock::now();
	BOOST_CHECK_GE(std::chrono::duration_cast<std::chrono::seconds>(end - begin).count(), (fixture_t::conn_pol_t::max_attempts * fixture_t::conn_pol_t::min_timeout * 2).count());
}

/**
	\test Section 4.4: "Connectivity Policy" of [1] Test: primary gateway available.
			==========================================================================
	Verify that less than 5 seconds passes if just the primary gateway is available.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(primary_gateway_available, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_n_sim<exchg_t>;
	fixture_t f;

	auto const& begin= std::chrono::system_clock::now();
	BOOST_REQUIRE_NO_THROW(
		typename exchg_t::first_type link(
			f.conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core));
	auto const& end= std::chrono::system_clock::now();
	BOOST_CHECK_LT(std::chrono::duration_cast<std::chrono::seconds>(end - begin).count(), fixture_t::conn_pol_t::min_timeout.count());
}

/**
	\test Re-connect to the available primary gateway.
			============================================
	Verify that less than 5 seconds passes if just the primary gateway is available for the re-connections.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(re_connnect, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_n_sim<exchg_t>;
	fixture_t f;

	BOOST_REQUIRE_NO_THROW(
		typename exchg_t::first_type link(
			f.conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core));
	auto const& begin= std::chrono::system_clock::now();
	BOOST_REQUIRE_NO_THROW(
		typename exchg_t::first_type link(
			f.conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core));
	auto const& end= std::chrono::system_clock::now();
	BOOST_CHECK_LT(std::chrono::duration_cast<std::chrono::seconds>(end - begin).count(), fixture_t::conn_pol_t::min_timeout.count());
}

/**
	\test Section 4.4: "Connectivity Policy" of [1] Test: primary gateway unavailable.
			============================================================================
	Verify that more than 3*5 seconds passes if just the primary gateway is unavailable, and the secondary available.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(secondary_gateway_available, exchg_t, exchg_t_types) {
	using fixture_t= conn_args<exchg_t>;
	fixture_t f;

	typename fixture_t::simulator_t::proc_rules_t proc_rules;
	no_latency_timestamps ts{0};
	typename fixture_t::simulator_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	typename fixture_t::simulator_t svr(
		typename fixture_t::simulator_t::ctor_args{
			boost::asio::ip::address(),
			unused_secondary_port,
			fixture_t::simulator_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		proc_rules,
		report_error,
		ts,
		typename fixture_t::simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)});

	auto const& begin= std::chrono::system_clock::now();
	BOOST_REQUIRE_NO_THROW(
		typename exchg_t::first_type link(
			f.conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
			exchanges::common::thread_traits::client_to_exchange_thread.core));
	auto const& end= std::chrono::system_clock::now();
	BOOST_CHECK_GE(std::chrono::duration_cast<std::chrono::seconds>(end - begin).count(), (fixture_t::conn_pol_t::max_attempts * fixture_t::conn_pol_t::min_timeout).count());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(admin)

/**
	\test Section 5.1: "Establishing a connection" of [1] Test: LogonRequest response.
			============================================================================
	Verify that the response to a LogonRequest is a LogonReply.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(logon, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::LogonRequest_t msg(
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::UserName_t(f.conn_pol.logon_args.username),
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.password),
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.new_password));
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::LogonReply_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::LogonReply_t));
	BOOST_CHECK_EQUAL(reply.type(), connection_t::msg_details_t::LogonReply_t::static_type);
	BOOST_CHECK_EQUAL(reply.rejectCode(), connection_t::msg_details_t::LogonReply_t::logon_success);
}

/**
	\test Section 5.2.2: "Heartbeats" of [1] Test: Client Heartbeat.
			==========================================================
	Verify that the response to a Heartbeat is nothing.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(client_heartbeat, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::ClientHeartbeat_t msg;
	BOOST_CHECK_NO_THROW(f.link.send(msg));
}

/**
	\test Section 5.2.2: "Heartbeats" of [1] Test: Server Heartbeat.
			==========================================================
	Verify that the simulator sends a heartbeat after the appropriate interval.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(server_heartbeat, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_n_sim<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const auto start= std::chrono::high_resolution_clock::now();
	typename exchg_t::first_type link(
		f.conn_pol,
		exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low,
		exchanges::common::thread_traits::client_to_exchange_thread.core);
	typename connection_t::msg_details_t::ClientHeartbeat_t msg;
	BOOST_CHECK(link.receive(msg));
	const auto got_hb= std::chrono::high_resolution_clock::now();
	BOOST_CHECK_EQUAL(msg.start_of_message, 2);
	BOOST_CHECK_EQUAL(msg.length(), sizeof(typename connection_t::msg_details_t::ClientHeartbeat_t));
	BOOST_CHECK_EQUAL(msg.type(), exchanges::MIT::common::MsgType::Heartbeat);
	BOOST_WARN_CLOSE(
		static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(got_hb - start).count()),
		static_cast<float>(
			std::chrono::duration_cast<std::chrono::milliseconds>(
				fixture_t::simulator_t::svr_mgr_t::heartbeats_t::heartbeat_interval)
				.count()),
		0.5);
}

/**
	\test Section 5.3: "Terminating a connection" of [1] Test: LogoutRequest.
			===================================================================
	Verify that the response to a LogoutRequest is a Logout.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(logout, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::LogoutRequest_t msg(exchanges::MIT::common::Reason_t{"fubar"});
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::Logout_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::Logout_t));
	BOOST_CHECK_EQUAL(reply.type(), connection_t::msg_details_t::Logout_t::static_type);
	BOOST_CHECK_EQUAL(reply.reason(), f.conn_pol.logoff_args.reason);
}

/**
	\test Section 5.1: "Establishing a connection" of [1] Test: LogonRequest & LogoutRequest response.
			============================================================================================
	Verify the behaviour of a LogonRequest followed by a and LogoutRequest.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(logon_logout, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::LogonRequest_t logon_msg(
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::UserName_t(f.conn_pol.logon_args.username),
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.password),
		typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.new_password));
	BOOST_REQUIRE_NO_THROW(f.link.send(logon_msg));
	typename connection_t::msg_details_t::LogonReply_t logon_reply;
	BOOST_CHECK(f.link.receive(logon_reply));
	BOOST_CHECK_EQUAL(logon_reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(logon_reply.length(), sizeof(typename connection_t::msg_details_t::LogonReply_t));
	BOOST_CHECK_EQUAL(logon_reply.type(), connection_t::msg_details_t::LogonReply_t::static_type);
	BOOST_CHECK_EQUAL(logon_reply.rejectCode(), connection_t::msg_details_t::LogonReply_t::logon_success);
	const typename connection_t::msg_details_t::LogoutRequest_t logout_msg(exchanges::MIT::common::Reason_t{"fubar"});
	BOOST_REQUIRE_NO_THROW(f.link.send(logout_msg));
	typename connection_t::msg_details_t::Logout_t logout_reply;
	BOOST_CHECK(f.link.receive(logout_reply));
	BOOST_CHECK_EQUAL(logout_reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(logout_reply.length(), sizeof(typename connection_t::msg_details_t::Logout_t));
	BOOST_CHECK_EQUAL(logout_reply.type(), connection_t::msg_details_t::Logout_t::static_type);
	BOOST_CHECK_EQUAL(logout_reply.reason(), f.conn_pol.logoff_args.reason);
}

/**
	\test Section 5.1: "Establishing a connection" of [1] Test: Re-LogonRequest & LogoutRequest response.
			===============================================================================================
	Verify the behaviour of a LogonRequest followed by a and LogoutRequest, repeated.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
/* TODO - doesn't pass as not fixed....
BOOST_AUTO_TEST_CASE_TEMPLATE(re_logon_logout, exchg_t, exchg_t_types) {
	using fixture_t=conn_args_n_sim<exchg_t>;
	using connection_t=typename fixture_t::connection_t;
	fixture_t f;

	{
		typename exchg_t::first_type link(f.conn_pol, exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low);
		const typename connection_t::msg_details_t::LogonRequest_t logon_msg(
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::UserName_t(f.conn_pol.logon_args.username),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.password),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.new_password)
		);
		BOOST_REQUIRE_NO_THROW(link.send(logon_msg));
		typename connection_t::msg_details_t::LogonReply_t logon_reply;
		BOOST_CHECK(link.receive(logon_reply));
		BOOST_CHECK_EQUAL(logon_reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(logon_reply.length(), sizeof(typename connection_t::msg_details_t::LogonReply_t));
		BOOST_CHECK_EQUAL(logon_reply.type(), connection_t::msg_details_t::LogonReply_t::static_type);
		BOOST_CHECK_EQUAL(logon_reply.rejectCode(), connection_t::msg_details_t::LogonReply_t::logon_success);
		const typename connection_t::msg_details_t::LogoutRequest_t logout_msg(exchanges::MIT::common::Reason_t{"fubar"});
		BOOST_REQUIRE_NO_THROW(link.send(logout_msg));
		typename connection_t::msg_details_t::Logout_t logout_reply;
		BOOST_CHECK(link.receive(logout_reply));
		BOOST_CHECK_EQUAL(logout_reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(logout_reply.length(), sizeof(typename connection_t::msg_details_t::Logout_t));
		BOOST_CHECK_EQUAL(logout_reply.type(), connection_t::msg_details_t::Logout_t::static_type);
		BOOST_CHECK_EQUAL(logout_reply.reason(), f.conn_pol.logoff_args.reason);
	}
	{
		typename exchg_t::first_type link(f.conn_pol, exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::low);
		const typename connection_t::msg_details_t::LogonRequest_t logon_msg(
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::UserName_t(f.conn_pol.logon_args.username),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.password),
			typename connection_t::msg_details_t::LogonRequest_t::logon_args_t::Password_t(f.conn_pol.logon_args.new_password)
		);
		BOOST_REQUIRE_NO_THROW(link.send(logon_msg));
		typename connection_t::msg_details_t::LogonReply_t logon_reply;
		BOOST_CHECK(link.receive(logon_reply));
		BOOST_CHECK_EQUAL(logon_reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(logon_reply.length(), sizeof(typename connection_t::msg_details_t::LogonReply_t));
		BOOST_CHECK_EQUAL(logon_reply.type(), connection_t::msg_details_t::LogonReply_t::static_type);
		BOOST_CHECK_EQUAL(logon_reply.rejectCode(), connection_t::msg_details_t::LogonReply_t::logon_success);
		const typename connection_t::msg_details_t::LogoutRequest_t logout_msg(exchanges::MIT::common::Reason_t{"fubar"});
		BOOST_REQUIRE_NO_THROW(link.send(logout_msg));
		typename connection_t::msg_details_t::Logout_t logout_reply;
		BOOST_CHECK(link.receive(logout_reply));
		BOOST_CHECK_EQUAL(logout_reply.start_of_message, 2);
		BOOST_CHECK_EQUAL(logout_reply.length(), sizeof(typename connection_t::msg_details_t::Logout_t));
		BOOST_CHECK_EQUAL(logout_reply.type(), connection_t::msg_details_t::Logout_t::static_type);
		BOOST_CHECK_EQUAL(logout_reply.reason(), f.conn_pol.logoff_args.reason);
	}
}
*/
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(client_initiated)

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to an invalid NewOrder.
			==========================================================================
	Verify that the response to an invalid NewOrder is a Reject.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(reject, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t msg(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Market,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::invalidInstrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		exchg_t::second_type::proc_rules_t::scaled_price);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::BusinessReject_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::BusinessReject_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::BusinessMessageReject);
	BOOST_CHECK_EQUAL(reply.clientOrderID().begin(), msg.clientOrderID().begin());
	BOOST_CHECK_EQUAL(reply.rejectCode(), connection_t::msg_details_t::BusinessReject_t::unknown_instrument);
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to an invalid OrderCancelRequest.
			====================================================================================
	Verify that the response to an invalid OrderCancelRequest is a OrderCancelReject.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_reject, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::OrderCancelRequest_t msg(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::invalidInstrumentID,
		exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::OrderCancelReject_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::OrderCancelReject_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::OrderCancelReject);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.originalClientOrderID());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a valid OrderCancelRequest.
			=================================================================================
	Verify that the response to a valid OrderCancelRequest is a cancelled order ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_accept, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t msg(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Limit,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		exchg_t::second_type::proc_rules_t::scaled_price + 1);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	const typename connection_t::msg_details_t::OrderCancelRequest_t msg1(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::invalidInstrumentID,
		exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg1));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(reply.executedPrice(), 0);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::New);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), msg.orderQty());
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::Cancelled);
	BOOST_CHECK_EQUAL(reply.executedPrice(), msg.limitPrice());
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), msg.orderQty());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to an invalid OrderCancelReplaceRequest.
			===========================================================================================
	Verify that the response to an invalid OrderCancelReplaceRequest is a cancelled order OrderCancelReject.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(modify_reject, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::OrderCancelReplaceRequest_t msg(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit,
		exchg_t::second_type::proc_rules_t::scaled_price,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::OrderCancelReject_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::OrderCancelReject_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::OrderCancelReject);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.originalClientOrderID());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a valid OrderCancelReplaceRequest.
			========================================================================================
	Verify that the response to a valid OrderCancelReplaceRequest is a cancelled order ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(modify_accept, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t msg(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Limit,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		exchg_t::second_type::proc_rules_t::scaled_price + 1);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	const typename connection_t::msg_details_t::OrderCancelReplaceRequest_t msg1(
		clientOrderId1,
		clientOrderId1,
		msg.instrumentID(),
		exchg_t::second_type::proc_rules_t::quantity_limit,
		msg.limitPrice() + 2,
		msg.tif(),
		exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg1));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(reply.executedPrice(), 0);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::New);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), msg.orderQty());
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::Replaced);
	BOOST_CHECK_EQUAL(reply.executedPrice(), msg1.limitPrice());
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), msg1.orderQty());
}

BOOST_AUTO_TEST_SUITE(new_order)

BOOST_AUTO_TEST_SUITE(buy)

BOOST_AUTO_TEST_SUITE(day)

BOOST_AUTO_TEST_SUITE(market)

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a BUY, DAY, MARKET NewOrder is a filled ExecutionReport.
			==============================================================================================================
	Verify that the response to a buy, day, market NewOrder is a filled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(new_order, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t msg(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Market,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		exchg_t::second_type::proc_rules_t::scaled_price);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Filled);
	BOOST_CHECK_EQUAL(reply.executedQty(), msg.orderQty());
	BOOST_CHECK_EQUAL(reply.leavesQty(), 0);
	BOOST_CHECK_EQUAL(reply.executedPrice(), msg.limitPrice());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a BUY, DAY, MARKET NewOrder is a partially-filled ExecutionReport.
			==============================================================================================================
	Verify that the response to a suitably large, buy, day, market NewOrder is a partially-filled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(partial_fill, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t msg(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Market,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit + 1,
		exchg_t::second_type::proc_rules_t::scaled_price);
	BOOST_REQUIRE_NO_THROW(f.link.send(msg));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), msg.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Partiallyfilled);
	BOOST_CHECK_EQUAL(reply.executedQty(), exchg_t::second_type::proc_rules_t::quantity_limit);
	BOOST_CHECK_EQUAL(reply.leavesQty(), msg.orderQty() - exchg_t::second_type::proc_rules_t::quantity_limit);
	BOOST_CHECK_EQUAL(reply.executedPrice(), msg.limitPrice());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a OrderCancelRequest is a cancelled ExecutionReport.
			==============================================================================================================
	Verify that the response to a OrderCancelRequest of the remaining quantity of a suitably large, buy, day, market NewOrder is a cancelled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(partial_fill_cancel, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t new_order(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Market,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit + 1,
		exchg_t::second_type::proc_rules_t::scaled_price);
	BOOST_REQUIRE_NO_THROW(f.link.send(new_order));
	typename connection_t::msg_details_t::ExecutionReport_t partial_fill;
	BOOST_CHECK(f.link.receive(partial_fill));
	const typename connection_t::msg_details_t::OrderCancelRequest_t cancel(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_REQUIRE_NO_THROW(f.link.send(cancel));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::Cancelled);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), new_order.orderQty() - exchg_t::second_type::proc_rules_t::quantity_limit);
	BOOST_CHECK_EQUAL(reply.executedPrice(), new_order.limitPrice());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a OrderCancelReplaceRequest is a replaced ExecutionReport.
			==============================================================================================================
	Verify that the response to a OrderCancelReplaceRequest of the remaining quantity of a suitably large, buy, day, market NewOrder is a replaced ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(partial_fill_replace, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t new_order(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Market,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit + 1,
		exchg_t::second_type::proc_rules_t::scaled_price);
	BOOST_REQUIRE_NO_THROW(f.link.send(new_order));
	typename connection_t::msg_details_t::ExecutionReport_t partial_fill;
	BOOST_CHECK(f.link.receive(partial_fill));
	const typename connection_t::msg_details_t::OrderCancelReplaceRequest_t replace(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::instrumentID,
		partial_fill.leavesQty() + 1,
		exchg_t::second_type::proc_rules_t::scaled_price * 2,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_REQUIRE_NO_THROW(f.link.send(replace));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::Replaced);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(reply.executedQty(), 0);
	BOOST_CHECK_EQUAL(reply.leavesQty(), partial_fill.leavesQty() + 1);
	BOOST_CHECK_EQUAL(reply.executedPrice(), new_order.limitPrice() * 2);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(limit)

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a BUY, DAY, LIMIT NewOrder is a filled ExecutionReport.
			==============================================================================================================
	Verify that the response to a buy, day, limit NewOrder is a filled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(new_order, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t new_order(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Limit,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		exchg_t::second_type::proc_rules_t::scaled_price - 1);
	BOOST_REQUIRE_NO_THROW(f.link.send(new_order));
	typename connection_t::msg_details_t::ExecutionReport_t reply;
	BOOST_CHECK(f.link.receive(reply));
	BOOST_CHECK_EQUAL(reply.start_of_message, 2);
	BOOST_CHECK_EQUAL(reply.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(reply.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(reply.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(reply.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(reply.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(reply.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(reply.orderStatus(), exchanges::MIT::common::OrderStatus::Filled);
	BOOST_CHECK_EQUAL(reply.executedQty(), new_order.orderQty());
	BOOST_CHECK_EQUAL(reply.leavesQty(), 0);
	BOOST_CHECK_EQUAL(reply.executedPrice(), new_order.limitPrice());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a suitable BUY, DAY, LIMIT NewOrder followed by an OrderCancelRequest is a cancelled ExecutionReport.
			===========================================================================================================================================================
	Verify that the response to a buy, day, limit NewOrder that is open on the exchange then cancelled is a cancelled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(open_cancelled, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t new_order(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Limit,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		fixture_t::simulator_t::proc_rules_t::scaled_price + 1);
	BOOST_REQUIRE_NO_THROW(f.link.send(new_order));
	const typename connection_t::msg_details_t::OrderCancelRequest_t cancel(
		clientOrderId1,
		clientOrderId1,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_REQUIRE_NO_THROW(f.link.send(cancel));
	typename connection_t::msg_details_t::ExecutionReport_t cancelled;
	BOOST_CHECK(f.link.receive(cancelled));
	BOOST_CHECK_EQUAL(cancelled.start_of_message, 2);
	BOOST_CHECK_EQUAL(cancelled.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(cancelled.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(cancelled.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(cancelled.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(cancelled.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(cancelled.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(cancelled.orderStatus(), exchanges::MIT::common::OrderStatus::New);
	BOOST_CHECK_EQUAL(cancelled.executedQty(), 0);
	BOOST_CHECK_EQUAL(cancelled.leavesQty(), new_order.orderQty());
	BOOST_CHECK_EQUAL(cancelled.executedPrice(), 0);
	BOOST_CHECK(f.link.receive(cancelled));
	BOOST_CHECK_EQUAL(cancelled.start_of_message, 2);
	BOOST_CHECK_EQUAL(cancelled.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(cancelled.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(cancelled.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(cancelled.execType(), exchanges::MIT::common::ExecType::Cancelled);
	BOOST_CHECK_EQUAL(cancelled.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(cancelled.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(cancelled.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(cancelled.executedQty(), 0);
	BOOST_CHECK_EQUAL(cancelled.leavesQty(), new_order.orderQty());
	BOOST_CHECK_EQUAL(cancelled.executedPrice(), new_order.limitPrice());
}

/**
	\test Section 9.1 "Order handling" of [1] Test: Response to a suitable BUY, DAY, LIMIT NewOrder followed by an OrderCancelReplaceRequest is a cancelled ExecutionReport.
			==================================================================================================================================================================
	Verify that the response to a buy, day, limit NewOrder that is open on the exchange then modified is a cancelled ExecutionReport.
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(open_modified, exchg_t, exchg_t_types) {
	using fixture_t= conn_args_sim_n_link_logon_logoff<exchg_t>;
	using connection_t= typename fixture_t::connection_t;
	fixture_t f;

	const typename connection_t::msg_details_t::NewOrder_t new_order(
		0,
		clientOrderId1,
		exchg_t::first_type::msg_details_t::OrderType::Limit,
		exchanges::MIT::common::TIF::Day,
		exchg_t::first_type::msg_details_t::Side::Buy,
		exchg_t::second_type::proc_rules_t::instrumentID,
		exchg_t::second_type::proc_rules_t::quantity_limit - 1,
		fixture_t::simulator_t::proc_rules_t::scaled_price + 1);
	BOOST_REQUIRE_NO_THROW(f.link.send(new_order));
	const typename connection_t::msg_details_t::OrderCancelReplaceRequest_t replace(
		clientOrderId1,
		clientOrderId1,
		new_order.instrumentID(),
		exchg_t::second_type::proc_rules_t::quantity_limit,
		new_order.limitPrice() + 2,
		new_order.tif(),
		exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_REQUIRE_NO_THROW(f.link.send(replace));
	typename connection_t::msg_details_t::ExecutionReport_t cancelled;
	BOOST_CHECK(f.link.receive(cancelled));
	BOOST_CHECK_EQUAL(cancelled.start_of_message, 2);
	BOOST_CHECK_EQUAL(cancelled.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(cancelled.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(cancelled.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(cancelled.execType(), exchanges::MIT::common::ExecType::New);
	BOOST_CHECK_EQUAL(cancelled.executedPrice(), 0);
	BOOST_CHECK_EQUAL(cancelled.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(cancelled.side(), exchg_t::first_type::msg_details_t::Side::Buy);
	BOOST_CHECK_EQUAL(cancelled.orderStatus(), exchanges::MIT::common::OrderStatus::New);
	BOOST_CHECK_EQUAL(cancelled.executedQty(), 0);
	BOOST_CHECK_EQUAL(cancelled.leavesQty(), new_order.orderQty());
	BOOST_CHECK(f.link.receive(cancelled));
	BOOST_CHECK_EQUAL(cancelled.start_of_message, 2);
	BOOST_CHECK_EQUAL(cancelled.length(), sizeof(typename connection_t::msg_details_t::ExecutionReport_t));
	BOOST_CHECK_EQUAL(cancelled.type(), exchanges::MIT::common::MsgType::ExecutionReport);
	BOOST_CHECK_EQUAL(cancelled.clientOrderID(), new_order.clientOrderID());
	BOOST_CHECK_EQUAL(cancelled.execType(), exchanges::MIT::common::ExecType::Replaced);
	BOOST_CHECK_EQUAL(cancelled.executedPrice(), replace.limitPrice());
	BOOST_CHECK_EQUAL(cancelled.instrumentID(), exchg_t::second_type::proc_rules_t::instrumentID);
	BOOST_CHECK_EQUAL(cancelled.side(), exchg_t::first_type::msg_details_t::Side::Sell);
	BOOST_CHECK_EQUAL(cancelled.orderStatus(), exchanges::MIT::common::OrderStatus::Cancelled);
	BOOST_CHECK_EQUAL(cancelled.executedQty(), 0);
	BOOST_CHECK_EQUAL(cancelled.leavesQty(), replace.orderQty());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
