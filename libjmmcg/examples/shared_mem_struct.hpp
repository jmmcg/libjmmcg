#ifndef LIBJMMCG_EXAMPLES_SHARED_MEM_STRUCT_HPP
#define LIBJMMCG_EXAMPLES_SHARED_MEM_STRUCT_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/shared_mem.hpp"

#include <array>
#include <atomic>

static inline const libjmmcg::shared_mem::name_type name{"shared_mem_test"};
static inline constexpr libjmmcg::shared_mem::magic_type magic{LIBJMMCG_GIT_SHA_SHORT};

/**
 * Note the random alignment.
 */
struct shared_data {
	std::atomic<bool> exit_child{false};	///< Used to signal that the sub-process should exit.
	std::atomic<bool> subproc_started{false};	///< Used to indicate that the sub-process has started and is ready to communicate.
	std::atomic<bool> changed{false};	///< Used to signal that the data, below has been changed in some arbitrary manner.
	bool ping{false};
	bool pong{false};
	std::array<std::uint64_t, 1024> data;
};

#endif
