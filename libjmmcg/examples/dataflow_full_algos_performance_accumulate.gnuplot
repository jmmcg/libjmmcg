## $Header$
##
## Copyright (c) 2016 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

set macros
datafile = 'dataflow_full_algos_performance_accumulate.csv'
set datafile separator '|'
set title "Scalability of parallel accumulate algorithm for 2<<10 random items.\nError-bars: % average deviation."
firstrow = system("head -n1 " . datafile . " | sed 's/ /_/g' | sed 's/|/ /g'")
set xlabel word(firstrow, 1) noenhance
set ylabel word(firstrow, 2) noenhance
set zlabel word(firstrow, 5) noenhance rotate by 90
set key autotitle columnhead
set key font ",9" noenhance font "Helvetica,20"
set xtics font ",9" noenhance font "Helvetica,20"
set ytics font ",9" noenhance font "Helvetica,20"
set ztics font ",9" noenhance font "Helvetica,20"
set zrange [0:*]
set style line 1 linetype 1 linecolor 1 linewidth 2 pointsize 1.5
set style line 2 linetype 2 linecolor 1 linewidth 0.7
set style line 3 linetype 1 linecolor 2 linewidth 2 pointsize 1.5
set style line 4 linetype 2 linecolor 2 linewidth 0.7
set style line 5 linetype 1 linecolor 3 linewidth 2 pointsize 1.5
set style line 6 linetype 2 linecolor 3 linewidth 0.7
set style line 7 linetype 1 linecolor 4 linewidth 2 pointsize 1.5
set style line 8 linetype 2 linecolor 4 linewidth 0.7
set style line 9 linetype 1 linecolor 5 linewidth 2 pointsize 1.5
set style line 10 linetype 2 linecolor 5 linewidth 0.7
set style line 11 linetype 1 linecolor 6 linewidth 2 pointsize 1.5
set style line 12 linetype 2 linecolor 6 linewidth 0.7
set style line 13 linetype 1 linecolor 7 linewidth 2 pointsize 1.5
set style line 14 linetype 2 linecolor 7 linewidth 0.7
set grid layerdefault linecolor rgb "#000000" linewidth 0.5,linecolor rgb "#000000" linewidth 0.5
set term svg size 1280,1024 noenhance font "Helvetica,20"
set output 'dataflow_full_algos_performance_accumulate.svg'
splot	\
	datafile every::1::5 using 1:2:($3*(1-$4)) notitle with lines linestyle 2,	\
	datafile every::1::5 using 1:2:($3*(1+$4)) notitle with lines linestyle 2,	\
	datafile every::1::5 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 1,	\
	datafile every::6::11 using 1:2:($3*(1-$4)) notitle with lines linestyle 4,	\
	datafile every::6::11 using 1:2:($3*(1+$4)) notitle with lines linestyle 4,	\
	datafile every::6::11 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 3,	\
	datafile every::12::17 using 1:2:($3*(1-$4)) notitle with lines linestyle 6,	\
	datafile every::12::17 using 1:2:($3*(1+$4)) notitle with lines linestyle 6,	\
	datafile every::12::17 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 5,	\
	datafile every::18::23 using 1:2:($3*(1-$4)) notitle with lines linestyle 8,	\
	datafile every::18::23 using 1:2:($3*(1+$4)) notitle with lines linestyle 8,	\
	datafile every::18::23 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 7,	\
	datafile every::24::29 using 1:2:($3*(1-$4)) notitle with lines linestyle 10,	\
	datafile every::24::29 using 1:2:($3*(1+$4)) notitle with lines linestyle 10,	\
	datafile every::24::29 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 9,	\
	datafile every::30::35 using 1:2:($3*(1-$4)) notitle with lines linestyle 12,	\
	datafile every::30::35 using 1:2:($3*(1+$4)) notitle with lines linestyle 12,	\
	datafile every::30::35 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 11,	\
	datafile every::36::41 using 1:2:($3*(1-$4)) notitle with lines linestyle 14,	\
	datafile every::36::41 using 1:2:($3*(1+$4)) notitle with lines linestyle 14,	\
	datafile every::36::41 using 1:2:3:xticlabels(1) notitle with linespoints linestyle 13
