/******************************************************************************
** Copyright © 2010 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/jthread.hpp"

#include <typeinfo>

using namespace libjmmcg;

typedef boost::mpl::list<
	// TODO	ppd::sequential_mode,
	ppd::heavyweight_threading>
	thread_types;

template<bool exit_val>
struct mutator final {
	unsigned long i{};

	bool __fastcall operator()(auto) noexcept(true) {
		++i;
		return exit_val;
	}
};

template<bool exit_val, class Mdl>
using thread_wrapper= ppd::wrapper<mutator<exit_val>, ppd::platform_api, Mdl>;

/**
	\test	Tests for the thread-as-a-class model.
*/
BOOST_AUTO_TEST_SUITE(thread_wrapper_tests)

BOOST_AUTO_TEST_SUITE(generic)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_thread, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	BOOST_CHECK_GT(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_suspended, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not on Pthreads:	thr.create(run_and_exit::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_suspended_thread_then_resume, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not supported on Pthreads:		thr.create(Thrds::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not supported on Pthreads:		thr.resume();
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_running_thread_then_suspend, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	// TODO: Not supported on Pthreads:		thr.suspend();
	BOOST_CHECK_GT(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_running_thread_then_suspend_then_resume, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	// TODO: Not supported on Pthreads:		thr.suspend();
	// TODO: Not supported on Pthreads:		thr.resume();
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	BOOST_CHECK_GT(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_two_threads, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_two_threads, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	BOOST_CHECK_GT(thr1->i, 0U);
	BOOST_CHECK_GT(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_two_suspended_threads, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.create(Thrds::thread_traits::api_params_type::create_suspended);
	// TODO: Not supported on Pthreads:		thr2.create(Thrds::thread_traits::api_params_type::create_suspended);
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_two_suspended_threads_then_resume_them, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.create(Thrds::thread_traits::api_params_type::create_suspended);
	// TODO: Not supported on Pthreads:		thr2.create(Thrds::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.resume();
	// TODO: Not supported on Pthreads:		thr2.resume();
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_two_running_threads_then_suspend_them, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	// TODO: Not supported on Pthreads:		thr1.suspend();
	// TODO: Not supported on Pthreads:		thr2.suspend();
	BOOST_CHECK_GT(thr1->i, 0U);
	BOOST_CHECK_GT(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_two_running_threads_then_suspend_and_resume_them, Mdl, thread_types) {
	typedef thread_wrapper<true, Mdl> run_and_exit;

	run_and_exit thr1;
	run_and_exit thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
	// TODO: Not supported on Pthreads:		thr1.suspend();
	// TODO: Not supported on Pthreads:		thr2.suspend();
	// TODO: Not supported on Pthreads:		thr1.resume();
	// TODO: Not supported on Pthreads:		thr2.resume();
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(stl_like)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_functor, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	std::atomic<unsigned int> i{0U};
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr(
			"jthr" LIBJMMCG_ENQUOTE(__LINE__),
			[&i]() {
				return true;
			}));
		BOOST_CHECK_NO_THROW(run_and_exit thr(
			typename run_and_exit::thread_traits::thread_name_t{"jthr" LIBJMMCG_ENQUOTE(__LINE__)},
			run_and_exit::thread_traits::api_params_type::priority_type::normal,
			run_and_exit::thread_traits::api_params_type::all_cpus_mask(),
			[&i]() {
				return true;
			}));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_functor_stop, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	std::atomic<unsigned int> i{0U};
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr(
			"jthr" LIBJMMCG_ENQUOTE(__LINE__),
			[&i](std::atomic_flag&) {
				return true;
			}));
		BOOST_CHECK_NO_THROW(run_and_exit thr(
			typename run_and_exit::thread_traits::thread_name_t{"jthr" LIBJMMCG_ENQUOTE(__LINE__)},
			run_and_exit::thread_traits::api_params_type::priority_type::normal,
			run_and_exit::thread_traits::api_params_type::all_cpus_mask(),
			[&i](std::atomic_flag&) {
				return true;
			}));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_stdbind, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	struct increment_ref {
		static void result(std::atomic<unsigned int>& i, std::atomic_flag& exit_requested) noexcept(true) {
			++i;
			exit_requested.test_and_set(std::memory_order_seq_cst);
		}
	};

	std::atomic<unsigned int> i{0U};
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr("jthr" LIBJMMCG_ENQUOTE(__LINE__), std::bind(&increment_ref::result, std::ref(i), std::placeholders::_1)));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_stdfn, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	struct increment_ref {
		bool operator()(std::atomic<unsigned int>& i, std::atomic_flag& exit_requested) noexcept(true) {
			++i;
			exit_requested.test_and_set(std::memory_order_seq_cst);
			return true;
		}
	};

	std::atomic<unsigned int> i{0U};
	std::function<void(std::atomic_flag&)> fn(std::bind(increment_ref(), std::ref(i), std::placeholders::_1));
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr("jthr" LIBJMMCG_ENQUOTE(__LINE__), std::move(fn)));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_lambda, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	std::atomic<unsigned int> i{0U};
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr("jthr" LIBJMMCG_ENQUOTE(__LINE__), [&i](std::atomic_flag& exit_requested) {
			++i;
			exit_requested.test_and_set(std::memory_order_seq_cst);
		}));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_suspended_thread_then_resume, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	std::atomic<unsigned int> i{0U};
	run_and_exit thr("jthr" LIBJMMCG_ENQUOTE(__LINE__), [&i](std::atomic_flag& exit_requested) {
		++i;
		exit_requested.test_and_set(std::memory_order_seq_cst);
	});
	// TODO: Not supported on Pthreads:		thr.create(Thrds::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 1U);
	// TODO: Not supported on Pthreads:		thr.resume();
	ppd::api_threading_traits<ppd::platform_api, Mdl>::sleep(100);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_two_threads, Mdl, thread_types) {
	using run_and_exit= ppd::jthread<ppd::platform_api, Mdl>;

	std::atomic<unsigned int> i{0U};
	{
		BOOST_CHECK_NO_THROW(run_and_exit thr1("jthr" LIBJMMCG_ENQUOTE(__LINE__), [&i](std::atomic_flag& exit_requested) {
			++i;
			exit_requested.test_and_set(std::memory_order_seq_cst);
		}));
		BOOST_CHECK_NO_THROW(run_and_exit thr2("jthr" LIBJMMCG_ENQUOTE(__LINE__), [&i](std::atomic_flag& exit_requested) {
			++i;
			exit_requested.test_and_set(std::memory_order_seq_cst);
		}));
	}
	BOOST_CHECK_GE(i, 0U);
	BOOST_CHECK_LE(i, 2U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(multi_threaded)

BOOST_AUTO_TEST_CASE(default_ctor) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE(start_thread) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	BOOST_CHECK_GT(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE(create_suspended) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not on Pthreads:	thr.create(run_and_exit::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE(create_suspended_thread_then_resume) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not supported on Pthreads:		thr.create(Thrds::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr->i, 0U);
	// TODO: Not supported on Pthreads:		thr.resume();
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
}

BOOST_AUTO_TEST_CASE(create_running_thread_then_suspend) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	// TODO: Not supported on Pthreads:		thr.suspend();
}

BOOST_AUTO_TEST_CASE(create_running_thread_then_suspend_then_resume) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr;
	BOOST_CHECK_EQUAL(thr->i, 0U);
	BOOST_CHECK_NO_THROW(thr.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	// TODO: Not supported on Pthreads:		thr.suspend();
	// TODO: Not supported on Pthreads:		thr.resume();
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	BOOST_CHECK_GT(thr->i, 0U);
}

BOOST_AUTO_TEST_CASE(default_ctor_two_threads) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE(start_two_threads) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	BOOST_CHECK_GT(thr1->i, 0U);
	BOOST_CHECK_GT(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE(start_two_suspended_threads) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.create(Thrds::thread_traits::api_params_type::create_suspended);
	// TODO: Not supported on Pthreads:		thr2.create(Thrds::thread_traits::api_params_type::create_suspended);
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
}

BOOST_AUTO_TEST_CASE(start_two_suspended_threads_then_resume_them) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.create(Thrds::thread_traits::api_params_type::create_suspended);
	// TODO: Not supported on Pthreads:		thr2.create(Thrds::thread_traits::api_params_type::create_suspended);
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	// TODO: Not supported on Pthreads:		thr1.resume();
	// TODO: Not supported on Pthreads:		thr2.resume();
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
}

BOOST_AUTO_TEST_CASE(create_two_running_threads_then_suspend_them) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	// TODO: Not supported on Pthreads:		thr1.suspend();
	// TODO: Not supported on Pthreads:		thr2.suspend();
	BOOST_CHECK_GT(thr1->i, 0U);
	BOOST_CHECK_GT(thr2->i, 0U);
}

BOOST_AUTO_TEST_CASE(create_two_running_threads_then_suspend_and_resume_them) {
	typedef thread_wrapper<false, ppd::heavyweight_threading> run_forever;

	run_forever thr1;
	run_forever thr2;
	BOOST_CHECK_EQUAL(thr1->i, 0U);
	BOOST_CHECK_EQUAL(thr2->i, 0U);
	BOOST_CHECK_NO_THROW(thr1.create_running());
	BOOST_CHECK_NO_THROW(thr2.create_running());
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	// TODO: Not supported on Pthreads:		thr1.suspend();
	// TODO: Not supported on Pthreads:		thr2.suspend();
	// TODO: Not supported on Pthreads:		thr1.resume();
	// TODO: Not supported on Pthreads:		thr2.resume();
	ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>::sleep(100);
	BOOST_CHECK_GT(thr1->i, 0U);
	BOOST_CHECK_GT(thr2->i, 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
