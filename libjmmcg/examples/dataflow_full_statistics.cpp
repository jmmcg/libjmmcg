/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, template<class> class CFG= no_control_flow_graph, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct fifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk,
		basic_statistics,
		CFG>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, template<class> class CFG, unsigned int PoolSize, unsigned int GSSk>
const typename fifo_queue_t<Db, Sz, Jn, Mdl, CFG, PoolSize, GSSk>::pool_type::pool_type::size_type fifo_queue_t<Db, Sz, Jn, Mdl, CFG, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, template<class> class CFG= no_control_flow_graph, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct lifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk,
		basic_statistics,
		CFG>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, template<class> class CFG, unsigned int PoolSize, unsigned int GSSk>
const typename lifo_queue_t<Db, Sz, Jn, Mdl, CFG, PoolSize, GSSk>::pool_type::pool_type::size_type lifo_queue_t<Db, Sz, Jn, Mdl, CFG, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode, control_flow_graph>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, no_control_flow_graph, 1>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph, 1>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, no_control_flow_graph, 2>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode, control_flow_graph>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, no_control_flow_graph, 1>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph, 1>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, no_control_flow_graph, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph, 2> >
	finite_test_types;

typedef boost::mpl::list<
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode, control_flow_graph>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode, control_flow_graph>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, control_flow_graph> >
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

struct res_t {
	long i;
};

struct work_type {
	typedef res_t result_type;

	int i_;

	explicit work_type(const int i)
		: i_(i) {
	}

	void __fastcall process(result_type& r) {
		r.i= i_ << 1;
	}

	void __fastcall mutate(result_type& r) {
		process(r);
	}

	bool __fastcall operator<(work_type const& i) const {
		return i_ < i.i_;
	}
};

struct work_type_simple {
	int i_;

	work_type_simple(const int i)
		: i_(i) {
	}

	void __fastcall process(res_t& r) {
		r.i= i_ << 1;
	}

	bool __fastcall operator<(work_type_simple const& i) const {
		return i_ < i.i_;
	}
};

struct bool_work_type {
	typedef bool result_type;

	int i_;

	explicit bool_work_type(const int i)
		: i_(i) {
	}

	void __fastcall process(result_type& r) const {
		r= static_cast<bool>(i_);
	}

	bool __fastcall operator<(bool_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API, typename Mdl>
struct horizontal_work_type {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type(bool& r) noexcept(true)
		: release(r) {
	}

	void __fastcall process() noexcept(false) {
		while(!release) {
			api_threading_traits<API, Mdl>::sleep(10);
		}
	}

	bool __fastcall operator<(horizontal_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API>
struct horizontal_work_type<API, sequential_mode> {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type(bool& r) noexcept(true)
		: release(r) {
	}

	void __fastcall process() noexcept(true) {
	}

	bool __fastcall operator<(horizontal_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API, typename Mdl>
struct horizontal_work_type_rel {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type_rel(bool& r) noexcept(true)
		: release(r) {}

	void __fastcall process() noexcept(true) {
		release= true;
	}

	bool __fastcall operator<(horizontal_work_type_rel const&) const {
		return true;
	}
};

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& context= pool << joinable() << work_type_simple(1);
	BOOST_CHECK_EQUAL(context->i, 2);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 1);
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 1);
	BOOST_CHECK_GE(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_hrz_work().total_samples(), 1);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& context= pool << joinable() << work_type_simple(1);
	auto const& context2= pool << joinable() << work_type_simple(3);
	BOOST_CHECK_EQUAL(context->i, 2);
	BOOST_CHECK_EQUAL(context2->i, 6);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 2);
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_hrz_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(horizontal_threading, T, finite_test_types) {
	typedef horizontal_work_type<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type> hrz_wk_t;
	typedef horizontal_work_type_rel<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type> hrz_wk_rel_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	bool release= false;
	hrz_wk_t hz(release);
	hrz_wk_rel_t hz_rel(release);
	auto const& context= pool << joinable() << hz;
	auto const& context_rel= pool << joinable() << hz_rel;
	*context;
	*context_rel;
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 2);
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_hrz_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unary_fn, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	auto const& context= pool.unary_fun(bool_work_type(0), std::logical_not<bool_work_type::result_type>());
	BOOST_CHECK_EQUAL(*context, true);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 2);
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_hrz_work().total_samples(), 2);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	auto const& context= pool.logical_and(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(*context, true);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 3);
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 3);
	BOOST_CHECK_GE(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_hrz_work().total_samples(), 3);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& context= pool << joinable() << work_type_simple(1);
	BOOST_CHECK_EQUAL(context->i, 2);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 1);
	// TODO surely this should be accurate?
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 1);
	BOOST_CHECK_EQUAL(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& context= pool << joinable() << work_type_simple(1);
	auto const& context2= pool << joinable() << work_type_simple(3);
	BOOST_CHECK_EQUAL(context->i, 2);
	BOOST_CHECK_EQUAL(context2->i, 6);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 2);
	// TODO surely this should be accurate?
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 2);
	BOOST_CHECK_EQUAL(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unary_fn, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& context= pool.unary_fun(bool_work_type(1), std::logical_not<bool_work_type::result_type>());
	BOOST_CHECK_EQUAL(*context, false);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 2);
	// TODO surely this should be accurate?
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 2);
	BOOST_CHECK_EQUAL(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& context= pool.logical_and(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(*context, true);
	BOOST_CHECK_EQUAL(pool.statistics().total_work_added(), 3);
	// TODO surely this should be accurate?
	BOOST_CHECK_GE(pool.statistics().total_vertical_work().total_samples(), 0);
	BOOST_CHECK_LE(pool.statistics().total_vertical_work().total_samples(), 3);
	BOOST_CHECK_EQUAL(pool.statistics().total_hrz_work().total_samples(), 0);
	BOOST_CHECK_GE(pool.statistics().total_work_added(), pool.statistics().total_vertical_work().total_samples() + pool.statistics().total_hrz_work().total_samples());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::cout << pool.statistics() << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
