/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/deleter.hpp"
#include "core/thread_api_traits.hpp"
#include "core/unique_ptr.hpp"

using namespace libjmmcg;
using namespace ppd;

typedef boost::mpl::list<
	sequential_mode,
	heavyweight_threading>
	thread_types;

template<class Mdl>
struct base {
	using lock_traits= api_lock_traits<platform_api, Mdl>;
	using deleter_t= default_delete<base>;

	virtual ~base() noexcept(true) {}

	virtual void deleter() final {
		deleter_t().operator()(this);
	}

	bool __fastcall operator<(base const& a) const noexcept(true) {
		return this < &a;
	}
};

template<class Mdl>
struct derived final : public base<Mdl> {
	using base_t= base<Mdl>;
	using lock_traits= typename base_t::lock_traits;
	using deleter_t= typename base_t::deleter_t;
};

template<class Mdl>
struct placement_dtor_test_t {
	using lock_traits= api_lock_traits<platform_api, Mdl>;
	using deleter_t= placement_dtor<placement_dtor_test_t>;

	const int val;

	placement_dtor_test_t() noexcept(true)
		: val(42) {}

	~placement_dtor_test_t() noexcept(true) {}

	void deleter() {
		deleter_t().operator()(this);
	}

	bool __fastcall operator<(placement_dtor_test_t const& a) const noexcept(true) {
		return this < &a;
	}
};

template<class Mdl>
struct base_inh_t {
	using lock_traits= api_lock_traits<platform_api, Mdl>;
	using deleter_t= default_delete<base_inh_t>;

	virtual ~base_inh_t() noexcept(true) {
	}

	virtual int val() const noexcept(true)= 0;

	virtual void deleter() {
		deleter_t().operator()(this);
	}
};

template<class Mdl, template<class> class Del>
struct derived_inh_t final : public base_inh_t<Mdl> {
	using base_t= base_inh_t<Mdl>;
	using lock_traits= typename base_t::lock_traits;
	using deleter_t= Del<derived_inh_t>;

	const int val_;

	explicit derived_inh_t(int i) noexcept(true)
		: val_(i) {}

	int val() const noexcept(true) override {
		return val_;
	}

	void deleter() override {
		deleter_t().operator()(this);
	}

	bool __fastcall operator<(derived_inh_t const& a) const noexcept(true) {
		return this < &a;
	}
};

template<class Mdl>
struct stack_test_t {
	using lock_traits= api_lock_traits<platform_api, Mdl>;
	using deleter_t= noop_dtor<stack_test_t>;

	const int val;

	stack_test_t() noexcept(true)
		: val(42) {}

	~stack_test_t() noexcept(true) {}

	void deleter() {
		deleter_t().operator()(this);
	}
};

BOOST_AUTO_TEST_SUITE(unique_ptr_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	ptr_t a;
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
	BOOST_CHECK(!a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	BOOST_CHECK_EQUAL(a.get(), ptr_a);
	BOOST_CHECK(!(!a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(placement_new_ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<placement_dtor_test_t<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	char buff[sizeof(typename ptr_t::value_type)];
	typename ptr_t::value_type* ptr_a= new(buff) typename ptr_t::value_type;
	ptr_t a(ptr_a);
	BOOST_CHECK_EQUAL(a.get(), ptr_a);
	BOOST_CHECK(!(!a));
	BOOST_CHECK_EQUAL(a->val, 42);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(mixed_dtors_placement_new_ctor_unique_ptr, Mdl, thread_types) {
	typedef derived_inh_t<Mdl, default_delete> def_del_t;
	typedef derived_inh_t<Mdl, placement_dtor> plment_del_t;
	typedef unique_ptr<base_inh_t<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	char buff[sizeof(plment_del_t)];
	def_del_t* ptr_def_del= new def_del_t(42);
	plment_del_t* ptr_plment_del= new(buff) plment_del_t(1066);
	ptr_t sp_def_del(ptr_def_del);
	ptr_t sp_plment_del(ptr_plment_del);
	BOOST_CHECK_EQUAL(sp_def_del.get(), ptr_def_del);
	BOOST_CHECK(!(!sp_def_del));
	BOOST_CHECK_EQUAL(sp_def_del->val(), 42);
	BOOST_CHECK_EQUAL(sp_plment_del.get(), ptr_plment_del);
	BOOST_CHECK(!(!sp_plment_del));
	BOOST_CHECK_EQUAL(sp_plment_del->val(), 1066);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(move_ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1(std::move(a));
	BOOST_CHECK_EQUAL(a1.get(), ptr_a);
	BOOST_CHECK_EQUAL(a.get(), typename ptr_t::atomic_ptr_t());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unique_ptr_ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	std::unique_ptr<typename ptr_t::value_type, typename ptr_t::value_type::deleter_t> ptr_a(new typename ptr_t::value_type);
	ptr_t a(std::move(ptr_a));
	BOOST_CHECK(a.get());
	BOOST_CHECK_EQUAL(ptr_a.get(), static_cast<typename ptr_t::value_type*>(0));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unique_ptr_move_ctor_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	std::unique_ptr<typename ptr_t::value_type, typename ptr_t::value_type::deleter_t> ptr_a(new typename ptr_t::value_type);
	ptr_t a(std::move(ptr_a));
	BOOST_CHECK(dynamic_cast<typename ptr_t::value_type const*>(a.get().get()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reset_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	a.reset();
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
	BOOST_CHECK(!a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assign_move_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1;
	a1= std::move(a);
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
	BOOST_CHECK_EQUAL(a1.get(), ptr_a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor_assign_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	ptr_t a;
	a= ptr_t(new typename ptr_t::value_type);
	BOOST_CHECK(a.get());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor_assign_move_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	std::unique_ptr<typename ptr_t::value_type, typename ptr_t::value_type::deleter_t> ptr_a(new typename ptr_t::value_type);
	ptr_t a;
	a= ptr_t(std::move(ptr_a));
	BOOST_CHECK(a.get());
	BOOST_CHECK_EQUAL(ptr_a.get(), static_cast<typename ptr_t::value_type*>(0));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(equals_move, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1(std::move(a));
	BOOST_CHECK(a != a1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(not_equals_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	typename ptr_t::value_type* ptr_a1= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1(ptr_a1);
	BOOST_CHECK(a != a1);
	BOOST_CHECK(!(a == a1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(equals_comparators_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1(std::move(a));
	BOOST_CHECK(!(a < a1) && !(a > a1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(not_equals_comparators_unique_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	typename ptr_t::value_type* ptr_a1= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	ptr_t a1(ptr_a1);
	BOOST_CHECK((a < a1) || (a > a1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(inheritance_wrapping, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;
	typedef unique_ptr<derived<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_b_t;

	typename ptr_b_t::value_type* ptr_b= new typename ptr_b_t::value_type;
	ptr_t b(ptr_b);
	BOOST_CHECK(b.get() == ptr_b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(moves_the_ptr, Mdl, thread_types) {
	typedef unique_ptr<base<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type* ptr_a= new typename ptr_t::value_type;
	ptr_t a(ptr_a);
	{
		ptr_t a1(std::move(a));
		BOOST_CHECK(a.get() != a1.get());
		BOOST_CHECK(a != a1);
	}
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor_stack_test, Mdl, thread_types) {
	typedef unique_ptr<stack_test_t<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	ptr_t a;
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
	BOOST_CHECK(!a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor_stack_test, Mdl, thread_types) {
	typedef unique_ptr<stack_test_t<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type val_a;
	ptr_t a(&val_a);
	BOOST_CHECK(a.get() == &val_a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(moves_stack_test, Mdl, thread_types) {
	typedef unique_ptr<stack_test_t<Mdl>, api_lock_traits<platform_api, Mdl>> ptr_t;

	typename ptr_t::value_type val_a;
	ptr_t a(&val_a);
	{
		ptr_t a1(std::move(a));
		BOOST_CHECK(a.get() != a1.get());
		BOOST_CHECK(a != a1);
		BOOST_CHECK_EQUAL(a1->val, 42);
	}
	BOOST_CHECK_EQUAL(a.get(), static_cast<typename ptr_t::value_type*>(0));
}

BOOST_AUTO_TEST_SUITE_END()
