/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "frozen/unordered_map.h"

using mapping_t= frozen::unordered_map<std::int16_t, char, 5>;
static inline constexpr const mapping_t mapping= {
	{1, '1'},
	{12, '1'},
	{42, '2'},
	{69, '3'},
	{667, '4'}};

inline constexpr char
convert(std::int16_t v) noexcept(true) {
	const auto iter= mapping.find(v);
	if(iter != mapping.end()) {
		return iter->second;
	} else {
		return '\0';
	}
}

BOOST_AUTO_TEST_SUITE(constexpr_map)

namespace test {
constexpr const char a= convert(12);
BOOST_MPL_ASSERT_RELATION(a, ==, '1');
}

BOOST_AUTO_TEST_SUITE_END()
