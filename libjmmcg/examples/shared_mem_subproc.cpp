/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_testsh4NjRr1_f % n
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "shared_mem_struct.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_CASE(create_shm_wait_for_data_updates) {
	shared_mem shm(name, sizeof(shared_data));
	shared_data& data= shm.get<magic, shared_data>();
	auto wait_for_new_data_or_exit= [&data]() {
		const bool old= data.changed;
		while(data.changed == old) {
			if(data.exit_child) {
				return true;
			} else {
				std::this_thread::yield();
			}
		}
		return false;
	};
	data.subproc_started= true;
	while(!data.exit_child) {
		if(wait_for_new_data_or_exit()) {
			break;
		}
		BOOST_CHECK_EQUAL(data.ping, true);
		data.pong= true;
		data.changed= !data.changed;
		if(wait_for_new_data_or_exit()) {
			break;
		}
	}
	BOOST_CHECK_EQUAL(data.exit_child, true);
}
