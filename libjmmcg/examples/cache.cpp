/******************************************************************************
** Copyright © 2002 by J.M.a_cacheGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/cache.hpp"

using namespace libjmmcg;

struct my_factory final : public cache::factory_base<int, int> {
	inline value_type __fastcall make(const key_type& i) const override {
		return i << 2;
	}
};

typedef boost::mpl::list<
	cache::ro<my_factory>,
	cache::ro<my_factory, cache::lru<my_factory::value_type>, cache::threading<my_factory::key_type, cache::lru<my_factory::value_type> >::multi<ppd::platform_api, ppd::sequential_mode>, cache::basic_statistics>,
	cache::ro<my_factory, cache::lru<my_factory::value_type>, cache::threading<my_factory::key_type, cache::lru<my_factory::value_type> >::multi<ppd::platform_api, ppd::heavyweight_threading>, cache::basic_statistics> >
	test_types;

BOOST_AUTO_TEST_SUITE(cache_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	BOOST_CHECK_EQUAL(cache.empty(), true);
	BOOST_CHECK_EQUAL(cache.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear_empty, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	cache.clear();
	BOOST_CHECK_EQUAL(cache.empty(), true);
	BOOST_CHECK_EQUAL(cache.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(flush_empty, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	cache.flush();
	BOOST_CHECK_EQUAL(cache.empty(), true);
	BOOST_CHECK_EQUAL(cache.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_one_item, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	BOOST_CHECK_EQUAL(cache[1].value(), 4);
	BOOST_CHECK_EQUAL(cache.empty(), false);
	BOOST_CHECK_EQUAL(cache.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_two_items, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	BOOST_CHECK_EQUAL(cache[1].value(), 4);
	BOOST_CHECK_EQUAL(cache[2].value(), 8);
	BOOST_CHECK_EQUAL(cache.empty(), false);
	BOOST_CHECK_EQUAL(cache.size(), 2U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(flush_small, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	cache[1].value();
	cache[2].value();
	cache[3].value();
	cache.flush();
	BOOST_CHECK_EQUAL(cache.empty(), false);
	BOOST_CHECK_EQUAL(cache.size(), 3U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(flush_full, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	cache[1].value();
	cache[2].value();
	cache[3].value();
	cache[4].value();
	cache[5].value();
	cache.flush();
	BOOST_CHECK_EQUAL(cache.empty(), false);
	BOOST_CHECK_EQUAL(cache.size(), 2U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear_small, T, test_types) {
	typedef T cache_t;

	cache_t cache(2, 4, 2, my_factory());
	cache[1].value();
	cache[2].value();
	cache.clear();
	BOOST_CHECK_EQUAL(cache.empty(), true);
	BOOST_CHECK_EQUAL(cache.size(), 0U);
}

BOOST_AUTO_TEST_SUITE_END()
