/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/stack_string.hpp"

#include <sstream>

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(string_tests)

BOOST_AUTO_TEST_SUITE(default_ctors)

/**
	Test that we can default construct & delete a stack_string object.
*/
BOOST_AUTO_TEST_CASE(default_ctor) {
	stack_string s;
}

/**
	Test that we can default construct & delete a stack_string object.
*/
BOOST_AUTO_TEST_CASE(default_ctor_empty) {
	stack_string s;
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(s.capacity(), stack_string::size_type());
}

/* TODO
BOOST_AUTO_TEST_CASE(nullptr_ctor)
{
	bool exception_caught=false;
	try {
		char const *src=nullptr;
		stack_string s(src);
	} catch (stack_string::exception const &e) {
		exception_caught=true;
	}
	BOOST_CHECK(exception_caught);
}
*/
BOOST_AUTO_TEST_CASE(default_ctor_cctor) {
	stack_string s1;
	stack_string s2(s1);
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(default_ctor_move_ctor) {
	stack_string s1;
	stack_string s2(std::move(s1));
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(default_ctor_swap) {
	stack_string s1, s2;
	s1.swap(s2);
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(default_ctor_assignment) {
	stack_string s1;
	stack_string s2;
	s2= s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(default_ctor_move_assignment) {
	stack_string s1;
	stack_string s2;
	s2= std::move(s1);
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(self_assignment) {
	stack_string s;
	s= s;
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(s.capacity(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(equality) {
	stack_string s1, s2;
	BOOST_CHECK(s1 == s2);
}

BOOST_AUTO_TEST_CASE(inequality) {
	stack_string s1, s2;
	BOOST_CHECK(!(s1 != s2));
}

BOOST_AUTO_TEST_CASE(clear) {
	stack_string s;
	s.clear();
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(s.capacity(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(reserve) {
	const stack_string::size_type res(1);
	stack_string s;
	s.reserve(res);
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(s.capacity(), res);
}

BOOST_AUTO_TEST_CASE(rereserve_bigger) {
	const stack_string::size_type res1(1);
	const stack_string::size_type res2(2);
	stack_string s;
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK_EQUAL(s.capacity(), res2);
}

BOOST_AUTO_TEST_CASE(rereserve_smaller) {
	const stack_string::size_type res1(2);
	const stack_string::size_type res2(1);
	stack_string s;
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK_EQUAL(s.capacity(), res1);
}

BOOST_AUTO_TEST_CASE(resize) {
	const stack_string::size_type res(1);
	stack_string s;
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_LE(s.size(), s.capacity());
}

BOOST_AUTO_TEST_CASE(reresize_bigger) {
	const stack_string::size_type res1(1);
	const stack_string::size_type res2(2);
	stack_string s;
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_LE(s.size(), s.capacity());
}

BOOST_AUTO_TEST_CASE(reresize_smaller) {
	const stack_string::size_type res1(2);
	const stack_string::size_type res2(1);
	stack_string s;
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
}

BOOST_AUTO_TEST_CASE(resize_init) {
	const stack_string::size_type res(1);
	const stack_string::value_type fill= 'X';
	stack_string s;
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_LE(s.size(), s.capacity());
	BOOST_CHECK_EQUAL(s[0], fill);
}

BOOST_AUTO_TEST_CASE(reresize_bigger_init) {
	const stack_string::size_type res1(1);
	const stack_string::size_type res2(2);
	const stack_string::value_type fill= 'X';
	stack_string s;
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_LE(s.size(), s.capacity());
	BOOST_CHECK_EQUAL(s[0], fill);
	BOOST_CHECK_EQUAL(s[1], fill);
}

BOOST_AUTO_TEST_CASE(reresize_smaller_init) {
	const stack_string::size_type res1(2);
	const stack_string::size_type res2(1);
	const stack_string::value_type fill= 'X';
	stack_string s;
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
	BOOST_CHECK_EQUAL(s[0], fill);
}

BOOST_AUTO_TEST_CASE(push_back) {
	const stack_string::value_type fill= 'X';
	stack_string s;
	s.push_back(fill);
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type(1));
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK_EQUAL(s[0], fill);
}

BOOST_AUTO_TEST_CASE(insert_nullptr) {
	stack_string s1, s2;
	const stack_string::iterator i= s1.insert(s1.end(), s2.begin(), s2.begin());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(i, s1.end());
}

BOOST_AUTO_TEST_CASE(erase) {
	stack_string s;
	const stack_string::iterator i= s.erase(s.begin(), s.end());
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(i, s.end());
}

BOOST_AUTO_TEST_CASE(replace_dest_empty) {
	stack_string s1, s2;
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.end());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(replace_src_empty) {
	stack_string s1, s2;
	s1.replace(s1.begin(), s1.end(), s2.begin(), s2.begin());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(replace_both_empty) {
	stack_string s1, s2;
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.begin());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(hash) {
	stack_string s;
	BOOST_CHECK_EQUAL(std::hash<stack_string>()(s), std::size_t());
}

BOOST_AUTO_TEST_CASE(insert) {
	stack_string s;
	std::stringstream ss;
	ss << s;
	BOOST_CHECK_EQUAL(ss.str(), std::string(""));
}

BOOST_AUTO_TEST_CASE(extract) {
	stack_string s;
	std::stringstream ss;
	ss >> s;
	BOOST_CHECK(s.empty());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
