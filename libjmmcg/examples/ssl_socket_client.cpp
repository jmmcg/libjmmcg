/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/mpl/list.hpp>
#include <boost/test/included/unit_test.hpp>

#include "core/ssl_socket_wrapper_glibc.hpp"

using namespace libjmmcg;

using lock_t= ppd::api_lock_traits<ppd::generic_traits::api_type::posix_pthreads, ppd::heavyweight_threading>::critical_section_type::lock_type;

using client_types= boost::mpl::list<
	socket::glibc::client::ssl::wrapper<lock_t>>;

BOOST_AUTO_TEST_SUITE(socket_tests)

BOOST_AUTO_TEST_SUITE(ssl)

BOOST_AUTO_TEST_SUITE(client)

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, client_t, client_types) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] client_t client{});
}

BOOST_AUTO_TEST_SUITE(no_SSL_verify)

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_SSL, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::none);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(client.is_open());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_HTTP, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::none);
	BOOST_CHECK_THROW(client.connect("duckduckgo.com", "80"), std::exception);
	BOOST_CHECK(!client.is_open());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_SSL_send_query_read_any_response, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::none);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	const std::array<char, 6> message{"fubar"};
	BOOST_CHECK_NO_THROW(client.write(message));
	char response[1024]{};
	BOOST_CHECK_NO_THROW([[maybe_unused]] auto ret= client.read(response));
	const std::string response_str(response, std::find(std::begin(response), std::end(response), '\0'));
	BOOST_CHECK(!response_str.empty());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reconnect_duckduckgo_SSL, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::none);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(client.is_open());
	BOOST_CHECK_NO_THROW(client.close());
	BOOST_CHECK(!client.is_open());
	BOOST_CHECK_NO_THROW(client.connect("google.co.uk", "443"));
	BOOST_CHECK(client.is_open());
	BOOST_CHECK_NO_THROW(client.close());
	BOOST_CHECK(!client.is_open());
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(client.is_open());
	BOOST_CHECK_NO_THROW(client.close());
	BOOST_CHECK(!client.is_open());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(SSL_verify)

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_SSL, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::peer);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(!client.local_ip().empty());
	BOOST_CHECK(client.is_open());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_SSL_send_query_read_any_response, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::peer);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(!client.local_ip().empty());
	const std::array<char, 6> message{"fubar"};
	BOOST_CHECK_NO_THROW(client.write(message));
	{
		char response[1024]{};
		BOOST_CHECK_NO_THROW([[maybe_unused]] auto ret= client.read(response));
		std::string const response_str(response, std::find(std::begin(response), std::end(response), '\0'));
		BOOST_CHECK(!response_str.empty());
	}
	BOOST_CHECK_NO_THROW(client.close());
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK(!client.local_ip().empty());
	BOOST_CHECK_NO_THROW(client.write(message));
	{
		char response[1024]{};
		BOOST_CHECK_NO_THROW([[maybe_unused]] auto ret= client.read(response));
		std::string const response_str(std::string(response, std::find(std::begin(response), std::end(response), '\0')));
		BOOST_CHECK(!response_str.empty());
	}
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_duckduckgo_SSL_send_query_read_responses_in_thread, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::peer);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	BOOST_CHECK_NO_THROW(client.set_options(0, 1024, std::chrono::seconds(1), socket::socket_priority::high, 0));
	auto read_message= [&client](std::stop_token const& stop) {
		char response[1024]{};
		while(!stop.stop_requested()) {
			BOOST_CHECK_NO_THROW([[maybe_unused]] auto const ret= client.read(response));
		}
	};
	std::jthread thread(read_message);
	std::this_thread::sleep_for(std::chrono::seconds(1));
	BOOST_CHECK_NO_THROW(client.close());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reconnect_duckduckgo_SSL_send_query_read_responses_in_thread, client_t, client_types) {
	client_t client(client_t::domain_t::ip_v4, client_t::verify_SSL_certificates::peer);
	BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
	auto read_message= [&client](std::stop_token const& stop) {
		char response[1024]{};
		while(!stop.stop_requested()) {
			BOOST_CHECK_NO_THROW([[maybe_unused]] auto const ret= client.read(response));
		}
	};
	{
		std::jthread thread(read_message);
		std::this_thread::sleep_for(std::chrono::seconds(1));
		BOOST_CHECK_NO_THROW(client.close());
	}
	{
		std::jthread thread(read_message);
		BOOST_CHECK_NO_THROW(client.connect("duckduckgo.com", "443"));
		std::this_thread::sleep_for(std::chrono::seconds(1));
		BOOST_CHECK_NO_THROW(client.close());
	}
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
