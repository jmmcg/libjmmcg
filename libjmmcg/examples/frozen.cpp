/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "frozen/unordered_map.h"

#include <cstdint>

using namespace libjmmcg;

using key_type= std::uint32_t;

struct uint32_hash {
	constexpr std::size_t operator()(key_type k) const noexcept {
		return k;
	}

	constexpr std::size_t operator()(key_type k, std::size_t r) const noexcept {
		return k ^ r;
	}
};

BOOST_AUTO_TEST_SUITE(frozen_tests)

BOOST_AUTO_TEST_CASE(ctor) {
	using collection_type= frozen::unordered_map<
		std::uint32_t,
		unsigned,
		7 /*,
		 uint32_hash*/
		>;
	collection_type colln({
		{1094800461, 0},
		{1163086424, 1},
		{1313819736, 2},
		{1280528216, 3},
		{1481724500, 4},
		{1163149634, 5},
		{1398030658, 6},
	});
	BOOST_CHECK_EQUAL(colln.size(), 7);
}

BOOST_AUTO_TEST_SUITE_END()
