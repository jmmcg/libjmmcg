/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/optimizing_fifo_flyback_buffer.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(optimizing_fifo_flyback_buffer_tests)

BOOST_AUTO_TEST_SUITE(generator)

BOOST_AUTO_TEST_CASE(ctor) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.capacity(), 4);
	BOOST_CHECK_EQUAL(buffer.max_capacity(), 6);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
}

BOOST_AUTO_TEST_CASE(generate_n_nothing) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 0;
	}),
		0);
	BOOST_CHECK(buffer.empty());
}

BOOST_AUTO_TEST_CASE(generate_n_1) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		return 1;
	}),
		1);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 1);
	BOOST_CHECK_EQUAL(buffer.capacity(), 4);
}

BOOST_AUTO_TEST_CASE(generate_n_2) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		return 2;
	}),
		2);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 2);
	BOOST_CHECK_EQUAL(buffer.capacity(), 4);
}

BOOST_AUTO_TEST_CASE(generate_n_4) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		begin[2]= '2';
		begin[3]= '3';
		return 4;
	}),
		4);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 4);
	BOOST_CHECK_EQUAL(buffer.capacity(), 4);
}

BOOST_AUTO_TEST_CASE(generate_n_5_cause_resize_to_max_capacity) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		begin[2]= '2';
		begin[3]= '3';
		return 4;
	}),
		4);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 4);
	BOOST_CHECK_EQUAL(buffer.capacity(), 4);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[4]= '4';
		return 1;
	}),
		1);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 5);
	BOOST_CHECK_EQUAL(buffer.capacity(), buffer.max_capacity());
}

BOOST_AUTO_TEST_CASE(generate_n_7_cause_resize_to_fail) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 4;
	}),
		4);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[4]= '4';
		return 2;
	}),
		2);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 6);
	BOOST_CHECK_EQUAL(buffer.capacity(), buffer.max_capacity());
	BOOST_CHECK_THROW(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 4;
	}),
		std::exception);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(consumer)

BOOST_AUTO_TEST_CASE(generate_1_pop_front_0) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 0;
	}),
		0);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 1);
}

BOOST_AUTO_TEST_CASE(generate_1_pop_front_1_and_more) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		return 1;
	}),
		1);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 1;
	}),
		0);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		return 1;
	}),
		1);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 1);
	BOOST_CHECK_THROW(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		return 2;
	}),
		std::exception);
}

BOOST_AUTO_TEST_CASE(make_consumer_claim_it_reads_excess) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		begin[2]= '2';
		return 3;
	}),
		3);
	BOOST_CHECK_EQUAL(buffer.size(), 3);
	BOOST_CHECK_THROW(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		BOOST_CHECK_EQUAL(begin[1], '1');
		BOOST_CHECK_EQUAL(begin[2], '2');
		return 4;
	}),
		std::exception);
	BOOST_CHECK_EQUAL(buffer.size(), 3);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		BOOST_CHECK_EQUAL(begin[1], '1');
		BOOST_CHECK_EQUAL(begin[2], '2');
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.size(), 2);
	BOOST_CHECK_THROW(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '1');
		BOOST_CHECK_EQUAL(begin[1], '2');
		throw std::runtime_error("TEST");
		return 1;
	}),
		std::exception);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
}

BOOST_AUTO_TEST_CASE(generate_3_pop_front_1_generate_n_check_data_moved_left) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		begin[2]= '2';
		return 3;
	}),
		3);
	BOOST_CHECK_EQUAL(buffer.size(), 3);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		BOOST_CHECK_EQUAL(begin[1], '1');
		BOOST_CHECK_EQUAL(begin[2], '2');
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.size(), 2);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '3';
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.size(), 3);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '4';
		return 1;
	}),
		1);
	BOOST_CHECK_EQUAL(buffer.size(), 4);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '1');
		BOOST_CHECK_EQUAL(begin[1], '2');
		BOOST_CHECK_EQUAL(begin[2], '3');
		BOOST_CHECK_EQUAL(begin[3], '4');
		return 4;
	}),
		4);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '5';
		return 1;
	}),
		1);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 1);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '5');
		return 1;
	}),
		1);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
}

BOOST_AUTO_TEST_CASE(generate_2_pop_front_throws_reset_buffer) {
	optimizing_fifo_flyback_buffer buffer(4, 6);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		begin[1]= '1';
		return 2;
	}),
		2);
	BOOST_CHECK_THROW(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		BOOST_CHECK_EQUAL(begin[1], '1');
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>("Testing bad consumer."));
		return 2;
	}),
		std::exception);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
	BOOST_CHECK_EQUAL(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator, optimizing_fifo_flyback_buffer::const_iterator) {
		return 1;
	}),
		0);
	BOOST_CHECK(buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 0);
	BOOST_CHECK_EQUAL(buffer.generate_n([](optimizing_fifo_flyback_buffer::iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		begin[0]= '0';
		return 1;
	}),
		1);
	BOOST_CHECK(!buffer.empty());
	BOOST_CHECK_EQUAL(buffer.size(), 1);
	BOOST_CHECK_THROW(buffer.pop_front([](optimizing_fifo_flyback_buffer::const_iterator begin, optimizing_fifo_flyback_buffer::const_iterator) {
		BOOST_CHECK_EQUAL(begin[0], '0');
		return 2;
	}),
		std::exception);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
