/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/factoring.hpp"

using namespace libjmmcg;

namespace std {

std::ostream&
operator<<(std::ostream& os, factoring::collection_type const& c) noexcept(false) {
	std::copy(c.begin(), c.end(), std::ostream_iterator<factoring::collection_type::value_type>(os, ", "));
	return os;
}

}

BOOST_AUTO_TEST_SUITE(factoring_tests)

BOOST_AUTO_TEST_SUITE(division)

BOOST_AUTO_TEST_CASE(check_values) {
	BOOST_CHECK_EQUAL(factoring::division(1), factoring::collection_type{});
	BOOST_CHECK_EQUAL(factoring::division(2), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::division(3), factoring::collection_type{3});
	BOOST_CHECK_EQUAL(factoring::division(4), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::division(5), factoring::collection_type{5});
	BOOST_CHECK_EQUAL(factoring::division(6), (factoring::collection_type{2, 3}));
	BOOST_CHECK_EQUAL(factoring::division(7), factoring::collection_type{7});
	BOOST_CHECK_EQUAL(factoring::division(8), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::division(9), factoring::collection_type{3});
	BOOST_CHECK_EQUAL(factoring::division(10), (factoring::collection_type{2, 5}));
	BOOST_CHECK_EQUAL(factoring::division(24), (factoring::collection_type{2, 3}));
	BOOST_CHECK_EQUAL(factoring::division(30), (factoring::collection_type{2, 3, 5}));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(monte_carlo)
/* TODO
BOOST_AUTO_TEST_CASE(check_values) {
	BOOST_CHECK_EQUAL(factoring::monte_carlo(1), factoring::collection_type{});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(2), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(3), factoring::collection_type{3});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(4), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(5), factoring::collection_type{5});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(6), (factoring::collection_type{2, 3}));
	BOOST_CHECK_EQUAL(factoring::monte_carlo(7), factoring::collection_type{7});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(8), factoring::collection_type{2});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(9), factoring::collection_type{3});
	BOOST_CHECK_EQUAL(factoring::monte_carlo(10), (factoring::collection_type{2, 5}));
	BOOST_CHECK_EQUAL(factoring::monte_carlo(24), (factoring::collection_type{2, 3}));
	BOOST_CHECK_EQUAL(factoring::monte_carlo(30), (factoring::collection_type{2, 3, 5}));
}
*/
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
