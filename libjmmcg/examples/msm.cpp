/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/msm.hpp"

using namespace libjmmcg;

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 * Note that this happens to be the enum tag-values of states.
 *
 * \see states
 */
template<>
struct perfect_hash<0U> {
	using key_type= unsigned;
	using element_type= std::uint32_t;

	enum : std::size_t {
		mask= 0xFFFF,
		denominator= 1
	};

	REALLY_FORCE_INLINE static constexpr std::size_t result(key_type v) noexcept(true) {
		return static_cast<element_type>(v & mask) % denominator;	// To better represent the implementation of the MIT variant.
	}
};

/**
 * Note that this happens to be the enum tag-values of states.
 *
 * \see states
 */
template<>
struct perfect_hash<0U, 1U, 2U> {
	using key_type= unsigned;
	using element_type= std::uint32_t;

	enum : std::size_t {
		mask= 0xFFFF,
		denominator= 3
	};

	REALLY_FORCE_INLINE static constexpr std::size_t result(key_type v) noexcept(true) {
		return static_cast<element_type>(v & mask) % denominator;	// To better represent the implementation of the MIT variant.
	}
};

}}

enum class states {
	start,
	middle,
	middle1,
	end,
	unknown
};

std::ostream&
operator<<(std::ostream& os, states const s) noexcept(false) {
	os << static_cast<std::underlying_type<states>::type>(s);
	return os;
}

struct data {
	states state{states::unknown};
};

struct data_with_payload {
	states state{states::unknown};
	unsigned j{68};
};

using msm_types= boost::mpl::list<
	msm::unroll,
	msm::jump_table,
	// TODO	msm::computed_goto,
	msm::hash>;

template<class MSMT>
struct noop_driver {
	struct no_op {
		template<class... Args>
		explicit no_op(Args&&...) noexcept(true) {}

		template<class... Args>
		constexpr auto process(Args&&...) const noexcept(true) {}
	};

	struct fn_event {
		using arguments_types= std::pair<
			states,
			std::tuple<
				std::tuple<no_op&> > >;
		using arguments_type= std::tuple_element<0, typename arguments_types::second_type>::type;
		using argument_type= typename std::tuple_element<0, arguments_type>::type;

		template<auto state, auto next>
		auto process(argument_type& p) const noexcept(true) {
			p.process();
			return next;
		}

		template<auto state, auto next, class Params>
		auto process(Params& p) const noexcept(true) {
			p.process();
			return next;
		}
	};

	struct state_machine_t final : MSMT::template state_transition_table<state_machine_t> {
		using base_t= typename MSMT::template state_transition_table<state_machine_t>;
		using row_t= typename MSMT::template row_types<states, states>;
		using transition_table= typename base_t::template rows<
			typename row_t::template row<states::start, fn_event, states::end> >;
	};

	using machine= typename MSMT::template machine<state_machine_t>;

	void process(states state) noexcept(false) {
		auto fn= no_op();
		msm.process(state, fn);
	}

	machine msm{};
};

template<class MSMT>
struct assign_driver_states {
	struct fn_event {
		using arguments_types= std::pair<
			states,
			std::tuple<
				std::tuple<data_with_payload&> > >;
		using arguments_type= std::tuple_element<0, typename arguments_types::second_type>::type;
		using argument_type= typename std::tuple_element<0, arguments_type>::type;

		explicit fn_event(unsigned& j)
			: j_(j) {}

		template<auto state, states next>
		auto process(argument_type& p) const noexcept(true) {
			p.state= next;
			p.j+= j_;
			return next;
		}

		template<auto state, auto next, class Params>
		auto process(Params& p) const noexcept(true) {
			p.state= next;
			p.j+= j_;
			return next;
		}

	private:
		unsigned& j_;
	};

	struct state_machine_t final : MSMT::template state_transition_table<state_machine_t> {
		using base_t= typename MSMT::template state_transition_table<state_machine_t>;
		using row_t= typename MSMT::template row_types<states, states>;
		using transition_table= typename base_t::template rows<
			typename row_t::template row<states::start, fn_event, states::middle>,
			typename row_t::template row<states::middle, fn_event, states::middle1>,
			typename row_t::template row<states::middle1, fn_event, states::end> >;
	};

	using machine= typename MSMT::template machine<state_machine_t>;

	assign_driver_states()
		: j(42), msm(j, j, j) {
	}

	void process(states state, data_with_payload& p) noexcept(false) {
		msm.process(state, p);
	}

	unsigned j;
	machine msm;
};

template<class MSMT>
struct assign_driver {
	struct assign_event {
		using arguments_types= std::pair<
			states,
			std::tuple<
				std::tuple<data&> > >;
		using arguments_type= std::tuple_element<0, typename arguments_types::second_type>::type;
		using argument_type= typename std::tuple_element<0, arguments_type>::type;

		template<auto state, states next>
		auto process(argument_type& p) const noexcept(true) {
			p.state= next;
			return next;
		}

		template<auto state, auto next, class Params>
		auto process(Params& p) const noexcept(true) {
			p.state= next;
			return next;
		}
	};

	struct is_true {
		template<auto state, auto next, class Params>
		bool process(Params& p) const noexcept(true) {
			return p.check;
		}
	};

	struct state_machine_t final : MSMT::template state_transition_table<state_machine_t> {
		using base_t= typename MSMT::template state_transition_table<state_machine_t>;
		using row_t= typename MSMT::template row_types<states, states>;
		using transition_table= typename base_t::template rows<
			typename row_t::template row<states::start, assign_event, states::middle>,
			typename row_t::template row<states::middle, assign_event, states::middle1>,
			typename row_t::template row<states::middle1, assign_event, states::end> >;
	};

	using machine= typename MSMT::template machine<state_machine_t>;

	void process(states state, data& p) noexcept(false) {
		msm.process(state, p);
	}

	machine msm{};
};

BOOST_AUTO_TEST_SUITE(msm_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(noop_driver_ctor, msm_type, msm_types) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] const noop_driver<msm_type> msm);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_noop_driver, msm_type, msm_types) {
	noop_driver<msm_type> msm;
	BOOST_CHECK_NO_THROW(msm.process(states::start));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assign_driver_states_ctor, msm_type, msm_types) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] const assign_driver_states<msm_type> msm);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_assign_driver_states, msm_type, msm_types) {
	assign_driver_states<msm_type> msm;
	data_with_payload d;
	BOOST_CHECK_NO_THROW(msm.process(states::start, d));
	BOOST_CHECK_EQUAL(d.state, states::middle);
	BOOST_CHECK_EQUAL(d.j, 68 + 42);
	BOOST_CHECK_NO_THROW(msm.process(states::middle, d));
	BOOST_CHECK_EQUAL(d.state, states::middle1);
	BOOST_CHECK_EQUAL(d.j, 68 + 42 + 42);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assign_driver_ctor, msm_type, msm_types) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] const assign_driver<msm_type> msm);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_assign_event, msm_type, msm_types) {
	assign_driver<msm_type> msm;
	data d;
	BOOST_CHECK_NO_THROW(msm.process(states::start, d));
	BOOST_CHECK_EQUAL(d.state, states::middle);
	BOOST_CHECK_NO_THROW(msm.process(states::middle, d));
	BOOST_CHECK_EQUAL(d.state, states::middle1);
	BOOST_CHECK_NO_THROW(msm.process(states::middle1, d));
	BOOST_CHECK_EQUAL(d.state, states::end);
}

BOOST_AUTO_TEST_SUITE_END()
