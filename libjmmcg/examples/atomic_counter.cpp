/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_api_traits.hpp"

using namespace libjmmcg;
using namespace ppd;

typedef boost::mpl::list<
	api_lock_traits<platform_api, sequential_mode>::atomic_counter_type<long>,
	api_lock_traits<platform_api, heavyweight_threading>::atomic_counter_type<long>,
	api_lock_traits<platform_api, heavyweight_threading>::atomic_counter_type<double> >
	ctr_types;

BOOST_AUTO_TEST_SUITE(atomic_counter_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, ctr_t, ctr_types) {
	ctr_t a;
	BOOST_CHECK_EQUAL(a.get(), 0);
	BOOST_CHECK(!a);
	BOOST_CHECK(a == 0);
	BOOST_CHECK(a != 1);
	BOOST_CHECK(a < 1);
	BOOST_CHECK(a <= 1);
	BOOST_CHECK(a <= 0);
	BOOST_CHECK(a > -1);
	BOOST_CHECK(a >= -1);
	BOOST_CHECK(a >= 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, ctr_t, ctr_types) {
	ctr_t a(1);
	BOOST_CHECK_EQUAL(a.get(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor, ctr_t, ctr_types) {
	ctr_t a(1);
	ctr_t b(a);
	BOOST_CHECK_EQUAL(a.get(), b.get());
	BOOST_CHECK(a == b);
	BOOST_CHECK(!(a != b));
	BOOST_CHECK(!(a < b));
	BOOST_CHECK(a <= b);
	BOOST_CHECK(!(a > b));
	BOOST_CHECK(a >= b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assign, ctr_t, ctr_types) {
	ctr_t a(1), b;
	b= a;
	BOOST_CHECK_EQUAL(a.get(), b.get());
	BOOST_CHECK(a == b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(increment, ctr_t, ctr_types) {
	ctr_t a(1);
	++a;
	BOOST_CHECK_EQUAL(a.get(), 2);
	BOOST_CHECK(a == 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(postincrement, ctr_t, ctr_types) {
	ctr_t a(1);
	ctr_t b(a++);
	BOOST_CHECK_EQUAL(a.get(), 2);
	BOOST_CHECK_EQUAL(b.get(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(decrement, ctr_t, ctr_types) {
	ctr_t a(1);
	--a;
	BOOST_CHECK_EQUAL(a.get(), 0);
	BOOST_CHECK(a == 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(postdecrement, ctr_t, ctr_types) {
	ctr_t a(1);
	ctr_t b(a--);
	BOOST_CHECK_EQUAL(a.get(), 0);
	BOOST_CHECK_EQUAL(b.get(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one, ctr_t, ctr_types) {
	ctr_t a(1);
	a+= 1;
	BOOST_CHECK_EQUAL(a.get(), 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(subtract_one, ctr_t, ctr_types) {
	ctr_t a(1);
	a-= 1;
	BOOST_CHECK_EQUAL(a.get(), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(apply_plus, ctr_t, ctr_types) {
	ctr_t a(1);
	BOOST_CHECK_EQUAL(a.apply(1, std::plus<typename ctr_t::value_type>()), 2);
	BOOST_CHECK_EQUAL(a.get(), 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(apply_minus, ctr_t, ctr_types) {
	ctr_t a(1);
	BOOST_CHECK_EQUAL(a.apply(1, std::minus<typename ctr_t::value_type>()), 0);
	BOOST_CHECK_EQUAL(a.get(), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(apply_multiply, ctr_t, ctr_types) {
	ctr_t a(6);
	BOOST_CHECK_EQUAL(a.apply(7, std::multiplies<typename ctr_t::value_type>()), 42);
	BOOST_CHECK_EQUAL(a.get(), 42);
}

BOOST_AUTO_TEST_SUITE_END()
