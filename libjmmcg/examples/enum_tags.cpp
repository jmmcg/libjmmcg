/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/enum_as_char_array.hpp"

using namespace libjmmcg;
using namespace enum_tags::mpl;

enum class enum_tags_as_strs : std::uint32_t {
	e5= to_tag<'9'>::value,
	e4= to_tag<'1', '2', '3', '4'>::value,
	e3= to_tag<'3', '1', '4', '2'>::value,
	e2= to_tag<'3', '1', '0', '0'>::value,
	e1= to_tag<'3', '0', '0', '0'>::value
};

static char const str3000[]= "3000";
static char const str1234[]= "1234";
static std::array<char, sizeof(enum_tags_as_strs)> str3142= {'3', '1', '4', '2'};

BOOST_AUTO_TEST_SUITE(enum_as_char_array)

BOOST_AUTO_TEST_CASE(dynamic_string_literal_conversiom) {
	BOOST_CHECK(enum_tags::convert<enum_tags_as_strs>("9") == enum_tags_as_strs::e5);
	// Slowest.
	BOOST_CHECK_EQUAL(str3000, std::string(to_array<enum_tags_as_strs::e1>::value_no_null.begin(), to_array<enum_tags_as_strs::e1>::value_no_null.end()));
	// Slow.
	BOOST_CHECK(strcmp(str3000, to_array<enum_tags_as_strs::e1>::value.begin()) == 0);
	const auto t3= enum_tags::convert<enum_tags_as_strs>(str3000);
	// Better...
	BOOST_CHECK_EQUAL(static_cast<std::uint64_t>(t3), static_cast<std::uint64_t>(enum_tags_as_strs::e1));
	// Best!
	BOOST_CHECK(t3 == enum_tags_as_strs::e1);
	const auto t1234= enum_tags::convert<enum_tags_as_strs>(str1234);
	BOOST_CHECK(t1234 == enum_tags_as_strs::e4);
	const auto t3142= enum_tags::convert<enum_tags_as_strs>(str3142);
	// Super-good!
	BOOST_CHECK(t3142 == enum_tags_as_strs::e3);
}

BOOST_AUTO_TEST_SUITE_END()
