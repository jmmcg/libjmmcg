/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"
#include "core/ttypes.hpp"

#include <boost/lexical_cast.hpp>

#include <charconv>
#include <chrono>

using namespace libjmmcg;

using timed_results_t=ave_deviation_meter<unsigned long long>;

ALIGN_TO_L1_CACHE static constexpr char const * const numbers[]={
	"572489496",
	"45643348",
	"51453335",
	"9978974234",
	"11447",
	"16836812",
	"15681223211",
	"125",
	"9845631699",
	"4598888",
	"0",
	"1",
	"1234567890123456789",
	"4581123500063313800"
};

struct std_atol {
	static long result(char const * const s, std::size_t) noexcept(true) {
		DEBUG_ASSERT(s);
		return std::atol(s);
	}
};

struct std_strtol {
	static long result(char const * const s, std::size_t) noexcept(true) {
		DEBUG_ASSERT(s);
		return std::strtol(s, nullptr, 10);
	}
};

struct std_from_chars {
	static long result(char const * const s, std::size_t sz) noexcept(true) {
		DEBUG_ASSERT(s);
		long res{};
		std::from_chars(s, s+sz, res, 10);
		return res;
	}
};

struct std_to_chars {
	static std::size_t result(long v, char * const s, std::size_t sz) noexcept(true) {
		DEBUG_ASSERT(s);
		std::to_chars(s, s+sz, v, 10);
		return 42;
	}
};

template<class V>
struct test_from_string_baseline {
	[[gnu::pure]] static V result(char const * const s, std::size_t sz) noexcept(true) {
		DEBUG_ASSERT(s);
		return fromstring<V>(s, sz);
	}
};

struct test_folly_ascii_to_int {
	[[gnu::pure]] static long result(char const * const s, std::size_t sz) noexcept(true) {
		DEBUG_ASSERT(s);
		DEBUG_ASSERT(sz<=std::numeric_limits<std::uint32_t>::max());
		return folly_ascii_to_int(s, static_cast<std::uint32_t>(sz));
	}
};

template<class V>
struct boost_lexical_cast_from {
	[[gnu::pure]] static V result(const char * const s, std::size_t) noexcept(true) {
		DEBUG_ASSERT(s);
		return boost::lexical_cast<V>(s);
	}
};

struct folly_uint64ToBufferUnsafe {
	[[gnu::pure]] static std::size_t result(long v, char * const s, std::size_t sz) noexcept(true) {
		DEBUG_ASSERT(s);
		return tostring(v, s, sz);
	}
};

using from_conversion_algorithms=boost::mpl::list<
	test_folly_ascii_to_int,
	test_from_string_baseline<unsigned long long>,
	test_from_string_baseline<double>,
	std_atol,
	boost_lexical_cast_from<long>,
	boost_lexical_cast_from<double>,
	std_strtol,
	std_from_chars
>;

using to_conversion_algorithms=boost::mpl::list<
	folly_uint64ToBufferUnsafe,
	std_to_chars
>;

BOOST_AUTO_TEST_SUITE(conversions_tests)

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("string_conversions_performance.csv"))

/**
	\test <a href="./examples/string_conversions_performance.svg">Graph</a> of performance results for string-to-long conversion algorithms.
			==========================================================================================
	Test the performance of various string-to-long conversion algorithms.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(from_string, algorithm, from_conversion_algorithms) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<25;
	const unsigned short loops_for_conv=4000;
#else
	const unsigned long test_size=2<<2;
	const unsigned short loops_for_conv=1;
#endif
	const double perc_conv_estimate=2.0;

	unsigned long res=0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&res]() {
			char const * const number=numbers[res%(sizeof(numbers)/sizeof(numbers[0]))];
			const std::size_t sz=std::strlen(number);
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long i=0; i<test_size; ++i) {
				res+=static_cast<unsigned long>(algorithm::result(number, sz));
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(test_size)*1000000.0/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()));
		}
	));
	BOOST_CHECK_GT(res, 0);
	std::cout<<typeid(algorithm).name()<<" conversions/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/string_conversions_performance.svg">Graph</a> of performance results for long-to-string conversion algorithms.
			==========================================================================================
	Test the performance of various long-to-string conversion algorithms.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(to_string, algorithm, to_conversion_algorithms) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<25;
	const unsigned short loops_for_conv=4000;
#else
	const unsigned long test_size=2<<2;
	const unsigned short loops_for_conv=1;
#endif
	const double perc_conv_estimate=5.0;

	std::size_t res=0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&res]() {
			ALIGN_TO_L1_CACHE std::array<char, 512> buff={};
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long i=0; i<test_size; ++i) {
				res+=algorithm::result(i, buff.begin(), buff.max_size());
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(test_size)*1000000.0/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()));
		}
	));
	BOOST_CHECK_GT(res, 0);
	std::cout<<typeid(algorithm).name()<<" conversions/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
