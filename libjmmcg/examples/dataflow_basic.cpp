/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

#include <boost/bind/bind.hpp>

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct fifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename fifo_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type fifo_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct lifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename lifo_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type lifo_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct lifo_lockfree_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo_lockfree,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename lifo_lockfree_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type lifo_lockfree_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct priority_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1, 2>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO - locks up...	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1>,
	// TODO - locks up...	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 1, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,

	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 2>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	// TODO - locks up...	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 2>,
	// TODO - locks up...	lifo_lockfree_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2> >
	finite_test_types;

typedef boost::mpl::list<
	// TODO not yet finished coding these types!	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading>,
	// TODO not yet finished coding these types!	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	// TODO not yet finished coding these types!	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO not yet finished coding these types!	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	// TODO not yet finished coding these types!	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	// TODO not yet finished coding these types!	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::nonjoinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	// TODO not yet finished coding these types!	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading>,
	// TODO not yet finished coding these types!	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 0, 2>,
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 0, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::nonjoinable, heavyweight_threading, 0, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading> >
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

struct res_t {
	long i;
};

struct work_type {
	typedef res_t result_type;

	int i_;

	explicit work_type(const int i)
		: i_(i) {
	}

	void __fastcall process(result_type& r) {
		r.i= i_ << 1;
	}

	void __fastcall mutate(result_type& r) {
		process(r);
	}

	bool __fastcall operator<(work_type const&) const noexcept(true) {
		return true;
	}
};

struct work_type_simple {
	int i_;

	explicit work_type_simple(const int i)
		: i_(i) {
	}

	void __fastcall process(res_t& r) {
		r.i= i_ << 1;
	}

	void __fastcall mutate(res_t& r) {
		process(r);
	}

	res_t __fastcall exec() {
		res_t r;
		process(r);
		return r;
	}

	bool __fastcall operator<(work_type_simple const&) const noexcept(true) {
		return true;
	}
};

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(all_joinable)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_CASE_TEMPLATE(one_thread, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(1);	// TODO should use T::pool_size.
	BOOST_CHECK_EQUAL(pool.pool_size(), 1U);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(n_threads, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << work_type_simple(1);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_boost_bind, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << boost::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_std_bind, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << std::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_time_critical_simple, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;
	typedef typename pool_type::template priority<pool_type::api_params_type::time_critical> time_critical;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << time_critical() << work_type_simple(1);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << work_type(1);
	pool << nonjoinable() << work_type(2);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work_atonce, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool(T::pool_size);
	pool << nonjoinable() << work_type_simple(1) << nonjoinable() << work_type_simple(3);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool;
	pool << nonjoinable() << work_type_simple(1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_boost_bind, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool;
	pool << nonjoinable() << boost::bind(&work_type_simple::exec, work_type_simple(1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_std_bind, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool;
	pool << nonjoinable() << std::bind(&work_type_simple::exec, work_type_simple(1));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_time_critical, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;
	typedef typename pool_type::template priority<pool_type::api_params_type::time_critical> time_critical;

	pool_type pool;
	pool << nonjoinable() << time_critical() << work_type(1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool;
	pool << nonjoinable() << work_type(1);
	pool << nonjoinable() << work_type(2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work_atonce, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;

	pool_type pool;
	pool << nonjoinable() << work_type_simple(1) << nonjoinable() << work_type_simple(3);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
