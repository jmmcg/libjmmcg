/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

#include <boost/graph/graphviz.hpp>
#include <boost/stacktrace.hpp>

#include <chrono>
#include <random>

using namespace libjmmcg;
using namespace ppd;

using timed_results_t= ave_deviation_meter<double>;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_lifo_lockfree_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo_lockfree,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct crew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

/**
	\todo Does not seem to be perf tested...?
*/
typedef boost::mpl::list<
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	//	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 4>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 8>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12> >
	finite_fifo_test_types;

typedef boost::mpl::list<
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 4>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 8>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>	 //,
	//	crew_normal_lifo_lockfree_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>
	//	crew_normal_lifo_lockfree_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>
	>
	finite_lifo_test_types;

void
gen_bt_terminate() {
	auto const& bt= boost::stacktrace::stacktrace();
	std::cerr << bt << std::endl;
}

namespace __cxxabiv1 {
extern "C" void
__cxa_pure_virtual(void) {
	gen_bt_terminate();
	std::terminate();
}
}

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable_dataflow)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(n_elements)

/**
	\test <a href="./examples/dataflow_full_algos_performance_accumulate.svg">Graph</a> of scalability of parallel accumulate.
			===================================
	For 2<<10 items. Note that we observe anti-scaling, as there are simply not enough items in the collection to offset the costs of threading.
	-# Results:
		-#	Build 1405:
			- Pool=0, items=1024, time taken: [0, 1.35999e-09 ~(+/-2.8e+02%), 3.1e-05], samples=100001, total=0.00014 sec.
			- Pool=1, items=1024, time taken: [1e-06, 1.01439e-06 ~(+/-10%), 2.5e-05], samples=5002, total=0.0051 sec.
			- Pool=2, items=1024, time taken: [2.9e-05, 9.26648e-05 ~(+/-10%), 0.00098], samples=13838, total=1.3 sec.
			- Pool=4, items=1024, time taken: [0.000136, 0.00021595 ~(+/-9.8%), 0.0003], samples=60, total=0.013 sec.
			- Pool=8, items=1024, time taken: [0.000272, 0.000470489 ~(+/-10%), 0.0008], samples=47, total=0.022 sec.
			- Pool=12, items=1024, time taken: [0.000419, 0.000632359 ~(+/-9.8%), 0.0015], samples=64, total=0.04 sec.
		-#	Build 1469:
			- Pool=0, items=1024, time taken: [0, 2.851e-10 ~(+/-2.2e+02%), 4e-05], samples=10000001, total=0.0029 sec.
			- Pool=1, items=1024, time taken: [1e-06, 1.00901e-06 ~(+/-10%), 4.6e-05], samples=488923, total=0.49 sec.
			- Pool=2, items=1024, time taken: [8e-06, 0.000108706 ~(+/-12%), 0.0032], samples=10000001, total=1.1e+03 sec.
			- Pool=4, items=1024, time taken: [7.8e-05, 0.000269517 ~(+/-10%), 0.00054], samples=675, total=0.18 sec.
			- Pool=8, items=1024, time taken: [0.000282, 0.000469618 ~(+/-10%), 0.0017], samples=304, total=0.14 sec.
			- Pool=12, items=1024, time taken: [0.000321, 0.000540948 ~(+/-10%), 0.00077], samples=192, total=0.1 sec.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, finite_lifo_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 10;
	const unsigned long num_reps= 10000000;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
#endif
	std::set_terminate(&gen_bt_terminate);
	pool_type pool(T::pool_size);
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	vtr_colln_t v;
	v.reserve(test_size);
	std::mt19937_64 gen(42);
	std::uniform_int_distribution<typename vtr_colln_t::value_type> distribution(0, std::numeric_limits<typename vtr_colln_t::value_type>::max() / (test_size + 1));
	std::generate_n(std::back_inserter(v.colln()), test_size, std::bind(distribution, gen));
	v.sync_size();
	const typename vtr_colln_t::value_type res_chk= std::accumulate(v.colln().begin(), v.colln().end(), typename vtr_colln_t::value_type());
	std::cout << ", items=" << v.size() << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		10.0,
		num_reps,
		[&pool, res_chk, &v]() {
			typedef typename pool_type::joinable joinable;

			typename vtr_colln_t::value_type res;
			const auto t1= std::chrono::high_resolution_clock::now();
			auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
			res= *context;
			const auto t2= std::chrono::high_resolution_clock::now();
			BOOST_CHECK_EQUAL(res, res_chk);
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / 1000000;
		}));
	std::cout << ", time taken: " << timed_results.first << " sec." << std::endl;
	std::cout << pool.statistics() << std::endl;
	std::ofstream ofs("accumulate.dot");
	pool.cfg().write_graphviz(ofs);
#ifdef JMMCG_PERFORMANCE_TESTS
//	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/dataflow_full_algos_performance_merge.svg">Graph</a> of scalability of parallel merge.
			===================================
	For 2<<25 items.
	-# Results:
		-#	Build 1655:
			- Pool=0, items=1024, time taken=
			- Pool=1, items=1024, time taken=
			- Pool=2, items=1024, time taken=
			- Pool=4, items=1024, time taken=
			- Pool=8, items=1024, time taken=
			- Pool=12, items=1024, time taken=
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(merge, T, finite_lifo_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 25;
	const unsigned long num_reps= 2000;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
#endif
	pool_type pool(T::pool_size);
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	vtr_colln_t v, v1;
	v.reserve(test_size);
	v1.reserve(test_size);
	std::mt19937_64 gen(42);
	std::uniform_int_distribution<typename vtr_colln_t::value_type> distribution(0, std::numeric_limits<typename vtr_colln_t::value_type>::max() / test_size);
	std::generate_n(std::back_inserter(v.colln()), test_size, std::bind(distribution, gen));
	v.sync_size();
	std::sort(v.colln().begin(), v.colln().end());
	std::generate_n(std::back_inserter(v1.colln()), test_size, std::bind(distribution, gen));
	v1.sync_size();
	std::sort(v1.colln().begin(), v1.colln().end());
	vtr_colln_t v_out;
	// This causes the sequential checks to fail.
	v_out.colln().reserve(v.size() + v1.size());
	v_out.resize_noinit_nolk(v.size() + v1.size());
	vtr_colln_t v_chk;
	v_chk.colln().reserve(v.size() + v1.size());
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()));
	v_chk.sync_size();
	std::cout << ", items=" << (v.size() + v1.size()) << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		10.0,
		num_reps,
		[&pool, &v_chk, &v, &v1, &v_out]() {
			typedef typename pool_type::joinable joinable;

			const auto t1= std::chrono::high_resolution_clock::now();
			auto const& context= pool << joinable() << pool.merge(v, v1, v_out);
			*context;
			const auto t2= std::chrono::high_resolution_clock::now();
			BOOST_CHECK_EQUAL(v_out.size(), v_chk.size());
			BOOST_CHECK(v_out == v_chk);
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / 1000000;
		}));
	std::cout << ", time taken: " << timed_results.first << " sec." << std::endl;
	std::cout << pool.statistics() << std::endl;
	std::ofstream ofs("merge.dot");
	pool.cfg().write_graphviz(ofs);
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/dataflow_full_algos_performance_sort.svg">Graph</a> of scalability of parallel sort.
			===================================
	For 2<<25 items.
	-# Results:
		-#	Build 1655:
			- Pool=0, items=1024, time taken=
			- Pool=1, items=1024, time taken=
			- Pool=2, items=1024, time taken=
			- Pool=4, items=1024, time taken=
			- Pool=8, items=1024, time taken=
			- Pool=12, items=1024, time taken=
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(sort_unsorted, T, finite_lifo_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 25;
	const unsigned long num_reps= 2000;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
#endif
	pool_type pool(T::pool_size);
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	vtr_colln_t v;
	v.reserve(test_size);
	std::mt19937_64 gen(42);
	std::uniform_int_distribution<typename vtr_colln_t::value_type> distribution(0, std::numeric_limits<typename vtr_colln_t::value_type>::max() / test_size);
	std::generate_n(std::back_inserter(v.colln()), test_size, std::bind(distribution, gen));
	vtr_colln_t v_chk(v);
	v.sync_size();
	v_chk.sync_size();
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	std::cout << ", items=" << v.size() << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		10.0,
		num_reps,
		[&pool, &v_chk, &v]() {
			typedef typename pool_type::joinable joinable;

			const auto t1= std::chrono::high_resolution_clock::now();
			auto const& context= pool << joinable() << pool.sort(v);
			*context;
			const auto t2= std::chrono::high_resolution_clock::now();
			BOOST_CHECK(v == v_chk);
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()) / 1000000;
		}));
	std::cout << ", time taken: " << timed_results.first << " sec." << std::endl;
	std::cout << pool.statistics() << std::endl;
	std::ofstream ofs("sort.dot");
	pool.cfg().write_graphviz(ofs);
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
