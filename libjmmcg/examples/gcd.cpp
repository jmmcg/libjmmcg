/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/gcd.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(gcd_tests)

BOOST_AUTO_TEST_SUITE(use_euclid)

BOOST_AUTO_TEST_CASE(check_values) {
	BOOST_CHECK_EQUAL(euclid::gcd(1, 1), 1);
	BOOST_CHECK_EQUAL(euclid::gcd(2, 1), 1);
	BOOST_CHECK_EQUAL(euclid::gcd(1, 2), 1);
	BOOST_CHECK_EQUAL(euclid::gcd(3, 2), 1);
	BOOST_CHECK_EQUAL(euclid::gcd(2, 3), 1);
	BOOST_CHECK_EQUAL(euclid::gcd(2, 6), 2);
	BOOST_CHECK_EQUAL(euclid::gcd(6, 2), 2);
	BOOST_CHECK_EQUAL(euclid::gcd(15, 25), 5);
	BOOST_CHECK_EQUAL(euclid::gcd(18, 24), 6);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(use_binary)

BOOST_AUTO_TEST_CASE(check_values) {
	BOOST_CHECK_EQUAL(binary::gcd(1, 1), 1);
	BOOST_CHECK_EQUAL(binary::gcd(2, 1), 1);
	BOOST_CHECK_EQUAL(binary::gcd(1, 2), 1);
	BOOST_CHECK_EQUAL(binary::gcd(3, 2), 1);
	BOOST_CHECK_EQUAL(binary::gcd(2, 3), 1);
	BOOST_CHECK_EQUAL(binary::gcd(2, 6), 2);
	BOOST_CHECK_EQUAL(binary::gcd(6, 2), 2);
	BOOST_WARN_EQUAL(binary::gcd(15, 25), 5);
	BOOST_CHECK_EQUAL(binary::gcd(18, 24), 6);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
