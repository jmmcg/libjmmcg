/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/constrained_override_type.hpp"

#include <tuple>

using namespace libjmmcg;

struct type1 {
	int i= 1;
};

struct type2 {
	int i= 2;
};

struct type3 {
	int i= 3;
};

struct tester {
	bool process(const type1& t1, type2& t2) const {
		return t1.i < ++t2.i;
	}

	bool process(const type1& t1, type3& t2) const {
		return t1.i > ++t2.i;
	}
};

struct tester1 {
	bool process(const type1& t1, type2& t2) const {
		return t1.i < ++t2.i;
	}

	bool process(const type1&, type3&) const {
		return false;
	}
};

struct tester2 {
	bool process(const type1&, type2&) const {
		return false;
	}

	bool process(const type1& t1, type3& t2) const {
		return t1.i > ++t2.i;
	}
};

BOOST_AUTO_TEST_SUITE(constrained_override_type_tests)

BOOST_AUTO_TEST_SUITE(polymorphic)

BOOST_AUTO_TEST_CASE(ctor_two_overloads) {
	using fn1_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type2&>;
	using fn2_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type3&>;
	using result_type= mpl::constrained_override_type::polymorphic::result_type<tester, fn1_t, fn2_t>;
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::abstract_base_type>::value);
	static_assert(std::is_base_of<tester, result_type::abstract_base_type>::value);
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::type>::value);
	static_assert(std::is_base_of<tester, result_type::type>::value);

	BOOST_CHECK_NO_THROW(result_type::final_type{});
}

BOOST_AUTO_TEST_CASE(call_two_overloads) {
	using fn1_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type2&>;
	using fn2_t= mpl::constrained_override_type::to_member_function_type<bool, std::tuple<const type1&, type3&>>::type;
	using result_type= mpl::constrained_override_type::polymorphic::result_type<tester, fn1_t, fn2_t>;
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::abstract_base_type>::value);
	static_assert(std::is_base_of<tester, result_type::abstract_base_type>::value);
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::type>::value);
	static_assert(std::is_base_of<tester, result_type::type>::value);

	type1 t1;
	type2 t2;
	result_type::final_type obj;
	result_type::abstract_base_type& itf= obj;
	BOOST_CHECK(itf.process(t1, t2));
	BOOST_CHECK_EQUAL(t2.i, 3);
	type3 t3;
	BOOST_CHECK(!itf.process(t1, t3));
	BOOST_CHECK_EQUAL(t2.i, 3);
	BOOST_CHECK_EQUAL(t3.i, 4);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(compile_time)

BOOST_AUTO_TEST_CASE(ctor_two_overloads) {
	using fn1_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type2&>;
	using fn2_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type3&>;
	using result_type= mpl::constrained_override_type::compile_time::result_type<fn1_t, fn2_t>;
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::abstract_base_type>::value);
	static_assert(!std::is_base_of<tester1, result_type::abstract_base_type>::value);
	static_assert(!std::is_base_of<tester2, result_type::abstract_base_type>::value);
	static_assert(
		std::is_base_of<
			mpl::constrained_override_type::base_type,
			result_type::type<
				tester1>>::value);
	static_assert(
		std::is_base_of<
			tester1,
			result_type::type<
				tester1>>::value);
	static_assert(
		std::is_base_of<
			tester2,
			result_type::type<
				tester2>>::value);

	BOOST_CHECK_NO_THROW((
		result_type::final_type<
			tester1>{}));
}

BOOST_AUTO_TEST_CASE(call_two_overloads) {
	using fn1_t= mpl::constrained_override_type::member_function_type<bool, const type1&, type2&>;
	using fn2_t= mpl::constrained_override_type::to_member_function_type<bool, std::tuple<const type1&, type3&>>::type;
	using result_type= mpl::constrained_override_type::compile_time::result_type<fn1_t, fn2_t>;
	static_assert(std::is_base_of<mpl::constrained_override_type::base_type, result_type::abstract_base_type>::value);
	static_assert(!std::is_base_of<tester1, result_type::abstract_base_type>::value);
	static_assert(!std::is_base_of<tester2, result_type::abstract_base_type>::value);
	static_assert(
		std::is_base_of<
			mpl::constrained_override_type::base_type,
			result_type::type<
				tester1>>::value);
	static_assert(
		std::is_base_of<
			tester1,
			result_type::type<
				tester1>>::value);
	static_assert(
		std::is_base_of<
			tester2,
			result_type::type<
				tester2>>::value);

	type1 t1;
	type2 t2;
	result_type::final_type<
		tester1>
		obj1;
	result_type::abstract_base_type& itf1= obj1;
	BOOST_CHECK(itf1.process(t1, t2));
	BOOST_CHECK_EQUAL(t2.i, 3);
	type3 t3;
	bool exception_thrown= false;
	try {
		const auto ret= itf1.process(t1, t3);
		BOOST_CHECK(!ret);
	} catch(std::exception const&) {
		exception_thrown= true;
	}
	BOOST_CHECK(exception_thrown);
	BOOST_CHECK_EQUAL(t2.i, 3);
	BOOST_CHECK_EQUAL(t3.i, 3);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
