/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/socket_client_manager.hpp"

using namespace libjmmcg;

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short unused_port= 12345u;
const std::chrono::milliseconds timeout{5000};

struct just_connect {
	template<class Fn>
	void operator()(Fn const& f) const {
		const boost::asio::ip::tcp::endpoint endpoint(localhost, unused_port);
		f(endpoint);
	}
};

struct message1 {
	const char c[46]= "The quick brown fox jumps over the lazy dog.";
};

using lock_t= ppd::api_lock_traits<ppd::generic_traits::api_type::no_api, ppd::sequential_mode>::critical_section_type::lock_type;

typedef boost::mpl::list<
	socket::asio::client_manager<lock_t>,
	socket::glibc::client_manager<lock_t> >
	client_types;

BOOST_AUTO_TEST_SUITE(socket_tests)

BOOST_AUTO_TEST_SUITE(client)

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, client_t, client_types) {
	just_connect conn_pol;
	BOOST_CHECK_THROW(
		client_t skt(
			sizeof(message1),
			sizeof(message1),
			timeout,
			client_t::socket_t::socket_priority::low,
			0,
			conn_pol),
		std::runtime_error);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
