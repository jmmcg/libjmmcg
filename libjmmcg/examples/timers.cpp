/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/hp_timer.hpp"
#include "core/stats_output.hpp"

#include <boost/chrono.hpp>

#include <chrono>
#include <cstdint>
#include <iostream>

#include <sys/time.h>
#if defined(i386) || defined(__i386__) || defined(__x86_64__)
#	include <x86intrin.h>
#endif

using namespace libjmmcg;

using timed_results_t= ave_deviation_meter<double>;

struct tester_rdtsc {
	using result_type= unsigned long long;

	static inline constexpr char const name[]= "__rdtsc";

	static result_type result() {
		return __rdtsc();
	}
};

struct tester_rdtscp {
	using result_type= unsigned long long;

	static inline constexpr char const name[]= "__rdtscp";

	static result_type result() {
		unsigned int cpuid;
		return __rdtscp(&cpuid);
	}
};

struct tester_gettimeofday {
	using result_type= long;

	static inline constexpr char const name[]= "gettimeofday";

	static result_type result() {
		timeval tv;
		gettimeofday(&tv, nullptr);
		return tv.tv_usec;
	}
};

struct tester_clock_gettime_REALTIME {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(REALTIME)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_REALTIME, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_REALTIME_COARSE {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(REALTIME_COARSE)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_REALTIME_COARSE, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_MONOTONIC {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(MONOTONIC)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_MONOTONIC, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_MONOTONIC_COARSE {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(MONOTONIC_COARSE)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_MONOTONIC_COARSE, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_MONOTONIC_RAW {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(MONOTONIC_RAW)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_MONOTONIC_RAW, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_PROCESS_CPUTIME_ID {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(PROCESS_CPUTIME_ID)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tv);
		return tv.tv_nsec;
	}
};

struct tester_clock_gettime_THREAD_CPUTIME_ID {
	using result_type= long;

	static inline constexpr char const name[]= "clock_gettime(THREAD_CPUTIME_ID)";

	static result_type result() {
		timespec tv;
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tv);
		return tv.tv_nsec;
	}
};

struct tester_std_chrono_high_resolution_clock {
	using result_type= long;

	static inline constexpr char const name[]= "std::chrono::high_resolution_clock";

	static result_type result() {
		return std::chrono::high_resolution_clock::now().time_since_epoch().count();
	}
};

struct tester_std_chrono_steady_clock {
	using result_type= long;

	static inline constexpr char const name[]= "std::chrono::steady_clock";

	static result_type result() {
		return std::chrono::steady_clock::now().time_since_epoch().count();
	}
};

struct tester_std_chrono_system_clock {
	using result_type= long;

	static inline constexpr char const name[]= "std::chrono::system_clock";

	static result_type result() {
		return std::chrono::system_clock::now().time_since_epoch().count();
	}
};

struct tester_boost_chrono_high_resolution_clock {
	using result_type= long;

	static inline constexpr char const name[]= "boost::chrono::high_resolution_clock";

	static result_type result() {
		return boost::chrono::high_resolution_clock::now().time_since_epoch().count();
	}
};

struct tester_boost_chrono_steady_clock {
	using result_type= long;

	static inline constexpr char const name[]= "boost::chrono::steady_clock";

	static result_type result() {
		return boost::chrono::steady_clock::now().time_since_epoch().count();
	}
};

struct tester_boost_chrono_system_clock {
	using result_type= long;

	static inline constexpr char const name[]= "boost::chrono::system_clock";

	static result_type result() {
		return boost::chrono::system_clock::now().time_since_epoch().count();
	}
};

struct tester_boost_posix_time_UTC_clock {
	using result_type= long;

	static inline constexpr char const name[]= "boost::posix_time::microsec_clock::universal_time";

	static result_type result() {
		return boost::posix_time::microsec_clock::universal_time().time_of_day().ticks();
	}
};

typedef boost::mpl::list<
	tester_rdtsc,
	tester_rdtscp,
	tester_gettimeofday,
	tester_clock_gettime_REALTIME,
	tester_clock_gettime_REALTIME_COARSE,
	tester_clock_gettime_MONOTONIC,
	tester_clock_gettime_MONOTONIC_COARSE,
	tester_clock_gettime_MONOTONIC_RAW,
	tester_clock_gettime_PROCESS_CPUTIME_ID,
	tester_clock_gettime_THREAD_CPUTIME_ID,
	tester_std_chrono_high_resolution_clock,
	tester_std_chrono_steady_clock,
	tester_std_chrono_system_clock,
	tester_boost_chrono_high_resolution_clock,
	tester_boost_chrono_steady_clock,
	tester_boost_chrono_system_clock,
	tester_boost_posix_time_UTC_clock>
	timer_types;

BOOST_AUTO_TEST_SUITE(timers)

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("timers.csv"))

/**
	\test <a href="./examples/timers.svg">Graph</a> of performance results for the various timer functions.
	==========================================================================================
	Results for 500000 repetitions.
	Note that accurate timings should follow the recommendations in <a href="https://www-ssl.intel.com/content/www/us/en/embedded/training/ia-32-ia-64-benchmark-code-execution-paper.html">How to Benchmark Code Execution Times on Intel (c) IA-32 and IA-64 Instruction Set Architectures</a> and note that RDTSC is stable across processors, see section 17.14.1 of the <a href="https://www-ssl.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-3b-part-2-manual.pdf">Intel (c) 64 and IA-32 Architectures Software Developer's Manual Volume 3B: System Programming Guide, Part 2</a>, as long as "tsc" is listed in "/sys/devices/system/clocksource/clocksource0/available_clocksource".
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(time, test, timer_types) {
	std::cout << LIBJMMCG_SYSTEM ", " LIBJMMCG_SYSTEM_PROCESSOR ", " LIBJMMCG_CXX_COMPILER_ID ", " LIBJMMCG_CXX_COMPILER_VERSION ", " LIBJMMCG_BUILD_TYPE;

#ifdef JMMCG_PERFORMANCE_TESTS
	const std::size_t num_loops= 500000;
	const unsigned short loops_for_conv= 500;
#else
	const std::size_t num_loops= 10;
	const unsigned short loops_for_conv= 5;
#endif
	const double perc_conv_estimate= 5.0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			typename test::result_type time= typename test::result_type();
			cpu_timer::element_type time_to_measure;
			{
				cpu_timer::out_of_order timer(time_to_measure);
				for(unsigned i= 0; i < num_loops; ++i) {
					time+= test::result();
				}
			}
#ifndef JMMCG_PERFORMANCE_TESTS
			BOOST_CHECK_GT(time_to_measure, 0U);
#endif
			return static_cast<timed_results_t::value_type>(time_to_measure) / num_loops;
		}));
	std::cout << test::name << " time in CPU ticks=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats << timed_results.first.to_csv() << std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
