/******************************************************************************
** Copyright © 2002 by J.M.a_cacheGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/factory.hpp"

using namespace libjmmcg;

typedef boost::mpl::list<ppd::heavyweight_threading, ppd::sequential_mode> thread_types;

BOOST_AUTO_TEST_SUITE(factory_creator_tests)

struct A {
	const int i;

	explicit inline A(const int j)
		: i(j) {
	}

	void operator=(A const&)= delete;
};

template<class T>
typename T::created_type __fastcall create_1() {
	return typename T::created_type(new A(1));
}

template<class T>
typename T::created_type __fastcall create_2() {
	return typename T::created_type(new A(2));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK(c.empty());
	BOOST_CHECK_EQUAL(c.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fail_to_find_item, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.find(1), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_and_find_it, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, &create_1<factory_t>)), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_and_erase_it, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, &create_1<factory_t>)), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), true);
	BOOST_CHECK_EQUAL(c.erase(1), 1U);
	BOOST_CHECK(c.empty());
	BOOST_CHECK_EQUAL(c.size(), 0U);
	BOOST_CHECK_EQUAL(c.find(1), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_2_items_and_find_them, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, &create_1<factory_t>)), true);
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(2, &create_2<factory_t>)), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 2U);
	BOOST_CHECK_EQUAL(c.find(1), true);
	BOOST_CHECK_EQUAL(c.find(2), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_2_items_and_erase_one, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, &create_1<factory_t>)), true);
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(2, &create_2<factory_t>)), true);
	BOOST_CHECK_EQUAL(c.erase(1), 1U);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), false);
	BOOST_CHECK_EQUAL(c.find(2), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(make_an_item, Mdl, thread_types) {
	typedef typename factory::creator<int, A, std::runtime_error> factory_t;

	factory_t c;
	c.insert(typename factory_t::value_type(1, &create_1<factory_t>));
	BOOST_CHECK_EQUAL(c.make(1)->i, 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(factory_clone_tests)

template<class Mdl>
struct A final : public sp_counter_type<long, ppd::api_lock_traits<ppd::platform_api, Mdl>> {
	typedef sp_counter_type<long, ppd::api_lock_traits<ppd::platform_api, Mdl>> base_t;

	const int i;

	explicit A(const int j)
		: base_t(), i(j) {
	}

	A(A const& j)
		: base_t(), i(j.i) {
	}

	~A() noexcept(true) {}

	void operator=(A const&)= delete;
};

template<class T, class Mdl>
typename T::created_type __fastcall clone_it(const typename T::mapped_type::first_type::element_type& a) {
	return typename T::created_type(new A<Mdl>(a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK(c.empty());
	BOOST_CHECK_EQUAL(c.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fail_to_find_item, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.find(1), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_and_find_it, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(1)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_and_erase_it, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(1)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), true);
	BOOST_CHECK_EQUAL(c.erase(1), 1U);
	BOOST_CHECK(c.empty());
	BOOST_CHECK_EQUAL(c.size(), 0U);
	BOOST_CHECK_EQUAL(c.find(1), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_2_items_and_find_them, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(1)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(2, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(2)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 2U);
	BOOST_CHECK_EQUAL(c.find(1), true);
	BOOST_CHECK_EQUAL(c.find(2), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_2_items_and_erase_one, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(1, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(1)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK_EQUAL(c.insert(typename factory_t::value_type(2, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(2)), &clone_it<factory_t, Mdl>))), true);
	BOOST_CHECK_EQUAL(c.erase(1), 1U);
	BOOST_CHECK(!c.empty());
	BOOST_CHECK_EQUAL(c.size(), 1U);
	BOOST_CHECK_EQUAL(c.find(1), false);
	BOOST_CHECK_EQUAL(c.find(2), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(make_an_item, Mdl, thread_types) {
	typedef typename factory::clone<int, A<Mdl>, std::runtime_error> factory_t;

	factory_t c;
	c.insert(typename factory_t::value_type(1, typename factory_t::mapped_type(typename factory_t::mapped_type::first_type(new A<Mdl>(1)), &clone_it<factory_t, Mdl>)));
	BOOST_CHECK_EQUAL(c.make(1)->i, 1);
}

BOOST_AUTO_TEST_SUITE_END()
