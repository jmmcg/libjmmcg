/******************************************************************************
 * * Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/lock_mem.hpp"
#include "core/ttypes.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(lock_mem_tests)

BOOST_AUTO_TEST_SUITE(basic)

BOOST_AUTO_TEST_CASE(nullptr_with_zero_size) {
	constexpr std::size_t size= 0;
	std::byte const* buff= nullptr;
	BOOST_CHECK_THROW([[maybe_unused]] lock_mem_range tmp(buff, size), std::exception);
}

BOOST_AUTO_TEST_CASE(nullptr_with_nonzero_size) {
	constexpr std::size_t size= 42;
	std::byte const* buff= nullptr;
	BOOST_CHECK_THROW([[maybe_unused]] lock_mem_range tmp(buff, size), std::exception);
}

BOOST_AUTO_TEST_CASE(non_nullptr_with_zero_size) {
	constexpr std::size_t size= 0;
	std::byte const* buff= reinterpret_cast<std::byte const*>(42);
	BOOST_CHECK_THROW([[maybe_unused]] lock_mem_range tmp(buff, size), std::exception);
}

BOOST_AUTO_TEST_CASE(stack_buff_with_zero_size) {
	constexpr std::size_t size= 0;
	std::array<volatile std::byte, size> buff;
	BOOST_CHECK_THROW([[maybe_unused]] lock_mem_range tmp(buff.data(), size), std::exception);
}

BOOST_AUTO_TEST_CASE(stack_buff_with_correct_size) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff{};
	BOOST_CHECK_NO_THROW([[maybe_unused]] lock_mem_range tmp(buff.data(), size));
}

BOOST_AUTO_TEST_CASE(stack_buff_with_correct_size_lock_twice) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff{};
	const lock_mem_range lk(buff.data(), size);
	BOOST_CHECK_NO_THROW([[maybe_unused]] lock_mem_range tmp(buff.data(), size));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(stack)

BOOST_AUTO_TEST_CASE(buff_with_correct_size_rw_values) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff;
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	const lock_mem_range locker(buff.data(), size);
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
	*buff.begin()= std::byte{68};
	*buff.rbegin()= std::byte{42};
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{68});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{42});
}

BOOST_AUTO_TEST_CASE(scoped_buff_with_correct_size_rw_values) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff;
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	{
		const lock_mem_range locker(buff.data(), size);
		BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
		BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
		*buff.begin()= std::byte{68};
		*buff.rbegin()= std::byte{42};
		BOOST_CHECK_EQUAL(*buff.begin(), std::byte{68});
		BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{42});
	}
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
}

BOOST_AUTO_TEST_CASE(scoped_buff_with_correct_size_rw_values_relock_diff_scopes) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff;
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	{
		const lock_mem_range locker(buff.data(), size);
		BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
		BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
		*buff.begin()= std::byte{68};
		*buff.rbegin()= std::byte{42};
		BOOST_CHECK_EQUAL(*buff.begin(), std::byte{68});
		BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{42});
	}
	const lock_mem_range locker(buff.data(), size);
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
}

BOOST_AUTO_TEST_CASE(scoped_buff_with_correct_size_rw_values_relock_same_scope) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff;
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	const lock_mem_range locker0(buff.data(), size);
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
	*buff.begin()= std::byte{68};
	*buff.rbegin()= std::byte{42};
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{68});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{42});
	const lock_mem_range locker2(buff.data(), size);
	*buff.begin()= std::byte{42};
	*buff.rbegin()= std::byte{68};
	BOOST_CHECK_EQUAL(*buff.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff.rbegin(), std::byte{68});
}

BOOST_AUTO_TEST_CASE(scoped_buff_with_correct_size_rw_diff_values_lock_same_scope) {
	constexpr std::size_t size= 42;
	std::array<volatile std::byte, size> buff0;
	*buff0.begin()= std::byte{42};
	*buff0.rbegin()= std::byte{68};
	const lock_mem_range locker0(buff0.data(), size);
	BOOST_CHECK_EQUAL(*buff0.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff0.rbegin(), std::byte{68});
	*buff0.begin()= std::byte{68};
	*buff0.rbegin()= std::byte{42};
	BOOST_CHECK_EQUAL(*buff0.begin(), std::byte{68});
	BOOST_CHECK_EQUAL(*buff0.rbegin(), std::byte{42});
	std::array<volatile std::byte, size> buff1;
	*buff1.begin()= std::byte{68};
	*buff1.rbegin()= std::byte{42};
	const lock_mem_range locker2(buff1.data(), size);
	BOOST_CHECK_EQUAL(*buff1.begin(), std::byte{68});
	BOOST_CHECK_EQUAL(*buff1.rbegin(), std::byte{42});
	*buff1.begin()= std::byte{42};
	*buff1.rbegin()= std::byte{68};
	BOOST_CHECK_EQUAL(*buff1.begin(), std::byte{42});
	BOOST_CHECK_EQUAL(*buff1.rbegin(), std::byte{68});
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(heap)

BOOST_AUTO_TEST_CASE(buff_with_correct_size) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	BOOST_CHECK_NO_THROW([[maybe_unused]] lock_mem_range tmp(buff.get(), size));
}

BOOST_AUTO_TEST_CASE(buff_with_correct_size_rw_values) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	const lock_mem_range locker(buff.get(), size);
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
	buff.get()[0]= std::byte{68};
	buff.get()[size - 1]= std::byte{42};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{68});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{42});
}

BOOST_AUTO_TEST_CASE(scoped_buff_with_correct_size_rw_values) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	{
		const lock_mem_range locker(buff.get(), size);
		BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
		BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
		buff.get()[0]= std::byte{68};
		buff.get()[size - 1]= std::byte{42};
		BOOST_CHECK_EQUAL(buff.get()[0], std::byte{68});
		BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{42});
	}
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(process)

BOOST_AUTO_TEST_CASE(lock_first_rw_values) {
	const lock_all_proc_mem locker(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
}

BOOST_AUTO_TEST_CASE(lock_second_rw_values) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	const lock_all_proc_mem locker(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
}

BOOST_AUTO_TEST_CASE(rw_values_then_lock) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
	const lock_all_proc_mem locker(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
	buff.get()[0]= std::byte{68};
	buff.get()[size - 1]= std::byte{42};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{68});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{42});
}

BOOST_AUTO_TEST_CASE(rw_values_then_scope_lock_then_rw) {
	constexpr std::size_t size= 42;
	std::unique_ptr<volatile std::byte[]> buff(new std::byte[size]);
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
	{
		const lock_all_proc_mem locker(lock_all_proc_mem::flags::current | lock_all_proc_mem::flags::on_fault);
		buff.get()[0]= std::byte{68};
		buff.get()[size - 1]= std::byte{42};
		BOOST_CHECK_EQUAL(buff.get()[0], std::byte{68});
		BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{42});
	}
	buff.get()[0]= std::byte{42};
	buff.get()[size - 1]= std::byte{68};
	BOOST_CHECK_EQUAL(buff.get()[0], std::byte{42});
	BOOST_CHECK_EQUAL(buff.get()[size - 1], std::byte{68});
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
