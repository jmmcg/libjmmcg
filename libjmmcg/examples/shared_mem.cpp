/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "shared_mem_struct.hpp"

#include <cstdlib>
#include <future>
#include <random>

static constexpr std::size_t size= sizeof(std::uint64_t) * 10;

using namespace libjmmcg;

struct create_shm_and_child {
	class ensure_exits {
	public:
		explicit ensure_exits(shared_data& data) noexcept(true)
			: data_(data) {}

		~ensure_exits() noexcept(true) {
			data_.exit_child= true;
		}

	private:
		shared_data& data_;
	};

	shared_mem shm{name, sizeof(shared_data)};
	shared_mem::allocator_type::unique_ptr<shared_data> const data{shm.get_allocator().make_unique<magic, shared_data>()};

	void wait_for_subproc_to_start() {
		while(!data->subproc_started) {
			std::this_thread::yield();
		}
	}

	void new_data() {
		data->changed= !data->changed;
	}

	void wait_for_new_data() {
		const bool old= data->changed;
		while(data->changed == old) {
			std::this_thread::yield();
		}
	}
};

BOOST_AUTO_TEST_SUITE(shared_mem_test)

BOOST_AUTO_TEST_CASE(ctor_default_pid) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] shared_mem shm(name, size));
}

BOOST_AUTO_TEST_CASE(ctor_set_pid) {
	BOOST_CHECK_NO_THROW([[maybe_unused]] shared_mem shm(name, size, boost::interprocess::ipcdetail::get_current_process_id()));
}

BOOST_AUTO_TEST_CASE(ctor_no_name) {
	BOOST_CHECK_THROW([[maybe_unused]] shared_mem tmp(shared_mem::name_type{""}, size), std::exception);
}

BOOST_AUTO_TEST_CASE(ctor_check_name) {
	shared_mem shm(name, size);
	BOOST_CHECK_EQUAL(shm.name_[0], '/');
	BOOST_CHECK(shm.name_.starts_with('/' + name + '.'));
}

BOOST_AUTO_TEST_CASE(ctor_zero_size) {
	BOOST_CHECK_THROW([[maybe_unused]] shared_mem tmp(name, 0), std::exception);
}

BOOST_AUTO_TEST_CASE(ctor_check_size) {
	shared_mem shm(name, size);
	BOOST_CHECK_EQUAL(shm.size(), size);
}

BOOST_AUTO_TEST_CASE(verify_mapped_addr_not_nullptr) {
	shared_mem shm(name, size);
	BOOST_CHECK_NE(shm.data(), nullptr);
}

BOOST_AUTO_TEST_CASE(ctor_get_with_wrong_magic) {
	shared_mem shm(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	constexpr libjmmcg::shared_mem::magic_type wrong_magic{};
	BOOST_CHECK_THROW((shm.get<wrong_magic, std::uint64_t>()), std::exception);
}

BOOST_AUTO_TEST_CASE(ctor_get_with_wrong_type) {
	shared_mem shm(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	BOOST_CHECK_THROW((shm.get<magic, std::uint32_t>()), std::exception);
}

BOOST_AUTO_TEST_CASE(write_word_to_memory_read_back_wrong_type) {
	shared_mem shm(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	*mem= 42;
	BOOST_CHECK_THROW((shm.get<magic, std::uint32_t>()), std::exception);
}

BOOST_AUTO_TEST_CASE(write_word_to_memory_read_back_wrong_magic) {
	shared_mem shm(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	*mem= 42;
	constexpr libjmmcg::shared_mem::magic_type wrong_magic(LIBJMMCG_GIT_SHA_SHORT >> 1u);
	BOOST_CHECK_THROW((shm.get<wrong_magic, std::uint64_t>()), std::exception);
}

BOOST_AUTO_TEST_CASE(create_shms_shared_another) {
	shared_mem shm(name, size);
	shared_mem shm_shared(name, size);
	BOOST_CHECK_NE(shm.data(), shm_shared.data());
	BOOST_CHECK_EQUAL(shm.size(), shm_shared.size());
}

BOOST_AUTO_TEST_CASE(create_two_shms_not_shared) {
	shared_mem shm(name, size);
	shared_mem shm_another(name + "ANOther", size);
	BOOST_CHECK_NE(shm.data(), shm_another.data());
	BOOST_CHECK_EQUAL(shm.size(), shm_another.size());
}

BOOST_AUTO_TEST_CASE(create_shms_shared_another_writes) {
	shared_mem shm(name, size);
	shared_mem shm_shared(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	*mem= 42;
	auto& mem_shared{shm_shared.get<magic, std::uint64_t>()};
	BOOST_CHECK_EQUAL(mem_shared, *mem);
	mem_shared= 68;
	BOOST_CHECK_EQUAL(mem_shared, *mem);
}

BOOST_AUTO_TEST_CASE(create_two_shms_not_shared_one_writes) {
	shared_mem shm(name, size);
	shared_mem shm_another(name + "ANOther", size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem_another{shm_another.get_allocator().make_unique<magic, std::uint64_t>()};
	*mem= 42;
	*mem_another= 68;
	BOOST_CHECK_NE(*mem_another, *mem);
}

BOOST_AUTO_TEST_CASE(fill_mapped_addr_data_multiple_shms) {
	shared_mem shm(name, size);
	shared_mem shm_shared(name, size);
	std::mt19937_64 init_gen(42);
	BOOST_MPL_ASSERT_RELATION(sizeof(std::int8_t), ==, sizeof(shared_mem::element_type));
	std::uniform_int_distribution<std::int8_t> init_distribution;
	std::vector<std::int8_t> data;
	std::generate_n(std::back_inserter(data), size, std::bind(init_distribution, init_gen));
	BOOST_CHECK_EQUAL(shm.size(), data.size());
	for(shared_mem::pointer dest= shm.begin(); auto const& v: data) {
		*dest= static_cast<std::byte>(v);
		++dest;
	}
	BOOST_CHECK(std::equal(data.begin(), data.end(), shm.begin(), [](auto const& l, auto const& r) {
		return static_cast<std::byte>(l) == r;
	}));
	BOOST_CHECK(std::equal(data.begin(), data.end(), shm_shared.begin(), [](auto const& l, auto const& r) {
		return static_cast<std::byte>(l) == r;
	}));
	shared_mem test_after_written(name, size);
	BOOST_CHECK(std::equal(data.begin(), data.end(), test_after_written.begin(), [](auto const& l, auto const& r) {
		return static_cast<std::byte>(l) == r;
	}));
}

BOOST_AUTO_TEST_CASE(write_word_to_memory_and_sync) {
	shared_mem shm(name, size);
	shared_mem::allocator_type::unique_ptr<std::uint64_t> const mem{shm.get_allocator().make_unique<magic, std::uint64_t>()};
	*mem= 42;
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 42);
	BOOST_CHECK_NO_THROW(shm.sync(reinterpret_cast<std::uint64_t*>(shm.data())));
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 42);
	BOOST_CHECK_NO_THROW(shm.sync(reinterpret_cast<std::uint64_t*>(shm.data()), shared_mem::sync_flags_t::req_update_wait));
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 42);
	*mem= 68;
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 68);
	BOOST_CHECK_NO_THROW(shm.sync(reinterpret_cast<std::uint64_t*>(shm.data())));
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 68);
	BOOST_CHECK_NO_THROW(shm.sync(reinterpret_cast<std::uint64_t*>(shm.data()), shared_mem::sync_flags_t::req_update_wait));
	BOOST_CHECK_EQUAL((shm.get<magic, std::uint64_t>()), 68);
}

BOOST_AUTO_TEST_CASE(create_shm_and_child_in_bg) {
	create_shm_and_child shm_and_child;
	int exit_code= -1;
	{
		const create_shm_and_child::ensure_exits exit(*shm_and_child.data);
		exit_code= std::system(LIBJMMCG_SHAREDMEM_SUBPROC_NAME " &");
		BOOST_CHECK_NO_THROW(shm_and_child.wait_for_subproc_to_start());
		shm_and_child.data->ping= true;
		BOOST_CHECK_NO_THROW(shm_and_child.new_data());
		BOOST_CHECK_NO_THROW(shm_and_child.wait_for_new_data());
		BOOST_CHECK_EQUAL(shm_and_child.data->pong, true);
	}
	BOOST_CHECK_EQUAL(exit_code, 0);
}

BOOST_AUTO_TEST_CASE(create_shm_and_child_controlled_by_thread) {
	create_shm_and_child shm_and_child;
	auto get_subproc_ret_code= std::async(
		std::launch::async,
		[]() {
			return std::system(LIBJMMCG_SHAREDMEM_SUBPROC_NAME);
		});
	{
		const create_shm_and_child::ensure_exits exit(*shm_and_child.data);
		BOOST_CHECK_NO_THROW(shm_and_child.wait_for_subproc_to_start());
		shm_and_child.data->ping= true;
		BOOST_CHECK_NO_THROW(shm_and_child.new_data());
		BOOST_CHECK_NO_THROW(shm_and_child.wait_for_new_data());
		BOOST_CHECK_EQUAL(shm_and_child.data->pong, true);
	}
	// No bloody idea why we get this weird difference...
	//	BOOST_CHECK_EQUAL(get_subproc_ret_code.get(), 256);
	BOOST_CHECK_EQUAL(get_subproc_ret_code.get(), 0);
}

BOOST_AUTO_TEST_SUITE_END()
