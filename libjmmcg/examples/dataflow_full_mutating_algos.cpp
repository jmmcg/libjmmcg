/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2> >
	finite_test_types;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	// TODO	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2> >
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

template<typename T>
struct square {
	static T last;

	void operator()(T t) {
		last+= t;
	};
};

template<typename T>
T square<T>::last;

inline int
sqr() {
	static int last= 0;
	return last++ << 1;
}

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable_dataflow)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(empty_colln)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v1.empty(), true);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v_out;
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v.size(), 0U);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.fill_n(v, 1, typename vtr_colln_t::value_type(1));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(1));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(nonempty_colln)

BOOST_AUTO_TEST_SUITE(one_element)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), 1U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1}, v1{1}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v1.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), 1U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1}, v_out;
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1};
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(2));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1};
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(2));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1};
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(two_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), 2U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
	BOOST_CHECK_EQUAL(v_out[1], -v[1]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2}, v1{1, 2}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v1.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), 2U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
	BOOST_CHECK_EQUAL(v_out[1], v[1] + v1[1]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2}, v_out;
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2};
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(3));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 3);
	BOOST_CHECK_EQUAL(v[1], 3);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2};
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(3));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 3);
	BOOST_CHECK_EQUAL(v[1], 3);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2};
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(n_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), 8U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
	BOOST_CHECK_EQUAL(v_out[1], -v[1]);
	BOOST_CHECK_EQUAL(v_out[2], -v[2]);
	BOOST_CHECK_EQUAL(v_out[3], -v[3]);
	BOOST_CHECK_EQUAL(v_out[4], -v[4]);
	BOOST_CHECK_EQUAL(v_out[5], -v[5]);
	BOOST_CHECK_EQUAL(v_out[6], -v[6]);
	BOOST_CHECK_EQUAL(v_out[7], -v[7]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{1, 2, 3, 4, 5, 6, 7, 8}, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), 8U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
	BOOST_CHECK_EQUAL(v_out[1], v[1] + v1[1]);
	BOOST_CHECK_EQUAL(v_out[2], v[2] + v1[2]);
	BOOST_CHECK_EQUAL(v_out[3], v[3] + v1[3]);
	BOOST_CHECK_EQUAL(v_out[4], v[4] + v1[4]);
	BOOST_CHECK_EQUAL(v_out[5], v[5] + v1[5]);
	BOOST_CHECK_EQUAL(v_out[6], v[6] + v1[6]);
	BOOST_CHECK_EQUAL(v_out[7], v[7] + v1[7]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v_out;
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_sequential_independent_copies, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{11, 22, 33, 44, 55, 66, 77, 88}, v3, v4;
	v3.reserve(v.size() + v1.size());
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	*context1;
	auto const& context2= pool << joinable() << pool.copy(v1, v4);
	*context2;
	BOOST_CHECK_EQUAL(v3.size(), 8U);
	BOOST_CHECK(v == v3);
	BOOST_CHECK_EQUAL(v4.size(), 8U);
	BOOST_CHECK(v1 == v4);
	vtr_colln_t v_chk;
	v_chk.reserve(v3.size() * 2);
	std::copy(v3.colln().begin(), v3.colln().end(), std::back_inserter(v_chk.colln()));
	BOOST_CHECK_EQUAL(v_chk.colln().size(), 8U);
	BOOST_CHECK_EQUAL(v_chk.size(), 8U);
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_independent_copies, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{11, 22, 33, 44, 55, 66, 77, 88}, v3, v4;
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v4);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v3.size(), 8U);
	BOOST_CHECK(v == v3);
	BOOST_CHECK_EQUAL(v4.size(), 8U);
	BOOST_CHECK(v1 == v4);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_second_larger, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{11, 22, 33, 44, 55, 66, 77, 88, 99}, v3;
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 9U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3 == v1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_invert_dependencies, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{11, 22, 33, 44, 55, 66, 77, 88, 99}, v3;
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context2;
	*context1;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 9U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3 == v1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_second_smaller, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8}, v1{11, 22, 33, 44, 55}, v3;
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 5U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3[0] == v1[0]);
	BOOST_CHECK(v3[1] == v1[1]);
	BOOST_CHECK(v3[2] == v1[2]);
	BOOST_CHECK(v3[3] == v1[3]);
	BOOST_CHECK(v3[4] == v1[4]);
	BOOST_CHECK(v3[5] == v[5]);
	BOOST_CHECK(v3[6] == v[6]);
	BOOST_CHECK(v3[7] == v[7]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8};
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(9));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 9);
	BOOST_CHECK_EQUAL(v[2], 3);
	BOOST_CHECK_EQUAL(v[3], 4);
	BOOST_CHECK_EQUAL(v[4], 5);
	BOOST_CHECK_EQUAL(v[5], 6);
	BOOST_CHECK_EQUAL(v[6], 7);
	BOOST_CHECK_EQUAL(v[7], 8);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8};
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(9));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 8);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 9);
	BOOST_CHECK_EQUAL(v[2], 9);
	BOOST_CHECK_EQUAL(v[3], 9);
	BOOST_CHECK_EQUAL(v[4], 9);
	BOOST_CHECK_EQUAL(v[5], 9);
	BOOST_CHECK_EQUAL(v[6], 9);
	BOOST_CHECK_EQUAL(v[7], 9);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse_8_elems, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8};
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 8);
	BOOST_CHECK_EQUAL(v[0], 8);
	BOOST_CHECK_EQUAL(v[1], 7);
	BOOST_CHECK_EQUAL(v[2], 6);
	BOOST_CHECK_EQUAL(v[3], 5);
	BOOST_CHECK_EQUAL(v[4], 4);
	BOOST_CHECK_EQUAL(v[5], 3);
	BOOST_CHECK_EQUAL(v[6], 2);
	BOOST_CHECK_EQUAL(v[7], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse_9_elems, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8, 9};
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 9);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 8);
	BOOST_CHECK_EQUAL(v[2], 7);
	BOOST_CHECK_EQUAL(v[3], 6);
	BOOST_CHECK_EQUAL(v[4], 5);
	BOOST_CHECK_EQUAL(v[5], 4);
	BOOST_CHECK_EQUAL(v[6], 3);
	BOOST_CHECK_EQUAL(v[7], 2);
	BOOST_CHECK_EQUAL(v[8], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse_9_elems_clear, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v{1, 2, 3, 4, 5, 6, 7, 8, 9};
	auto const& context= pool << joinable() << pool.reverse(v);
	v.clear();
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v.size(), 0);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_SUITE(empty_colln)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v_out;
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v1.empty(), true);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v.size(), 0U);
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.fill_n(v, 1, typename vtr_colln_t::value_type(1));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(1));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(nonempty_colln)

BOOST_AUTO_TEST_SUITE(one_element)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), 1U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v1.push_back(1);
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v1.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), 1U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(2));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(2));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 1);
	BOOST_CHECK_EQUAL(v[0], 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(two_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), 2U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
	BOOST_CHECK_EQUAL(v_out[1], -v[1]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(2);
	v1.push_back(1);
	v1.push_back(2);
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v1.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), 2U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
	BOOST_CHECK_EQUAL(v_out[1], v[1] + v1[1]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(3));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 3);
	BOOST_CHECK_EQUAL(v[1], 3);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(3));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 3);
	BOOST_CHECK_EQUAL(v[1], 3);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(n_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(transform, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.transform(v, v_out, std::negate<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), 8U);
	BOOST_CHECK_EQUAL(v_out[0], -v[0]);
	BOOST_CHECK_EQUAL(v_out[1], -v[1]);
	BOOST_CHECK_EQUAL(v_out[2], -v[2]);
	BOOST_CHECK_EQUAL(v_out[3], -v[3]);
	BOOST_CHECK_EQUAL(v_out[4], -v[4]);
	BOOST_CHECK_EQUAL(v_out[5], -v[5]);
	BOOST_CHECK_EQUAL(v_out[6], -v[6]);
	BOOST_CHECK_EQUAL(v_out[7], -v[7]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(transform_2_collns, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	v1.push_back(6);
	v1.push_back(7);
	v1.push_back(8);
	auto const& context= pool << joinable() << pool.transform(v, v1, v_out, std::plus<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), 8U);
	BOOST_CHECK_EQUAL(v_out[0], v[0] + v1[0]);
	BOOST_CHECK_EQUAL(v_out[1], v[1] + v1[1]);
	BOOST_CHECK_EQUAL(v_out[2], v[2] + v1[2]);
	BOOST_CHECK_EQUAL(v_out[3], v[3] + v1[3]);
	BOOST_CHECK_EQUAL(v_out[4], v[4] + v1[4]);
	BOOST_CHECK_EQUAL(v_out[5], v[5] + v1[5]);
	BOOST_CHECK_EQUAL(v_out[6], v[6] + v1[6]);
	BOOST_CHECK_EQUAL(v_out[7], v[7] + v1[7]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(copy, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v_out;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.copy(v, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK(v == v_out);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_sequential_independent_copies, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v3, v4;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(11);
	v1.push_back(22);
	v1.push_back(33);
	v1.push_back(44);
	v1.push_back(55);
	v1.push_back(66);
	v1.push_back(77);
	v1.push_back(88);
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	*context1;
	auto const& context2= pool << joinable() << pool.copy(v1, v4);
	*context2;
	BOOST_CHECK_EQUAL(v3.size(), 8U);
	BOOST_CHECK(v == v3);
	BOOST_CHECK_EQUAL(v4.size(), 8U);
	BOOST_CHECK(v1 == v4);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_independent_copies, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v3, v4;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(11);
	v1.push_back(22);
	v1.push_back(33);
	v1.push_back(44);
	v1.push_back(55);
	v1.push_back(66);
	v1.push_back(77);
	v1.push_back(88);
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v4);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v3.size(), 8U);
	BOOST_CHECK(v == v3);
	BOOST_CHECK_EQUAL(v4.size(), 8U);
	BOOST_CHECK(v1 == v4);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_second_larger, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v3;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(11);
	v1.push_back(22);
	v1.push_back(33);
	v1.push_back(44);
	v1.push_back(55);
	v1.push_back(66);
	v1.push_back(77);
	v1.push_back(88);
	v1.push_back(99);
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 9U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3 == v1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_invert_dependencies, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v3;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(11);
	v1.push_back(22);
	v1.push_back(33);
	v1.push_back(44);
	v1.push_back(55);
	v1.push_back(66);
	v1.push_back(77);
	v1.push_back(88);
	v1.push_back(99);
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context2;
	*context1;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 9U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3 == v1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_concurrent_dependent_copies_second_smaller, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v, v1, v3;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v1.push_back(11);
	v1.push_back(22);
	v1.push_back(33);
	v1.push_back(44);
	v1.push_back(55);
	auto const& context1= pool << joinable() << pool.copy(v, v3);
	auto const& context2= pool << joinable() << pool.copy(v1, v3);
	*context1;
	*context2;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 5U);
	BOOST_CHECK(v3.size() == v1.size());
	BOOST_CHECK(v3 == v1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill_n, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.fill_n(v, 2, typename vtr_colln_t::value_type(9));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 2);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 9);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fill, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.fill(v, typename vtr_colln_t::value_type(9));
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 8);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 9);
	BOOST_CHECK_EQUAL(v[2], 9);
	BOOST_CHECK_EQUAL(v[3], 9);
	BOOST_CHECK_EQUAL(v[4], 9);
	BOOST_CHECK_EQUAL(v[5], 9);
	BOOST_CHECK_EQUAL(v[6], 9);
	BOOST_CHECK_EQUAL(v[7], 9);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse_8_elems, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 8);
	BOOST_CHECK_EQUAL(v[0], 8);
	BOOST_CHECK_EQUAL(v[1], 7);
	BOOST_CHECK_EQUAL(v[2], 6);
	BOOST_CHECK_EQUAL(v[3], 5);
	BOOST_CHECK_EQUAL(v[4], 4);
	BOOST_CHECK_EQUAL(v[5], 3);
	BOOST_CHECK_EQUAL(v[6], 2);
	BOOST_CHECK_EQUAL(v[7], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reverse_9_elems, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v.push_back(9);
	auto const& context= pool << joinable() << pool.reverse(v);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), false);
	BOOST_CHECK_EQUAL(v.size(), 9);
	BOOST_CHECK_EQUAL(v[0], 9);
	BOOST_CHECK_EQUAL(v[1], 8);
	BOOST_CHECK_EQUAL(v[2], 7);
	BOOST_CHECK_EQUAL(v[3], 6);
	BOOST_CHECK_EQUAL(v[4], 5);
	BOOST_CHECK_EQUAL(v[5], 4);
	BOOST_CHECK_EQUAL(v[6], 3);
	BOOST_CHECK_EQUAL(v[7], 2);
	BOOST_CHECK_EQUAL(v[8], 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
