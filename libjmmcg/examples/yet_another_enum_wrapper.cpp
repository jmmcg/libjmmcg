/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/yet_another_enum_wrapper.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(yet_another_enum_wrapper_tests)

BOOST_AUTO_TEST_SUITE(defaulted)

BOOST_AUTO_TEST_CASE(one_tag) {
	LIBJMMCG_MAKE_ENUM(foo, int, bar);
	BOOST_CHECK_EQUAL(foo::bar, 0);
	BOOST_CHECK_EQUAL(foo::to_string(foo::bar), "foo::bar");
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::bar)).has_value());
}

BOOST_AUTO_TEST_CASE(two_tags) {
	LIBJMMCG_MAKE_ENUM(foo, int, bar, baz);
	BOOST_CHECK_EQUAL(foo::bar, 0);
	BOOST_CHECK_EQUAL(foo::baz, 1);
	BOOST_CHECK_EQUAL(foo::to_string(foo::bar), "foo::bar");
	BOOST_CHECK_EQUAL(foo::to_string(foo::baz), "foo::baz");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::bar)), foo::baz);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::baz)).has_value());
}

BOOST_AUTO_TEST_CASE(three_tags) {
	LIBJMMCG_MAKE_ENUM(foo, int, bar, baz, fubar);
	BOOST_CHECK_EQUAL(foo::bar, 0);
	BOOST_CHECK_EQUAL(foo::baz, 1);
	BOOST_CHECK_EQUAL(foo::fubar, 2);
	BOOST_CHECK_EQUAL(foo::to_string(foo::bar), "foo::bar");
	BOOST_CHECK_EQUAL(foo::to_string(foo::baz), "foo::baz");
	BOOST_CHECK_EQUAL(foo::to_string(foo::fubar), "foo::fubar");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::bar)), foo::baz);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::baz)), foo::fubar);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::fubar)).has_value());
}

BOOST_AUTO_TEST_CASE(four_tags) {
	LIBJMMCG_MAKE_ENUM(foo, int, foo0, foo1, foo2, foo3);
	BOOST_CHECK_EQUAL(foo::foo0, 0);
	BOOST_CHECK_EQUAL(foo::foo1, 1);
	BOOST_CHECK_EQUAL(foo::foo2, 2);
	BOOST_CHECK_EQUAL(foo::foo3, 3);
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo1), "foo::foo1");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo2), "foo::foo2");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo3), "foo::foo3");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo0)), foo::foo1);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo1)), foo::foo2);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo2)), foo::foo3);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::foo3)).has_value());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(custom)

BOOST_AUTO_TEST_CASE(one_tag) {
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4));
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 1);
	BOOST_CHECK_EQUAL(foo::foo0, 4);
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0=4");
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::foo0)).has_value());
}

BOOST_AUTO_TEST_CASE(two_tags) {
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4), (foo1, 5));
	BOOST_CHECK_EQUAL(foo::foo0, 4);
	BOOST_CHECK_EQUAL(foo::foo1, 5);
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0=4");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo1), "foo::foo1=5");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo0)), foo::foo1);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::foo1)).has_value());
}

BOOST_AUTO_TEST_CASE(two_char_tags) {
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, char, (foo0, '0'), (foo1, '1'));
	BOOST_CHECK_EQUAL(foo::foo0, '0');
	BOOST_CHECK_EQUAL(foo::foo1, '1');
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0='0'");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo1), "foo::foo1='1'");
	std::string const test_0{"0"};
	BOOST_CHECK_EQUAL(foo::to_enum({test_0.begin(), test_0.end()}), foo::foo0);
	std::string const test_1{"1"};
	BOOST_CHECK_EQUAL(foo::to_enum({test_1.begin(), test_1.end()}), foo::foo1);
}

BOOST_AUTO_TEST_CASE(three_tags) {
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4), (foo1, 5), (foo2, 42));
	BOOST_CHECK_EQUAL(foo::foo0, 4);
	BOOST_CHECK_EQUAL(foo::foo1, 5);
	BOOST_CHECK_EQUAL(foo::foo2, 42);
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0=4");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo1), "foo::foo1=5");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo2), "foo::foo2=42");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo0)), foo::foo1);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo1)), foo::foo2);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::foo2)).has_value());
}

BOOST_AUTO_TEST_CASE(four_tags) {
	LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4), (foo1, 5), (foo2, 42), (foo3, 68));
	BOOST_CHECK_EQUAL(foo::foo0, 4);
	BOOST_CHECK_EQUAL(foo::foo1, 5);
	BOOST_CHECK_EQUAL(foo::foo2, 42);
	BOOST_CHECK_EQUAL(foo::foo3, 68);
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo0), "foo::foo0=4");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo1), "foo::foo1=5");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo2), "foo::foo2=42");
	BOOST_CHECK_EQUAL(foo::to_string(foo::foo3), "foo::foo3=68");
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo0)), foo::foo1);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo1)), foo::foo2);
	BOOST_CHECK_EQUAL(*foo::next(foo::const_iterator(foo::foo2)), foo::foo3);
	BOOST_CHECK(!foo::next(foo::const_iterator(foo::foo3)).has_value());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
