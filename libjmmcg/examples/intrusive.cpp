/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/intrusive.hpp"

using namespace libjmmcg;
using namespace ppd;

typedef boost::mpl::list<
	sequential_mode,
	heavyweight_threading>
	thread_types;

template<class LT>
struct data final : public intrusive::node_details_itf<LT>, public sp_counter_type<typename intrusive::node_details_itf<LT>::atomic_ctr_t::value_type, LT> {
	typedef sp_counter_type<typename intrusive::node_details_itf<LT>::atomic_ctr_t::value_type, LT> base_t;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::atomic_ctr_t atomic_ctr_t;
	typedef typename base_t::deleter_t deleter_t;

	const int i;

	explicit data(int j) noexcept(true)
		: i(j) {}

	~data() noexcept(true) {}

	tstring
	to_string() const noexcept(false) override {
		tostringstream os;
		os << "i=" << i;
		return os.str();
	}
};

BOOST_AUTO_TEST_SUITE(instrusive)

BOOST_AUTO_TEST_SUITE(stack)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_one_item, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val(new typename stack_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_and_pop_one_item, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val(new typename stack_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	typename stack_t::value_type ret(l.top());
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop();
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_one_item_clear, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val(new typename stack_t::value_type::value_type(1));
	l.push(val);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK_NO_THROW(l.clear());
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_two_items, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val1(new typename stack_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename stack_t::value_type val2(new typename stack_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_two_items_pop_one1, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val1(new typename stack_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	typename stack_t::value_type val2(new typename stack_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push(val2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	typename stack_t::value_type ret(l.top());
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_two_items_pop_one2, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> stack_t;

	stack_t l;
	typename stack_t::value_type val1(new typename stack_t::value_type::value_type(1));
	l.push(val1);
	typename stack_t::value_type ret(l.top());
	BOOST_CHECK_EQUAL(val1, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	typename stack_t::value_type val2(new typename stack_t::value_type::value_type(2));
	l.push(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_two_items_pop_two, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push(val2);
	typename container_t::value_type ret2(l.top());
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret1(l.top());
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_three_items_pop_three, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push(val2);
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	l.push(val3);
	typename container_t::value_type ret3(l.top());
	BOOST_CHECK_EQUAL(val3, ret3);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 3U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	typename container_t::value_type ret2(l.top());
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret1(l.top());
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(empty_begin_is_end, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.begin(), l.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(one_item_begin_is_not_end, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push(val);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_NE(l.begin(), l.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(one_item_begin_is_first, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push(val);
	BOOST_CHECK_EQUAL(*l.begin(), val);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(one_item_next_is_end, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push(val);
	typename container_t::iterator iter= l.begin();
	++iter;
	BOOST_CHECK_NE(iter, l.begin());
	BOOST_CHECK_EQUAL(iter, l.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(erase_item_size_one, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push(val);
	l.erase(l.begin());
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(erase_item_size_two, Mdl, thread_types) {
	typedef intrusive::stack<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push(val1);
	l.erase(l.begin());
	BOOST_CHECK(l.empty());
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push(val2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(slist)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_one_item, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push_back(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_and_pop_front_one_item, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push_back(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_two_items, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_back(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push_back(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_two_items_pop_one1, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_back(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val1, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	BOOST_CHECK(l.tail_reachable_from_head());
	l.push_back(val2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_two_items_pop_one2, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_back(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	BOOST_CHECK(l.tail_reachable_from_head());
	l.push_back(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val1, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	BOOST_CHECK(l.tail_reachable_from_head());
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_two_items_pop_two, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_back(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push_back(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	typename container_t::value_type ret1(l.front());
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret2(l.front());
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_back_three_items_pop_three, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_back(val1);
	BOOST_CHECK(l.tail_reachable_from_head());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK_EQUAL(l.size_n(), 1U);
	typename container_t::value_type should_be_val1_0(l.front());
	BOOST_CHECK_EQUAL(val1, should_be_val1_0);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_back(val2);
	BOOST_CHECK(l.tail_reachable_from_head());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	BOOST_CHECK_EQUAL(l.size_n(), 2U);
	typename container_t::value_type should_be_val1_1(l.front());
	BOOST_CHECK_EQUAL(val1, should_be_val1_1);
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	l.push_back(val3);
	BOOST_CHECK(l.tail_reachable_from_head());
	typename container_t::value_type should_be_val1_2(l.front());
	BOOST_CHECK_EQUAL(should_be_val1_2.get().get()->sp_count(), 5);
	BOOST_CHECK_EQUAL(val1, should_be_val1_2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 3U);
	BOOST_CHECK_EQUAL(l.size_n(), 3U);
	l.pop_front();
	BOOST_CHECK(l.tail_reachable_from_head());
	BOOST_CHECK_EQUAL(should_be_val1_2.get().get()->sp_count(), 4);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	typename container_t::value_type should_be_val2(l.front());
	BOOST_CHECK_EQUAL(should_be_val2.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2, should_be_val2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK(l.tail_reachable_from_head());
	BOOST_CHECK_EQUAL(should_be_val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type should_be_val3(l.front());
	BOOST_CHECK_EQUAL(should_be_val3.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val3, should_be_val3);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(should_be_val1_2.get().get()->sp_count(), 4);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 4);
	BOOST_CHECK_EQUAL(should_be_val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(should_be_val3.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_one_item, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push_front(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_and_pop_front_one_item, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 1);
	l.push_front(val);
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_two_items, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_front(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push_front(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_and_back_one_item, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_front(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push_back(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_two_items_pop_one1, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 1);
	l.push_front(val1);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 1);
	l.push_front(val2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 3);
	BOOST_CHECK_EQUAL(val2, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_two_items_pop_one2, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type ret(l.front());
	BOOST_CHECK_EQUAL(val1, ret);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_front(val2);
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_two_items_pop_two, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_front(val2);
	typename container_t::value_type ret2(l.front());
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret1(l.front());
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(push_front_three_items_pop_three, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_front(val2);
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	l.push_front(val3);
	typename container_t::value_type ret3(l.front());
	BOOST_CHECK_EQUAL(val3, ret3);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 3U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	typename container_t::value_type ret2(l.front());
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret1(l.front());
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(mixed_push_pop_three_items1, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_back(val2);
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	l.push_front(val3);
	typename container_t::value_type ret3(l.front());
	BOOST_CHECK_EQUAL(val3, ret3);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 3U);
	BOOST_CHECK(l.tail_reachable_from_head());
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	typename container_t::value_type ret1(l.front());
	BOOST_CHECK_EQUAL(val1, ret1);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 2U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	typename container_t::value_type ret2(l.front());
	BOOST_CHECK_EQUAL(val2, ret2);
	BOOST_CHECK(!l.empty());
	BOOST_CHECK_EQUAL(l.size(), 1U);
	l.pop_front();
	BOOST_CHECK_EQUAL(val1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret1.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret2.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(val3.get().get()->sp_count(), 2);
	BOOST_CHECK_EQUAL(ret3.get().get()->sp_count(), 2);
	BOOST_CHECK(l.empty());
	BOOST_CHECK_EQUAL(l.size(), 0U);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(find_item_size_one, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push_front(val);
	typename container_t::iterator i(std::find(l.begin(), l.end(), val));
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val);
	BOOST_CHECK_EQUAL(i->i, val->i);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(not_find_wrong_item_size_one, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_front(val1);
	typename container_t::iterator i(std::find(l.begin(), l.end(), val2));
	BOOST_CHECK_EQUAL(i, l.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(not_find_item_popped_size_one, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val(new typename container_t::value_type::value_type(1));
	l.push_front(val);
	typename container_t::iterator i(std::find(l.begin(), l.end(), val));
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val);
	BOOST_CHECK_EQUAL(i->i, val->i);
	l.pop_front();
	typename container_t::iterator i1(std::find(l.begin(), l.end(), val));
	BOOST_CHECK_EQUAL(i1, l.end());
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(not_find_item_popped_size_two, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_front(val2);
	typename container_t::iterator i2_1(std::find(l.begin(), l.end(), val2));
	BOOST_CHECK_NE(i2_1, l.end());
	BOOST_CHECK_EQUAL(*i2_1, val2);
	BOOST_CHECK_EQUAL(i2_1->i, val2->i);
	l.pop_front();
	typename container_t::iterator i2_2(std::find(l.begin(), l.end(), val2));
	BOOST_CHECK_EQUAL(i2_2, l.end());
	typename container_t::iterator i1(std::find(l.begin(), l.end(), val1));
	BOOST_CHECK_NE(i1, l.end());
	BOOST_CHECK_EQUAL(*i1, val1);
	BOOST_CHECK_EQUAL(i1->i, val1->i);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(find_item_size_two, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	l.push_front(val1);
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	l.push_back(val2);
	typename container_t::iterator i1(std::find(l.begin(), l.end(), val1));
	BOOST_CHECK_NE(i1, l.end());
	BOOST_CHECK_EQUAL(*i1, val1);
	BOOST_CHECK_EQUAL(i1->i, val1->i);
	typename container_t::iterator i2(std::find(l.begin(), l.end(), val2));
	BOOST_CHECK_NE(i2, l.end());
	BOOST_CHECK_EQUAL(*i2, val2);
	BOOST_CHECK_EQUAL(i2->i, val2->i);
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_iterator_walks_through_list_three_items, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	l.push_back(val2);
	l.push_front(val1);
	l.push_back(val3);
	typename container_t::iterator i(l.begin());
	BOOST_CHECK_EQUAL(*i, val1);
	BOOST_CHECK_EQUAL(i->i, val1->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val2);
	BOOST_CHECK_EQUAL(i->i, val2->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val3);
	BOOST_CHECK_EQUAL(i->i, val3->i);
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_iterator_walks_through_list_n_items, Mdl, thread_types) {
	typedef intrusive::slist<data<api_lock_traits<platform_api, Mdl>>, api_lock_traits<platform_api, Mdl>> container_t;

	container_t l;
	typename container_t::value_type val1(new typename container_t::value_type::value_type(1));
	typename container_t::value_type val2(new typename container_t::value_type::value_type(2));
	typename container_t::value_type val3(new typename container_t::value_type::value_type(3));
	typename container_t::value_type val4(new typename container_t::value_type::value_type(4));
	typename container_t::value_type val5(new typename container_t::value_type::value_type(5));
	typename container_t::value_type val6(new typename container_t::value_type::value_type(6));
	l.push_back(val3);
	l.push_front(val2);
	l.push_back(val4);
	l.push_back(val5);
	l.push_front(val1);
	l.push_back(val6);
	typename container_t::iterator i(l.begin());
	BOOST_CHECK_EQUAL(*i, val1);
	BOOST_CHECK_EQUAL(i->i, val1->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val2);
	BOOST_CHECK_EQUAL(i->i, val2->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val3);
	BOOST_CHECK_EQUAL(i->i, val3->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val4);
	BOOST_CHECK_EQUAL(i->i, val4->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val5);
	BOOST_CHECK_EQUAL(i->i, val5->i);
	++i;
	BOOST_CHECK_NE(i, l.end());
	BOOST_CHECK_EQUAL(*i, val6);
	BOOST_CHECK_EQUAL(i->i, val6->i);
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	++i;
	BOOST_CHECK_EQUAL(i, l.end());
	BOOST_CHECK(l.tail_reachable_from_head());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
