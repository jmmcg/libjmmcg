/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/lfsr_counter.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(lfsr_counter_tests)

BOOST_AUTO_TEST_SUITE(bits8)

using lfsr_t= lfsr::maximal<8U>;

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 0x80);
	BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 0x1);
	BOOST_CHECK_EQUAL(lfsr_t::period(2), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 0x95);
	BOOST_CHECK_EQUAL(lfsr_t::period(42), lfsr_t::max());
}

BOOST_AUTO_TEST_SUITE(secure)

using secure_t= libjmmcg::lfsr::secure<lfsr_t::size>;

BOOST_AUTO_TEST_CASE(ctor) {
	BOOST_CHECK_NO_THROW(secure_t sec{});
}

BOOST_AUTO_TEST_CASE(next_1) {
	secure_t sec(42);
	BOOST_CHECK_NE(sec.next(), 0);
}

BOOST_AUTO_TEST_CASE(next_2) {
	secure_t sec(42);
	secure_t::element_type const next0= sec.next();
	BOOST_CHECK_NE(next0, 0);
	secure_t::element_type const next1= sec.next();
	BOOST_CHECK_NE(next1, 0);
	BOOST_CHECK_NE(next0, next1);
}

BOOST_AUTO_TEST_CASE(two_same_seed_next_2) {
	secure_t sec0(42);
	secure_t sec1(42);
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_EQUAL(next00, next10);
	BOOST_CHECK_EQUAL(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_CASE(two_different_seeds_next_2) {
	secure_t sec0(42);
	secure_t sec1(69);
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_NE(next00, next10);
	BOOST_CHECK_NE(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_CASE(two_different_seeds_from_entropy_next_2) {
	secure_t sec0;
	secure_t sec1;
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_NE(next00, next10);
	BOOST_CHECK_NE(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits9)

using lfsr_t= lfsr::maximal<9U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 256);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 277);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits10)

using lfsr_t= lfsr::maximal<10U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 512);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 21);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits16)

using lfsr_t= lfsr::maximal<16U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 513);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1026);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 21544);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits32)

using lfsr_t= lfsr::maximal<32U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	// Takes minutes in release builds, too slow. Verified as maximal.
	// BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 2147483648);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 2147483649);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 2147483669);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits44)

using lfsr_t= lfsr::maximal<44U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_WARN("44-bit LFSR: Maximality has not been verified.");
	//	BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1).to_ulong(), 8796093022208UL);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2).to_ulong(), 1UL);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42).to_ulong(), 21UL);
}

BOOST_AUTO_TEST_SUITE(secure)

using secure_t= libjmmcg::lfsr::secure<lfsr_t::size>;

BOOST_AUTO_TEST_CASE(ctor) {
	BOOST_CHECK_NO_THROW(secure_t sec{});
}

BOOST_AUTO_TEST_CASE(next_1) {
	secure_t sec(42);
	BOOST_CHECK_NE(sec.next(), 0);
}

BOOST_AUTO_TEST_CASE(next_2) {
	secure_t sec(42);
	secure_t::element_type const next0= sec.next();
	BOOST_CHECK_NE(next0, 0);
	secure_t::element_type const next1= sec.next();
	BOOST_CHECK_NE(next1, 0);
	BOOST_CHECK_NE(next0, next1);
}

BOOST_AUTO_TEST_CASE(two_same_seed_next_2) {
	secure_t sec0(42);
	secure_t sec1(42);
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_EQUAL(next00, next10);
	BOOST_CHECK_EQUAL(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_CASE(two_different_seeds_next_2) {
	secure_t sec0(42);
	secure_t sec1(69);
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_NE(next00, next10);
	BOOST_CHECK_NE(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_CASE(two_different_seeds_from_entropy_next_2) {
	secure_t sec0;
	secure_t sec1;
	secure_t::element_type const next00= sec0.next();
	BOOST_CHECK_NE(next00, 0);
	secure_t::element_type const next01= sec0.next();
	BOOST_CHECK_NE(next01, 0);
	BOOST_CHECK_NE(next00, next01);
	secure_t::element_type const next10= sec1.next();
	BOOST_CHECK_NE(next10, 0);
	secure_t::element_type const next11= sec1.next();
	BOOST_CHECK_NE(next11, 0);
	BOOST_CHECK_NE(next10, next11);

	BOOST_CHECK_NE(next00, next10);
	BOOST_CHECK_NE(next01, next11);
	BOOST_CHECK_NE(next00, next11);
	BOOST_CHECK_NE(next01, next10);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits46)

using lfsr_t= lfsr::maximal<46U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_WARN("46-bit LFSR: Maximality has not been verified.");
	// BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1).to_ulong(), 35184372088832UL);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 21);
}

BOOST_AUTO_TEST_CASE(check_value_44257) {
	BOOST_CHECK_EQUAL(lfsr_t::next(44257).to_ulong(), 35184372110960UL);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits62)

using lfsr_t= lfsr::maximal<62U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_WARN("62-bit LFSR: Maximality has not been verified.");
	// BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1).to_ulong(), 2305843009213693952U);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2).to_ulong(), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42).to_ulong(), 2305843009213693973U);
}

BOOST_AUTO_TEST_CASE(check_value_44257) {
	BOOST_CHECK_EQUAL(lfsr_t::next(44257).to_ulong(), 2305843009213716080U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits64)

using lfsr_t= lfsr::maximal<64U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_WARN("64-bit LFSR: Maximality has not been verified.");
	// BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	BOOST_CHECK_EQUAL(lfsr_t::next(1), 9223372036854775808U);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 21);
}

BOOST_AUTO_TEST_CASE(check_value_44257) {
	BOOST_CHECK_EQUAL(lfsr_t::next(44257), 9223372036854797936U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(bits128)

using lfsr_t= lfsr::maximal<128U>;

BOOST_AUTO_TEST_CASE(check_maximal) {
	BOOST_WARN("128-bit LFSR: Maximality has not been verified.");
	// BOOST_CHECK_EQUAL(lfsr_t::period(1), lfsr_t::max());
}

BOOST_AUTO_TEST_CASE(check_value_1) {
	std::ostringstream os;
	os << lfsr_t::next(1);
	BOOST_CHECK_EQUAL(os.str(), "92233720368547758080");
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(lfsr_t::next(2), 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(lfsr_t::next(42), 21);
}

BOOST_AUTO_TEST_CASE(check_value_44257) {
	std::ostringstream os;
	os << lfsr_t::next(44257);
	BOOST_CHECK_EQUAL(os.str(), "922337203685477580822128");
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
