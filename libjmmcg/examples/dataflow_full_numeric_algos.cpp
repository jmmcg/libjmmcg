/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_normal_lifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct crew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename crew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type crew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2> >
	finite_test_types;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	// TODO	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	// TODO	crew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	crew_priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading, 0, 2> >
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

template<typename T>
struct square {
	static T last;

	void operator()(T t) {
		last+= t;
	};
};

template<typename T>
T square<T>::last;

inline int
sqr() {
	static int last= 0;
	return last++ << 1;
}

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable_dataflow)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(empty_colln)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	BOOST_CHECK_EQUAL(*context, 0);
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(nonempty_colln)

BOOST_AUTO_TEST_SUITE(one_element)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.size(), 1U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(two_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 3);
	BOOST_CHECK_EQUAL(v.size(), 2U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 2);
	BOOST_CHECK_EQUAL(v.size(), 2U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(n_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 36);
	BOOST_CHECK_EQUAL(v.size(), 8U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 40320);
	BOOST_CHECK_EQUAL(v.size(), 8U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_SUITE(empty_colln)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 0);
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(nonempty_colln)

BOOST_AUTO_TEST_SUITE(one_element)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.size(), 1U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 1);
	BOOST_CHECK_EQUAL(v.size(), 1U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(two_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 3);
	BOOST_CHECK_EQUAL(v.size(), 2U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.accumulate(v, 1, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 2);
	BOOST_CHECK_EQUAL(v.size(), 2U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(n_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.accumulate(v, typename vtr_colln_t::value_type());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 36);
	BOOST_CHECK_EQUAL(v.size(), 8U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(accumulate_op, T, infinite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.accumulate(v, 1L, std::multiplies<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	BOOST_CHECK_EQUAL(*context, 40320);
	BOOST_CHECK_EQUAL(v.size(), 8U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
