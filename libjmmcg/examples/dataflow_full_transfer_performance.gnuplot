## $Header$
##
## Copyright (c) 2016 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

set macros
datafile = 'dataflow_full_transfer_performance.csv'
set datafile separator '|'
set title "Comparison of horizontal and vertical dataflow performance.\nError-bars: % average deviation."
firstrow = system("head -n1 " . datafile . " | sed 's/ /_/g' | sed 's/|/ /g'")
set xlabel word(firstrow, 1) noenhance
set ylabel word(firstrow, 8) noenhance
set key autotitle columnhead noenhance
set key font ",9" noenhance font "Helvetica,20"
set xtics font ",9" noenhance font "Helvetica,20"
set ytics font ",9" noenhance font "Helvetica,20"
set yrange [0:*]
set rmargin at screen 0.95
set bmargin at screen 0.12
set term svg size 1280,1024 noenhance font "Helvetica,20"
set output 'dataflow_full_transfer_performance.svg'
plot	\
	datafile using 1:2:($2*$3) with yerrorbars linecolor 1,	\
	datafile using 1:2:xticlabels(1) notitle with lines linecolor 1,	\
	datafile using 1:4:($4*$5) with yerrorbars linecolor 2,	\
	datafile using 1:4:xticlabels(1) notitle with lines linecolor 2,	\
	datafile using 1:6:($6*$7) with yerrorbars linecolor 3,	\
	datafile using 1:6:xticlabels(1) notitle with lines linecolor 3
