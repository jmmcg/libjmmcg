/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/fibonacci.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(fibonacci_tests)

BOOST_AUTO_TEST_SUITE(dyn)

BOOST_AUTO_TEST_CASE(fibonacci_0) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(0), 1ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(0), 1ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_1) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(1), 1ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(1), 1ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_2) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(2), 2ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(2), 2ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_3) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(3), 3ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(3), 3ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_4) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(4), 5ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(4), 5ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_5) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(5), 8ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(5), 8ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_6) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(6), 13ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(6), 13ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_7) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(7), 21ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(7), 21ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_8) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(8), 34ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(8), 34ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_9) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(9), 55ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(9), 55ull);
}

BOOST_AUTO_TEST_CASE(fibonacci_10) {
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci::result(10), 89ull);
	BOOST_CHECK_EQUAL(libjmmcg::dyn::fibonacci_pe::result(10), 89ull);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
