/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/prime_numbers.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(prime_numbers_tests)

BOOST_AUTO_TEST_CASE(zero_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(0);
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(zero_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 0>::result();
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(one_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(1);
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(one_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 1>::result();
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(two_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(2);
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(two_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 2>::result();
	BOOST_CHECK(primes.empty());
}

BOOST_AUTO_TEST_CASE(three_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(3);
	BOOST_CHECK_EQUAL(primes.size(), 1UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
}

BOOST_AUTO_TEST_CASE(three_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 3>::result();
	BOOST_CHECK_EQUAL(primes.size(), 1UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
}

BOOST_AUTO_TEST_CASE(four_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(4);
	BOOST_CHECK_EQUAL(primes.size(), 2UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
	BOOST_CHECK_EQUAL(primes[1], 3UL);
}

BOOST_AUTO_TEST_CASE(four_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 4>::result();
	BOOST_CHECK_EQUAL(primes.size(), 2UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
	BOOST_CHECK_EQUAL(primes[1], 3UL);
}

BOOST_AUTO_TEST_CASE(ten_dyn) {
	const auto primes= dyn::sieve_of_eratosthenes(10);
	BOOST_CHECK_EQUAL(primes.size(), 4UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
	BOOST_CHECK_EQUAL(primes[1], 3UL);
	BOOST_CHECK_EQUAL(primes[2], 5UL);
	BOOST_CHECK_EQUAL(primes[3], 7UL);
}

BOOST_AUTO_TEST_CASE(ten_mpl) {
	const auto primes= mpl::sieve_of_eratosthenes<unsigned long, 10>::result();
	BOOST_CHECK_EQUAL(primes.size(), 4UL);
	BOOST_CHECK_EQUAL(primes[0], 2UL);
	BOOST_CHECK_EQUAL(primes[1], 3UL);
	BOOST_CHECK_EQUAL(primes[2], 5UL);
	BOOST_CHECK_EQUAL(primes[3], 7UL);
}

BOOST_AUTO_TEST_SUITE_END()
