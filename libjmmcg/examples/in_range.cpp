/******************************************************************************
** Copyright © 2025 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/in_range.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(in_range_tests)

BOOST_AUTO_TEST_CASE(assorted) {
	BOOST_CHECK(in_range(1, 3, 2));
	BOOST_CHECK(in_range(-1, 3, 2));
	BOOST_CHECK(in_range(-3, 3, -2));
	BOOST_CHECK(in_range(-3, 0, -2));
	BOOST_CHECK(in_range(-3, -1, -2));
	BOOST_CHECK(in_range(-8, -3.2, -5.682857142857733));
	BOOST_CHECK(!in_range(1, 2, 3));
	BOOST_CHECK(!in_range(1, 2, 0));
	BOOST_CHECK(!in_range(-1, 3, -2));
	BOOST_CHECK(!in_range(-3, 3, 5));
	BOOST_CHECK(!in_range(-3, 3, -5));
	BOOST_CHECK(!in_range(-3, 0, -5));
	BOOST_CHECK(!in_range(-3, 0, 5));
	BOOST_CHECK(!in_range(-3, -1, -5));
	BOOST_CHECK(!in_range(-3, -1, 0));
}

BOOST_AUTO_TEST_SUITE_END()
