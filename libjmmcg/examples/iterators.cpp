/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/csv_iterator.hpp"
#include "core/line_iterator.hpp"

#include <algorithm>
#include <sstream>
#include <vector>

using namespace libjmmcg;

constexpr char data[]= "k, l, m, 10, 11, 12\nd, e, f, 4, 5, 6\nh, i, j, 7, 8, 9\na, b, c, 1, 2, 3\n";

BOOST_AUTO_TEST_SUITE(iterator_tests)

BOOST_AUTO_TEST_CASE(ctor_eof) {
	BOOST_CHECK_NO_THROW(line_iterator eof);
}

BOOST_AUTO_TEST_CASE(ctor_eol) {
	BOOST_CHECK_NO_THROW(line_iterator eof);
}

BOOST_AUTO_TEST_CASE(basic) {
	std::stringstream ss;
	ss << data;
	line_iterator it(ss), eof;
	std::vector<std::string> data(it, eof);

	// Sort it to ensure that we are not just pretending.
	std::sort(data.begin(), data.end());

	for(auto const& line: data) {
		BOOST_CHECK(!line.empty());
		const csv_iterator eol;
		for(csv_iterator csv_it(line); csv_it != eol; ++csv_it) {
			BOOST_CHECK(!csv_it->empty());
		}
	}
}

BOOST_AUTO_TEST_CASE(stream) {
	std::stringstream ss;
	ss << data;
	line_iterator it(ss), eof;
	while(it != eof) {
		std::string const line= *it++;
		BOOST_CHECK(!line.empty());
		const csv_iterator eol;
		for(csv_iterator csv_it(line); csv_it != eol; ++csv_it) {
			BOOST_CHECK(!csv_it->empty());
		}
	}
}

BOOST_AUTO_TEST_CASE(select_2nd_element) {
	std::stringstream ss;
	ss << data;
	line_iterator it(ss), eof;
	std::string const line= *it;
	BOOST_CHECK(!line.empty());
	const csv_iterator eol;
	csv_iterator csv_it(line);
	auto val_iter= std::next(csv_it);
	BOOST_CHECK_EQUAL(*val_iter, " l");
}

BOOST_AUTO_TEST_SUITE_END()
