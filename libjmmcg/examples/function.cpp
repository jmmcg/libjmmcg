/******************************************************************************
** Copyright © 2025 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/function.hpp"

#include <queue>
#include <string>

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(function_tests)

BOOST_AUTO_TEST_CASE(ctor1) {
	using fn1_t= function<void(), 1>;
	BOOST_CHECK_NO_THROW([[maybe_unused]] fn1_t fn1{[]() {}});
}

BOOST_AUTO_TEST_CASE(ctor2) {
	using fn1_t= function<std::string(), 1>;
	BOOST_CHECK_NO_THROW([[maybe_unused]] fn1_t fn1{[]() {
		return std::string("fn1");
	}});
}

BOOST_AUTO_TEST_CASE(ctor3) {
	using fn1_t= function<std::string(int), 1>;
	fn1_t fn1{[](int i) {
		return std::string("fn1_") + std::to_string(i);
	}};
	BOOST_CHECK_EQUAL(fn1(42), "fn1_42");
	BOOST_CHECK_NE(fn1.hash(), 0);
	BOOST_CHECK_EQUAL(fn1.hash(), std::hash<fn1_t>()(fn1));
}

BOOST_AUTO_TEST_CASE(ctor_non_POD_lamdba_capture) {
	using fn1_t= function<std::string(), 32>;
	using fn2_t= function<std::string(), fn1_t::capacity + 1>;
	std::string str{"42"};
	fn1_t fn1{[str]() {
		return "fn1_" + str;
	}};
	BOOST_CHECK_EQUAL(fn1(), "fn1_42");
	BOOST_CHECK_NE(fn1.hash(), 0);
	BOOST_CHECK_EQUAL(fn1.hash(), std::hash<fn1_t>()(fn1));
	fn2_t fn2{fn1};
	BOOST_CHECK_EQUAL(fn2(), "fn1_42");
	BOOST_CHECK_NE(fn2.hash(), 0);
	BOOST_CHECK_EQUAL(fn2.hash(), std::hash<fn2_t>()(fn2));
	BOOST_CHECK_NE(fn1.hash(), fn2.hash());
}

BOOST_AUTO_TEST_CASE(ctor_fun_lambda_capture) {
	using fn1_t= function<std::string(), 32>;
	using fn2_t= function<std::string(), fn1_t::capacity + 39>;
	std::string str{"42"};
	fn1_t fn1{[str]() {
		return "fn1_" + str;
	}};
	BOOST_CHECK_EQUAL(fn1(), "fn1_42");
	fn2_t fn2{[fn1, s= str + "_"]() {
		return "fn2_" + s + fn1();
	}};
	BOOST_CHECK_EQUAL(fn2(), "fn2_42_fn1_42");
}

BOOST_AUTO_TEST_CASE(ctor5) {
	std::string str{"42"};
	auto fn1= make_function([str]() {
		return "fn1_" + str;
	});
	BOOST_CHECK_EQUAL(fn1(), "fn1_42");
	auto fn2= make_function([fn1, s= str + "_"]() {
		return "fn2_" + s + fn1();
	});
	BOOST_CHECK_EQUAL(fn2(), "fn2_42_fn1_42");
}

BOOST_AUTO_TEST_CASE(ctor6) {
	auto fn1= make_function([](int i) {
		return std::string("fn1_") + std::to_string(i);
	});
	BOOST_CHECK_EQUAL(fn1(42), "fn1_42");
}

BOOST_AUTO_TEST_CASE(ctor7) {
	using fn1_t= function<void(int&), 1>;
	fn1_t fn1= make_function([](int& i) {
		++i;
	});
	int i= 41;
	fn1(i);
	BOOST_CHECK_EQUAL(i, 42);
}

BOOST_AUTO_TEST_CASE(cctor) {
	using fn1_t= function<std::string(), 1>;
	fn1_t fn1{[]() -> std::string {
		return "fn1";
	}};
	fn1_t fn2{fn1};
	BOOST_CHECK_EQUAL(fn2(), "fn1");
}

BOOST_AUTO_TEST_CASE(queue) {
	using fn_t= function<std::string(), 1024>;

	int i= 42;
	std::string str{"bar"};
	std::queue<fn_t> que;
	que.emplace([]() -> std::string {
		return "foo1";
	});
	que.emplace([]() -> std::string {
		return "foo2";
	});
	que.emplace([i]() -> std::string {
		return "foo3_" + std::to_string(i);
	});
	que.emplace([str]() -> std::string {
		return "foo4_" + str;
	});
	{
		fn_t fn5([str]() -> std::string {
			return "foo5_" + str;
		});
		que.emplace(std::move(fn5));
	}
	que.pop();
	BOOST_CHECK_EQUAL(que.front()(), "foo2");
	que.pop();
	BOOST_CHECK_EQUAL(que.front()(), "foo3_42");
	que.pop();
	BOOST_CHECK_EQUAL(que.front()(), "foo4_bar");
	que.pop();
	BOOST_CHECK_EQUAL(que.front()(), "foo5_bar");
}

BOOST_AUTO_TEST_SUITE_END()
