/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ttypes.hpp"

using namespace libjmmcg;

typedef boost::mpl::list<
	char[3],
	char[1024]>
	buffer_types;

BOOST_AUTO_TEST_SUITE(conversions_tests)

BOOST_AUTO_TEST_SUITE(tostrings)

BOOST_AUTO_TEST_SUITE(as_ptr)

BOOST_AUTO_TEST_CASE_TEMPLATE(zero, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(0, buffer, sizeof(buffer));
	BOOST_CHECK_EQUAL(tostring(0), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(one, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(1, buffer, sizeof(buffer));
	BOOST_CHECK_EQUAL(tostring(1), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(2, buffer, sizeof(buffer));
	BOOST_CHECK_EQUAL(tostring(2), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ten, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(10, buffer, sizeof(buffer));
	BOOST_CHECK_EQUAL(tostring(10), buffer);
	BOOST_CHECK_EQUAL(2, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fourty_two, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(42, buffer, sizeof(buffer));
	BOOST_CHECK_EQUAL(tostring(42), buffer);
	BOOST_CHECK_EQUAL(2, len);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(as_array)

BOOST_AUTO_TEST_CASE_TEMPLATE(zero, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(0UL, buffer);
	BOOST_CHECK_EQUAL(tostring(0), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(one, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(1UL, buffer);
	BOOST_CHECK_EQUAL(tostring(1), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(2UL, buffer);
	BOOST_CHECK_EQUAL(tostring(2), buffer);
	BOOST_CHECK_EQUAL(1, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ten, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(10UL, buffer);
	BOOST_CHECK_EQUAL(tostring(10), buffer);
	BOOST_CHECK_EQUAL(2, len);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(fourty_two, buffer_t, buffer_types) {
	buffer_t buffer{};
	const auto len= tostring(42UL, buffer);
	BOOST_CHECK_EQUAL(tostring(42), buffer);
	BOOST_CHECK_EQUAL(2, len);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(fromstrings)

BOOST_AUTO_TEST_CASE(zero) {
	const char buff[]= "0";
	const long res0= fromstring<int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res0);
	const long res0u= fromstring<unsigned int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res0u);
	const long res1= fromstring<long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res1);
	const long res1u= fromstring<unsigned long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res1u);
	const long res2s= fromstring<std::size_t>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res2s);
	const double res2= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(0, res2);
}

BOOST_AUTO_TEST_CASE(one) {
	const char buff[]= "1";
	const long res0= fromstring<int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res0);
	const long res0u= fromstring<unsigned int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res0u);
	const long res1= fromstring<long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res1);
	const long res1u= fromstring<unsigned long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res1u);
	const long res2s= fromstring<std::size_t>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res2s);
	const double res2= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(1, res2);
}

BOOST_AUTO_TEST_CASE(two) {
	const char buff[]= "2";
	const long res0= fromstring<int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res0);
	const long res0u= fromstring<unsigned int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res0u);
	const long res1= fromstring<long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res1);
	const long res1u= fromstring<unsigned long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res1u);
	const long res2s= fromstring<std::size_t>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res2s);
	const double res2= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2, res2);
}

BOOST_AUTO_TEST_CASE(e) {
	const char buff[]= "2.71828182846";
	const double res= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(2.71828182846, res);
}

BOOST_AUTO_TEST_CASE(pi) {
	const char buff[]= "3.14159265359";
	const double res= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(3.14159265359, res);
}

BOOST_AUTO_TEST_CASE(ten) {
	const char buff[]= "10";
	const long res1= fromstring<long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(10, res1);
	const double res2= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(10, res2);
}

BOOST_AUTO_TEST_CASE(fourtytwo) {
	const char buff[]= "42";
	const long res0= fromstring<int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res0);
	const long res0u= fromstring<unsigned int>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res0u);
	const long res1= fromstring<long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res1);
	const long res1u= fromstring<unsigned long>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res1u);
	const long res2s= fromstring<std::size_t>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res2s);
	const double res2= fromstring<double>(buff, sizeof(buff) - 1);
	BOOST_CHECK_EQUAL(42, res2);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
