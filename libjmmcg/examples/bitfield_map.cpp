/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/graph/graph_concepts.hpp>
#include <boost/test/included/unit_test.hpp>

#include "core/bitfield_map.hpp"

#include <boost/mpl/map.hpp>

using namespace libjmmcg;

enum bitfield_3_positions : std::uint8_t {
	pos1= (0x1U << 0),
	pos2= (0x1U << 1),
	pos3= (0x1U << 2)
};

struct p1_t {
	std::uint8_t i= 1;
};

struct p2_t {
	std::uint32_t i= 2;
};

typedef boost::mpl::map<
	boost::mpl::pair<
		std::integral_constant<bitfield_3_positions, bitfield_3_positions::pos1>::type,
		int>,
	boost::mpl::pair<
		std::integral_constant<bitfield_3_positions, bitfield_3_positions::pos2>::type,
		p1_t>,
	boost::mpl::pair<
		std::integral_constant<bitfield_3_positions, bitfield_3_positions::pos3>::type,
		p2_t> >
	bitfield_3_to_type_map;

BOOST_AUTO_TEST_SUITE(bitfield_map_tests)

BOOST_AUTO_TEST_CASE(ctor) {
	using m_t= bitfield_map<bitfield_3_to_type_map>;
	BOOST_MPL_ASSERT_RELATION(sizeof(m_t), ==, sizeof(bitfield_3_positions) + sizeof(int) + sizeof(p1_t) + sizeof(p2_t));
	m_t m;
	BOOST_CHECK(m.empty());
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m.max_size(), m.size());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(ctor_move) {
	bitfield_map<bitfield_3_to_type_map> m1;
	bitfield_map<bitfield_3_to_type_map> m2(std::move(m1));
	BOOST_CHECK(m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(cctor) {
	bitfield_map<bitfield_3_to_type_map> m1;
	bitfield_map<bitfield_3_to_type_map> m2(m1);
	BOOST_CHECK(m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(insert_1_item_at_find) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK(!m.empty());
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m.max_size(), m.size());
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(insert_1_move) {
	bitfield_map<bitfield_3_to_type_map> m1;
	BOOST_CHECK_NO_THROW(m1.push_back<bitfield_3_positions::pos1>(42));
	bitfield_map<bitfield_3_to_type_map> m2(std::move(m1));
	BOOST_CHECK(m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(!m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_EQUAL(m2.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(insert_1_cctor) {
	bitfield_map<bitfield_3_to_type_map> m1;
	BOOST_CHECK_NO_THROW(m1.push_back<bitfield_3_positions::pos1>(42));
	bitfield_map<bitfield_3_to_type_map> m2(m1);
	BOOST_CHECK(!m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_EQUAL(m1.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(!m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_EQUAL(m2.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(insert_1_move_assign) {
	bitfield_map<bitfield_3_to_type_map> m1;
	BOOST_CHECK_NO_THROW(m1.push_back<bitfield_3_positions::pos1>(42));
	bitfield_map<bitfield_3_to_type_map> m2;
	BOOST_CHECK_NO_THROW(m2= std::move(m1));
	BOOST_CHECK(m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(!m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_EQUAL(m2.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(insert_2_move_assign) {
	bitfield_map<bitfield_3_to_type_map> m1;
	BOOST_CHECK_NO_THROW(m1.push_back<bitfield_3_positions::pos1>(42));
	bitfield_map<bitfield_3_to_type_map> m2;
	BOOST_CHECK_NO_THROW(m2.push_back<bitfield_3_positions::pos2>(p1_t()));
	BOOST_CHECK_NO_THROW(m2= std::move(m1));
	BOOST_CHECK(!m1.empty());
	BOOST_CHECK_EQUAL(m1.size(), sizeof(bitfield_3_positions) + sizeof(p1_t));
	BOOST_CHECK_GT(m1.max_size(), m1.size());
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.at<bitfield_3_positions::pos2>().i, p1_t().i);
	BOOST_CHECK_THROW(m1.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos2>(), true);
	BOOST_CHECK_EQUAL(m1.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK(!m2.empty());
	BOOST_CHECK_EQUAL(m2.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK_GT(m2.max_size(), m2.size());
	BOOST_CHECK_EQUAL(m2.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m2.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m2.find<bitfield_3_positions::pos3>(), false);
}

BOOST_AUTO_TEST_CASE(clear_1_element) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.clear());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos3>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos3>(), false);
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK(m.empty());
}

BOOST_AUTO_TEST_CASE(at_1_item) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.at<bitfield_3_positions::pos1>());
	BOOST_CHECK_NO_THROW(m.at<bitfield_3_positions::pos1>()= 68);
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos1>(), 68);
}

BOOST_AUTO_TEST_CASE(erase_1_item) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.erase<bitfield_3_positions::pos1>());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK(m.empty());
}

BOOST_AUTO_TEST_CASE(insert_2_items_at_find) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos2>(p1_t()));
	BOOST_CHECK(!m.empty());
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions) + sizeof(int) + sizeof(p1_t));
	BOOST_CHECK_GE(m.max_size(), m.size());
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos2>().i, p1_t().i);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), true);
}

BOOST_AUTO_TEST_CASE(invalid_insert_2_erase_1_element) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos2>(p1_t()));
	BOOST_CHECK_NO_THROW(m.erase<bitfield_3_positions::pos1>());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), true);
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions) + sizeof(p1_t));
	BOOST_CHECK(!m.empty());
}

BOOST_AUTO_TEST_CASE(insert_2_erase_2_elements) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos2>(p1_t()));
	BOOST_CHECK_NO_THROW(m.erase<bitfield_3_positions::pos2>());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_NO_THROW(m.at<bitfield_3_positions::pos1>());
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), true);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions) + sizeof(int));
	BOOST_CHECK(!m.empty());
	BOOST_CHECK_NO_THROW(m.erase<bitfield_3_positions::pos1>());
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos1>(), std::range_error);
	BOOST_CHECK_THROW(m.at<bitfield_3_positions::pos2>(), std::range_error);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos1>(), false);
	BOOST_CHECK_EQUAL(m.find<bitfield_3_positions::pos2>(), false);
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions));
	BOOST_CHECK(m.empty());
}

BOOST_AUTO_TEST_CASE(insert_3_items_at) {
	bitfield_map<bitfield_3_to_type_map> m;
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos1>(42));
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos2>(p1_t()));
	BOOST_CHECK_NO_THROW(m.push_back<bitfield_3_positions::pos3>(p2_t()));
	BOOST_CHECK(!m.empty());
	BOOST_CHECK_EQUAL(m.size(), sizeof(bitfield_3_positions) + sizeof(int) + sizeof(p1_t) + sizeof(p2_t));
	BOOST_CHECK_GE(m.max_size(), m.size());
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos1>(), 42);
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos2>().i, p1_t().i);
	BOOST_CHECK_EQUAL(m.at<bitfield_3_positions::pos3>().i, p2_t().i);
}

BOOST_AUTO_TEST_SUITE_END()
