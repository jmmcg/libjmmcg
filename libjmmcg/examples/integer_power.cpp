/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/integer_power.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(dynamic_integer_powers)

BOOST_AUTO_TEST_CASE(two_pow_zero) {
	BOOST_CHECK_EQUAL(pow<0>(2), 1);
}

BOOST_AUTO_TEST_CASE(two_pow_one) {
	BOOST_CHECK_EQUAL(pow<1>(2), 2);
}

BOOST_AUTO_TEST_CASE(two_pow_two) {
	BOOST_CHECK_EQUAL(pow<2>(2), 4);
}

BOOST_AUTO_TEST_CASE(two_pow_eight) {
	BOOST_CHECK_EQUAL(pow<8>(2), 256);
}

BOOST_AUTO_TEST_CASE(two_pow_minus_one) {
	BOOST_CHECK_EQUAL(pow<-1>(2.0), 0.5);
}

BOOST_AUTO_TEST_CASE(two_pow_minus_two) {
	BOOST_CHECK_EQUAL(pow<-2>(2.0), 0.25);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(static_integer_powers)

BOOST_AUTO_TEST_CASE(two_pow_zero) {
	typedef binary_right_to_left::mpl::pow<2, 0> pow_2_0_t;
	const double res= pow_2_0_t::result;
	BOOST_CHECK_EQUAL(pow<0>(2), res);
}

BOOST_AUTO_TEST_CASE(two_pow_one) {
	typedef binary_right_to_left::mpl::pow<2, 1> pow_2_1_t;
	const double res= pow_2_1_t::result;
	BOOST_CHECK_EQUAL(pow<1>(2), res);
}

BOOST_AUTO_TEST_CASE(two_pow_two) {
	typedef binary_right_to_left::mpl::pow<2, 2> pow_2_2_t;
	const double res= pow_2_2_t::result;
	BOOST_CHECK_EQUAL(pow<2>(2), res);
}

BOOST_AUTO_TEST_CASE(two_pow_eight) {
	typedef binary_right_to_left::mpl::pow<2, 8> pow_2_8_t;
	const double res= pow_2_8_t::result;
	BOOST_CHECK_EQUAL(pow<8>(2), res);
}

BOOST_AUTO_TEST_CASE(two_pow_minus_one) {
	typedef binary_right_to_left::mpl::pow<2, -1> pow_2_n1_t;
	const double res= pow_2_n1_t::result;
	BOOST_CHECK_EQUAL(0, res);
}

BOOST_AUTO_TEST_CASE(two_pow_minus_two) {
	typedef binary_right_to_left::mpl::pow<2, -2> pow_2_n2_t;
	const double res= pow_2_n2_t::result;
	BOOST_CHECK_EQUAL(0, res);
}

BOOST_AUTO_TEST_SUITE_END()
