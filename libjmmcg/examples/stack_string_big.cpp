/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/stack_string.hpp"

using namespace libjmmcg;

using stack_string_12= basic_stack_string<12, char>;

BOOST_AUTO_TEST_SUITE(string_tests)

BOOST_AUTO_TEST_SUITE(big_string)

BOOST_AUTO_TEST_CASE(ctor) {
	char const src[]= "a very very big string";
	BOOST_CHECK_GT(sizeof(src) - 1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(ctor_cctor) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2(s1);
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_move_ctor) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2(std::move(s1));
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string_12::size_type());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_GE(s1.capacity(), stack_string_12::size_type());
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(index) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	BOOST_CHECK_EQUAL(s[0], 'a');
	BOOST_CHECK_EQUAL(s[1], ' ');
	BOOST_CHECK_EQUAL(s[2], 'v');
	BOOST_CHECK_EQUAL(s[3], 'e');
	BOOST_CHECK_EQUAL(s[4], 'r');
	BOOST_CHECK_EQUAL(s[5], 'y');
	BOOST_CHECK_EQUAL(s[6], ' ');
	BOOST_CHECK_EQUAL(s[7], 'v');
	BOOST_CHECK_EQUAL(s[8], 'e');
	BOOST_CHECK_EQUAL(s[9], 'r');
	BOOST_CHECK_EQUAL(s[10], 'y');
	BOOST_CHECK_EQUAL(s[11], ' ');
	BOOST_CHECK_EQUAL(s[12], 'b');
	BOOST_CHECK_EQUAL(s[13], 'i');
	BOOST_CHECK_EQUAL(s[14], 'g');
	BOOST_CHECK_EQUAL(s[15], ' ');
	BOOST_CHECK_EQUAL(s[16], 's');
	BOOST_CHECK_EQUAL(s[17], 't');
	BOOST_CHECK_EQUAL(s[18], 'r');
	BOOST_CHECK_EQUAL(s[19], 'i');
	BOOST_CHECK_EQUAL(s[20], 'n');
	BOOST_CHECK_EQUAL(s[21], 'g');
}

BOOST_AUTO_TEST_CASE(default_ctor_swap) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2;
	s1.swap(s2);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string_12::size_type());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(src, src + sizeof(src), s2.begin()));
	BOOST_CHECK_GE(s1.capacity(), stack_string_12::size_type());
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(ctor_swap) {
	char const src1[]= "a very very big string";
	char const src2[]= "another very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s1.swap(s2);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src2) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src2));
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1) - 1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_GE(s1.capacity(), sizeof(src2) - 1);
	BOOST_CHECK_GE(s2.capacity(), sizeof(src1) - 1);
}

BOOST_AUTO_TEST_CASE(cctor_swap) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2(s1);
	s1.swap(s2);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_assignment) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2;
	s2= s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(two_ctor_assignment) {
	char const src1[]= "a very very big string";
	char const src2[]= "another very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2= s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_move_assignment) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src);
	stack_string_12 s2;
	s2= std::move(s1);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string_12::size_type());
	BOOST_CHECK_EQUAL(s1.capacity(), s1.size());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(two_ctor_move_assignment) {
	char const src1[]= "a very very big string";
	char const src2[]= "another very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2= std::move(s1);
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1) - 1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(self_assignment) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	s= s;
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(self_move_assignment) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	s= std::move(s);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(equality) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src), s2(src);
	BOOST_CHECK(s1 == s2);
}

BOOST_AUTO_TEST_CASE(inequality) {
	char const src1[]= "a very very big string      ";
	char const src2[]= "another very very big string";
	stack_string_12 s1(src1), s2(src2);
	BOOST_CHECK(s1 != s2);
}

BOOST_AUTO_TEST_CASE(inequality_sizes1) {
	char const src1[]= "another very very big string";
	char const src2[]= "a very very big string";
	stack_string_12 s1(src1), s2(src2);
	BOOST_CHECK(s1 != s2);
}

BOOST_AUTO_TEST_CASE(inequality_sizes2) {
	char const src1[]= "a very very big string";
	char const src2[]= "another very very big string";
	stack_string_12 s1(src1), s2(src2);
	BOOST_CHECK(s1 != s2);
}

BOOST_AUTO_TEST_CASE(clear) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	s.clear();
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string_12::size_type());
	BOOST_CHECK_GE(s.capacity(), sizeof(src) - 1);
}

BOOST_AUTO_TEST_CASE(reserve_smaller) {
	const stack_string_12::size_type res(1);
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(reserve_bigger) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res(sizeof(src) + 1);
	stack_string_12 s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK_GE(s.capacity(), res);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(rereserve_bigger) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(sizeof(src) + 2);
	stack_string_12 s(src);
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK_GE(s.capacity(), res2);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(rereserve_smaller) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(sizeof(src) - 2);
	stack_string_12 s(src);
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK_GE(s.capacity(), res1);
}

BOOST_AUTO_TEST_CASE(resize_smaller) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res(sizeof(src) - 1);
	BOOST_CHECK_GT(res, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_bigger) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res(sizeof(src) + 1);
	stack_string_12 s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), std::prev(s.end()), src));
}

BOOST_AUTO_TEST_CASE(reresize_bigger) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(res1 + 1);
	stack_string_12 s(src);
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), std::prev(s.end(), res2 - sizeof(src)), src));
}

BOOST_AUTO_TEST_CASE(reresize_smaller) {
	char const src[]= "a very very big string";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(res1 - 1);
	BOOST_CHECK_GT(res1, stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(res2, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_smaller_init) {
	const stack_string_12::value_type fill= 'X';
	char const src[]= "a very very big string";
	const stack_string_12::size_type res(sizeof(src) - 1);
	BOOST_CHECK_GT(res, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_bigger_init) {
	const stack_string_12::value_type fill= 'X';
	char const src[]= "a very very big string";
	char const result[]= "a very very big stringXX";
	const stack_string_12::size_type res(sizeof(src) + 1);
	BOOST_CHECK_GT(res, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(reresize_bigger_init) {
	const stack_string_12::value_type fill= 'X';
	char const src[]= "a very very big string";
	char const result[]= "a very very big stringXXX";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(res1 + 1);
	BOOST_CHECK_GT(res1, stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(res2, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(reresize_smaller_init) {
	const stack_string_12::value_type fill= 'X';
	char const src[]= "a very very big string";
	char const result[]= "a very very big stringX";
	const stack_string_12::size_type res1(sizeof(src) + 1);
	const stack_string_12::size_type res2(res1 - 1);
	BOOST_CHECK_GT(res1, stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(res2, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(push_back) {
	const stack_string_12::value_type fill= 'X';
	char const src[]= "a very very big string";
	char const result[]= "a very very big stringX";
	stack_string_12 s(src);
	s.push_back(fill);
	BOOST_CHECK_EQUAL(s.size(), sizeof(src));
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(insert_str) {
	const stack_string_12 src("a very very big string");
	BOOST_CHECK_GT(sizeof(src) - 1, stack_string_12::small_string_max_size);
	stack_string_12 s;
	const stack_string_12::iterator i= s.insert(s.end(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), src.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), src.begin()));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(insert_str_end) {
	const stack_string_12 src("a very very big string");
	BOOST_CHECK_GT(sizeof(src) - 1, stack_string_12::small_string_max_size);
	const char result[]= "a very very big stringa very very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.insert(s.end(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin() + src.size());
}

BOOST_AUTO_TEST_CASE(insert_str_begin) {
	const stack_string_12 src("a very very big string");
	BOOST_CHECK_GT(sizeof(src) - 1, stack_string_12::small_string_max_size);
	const char result[]= "a very very big stringa very very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.insert(s.begin(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(insert_str_internal) {
	const stack_string_12 src("a very very big string");
	BOOST_CHECK_GT(sizeof(src) - 1, stack_string_12::small_string_max_size);
	const char result[]= "aa very very big string very very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.insert(std::next(s.begin()), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin()));
}

BOOST_AUTO_TEST_CASE(erase_all) {
	stack_string_12 src("a very very big string");
	const stack_string_12::iterator i= src.erase(src.begin(), src.end());
	BOOST_CHECK(src.empty());
	BOOST_CHECK_EQUAL(src.size(), stack_string_12::size_type());
	BOOST_CHECK_EQUAL(i, src.end());
}

BOOST_AUTO_TEST_CASE(erase_none) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.erase(s.begin(), s.begin());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(erase_begin_one) {
	char const src[]= "a very very big string";
	char const result[]= " very very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.erase(s.begin(), std::next(s.begin()));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(erase_end_one) {
	char const src[]= "a very very big string";
	char const result[]= "a very very big strin";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.erase(std::prev(s.end()), s.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.end());
}

BOOST_AUTO_TEST_CASE(erase_middle_one) {
	char const src[]= "a very very big string";
	char const result[]= "a ery very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.erase(std::next(s.begin(), 2), std::next(s.begin(), 3));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin(), 2));
}

BOOST_AUTO_TEST_CASE(erase_middle_two) {
	char const src[]= "a very very big string";
	char const result[]= "a ry very big string";
	stack_string_12 s(src);
	const stack_string_12::iterator i= s.erase(std::next(s.begin(), 2), std::next(s.begin(), 4));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin(), 2));
}

BOOST_AUTO_TEST_CASE(replace_dest_empty) {
	char const src[]= "a very very big string";
	stack_string_12 s1, s2(src);
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
}

BOOST_AUTO_TEST_CASE(replace_src_empty) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src), s2;
	s1.replace(s1.begin(), s1.end(), s2.begin(), s2.begin());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string_12::size_type());
}

BOOST_AUTO_TEST_CASE(replace_both_empty) {
	char const src[]= "a very very big string";
	stack_string_12 s1(src), s2;
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.begin());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
}

BOOST_AUTO_TEST_CASE(replace_begin_shrink) {
	char const src[]= "a very very big string";
	char const result[]= "aery very big string";
	stack_string_12 s1(src), s2(src);
	s1.replace(s1.begin(), std::next(s1.begin(), 3), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_end_shrink) {
	char const src[]= "a very very big string";
	char const result[]= "a very very big stra";
	stack_string_12 s1(src), s2(src);
	s1.replace(std::prev(s1.end(), 3), s1.end(), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_middle_shrink) {
	char const src[]= "a very very big string";
	char const result[]= "aaery very big string";
	stack_string_12 s1(src), s2(src);
	s1.replace(std::next(s1.begin()), std::next(s1.begin(), 3), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_begin_grow) {
	char const src[]= "a very very big string";
	char const result[]= "a very very big stringvery very big string";
	stack_string_12 s1(src), s2(src);
	s1.replace(s1.begin(), std::next(s1.begin(), 2), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_end_grow) {
	char const src[]= "a very very big string";
	char const result[]= "a very very big stria very very big string";
	stack_string_12 s1(src), s2(src);
	s1.replace(std::prev(s1.end(), 2), s1.end(), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_middle_grow) {
	char const src[]= "a very very big string";
	char const result[]= "a a very very big stringry very big string";
	stack_string_12 s1(src), s2(src);
	s1.replace(std::next(s1.begin(), 2), std::next(s1.begin(), 4), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result) - 1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(insert) {
	char const src[]= "a very very big string";
	stack_string_12 s(src);
	std::stringstream ss;
	ss << s;
	BOOST_CHECK_EQUAL(ss.str(), std::string(src));
}

BOOST_AUTO_TEST_CASE(extract) {
	char const src[]= "a very very big string";
	stack_string_12 s;
	const stack_string_12 res(src);
	std::stringstream ss;
	ss << src;
	ss >> s;
	BOOST_CHECK_EQUAL(s, res);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
