/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

#include <boost/bind/bind.hpp>

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct fifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct lifo_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct priority_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

typedef boost::mpl::list<
	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,

	fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2> >
	finite_test_types;

typedef boost::mpl::list<
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	/* TODO not yet finished coding these types!
		fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
		lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
		priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

		fifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
		lifo_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
		priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	*/
	fifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	lifo_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>,
	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading> >
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

struct res_t {
	long i;
};

struct work_type {
	typedef res_t result_type;

	int i_;

	explicit work_type(const int i)
		: i_(i) {
	}

	void __fastcall process(result_type& r) noexcept(true) {
		r.i= i_ << 1;
	}

	void __fastcall mutate(result_type& r) noexcept(true) {
		process(r);
	}

	bool __fastcall operator<(work_type const& i) const {
		return i_ < i.i_;
	}
};

struct work_type_simple {
	int i_;

	work_type_simple(const int i)
		: i_(i) {
	}

	void __fastcall process(res_t& r) {
		r.i= (i_ << 1);
	}

	void __fastcall mutate(res_t& r) {
		process(r);
	}

	res_t exec() {
		res_t r;
		process(r);
		return r;
	}

	bool __fastcall operator<(work_type_simple const& i) const {
		return i_ < i.i_;
	}
};

struct bad_return_type {};

template<generic_traits::api_type::element_type API, typename Mdl>
struct erase_test_work_type {
	typedef void result_type;

	static inline bool waiting{false};

	void __fastcall process() noexcept(true) {
		while(waiting) {
			api_threading_traits<API, Mdl>::sleep(10);
		}
	}

	bool __fastcall operator<(erase_test_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API>
struct erase_test_work_type<API, sequential_mode> {
	typedef void result_type;

	static inline bool waiting{true};

	void __fastcall process() noexcept(true) {
	}

	bool __fastcall operator<(erase_test_work_type const&) const {
		return true;
	}
};

struct bool_work_type {
	typedef bool result_type;

	int i_;

	explicit bool_work_type(const int i)
		: i_(i) {
	}

	void __fastcall process(result_type& r) const {
		r= static_cast<bool>(i_);
	}

	bool __fastcall operator<(bool_work_type const&) const {
		return true;
	}
};

struct throw_work_type {
	typedef res_t result_type;
	typedef std::runtime_error exception_t;

	void __fastcall process(result_type&) noexcept(false) {
		BOOST_THROW_EXCEPTION(exception_t("test"));
	}

	void __fastcall mutate(result_type& r) noexcept(false) {
		process(r);
	}

	bool __fastcall operator<(throw_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API, typename Mdl>
struct horizontal_work_type {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type(bool& r) noexcept(true)
		: release(r) {
	}

	void __fastcall process() noexcept(false) {
		while(!release) {
			api_threading_traits<API, Mdl>::sleep(10);
		}
	}

	bool __fastcall operator<(horizontal_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API>
struct horizontal_work_type<API, sequential_mode> {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type(bool& r) noexcept(true)
		: release(r) {
	}

	void __fastcall process() noexcept(true) {
	}

	bool __fastcall operator<(horizontal_work_type const&) const {
		return true;
	}
};

template<generic_traits::api_type::element_type API, typename Mdl>
struct horizontal_work_type_rel {
	typedef void result_type;

	bool& release;

	explicit horizontal_work_type_rel(bool& r) noexcept(true)
		: release(r) {}

	void __fastcall process() noexcept(true) {
		release= true;
	}

	bool __fastcall operator<(horizontal_work_type_rel const&) const {
		return true;
	}
};

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(nowait)

BOOST_AUTO_TEST_CASE_TEMPLATE(one_thread, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(1);
	BOOST_CHECK_EQUAL(pool.pool_size(), 1U);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(n_threads, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& exec= pool << joinable() << work_type_simple(1);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_boost_bind, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& exec= pool << joinable() << boost::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_std_bind, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& exec= pool << joinable() << std::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(erase_one_work_after_waiting, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto&& exec= pool << joinable() << work_type(1);
	*exec;
	pool.erase(exec);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(access_erased_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::nonjoinable nonjoinable;
	typedef typename pool_type::joinable joinable;

	typedef erase_test_work_type<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type> erase_wk_t;

	pool_type pool(T::pool_size);
	// Block the pool.
	for(typename pool_type::pool_type::size_type i= 0; i < pool.pool_size(); ++i) {
		pool << nonjoinable() << erase_wk_t();
	}
	// Now this work is added, it will wait in the queue.
	auto&& exec= pool << joinable() << work_type_simple(1);
	pool.erase(exec);
	bool exeception_caught= std::is_same<typename T::thread_pool_traits::os_traits::thread_traits::model_type, sequential_mode>::value;
	try {
		[[maybe_unused]] auto const ret= exec->i;
	} catch(const std::exception&) {
		exeception_caught= true;
	}
	// Release the threads in the pool.
	erase_wk_t::waiting= false;
	api_threading_traits<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type>::sleep(100);
	BOOST_CHECK_EQUAL(exeception_caught, true);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_time_critical, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;
	typedef typename pool_type::template priority<pool_type::api_params_type::time_critical> time_critical;

	pool_type pool(T::pool_size);
	auto const& exec= pool << joinable() << time_critical() << work_type_simple(1);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_throw_exception, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	bool exception_caught= false;
	pool_type pool(T::pool_size);
	try {
		auto const& exec= pool << joinable() << throw_work_type();
		*exec;
	} catch(std::exception const& e) {
		exception_caught= (std::string(e.what()) == "test");
	} catch(...) {
		exception_caught= false;
	}
	BOOST_CHECK_EQUAL(exception_caught, true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	auto const& exec= pool << joinable() << work_type_simple(1);
	auto const& exec2= pool << joinable() << work_type_simple(3);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(exec2->i, 6);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(horizontal_threading, T, finite_test_types) {
	typedef horizontal_work_type<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type> hrz_wk_t;
	typedef horizontal_work_type_rel<T::thread_pool_traits::os_traits::thread_traits::api_params_type::api_type, typename T::thread_pool_traits::os_traits::thread_traits::model_type> hrz_wk_rel_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	bool release= false;
	hrz_wk_t hz(release);
	hrz_wk_rel_t hz_rel(release);
	auto const& exec= pool << joinable() << hz;
	auto const& exec_rel= pool << joinable() << hz_rel;
	*exec;
	*exec_rel;
	BOOST_CHECK_EQUAL(hz.release, true);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unary_fn, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	auto const& exec= pool.unary_fun(bool_work_type(0), std::logical_not<bool_work_type::result_type>());
	BOOST_CHECK_EQUAL(*exec, true);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and_false, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	auto const& exec= pool.logical_and(bool_work_type(0), bool_work_type(2));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(*exec, false);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and_true, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	auto const& exec= pool.logical_and(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(*exec, true);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_or_false, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	auto const& exec= pool.logical_or(bool_work_type(0), bool_work_type(0));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(*exec, false);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_or_true, T, finite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	auto const& exec= pool.logical_or(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(*exec, true);
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_SUITE(nowait)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& exec= pool << joinable() << work_type_simple(1);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_boost_bind, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& exec= pool << joinable() << boost::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_std_bind, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& exec= pool << joinable() << std::bind(&work_type_simple::exec, work_type_simple(1));
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_time_critical, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;
	typedef typename pool_type::template priority<pool_type::api_params_type::time_critical> time_critical;

	pool_type pool;
	auto const& exec= pool << joinable() << time_critical() << work_type_simple(1);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work_throw_exception, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	bool exception_caught= false;
	pool_type pool;
	try {
		auto const& exec= pool << joinable() << throw_work_type();
		*exec;
	} catch(std::exception const& e) {
		exception_caught= (std::string(e.what()) == "test");
	} catch(...) {
		exception_caught= false;
	}
	BOOST_CHECK_EQUAL(exception_caught, true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_two_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool;
	auto const& exec= pool << joinable() << work_type_simple(1);
	auto const& exec2= pool << joinable() << work_type_simple(3);
	BOOST_CHECK_EQUAL(exec->i, 2);
	BOOST_CHECK_EQUAL(exec2->i, 6);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::erew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(generic_traits::memory_access_modes::crew_memory_access), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(generic_traits::memory_access_modes::crew_memory_access), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(unary_fn, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& exec= pool.unary_fun(bool_work_type(1), std::logical_not<bool_work_type::result_type>());
	BOOST_CHECK_EQUAL(*exec, false);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and_false, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& exec= pool.logical_and(bool_work_type(0), bool_work_type(2));
	BOOST_CHECK_EQUAL(*exec, false);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_and_true, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& exec= pool.logical_and(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(*exec, true);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_or_false, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& exec= pool.logical_or(bool_work_type(0), bool_work_type(0));
	BOOST_CHECK_EQUAL(*exec, false);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(logical_or_true, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;

	pool_type pool;
	auto const& exec= pool.logical_or(bool_work_type(1), bool_work_type(2));
	BOOST_CHECK_EQUAL(*exec, true);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
	BOOST_CHECK_EQUAL(pool.min_time(exec), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(exec), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
