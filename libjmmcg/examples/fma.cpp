/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/fma.hpp"

#include <chrono>

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(fma_tests)

BOOST_AUTO_TEST_CASE(all_zero) {
	using namespace fma;

	const double m= 0;
	const double x= 0;
	const double c= 0;
	const double y= dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 0.0);
}

BOOST_AUTO_TEST_CASE(all_zero_commute_multiply) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= m * dbl(x) + c;
	BOOST_CHECK_EQUAL(y, 17.0);
}

BOOST_AUTO_TEST_CASE(all_zero_commute_add) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= c + dbl(x) * m;
	BOOST_CHECK_EQUAL(y, 17.0);
}

BOOST_AUTO_TEST_CASE(all_zero_commute_multipy_and_add) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= c + m * dbl(x);
	BOOST_CHECK_EQUAL(y, 17.0);
}

BOOST_AUTO_TEST_CASE(all_zero_nest) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= dbl(dbl(x) * m + c) * m + c;
	BOOST_CHECK_EQUAL(y, 56.0);
}

BOOST_AUTO_TEST_CASE(m_one_others_zero) {
	using namespace fma;

	const double m= 1;
	const double x= 0;
	const double c= 0;
	const double y= dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 0.0);
}

BOOST_AUTO_TEST_CASE(all_ones) {
	using namespace fma;

	const double m= 1;
	const double x= 1;
	const double c= 1;
	const double y= dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 2.0);
}

BOOST_AUTO_TEST_CASE(one_two) {
	using namespace fma;

	const double m= 1;
	const double x= 2;
	const double c= 1;
	const double y= dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 3.0);
}

BOOST_AUTO_TEST_CASE(all_twos) {
	using namespace fma;

	const double m= 2;
	const double x= 2;
	const double c= 2;
	const double y= dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 6.0);
}

BOOST_AUTO_TEST_CASE(subtract_twos) {
	using namespace fma;

	const double m= 2;
	const double x= 2;
	const double c= 2;
	const double y= dbl(x) * m - c;
	BOOST_CHECK_EQUAL(y, 2.0);
}

BOOST_AUTO_TEST_CASE(associative_subtract_twos) {
	using namespace fma;

	const double m= 2;
	const double x= 2;
	const double c= 2;
	const double y= c - dbl(x) * m;
	BOOST_CHECK_EQUAL(y, -2.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_surrounding_multiplies) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= m * dbl(x) * m + c;
	BOOST_CHECK_EQUAL(y, 41.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_post_multiplies) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= dbl(x) * m * m + c;
	BOOST_CHECK_EQUAL(y, 41.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_preceeding_multiplies) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	const double c= 5;
	const double y= m * m * dbl(x) + c;
	BOOST_CHECK_EQUAL(y, 41.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_dbls_add) {
	using namespace fma;

	const dbl m= 3;
	const double x= 4;
	const dbl c= 5;
	const dbl y= m * m * dbl(x) + c;
	BOOST_CHECK_EQUAL(y, 41.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_dbls_subtract) {
	using namespace fma;

	const dbl m= 3;
	const double x= 4;
	const dbl c= 5;
	const dbl y= m * m * dbl(x) - c;
	BOOST_CHECK_EQUAL(y, 31.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_dbls_associative_add) {
	using namespace fma;

	const dbl m= 2;
	const double x= 3;
	const dbl c= 4;
	const dbl y= c + m * m * dbl(x);
	BOOST_CHECK_EQUAL(y, 16.0);
}

BOOST_AUTO_TEST_CASE(all_zero_two_dbls_associative_subtract) {
	using namespace fma;

	const dbl m= 3;
	const double x= 4;
	const dbl c= 5;
	const dbl y= c - m * m * dbl(x);
	BOOST_CHECK_EQUAL(y, -31.0);
}

BOOST_AUTO_TEST_CASE(multiply_equals_add) {
	using namespace fma;

	const double m= 3;
	const double c= 5;
	double y= 4;
	y*= dbl(m) + c;
	BOOST_CHECK_EQUAL(y, 17.0);
}

BOOST_AUTO_TEST_CASE(multiply_equals_subtract) {
	using namespace fma;

	const double m= 3;
	const double c= 5;
	double y= 4;
	y*= dbl(m) - c;
	BOOST_CHECK_EQUAL(y, 7.0);
}

BOOST_AUTO_TEST_CASE(add_equals) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	double y= 5;
	y+= x * dbl(m);
	BOOST_CHECK_EQUAL(y, 17.0);
}

BOOST_AUTO_TEST_CASE(subtract_equals) {
	using namespace fma;

	const double m= 3;
	const double x= 4;
	double y= 5;
	y-= x * dbl(m);
	BOOST_CHECK_EQUAL(y, 7.0);
}

typedef boost::mpl::list<
	double,
	fma::dbl>
	check_fma_tests;

BOOST_AUTO_TEST_CASE_TEMPLATE(performance_all_zero_commute_multiply, T, check_fma_tests) {
	using timed_results_t= ave_deviation_meter<double>;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 19;
	const unsigned long num_reps= 10000;
#else
	const unsigned long test_size= 2 << 2;
	const unsigned long num_reps= 2;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		0.1,
		num_reps,
		[]() {
			const double m= 3;
			const double x= 4;
			const double c= 5;
			double y= 0;
			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long num_loops= 0; num_loops < test_size; ++num_loops) {
				y+= m * T(x) + c;
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			BOOST_CHECK_EQUAL(y, test_size * 17.0);
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()) / test_size;
		}));
	std::cout << "Time per " << typeid(T).name() << " operation: " << timed_results.first << " nanosec." << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()
