/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/jthread.hpp"
#include "core/latency_timestamps.hpp"
#include "core/max_min.hpp"
#include "core/socket_client_manager.hpp"

using namespace libjmmcg;

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

struct thread_traits {
	using api_threading_traits= ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>;

	struct thread_info {
		const unsigned short core{};
		const api_threading_traits::api_params_type::priority_type priority{};
	};

	static inline constexpr auto exchange_simulator_thread= thread_info{
		0,
		api_threading_traits::api_params_type::priority_type::normal};
};

}}}}

namespace libisimud= isimud::ISIMUD_VER_NAMESPACE;
using namespace libisimud;

#include "core/socket_server.hpp"

using api_thread_traits= ppd::thread_params<ppd::platform_api>;
using timed_results_t= ave_deviation_meter<double>;
const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short unused_port= 12347u;
const std::chrono::milliseconds timeout{5000};

struct msgs_t {
	struct static_size {
		struct [[gnu::packed]] Header_t {
			enum : bool {
				has_static_size= true
			};

			const std::size_t length_;
			std::uint64_t sequence{};

			explicit constexpr Header_t(std::size_t l) noexcept(true)
				: length_(l) {}

			constexpr std::size_t length() const noexcept(true) {
				return length_;
			}

			constexpr bool is_valid() const noexcept(true);
		};

		struct [[gnu::packed]] message : Header_t {
			enum : std::size_t {
				header_t_size= sizeof(Header_t)
			};

			const std::uint8_t c= 42;
			std::array<unsigned char, 123> data{'x'};

			constexpr message() noexcept(true)
				: Header_t(sizeof(message)) {}
		};

		struct [[gnu::packed]] heartbeat : Header_t {
			enum : std::size_t {
				header_t_size= sizeof(Header_t)
			};

			enum : unsigned {
				max_missed_heartbeats= 1
			};

			static inline constexpr const std::chrono::seconds heartbeat_interval{1};

			const uint64_t c= 69;

			constexpr heartbeat() noexcept(true)
				: Header_t(sizeof(heartbeat)) {}
		};

		enum : std::size_t {
			min_msg_size= min<std::size_t, sizeof(message), sizeof(heartbeat)>::value,
			max_msg_size= max<std::size_t, sizeof(message), sizeof(heartbeat)>::value,
			header_t_size= sizeof(Header_t)
		};

		using msg_buffer_t= std::array<std::byte, max_msg_size>;
	};

	struct variable_size {
		struct [[gnu::packed]] Header_t {
			enum : bool {
				has_static_size= false
			};

			const std::size_t length_;
			std::uint64_t sequence{};

			explicit constexpr Header_t(std::size_t l) noexcept(true)
				: length_(l) {}

			constexpr std::size_t length() const noexcept(true) {
				return length_;
			}

			constexpr bool is_valid() const noexcept(true);
		};

		struct [[gnu::packed]] message : Header_t {
			enum : std::size_t {
				header_t_size= sizeof(Header_t)
			};

			const std::uint8_t c= 42;
			std::array<char, 123> data{'x'};

			constexpr message() noexcept(true)
				: Header_t(sizeof(message)) {}

			constexpr bool is_valid() const noexcept(true) {
				return Header_t::is_valid() && length_ == sizeof(message);
			}
		};

		struct [[gnu::packed]] heartbeat : Header_t {
			enum : std::size_t {
				header_t_size= sizeof(Header_t)
			};

			enum : unsigned {
				max_missed_heartbeats= 1
			};

			static inline constexpr const std::chrono::seconds heartbeat_interval{1};

			const std::uint64_t c= 69;

			constexpr heartbeat() noexcept(true)
				: Header_t(sizeof(heartbeat)) {}

			constexpr bool is_valid() const noexcept(true) {
				return Header_t::is_valid() && length_ == sizeof(heartbeat);
			}
		};

		enum : std::size_t {
			min_msg_size= min<std::size_t, sizeof(message), sizeof(heartbeat)>::value,
			max_msg_size= max<std::size_t, sizeof(message), sizeof(heartbeat)>::value,
			header_t_size= sizeof(Header_t)
		};

		using msg_buffer_t= std::array<std::byte, max_msg_size>;
	};
};

constexpr bool
msgs_t::static_size::Header_t::is_valid() const noexcept(true) {
	return length_ >= msgs_t::static_size::min_msg_size && length_ <= msgs_t::static_size::max_msg_size;
}

constexpr bool
msgs_t::variable_size::Header_t::is_valid() const noexcept(true) {
	return length_ >= msgs_t::static_size::min_msg_size && length_ <= msgs_t::static_size::max_msg_size;
}

struct just_connect {
	template<class Fn>
	void operator()(Fn const& f) const {
		const boost::asio::ip::tcp::endpoint endpoint(localhost, unused_port);
		f(endpoint);
	}
};

struct num_msgs {
	unsigned long recv{};
	unsigned long send{};

	friend std::ostream& operator<<(std::ostream& os, num_msgs const& ctrs) noexcept(false) {
		os << "recv=" << ctrs.recv << ", send=" << ctrs.send;
		return os;
	}
};

template<class SktT, class MsgsT>
struct sink {
	using socket_t= SktT;
	using src_msg_details_t= MsgsT;
	using msg_details_t= src_msg_details_t;

	struct heartbeats {
		using report_error_fn_t= std::function<void(std::string svr_hb_details, std::exception_ptr eptr)>;

		enum : unsigned {
			max_missed_heartbeats= 1
		};

		static inline constexpr const std::chrono::seconds heartbeat_interval{1};

		template<class Skt>
		constexpr heartbeats(Skt const&, report_error_fn_t&) noexcept(true) {}

		static constexpr void stop() noexcept(true) {}
	};

	using report_error_fn_t= heartbeats::report_error_fn_t;

	std::shared_ptr<num_msgs> msg_ctrs;

	explicit sink(std::shared_ptr<num_msgs> ctrs)
		: msg_ctrs(ctrs) {
	}

	template<class Buff, class Session>
	bool process_msg(Buff& buff, Session&, Session&) {
		using hdr_t= typename msg_details_t::Header_t;
		[[maybe_unused]] hdr_t const& hdr= reinterpret_cast<hdr_t const&>(buff);
		DEBUG_ASSERT(hdr.is_valid());
		++msg_ctrs->recv;
		return false;
	}

	friend std::ostream& operator<<(std::ostream& os, sink const& sr) noexcept(false) {
		os << "msg_ctrs=" << sr.msg_ctrs;
		return os;
	}
};

template<class SktT, class MsgsT>
struct simple_reflect {
	using socket_t= SktT;
	using src_msg_details_t= MsgsT;
	using msg_details_t= src_msg_details_t;

	struct heartbeats {
		using report_error_fn_t= std::function<void(std::string svr_hb_details, std::exception_ptr eptr)>;

		enum : unsigned {
			max_missed_heartbeats= 1
		};

		static inline constexpr const std::chrono::seconds heartbeat_interval{1};

		template<class Skt>
		explicit constexpr heartbeats(Skt const&, report_error_fn_t&) noexcept(true) {}

		static constexpr void stop() noexcept(true) {}
	};

	using report_error_fn_t= heartbeats::report_error_fn_t;

	std::shared_ptr<num_msgs> msg_ctrs;

	explicit simple_reflect(std::shared_ptr<num_msgs> ctrs)
		: msg_ctrs(ctrs) {
	}

	template<class Buff>
	bool process_msg(Buff& buff, socket_t&, socket_t& client_skt) {
		using hdr_t= typename msg_details_t::Header_t;
		[[maybe_unused]] hdr_t const& hdr= reinterpret_cast<hdr_t const&>(buff);
		DEBUG_ASSERT(hdr.is_valid());
		++msg_ctrs->recv;
		client_skt.write(buff);
		++msg_ctrs->send;
		DEBUG_ASSERT(msg_ctrs->recv == msg_ctrs->send);
		return false;
	}

	template<class Buff, class Session>
	bool process_msg(Buff& buff, Session&, Session& client_skt) {
		using hdr_t= typename msg_details_t::Header_t;
		[[maybe_unused]] hdr_t const& hdr= reinterpret_cast<hdr_t const&>(buff);
		DEBUG_ASSERT(hdr.is_valid());
		++msg_ctrs->recv;
		client_skt.socket().write(buff);
		++msg_ctrs->send;
		DEBUG_ASSERT(msg_ctrs->recv == msg_ctrs->send);
		return false;
	}

	friend std::ostream& operator<<(std::ostream& os, simple_reflect const& sr) noexcept(false) {
		os << "msg_ctrs=" << sr.msg_ctrs;
		return os;
	}
};

using asio_client_mgr_t= socket::asio::client_manager<
	ppd::api_lock_traits<ppd::platform_api, ppd::heavyweight_threading>::critical_section_type::lock_type>;
using glibc_client_mgr_t= socket::glibc::client_manager<
	ppd::api_lock_traits<ppd::platform_api, ppd::heavyweight_threading>::critical_section_type::lock_type>;

template<template<class, class> class ProcRules>
using cxns_types= boost::mpl::list<
	std::pair<
		asio_client_mgr_t,
		socket::svr<
			socket::server_manager::loopback<
				ProcRules<asio_client_mgr_t::socket_t, msgs_t::static_size>,
				typename ProcRules<asio_client_mgr_t::socket_t, msgs_t::static_size>::heartbeats,
				asio_client_mgr_t::socket_t> > >,
	std::pair<
		glibc_client_mgr_t,
		socket::svr<
			socket::server_manager::loopback<
				ProcRules<glibc_client_mgr_t::socket_t, msgs_t::static_size>,
				typename ProcRules<glibc_client_mgr_t::socket_t, msgs_t::static_size>::heartbeats,
				glibc_client_mgr_t::socket_t> > >,
	std::pair<
		asio_client_mgr_t,
		socket::svr<
			socket::server_manager::loopback<
				ProcRules<asio_client_mgr_t::socket_t, msgs_t::variable_size>,
				typename ProcRules<asio_client_mgr_t::socket_t, msgs_t::variable_size>::heartbeats,
				asio_client_mgr_t::socket_t> > >,
	std::pair<
		glibc_client_mgr_t,
		socket::svr<
			socket::server_manager::loopback<
				ProcRules<glibc_client_mgr_t::socket_t, msgs_t::variable_size>,
				typename ProcRules<glibc_client_mgr_t::socket_t, msgs_t::variable_size>::heartbeats,
				glibc_client_mgr_t::socket_t> > > >;

using cxns_sink_types= cxns_types<sink>;
using cxns_reflect_types= cxns_types<simple_reflect>;

BOOST_AUTO_TEST_SUITE(socket_tests)

BOOST_AUTO_TEST_SUITE(server)

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, cxns_t, cxns_reflect_types) {
	using svr_t= typename cxns_t::second_type;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	BOOST_CHECK_NO_THROW(
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)}));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_client, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	svr_t svr(
		typename svr_t::ctor_args{
			boost::asio::ip::address(),
			unused_port,
			svr_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		proc_rules,
		report_error,
		ts,
		libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
	BOOST_CHECK_NO_THROW(
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_two_successive_clients, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	svr_t svr(
		typename svr_t::ctor_args{
			boost::asio::ip::address(),
			unused_port,
			svr_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		proc_rules,
		report_error,
		ts,
		libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
	{
		BOOST_CHECK_NO_THROW(
			client_t skt(
				msgs_t::min_msg_size,
				msgs_t::max_msg_size,
				timeout,
				client_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				just_connect()));
	}
	{
		BOOST_CHECK_NO_THROW(
			client_t skt(
				msgs_t::min_msg_size,
				msgs_t::max_msg_size,
				timeout,
				client_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				just_connect()));
	}
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_two_simultaneous_clients, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	svr_t svr(
		typename svr_t::ctor_args{
			boost::asio::ip::address(),
			unused_port,
			svr_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority},
		proc_rules,
		report_error,
		ts,
		libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
	BOOST_CHECK_NO_THROW(
		client_t skt1(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		client_t skt2(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_client_one_message, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(!skt.local_ip().empty());
		BOOST_CHECK(skt.socket().is_open());
		typename msgs_t::message msg;
		BOOST_CHECK_NO_THROW(skt.write(msg));
		BOOST_CHECK(skt.socket().is_open());
		typename msgs_t::message response;
		BOOST_CHECK(!skt.read(response));
		BOOST_CHECK(skt.socket().is_open());
		BOOST_CHECK_EQUAL(response.c, msg.c);
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 1);
		BOOST_CHECK(skt.socket().is_open());
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_client_one_message_reconnect_sequential, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		{
			client_t skt(
				msgs_t::min_msg_size,
				msgs_t::max_msg_size,
				timeout,
				client_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				just_connect());
			BOOST_CHECK(skt.socket().is_open());
			typename msgs_t::message msg;
			BOOST_CHECK_NO_THROW(skt.write(msg));
			typename msgs_t::message response;
			BOOST_CHECK(!skt.read(response));
			BOOST_CHECK_EQUAL(response.c, msg.c);
			BOOST_CHECK_EQUAL(msg_ctrs->recv, 1);
			BOOST_CHECK(skt.socket().is_open());
		}
		{
			client_t skt(
				msgs_t::min_msg_size,
				msgs_t::max_msg_size,
				timeout,
				client_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				just_connect());
			BOOST_CHECK(skt.socket().is_open());
			typename msgs_t::message msg;
			BOOST_CHECK_NO_THROW(skt.write(msg));
			typename msgs_t::message response;
			BOOST_CHECK(!skt.read(response));
			BOOST_CHECK_EQUAL(response.c, msg.c);
			BOOST_CHECK_EQUAL(msg_ctrs->recv, 2);
			BOOST_CHECK(skt.socket().is_open());
		}
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_server_and_client_two_messages, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());
		typename msgs_t::message msg1;
		BOOST_CHECK_NO_THROW(skt.write(msg1));
		typename msgs_t::message response1;
		BOOST_CHECK(!skt.read(response1));
		BOOST_CHECK_EQUAL(response1.c, msg1.c);
		typename msgs_t::heartbeat msg2;
		BOOST_CHECK_NO_THROW(skt.write(msg2));
		typename msgs_t::heartbeat response2;
		BOOST_CHECK(!skt.read(response2));
		BOOST_CHECK_EQUAL(response2.c, msg2.c);
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 2);
		BOOST_CHECK(skt.socket().is_open());
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(two_repeated_servers_and_clients, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());
		typename msgs_t::message msg;
		BOOST_CHECK_NO_THROW(skt.write(msg));
		typename msgs_t::message response;
		BOOST_CHECK(!skt.read(response));
		BOOST_CHECK_EQUAL(response.c, msg.c);
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 1);
		BOOST_CHECK(skt.socket().is_open());
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 1);
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());
		typename msgs_t::heartbeat msg;
		BOOST_CHECK_NO_THROW(skt.write(msg));
		typename msgs_t::heartbeat response;
		BOOST_CHECK(!skt.read(response));
		BOOST_CHECK_EQUAL(response.c, msg.c);
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 2);
		BOOST_CHECK(skt.socket().is_open());
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reflect_msgs_at_a_time, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 60000;
	const unsigned short loops_for_conv= 900;
#else
	const unsigned long num_loops= 100;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 0.1;

#ifdef JMMCG_PERFORMANCE_TESTS
	latency_timestamps ts{2 * 3 * num_loops * loops_for_conv};
#else
	no_latency_timestamps ts{0U};
#endif
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());

		auto send_and_receive_all= [&skt]() {
			auto send_and_receive= [&skt]() {
				const typename msgs_t::message msg1;
				skt.write(msg1);
				typename msgs_t::message response1;
				BOOST_CHECK(!skt.read(response1));
				BOOST_CHECK_EQUAL(response1.c, msg1.c);
				BOOST_CHECK(skt.socket().is_open());
			};

			const std::chrono::high_resolution_clock::time_point start_time= std::chrono::high_resolution_clock::now();
			for(unsigned long i= 0; i < num_loops; ++i) {
				send_and_receive();
			}
			const std::chrono::high_resolution_clock::time_point end_time= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()) / num_loops);
		};

		const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
			perc_conv_estimate,
			loops_for_conv,
			std::move(send_and_receive_all)));
		BOOST_CHECK(skt.socket().is_open());
		std::cout << boost::core::demangle(typeid(client_t).name()) << "\n\tMessage round-trip in-of-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
		BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
		ts.write_to_named_csv_file(std::cout, "socket_server_reflect_msgs_at_a_time_performance_latencies");
#endif
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 2 * num_loops);
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 2 * num_loops);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sink_one_msg_repeatedly, cxns_t, cxns_sink_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	const unsigned long num_loops= 100;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());

		auto send= [&skt]() {
			auto send_batch_of_msgs= [&skt]() {
				typename msgs_t::heartbeat msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			{
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send(
					[send_batch_of_msgs]() {
						send_batch_of_msgs();
					});
			}
		};

		BOOST_CHECK_NO_THROW(send());
		BOOST_CHECK_EQUAL(msg_ctrs->recv, num_loops);
		BOOST_CHECK(skt.socket().is_open());
	}
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sink_different_msgs_parallel, cxns_t, cxns_sink_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	const unsigned long num_loops= 100;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());

		auto send= [&skt]() {
			auto send_batch_of_msgs1= [&skt]() {
				typename msgs_t::message msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			auto send_batch_of_msgs2= [&skt]() {
				typename msgs_t::message msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i + num_loops;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			auto send_batch_of_hbs= [&skt]() {
				typename msgs_t::heartbeat msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			{
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send_msg1(
					[send_batch_of_msgs1]() {
						send_batch_of_msgs1();
					});
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send_msg2(
					[send_batch_of_msgs2]() {
						send_batch_of_msgs2();
					});
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send_hb(
					[send_batch_of_hbs]() {
						send_batch_of_hbs();
					});
			}
		};

		BOOST_CHECK_NO_THROW(send());
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 3 * num_loops);
		BOOST_CHECK(skt.socket().is_open());
	}
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reflect_msgs_parallel, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

	constexpr const unsigned long num_loops= 100;

	no_latency_timestamps ts(0U);
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());

		auto send_and_receive= [&skt]() {
			auto send_batch_of_msgs= [&skt]() {
				typename msgs_t::message msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			auto receive_batch_of_msgs= [&skt]() {
				typename msgs_t::message response;
				for(unsigned long i= 0; i < num_loops; ++i) {
					BOOST_CHECK(!skt.read(response));
					BOOST_CHECK(skt.socket().is_open());
				}
				BOOST_CHECK(response.is_valid());
				BOOST_CHECK_EQUAL(response.sequence, num_loops - 1);
			};
			{
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send(
					[send_batch_of_msgs]() {
						send_batch_of_msgs();
					});
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> receive(
					[receive_batch_of_msgs]() {
						receive_batch_of_msgs();
					});
			}
		};

		BOOST_CHECK_NO_THROW(send_and_receive());
		BOOST_CHECK_EQUAL(msg_ctrs->recv, num_loops);
		BOOST_CHECK(skt.socket().is_open());
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, num_loops);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(reflect_different_msgs_parallel, cxns_t, cxns_reflect_types) {
	using client_t= typename cxns_t::first_type;
	using svr_t= typename cxns_t::second_type;
	using msgs_t= typename svr_t::proc_rules_t::msg_details_t;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long num_loops= 1000;
	const unsigned short loops_for_conv= 200;
#else
	const unsigned long num_loops= 100;
	const unsigned short loops_for_conv= 1;
#endif
	const double perc_conv_estimate= 0.1;

#ifdef JMMCG_PERFORMANCE_TESTS
	latency_timestamps ts{2 * 3 * num_loops * loops_for_conv};
#else
	no_latency_timestamps ts{0U};
#endif
	std::shared_ptr<num_msgs> msg_ctrs(new num_msgs);
	typename svr_t::proc_rules_t proc_rules(msg_ctrs);
	typename svr_t::report_error_fn_t report_error= [] NEVER_INLINE(std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", exception information: " << eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: " << svr_details << ", unknown exception.");
		}
	};
	{
		svr_t svr(
			typename svr_t::ctor_args{
				boost::asio::ip::address(),
				unused_port,
				svr_t::socket_t::socket_priority::high,
				exchanges::common::thread_traits::exchange_simulator_thread.core,
				exchanges::common::thread_traits::exchange_simulator_thread.priority},
			proc_rules,
			report_error,
			ts,
			libisimud::exchanges::common::thread_traits::api_threading_traits::thread_name_t{"svr" LIBJMMCG_ENQUOTE(__LINE__)});
		client_t skt(
			msgs_t::min_msg_size,
			msgs_t::max_msg_size,
			timeout,
			client_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			just_connect());
		BOOST_CHECK(skt.socket().is_open());

		auto send_and_receive= [&skt]() {
			auto send_batch_of_msgs1= [&skt]() {
				typename msgs_t::message msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			auto send_batch_of_heartbeats= [&skt]() {
				typename msgs_t::heartbeat msg;
				BOOST_CHECK(msg.is_valid());
				for(unsigned long i= 0; i < num_loops; ++i) {
					msg.sequence= i;
					skt.write(msg);
					BOOST_CHECK(skt.socket().is_open());
				}
			};
			auto receive_batch_of_msgs= [&skt]() {
				typename msgs_t::msg_buffer_t::value_type response[msgs_t::max_msg_size];
				typename msgs_t::Header_t const* hdr= reinterpret_cast<typename msgs_t::Header_t const*>(response);
				for(unsigned long i= 0; i < 2 * num_loops; ++i) {
					BOOST_CHECK(!skt.read(response));
					BOOST_CHECK(skt.socket().is_open());
				}
				BOOST_CHECK(hdr->is_valid());
				BOOST_CHECK_EQUAL(hdr->sequence, num_loops - 1);
			};
			const std::chrono::high_resolution_clock::time_point start_time= std::chrono::high_resolution_clock::now();
			{
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send(
					[send_batch_of_msgs1]() {
						send_batch_of_msgs1();
					});
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> send_heartbeats(
					[send_batch_of_heartbeats]() {
						send_batch_of_heartbeats();
					});
				ppd::jthread<ppd::platform_api, ppd::heavyweight_threading> receive(
					[receive_batch_of_msgs]() {
						receive_batch_of_msgs();
					});
			}
			const std::chrono::high_resolution_clock::time_point end_time= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()) / num_loops);
		};

		// TODO		const lock_all_proc_mem locker(lock_all_proc_mem::flags::current|lock_all_proc_mem::flags::on_fault);
		BOOST_CHECK_NO_THROW(send_and_receive());
		BOOST_CHECK_EQUAL(msg_ctrs->recv, 2 * num_loops);
		BOOST_CHECK(skt.socket().is_open());

		const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
			perc_conv_estimate,
			loops_for_conv,
			std::move(send_and_receive)));
		std::cout << boost::core::demangle(typeid(client_t).name()) << "\n\tMessage round-trip out-of-order time (microseconds)=" << timed_results.first << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
		BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
		ts.write_to_named_csv_file(std::cout, "socket_server_reflect_different_msgs_parallel_performance_latencies");
#endif
	}
	BOOST_CHECK_EQUAL(msg_ctrs->send, 3 * 2 * num_loops);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
