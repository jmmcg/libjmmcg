/******************************************************************************
 ** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/int128_compatibility.hpp"

BOOST_AUTO_TEST_SUITE(int128_compatibility_tests)

BOOST_AUTO_TEST_SUITE(int188_t)

using test_type= libjmmcg::int128_t;

BOOST_AUTO_TEST_CASE(default_ctor) {
	BOOST_CHECK_EQUAL(test_type{}, 0);
}

BOOST_AUTO_TEST_CASE(default_ctor_two_equal) {
	BOOST_CHECK_EQUAL(test_type{}, test_type{});
}

BOOST_AUTO_TEST_CASE(ctor_one) {
	BOOST_CHECK_EQUAL(test_type{1}, 1);
}

BOOST_AUTO_TEST_CASE(ctor_one_two_equal) {
	BOOST_CHECK_EQUAL(test_type{1}, test_type{1});
	BOOST_CHECK_NE(test_type{0}, test_type{1});
}

BOOST_AUTO_TEST_CASE(ctor_9223372036854775808ULL) {
	BOOST_CHECK_EQUAL(test_type{922337206854775808LL}, 922337206854775808LL);
	BOOST_CHECK_NE(test_type{922337206854775808LL}, 42);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(test_type{2}, 2);
	BOOST_CHECK_NE(test_type{2}, 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(test_type{42}, 42);
	BOOST_CHECK_NE(test_type{42}, 1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(uint188_t)

using test_type= libjmmcg::uint128_t;

BOOST_AUTO_TEST_CASE(default_ctor) {
	BOOST_CHECK_EQUAL(test_type{}, 0);
}

BOOST_AUTO_TEST_CASE(default_ctor_two_equal) {
	BOOST_CHECK_EQUAL(test_type{}, test_type{});
}

BOOST_AUTO_TEST_CASE(ctor_one) {
	BOOST_CHECK_EQUAL(test_type{1}, 1);
}

BOOST_AUTO_TEST_CASE(ctor_one_two_equal) {
	BOOST_CHECK_EQUAL(test_type{1}, test_type{1});
	BOOST_CHECK_NE(test_type{0}, test_type{1});
}

BOOST_AUTO_TEST_CASE(ctor_9223372036854775808ULL) {
	BOOST_CHECK_EQUAL(test_type{9223372036854775808ULL}, 9223372036854775808ULL);
	BOOST_CHECK_NE(test_type{9223372036854775808ULL}, 42);
}

BOOST_AUTO_TEST_CASE(check_value_2) {
	BOOST_CHECK_EQUAL(test_type{2}, 2);
	BOOST_CHECK_NE(test_type{2}, 1);
}

BOOST_AUTO_TEST_CASE(check_value_42) {
	BOOST_CHECK_EQUAL(test_type{42}, 42);
	BOOST_CHECK_NE(test_type{42}, 1);
}

BOOST_AUTO_TEST_CASE(to_string_1) {
	std::ostringstream os;
	test_type t{1};
	os << t;
	BOOST_CHECK_EQUAL(os.str(), "1");
}

BOOST_AUTO_TEST_CASE(to_string_2) {
	std::ostringstream os;
	test_type t{2};
	os << t;
	BOOST_CHECK_EQUAL(os.str(), "2");
}

BOOST_AUTO_TEST_CASE(to_string_9223372036854775808) {
	std::ostringstream os;
	test_type t{9223372036854775808ULL};
	os << t;
	BOOST_CHECK_EQUAL(os.str(), "9223372036854775808");
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
