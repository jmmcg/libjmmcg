/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>

#include "core/unordered_tuple.hpp"

using namespace libjmmcg;

struct base {
	using key_type= std::int32_t;
	static_assert(sizeof(key_type) <= sizeof(std::size_t));

	struct hasher {
		using key_type= base::key_type;

		enum : unsigned {
			denominator= 3
		};

		static constexpr std::size_t result(key_type k) noexcept(true) {
			return k;
		}

		constexpr std::size_t operator()(key_type k) const noexcept(true) {
			return k;
		}

		constexpr std::size_t operator()(key_type k, std::size_t r) const noexcept(true) {
			return k ^ r;
		}
	};

	virtual base::key_type hashed() const= 0;
	virtual int fn(int j) const= 0;
	virtual void nfn(int j)= 0;
	virtual std::string to_string() const noexcept(false)= 0;

protected:
	virtual ~base()= default;
};

struct int1 final : base {
	static inline constexpr const base::key_type hash= 0;

	int i_;

	explicit int1(int i)
		: i_(i) {}

	base::key_type hashed() const override {
		return hash;
	}

	int fn(int j) const override {
		return i_ * j;
	}

	void nfn(int j) override {
		i_+= j;
	}

	std::string to_string() const noexcept(false) override {
		std::ostringstream os;
		os << i_;
		return os.str();
	}
};

struct int2 final : base {
	static inline constexpr const base::key_type hash= 1;

	int i_;

	explicit int2(int i)
		: i_(i) {}

	base::key_type hashed() const override {
		return hash;
	}

	int fn(int j) const override {
		return i_ + j;
	}

	void nfn(int j) override {
		i_+= j;
	}

	std::string to_string() const noexcept(false) override {
		std::ostringstream os;
		os << i_;
		return os.str();
	}
};

struct double1 final : base {
	static inline constexpr const base::key_type hash= 2;

	double i_;

	explicit double1(double i)
		: i_(i) {}

	base::key_type hashed() const override {
		return hash;
	}

	int fn(int j) const override {
		return static_cast<int>(i_ + j);
	}

	void nfn(int j) override {
		i_+= j;
	}

	std::string to_string() const noexcept(false) override {
		std::ostringstream os;
		os << i_;
		return os.str();
	}
};

template<class T>
struct extract {
	static inline constexpr const typename T::key_type value= T::hash;
};

BOOST_AUTO_TEST_SUITE(unordered_tuple_tests)

BOOST_AUTO_TEST_CASE(ctor) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	BOOST_CHECK_NO_THROW(collection_type colln(int1(667), int2(42), double1(68.999999)));
	BOOST_CHECK_EQUAL(std::tuple_size<collection_type::all_elements_t>::value, 3);
	BOOST_CHECK_EQUAL((extract<std::tuple_element<0, collection_type::all_elements_t>::type>::value), int1::hash);
	BOOST_CHECK_EQUAL((extract<std::tuple_element<1, collection_type::all_elements_t>::type>::value), int2::hash);
	BOOST_CHECK_EQUAL((extract<std::tuple_element<2, collection_type::all_elements_t>::type>::value), double1::hash);
}

BOOST_AUTO_TEST_CASE(access_in_order) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(667), int2(42), double1(68.999999));
	BOOST_CHECK_EQUAL(colln[int1::hash].hashed(), int1::hash);
	BOOST_CHECK_EQUAL(colln[int2::hash].hashed(), int2::hash);
	BOOST_CHECK_EQUAL(colln[double1::hash].hashed(), double1::hash);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
}

BOOST_AUTO_TEST_CASE(access_reverse_order) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(667), int2(42), double1(68.999999));
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
}

BOOST_AUTO_TEST_CASE(access_in_then_reverse_order) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(667), int2(42), double1(68.999999));
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
}

BOOST_AUTO_TEST_CASE(access_reverse_then_in_order) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(667), int2(42), double1(68.999999));
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
}

BOOST_AUTO_TEST_CASE(access_in_order_shuffle_order_colln_types) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, double1, int2, int1>;

	collection_type colln(double1(68.999999), int2(42), int1(667));
	BOOST_CHECK_EQUAL(colln[int1::hash].hashed(), int1::hash);
	BOOST_CHECK_EQUAL(colln[int2::hash].hashed(), int2::hash);
	BOOST_CHECK_EQUAL(colln[double1::hash].hashed(), double1::hash);
	BOOST_CHECK_EQUAL(colln[int1::hash].fn(1), 667);
	BOOST_CHECK_EQUAL(colln[int2::hash].fn(13), 55);
	BOOST_CHECK_EQUAL(colln[double1::hash].fn(7), 75);
}

BOOST_AUTO_TEST_CASE(accumulate) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(1), int2(2), double1(3.1));
	auto const ret= colln.accumulate(1.0f, [](auto const& v, auto const& rhs) {
		return v + static_cast<float>(rhs.fn(static_cast<int>(v)));
	});
	BOOST_CHECK_EQUAL(ret, 15);
}

BOOST_AUTO_TEST_CASE(stream_out) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(667), int2(42), double1(68.999999));
	std::stringstream ss;
	ss << colln;
	BOOST_CHECK(!ss.str().empty());
}

BOOST_AUTO_TEST_CASE(generate) {
	using collection_type= unordered_tuple<base::key_type, base, base::hasher, extract, int1, int2, double1>;

	collection_type colln(int1(1), int2(2), double1(3.1));
	std::stringstream ss;
	ss << colln;
	colln.generate([](auto& v) {
		v.nfn(2);
	});
	std::stringstream ssa;
	ssa << colln;
	BOOST_CHECK_NE(ss.str(), ssa.str());
}

BOOST_AUTO_TEST_SUITE_END()
