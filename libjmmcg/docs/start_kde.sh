## Copyright (c) 2017 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#!/bin/bash

export KDEDIRS=$HOME/usr/local/
export QTDIR=$HOME/usr/local/Trolltech/Qt-4.8.7
export XDG_DATA_DIRS=$KDEDIRS/share:$XDG_DATA_DIRS:/usr/share  
export XDG_CONFIG_DIRS=$KDEDIRS/etc/xdg:$XDG_CONFIG_DIRS:/etc/xdg  
export PATH=$KDEDIRS/bin:$QTDIR/bin:$PATH  
export LD_LIBRARY_PATH=$KDEDIRS/lib:$KDEDIRS/lib64:$QTDIR/lib:$LD_LIBRARY_PATH
echo $LD_LIBRARY_PATH
export QT_PLUGIN_PATH=$KDEDIRS/lib/plugins:$KDEDIRS/lib64/plugins:$KDEDIRS/lib/x86_64-linux-gnu/plugins:$QTDIR/plugins:$QT_PLUGIN_PATH  
# (lib64 instead of lib on some systems, like OpenSUSE)
export QML2_IMPORT_PATH=$KDEDIRS/lib/qml:$KDEDIRS/lib64/qml:$KDEDIRS/lib/x86_64-linux-gnu/qml:$QTDIR/qml  
export QML_IMPORT_PATH=$QML2_IMPORT_PATH  
export KDE_SESSION_VERSION=4
export KDE_FULL_SESSION=true
kbuildsycoca4 --noincremental
$1 &
